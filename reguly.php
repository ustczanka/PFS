<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Scrabble : Reguły gry w SCRABBLE</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("scrabble","reguly");</script>
	<style>
	table.klasyfikacja td img{
			vertical-align: middle;
	}
	table.klasyfikacja td:first-child{
		font-weight: normal;
		text-align: center;
		vertical-align: middle;
		padding: 10px;
	}
	table.klasyfikacja tr:first-child td:first-child{
		font-weight: bold;
		text-align: center;
	}
	table.klasyfikacja td{
		padding: 10px;
	}
	</style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Reguły gry")</script></h1>

<h2>Wprowadzenie</h2>
SCRABBLE to gra słowna dla 2, 3 lub 4 graczy. Polega na układaniu na planszy powiązanych ze sobą słów przy użyciu płytek z literami o różnej wartości -
przypomina to budowanie krzyżówki. Celem gry jest uzyskanie jak najwyższego wyniku. Każdy gracz stara się uzyskać jak najwięcej punktów układając słowa
w taki sposób, by wykorzystać wartość liter i premiowane pola na planszy. Zależnie od umiejętności graczy, suma uzyskanych w grze punktów może osiągnąć
od 400 do 800, albo nawet więcej.

<h2>Zawartość kompletu</h2>
<ul>
	<li><b>100 płytek z literami</b><br />
    		<table class="klasyfikacja">
	<tr><td>Płytki / Pkt.</td><td>Płytki / Pkt.</td><td>Płytki / Pkt.</td><td>Płytki / Pkt.</td><td>Płytki / Pkt.</td><td>Płytki / Pkt.</td><td>Płytki / Pkt.</td><td>Płytki / Pkt.</td></tr>
	<tr><td><img src="files/tiles/a.png"> 9 / 1</td>
		<td><img src="files/tiles/a5.png"> 1 / 5</td>
		<td><img src="files/tiles/b.png"> 2 / 3</td>
		<td><img src="files/tiles/c.png"> 3 / 2</td>
		<td><img src="files/tiles/c6.png"> 1 / 6</td>
		<td><img src="files/tiles/d.png"> 3 / 2</td>
		<td><img src="files/tiles/e.png"> 7 / 1</td>
		<td><img src="files/tiles/e5.png"> 1 / 5</td>
	</tr><tr>
		
		<td><img src="files/tiles/f.png"> 1 / 5</td>
		<td><img src="files/tiles/g.png"> 2 / 3</td>
		<td><img src="files/tiles/h.png"> 2 / 3</td>
		<td><img src="files/tiles/i.png"> 8 / 1</td>
		<td><img src="files/tiles/j.png"> 2 / 3</td>
		<td><img src="files/tiles/k.png"> 3 / 2</td>
		<td><img src="files/tiles/l.png"> 3 / 2</td>
		<td><img src="files/tiles/l3.png"> 2 / 3</td>
	</tr><tr>
		<td><img src="files/tiles/m.png"> 3 / 2</td>
		<td><img src="files/tiles/n.png"> 5 / 1</td>
		<td><img src="files/tiles/n7.png"> 1 / 7</td>
		<td><img src="files/tiles/o.png"> 6 / 1</td>		
		<td><img src="files/tiles/o5.png"> 1 / 5</td>
		<td><img src="files/tiles/p.png"> 3 / 2</td>
		<td><img src="files/tiles/r.png"> 4 / 1</td>
		<td><img src="files/tiles/s.png"> 4 / 1</td>
	</tr><tr>
		<td><img src="files/tiles/s5.png"> 1 / 5</td>
		<td><img src="files/tiles/t.png"> 3 / 2</td>
		<td><img src="files/tiles/u.png"> 2 / 3</td>
		<td><img src="files/tiles/w.png"> 4 / 1</td>
		<td><img src="files/tiles/y.png"> 4 / 2</td>
		<td><img src="files/tiles/z.png"> 5 / 1</td>
		<td><img src="files/tiles/z9.png"> 1 / 9</td>
		<td><img src="files/tiles/z5.png"> 1 / 5</td>
	</tr><tr>
		<td><img src="files/tiles/blank.png"> 2 / 0</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
	</tr>
</table>
Gracze mają do dyspozycji 98 płytek z literami alfabetu oraz dwie płytki puste, które będziemy nazywać blankami (patrz powyżej). Każdej literze odpowiada określona liczba punktów  (widoczna w prawym dolnym rogu płytki). Blank nie ma żadnej wartości punktowej, ale może zastępować dowolną literę z zestawu. Gracz, który kładzie blanka, musi powiedzieć, jaką literę blank zastępuje - ustalenie to pozostanie ważne do końca gry.</li>
	<li><b>4 stojaki</b><br />Przed rozpoczęciem gry każdy jej uczestnik bierze stojak, na którym będzie układał swoje płytki.</li>
    <li><b>1 woreczek z płytkami</b><br />Przed rozpoczęciem gry wszystkie płytki muszą zostać umieszczone w woreczku.</li>
    <li><b>1 plansza</b><br />Plansza składa się z kwadratowych pól ułożonych w 15 wierszach i 15 kolumnach, rozdzielonych liniami. </li>
</ul>

<h2>Pola premiowe</h2>
Na planszy znajdują się specjalne premiowe pola, które zwiększają wartość układanych słów. Jasnoniebieskie pola podwajają wartość położonej na nim litery. Ciemnoniebieskie pola potrajają tę wartość. Jeśli jedna z liter układanego wyrazu zostanie umieszczona na jasnoczerwonym polu, wartość całego wyrazu zostanie podwojona, a jeśli na ciemnoczerwonym polu - potrojona. Jeśli słowo znajdzie się na polach zarówno  premii literowych jak i słownych, przed podwojeniem lub potrojeniem wartości słowa zostają uwzględnione wszystkie premie literowe. <br />
Każda premia dotyczy tylko tego ruchu, w którym gracz kładzie na niej płytkę z literą.<br /><br />
<i>Uwaga: Kiedy na polu podwójnej lub potrójnej premii słownej zostanie położony blank, suma wartości liter tworzących słowo zostanie podwojona lub potrojona mimo zerowej wartości samego blanka. Blank położony na polu podwójnej lub potrójnej premii literowej nadal ma wartość zerową.</i>

<h2>Współrzędne</h2>
Wokół zewnętrznej krawędzi planszy umieszczono współrzędne. Służą one wyłącznie do opisu położenia pola na planszy. Na przykład, pole w lewym górnym rogu będzie nosić nazwę A1, jeśli znajdzie się na nim pierwsza litera słowa ułożonego poziomo. Jeśli natomiast zaczyna się na nim słowo ułożone pionowo, będziemy je nazywać 1A.

<h2>Zapisywanie wyników</h2>
Jeden z graczy otrzymuje zadanie zapisywania wyników, może także brać udział w grze. Gracz ten zapisuje punkty graczy po zakończeniu każdego ruchu.

<h2>Rozpoczęcie gry</h2>
Wszystkie płytki zostają umieszczone w woreczku. Aby ustalić, kto rozpocznie grę, każdy uczestnik bierze z woreczka jedną płytkę. Gracz, który ma literę najbliższą początkowi alfabetu, będzie zaczynać (blank zastępuje dowolną literę z zestawu). Płytki wracają do woreczka i muszą zostać wymieszane. Wszyscy gracze po kolei biorą po siedem płytek i układają je na swoich stojakach. Gra toczy się zgodnie z ruchem wskazówek zegara. W każdym ruchu gracz może wyłożyć słowo na planszy, wymienić płytki, lub opuścić kolejkę.

<h2>Ułożenie pierwszego wyrazu</h2>
Pierwszy gracz tworzy z dwóch lub więcej płytek słowo i układa je na planszy poziomo (od lewej do prawej) lub pionowo ( z góry na dół), w taki sposób, by jedna z płytek znalazła się na centralnym polu (z gwiazdką)). Nie wolno układać słów ukośnie. Wszystkie płytki użyte podczas ruchu muszą znaleźć się po ułożeniu na planszy w jednej ciągłej linii pionowej lub poziomej.

<h2>Wymiana płytek</h2>
Każdy z graczy może wykorzystać swój ruch na wymianę dowolnej liczby płytek ze swojego stojaka. W tym celu zdejmuje je ze stojaka tak, by nie było widać liter, bierze z woreczka losowo taką samą liczbę płytek, a następnie wrzuca odłożone płytki do woreczka. Na tym jego ruch się kończy i kolejka przechodzi na gracza z lewej strony (jeżeli w grze bierze udział więcej niż dwu graczy).

<h2>Opuszczenie kolejki</h2>
Oprócz możliwości ułożenia słów na planszy lub wymiany płytek, gracz może także postanowić, że opuści kolejkę, niezależnie od tego, czy jest w stania ułożyć słowo, czy nie. gra kończy się jednak, jeśli wszyscy gracze dwa razy z rzędu opuszczą kolejkę.

<h2>Dozwolone słowa</h2>
Dozwolone jest korzystanie ze wszystkich słów znajdujących się w słownikach języka polskiego oraz ich poprawnych form gramatycznych, z wyjątkiem tych, które zaczynają się od wielkiej litery, są skrótami, przedrostkami lub przyrostkami albo wymagają użycia apostrofu lub łącznika. Wszystkie słowa
dozwolone w grze zawarte są w <a href="osps.php">Oficjalnym Słowniku Polskiego Scrabblisty (OSPS)</a>.<br />
Po zakończeniu ruchu, nie można zmieniać położenia  na planszy żadnej płytki, ani jej zdejmować, chyba że słowo zostanie słusznie zakwestionowane.

<h2>Kwestionowanie słów </h2>
Po ułożeniu słowa a jeszcze przed zapisaniem wyniku i przejściem kolejki na następnego gracza można zakwestionować poprawność tego słowa. Można wtedy (i tylko wtedy) sprawdzić je w słowniku. Jeśli zakwestionowane słowo okaże się niedozwolone, gracz musi zabrać z planszy wyłożone przez siebie płytki i
traci ruch.

<h2>Obliczanie wartości pierwszego słowa</h2>
Gracz kończy ruch obliczając wartość ułożonego słowa i przekazując ją zapisującemu, który rejestruje ją na arkuszu wyników. Wartość słowa oblicza się dodając liczby widoczne na płytkach, z uwzględnieniem wszystkich pól premiowych na których słowo się znajduje.

<h2>Zakończenie ruchu</h2>
Na koniec każdego ruchu gracz dobiera z woreczka tyle płytek, ile wyłożył, dzięki czemu zawsze ma na stojaku siedem płytek.

<h2>Specjalna premia </h2>
Gracz, któremu uda się w jednym ruchu wyłożyć wszystkie siedem płytek, otrzymuje 50-punktową premię. Jest ona dodawana do wyniku po uwzględnieniu wszystkich pól premiowych.

<h2>Ruch następnego gracza</h2>
Drugi gracz, podobnie jak każdy następny, ma do wyboru: dołożenie jednej lub kilku płytek do tych. które już leżą na planszy i utworzenie nowych słów o długości co najmniej dwóch liter, wymianę płytek albo opuszczenie kolejki. Wszystkie płytki użyte podczas ruchu muszą znaleźć się po ułożeniu na planszy w jednej ciągłej linii pionowej lub poziomej. Jeżeli ułożone płytki łączą się z innymi płytkami na sąsiednich palach, muszą z nimi tworzyć pełne i sensowne słowa, podobnie jak w klasycznej krzyżówce. Gracz uzyskuje punkty za wszystkie słowa utworzone lub zmodyfikowane w wyniku swojego
ruchu. Nie należy zapominać o polach premiowych na których gracz położył swoje płytki.

<h2>Tworzenie nowych słów</h2>
Nowe słowa można tworzyć na pięć sposobów:
<ol>
	<li>Dołożenie jednej lub kilku płytek na początku lub na końcu słowa już znajdującego się na planszy, albo też zarówno na początku, jak i na końcu takiego słowa.</li>
	<li>Ułożenie słowa pod kątem prostym do słowa znajdującego się na planszy. Nowe słowo musi wykorzystywać jedną z liter słowa leżącego na planszy.</li>
	<li>Ułożenie całego słowa równolegle do słowa już istniejącego w taki sposób, by stykające się płytki także tworzyły całe słowa.</li>
	<li>Nowe słowo może także dodać literę do istniejącego słowa.</li>
    <li>Ostatnia możliwość to „mostek” między dwiema lub więcej literami.</li>
</ol>
 
<h2>Zakończenie gry</h2>
Gra kończy się, kiedy wszystkie płytki zostały wyjęte z woreczka, a jeden z graczy wykorzystał wszystkie płytki ze swojego stojaka. Gra kończy się także wtedy, gdy zostaną wykonane wszystkie możliwe ruchy i wszyscy gracze opuszczą dwie kolejki z rzędu. <br />
<i>Uwaga: Opuszczenie przez wszystkich graczy dwu kolejek z rzędu kończy grę niezależnie od tego jak wiele płytek pozostało jeszcze w woreczku i na stojakach.</i><br />
Po zsumowaniu wszystkich punktów wynik każdego gracza zmniejsza się o sumę punktów widniejących na płytkach pozostających na jego stojaku, a jeżeli
jeden z graczy wykorzystał wszystkie płytki, jego wynik zwiększa się o sumę niewykorzystanych płytek wszystkich pozostałych uczestników gry. Przykład:
jeśli graczowi nr 1 pozostały na stojaku litery Ż i B, jego wynik zostanie zmniejszony o 8 punktów. Gracz, który wykorzystał wszystkie płytki, dodaje sobie 8 punktów. <br />
Pamiętaj - wynik gry może zależeć od ostatniej litery w woreczku!

<h2>Wyjaśnienie często niewłaściwie interpretowanych reguł</h2>
<ul>
	<li>Jeżeli płytka styka się z inną płytką w sąsiednim polu, musi z nią tworzyć pełne słowo, podobnie jak w klasycznej krzyżówce.</li>
    <li>Słowo można w jednym ruchu przedłużyć z obu jego końców, tak jak w swyrazach PRÓBUJE i S-PRÓBUJE-MY.</li>
    <li>Wszystkie płytki użyte podczas ruchu muszą znaleźć się po ułożeniu na planszy w jednej ciągłej linii pionowej lub poziomej.</li>
    <li>Nie jest dozwolone dokładanie w jednym ruchu płytek do różnych słów w różnych częściach planszy.</li>
    <li>Dodatkowe punkty za pola premiowe dodaje się tylko w tym ruchu, w którym zostały na nich położone płytki.</li>
    <li>Jeśli w jednym ruchu zostanie utworzonych kilka słów, liczone jest każde takie słowo. Wartość wspólnych liter (ze wszystkimi premiami, jeśli leżą na polach premiowych) jest liczona dla każdego słowa oddzielnie.</li>
    <li>Jeśli wyraz przechodzi przez dwa pola <i>premii słownych,</i> suma punktów jego liter jest mnożona przez 4 (2x2), albo mnożona przez 9 (3x3).</li>
    <li>Kiedy na polu podwójnej lub potrójnej premii słownej zostanie położony blank, suma wartości liter tworzących słowo zostanie podwojona lub potrojona mimo zerowej wartości samego blanka. Blank położony na polu podwójnej lub potrójnej premii literowej nadal ma wartość zerową.</li>
</ul>

<h2>Inne często zadawane pytania</h2>
Kiedy jeden z graczy wykorzysta wszystkie swoje płytki, a woreczek jest pusty, gra kończy się, nie można już wykonywać żadnych ruchów. Zdarza się, że żadnemu z graczy nie uda się pozbyć wszystkich płytek. Gra toczy się wtedy do chwili wykonania wszystkich możliwych ruchów. Jeśli gracz nie jest w stanie wykonać ruchu, opuszcza kolejkę. Jeśli wszyscy gracze dwa razy z rzędu opuszczą kolejkę, gra kończy się.<br /><br />

Podczas układania słów nie wolno korzystać ze słownika. Może on być wykorzystywany wyłącznie po wyłożeniu słowa l zakwestionowaniu go.<br /><br />

Każde słowo może być użyte podczas gry wielokrotnie.

<?require_once "files/php/bottom.php"?>
</body>
</html>

