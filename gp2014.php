<?
require_once "files/php/funkcje.php";

$gp_tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( 'YEAR(data_od)' => '2014', 'rank' => $TOUR_STATUS[gp]),
    order   => array ( 'data_od' )
));

?>
<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Grand Prix 2014</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("turnieje","gp2014")</script>
    <style type="text/css">
        table.linki{margin: 20px auto 0 auto;}
        table.linki td{ padding: 8px;vertical-align: top;}
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Grand Prix 2014")</script></h1>

<h2>Turnieje Grand Prix 2014</h2>
Cykl Grand Prix 2014 obejmuje 6 turniejów:
<table class="linki ramkadolna">

<?php
$cnt = 1;
foreach ($gp_tours as $tour) {
    print "<tr><td>" . ($cnt++) . ".</td><td><a href='../turniej.php?id=$tour->id'>$tour->nazwa</a></td></tr>";
}
?>

</table>
<br><br>

<h2>Klasyfikacje</h2>
Cykl Grand Prix 2014 prowadzony będzie w trzech kategoriach:<br> <br><ol>
<li>Klasyfikacja Złota - główna (dla wszystkich uczestników turniejów z cyklu GP)
<li>Klasyfikacja Srebrna (dla uczestników turniejów z cyklu GP posiadających ranking poniżej 130,00)
<li>Klasyfikacja Brązowa (dla uczestników turniejów z cyklu GP posiadających ranking poniżej 115,00)
</ol>
<h2>Zasady punktacji</h2>
<ol>
        <li>Punkty do klasyfikacji głównej (Złotej) Grand Prix 2014 zdobywa tylu uczestników, ile wynosi część całkowita liczby obliczonej według wzoru: liczba uczestników, którzy uczestniczyli najmniej w 5 rundach turnieju, dzielona przez 3.
	<li>Zawodnik, który zajmie ostatnie punktowane miejsce, otrzymuje 1 punkt, każdy kolejny o jeden punkt więcej.
	<li>Dodatkowo zdobywcy 3 czołowych miejsc otrzymują premię: 10 punktów za miejsce pierwsze, 6 punktów za drugie i 2 punkty za trzecie (przykład 1: przy 62 uczestnikach: 20 miejsce - 1 punkt, 19-2, 18-3, itd., a rosnąco: 4 miejsce -17, 3-18+2, 2-19+6, 1-20+10), (przykład 2: przy 85 uczestnikach: 28 miejsce-1 punkt, 27-2, 26-3, itd. a rosnąco 4-25, 3-26+2, 2-27+6, 1-28+10.).
	<li>Minimalna liczba uczestników zdobywających punkty do klasyfikacji Grand Prix wynosi 20 (tzn. w przypadku turnieju, w którym uczestniczyło mniej niż 60 osób, minimum w pięciu rundach turnieju, zawodnik za miejsce 20 otrzymuje 1 punkt, za 19-2, itd., a za 4-17, za 3-18+2, za 2-19+6, za 1-20+10).
	<li>Maksymalna liczba uczestników zdobywających punkty do klasyfikacji Grand Prix wynosi 30 (tzn. w przypadku turnieju, w którym uczestniczyło więcej niż 90 osób, minimum w pięciu rundach turnieju, zawodnik za miejsce 30 otrzymuje 1 punkt, za 29-2, itd., za 4-27, za 3-28+2, za 2-29+6 i za 1-30+10).
	<li>Punkty do klasyfikacji srebrnej otrzymują zawodnicy, którzy w danym turnieju występowali z rankingiem poniżej 130,00. Zawodnikom tym przyznaje się do klasyfikacji srebrnej tyle punktów, ile zdobyli w klasyfikacji złotej, a dodatkowo za pierwsze dziesięć miejsc w swojej grupie rankingowej, tzn. za pierwsze miejsce wśród osób z rankingiem poniżej 130,00 zawodnik otrzymuje 10 punktów, za drugie 9 punktów, itd.
	<li>Punkty do klasyfikacji brązowej otrzymują zawodnicy, którzy w danym turnieju występowali z rankingiem poniżej 115,00. Zawodnikom tym przyznaje się do klasyfikacji brązowej tyle punktów, ile zdobyli w klasyfikacji złotej, a dodatkowo za pierwsze dziesięć miejsc w swojej grupie rankingowej, tzn. za pierwsze miejsce wśród osób z rankingiem poniżej 115,00 zawodnik otrzymuje 10 punktów, za drugie 9 punktów, itd.
	<li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Grand Prix 2014 do końcowej klasyfikacji zalicza się cztery najlepsze wyniki  z sześciu turniejów.
	<li>Przy równej liczbie punktów w klasyfikacji końcowej o kolejności decyduje wyższa zdobycz punktowa w najlepszym turnieju, a jeśli to nie da efektu (gracze mieli identyczny wynik) w kolejnym najlepszym turnieju, itd. Jeśli i to nie przyniesie rozstrzygnięcia, zawodnicy zajmują miejsce ex-aequo, a nagrody finansowe za ich miejsca są sumowane i dzielone po równo.
	 
</ol>

<h2>Nagrody</h2>
Nagrody w cyklu głównym Grand Prix (GP Złote) wynoszą:
<ul>
    <li>za I miejsce 600 zł</li>
    <li>za II miejsce 400 zł</li>
    <li>za III miejsce 200 zł</li>
</ul>
Nagrody w cyklu GP Srebrne wynoszą:
<ul>
    <li>za I miejsce 300 zł</li>
    <li>za II miejsce 200 zł</li>
    <li>za III miejsce 100 zł</li>
</ul>
Nagrody w cyklu GP Brązowe wynoszą:
<ul>
    <li>za I miejsce 300 zł</li>
    <li>za II miejsce 200 zł</li>
    <li>za III miejsce 100 zł</li>
</ul>
W przypadku zajęcia przez zawodnika miejsca premiowanego nagrodą w więcej niż jednej klasyfikacji (GP Złote, GP Srebrne, GP Brązowe), otrzymuje on najwyższą nagrodę, a pozostałe przyznawane są zawodnikom, którzy w odpowiedniej klasyfikacji zajęli kolejne miejsca.
<a name="klasyfikacja"></a>
<h2>Klasyfikacja Główna - Grand Prix Złote</h2>

<h2>Klasyfikacja Grand Prix Srebrne</h2>

<h2>Klasyfikacja Grand Prix Brązowe</h2>

<?include "files/php/bottom.php"?>
</body>
</html>
