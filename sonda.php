<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Sonda Mistrzostw Polski 2013</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
</head>

<body>



<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Sonda MP2013")</script></h1>
<center>
<table>
<tr><td>

<!-- SONDA -->
<script language="javascript">function ResultWindow(my_win_param) { window.open("http://www.sonda.pl/wait.php3","sondaplwin",my_win_param); return false; }</script><form method="post" action="http://www.sonda.pl/vote.php3" target="sondaplwin" OnSubmit="ResultWindow('toolbar=no,scrollbars=yes,directories=no,status=no,menubar=no,resizable=no,width=475,height=500')">
<table width="500" cellpadding="5" cellspacing="5" border="1">
<tr ><td bgcolor="#5588CC"><center><font size="2" color="#FFFFFF" face="Verdana"><b>SONDA</b></font></center></td></tr>
<tr><td bgcolor="#DDEEFF"><input type="hidden" name="id" value="619196"><input type="hidden" name="uid" value="166422"><font color="#000000" size="2" face="Verdana"><b>Kto zostanie Mistrzem Polski w tym roku?</b></font><br><br>
<table border="0" cellpadding="5" cellspacing="5">
<tr><td align="left" width="50%"><input type="radio" name="poll" value="1"><img src="foto/sonda/kmowka.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Krzysztof Mówka</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="2"><img src="foto/sonda/pzareba.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Paweł Zaręba</font></td></tr>
<tr><td align="left" width="50%"><input type="radio" name="poll" value="3"><img src="foto/sonda/pjackowski.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Paweł Jackowski</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="4"><img src="foto/sonda/malabrudzinski.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Michał Alabrudziński</font></td></tr>
<tr><td align="left" width="50%"><input type="radio" name="poll" value="5"><img src="foto/sonda/bmorawski.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Bartosz Morawski</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="6"><img src="foto/sonda/kmerklejn.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Kazimierz Merklejn</font></td></tr>
<tr><td align="left" width="50%"><input type="radio" name="poll" value="7"><img src="foto/sonda/kgorka.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Kamil Górka</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="8"><img src="foto/sonda/mwrzesniewski.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Mariusz Wrześniewski</font></td></tr>
<tr><td align="left" width="50%"><input type="radio" name="poll" value="9"><img src="foto/sonda/tzwolinski.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Tomasz Zwoliński</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="10"><img src="foto/sonda/splachta.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Szymon Płachta</font></td></tr>
<tr><td align="left" width="50%"><input type="radio" name="poll" value="11"><img src="foto/sonda/muglik.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Mirosław Uglik</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="12"><img src="foto/sonda/mczupryniak.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Maciej Czupryniak</font></td></tr>
<tr><td align="left" width="50%"><input type="radio" name="poll" value="13"><img src="foto/sonda/mmroziuk.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Marcin Mroziuk</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="14"><img src="foto/sonda/durbacki.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Dominik Urbacki</font></td></tr>
<tr><td align="left" width="50%"><input type="radio" name="poll" value="15"><img src="foto/sonda/rlenartowski.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Rafał Lenartowski</font></td>
<td align="left" width="50%"><input type="radio" name="poll" value="16"><img src="foto/sonda/jmrozowski.jpg" align="middle"><font size="2" color="#000000" face="Verdana">Jan Mrozowski</font></td></tr>
</table><center><input type="submit" value="Zagłosuj"><br><br><a href="http://www.sonda.pl/wait.php3" OnClick="javascript:window.open('http://www.sonda.pl/wyniki.php3?id=619196&uid=166422', 'sondaplwin', 'toolbar=no,scrollbars=yes,directories=no,status=no,menubar=no,resizable=no,width=475,height=500'); return false;">Zobacz wyniki</a><br></center></td></tr></table></form>
<!-- SONDA KONIEC -->
</td><td>&nbsp;</td><td>
<!-- SONDA -->
<script language= "javascript" src="http://www.sonda.pl/show.php3?id=619198&uid=166422">
</script>
<!-- SONDA KONIEC -->
<br><br>
<!-- SONDA -->
<script language= "javascript" src="http://www.sonda.pl/show.php3?id=619202&uid=166422">
</script>
<!-- SONDA KONIEC -->
</td> </tr>
</table>
</center>
<?require_once "files/php/bottom.php"?>
</body>
</html>
