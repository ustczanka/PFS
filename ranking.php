<?php
include_once "files/php/funkcje.php";

$tour = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( '<rank' => $TOUR_STATUS[vacation], '!rank' => 0, '<=data_do' => date ('Y-m-d'), '!ranking' => '' ),
    order   => array ( '!data_do' ),
    limit   => 1
));

$tour = $tour[0];
list ($ranking, $poczekalnia) = explode ("Poczekalnia", $tour->ranking);
$poczekalnia = "Poczekalnia" . $poczekalnia;
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Ogólnopolska Lista Rankingowa</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" />
    <![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script>
    <![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script>
    <![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("ranking","aktranking");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
    <style type="text/css">
        table td.lp{
            text-align: right;
        }
        table td.osoba{
            text-align: left;
        }
        #tabs-ranking2014 th, #tabs-ranking2014 td {
            padding: 3px 20px;
            text-align: left;
        }
        #tabs-ranking2014 .lp {
            padding: 3px 0px;
            text-align: right;
        }
    </style>
</head>

<body><?require_once "files/php/menu.php"?>
<h1><script>naglowek("Ogólnopolska Lista Rankingowa")</script></h1>

<div id="tabs" style="display:none;">
    <ul>
        <li><a href="#tabs-ranking">Lista rankingowa</a></li>
        <li><a href="#tabs-poczekalnia">Poczekalnia</a></li>
        <li><a href="#tabs-ranking2014">Ranking roczny 2014</a></li>
        <li><a href="#tabs-hollington">Jak powstaje ranking</a></li>
    </ul>

    <div id="tabs-ranking">
        <pre> <? print $ranking; ?> </pre>
    </div>

    <div id="tabs-poczekalnia">
        <pre> <? print $poczekalnia; ?> </pre>
    </div>


    <div id="tabs-ranking2014">
        <a href="rozne/skalp2014.xlsx">Pełna lista rankingu dla partii rozegranych w 2014 roku.</a><br /><br />
        <table>
           <tr><th class='lp'></th> <th></th> <th>Ranking</th> <th>Skalpy</th> <th>Partie</th> </tr>
<?
    $lp   = 1;
    $rows = pfs_select (array ( table => $DB_TABLES[year_rank], order => array ( '!ranking' ) ));

    foreach ($rows as $row) {
        print "<tr> <td class='lp'>" . $lp++ . "</td> <td>".$row->osoba."</td> <td>".$row->ranking."</td> <td>".$row->skalpy."</td> <td>".$row->partie."</td> </tr>";
    }
?>
        </table>
    </div>

    <div id="tabs-hollington">
        <h2>Metoda Hollingtona</h2>
        Do rankingu zalicza się wszystkie partie rozegrane podczas oficjalnych turniejów Scrabble w ciągu ostatnich dwóch lat. Aby znaleźć się na liście rankingowej, należy mieć rozegrane w tym okresie przynajmniej 30 partii. Zawodnikom nie posiadającym jeszcze rankingu przydziela się na czas trwania turnieju ranking tymczasowy równy 100.<br /><br />

        <i>Ranking to suma punktów pomocniczych, zdobytych na przeciwnikach (punkty te żargonowo zwane są „skalpami”), podzielona przez liczbę rozegranych partii.</i><br /><br />

        „Skalpem” zawodnika z rozegranej partii jest ranking przeciwnika:
        <ul>
            <li>powiększony o 50 w przypadku zwycięstwa;</li>
            <li>pomniejszony o 50 w przypadku porażki;</li>
            <li>nie zmieniony w przypadku remisu.</li>
        </ul> <br />
        Załóżmy, że gracz X ma ranking 107, a gracz Y ma ranking 96. Spotykają się ze sobą na turnieju.<br />
        Jeżeli wygra X, to jego „skalp” z tej partii wyniesie 146 (96 + 50), a „skalp” gracza Y wyniesie 57 (107 - 50).<br />
        Gdy wygra Y, to jego „skalp” wyniesie 157 (107 + 50), a „skalp” gracza X wyniesie 46 (96 - 50).<br />
        Gdy padnie remis, to „skalp” gracza X wyniesie 96, a „skalp” gracza Y wyniesie 107.<br /><br />

        Zasadę tę modyfikuje się w jednym przypadku — gdy różnica rankingów między przeciwnikami przekracza 50, a zwycięży gracz z wyższym rankingiem. Załóżmy, że gracz X ma ranking 135, a gracz Y ma ranking 70. Gdyby nie zmodyfikowano zasady generalnej, to w przypadku zwycięstwa gracza X, jego „skalp” wyniósłby 120 (70 + 50), zatem poniósłby on stratę mimo zwycięstwa, natomiast „skalp” gracza Y wyniósłby 85 (135 - 50), czyli zanotowałby on zysk mimo porażki. Modyfikacja zasady generalnej polega na tym, że w takim przypadku „skalpami” obu graczy są ich własne rankingi (dla gracza X - 135, a dla gracza Y - 70).<br /><br />

        Ranking zaokrągla się do najbliższej liczby całkowitej (dokładne 5/10 zakrągla się w górę). Jedynie dla potrzeb ustalenia kolejności na liście rankingowej porównuje się dokładne, nie zaokrąglone liczby.

        <h2>Poprawki PFS-u</h2>
        Od pewnego czasu dało się zauważyć, że ranking liczony tą metodą, po przekroczeniu ok. 150-200 partii, ulega nienaturalnemu usztywnieniu i, w przypadku graczy, których poziom gry się szybko zmienia (zwykle poprawia), przestaje odzwierciedlać ich rzeczywistą, aktualną siłę gry. Ponadto gracze, którzy mają ranking niższy od 100, są przeważnie (choćby z racji doświadczenia turniejowego) silniejsi od debiutantów, którym przecież na czas turnieju przydziela się ranking tymczasowy równy 100. W związku z tym Zarząd PFS w kwietniu 1999 wprowadził do opisanej metody modyfikacje, zgodnie z którymi:
        <ul>
            <li>dwuletni okres naliczania rankingu jest weryfikowany na bieżąco</li>
            <li>ranking jest liczony z takiej liczby ostatnio rozegranych turniejów, by liczba zaliczonych doń partii nie przekroczyła 200</li>
            <li>zawodnikom, którzy mają ranking niższy od 100, przydziela się na czas trwania turnieju ranking tymczasowy równy 100 (podobnie, jak debiutantom)</li>
        </ul>
    </div>
</div>






<?require_once "files/php/bottom.php"?>
</body>
</html>
