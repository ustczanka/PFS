<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: PFS : Informacje dla członków PFS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","dlaczlonkow");</script>
 	<style type="text/css">
 		p.konto{
			text-align:center;
			margin: 10px;
		}
 	</style>
</head>

<body>
<?include "files/php/menu.php"?>
<h1><script>naglowek("Informacje dla członków PFS")</script></h1>
<h2>Forum dyskusyjne</h2>
Zapraszamy do dyskusji o sprawach dotyczących Federacji. Jeżeli masz jakiekolwiek propozycje, uwagi, skargi, zażalenia zapraszamy do wejścia na forum, które znajduje się pod adresem <a href="http://forum.pfs.org.pl/" target="_blank">http://forum.pfs.org.pl/</a>.<br />
W sprawie dostępu do forum prosimy o kontakt z <a onClick="sendMail('k.wyrebkiewicz','pfs.org.pl')">Karolem Wyrębkiewiczem</a>.

<!--<h2> Kody do gry na www.zagraj.pl </h2>
Przypominamy, że członkowie PFS mają prawo do otrzymywania bezpłatnych kodów do gry na serwerze <a href="http://www.zagraj.pl/" target="_blank">www.zagraj.pl</a>. Członkowie otrzymują kody do gry zawsze przed rozpoczęciem danego miesiąca. W przypadku nieotrzymania aktualnego kodu prosimy o kontakt z <a onClick="sendMail('j.puchalski','pfs.org.pl')">Jarosławem Puchalskim</a>. 
-->
<h2>Ostateczny termin opłacania składek za 2014 r.</h2>
Zarząd przypomina członkom stowarzyszenia, którzy nie mają opłaconych do końca roku składek, że zgodnie z <a href="uchwala.php?rok=2013#uchwala7">uchwałą nr 7/2013 z 25.02.2013 r.</a> dzień <b>30 czerwca 2014 r.</b> jest ostatecznym terminem opłacenia składki za 2014 rok. W szczególności osoby, które nie opłacają składek za cały rok z góry, powinny do tego dnia opłacić składkę do końca roku.<br><br>

Aby uzyskać listę członków PFS z opłaconymi skladkami lub informację do kiedy składka jest opłacona,  prosimy o kontakt ze skarbnikiem PFS - Krzysztofem Bukowskim - mailowo: <a onClick="sendMail('k.bukowski','pfs.org.pl')">(napisz e-mail)</a> lub telefonicznie: 792 304 104.

<h2>Składka członkowska</h2>
Wysokość składek członkowskich za rok 2014 wynosi <b>84 zł</b>.<br>
Osoby przystępujące do Federacji płacą składkę za okres nie krótszy niż rok (kolejne składki można regulować w krótszych okresach).<br />
Informujemy też, że zgodnie z uchwałą Zarządu <a href="uchwala.php?rok=2007#uchwala2">nr 2/2007 z 13.01.2007 r.</a> obowiązują następujące zniżki w wysokości składki:
<ul>
	<li><b>składka rodzinna</b> w wysokości 50% (pierwsza osoba płaci 100% stawki, każda następna z rodziny 50% stawki)</li>
	<li><b>studenci i uczniowie</b> (do 26 roku życia) płacą składkę w wysokości 75%</li>
	<li><b>seniorzy</b> (osoby, które ukończyły 60 rok życia) płacą składkę w wysokości 50%</li>
	<li><b>nestorzy</b> (osoby, które ukończyły 70 rok życia) są zwolnieni z opłacania składek</li>
	<li><b>dzieci i młodzież</b> (do 18 roku życia) płacą składkę w wysokości 25%</li>
	<li><b>prowadzący szkolne koła scrabblowe</b> (zarejestrowane w programie "Scrabble w szkole") płacą składkę w wysokości 50%.</li>
</ul>
<br />
Składki można wpłacać na konto (z podaniem za kogo opłacona jest składka i za jaki okres):
<p class="konto">Polska Federacja Scrabble<br />
Volkswagen Bank direct<br />
<b>60 2130 0004 2001 0386 3883 0001</b></p>
jak również do skarbnika Federacji w czasie któregoś z turniejów.
<?require_once "files/php/bottom.php"?>
</body>
</html>
