<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Różności : Scrabble w internecie</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("roznosci","scrabblenet");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Scrabble w internecie")</script></h1>
Masz własną stronę o Scrabble lub znasz jakąś fajną? <a onClick="sendMail('pfs','pfs.org.pl')">Napisz!</a>

<h2>Scrabble online</h2>
<ul>
	<li><a href="http://www.zagraj.pl" target="_blank">www.zagraj.pl</a> — portal z grami online, w tym: Scrabble, karty Scrabble i Belgijka. Do sprawdzania słów wykorzystywany jest Oficjalny Słownik Polskiego Scrabblisty</li>
</ul>

<h2>Kluby Scrabble w Polsce</h2>
<ul>
<?$sql_conn = pfs_connect ();
$result = mysql_query("SELECT * FROM $DB_TABLES[clubs] WHERE stronawww<>'' AND data<>'0000-00-00' ORDER BY nazwa");
while($row = mysql_fetch_array($result)){
	print "<li><a href='".$row['stronawww']."' target='_blank'>".$row['stronawww']."</a>— ".$row['nazwa']."</li>";
}
?>
</ul>

<h2>Inne polskie strony o Scrabble</h2>
<ul>
	<li><a href="http://usenet.gazeta.pl/usenet/0,1.html?group=pl.rec.gry.scrabble&amp;x=7&amp;y=9" target="_blank">pl.rec.gry.scrabble</a> — grupa dyskusyjna o Scrabble</li>
	<li><a href="http://www.scrabble.info.pl" target="_blank">www.scrabble.info.pl</a> — Świat Scrabble — strona miłośników scrabble autorstwa Sławka Zabawy</li>
	<li><a href="http://slowbal.ptr.org.pl/" target="_blank">slowbal.ptr.org.pl</a> — strona Piotra Grondziowskiego, autora programu do rozgrywek korespondencyjnych Słów Bal</li>
	<li><a href="http://www.astercity.net/~wecilibe/" target="_blank">www.astercity.net/~wecilibe</a> — Puchar Mistrzów, rozgrywki korespondencyjne</li>
	<li><a href="http://www.sliwa.zabrze.pl/scrabble.html" target="_blank">www.sliwa.zabrze.pl/scrabble.html</a> — strona Maćka Śliwy, organizatora turniejów w Zabrzu</li>
	<li><a href="http://scrabble.lodz.pl/scrabble-guide/" target="_blank">scrabble.lodz.pl/scrabble-guide/</a> — strona Radosława Jachny, zawierająca bazę słówek</li>
</ul>

<h2>Scrabble na świecie</h2>
<ul>
	<li><a href="http://www.mattelscrabble.com/" target="_blank">www.mattelscrabble.com</a> — oficjalna strona Scrabble firmy Mattel</li>
	<li><a href="http://www.scrabble.com/" target="_blank">www.scrabble.com</a> — oficjalna strona Scrabble firmy Hasbro (USA, Kanada)</li>
	<li><a href="http://www.scrabble-assoc.com" target="_blank">www.scrabble-assoc.com</a> — National Scrabble Association (USA, Kanada)</li>
	<li><a href="http://www.absp.org.uk/index.html" target="_blank">www.absp.org.uk</a> — Association of British Scrabble Players</li>
	<li><a href="http://scrabble.co.nz/" target="_blank">scrabble.co.nz</a> — New Zealand Association of Scrabble Players</li>
	<li><a href="http://www.ffsc.fr" target="_blank">www.ffsc.fr</a> — Fédération Française de Scrabble</li>
	<li><a href="http://www.fbsc.be" target="_blank">www.fbsc.be</a> — Fédération Belge de Scrabble</li>
	<li><a href="http://www.fqcsf.qc.ca/" target="_blank">www.fqcsf.qc.ca/</a> — Fédération Québécoise des clubs de Scrabble Francophone</li>
	<li><a href="http://www.toucanet.com/" target="_blank">www.toucanet.com</a> — Singapore Scrabble Association</li>
	<li><a href="http://www.nigerianscrabble.com/" target="_blank">www.nigerianscrabble.com</a> — Nigerian Scrabble Federation</li>
	<li><a href="http://scrabble.hrejsi.cz/" target="_blank">scrabble.hrejsi.cz</a> — Česká asociace Scrabble</li>
	<li><a href="http://home.teleport.com/~stevena/scrabble/faq.html" target="_blank">home.teleport.com/~stevena/scrabble/faq.html</a> — The Scrabble FAQ</li>
	<li><a href="http://www.scrabblelinks.com" target="_blank">www.scrabblelinks.com</a> — Tons of Scrabble Resources</li>
	<li><a href="http://www.champions.ortograf.com/" target="_blank">www.champions.ortograf.com</a> — Scrabble Champions</li>
</ul>

<h2 id="slowniki">Słowniki on-line</h2>
<ul>
	<li><a href="http://sjp.pwn.pl" target="_blank">Słownik Języka Polskiego PWN</a></li>
	<li><a href="http://swo.pwn.pl" target="_blank">Słownik Wyrazów Obcych PWN</a></li>
	<li><a href="http://so.pwn.pl" target="_blank">Słownik Ortograficzny PWN</a></li>
	<li><a href="http://www.slownik-online.pl/index.php" target="_blank">Słownik wyrazów obcych i zwrotów obcojęzycznych</a> Władysława Kopalińskiego</li>
	<li><a href="http://encyklopedia.pwn.pl" target="_blank">Encyklopedia PWN</a></li>
	<li><a href="http://scrabblemania.pl/" target="_blank">Scrabble Mania - słownik i anagramator on-line</a> (bazuje na sjp.pl)</li>
	<li><a href="http://wordblender.pl/" target="_blank">Wordblender - słownik i anagramator on-line</a> (bazuje na sjp.pl)</li>
	<li><a href="http://slowonadzis.pl/" target="_blank">Słowo na dziś - strona wspomagająca naukę słów</a></li>
</ul>

<?require_once "files/php/bottom.php"?>
</body>
</html>
