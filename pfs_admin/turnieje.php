<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');

if (isset ($_GET[delete])) {
    pfs_delete ($DB_TABLES[registrations], array (id => $_GET[delete]), true);
}

else if (isset ($_GET[confirm])) {
    pfs_update ($DB_TABLES[registrations], array ( potwierdzenie => 1 ), array (id => $_GET[confirm]), true);
}

if (isset ($_GET['edit'])) {
    $in = pfs_select_one (array (
        table   => $DB_TABLES[tours],
        where   => array (id => $_GET['edit'])
    ));

    $people = pfs_select (array (
        table   => $DB_TABLES[registrations],
        where   => array ( id_turnieju => $in->id )
    ));
}
else {
    $in->rank   = $TOUR_STATUS[rank];
    $in->status = 'main';
}

?>

<html>
<head>
    <title>Plan turnieju</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.form.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <script>
    $(document).ready(function(){
        $( "#grupa" ).autocomplete ({
            source: function ( request, response ) {
                $.ajax({
                    url:        "actions.php",
                    dataType:   "json",
                    type:       "POST",
                    data:       { action: 'tour_groups_list', term: request.term },
                    success:    function( data ) {
                        response ( $.map (data, function (item) { return item.group }));
                    }
                });
            },
            minLength: 2
        });

        $("#zwyciezca, #miejsce2, #miejsce3").autocomplete ({
            source: function ( request, response ) {
                $.ajax({
                    url:        "actions.php",
                    dataType:   "json",
                    type:       "POST",
                    data:       { action: 'players_list', term: request.term },
                    success:    function( data ) {
                        response ( data);
                    }
                });
            },
            minLength: 2
        });

        $( "#options, #status, .buttony" ).buttonset ();
        $( "#tabs" ).tabs ().show ();
        Loader.init ();
        Dialog.init ();

        $('#tabs-info form').ajaxForm ({
            dataType:   "json",
            type:       "POST",
            url:        "actions.php",
            beforeSubmit:  function () {
                Loader.show ();
            },
            success: function (json) {
                if (json.info) {
                    Loader.hide ();
                    Dialog.show (json.msg);
                }
            }
        });

        $('#tabs-story form').ajaxForm ({
            dataType:   "json",
            type:       "POST",
            url:        "actions.php",
            beforeSubmit:  function () {
                Loader.show ();
            },
            success: function (json) {
                if (json.info) {
                    Loader.hide ();
                    Dialog.show (json.msg);
                    $("#tabs-story form input[type='file']").attr({ value: '' });
                }
            },
        });
    });

    </script>
</head>

<body>
<div id='header'>
    <h1>Plan turnieju</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='turnieje_lista.php'>Turnieje</a></li>
        <? if ($in->id) print "<li><a target='_blank' href='http://www.pfs.org.pl/turniej.php?id=$in->id'>Strona turnieju</a></li>";?>
    </ul>
</div>

<div id="content">
    <div id="tabs" style="display:none;">

    <ul>
        <li><a href="#tabs-info">Informacje</a></li>
        <li><a href="#tabs-registered">Lista zapisanych</a></li>
        <li><a href="#tabs-story">Relacja</a></li>
    </ul>

<?
print "
<div id='tabs-info'>
    <form method='POST'>" .
    ($in->id ? "<input type='hidden' name='tour_id' value='$in->id'>" : "") . "
    <input type='hidden' name='action' value='tour_save'>
    <table class='formTable'>
        <tr>
            <th>Nazwa:</th>
            <td><input type='text' name='nazwa' maxlength='255' value='".$in->nazwa."' class='long'></td>
        </tr>
        <tr>
            <th>Miejscowość:</th>
            <td><input type='text' name='miasto' maxlength='255' class='medium' value='".$in->miasto."'></td>
        </tr>
        <tr>
            <th>Od - Do:</th>
            <td>
                <input class='datepicker' type='text' name='data_od' maxlength='10' size='12' value='".$in->data_od."'>
                <input class='datepicker' type='text' name='data_do' maxlength='10' size='12' value='".$in->data_do."'>
            </td>
        </tr>
        <tr>
            <th>Typ turnieju:</th>
            <td id='options' >
                <input type='radio' name='rank' id='rank0' value='$TOUR_STATUS[norank]' ". ($in->rank == $TOUR_STATUS[norank] ? " checked='checked'" : "") .">
                <label for='rank0'>Nierankingowy</label>
                <input type='radio' name='rank' id='rank1' value='$TOUR_STATUS[rank]' ". ($in->rank == $TOUR_STATUS[rank] ? " checked='checked'" : "") .">
                <label for='rank1'>Rankingowy</label>
                <input type='radio' name='rank' id='rank2' value='$TOUR_STATUS[gp]' ". ($in->rank == $TOUR_STATUS[gp] ? " checked='checked'" : "") .">
                <label for='rank2'>Grand Prix</label>
                <input type='radio' name='rank' id='rank3' value='$TOUR_STATUS[vacation]' ". ($in->rank == $TOUR_STATUS[vacation] ? " checked='checked'" : "") .">
                <label for='rank3'>Wczasy</label>
                <input type='radio' name='rank' id='rank4' value='$TOUR_STATUS[other]' ". ($in->rank == $TOUR_STATUS[other] ? " checked='checked'" : "") .">
                <label for='rank4'>Inne wydarzenie</label>
            </td>
        </tr>
        <tr>
            <th>Formularz PFS:</th>
            <td>
                <input type='checkbox' name='formularz' id='formularz' class='button' value='tak'".($in->formularz ? " checked" : "").">
                <label for='formularz'>Załącz</label>
            </td>
        </tr>
        <tr>
            <th>Grupa turnieju:</th>
            <td><input id='grupa' type='text' name='typ' class='medium' maxlength='50' value='$in->typ'></td>
        </tr>
        <tr>
            <th>Rodzaj turnieju:</th>
            <td><input type='text' name='rodzaj' maxlength='255' class='medium' value='$in->rodzaj'></td>
        </tr>
        <tr>
            <th>Ilość rund:</th>
            <td><input type='text' name='ilosc_rund' maxlength='3' size='12' value='$in->ilosc_rund'/></td>
        </tr>
        <tr>
            <th>Sposób rozgrywania:<br /><button onclick='wstaw(\"sposob\")' type='button' class='button'>11 + 1</button></th>
            <td><textarea name='sposob' maxlength='65530' class='long' rows='3'/>$in->sposob</textarea></td>
        </tr>
        <tr>
            <th>Miejsce rozgrywek:</th>
            <td><textarea name='miejsce' maxlength='65530' class='long' rows='4'/>$in->miejsce</textarea></td>
        </tr>
        <tr>
            <th>Obrońca tytułu:</th>
            <td><input type='text' name='obronca' maxlength='255' class='medium' value='$in->obronca'></td>
        </tr>
        <tr>
            <th>Sędzia:</th>
            <td><input type='text' name='sedzia' maxlength='255' class='medium' value='$in->sedzia'></td>
        </tr>
        <tr>
            <th>Strona www turnieju:</th>
            <td><input type='text' name='stronawww' maxlength='255' class='long' value='$in->stronawww'></td>
        </tr>
        <tr>
            <th>Harmonogram:<br /><button onclick='wstaw(\"harmonogram\")' type='button' class='button'>Szablon</button></th>
            <td><textarea name='harmonogram' maxlength='65530' class='long' rows='8'>$in->harmonogram</textarea></td>
        </tr>
        <tr>
            <th>Wpisowe:<br /><button onclick='wstaw(\"wpisowe\")' type='button' class='button'>Stawki PFS</button></th>
            <td><textarea name='wpisowe' maxlength='65530' class='long' rows='3'>$in->wpisowe</textarea></td>
        </tr>
        <tr>
            <th>Dojazd:</th>
            <td><textarea name='dojazd' maxlength='65530' class='long' rows='3'>$in->dojazd</textarea></td>
        </tr>
        <tr>
            <th>Wyżywienie:</th>
            <td><textarea name='wyzywienie' maxlength='65530' class='long' rows='3'>$in->wyzywienie</textarea></td>
        </tr>
        <tr>
            <th>Noclegi:</th>
            <td><textarea name='noclegi' maxlength='65530' class='long' rows='5'>$in->noclegi</textarea></td>
        </tr>
        <tr>
            <th>Nagrody:</th>
            <td><textarea name='nagrody' maxlength='65530' class='long' rows='3'>$in->nagrody</textarea></td>
        </tr>
        <tr>
            <th>Dodatkowe atrakcje:</th>
            <td><textarea name='atrakcje' maxlength='65530' class='long' rows='3'>$in->atrakcje</textarea></td>
        </tr>
        <tr>
            <th>Zgłoszenia uczestników:</th>
            <td><textarea name='zgloszenia' maxlength='65530' class='long' rows='3'>$in->zgloszenia</textarea></td>
        </tr>
        <tr>
            <th>Organizatorzy:</th>
            <td><textarea name='organizatorzy' maxlength='65530' class='long' rows='4'>$in->organizatorzy</textarea></td>
        </tr>
        <tr>
            <th>Sponsorzy:</th>
            <td><textarea name='sponsorzy' maxlength='65530' class='long' rows='4'>$in->sponsorzy</textarea></td>
        </tr>
        <tr>
            <th>Dodatkowe informacje<br>na górze strony:</th>
            <td><textarea name='dodatkowe_przed' maxlength='65530' class='long' rows='6'>$in->dodatkowe_przed</textarea></td>
        </tr>
        <tr>
            <th>Dodatkowe informacje<br>na dole strony:</th>
            <td><textarea name='dodatkowe' maxlength='65530' class='long' rows='6'/>$in->dodatkowe</textarea></td>
        </tr>
        <tr>
            <th>Lista uczestników:<br /><button onclick='wstaw(\"uczestnicy\")' type='button' class='button'>Dodaj</button></th>
            <td><textarea name='uczestnicy' maxlength='65530' class='long' rows='12'/>$in->uczestnicy</textarea></td>
        </tr>
    </table>

    <div style='width:780px;margin:0 auto;text-align:center;margin-top:7px;'>
        <input type='submit' value='Aktualizuj turniej' class='button'>
    </div>
    <div class='progressbar'></div>

    </form>
    </div>

    <div id='tabs-registered'>
        <table>
            <tr>
                <th></th>
                <th>Osoba</th>
                <th>Miejscowość</th>
                <th>E-mail</th>
                <th>Wypisanie</th>
                <th>Potwierdzenie udziału</th>
            </tr>";

    foreach ($people as $person) {
        print "<tr>
            <td class='lp'>" . ( ++$lp ) . "</td>
            <td class='person'>$person->osoba</td>
            <td>$person->miasto</td>
            <td>$person->email</td>
            <td class='icons'><a class='delete' href='turnieje.php?edit=$in->id&delete=$person->id#tabs-registered' title='Wypisz' onclick='return confirmDelete (\"$person->osoba\");'></a></td>".
            ($person->potwierdzenie == 1 ? "<td></td>" : "<td class='icons'><a href='turnieje.php?edit=$in->id&confirm=$person->id#tabs-registered' class='edit' title='Zatwierdź udział'></a></td>")."
        </tr>";
    }

    print "
        </table>
    </div>


    <div id='tabs-story'>
    <form enctype='multipart/form-data' method='POST'>" .
    ($in->id ? "<input type='hidden' name='tour_id' value='$in->id'>" : "") . "
    <input type='hidden' name='MAX_FILE_SIZE' value='1200000'>
    <input type='hidden' name='action' value='story_save'>
    <table class='formTable'>
        <tr>
            <th>Turniej:</th>
            <td>$in->nazwa</td>
        </tr>
        <tr>
            <th>Tytuł relacji:</th>
            <td><input type='text' name='tytul' maxlength='255' value='$in->tytul'></td>
        </tr>
        <tr>
            <th>Status:</th>
            <td id='status'>
                <input type='radio' name='status' id='status1' class='radio' value='main'".    ($in->status == 'main'    ? " checked" : "").">
                <label for='status1'>Główna relacja na stonie głównej</label>
                <input type='radio' name='status' id='status2' class='radio' value='short'".   ($in->status == 'short'   ? " checked" : "").">
                <label for='status2'>Zakładka na stronie głównej</label>
                <input type='radio' name='status' id='status3' class='radio' value='archive'". ($in->status == 'archive' ? " checked" : "").">
                <label for='status3'>Archiwum<br></label>
            </td>
        </tr>
        <tr>
            <th>Zdjęcie:</th>
            <td class='buttony'>
                <input name='foto' type='file' style='width:220px;'>
                <input type='checkbox' name='remove_foto' value='true' id='remove_foto' />
                <label for='remove_foto'>Usuń</label>
            </td>

        </tr>
        <tr>
            <th>Klasyfikacja:</th>
            <td class='buttony'>
                <input name='klasyfikacja' type='file' style='width:220px;'>
                <input type='checkbox' name='remove_klasyfikacja' value='true' id='remove_klasyfikacja' />
                <label for='remove_klasyfikacja'>Usuń</label>
            </td>
        </tr>
        <tr>
            <th>HH:</th>
            <td class='buttony'>
                <input name='hh' type='file' style='width:220px;'>
                <input type='checkbox' name='remove_hh' value='true' id='remove_hh' />
                <label for='remove_hh'>Usuń</label>
            </td>
        </tr>
        <tr>
            <th>Rundy:</th>
            <td class='buttony'>
                <input name='rundy' type='file' style='width:220px;'>
                <input type='checkbox' name='remove_rundy' value='true' id='remove_rundy' />
                <label for='remove_rundy'>Usuń</label>
            </td>
        </tr>
        <tr>
            <th>Ranking:</th>
            <td class='buttony'>
                <input name='ranking' type='file' style='width:220px;'>
                <input type='checkbox' name='remove_ranking' value='true' id='remove_ranking' />
                <label for='remove_ranking'>Usuń</label>
            </td>
        </tr>
        <tr>
            <th>Frekwencja:</th>
            <td><input type='text' name='frekwencja' maxlength='255' class='medium' value='$in->frekwencja'></td>
        </tr>
        <tr>
            <th>Zwycięzca turnieju:</th>
            <td><input type='text' name='zwyciezca' id='zwyciezca' maxlength='255' class='medium' value='$in->zwyciezca'></td>
        </tr>
        <tr>
            <th>Drugie miejsce:</th>
            <td><input type='text' name='miejsce2' id='miejsce2' maxlength='255' class='medium' value='$in->miejsce2'></td>
        </tr>
        <tr>
            <th>Trzecie miejsce:</th>
            <td><input type='text' name='miejsce3' id='miejsce3' maxlength='255' class='medium' value='$in->miejsce3'></td>
        </tr>
        <tr>
            <th>Treść relacji:</th>
            <td><textarea name='tresc' maxlength='65530' rows='16'/>$in->tresc</textarea>
        </tr>".
        //<tr>
        //    <th>Skrót relacji:</th>
        //    <td><textarea name='skrot' maxlength='65530' rows='4'>".$in->skrot."</textarea>
        //</tr>
        "
        <tr>
            <th>Autor relacji:</th>
            <td><input type='text' name='autor' maxlength='255' class='medium' value='$in->autor'/></td>
        </tr>
        <tr>
            <th>URL galerii 1:</th>
            <td><input type='text' name='galeria1' maxlength='2555' value='$in->galeria1'/></td>
        </tr>
        <tr>
            <th>URL galerii 2:</th>
            <td><input type='text' name='galeria2' maxlength='2555' value='$in->galeria2'/></td>
        </tr>
        <tr>
            <th>URL galerii 3:</th>
            <td><input type='text' name='galeria3' maxlength='2555' value='$in->galeria3'/></td>
        </tr>
        <tr>
            <th>URL galerii 4:</th>
            <td><input type='text' name='galeria4' maxlength='2555'value='$in->galeria4'/></td>
        </tr>
        <tr>
            <th>URL galerii 5:</th>
            <td><input type='text' name='galeria5' maxlength='2555' value='$in->galeria5'/></td>
        </tr>
    </table>

    <div style='width:780px;margin:0 auto;text-align:center;margin-top:7px;'>
        <input type='submit' value='Aktualizuj relację' class='button'>
    </div>

    </form>
</div>";
?>
    </div>
</div>
</body>
</html>
