<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();
$rok      = ($_GET['rok'] ? $_GET['rok'] : date ('Y'));

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[medals], array ('id' => $_GET['delete']));
}

else if ($_POST['change_a']) {
    $data = array (
        'data'   => date ('Y.m.d G:i'),
        'osoba'  => $_POST['osoba'],
        'uwagi'  => $_POST['uwagi']
    );
    pfs_insert ($DB_TABLES[medals_update], $data);
}

else if ($_POST['change']) {
     $data = array (
        'osoba'     => $_POST['osoba'],
        'zlote'     => $_POST['zlote'],
        'srebrne'   => $_POST['srebrne'],
        'brazowe'   => $_POST['brazowe'],
        'rok'       => $rok
    );

    $_POST['change'] == -1
        ? pfs_insert ($DB_TABLES[medals], $data)
        : pfs_update ($DB_TABLES[medals], $data, array ('id' => $_POST['change']));
}
?>

<html>
<head>
    <title>Medale <? print $rok; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <?php
        $rows = pfs_select (array (
            distinct    => 1,
            fields      => array ( 'osoba' ),
            table       => $DB_TABLES[medals],
            order       => array ( 'osoba' )
        ));

        $tabela = "<script> var ludzie = [";

        foreach ($rows as $row) {
            $tabela .= "'" . $row->osoba . "', ";
        }

        $tabela .= "'']; </script>";

        print $tabela;
    ?>
    <script>
        function zmianaRoku (sel){
            opcje = sel.options;
            for (i = 0; i < opcje.length; i++) {
                if (opcje[i].selected) {
                    rok = opcje[i].value;
                }
            }
            document.location = "medale.php?rok=" + rok;
        }

        $(function() {
            $("#osoba").autocomplete ( { source: ludzie } );
        });
    </script>
</head>

<body>
<div id='header'>
    <h1>Klasyfikacja medalowa <? print $rok; ?></h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='medale.php?nowa=1&rok=<? print $rok; ?>'>Dodaj nową osobę</a></li>
        <li><select onchange='zmianaRoku (this);'>
<?
for ($y = date('Y'); $y >= 1993; $y--) {
    print '<option value="' . $y . '"' . ($rok == $y ? ' selected="selected">' : '>') . $y . '</option>';
}
?>
        </select></li>
        <li>
<?
$row = pfs_select_one (array (
    table   => $DB_TABLES[medals_update],
    order   => array ( '!id' )
));
print "<form action='medale.php?rok=$rok' style='display:inline;' method='post' >";
    if(isset($_GET['edit_a'])){
        print "<input type='hidden' name='change_a' value='1'>";
        print "kto: <input type='text' name='osoba' size='10' value='".$row->osoba."'>&nbsp;";
        print "uwagi: <input type='text' name='uwagi' size='20' value='".$row->uwagi."'>";
        print "&nbsp;<input type='submit' value='Zapisz' class='button'>";
    }
    else{
        $data = date('d.m.y (G:i)', strtotime($row->data));
        print "<a href='medale.php?edit_a=1&rok=".$rok."' title='zmień'>ostatnia aktualizacja: <b>".$data."</b>";
        if ($row->uwagi != '')
            print " - ".$row->uwagi;
        print  " (".$row->osoba.")</a>";
    }
    print "</form>";

?>
        </li>
    </ul>
</div>

<div id="content">
    <table style='width:500px'>
        <tr>
            <th class='lp'></th>
            <th class='person'>Imię i nazwisko</th>
            <th class='center'>1</th>
            <th class='center'>2</th>
            <th class='center'>3</th>
            <th class='icons'></th>
        </tr>
<?
if ($_GET['nowa']){
?>
    <form action='medale.php?rok=<? print $rok; ?>' method='post'>
        <input type='hidden' name='change' value='-1'>
        <tr>
            <td class='lp'></td>
            <td class='person'><input type='text' name='osoba' id='osoba' size='16'></td>
            <td><input type='text' name='zlote' size='1'></td>
            <td><input type='text' name='srebrne' size='1'></td>
            <td><input type='text' name='brazowe' size='1'></td>
            <td><input type='submit' value='Zapisz' class='button' /></td>
        </tr>
    </form>
<?
}

$i      = 1;
$pop1   = 0;
$pop2   = 0;
$pop3   = 0;
$medals = pfs_select (array (
    table   => $DB_TABLES[medals],
    where   => array ( rok => $rok ),
    order   => array ( '!zlote', '!srebrne', '!brazowe' )
));
foreach ($medals as $row) {
    $lp = ($pop1 == $row->zlote && $pop2 == $row->srebrne && $pop3 == $row->brazowe ? '&nbsp;' : $i);
    $i++;
    $pop1 = $row->zlote;
    $pop2 = $row->srebrne;
    $pop3 = $row->brazowe;

    if ((isset ($_GET['edit'])) && ($_GET['edit'] == $row->id)) {
        print "
            <form action='medale.php?rok=".$rok."' method='post'>
                <input type='hidden' name='change' value='".$row->id."'>
                <tr>
                    <td class='lp'>".$lp."</td>
                    <td class='person'><input type='text' name='osoba' id='osoba' size='16' value='".$row->osoba."'></td>
                    <td><input type='text' name='zlote' size='1' value='".$row->zlote."'></td>
                    <td><input type='text' name='srebrne' size='1' value='".$row->srebrne."'></td>
                    <td><input type='text' name='brazowe' size='1' value='".$row->brazowe."'></td>
                    <td><input type='submit' value='Zapisz' class='button'>
                </tr>
            </form>";
    }
    else{
        print '
            <tr>
                <td class="lp">'.$lp.'</td>
                <td class="person">'.$row->osoba.'</td>
                <td class="center">'.$row->zlote.'</td>
                <td class="center">'.$row->srebrne.'</td>
                <td class="center">'.$row->brazowe.'</td>
                <td class="icons">
                    <a href="medale.php?delete='.$row->id.'&rok='.$rok.'" title="usuń" class="delete" onclick="return confirmDelete (\'' . $row->osoba . '\');"></a>
                    <a href="medale.php?edit='.$row->id.'&rok='.$rok.'" title="edytuj" class="edit"></a>
                </td>
            </tr>';
    }
}
?>
    </table>
</div>
</body>
</html>
