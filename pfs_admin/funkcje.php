<?php

define ('DIR_JS',  DIR_MAIN . 'files/js/');
define ('DIR_CSS', DIR_MAIN . 'files/css/');
define ('DIR_PHP', DIR_MAIN . 'files/php/');
define ('DIR_IMG', DIR_MAIN . 'files/img/');
setlocale(LC_TIME, 'pl_PL.utf-8');
// setlocale(LC_ALL, 'pl-PL.utf8', 'pl_PL.UTF8', 'pl_PL.utf8', 'pl_PL.UTF-8', 'pl_PL.utf-8', 'polish_POLISH.UTF8', 'polish_POLISH.utf8', 'pl.UTF8', 'polish.UTF8', 'polish-pl.UTF8', 'PL.UTF8', 'polish.utf8', 'polish-pl.utf8', 'PL.utf8');

$DB_CONNS = array (
    www     => array ( dbname => 'ustczanka',  user => 'ustczanka',  password => 'vera1234'),
    osps    => array ( dbname => 'ustczanka1', user => 'ustczanka1', password => 'vera1234'),
);

$SQL = pfs_connect ();

$TOUR_STATUS = array (
    norank      => 0,
    rank        => 1,
    gp          => 2,
    vacation    => 3,
    other       => 4,
);

$DB_TABLES = array (
    tours           => 'tournaments',
    sevens          => 'sevens',
    year_rank       => 'year_rank',
    gp2013          => 'gp2013',
    gp2012          => 'gp2012',
    gp2011          => 'gp2011',
    gp2010          => 'gp2010',
    gp2009          => 'gp2009',
    gp2008          => 'gp2008',
    medals_update   => 'medals_update',
    medals          => 'medals',
    resolutions     => 'pfs_resolutions',
    clubs           => 'scrabble_clubs',
    players         => 'scrabble_players',
    news            => 'news',
    games           => 'boards',
    moves           => 'boards_moves',
    live            => 'live',
    registrations   => 'tour_registrations',
    groups          => 'tour_groups',
    photos          => 'photos',
);

$BOARD_BONUSES = array (
    word3       => array (0,  7,  14, 105, 119, 210, 217, 224),
    word2       => array (16, 28, 32, 42,  48,  56,  64,  70,  112, 154, 160, 168, 176, 182, 192, 196, 208),
    letter3     => array (20, 24, 76, 80,  84,  88,  136, 140, 144, 148, 200, 204),
    letter2     => array (3,  11, 36, 38,  45,  52,  59,  92,  96,  98,  102, 108, 116, 122, 126, 128, 132, 165, 172, 179, 186, 188, 213, 221),
);

//  @function pfs_connect
//  @input
//      * 0: db - (STRING) klucz z $DB_CONNS

function pfs_connect ($db = 'www') {
    if ($SQL) {
        return $SQL;
    }

    global $DB_CONNS;
    $conn = mysql_connect ('ustczanka.home.pl:3306', $DB_CONNS[$db][user], $DB_CONNS[$db][password]) or die ('Nie można się połączyć: ' . mysql_error());

    mysql_select_db ($DB_CONNS[$db][dbname]);
    // mysql_query ("SET NAMES '" . $conn_charset . "'");

    return $conn;
}

//  @function pfs_refresh
//  @input
//      * 0: argv - (ARRAY) tablica wartosci, z ktorych budowany jest query string doklejany do odswiezanego urla

function pfs_refresh ($argv = null) {
    if ($argv) {
        $query_string = '?';
        foreach ($argv as $key => $value) {
            $query_string .= "$key=$value&";
        }
        $query_string  = rtrim ($query_string, '&');
    }

    header ('Location: http://' . $_SERVER["HTTP_HOST"] . $_SERVER["SCRIPT_NAME"] . $query_string);
}

//  @function _pfs_select__field_format
//  @input
//      * 0: column  - (STRING)

function _pfs_select__field_format ($column) {
    return (preg_match ('/^(YEAR|MONTH|COUNT|SUM|MAX)/', $column, $matches) ? "$column AS '". strtolower ($matches[1]) ."'" : "`$column`");
}

//  @function pfs_select
//  @input
//      * 0: data
//          ** table    - (STRING)
//          ** fields   - (ARRAY) YEAR, COUNT sa nie-backtick'owane
//          ** distinct - (BOOL)
//          ** where    - (HASH) ! negacja wartosci, % like, < > <= >= porownania, YEAR, COUNT
//          ** order    - (ARRAY) ! desc
//          ** limit    - (INT)
//          ** groupby  - (ARRAY)

function pfs_select ($data) {
    if ($data[fields]) {
        $fields =  join (',', array_map (_pfs_select__field_format, $data[fields]));
    }
    else {
        $fields = '*';
    }

    $query = 'SELECT '. ($data[distinct] ? ' DISTINCT ' : '') ."$fields FROM `$data[table]`";

    if ($data[where]) {
        $query .= " WHERE ";
        foreach ($data[where] as $column => $value) {
            $column = preg_replace ('/^!/', '', $column, 1, $neg);
            $op     = ($neg ? '!=' : '=');

            $column = preg_replace ('/^%/', '', $column, 1, $like);
            $op     = ($like ? ($neg ? 'NOT LIKE' : 'LIKE') : $op);

            if (!$neg && !$like && preg_match ('/^(<=|>=|<|>)/', $column, $matches)) {
                $column = preg_replace ('/^(<=|>=|<|>)/', '', $column, 1, $cmp);
                $op     = $matches[1];
            }

            if (!preg_match ('/^(YEAR|MONTH|DAY|COUNT|DATE|SUM|MAX|NOW|ADDDATE)/', $column)) {
                $column = "`$column`";
            }

            $value = safe_string ($value);
            if (!preg_match ('/^(YEAR|MONTH|DAY|DATE|COUNT|SUM|MAX|NOW|ADDDATE)/', $value)) {
                $value = "'$value'";
            }

            $query .= "$column $op $value AND ";
        }
        $query = substr ($query, 0, -5);
    }

    if ($data[groupby]) {
        $query .= " GROUP BY ";
        foreach ($data[groupby] as $column) {
            $query .= "`$column`,";
        }
        $query  = rtrim ($query, ',');
    }

    if ($data[order]) {
        $query .= " ORDER BY ";
        foreach ($data[order] as $column) {
            $column = preg_replace ('/^!/', '', $column, 1, $desc);
            $dir    = ($desc ? 'DESC' : 'ASC');
            $query .= "`$column` $dir,";
        }
        $query  = rtrim ($query, ',');
    }

    if ($data[limit]) {
        $query .= " LIMIT $data[limit]";
    }
    $i      = 0;

    $result = mysql_query ($query . ';') or die (mysql_error ($query));

    $ret = array ();
    while ($row = mysql_fetch_object ($result)) {
        $ret[$i++] = $row;
    }

    mysql_free_result ($result);
    return $ret;
}

//  @function pfs_select_one
//  @input
//      * 0: data
//          ** table    - (STRING)
//          ** fields   - (ARRAY) YEAR, COUNT sa nie-backtick'owane
//          ** where    - (HASH) ! negacja wartosci, % like, < > <= >= porownania, YEAR, COUNT
//          ** order    - (ARRAY) ! desc
//          ** limit    - (INT)
//          ** groupby  - (ARRAY)

function pfs_select_one ($data) {
    $one = pfs_select (array (
        table   => $data[table],
        fields  => $data[fields],
        where   => $data[where],
        order   => $data[order],
        groupby => $data[groupby],
        limit   => 1
    ));

    return ($one ? $one[0] : null);
}

//  @function pfs_update
//  @input
//      * 0: table      - (STRING)
//      * 1: data       - (ARRAY) kolumna => wartosc
//      * 2: where      - (ARRAY) kolumna => wartosc
//      * 3: norefresh  - (BOOL)

function pfs_update ($table, $data, $where, $norefresh = false) {
    if (!$where || !$data) return;
    $query = "UPDATE `$table` SET ";

    foreach ($data as $column => $value) {
        $query .= "`$column` = '". safe_string ($value) ."',";
    }
    $query  = rtrim ($query, ',') . ' WHERE ';

    foreach ($where as $column => $value) {
        $query .= "`$column` = '". safe_string ($value) ."' AND ";
    }
    $query = substr ($query, 0, -5) . ';';

    mysql_query ($query) or die (mysql_error($query));
    if (!$norefresh) pfs_refresh ();
}

//  @function pfs_replace
//  @input
//      * 0: table      - (STRING)
//      * 1: data       - (ARRAY) kolumna => wartosc
//      * 2: norefresh  - (BOOL)

function pfs_replace ($table, $data, $norefresh = false) {
    $query = "REPLACE INTO `$table` SET ";

    foreach ($data as $column => $value) {
        $query .= "`$column` = '". safe_string ($value) ."',";
    }
    $query  = rtrim ($query, ',') . ';';

    mysql_query ($query) or die (mysql_error($query));
    if (!$norefresh) pfs_refresh ();
}

//  @function pfs_insert
//  @input
//      * 0: table      - (STRING)
//      * 1: data       - (ARRAY) kolumna => wartosc
//      * 2: norefresh  - (BOOL)

function pfs_insert ($table, $data, $norefresh = false) {
    $query = "INSERT INTO `$table` SET ";

    foreach ($data as $column => $value) {
        $query .= "`$column` = '". safe_string ($value) ."',";
    }
    $query  = rtrim ($query, ',') . ';';

    mysql_query ($query) or die (mysql_error());
    if (!$norefresh) pfs_refresh ();

    return mysql_insert_id ();
}

//  @function pfs_delete
//  @input
//      * 0: table      - (STRING)
//      * 1: where      - (ARRAY) kolumna => wartosc
//      * 2: norefresh  - (BOOL)

function pfs_delete ($table, $where, $norefresh = false) {
    if (!$where) return;

    $query = "DELETE FROM `$table` WHERE ";

    foreach ($where as $column => $value) {
        $query .= "`$column` = '". safe_string ($value) ."' AND ";
    }
    $query = substr ($query, 0, -5) . ';';

    mysql_query ($query) or die (mysql_error());
    if (!$norefresh) pfs_refresh ();
}


function safe_string ($str) {
    $str        = (string) $str;
    $len        = strlen ($str);
    $escape_cnt = 0;
    $ret        = '';

    for ($offset = 0; $offset < $len; $offset++) {
        switch ($c = $str{$offset}) {
            case "'":
                // Escapes this quote only if its not preceded by an unescaped backslash
                if ($escape_cnt % 2 == 0) $ret .= "\\";
                $escape_cnt  = 0;
                $ret        .= $c;
                break;
            case '"':
                // Escapes this quote only if its not preceded by an unescaped backslash
                if ($escape_cnt % 2 == 0) $ret .= "\\";
                $escape_cnt  = 0;
                $ret        .= $c;
                break;
            case '\\':
                $escape_cnt++;
                $ret .= $c;
                break;
            default:
                $escape_cnt  = 0;
                $ret        .= $c;
        }
    }
    return $ret;
}

function pfs_photo_update ($photo, $table, $id) {
    global $DB_TABLES;
    if ($photo['size'] <= 0) {
        return;
    }

    $tmp = $photo['tmp_name'];
    list ($data['photo_width'], $data['photo_height'], $data['photo_type']) = getimagesize ($tmp);

    $data['photo_type'] = image_type_to_mime_type ($data['photo_type']);

    $fh                     = fopen ($tmp, 'r');
    $data['photo_content']  = addslashes (fread ($fh, filesize ($tmp)));
    fclose ($fh);

    $p = pfs_select_one (array (
        table   => $DB_TABLES[photos],
        fields  => (array ('id')),
        where   => array (
            'table'     => $table,
            'table_id'  => $id
        )
    ));

    if (!$p) {
        $data['table']      = $table;
        $data['table_id']   = $id;

        pfs_insert ($DB_TABLES[photos], $data, true);
        return;
    }

    pfs_update ($DB_TABLES[photos], $data, array ( id => $p->id), true);
    return;
}

function pfs_photo_remove ($table, $id) {
    global $DB_TABLES;

    pfs_delete ($DB_TABLES[photos], array ( 'table' => $table, 'table_id' => $id ));
    return;
}

function pfs_photo_read ($table, $id) {
    global $DB_TABLES;
    $photo = pfs_select_one (array (
        table   => $DB_TABLES[photos],
        where   => array (
            'table'     => $table,
            'table_id'  => $id
        )
    ));

    return $photo;
}


















function menuTurnieju ($relacja, $main_page=false) {
    global $DB_TABLES;

    if ($relacja->data_od && $relacja->data_do)
        $data = wyswietlDate ($relacja->data_od, $relacja->data_do, true) . " — ";

    print "<div class='naglowekrelacji'><span class='tytul'>$relacja->tytul<span class='nazwa'>$data $relacja->nazwa</span></span>";

    if ($main_page) {
        if ($relacja->klasyfikacja)
            print "<a href='turniej.php?id=$relacja->id#klasyfikacja' id='klasyfikacja' class='przycisk'>Wyniki</a>";
    }
    else {
        $rok = substr ($relacja->data_do, 0, 4);
        print "<a href='#' id='info' class='przycisk'>Informacje</a>";
        if ($relacja->tresc || pfs_photo_read ('tours', $relacja->id)) print "<a href='#' id='relacja' class='przycisk'>Relacja</a>";

        if ($relacja->klasyfikacja) print "<a href='#' id='klasyfikacja' class='przycisk'>Wyniki</a>";
        if ($relacja->hh)           print "<a href='#' id='hh' class='przycisk'>HH</a>";
        if ($relacja->rundy)        print "<a href='#' id='rundy' class='przycisk'>Rundy</a>";
        if ($relacja->ranking)      print "<a href='#' id='ranking' class='przycisk'>Ranking</a>";
        if (file_exists("turnieje/files/stat".$idt.".html")) print "<a onclick=window.open('turnieje/files/stat$idt.html') class='przycisk'>Statystyki</a>";
        if (file_exists("turnieje/files/ddt". $idt.".html")) print "<a onclick=window.open('turnieje/files/ddt$idt.html')  class='przycisk'>DDT</a>";

        $result = mysql_query ("SELECT 1 FROM $DB_TABLES[live] WHERE `id_turnieju` = $relacja->id");
        if (mysql_num_rows ($result)) {
            print "<a href='live/index.php?id=$relacja->id' class='przycisk'>Przebieg</a>";
        }
    }

    print "<div class='galleries'>";
    if($relacja->galeria1) print "<a href='$relacja->galeria1'></a> ";
    if($relacja->galeria2) print "<a href='$relacja->galeria2'></a> ";
    if($relacja->galeria3) print "<a href='$relacja->galeria3'></a> ";
    if($relacja->galeria4) print "<a href='$relacja->galeria4'></a> ";
    if($relacja->galeria5) print "<a href='$relacja->galeria5'></a> ";
    print "<img src='files/img/gallery.png' style='visibility:hidden;' alt='galeria' />";
    print "</div>";

    print "</div>";
}

function pokazRelacje ($relacja, $reklamy=false) {
    global $DB_TABLES;

    print "<div class='relacja' style='clear:both;'>";
    $photo = pfs_photo_read ('tours', $relacja->id);

    if($photo)
        print "<img class='onleft' src='http://pfs.org.pl/foto_action.php?id=$photo->id' alt='$relacja->tytul' />";

    print "$relacja->tresc$relacja->skrot</div>";
    if($relacja->autor) print "<div class='autor'>relacja: $relacja->autor</div>";

    if ($relacja->id) {
        $boards = pfs_select ( array (
            table   => $DB_TABLES[games],
            where   => array ( id_turnieju => $relacja->id ),
            order   => array ('id')
        ));

        foreach ($boards as $board) {
            print "<a href='http://www.pfs.org.pl/zapis.php?id=$board->id'>$board->nazwa - <b>$board->gospodarz : $board->gosc</b></a><br />";
        }
    }

    print "<div style='clear:both;'></div>";
}

function wyswietlDate ($d1, $d2, $show_year) {
    $od         = explode ("-", $d1);
    $do         = explode ("-", $d2);
    $data_od    = mktime (0, 0, 0, $od[1], $od[2], $od[0]);
    $data_do    = mktime (0, 0, 0, $do[1], $do[2], $do[0]);

    $ret        = strftime (($show_year ? '%d' : '%e'), $data_od);

    if ($data_od != $data_do) {
        $ret .= " - " . trim (strftime (($show_year ? '%d %B %Y' : '%e %B'), $data_do));
    }
    else {
        $ret .= strftime (($show_year ? ' %B %Y' : ' %B'), $data_od);
    }

    $mianownik  = array ("styczeń", "luty",  "marzec","kwiecień","maj", "czerwiec","lipiec","sierpień","wrzesień","październik", "listopad", "grudzień");
    $dopelniacz = array ("stycznia","lutego","marca", "kwietnia","maja","czerwca", "lipca", "sierpnia","września","października","listopada","grudnia");

    return str_replace ($mianownik, $dopelniacz, $ret);
}

function wyciagnijCzolowke ($rows) {
    global $DB_TABLES, $TOUR_STATUS;
    $czolowka = "<table>";

    $tour = pfs_select (array (
        table   => $DB_TABLES[tours],
        fields  => array ( 'ranking' ),
        where   => array ( '<rank' => $TOUR_STATUS[vacation], '!rank' => 0, '<=data_do' => date ('Y-m-d'), '!ranking' => '' ),
        order   => array ( '!data_do' ),
        limit   => 1
    ));

    $tour = $tour[0];

    $i = 1;
    foreach (preg_split ("/(\r?\n)/", $tour->ranking) as $line) {
        if ($data = sscanf($line, "%[0-9 ]%[a-zA-ZęĘóÓąĄśŚłŁżŻźŹćĆńŃ./- ]%[0-9.]")) {
            list ($position, $person, $ranking) = $data;

            if (trim ($position) == $i) {
                list ($fname, $sname)   = explode (" ", $person);
                $ranking                = trim ($ranking);
                $czolowka .= "<tr><td>$i.&nbsp;$fname&nbsp;$sname</td><td>&nbsp;&nbsp;$ranking</td></tr>";
                if ($i++ >= $rows)
                    break;
            }
            $data = null;
        }
    }

    $czolowka .= "</table>";
    return $czolowka;
}

function sierotki ($str) {
    $sierotki = array (" - ", " ,","'"," z "," i "," a "," w ","(w "," we "," o "," u "," że "," ale "," na "," po "," ze "," za "," od "," do "," co "," Z "," I "," A "," W "," We "," O "," U "," Że "," Ale "," Na "," Po "," Ze "," Za "," Od "," Do "," Co ");
    $popraw   = array (" — ", ",","&apos;"," z&nbsp;"," i&nbsp;"," a&nbsp;"," w&nbsp;","(w&nbsp;"," we&nbsp;"," o&nbsp;"," u&nbsp;"," że&nbsp;"," ale&nbsp;"," na&nbsp;"," po&nbsp;"," ze&nbsp;"," za&nbsp;"," od&nbsp;"," do&nbsp;"," co&nbsp;"," Z&nbsp;"," I&nbsp;"," A&nbsp;"," W&nbsp;"," We&nbsp;"," O&nbsp;"," U&nbsp;"," Że&nbsp;"," Ale&nbsp;"," Na&nbsp;"," Po&nbsp;"," Ze&nbsp;"," Za&nbsp;"," Od&nbsp;"," Do&nbsp;"," Co&nbsp;");

    return str_replace ($sierotki, $popraw, $str);
}

function plikNaSerwer ($file, $nazwa, $sciezka) {
    if(file_exists($file['tmp_name'])){
        $nazwa          = strtolower($nazwa);
        $plik_tmp       = $file['tmp_name'];
        $plik_nazwa     = $file['name'];
        $plik_rozmiar   = $file['size'];
        $dest           = $sciezka.$nazwa;
        if(is_uploaded_file($plik_tmp)){
            $aa = move_uploaded_file($plik_tmp, $dest);
            chmod($dest, 0644);
        }
        else
            echo "Nie udało się wysłać pliku <strong>$plik_nazwa</strong> na serwer";
    }
}

function mail_utf8 ($from, $to, $subject, $message) {
    $subject    = "=?UTF-8?B?". base64_encode ($subject) ."?=";
    $headers    = "From: $from\n"
        . "Content-Type: text/html; "
        . "charset=UTF-8; format=flowed\n"
        . "MIME-Version: 1.0\n"
        . "Content-Transfer-Encoding: 8bit\n";

    return mail ($to, $subject, $message, $headers);
}

function no_pl_chars ($text) {
    $pl   = array ("ą","ż","ś","ź","ć","ń","ł","ó","ę","Ą","Ż","Ś","Ź","Ć","Ń","Ł","Ó","Ę"," ");
    $nopl = array ("a","z","s","z","c","n","l","o","e","A","Z","S","Z","C","N","L","O","E","_");

    return str_replace ($pl, $nopl, $text);
}

?>
