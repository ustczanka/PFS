<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();
$rok      = ($_GET['rok'] ? $_GET['rok'] : date ('Y'));

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[resolutions], array ('id' => $_GET['delete']));
}

else if ($_POST['change']) {
    $data = array (
        'data'          => $_POST['data'],
        'numer'         => $_POST['numer'],
        'sprawa'        => $_POST['sprawa'],
        'tresc'         => $_POST['tresc'],
        'nieaktualna'   => ($_POST['nieaktualna'] == 'tak' ? 1 : 0)
    );

    $_POST['change'] == -1
        ? pfs_insert ($DB_TABLES[resolutions], $data)
        : pfs_update ($DB_TABLES[resolutions], $data, array ('id' => $_POST['change']));
}
?>

<html>
<head>
    <title>Uchwały PFS z roku <? print $rok; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <script>
        function zmianaRoku(sel)
        {
            opcje = sel.options;
            for(i=0; i<opcje.length; i++)
                if(opcje[i].selected)
                    rok = opcje[i].value;
            document.location="uchwaly.php?rok="+rok;
        }
    </script>
</head>

<body>
<div id='header'>
    <h1>Uchwały PFS z roku <? print $rok; ?></h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='uchwaly.php?nowa=1&rok=<? print $rok; ?>'>Nowa uchwała</a></li>
        <li><select onchange='zmianaRoku (this);'>
<?
for ($y = date('Y'); $y >= 2004; $y--) {
    print '<option value="' . $y . '"' . ($rok == $y ? ' selected="selected">' : '>') . $y . '</option>';
}
?>
        </select></li>
    </ul>
</div>

<div id="content">
    <table>
        <tr>
            <th class='lp'>Nr</th>
            <th class='data'>Data</th>
            <th>Sprawa</td>
            <th colspan='2'></th>
        </tr>
<?
if (isset ($_GET['nowa'])){
?>
    <form action='uchwaly.php?rok='.<? print $rok; ?>.' method='post'>
        <input type='hidden' name='change' value='-1'>
        <tr>
            <td class='lp'><input type='text' name='numer' size='3'></td>
            <td class='data kalendarz'><input class='datepicker' type='text' name='data' size='9'></td>
            <td>
                <input type='text' name='sprawa' size='100'>
            </td>
            <td>
                <input type='checkbox' name='nieaktualna' value='tak' title='Zaznaczone jeśli nieaktualna'>
                <input type='submit' value='Zapisz' class='button' />
            </td>
        </tr>
        <tr>
            <td colspan='5'>
                <textarea name='tresc' maxlength='65530' rows='10' style='width:100%;'></textarea>
            </td>
        </tr>
    </form>
<? }

$rows = pfs_select (array (
    table   => $DB_TABLES[resolutions],
    where   => array ( 'YEAR(`data`)' => $rok ),
    order   => array ( '!data', '!numer' )
));

foreach ($rows as $row) {
    if ((isset ($_GET['edit'])) && ($_GET['edit'] == $row->id)) {
        print "
            <form action='uchwaly.php?rok=$rok' method='post'>
            <input type='hidden' name='change' value='$row->id'>
            <tr>
                <td class='lp'><input type='text' name='numer' size='3' value='$row->numer'></td>
                <td class='data kalendarz'><input class='datepicker' type='text' name='data' size='9' value='$row->data'></td>
                <td><input type='text' name='sprawa' size='100' value='$row->sprawa'></td>
                <td>
                    <input type='checkbox' name='nieaktualna' value='tak' title='Zaznaczone jeśli nieaktualna'".
                        ($row->nieaktualna ? " checked" : "").">
                    <input type='submit' value='Zapisz' class='button'>
                </td>
            </tr>
            <tr>
                <td colspan='4'>
                    <textarea name='tresc' maxlength='65530' rows='10' style='width:100%;'>".$row->tresc."</textarea>
                </td>
            </tr>
            </form>";
    }
    else{
        print '
            <tr>
                <td class="lp">'.$row->numer.'</td>
                <td class="data">'.$row->data.'</td>
                <td>'.($row->nieaktualna ? '<i>'.$row->sprawa.'</i>' : $row->sprawa).'</td>
                <td class="icons">
                    <a href="uchwaly.php?edit='.$row->id.'&rok='.$rok.'" title="edytuj" class="edit"></a>
                    <a href="uchwaly.php?delete='.$row->id.'&rok='.$rok.'" title="usuń" class="delete" onclick="return confirmDelete (\'uchwałę nr ' . $row->numer . ' z dnia ' . $row->data . '\');"></a>
                </td>
            </tr>';
    }
}
?>

    </table>
</div>
</body>
</html>
