<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'siodemki') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[news], array ('id' => $_GET['delete']));
}

else if (isset ($_POST['change'])){
    $data = array (
        'data'  => time(),
        'tytul' => sierotki ($_POST['tytul']),
        'descr' => sierotki ($_POST['descr']),
        'link'  => $_POST['link'],
    );

    if ($_POST['change'] != -1 && (!isset ($_POST['update_date']) || $_POST['update_date'] != 'true')) {
        unset ($data['data']);
    }

    $_POST['change'] == -1
        ? pfs_insert ($DB_TABLES[news], $data)
        : pfs_update ($DB_TABLES[news], $data, array ('id' => $_POST['change']));
}
?>

<html>
<head>
    <title>Nowości na stronie</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <style type="text/css">
        .news-date {
            font-weight: bold;
            display: block;
        }
        .header {
            float: left;
            margin: 10px 0 0 5px;
            font-weight: bold;
        }
        #update_date {
            width: auto;
            margin-top: 20px;
        }
    </style>
</head>

<body>
<div id='header'>
    <h1>Nowości na stronie</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='nowosci.php?nowa=1'>Dodaj newsa</a></li>
    </ul>
</div>

<div id="content">
    <table style="width:700px;">
        <tr>
            <th class='data'>Data</th>
            <th>Treść</th>
            <th></th>
        </tr>
<?

if (isset($_GET['nowa'])) {
    print "
        <form enctype='multipart/form-data' action='nowosci.php' method='post'>
            <input type='hidden' name='change' value='-1'>
            <tr>
                <td>
                    <span class='header'>Link:</span>
                    <input type='text' name='link' />

                    <span class='header'>Tytuł:</span>
                    <input type='text' name='tytul' />

                    <span class='header'>Skrót newsa:</span>
                    <textarea name='descr' maxlength='65530' rows='3'></textarea>
                </td>
                <td style='width:120px;'>
                    <input type='submit' value='Zapisz' class='button' />
                </td>
            </tr>
        </form>";
}

$rows = pfs_select (array (
    table   => $DB_TABLES[news],
    order   => array ( '!data' )
));

foreach ($rows as $row) {
    if((isset($_GET['edit'])) && ($_GET['edit']==$row->id)){
        print "
            <form enctype='multipart/form-data' action='nowosci.php' method='post'>
                <input type='hidden' name='change' value='".$row->id."'>
                <tr>
                    <td>
                        <span class='header'>Link:</span>
                        <input type='text' name='link' value='".$row->link."'>

                        <span class='header'>Tytuł:</span>
                        <input type='text' name='tytul' value='".$row->tytul."'>

                        <span class='header'>Skrót newsa:</span>
                        <textarea name='descr' maxlength='65530' rows='3'>".$row->descr."</textarea>
                    </td>
                    <td style='width:120px;'>
                        <input type='submit' value='Zapisz' class='button' /><br />
                        <input type='checkbox' name='update_date' id='update_date' value='true' checked='checked' />
                        <label for='update_date'>Aktualizuj datę</label>
                    </td>
                </tr>
            </form>";
    }

    else {
        print '
            <tr>
                <td>
                    <span class="news-date">' . $row->data . '</span>
                    <a href="' . $row->link . '">'.$row->tytul.'</a>
                </td>
                <td class="icons">
                    <a href="nowosci.php?edit='.$row->id.'" title="edytuj" class="edit"></a>
                    <a href="nowosci.php?delete='.$row->id.'" title="usuń" class="delete" onclick="return confirmDelete (\'newsa z ' . $row->data . '\');"></a>
                </td>
            </tr>';
    }
}

?>
    </table>
</div>
</body>
</html>
