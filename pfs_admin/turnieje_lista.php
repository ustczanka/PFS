<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[tours], array ('id' => $_GET['delete']));
}
?>

<html>
<head>
    <title>Turnieje PFS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <script>
        $(document).ready (function () {
            $( "#tabs" ).tabs ({ active: 0 }).show ();
        });
    </script>
</head>

<body>
<div id='header'>
    <h1>Turnieje Polskiej Federacji Scrabble</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='turnieje.php'>Nowy turniej</a></li>
    </ul>
</div>

<div id="content">
    <div id="tabs" style="display:none;">

<?php
$act_year = date('Y') + 1;

print '<ul>';
for ($year = $act_year; $year >= 1993; $year--) {
    print '<li><a href="#tabs-'.$year.'">' . $year . '</a></li>';
}
print '<li><a href="#tabs-bezdaty">Bez daty</a></li>';
print '</ul>';

for ($year = $act_year; $year >= 1993; $year--) {
    print '<div id="tabs-'.$year.'"><table><tr><th colspan="4">' . $year . '</th></tr>';
    $tours = pfs_select (array (
        table   => $DB_TABLES[tours],
        where   => array ( 'YEAR(`data_od`)' => $year ),
        order   => array ( '!data_do' )
    ));
    foreach ($tours as $tour) {
        print "<tr>
            <td class='icons'>
                <a href='turnieje.php?edit=$tour->id' title='Edycja' class='edit'></a>
                <a href='http://www.pfs.org.pl/turniej.php?id=$tour->id' title='pfs' target='_blank' class='link'></a>
                <a href='turnieje_lista.php?delete=$tour->id' title='Usunięcie' class='delete' onclick='return confirmDelete (\"$tour->nazwa\");'></a>
            </td>
            <td class='data'>$tour->data_od</td>
            <td>$tour->miasto</td>
            <td><a href='turnieje.php?edit=$tour->id' title='Edycja'>
                <b>Turniej:</b> $tour->nazwa".
                (!$tour->typ ? " <span class='important' title='Brak typu turnieju'>*</span>" : "") .
                ($tour->tytul ? "<br><span class='status $tour->status'></span><b>Relacja:</b> $tour->tytul" : "") ."
                </a>
            </td>
        </tr>";
    }
    print '</table></div>';
}

print '<div id="tabs-bezdaty"><table>';
    $tours = pfs_select (array (
        table   => $DB_TABLES[tours],
        where   => array ( 'YEAR(`data_od`)' => '0' )
    ));
    foreach ($tours as $tour) {
        print "<tr>
            <td class='icons'>
                <a href='turnieje.php?edit=$tour->id' title='Edycja' class='edit'></a>
                <a href='http://www.pfs.org.pl/turniej.php?id=$tour->id' title='pfs' target='_blank' class='link'></a>
                <a href='turnieje_lista.php?delete=$tour->id' title='Usunięcie' class='delete' onclick='return confirmDelete (\"$tour->nazwa\");'></a>
            </td>
            <td class='data'>$tour->data_od</td>
            <td>$tour->miasto</td>
            <td><a href='turnieje.php?edit=$tour->id' title='Edycja'>
                <b>Turniej:</b> $tour->nazwa".
                (!$tour->typ ? " <span class='important' title='Brak typu turnieju'>*</span>" : "") .
                ($tour->tytul ? "<br><span class='status $tour->status'></span><b>Relacja:</b> $tour->tytul" : "") ."
                </a>
            </td>
        </tr>";
    }
    print '</table></div>';
?>
    </div>
</div>
</body>
</html>
