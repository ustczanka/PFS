<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'zapisy') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[games], array ('id' => $_GET['delete']), 1);
    pfs_delete ($DB_TABLES[moves],  array ('id_zapisu' => $_GET['delete']));
}

else if (isset ($_POST['change'])) {
    $data = array (
        'id_turnieju'   => $_POST['id_turnieju'],
        'nazwa'         => $_POST['nazwa'],
        'data'          => $_POST['data'],
        'gospodarz'     => $_POST['gospodarz'],
        'gosc'          => $_POST['gosc']
    );

    $_POST['change'] == -1
        ? pfs_insert ($DB_TABLES[games], $data)
        : pfs_update ($DB_TABLES[games], $data, array ('id' => $_POST['change']));
}
?>

<html>
<head>
    <title>Zapisy partii turniejowych</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>Zapisy partii turniejowych</h1>
    <ul class='menu'>
<? if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='main.php'>Menu</a></li>"; } ?>
        <li><a href='zapisy_lista.php?nowa=1'>Nowy zapis</a></li>
    </ul>
</div>

<div id="content">
    <table>
        <tr>
            <th class='lp'></th>
            <th class='data'>Data</th>
            <th>Id tur.</th>
            <th>Nazwa</th>
            <th class='person'>Gospodarz</th>
            <th class='person'>Gość</th>
            <th class='center'>Wynik</th>
            <th class="icons"></th>
        </tr>
<?
if ($_GET['nowa']) {
?>
        <form action='zapisy_lista.php' method='post'>
            <input type='hidden' name='change' value='-1'>
            <tr>
                <td class='lp'></td>
                <td class='data kalendarz'><input type='text' name='data' size='9' class='datepicker'></td>
                <td><input type='text' name='id_turnieju'></td>
                <td><input type='text' name='nazwa' size='50'></td>
                <td class='person'><input type='text' name='gospodarz' size='20'></td>
                <td class='person'><input type='text' name='gosc' size='20'></td>
                <td></td>
                <td class='icons'><input type='submit' value='Zapisz' class='button'></td>
            </tr>
        </form>
<?
}

$i      = 1;
$rows  = pfs_select (array (
    table   => $DB_TABLES[games],
    order   => array ( '!data', 'nazwa' )
));
foreach ($rows as $row) {
    $suma1 = pfs_select_one (array (
        table   => $DB_TABLES[moves],
        fields  => array ( 'SUM(`punkty`)'),
        where   => array ( id_zapisu => $row->id, '!punkty' => -1000, gracz => 1 ),
        groupby => array ( 'gracz' )
    ));
    $suma1 = $suma1->sum;
    $suma2 = pfs_select_one (array (
        table   => $DB_TABLES[moves],
        fields  => array ( 'SUM(`punkty`)'),
        where   => array ( id_zapisu => $row->id, '!punkty' => -1000, gracz => 2 ),
        groupby => array ( 'gracz' )
    ));
    $suma2 = $suma2->sum;

    if ($_GET['edit'] && $_GET['edit'] == $row->id) {
        print "
            <form action='zapisy_lista.php' method='post'>
                <input type='hidden' name='change' value='".$row->id."'>
                <tr>
                    <td class='lp'>".$i++."</td>
                    <td class='data kalendarz'><input type='text' name='data' class='datepicker' value='".$row->data."'></td>
                    <td><input type='text' name='id_turnieju' value='".$row->id_turnieju."'></td>
                    <td><input type='text' name='nazwa' size='50' value='".$row->nazwa."'></td>
                    <td class='person'><input type='text' name='gospodarz' size='20' value='".$row->gospodarz."'></td>
                    <td class='person'><input type='text' name='gosc' size='20' value='".$row->gosc."'></td>
                    <td style='text-align:center;'>".$suma1."&nbsp;:&nbsp;".$suma2."</td>
                    <td class='icons'><input type='submit' value='Zapisz' class='button'></td>
                </tr>
            </form>";
    }

    else {
        print '
            <tr>
                <td class="lp">'.$i++.'</td>
                <td class="data">'.$row->data.'</td>
                <td>'.$row->id_turnieju.'</td>
                <td>'.$row->nazwa.'</td>
                <td class="person">'.$row->gospodarz.'</td>
                <td class="person">'.$row->gosc.'</td>
                <td style="text-align:center;">'.$suma1."&nbsp;:&nbsp;".$suma2.'</td>
                <td class="icons">
                    <a href="zapisy_lista.php?edit='.$row->id.'" title="edytuj" class="edit"></a>
                    <a href="zapis.php?id='.$row->id.'" title="plansza" class="plansza"></a>
                    <a href="zapisy_lista.php?delete='.$row->id.'" title="usuń" class="delete" onclick="return confirmDelete (\'' . $row->nazwa . '\');"></a>
                </td>
            </tr>';
    }
}
?>
    </table>
</div>
</body>
</html>
