<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'siodemki') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}
?>

<html>
<head>
    <title>PFS::Strona główna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>

<div id='header'>
    <h1>Strona główna</h1>
    <ul class="menu">
<?
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='turnieje_lista.php'>Turnieje</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin' || $_COOKIE['pfsuser'] == 'siodemki') { print "<li><a href='siodemki_lista.php'>Siódemki</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='rankroczny.php'>Ranking roczny</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='gp2013.php'>GP 2013</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='medale.php'>Medale</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='uchwaly.php'>Uchwały PFS</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='kluby_lista.php'>Kluby</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='scrabblisci.php'>Scrabbliści</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin' || $_COOKIE['pfsuser'] == 'siodemki') { print "<li><a href='nowosci.php'>Nowości</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin' || $_COOKIE['pfsuser'] == 'siodemki') { print "<li><a href='osps_aktualizacja.php'>OSPS</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='zapisy_lista.php'>Plansze</a></li>"; }
    if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='live_lista.php'>LIVE</a></li>"; }
?>
    </ul>
</div>

<div id='content'>
    <div class='todo'>Po każdym turnieju:<ul>
        <li>baza: relacja</li>
        <li>baza: aktualizacja medali, rankingu rocznego i GP (jeśli turniej był GP)</li>
        <li>MP, GP, PP: dodać zwycięzcę do zakładki Scrabbliści</li>
    </ul></div>

    <div class='todo'>Na początku roku:<ul>
        <li>dlasponsorow.php: aktualizacja</li>
        <li>jakprzystapic.php: aktualizacja składek</li>
        <li>dlaczlonkow.php: aktualizacja składek</li>
        <li>finanse.php: aktualizacja</li>
        <li>gp.php: nowe GP</li>
    </ul></div>

    <div class='todo'>Pod koniec roku:<ul>
        <li>archiwum_gp.php: dodać GP z mijającego roku</li>
        <li>podsumowanie.php: podsumowanie mijającego roku (dodanie linku w archiwum)</li>
    </ul></div>
<div>

</body>
</html>
