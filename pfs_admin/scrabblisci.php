<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();
$typ      = ($_GET['typ'] ? $_GET['typ'] : 'MP');
$typy     = array (
    "MP"    => "Mistrzowie Polski",
    "GP"    => "Zwycięzcy Grand Prix",
    "PP"    => "Zdobywcy Pucharu Polski",
    "LR"    => "Liderzy rankingu"
);

function sprobujZnalezcDane ($osoba, $typdanej) {
    global $DB_TABLES;
    if (!$osoba || !$typdanej) return;
    $row = pfs_select_one (array (
        table   => $DB_TABLES[players],
        where   => array ( osoba => $osoba ),
    ));
    return $row->$typdanej;
}

if ($_GET['delete']) {
    pfs_delete ($DB_TABLES[players], array ('id' => $_GET['delete']));
}

else if ($_POST['change']) {
    $statnr = ($_POST['statnr'] ? $_POST['statnr'] : sprobujZnalezcDane ($_POST['osoba'], 'statnr'));

    if (!($plik = $_FILES['plik'])) {
        $plik = sprobujZnalezcDane ($_POST['osoba'], 'plik');
    }

    $data = array (
        'rok'    => $_POST['rok'],
        'osoba'  => $_POST['osoba'],
        'notka'  => sierotki ($_POST['notka']),
        'statnr' => $statnr,
        'typ'    => $typ
    );

    if ($_POST['change'] == -1) {
        $auto_id = pfs_insert ($DB_TABLES[players], $data);
        pfs_photo_update ($plik, $DB_TABLES[players], $auto_id);
    }
    else {
        pfs_update ($DB_TABLES[players], $data, array ('id' => $_POST['change']), true);
        pfs_photo_update ($plik, $DB_TABLES[players], $_POST['change']);
    }
}
?>

<html>
<head>
    <title>Wybitni scrabbliści</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css" />
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
    <script>
        function zmianaTypu(sel)
        {
            opcje = sel.options;
            for(i=0; i<opcje.length; i++)
                if(opcje[i].selected)
                    typ = opcje[i].value;
            document.location="scrabblisci.php?typ="+typ;
        }
    </script>
</head>

<body>
<div id='header'>
    <h1>Wybitni scrabbliści</h1>
    <ul class='menu'>
        <li><a href='main.php'>Menu</a></li>
        <li><a href='scrabblisci.php?nowa=1&typ=<? print $typ; ?>'>Dodaj nową osobę</a></li>
        <li><select onchange='zmianaTypu(this)'>
<?
foreach ($typy as $key => $value) {
    print '<option value="' . $key . '"' . ($typ == $key ? ' selected="selected"' : '') . '>' . $value . '</option>';
}
?>
        </select></li>
    </ul>
</div>

<div id="content">
    <table style="width:600px;">
        <tr>
            <th class='lp'><?php print ($typ == 'LR' ? 'Lp.' : 'Rok'); ?></th>
            <th class='person'>Imię i nazwisko</th>
            <th class='center'>Statystyki</th>
            <th></th>
            <th></th>
        </tr>

<?
if ($_GET['nowa']) {
    print "
        <form enctype='multipart/form-data' action='scrabblisci.php?typ=".$typ."' method='post'>
            <input type='hidden' name='change' value='-1'>
            <input type='hidden' name='MAX_FILE_SIZE' value='400000'>
            <tr>
                <td class='lp'><input type='text' name='rok' size='3'></td>
                <td class='person'><input type='text' name='osoba' size='30'></td>
                <td><input type='text' name='statnr' size='3'></td>
                <td><input name='plik' type='file'></td>
                <td><input type='submit' value='Zapisz' class='button'></td>
            </tr>
            <tr>
                <td colspan='5'>
                    <textarea name='notka' maxlength='65530' rows='8' style='width:100%;'>
                </textarea>
            </td>
        </form>";
}

$rows = pfs_select (array (
    table   => $DB_TABLES[players],
    where   => array ( typ => $typ ),
    order   => array ( '!rok' ),
));
foreach ($rows as $player) {
    if ($_GET['edit'] && $_GET['edit'] == $player->id){
        print "
            <form enctype='multipart/form-data' action='scrabblisci.php?typ=".$typ."' method='post'>
                <input type='hidden' name='change' value='".$player->id."'>
                <input type='hidden' name='MAX_FILE_SIZE' value='400000'>
                <tr>
                    <td class='lp'><input type='text' name='rok' size='3' value='".$player->rok."'></td>
                    <td class='person'><input type='text' name='osoba' size='30' value='".$player->osoba."'></td>
                    <td><input type='text' name='statnr' size='3' value='".$player->statnr."'></td>
                    <td>".
                        ($player->plik != null && file_exists ("../pliki_strony/scrabblisci/".$player->plik)
                            ? "<img src='../pliki_strony/scrabblisci/".$player->plik."' style='max-width:200px;margin-left:0px;'/>"
                            : ""
                        ) . "
                        <input name='plik' type='file'/>
                    </td>
                    <td><input type='submit' value='Zapisz' class='button'></td>
                </tr>
                <tr>
                    <td colspan='5'>
                        <textarea name='notka' maxlength='65530' rows='8' style='width:100%;'>".$player->notka."</textarea>
                    </td>
                </tr>
            </form>";
    }

    else {
        print '
            <tr>
                <td class="lp">'.$player->rok.'</td>
                <td class="person">'.$player->osoba.'</td>
                <td colspan="2">'.$player->statnr.'</td>
                <td class="icons">
                    <a href="scrabblisci.php?edit='.$player->id.'&typ='.$typ.'" title="edytuj" class="edit"></a>
                    <a href="scrabblisci.php?delete='.$player->id.'&typ='.$typ.'" title="usuń" class="delete" onclick="return confirmDelete (\'' . $player->osoba . '\');"></a>
                </td>';
    }
}
?>

    </table>
</div>
</body>
</html>
