<?php
if ($_COOKIE['pfsuser'] != 'admin' && $_COOKIE['pfsuser'] != 'live') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$sql_conn = pfs_connect  ();

if ($_GET['unsetlive']) {
    pfs_update ($DB_TABLES[tours], array (show_live => 0), array (id => $_GET['unsetlive']));
}
else if ($_GET['setlive']) {
    pfs_update ($DB_TABLES[tours], array (show_live => 1), array (id => $_GET['setlive']));
}

?>

<html>
<head>
    <title>LIVE - Najbliższe turnieje PFS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/admin.css" type="text/css">
    <link rel="stylesheet" href="http://pfs.org.pl/files/css/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="http://pfs.org.pl/files/js/admin.js"></script>
</head>

<body>
<div id='header'>
    <h1>LIVE - Najbliższe turnieje PFS</h1>
    <ul class='menu'>
<? if ($_COOKIE['pfsuser'] == 'admin') { print "<li><a href='main.php'>Menu</a></li>"; } ?>
    </ul>
</div>

<div id="content">
    <table style="width:600px;">
        <tr>
            <th>Turniej</th>
            <th class='lp'>Ilość rund</th>
            <th></th>
        </tr>

<?php
$result = mysql_query ("SELECT `id`, `nazwa`, `show_live`, `ilosc_rund` FROM $DB_TABLES[tours] WHERE `data_do` >= ADDDATE(NOW(), -12) AND `ilosc_rund`>0 ORDER BY `data_od`");
while ($row = mysql_fetch_object ($result)) {
    print "
    <form action='live_lista.php' method='post'>
        <tr>".
        ($row->show_live
            ? "<td><a class='important' href='live.php?id=$row->id'>$row->nazwa</a></td>"
            : "<td>$row->nazwa</td>"
        ).
    "<td>$row->ilosc_rund</td>".
    ($row->show_live
        ? "<td><a class='important' href='live_lista.php?unsetlive=$row->id'>Deaktywuj&nbsp;relację</a></td>"
        : "<td><a href='live_lista.php?setlive=$row->id'>Aktywuj&nbsp;relację</a></td>"
    ).
    "</form>";
}
?>
    </table>
</div>
</body>
</html>
