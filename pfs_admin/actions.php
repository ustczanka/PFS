<?php
if ($_COOKIE['pfsuser'] != 'admin') {
    setcookie ('pfsuser', '', time () - 3600);
    header ('Location: http://admin.pfs.org.pl');
    exit ();
}

include_once ('funkcje.php');
$action = isset ($_POST['action']) ? $_POST['action'] : $_GET['action'];

switch ($action) {
    case 'tour_save':
        pfs_tour_save ($_POST);
        break;
    case 'story_save':
        pfs_story_save ($_POST);
        break;
    case 'tour_groups_list':
        pfs_tour_groups_get ($_POST['term']);
        break;
    case 'players_list':
        pfs_players_get ($_POST['term']);
        break;
    default:
        print $action . " - nieprawidłowa akcja";
        break;
}

function pfs_tour_save ($data) {
    global $DB_TABLES;
    $save_data = array (
        'nazwa'             => sierotki ($data['nazwa']),
        'data_od'           => $data['data_od'],
        'data_do'           => $data['data_do'],
        'rodzaj'            => $data['rodzaj'],
        'rank'              => $data['rank'],
        'formularz'         => ($data['formularz'] == 'tak'),
        'typ'               => $data['typ'],
        'ilosc_rund'        => $data['ilosc_rund'],
        'sposob'            => $data['sposob'],
        'miejsce'           => $data['miejsce'],
        'miasto'            => $data['miasto'],
        'obronca'           => $data['obronca'],
        'sedzia'            => $data['sedzia'],
        'stronawww'         => $data['stronawww'],
        'harmonogram'       => sierotki ($data['harmonogram']),
        'wpisowe'           => sierotki ($data['wpisowe']),
        'dojazd'            => sierotki ($data['dojazd']),
        'wyzywienie'        => sierotki ($data['wyzywienie']),
        'noclegi'           => sierotki ($data['noclegi']),
        'nagrody'           => sierotki ($data['nagrody']),
        'atrakcje'          => sierotki ($data['atrakcje']),
        'zgloszenia'        => sierotki ($data['zgloszenia']),
        'organizatorzy'     => sierotki ($data['organizatorzy']),
        'sponsorzy'         => sierotki ($data['sponsorzy']),
        'dodatkowe'         => sierotki ($data['dodatkowe']),
        'dodatkowe_przed'   => sierotki ($data['dodatkowe_przed']),
        'uczestnicy'        => $data['uczestnicy']
    );

    isset ($data['tour_id']) && $data['tour_id']
        ? pfs_update ($DB_TABLES[tours], $save_data, array ('id' => $data['tour_id']), true)
        : pfs_insert ($DB_TABLES[tours], $save_data, true);

    print json_encode (array ( info => 1, msg => "Pomyślnie zapisano informacje o turnieju."));
}

function pfs_story_save ($data) {
    global $DB_TABLES;
    $save_data = array (
        'tytul'         => sierotki($data['tytul']),
        'frekwencja'    => $data['frekwencja'],
        'tresc'         => sierotki ($data['tresc']),
        'skrot'         => sierotki ($data['skrot']),
        'galeria1'      => $data['galeria1'],
        'galeria2'      => $data['galeria2'],
        'galeria3'      => $data['galeria3'],
        'galeria4'      => $data['galeria4'],
        'galeria5'      => $data['galeria5'],
        'autor'         => $data['autor'],
        'zwyciezca'     => $data['zwyciezca'],
        'miejsce2'      => $data['miejsce2'],
        'miejsce3'      => $data['miejsce3'],
        'status'        => $data['status']
    );

    foreach (explode (' ', 'klasyfikacja hh rundy ranking') as $column) {
        if ($data['remove_' . $column] == 'true') {
            $save_data[$column] = '';
        }

        else if ($_FILES[$column]['size'] > 0) {
            $tmp           = $_FILES[$column]['tmp_name'];
            $fh            = fopen ($tmp, 'r');
            $save_data[$column] = addslashes (fread ($fh, filesize ($tmp)));
            fclose ($fh);
        }
    }

    if (isset ($data['tour_id']) && $data['tour_id']) {
        pfs_update ($DB_TABLES[tours], $save_data, array ('id' => $data['tour_id']), true);

        if ($data['remove_foto'] == 'true') {
            pfs_photo_remove ('tours', $data['tour_id']);
        }
        else {
            pfs_photo_update ($_FILES[foto], 'tours', $data['tour_id']);
        }
    }
    else {
        $auto_id = pfs_insert ($DB_TABLES[tours], $save_data, true);
        pfs_photo_update ($_FILES[foto], 'tours', $auto_id);
    }

    print '<textarea>' . json_encode (array ( info => 1, msg => "Pomyślnie zapisano relację z turnieju.")) . '</textarea>';
}

function pfs_tour_groups_get ($search) {
    global $DB_TABLES;

    $select = array (
        table   => $DB_TABLES[groups],
        order   => array ('group')
    );

    if ($search) {
        $select[where] = array ( '%group' => "%$search%");
    }

    $list = pfs_select ($select);

    print json_encode ($list);
}

function pfs_players_get ($search) {
    global $DB_TABLES;

    $list = pfs_select (array (
        table       => $DB_TABLES[tours],
        distinct    => true,
        fields      => array ( 'zwyciezca', 'miejsce2', 'miejsce3' )
    ));

    $players = array ();

    foreach ($list as $item) {
        if ($search) {
            if ($item->zwyciezca != "" && preg_match ('/'.$search.'/i', $item->zwyciezca)) array_push ($players, $item->zwyciezca);
            if ($item->miejsce2  != "" && preg_match ('/'.$search.'/i', $item->miejsce2)) array_push ($players, $item->miejsce2);
            if ($item->miejsce3  != "" && preg_match ('/'.$search.'/i', $item->miejsce3)) array_push ($players, $item->miejsce3);
        }
    }

    $players = array_unique ($players);
    sort ($players);

    print json_encode ($players);
}

?>
