<?
include_once "files/php/funkcje.php";

$mp = mysql_query ('
    SELECT `b`.`id`, `b`.`nazwa`, `b`.`gospodarz`, `b`.`gosc`, YEAR(`b`.`data`) AS `rok`
    FROM `boards` AS `b` LEFT JOIN `tournaments` ON `tournaments`.`id`=`b`.`id_turnieju`
    WHERE `typ`="Mistrzostwa Polski"
    ORDER BY `b`.`data` DESC, `b`.`id` DESC');

$pp = mysql_query ('
    SELECT `b`.`id`, `b`.`nazwa`, `b`.`gospodarz`, `b`.`gosc`, YEAR(`b`.`data`) AS `rok`
    FROM `boards` AS `b` LEFT JOIN `tournaments` ON `tournaments`.`id`=`b`.`id_turnieju`
    WHERE `typ`="Puchar Polski"
    ORDER BY `b`.`data` DESC, `b`.`id` DESC');

$other = mysql_query ('
    SELECT `b`.`id`, `b`.`nazwa`, `b`.`gospodarz`, `b`.`gosc`
    FROM `boards` AS `b` LEFT JOIN `tournaments` ON `tournaments`.`id`=`b`.`id_turnieju`
    WHERE `typ` NOT IN ("Puchar Polski", "Mistrzostwa Polski")
    ORDER BY `b`.`data` DESC, `b`.`id` DESC');

?>

<html>
    <title>Polska Federacja Scrabble :: Turnieje : Zapisy partii turniejowych</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("turnieje","archiwum");
         $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
    <style>
        table.klasyfikacja tr:first-child, table.klasyfikacja td:first-child { font-width: normal; }
    </style>
</head>

<body>
<? require_once "files/php/menu.php"; ?>
<h1><script>naglowek("Zapisy partii turniejowych")</script></h1>

<div id="tabs" style="display:none;">
    <ul>
        <li><a href='#tabs-mp'>Mistrzostwa Polski</a></li>
        <li><a href='#tabs-pp'>Puchary Polski</a></li>
        <li><a href='#tabs-other'>Inne ciekawe partie</a></li>
    </ul>

<div id='tabs-mp'>
<table class="zapisy">
    <tr><td class="rok">2004</td><td><a href="zapisy/final041.html" target="_blank">XII MP - 1. finał: M.Wrześniewski - K.Mówka 340:269</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/final042.html" target="_blank">XII MP - 2. finał: K.Mówka - M.Wrześniewski 274:443</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/final043.html" target="_blank">XII MP - 3. finał: M.Wrześniewski - K.Mówka 419:318</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/final044.html" target="_blank">XII MP - 4. finał: K.Mówka - M.Wrześniewski 459:354</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/final045.html" target="_blank">XII MP - 5. finał: M.Wrześniewski - K.Mówka 400:316</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/3m041.html" target="_blank">XII MP - 1. partia meczu o 3 miejsce: M.Żbikowski - D.Pikul 352:404</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/3m042.html" target="_blank">XII MP - 2. partia meczu o 3 miejsce: D.Pikul - M.Żbikowski 380:344</a></td></tr>
    <tr><td class="rok">2005</td><td><a href="zapisy/final051/00.html" target="_blank">XIII MP - 1. finał: Mariusz Skrobosz - Marek Syczuk 455:400</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/final052/00.html" target="_blank">XIII MP - 2. finał: Mariusz Skrobosz - Marek Syczuk 418:389</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/final053/00.html" target="_blank">XIII MP - 3. finał: Mariusz Skrobosz - Marek Syczuk 427:337</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/final054/00.html" target="_blank">XIII MP - 4. finał: Mariusz Skrobosz - Marek Syczuk 380:350</a></td></tr>
    <tr><td class="rok">2006</td><td><a href="zapisy/mp2006/final1.html" target="_blank">XVI MP - 1. finał: Krzysztof Mówka - Mariusz Skrobosz 515:422</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/mp2006/final2.html" target="_blank">XVI MP - 2. finał: Mariusz Skrobosz - Krzysztof Mówka 347:426</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/mp2006/final3.html" target="_blank">XVI MP - 3. finał: Mariusz Skrobosz - Krzysztof Mówka 345:422</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/mp2006/final4.html" target="_blank">XVI MP - 4. finał: Mariusz Skrobosz - Krzysztof Mówka 434:349</a></td></tr>
    <tr><td>&nbsp;</td><td><a href="zapisy/mp2006/final5.html" target="_blank">XVI MP - 5. finał: Krzysztof Mówka - Mariusz Skrobosz 528:346</a></td></tr>
</table>
<table class="zapisy">
<?
$year = 0;
while ($board = mysql_fetch_object ($mp)) {
    print '<tr>';
    if ($board->rok != $year) {
        print "<td class='rok'>$board->rok</td>";
        $year = $board->rok;
    }
    else {
        print '<td>&nbsp;</td>';
    }
    print "<td><a href='zapis.php?id=$board->id'><b>$board->nazwa</b>: $board->gospodarz - $board->gosc</a></td>
    </tr>";
}
?>
</table>
</div>

<div id='tabs-pp'>
<table class="zapisy">
<?
$year = 0;
while ($board = mysql_fetch_object ($pp)) {
    print '<tr>';
    if ($board->rok != $year) {
        print "<td class='rok'>$board->rok</td>";
        $year = $board->rok;
    }
    else {
        print '<td>&nbsp;</td>';
    }
    print "<td><a href='zapis.php?id=$board->id'><b>$board->nazwa</b>: $board->gospodarz - $board->gosc</a></td>
    </tr>";
}
?>
</table>
</div>

<div id='tabs-other'>
<h2>Inne ciekawe partie</h2>
<table class="zapisy">
    <tr><td><a href="zapisy/529-436.html" target="_blank">Suma wyników - 965 punktów: E.Stefaniak - J.Hoffmann 529:436</a></td></tr>
    <tr><td><a href="zapisy/545-421.html" target="_blank">Suma wyników - 966 punktów: M.Kania - Z.Szota 545:421</a></td></tr>
    <tr><td><a href="zapisy/570-407.html" target="_blank">Suma wyników - 977 punktów: G.Święcki - Z.Szota 570:407</a></td></tr>
    <tr><td><a href="zapisy/514-468.html" target="_blank">Suma wyników - 982 punkty: M.Kania - M.Kamiński 514:468</a></td></tr>
    <tr><td><a href="zapisy/zbi-gor.html" target="_blank">Suma wyników - 1031 pkt: M. Żbikowski - K. Górka 577:454</a></td></tr>
    <tr><td><a href="zapisy/puc-zwo.html" target="_blank">Suma wyników - 1050 pkt: J.Puchalski - T.Zwoliński 590:460</a></td></tr>
    <tr><td><a href="zapisy/cze-ban1.html" target="_blank">Cztery skrable na otwarciu: D.Banaszek - W.Czerwoniec 479:300</a></td></tr>
    <tr><td><a href="zapisy/skb-zbi1.html" target="_blank">Sześć skrabli jednego gracza: M.Skrobosz - M.Żbikowski 629:218</a></td></tr>
    <tr><td><a href="zapisy/skb-mad.html" target="_blank">Sześć skrabli jednego gracza: M.Skrobosz - B.Madej 590:248</a></td></tr>
    <tr><td><a href="zapisy/zwo-gos.html" target="_blank">Sześć skrabli jednego gracza: T.Zwoliński - A.Gostomski 645:278</a></td></tr>
    <tr><td><a href="zapisy/puc-zwo.html" target="_blank">Najwyżej punktowany ruch - 249 pkt: J.Puchalski - T.Zwoliński 590:460</a></td></tr>
    <tr><td><a href="zapisy/szm-wol.html" target="_blank">Najwyżej punktowany ruch - 257 pkt: A.Szmidtke - M.Wołowiec 648:347</a></td></tr>
    <tr><td><a href="zapisy/bor-las.html" target="_blank">Najwyżej punktowany ruch - 257 pkt: J.Borowski - M.Łasiewicki 691:207</a></td></tr>
    <tr><td><a href="zapisy/474-474.html" target="_blank">Najwyższy remis: D.Białobrzewski - T.Wiśniewski 474:474</a></td></tr>
    <tr><td><a href="zapisy/mow-skr.html" target="_blank">Najwyższy wynik pokonanego - 515 pkt: K.Mówka - M.Skrobosz 515:518</a></td></tr>
    <tr><td><a href="zapisy/syl-zbi.html" target="_blank">Analiza końcówki partii: M.Sylwestrzuk - M.Żbikowski 501:502</a></td></tr>
    <tr><td><a href="zapisy/5siodemekotwarcie.html" target="_blank">Pięć skrabli na otwarciu: B.Książek - M.Żbikowski 292:620</a></td></tr>
</table>

<table class="zapisy">
<?
while ($board = mysql_fetch_object ($other)) {
    print "<tr><td><a href='zapis.php?id=$board->id'><b>$board->nazwa</b>: $board->gospodarz - $board->gosc</a></td></tr>";
}
?>
</table>
</div>
</div>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
