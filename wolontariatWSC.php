﻿<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Wolontariat podczas Mistrzostw Świata w Scrabble po angielsku</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>

<?require_once "../files/php/menu.php"?>
<h1>Wolontariat podczas Mistrzostw Świata w Scrabble po angielsku</h1>

<center><a href="http://www.wscgames.com/" target="_blank"><img src="http://www.wscgames.com/images/logos/2011-wsc.gif"></a></center>

<div style="width:90%;" >

Witamy,<br>


Jeżeli trafiłeś na naszą stronę mamy nadzieję, że chcesz zostać wolontariuszem podczas World Scrabble Championship 2011 (Mistrzostw Świata w Scrabble po angielsku) lub imprez towarzyszacych temu niezwykłemu wydarzeniu. WSC 2011 to najważniejsza i jedna z największych imprez scrablowych na świecie. Reprezentanci ponad 40 państw stoczą przez pięć dni wiele pasjonujących pojedynków na słowa, aby wyłonić spośród siebie Mistrza Świata.<br><br>
Tegoroczny turniej organizowany jest przez firmę Mattel przy współudziale Polskiej Federacji Scrabble.<br><br>

Wolontariusz to według Ustawy o działalności pożytku publicznego i o wolontariacie ten, kto dobrowolnie i świadomie oraz bez wynagrodzenia angażuje się w pracę na rzecz osób, organizacji pozarządowych, a także rozmaitych instytucji działających w różnych obszarach społecznych.<br><br>

Określenie bezpłatna nie oznacza bezinteresowna, lecz bez wynagrodzenia materialnego. W rzeczywistości wolontariusz uzyskuje liczne korzyści niematerialne: satysfakcję, spełnienie swoich motywacji (poczucie sensu, uznanie ze strony innych, podwyższenie samooceny itd.), zyskuje nowych przyjaciół i znajomych, zdobywa wiedzę, doświadczenie i nowe umiejętności, a w związku z tym i lepszą pozycję na rynku pracy.<br><br>

Jeżeli jesteś gotowy na przeżycie przygody, kochasz grę w Scrabble i niepowtarzalną atmosferę, która pojawia się zawsze przy dużych imprezach sportowych, chcesz poznać nowych ludzi, dla których bezinteresowna pomoc innym ma również znaczenie, zapraszamy do współpracy.<br><br>

Wolontariusze podczas WSC 2011 będą podzieleni na dwa zespoły zadaniowe:<br><br>
<b>- bezpośrednia praca przy WSC 2011<br></b>
- termin: od popołudnia 11.10. do 16.10.<br>
- wymagana znajomość języka angielskiego na poziomie komunikatywnym<br>
- mile widziane doświadczenie związane z turniejową grą w Scrabble<br>
- główne zadania: opieka nad uczestnikami WSC 2011 i pomoc sędziemu<br><br>

<b>- praca przy imprezach towarzyszących WSC 2011<br></b>
- termin: 16.10.<br>
- mile widziane doświadczenie związane z grą w Scrabble<br>
- główne zadania: pomoc przy organizacji imprezy (turniej i zabawy scrablowe, pilnowanie porządku)<br><br>

Jeżeli masz jakieś pytania – pisz - postaramy się rozwiać Twoje wątpliwości.<br><br>

<a href="mailto:pfs@pfs.org.pl">pfs@pfs.org.pl</a><br><br>

Do zobaczenia podczas World Scrabble Championship 2011!<br><br>


Aby zostać wolontariuszem podczas World Scrabble Championship 2011 musisz wypełnić poniższy formularz zgłoszeniowy.<br><br>
Na zgłoszenia czekamy do 30.09.<br><br>


<? if(empty($_POST['submit'])){	
	print "<form method=\"post\">
	<table><tr><td>
	<div>Imię i nazwisko:<br />
	<input name=\"osoba\" type=\"text\" maxlength=\"50\" size=\"30\"></div><br>
	
	<div>Miasto:<br />
	<input name=\"miasto\" type=\"text\" maxlength=\"50\" size=\"30\"></div><br>

	<div>Adres e-mail:<br />
	<input name=\"email\" type=\"text\" maxlength=\"50\" size=\"30\"></div><br>
	
	<div>Numer telefonu:<br />
	<input name=\"tel\" type=\"text\" maxlength=\"50\" size=\"30\"></div><br>

	<div>Data urodzenia:<br />
	<input name=\"dataur\" type=\"text\" maxlength=\"50\" size=\"30\"></div><br>

	<div>Poziom znajomość języka angielskiego:<br />
	<input name=\"ang\" type=\"text\" maxlength=\"50\" size=\"30\"></div><br>

	<div>Doświadczenie ze Scrabble:<br />
	<textarea name=\"scrabble\" rows=\"5\" cols=\"30\"></textarea></div><br><br>


	<div>
Dyspozycyjność w czasie WSC 2011 (godziny podane są orientacyjnie) - jeżeli jesteś dyspozycyjny tylko przez część dnia opisz to w uwagach na dole formularza<br><br>
	<input type=\"checkbox\" name=\"11\" value=\"tak\" class=\"checkbox\">11.10. (wtorek) - godz. 17-22</input><br /><br>
	<input type=\"checkbox\" name=\"12\" value=\"tak\" class=\"checkbox\">12.10. (środa) - godz. 9-20</input><br /><br>
	<input type=\"checkbox\" name=\"13\" value=\"tak\" class=\"checkbox\">13.10. (czwartek) - godz. 8-20</input><br /><br>
	<input type=\"checkbox\" name=\"14\" value=\"tak\" class=\"checkbox\">14.10. (piątek) - godz. 8-20</input><br /><br>
	<input type=\"checkbox\" name=\"15\" value=\"tak\" class=\"checkbox\">15.10. (sobota) - godz. 8-20</input><br /><br>
	<input type=\"checkbox\" name=\"16\" value=\"tak\" class=\"checkbox\">16.10. (niedziela) - godz. 8-17</input><br /><br>
	

</div>

	</td></tr><br>
	<tr><td colspan=\"2\">
	<div>Uwagi:<br />
	<textarea name=\"uwagi\" rows=\"5\" cols=\"80\"></textarea></div><br>


	<input type=\"submit\" name=\"submit\" value=\"Wyślij\" class=\"przycisk\" >
	</td></tr></table>
	</form>";
 }
else{
	$message = "Imię i nazwisko: $_POST[osoba]\nMiasto: $_POST[miasto]\n\nNr telefonu: $_POST[tel]\nData ur.:$_POST[dataur]\n\nZnajomosc angielskiego: $_POST[ang]\nDoswiadczenie w Scrabble: $_POST[scrabble]\n\n11.10.:$_POST[11]\n12.10.:$_POST[12]\n13.10.:$_POST[13]\n14.10.:$_POST[14]\n15.10.:$_POST[15]\n16.10.:$_POST[16]\n\nUwagi: $_POST[uwagi]";
	$header = "From: $_POST[osoba] <$_POST[email]>\nContent-Type: text/plain; charset=\"utf-8\"\nContent-Transfer-Encoding: 8bit"; 
	@mail("pfs@pfs.org.pl", "Zgloszenie - wolontariat WSC", "$message", "$header") or die('Nie udało się wysłać wiadomości');
	echo "Dziękujemy za zgłoszenie. Wkrótce się z Tobą skontaktujemy."; 
}
?>


</div>








<?require_once "../files/php/bottom.php"?>
</body>
</html>

