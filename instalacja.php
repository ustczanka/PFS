<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Instrukcja instalacji programu sędziowskiego</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("pfs","sedziowie");</script>
    <style>
        p.paragraph {
            margin: 20px;
        }
        ol#steps img {
            display: block;
            margin: 20px auto;
        }
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Instalacja programu sędziowskiego")</script></h1>

<p class="paragraph">Instrukcja dotyczy instalacji progamu sędziowskiego na systemie <b>Windows 7</b>.</p>

<ol id="steps">
    <li><a href="http://ustczanka.pl/scrabble.manager.rar">Ściągnij</a> i rozpakuj paczkę instalacyjną. W rozpakowanym folderze znajdują się
      2 katalogi i 2 pliki niezbędne do instalacji.<img src="/files/img/instalacja_1.jpg" /></li>
    <li>Uruchom plik <b>/IB5/Client32/setup.exe</b> i zainstaluj klikając przycisk <b>Next</b> na kolejnych ekranach (nie trzeba zmieniać żadnych ustawień).</li>
    <li>Uruchom plik <b>/IB2007/ib_install.exe</b> i zainstaluj klikając przycisk <b>Next</b> (albo <b>Yes</b> na ekranie z regulaminem) na kolejnych ekranach.</li>
    <li>Gdy po instalacji pojawi się poniższy ekran kliknij <b>Cancel</b>, następnie <b>Ok</b> i <b>Finish</b>.
        <img src="/files/img/instalacja_2.jpg" /></li>
    <li>Do katalogu <b>C:/Borland/license</b> skopiuj plik <b>reg790.txt</b></li>
    <li>Uruchom plik <b>bdesetup.exe</b> i zainstaluj klikając przycisk <b>Next</b> na kolejnych ekranach.</li>
    <li>W Panelu Sterowania uruchom <b>BDE Administrator</b>.</li>
    <li>Przejdź do zakładki <b>Configuration</b>.</li>
    <li>Rozwiń drzewko wg ścieżki: <b>Configuration -> Drivers -> ODBC -> INTERSOLV InterBase ODBC Driver</b>.
      <img src="/files/img/instalacja_3.jpg" /></li>
    <li>Na tym ostatnim elemencie kliknij prawym przyciskiem myszy i wybierz <b>ODBC Administrator</b>.</li>
    <li>W oknie, które się pojawi kliknij <b>Dodaj</b>, a następnie w okienku wybierz <b>INTERSOLV InterBase ODBC Driver</b> i kliknij <b>Zakończ</b>.
      <img src="/files/img/instalacja_4.jpg" /></li>
    <li>W okienku które się pojawi wypełnij pola tak jak na obrazku. W polu <b>Database Name</b> podaj ścieżkę do pliku z bazą na Twoim komputerze.<br>
        Aktualna baza znajduje się zawsze pod adresem <a href="http://pfs.org.pl/baza.zip">http://pfs.org.pl/baza.zip</a>.
        <img src="/files/img/instalacja_5.jpg" /></li>
    <li>W menu Start wybierz <b>Borland InterBase 2007 -> InterBase Server Manager</b>.</li>
    <li>W okienku które się pojawi zaptaszkuj checkboxa i kliknij przycisk <b>Start</b>.<img src="/files/img/instalacja_6.jpg" /></li>
    <li>Uruchom plik <b>scrabble.exe</b> - gotowe :)</li>
<ol>

<?require_once "files/php/bottom.php"?>
</body>
</html>
