<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Słownik : Oficjalna lista dozwolonych trzyliterówek z objaśnieniami</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" />
    <![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script>
    <![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script>
    <![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("scrabble","trojki");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
    <style type="text/css">
        .regular, .ui-tabs-panel.ui-widget-content {
            font-family: "Courier New";
        }
    </style>
</head>

<body>
<? require_once "files/php/menu.php"; ?>
<h1><script>naglowek("Lista słów trzyliterowych")</script></h1>

<div id="tabs" style="display:none;">
    <ul>
        <li><a href='#tabs-a'>A</a></li>
        <li><a href='#tabs-b'>B</a></li>
        <li><a href='#tabs-c'>C</a></li>
        <li><a href='#tabs-ć'>Ć</a></li>
        <li><a href='#tabs-d'>D</a></li>
        <li><a href='#tabs-e'>E</a></li>
        <li><a href='#tabs-ę'>Ę</a></li>
        <li><a href='#tabs-f'>F</a></li>
        <li><a href='#tabs-g'>G</a></li>
        <li><a href='#tabs-h'>H</a></li>
        <li><a href='#tabs-i'>I</a></li>
        <li><a href='#tabs-j'>J</a></li>
        <li><a href='#tabs-k'>K</a></li>
        <li><a href='#tabs-l'>L</a></li>
        <li><a href='#tabs-ł'>Ł</a></li>
        <li><a href='#tabs-m'>M</a></li>
        <li><a href='#tabs-n'>N</a></li>
        <li><a href='#tabs-o'>O</a></li>
        <li><a href='#tabs-ó'>Ó</a></li>
        <li><a href='#tabs-p'>P</a></li>
        <li><a href='#tabs-r'>R</a></li>
        <li><a href='#tabs-s'>S</a></li>
        <li><a href='#tabs-ś'>Ś</a></li>
        <li><a href='#tabs-t'>T</a></li>
        <li><a href='#tabs-u'>U</a></li>
        <li><a href='#tabs-w'>W</a></li>
        <li><a href='#tabs-y'>Y</a></li>
        <li><a href='#tabs-z'>Z</a></li>
        <li><a href='#tabs-ź'>Ź</a></li>
        <li><a href='#tabs-ż'>Ż</a></li>
        <li><a href='#tabs-wszystkie'>Wszystkie bez objaśnień</a></li>
    </ul>

<div id='tabs-a'>
<b>ABO</b> - <i>daw.</i> albo<br />
<b>ABY</b> - żeby (spójnik)<br />
<b>ACH</b> - wykrzyknik wyrażający zachwyt<br />
<b>ACZ</b> - aczkolwiek<br />
<b>AFT</b> - D.lm. od AFTA - małe bolesne owrzodzenie na błonie śluzowej jamy ustnej<br />
<b>AGA</b> - ropucha olbrzymia z Ameryki południowej, oficer turecki<br />
<b>AGĄ</b> - N.lp. od AGA<br />
<b>AGĘ</b> - B.lp. od AGA<br />
<b>AGF</b> - D.lm. od AGFA - produkt firmy AGFA<br />
<b>AGI</b> - M.lm. od AGA<br />
<b>AGO</b> - W.lp. od AGA<br />
<b>AHA</b> - wykrzyknik wyrażający zrozumienie<br />
<b>AIR</b> - aria, pieśń<br />
<b>AIS</b> - dźwięk a podwyższony o 1/2 tonu<br />
<b>AKR</b> - jednostka powierzchni gruntów w krajach anglosaskich równa 4840 jardom (ok. 4047 m2)<br />
<b>AKT</b> - część przedstawienia teatralnego<br />
<b>ALA</b> - oddział jazdy w starożytnym Rzymie<br />
<b>ALĄ</b> - N.lp. od ALA<br />
<b>ALB</b> - demon w mitologii germańskiej (ALP), D.lm. od ALBA - długa biała szata liturgiczna<br />
<b>ALD</b> - D.lm. od ALDA - książka pochodząca z oficyny Aldusa Manutiusa (XV-XVI w.)<br />
<b>ALE</b> - partykuła wyrażająca podziw<br />
<b>ALĘ</b> - B.lp. od ALA<br />
<b>ALF</b> - D.lm. od ALFA - nazwa greckiej litery α, Α<br />
<b>ALG</b> - D.lm. od ALGA - glon<br />
<b>ALI</b> - D.lp. od ALA<br />
<b>ALK</b> - D.lm. od ALKA - morski ptak<br />
<b>ALO</b> - W.lp. od ALA<br />
<b>ALP</b> - demon w mitologii germańskiej (ALB)<br />
<b>ALT</b> - niski głos kobiecy lub chłopięcy; osoba śpiewająca takim głosem<br />
<b>AMF</b> - D.lm. od AMFA - <i>pot.</i> amfetamina<br />
<b>ANA</b> - określenie na receptach "po równo"<br />
<b>ANI</b> - spójnik<br />
<b>ANO</b> - partykuła wzmacniająca<br />
<b>ANS</b> - D.lm. od ANSA - niechęć, uraza do kogoś<br />
<b>ANT</b> - D.lm. od ANTA - wysunięty na zewnątrz fragment ściany bocznej antycznej świątyni greckiej<br />
<b>ARA</b> - duża, jaskrawo upierzona papuga z Ameryki Południowej<br />
<b>ARĄ</b> - N.lp. od ARA<br />
<b>ARB</b> - D.lm. od ARBA - ciężki dwukołowy wóz używany na Krymie i Kaukazie<br />
<b>ARĘ</b> - B.lp. od ARA<br />
<b>ARF</b> - D.lm. od ARFA - przesiewacz piasku, żwiru<br />
<b>ARK</b> - D.lm. od ARKA - wielka łódź (arka Noego)<br />
<b>ARO</b> - W.lp. od ARA<br />
<b>ARS</b> - D.lm. od ARSA - w metryce antycznej: sylaba, na którą pada akcent<br />
<b>ARY</b> - M.lm. od ARA<br />
<b>ASA</b> - D.lp. od AS<br />
<b>ASY</b> - M.lm. od AS<br />
<b>ATA</b> - D.lp. od AT<br />
<b>ATU</b> - umownie najsilniejszy kolor w kartach<br />
<b>ATY</b> - M.lm. od AT<br />
<b>AUŁ</b> - osada górali kaukaskich<br />
<b>AUR</b> - D.lm. od AURA - pogoda<br />
<b>AUT</b> - w grach sportowych: wyjście piłki poza boisko (OUT), D.lm. od AUTO<br />
</div>
<div id='tabs-b'>
<b>BAB</b> - D.lm. od BABA - wielka kobieta<br />
<b>BAĆ</b> - (się) doznawać uczucia strachu<br />
<b>BAD</b> - uzdrowisko<br />
<b>BAI</b> - D.lp. od BAJA - gruba miękka tkanina bawełniana<br />
<b>BAJ</b> - ten, kto opowiada bajki, bajarz, D.lm. od BAJA, tr.rozk. od BAJAĆ - opowiadać bajki<br />
<b>BAK</b> - zbiornik na paliwo w samochodzie, D.lm. od BAKA - żeglarski naziemny znak nawigacyjny w postaci wieży lub stożka<br />
<b>BAL</b> - huczna zabawa taneczna, budulcowy pień drzewa<br />
<b>BAŁ</b> - 3.os.lp. cz.przeszłego od BAĆ się<br />
<b>BAM</b> - wykrzyknik naśladujący odgłos dzwonu<br />
<b>BAN</b> - średniowieczny zarządca banatu - nadgranicznego okręgu wojskowego na Węgrzech<br />
<b>BAŃ</b> - D.lm. od BANIA - dynia, D.lm. od BANIE - rzeczownik odsłowny od czasownika BAĆ się<br />
<b>BAR</b> - jadłodajnia; pierwiastek chemiczny; dawna jednostka ciśnienia<br />
<b>BAS</b> - najniższy głos męski; śpiewak o takim głosie<br />
<b>BAT</b> - rzemień; duża łódź rzeczna<br />
<b>BAW</b> - tr.rozk. od BAWIĆ<br />
<b>BAZ</b> - D.lm. od BAZA - podstawa, punkt oparcia<br />
<b>BĄK</b> - owad<br />
<b>BEE</b> - wyraz naśladujący beczenie owcy<br />
<b>BEG</b> - wyższy urzędnik w dawnej Turcji<br />
<b>BEJ</b> - wyższy urzędnik w dawnej Turcji<br />
<b>BEK</b> - głos owcy, daw. obrońca sportowy, D.lm. od BEKA - duża beczka<br />
<b>BEL</b> - jednostka fizyczna do określania natężenia dźwięku, D.lm. od BELA - zwój tkaniny<br />
<b>BEN</b> - "syn" w nazwiskach żydowskich i arabskich<br />
<b>BER</b> - gatunek trawy, D.lm. od BERA - odmiana gruszy o wydłużonych owocach<br />
<b>BET</b> - część pościeli, D.lm. od BETA - nazwa greckiej litery β, Β<br />
<b>BEZ</b> - krzew, D.lm. od BEZA - ciastko z ubitej z cukrem piany z białek, przyimek<br />
<b>BEŻ</b> - kolor piaskowy<br />
<b>BĘC</b> - wykrzyknik naśladujący odgłos uderzenia<br />
<b>BIB</b> - D.lm. od BIBA - przyjęcie z alkoholem<br />
<b>BIĆ</b> - uderzać<br />
<b>BID</b> - D.lm. od BIDA - dwukółka (wózek)<br />
<b>BIG</b> - D.lm. od BIGA - dwukółka<br />
<b>BIJ</b> - tr.rozk. od BIĆ<br />
<b>BIL</b> - kij do bilarda, D.lm. od BILA - kula bilardowa<br />
<b>BIŁ</b> - 3.os.lp. cz.przeszłego od BIĆ<br />
<b>BIM</b> - D.lm. od BIMA - podium z pulpitem usytuowane centralnie w głównej sali synagogi, odgłos bicia zegara<br />
<b>BIS</b> - powtórne wykonanie utworu<br />
<b>BIT</b> - najmniejsza jednostka informacji w telekomunikacji i informatyce<br />
<b>BIZ</b> - D.lm. od BIZA - skórzany pasek przy cholewce buta<br />
<b>BOA</b> - duży wąż z Ameryki Południowej (boa dusiciel)<br />
<b>BOB</b> - londyński policjant<br />
<b>BOĆ</b> - wzmocnione "bo"<br />
<b>BOD</b> - jednostka szybkości przesyłania informacji<br />
<b>BOI</b> - od BAĆ się, D.lp. od BOJA - pływający znak nawigacyjny<br />
<b>BOJ</b> - chłopiec hotelowy (boy)<br />
<b>BOK</b> - prawa lub lewa strona czegoś<br />
<b>BOL</b> - D.lm. od BOLA - długi rzemień z przywiązanymi na końcu owiniętymi w skórę kamieniami<br />
<b>BOM</b> - przyrząd gimnastyczny (pozioma belka drewniana, bum)<br />
<b>BON</b> - kupon na towary lub usługi, D.lm. od BONA - wychowawczyni małych dzieci w dawnych
zamożnych rodzinach<br />
<b>BOR</b> - pierwiastek chemiczny, D.lm. od BORA - zimny, suchy, gwałtowny wiatr wiejący zimą nad Adriatykiem<br />
<b>BOT</b> - damskie obuwie z wysoką cholewą<br />
<b>BOY</b> - chłopiec na posyłki w hotelu<br />
<b>BÓB</b> - roślina motylkowa<br />
<b>BÓG</b> - istota najwyższa, stworzyciel wszechświata<br />
<b>BÓJ</b> - starcie zbrojne, tr.rozk. od BAĆ się<br />
<b>BÓL</b> - zmysłowe wrażenie cierpienia, tr.rozk. od BOLEĆ<br />
<b>BÓR</b> - duży, gęsty, stary las<br />
<b>BRR</b> - okrzyk wstrętu<br />
<b>BRU</b> - D.lp. od BER<br />
<b>BRY</b> - M.lm. od BER<br />
<b>BUC</b> - człowiek zarozumiały<br />
<b>BUD</b> - D.lm. od BUDA - budynek, pomieszczenie dla sprzętów, zwierząt<br />
<b>BUF</b> - D.lm. od BUFA - poszerzona część rękawa<br />
<b>BUG</b> - błąd w programie lub systemie komputerowym<br />
<b>BUK</b> - gatunek drzewa<br />
<b>BUL</b> - tr.rozk. od BULIĆ - drogo płacić<br />
<b>BUŁ</b> - D.lm. od BUŁA - duża bułka<br />
<b>BUM</b> - przyrząd gimnastyczny (bom)<br />
<b>BUR</b> - D.lm. od BURA - ostre napomnienie, nagana<br />
<b>BUS</b> - mikrobus<br />
<b>BUT</b> - obuwie, D.lm. od BUTA - pycha, duma<br />
<b>BUZ</b> - D.lm. od BUZA - wschodni napój orzeźwiający z mąki owsianej lub prosa<br />
<b>BUŹ</b> - D.lm. od BUZIA - usta<br />
<b>BYĆ</b> - istnieć<br />
<b>BYK</b> - samiec krowy<br />
<b>BYŁ</b> - 3.os.lp. cz.przeszłego od BYĆ<br />
<b>BYM</b> - BY z końcówką osobową<br />
<b>BYŚ</b> - BY z końcówką osobową<br />
<b>BYT</b> - istnienie<br />
<b>BZU</b> - D.lp. od BEZ<br />
<b>BZY</b> - M.lm. od BEZ<br />
</div>
<div id='tabs-c'>
<b>CAB</b> - dawny konny pojazd dwukołowy<br />
<b>CAF</b> - klauzula umowy w handlu morskim (cif)<br />
<b>CAL</b> - jednostka długości używana w krajach anglosaskich, równa 2,54 cm<br />
<b>CAP</b> - stary baran lub kozioł<br />
<b>CAR</b> - dawny władca Rosji<br />
<b>CEK</b> - wada kafla polegająca na włoskowatych rysach w polewie<br />
<b>CEL</b> - to, do czego się dąży, D.lm. od CELA - izba więzienna<br />
<b>CEŁ</b> - D.lm. od CŁO<br />
<b>CEN</b> - D.lm. od CENA - wartość przedmiotu wyrażona w pieniądzu<br />
<b>CEŃ</b> - tr.rozk. od CENIĆ - przyznawać czemuś wartość<br />
<b>CEP</b> - narzędzie do ręcznego młócenia zboża<br />
<b>CER</b> - pierwiastek chemiczny, D.lm. od CERA - powierzchnia skóry twarzy<br />
<b>CES</b> - dźwięk c obniżony o pół tonu<br />
<b>CEW</b> - D.lm. od CEWA - bęben do nawijania liny, tr.rozk. od CEWIĆ - przewijać przędzę z motków w kopki wątkowe<br />
<b>CEZ</b> - pierwiastek chemiczny, D.lm. od CEZA - sieć do połowu ryb dennych<br />
<b>CHA</b> - odgłos śmiechu<br />
<b>CHE</b> - głośny śmiech z odcieniem złośliwości<br />
<b>CHI</b> - odgłos cichego śmiechu<br />
<b>CIE</b> - nazwa litery Ć<br />
<b>CIĘ</b> - B. zaimka TY<br />
<b>CIF</b> - klauzula umowy w handlu morskim (caf)<br />
<b>CIP</b> - D.lm. od CIPA - <i>wulg.</i> żeński narząd płciowy<br />
<b>CIS</b> - drzewo iglaste, dźwięk c podwyższony o pół tonu<br />
<b>CIŹ</b> - D.lm. od CIZIA - lekceważąco o młodej, ładnej dziewczynie<br />
<b>CIŻ</b> - M.lm. od zaimka osobowego TEN z partykułą wzmacniającą ŻE<br />
<b>CLĄ</b> - 3 os.lm. od CLIĆ - nakładać cło<br />
<b>CLE</b> - Ms.lp. od CŁO<br />
<b>CLĘ</b> - 1 os.lp. od CLIĆ - nakładać cło<br />
<b>CLI</b> - 3 os.lp. od CLIĆ - nakładać cło<br />
<b>CŁA</b> - D.lp. od CŁO<br />
<b>CŁO</b> - podatek od przewożenia towarów przez granicę<br />
<b>CŁU</b> - C.lp. od CŁO<br />
<b>CNA</b> - M.lp.rż. od CNY<br />
<b>CNĄ</b> - N.lp.rż. od CNY<br />
<b>CNE</b> - M.lm.rż. od CNY<br />
<b>CNI</b> - M.lm.rm. od CNY, 3 os.lp. od CNIĆ się - tęsknić<br />
<b>CNY</b> - szlachetny, cnotliwy<br />
<b>COB</b> - koń rasy anglo-normandzkiej<br />
<b>COD</b> - D.lm. od CODA - zakończenie kompozycji muzycznej (koda)<br />
<b>COM</b> - nazwa domeny internetowej (biznes)<br />
<b>COŚ</b> - zaimek nieokreślony<br />
<b>CÓR</b> - D.lm. od CÓRA - córka<br />
<b>CÓŻ</b> - wzmocniona partykuła co<br />
<b>CUĆ</b> - tr.rozk. od CUCIĆ - przywracać do przytomności<br />
<b>CUD</b> - zjawisko nadprzyrodzone<br />
<b>CUG</b> - zaprzęg z 4 lub 6 koni jednej maści zaprzężonych parami; koń w takim zaprzęgu<br />
<b>CUM</b> - D.lm. od CUMA - lina holownicza statku<br />
<b>CUP</b> - wyraz naśladujący odgłos uderzenia<br />
<b>CYC</b> - <i>wulg</i>. pierś kobieca<br />
<b>CYG</b> - D.lm. od CYGA - zabawka dziecięca (bąk)<br />
<b>CYK</b> - odgłos cykania zegara<br />
<b>CYM</b> - D.lm. od CYMA - profil w kształcie S<br />
<b>CYN</b> - D.lm. od CYNA - pierwiastek chemiczny<br />
<b>CYT</b> - wykrzyknik nakazujący zachowanie ciszy<br />
<b>CZE</b> - chińska jednostka miary<br />
<b>CZI</b> - chińska jednostka miary<br />
<b>CZY</b> - partykuła rozpoczynająca zdanie pytające<br />
</div>
<div id='tabs-ć'>
<b>ĆMA</b> - motyl nocny<br />
<b>ĆMĄ</b> - N.lp. od ĆMA<br />
<b>ĆMĘ</b> - B.lp. od ĆMA<br />
<b>ĆMI</b> - 3.os.lp. od ĆMIĆ - palić fajkę<br />
<b>ĆMO</b> - W.lp. od ĆMA<br />
<b>ĆMY</b> - M.lm. od ĆMA<br />
<b>ĆPA</b> - 3.os.lp. od ĆPAĆ - brać narkotyki<br />
</div>
<div id='tabs-d'>
<b>DAĆ</b> - przekazać coś na własność<br />
<b>DAG</b> - D.lm. od DAGA - krótki sztylet o szerokiej klindze zwężającej się ku końcowi<br />
<b>DAJ</b> - tr.rozk. od DAĆ<br />
<b>DAL</b> - odległa przestrzeń<br />
<b>DAŁ</b> - 3.os.lp. cz.przeszłego od DAĆ<br />
<b>DAM</b> - D.lm. od DAMA - elegancka kobieta, 1.os.lp. od DAĆ<br />
<b>DAN</b> - każdy z 10 stopni mistrzowskich w dalekowschodnich sztukach walki<br />
<b>DAŃ</b> - danina, D.lm. od DANIE - potrawa podawana na stół<br />
<b>DAO</b> - w daoizmie - bezosobowe bóstwo, ład kosmiczny, zbiór praw rządzących przyrodą (TAO)<br />
<b>DAR</b> - upominek<br />
<b>DAT</b> - D.lm. od DATA - termin kalendarzowy jakiegoś zdarzenia<br />
<b>DĄB</b> - gatunek drzewa<br />
<b>DĄĆ</b> - dmuchać, wiać<br />
<b>DĄŁ</b> - 3.os.lp. cz.przeszłego od DĄĆ<br />
<b>DĄS</b> - kaprys, grymas<br />
<b>DĄŻ</b> - tr.rozk. od DĄŻYĆ - zmierzać w określonym celu<br />
<b>DBA</b> - 3.os.lp. od DBAĆ - troszczyć się o coś<br />
<b>DEJ</b> - oficer w dawnym tureckim wojsku janczarskim<br />
<b>DEK</b> - pokład statku, D.lm. od DEKA - przykrycie, pokrowiec<br />
<b>DEM</b> - podstawowa jednostka administracyjna w starożytnej Grecji (gmina), D.lm. od DEMO - program komputerowy<br />
<b>DEN</b> - D.lm. od DNA, D.lm. od DNO<br />
<b>DER</b> - D.lm. od DERA - gruby koc<br />
<b>DES</b> - dźwięk d obniżony o pół tonu, D.lm. od DESA - sklep z antykami<br />
<b>DĘB</b> - tr.rozk. od DĘBIĆ - zdejmować korę z dębu<br />
<b>DĘĆ</b> - D.lm. od DĘCIE - rzeczownik odsłowny od czasownika DĄĆ<br />
<b>DĘG</b> - D.lm. od DĘGA - sztaba u wierzei<br />
<b>DIP</b> - gęsty zimny sos na bazie jogurtu lub śmietany jako dodatek do potraw<br />
<b>DIS</b> - w mitologii germańskiej: demon śmierci, zniszczenia i wojny, dźwięk d podwyższony o pół tonu<br />
<b>DIW</b> - D.lm. od DIWA - słynna śpiewaczka operowa<br />
<b>DLA</b> - przyimek<br />
<b>DMĄ</b> - 3.os.lm. od DĄĆ<br />
<b>DMĘ</b> - 1.os.lp. od DĄĆ<br />
<b>DNA</b> - choroba (podagra, skaza moczanowa), D.lp. od DNO<br />
<b>DNĄ</b> - N.lp. od DNA<br />
<b>DNĘ</b> - B.lp. od DNA<br />
<b>DNI</b> - D.lm. od DZIEŃ<br />
<b>DNO</b> - spód czegoś<br />
<b>DNU</b> - C.lp. od DNO<br />
<b>DNY</b> - M.lm. od DNA<br />
<b>DOC</b> - format pliku<br />
<b>DOG</b> - rasa psa<br />
<b>DOI</b> - 3.os.lp. od DOIĆ - wyciskać mleko z wymion (krowy, kozy)<br />
<b>DOK</b> - basen portowy, w którym buduje się i naprawia statki<br />
<b>DOL</b> - D.lm. od DOLA - los, położenie, stan<br />
<b>DOM</b> - budynek mieszkalny<br />
<b>DON</b> - tytuł grzecznościowy używany w Hiszpanii, Portugalii i we Włoszech<br />
<b>DOŃ</b> - <i>książk.</i> do niego<br />
<b>DOZ</b> - D.lm. od DOZA - dawka leku, surowca<br />
<b>DÓB</b> - D.lm. od DOBA - 24 godziny<br />
<b>DÓJ</b> - dojenie, tr.rozk. od DOIĆ - wyciskać mleko z wymion (krowy, kozy)<br />
<b>DÓL</b> - D.lm. od DOLA - los, położenie, stan<br />
<b>DÓŁ</b> - otwór wykopany w ziemi<br />
<b>DÓZ</b> - D.lm. od DOZA - dawka leku, surowca<br />
<b>DRĄ</b> - 3.os.lm. od DRZEĆ - rwać na kawałki<br />
<b>DRĘ</b> - 1.os.lp. od DRZEĆ - rwać na kawałki<br />
<b>DUD</b> - D.lm. od DUDA - starodawny ludowy instrument muzyczny<br />
<b>DUG</b> - D.lm. od DUGA - drewniany kabłąk w zaprzęgu mocujący gązwy chomąta do hołobli<br />
<b>DUH</b> - D.lm. od DUGA - drewniany kabłąk w zaprzęgu mocujący gązwy chomąta do hołobli<br />
<b>DUM</b> - D.lm. od DUMA - poczucie osobistej godności<br />
<b>DUO</b> - duet<br />
<b>DUP</b> - D.lp. od DUPA - pośladki, tyłek<br />
<b>DUR</b> - ostra choroba zakaźna (tyfus)<br />
<b>DUŚ</b> - tr.rozk. od DUSIĆ - ściskać za gardło<br />
<b>DWA</b> - liczebnik<br />
<b>DWU</b> - C.lp. od DWA<br />
<b>DYB</b> - D.lm. od DYBY - narzędzie tortur (deski z otworami na ręce i nogi więźnia), tr.rozk. od DYBAĆ - czyhać, czatować na kogoś<br />
<b>DYG</b> - lekki ukłon<br />
<b>DYL</b> - pień drzewny<br />
<b>DYM</b> - lotne produkty spalania, tr.rozk. od DYMIĆ - wydzielać dym<br />
<b>DYN</b> - jednostka siły, D.lm. od DYNA - jednostka siły<br />
<b>DYŃ</b> - D.lm. od DYNIA - roślina warzywna<br />
<b>DYZ</b> - D.lm. od DYZA - dysza (przewód dla pary, gazów i cieczy)<br />
</div>
<div id='tabs-e'>
<b>ECH</b> - D.lm. od ECHO - powtórzenie dźwięku<br />
<b>ECU</b> - jednostka monetarna przyjęta przez kraje EWG, w 1999 zastąpiona przez euro<br />
<b>EDU</b> - nazwa domeny internetowej (edukacja)<br />
<b>EGO</b> - w psychoanalizie: świadoma część osobowości (jaźń)<br />
<b>EHE</b> - wykrzyknik wyrażający wyrzut, dezaprobatę<br />
<b>EIS</b> - dźwięk e podwyższony o pół tonu<br />
<b>ELF</b> - w mitologii germańskiej: duszek, istota pośrednia między człowiekiem i bóstwem<br />
<b>EMU</b> - duży ptak nielotny z Australii (podobny do strusia)<br />
<b>END</b> - klawisz<br />
<b>EON</b> - wiek, era; istota będąca uosobieniem sił i atrybutów Boga (w gnostycyzmie)<br />
<b>ERA</b> - okres zapoczątkowany doniosłym wydarzeniem<br />
<b>ERĄ</b> - N.lp. od ERA<br />
<b>ERB</b> - pierwiastek chemiczny<br />
<b>ERĘ</b> - B.lp. od ERA<br />
<b>ERG</b> - dawna jednostka pracy i energii; pustynia piaszczysta w Afryce Północnej<br />
<b>ERO</b> - W.lp. od ERA<br />
<b>ERY</b> - M.lm. od ERA<br />
<b>ESU</b> - D.lp. od ES - zawijas<br />
<b>ESY</b> - M.lm. od ES<br />
<b>ETA</b> - nazwa greckiej litery η, Η<br />
<b>ETĄ</b> - N.lp. od ETA<br />
<b>ETĘ</b> - B.lp. od ETA<br />
<b>ETO</b> - W.lp. od ETA<br />
<b>ETY</b> - M.lm. od ETA<br />
<b>EWA</b> - <i>książk.</i> o każdej kobiecie (np. kwiatek dla ewy)<br />
<b>EWĄ</b> - N.lp. od EWA<br />
<b>EWE</b> - język afrykańskich plemion Ewe (Ghana, Togo, Benin)<br />
<b>EWĘ</b> - B.lp. od EWA<br />
<b>EWO</b> - W.lp. od EWA<br />
<b>EWY</b> - M.lm. od EWA<br />
</div>
<div id='tabs-ę'>
<b>ĘSI</b> - okrzyk dziecka zawiadamiający o potrzebie naturalnej<br />
</div>
<div id='tabs-f'>
<b>FAG</b> - bakteriofag (wirus mający zdolność niszczenia bakterii)<br />
<b>FAI</b> - D.lp. od FAJA - duża fajka<br />
<b>FAJ</b> - D.lm. od FAJA - duża fajka<br />
<b>FAL</b> - D.lm. od FALA - wzniesienie wody<br />
<b>FAŁ</b> - lina do podnoszenia żagla<br />
<b>FAM</b> - D.lm. od FAMA - wieść, pogłoska<br />
<b>FAN</b> - entuzjasta, wielbiciel zespołu muzycznego, sportowca itp.<br />
<b>FAR</b> - D.lm. od FARA - kościół parafialny<br />
<b>FAS</b> - klauzula umowy w handlu morskim, D.lm. od FASA - duża beczka do przechowywania produktów spożywczych<br />
<b>FAZ</b> - D.lm. od FAZA - okres, stadium, stan przejściowy<br />
<b>FEN</b> - porywisty wiatr wiejący w Alpach od gór ku dolinom; moneta chińska - 1/100 juana<br />
<b>FES</b> - dźwięk f obniżony o pół tonu<br />
<b>FET</b> - D.lm. od FETA - uroczystość, uczta<br />
<b>FEZ</b> - muzułmańskie nakrycie głowy z czerwonego filcu w kształcie ściętego stożka<br />
<b>FIG</b> - D.lm. od FIGA - owoc figowca<br />
<b>FIK</b> - wykrzyknienie na oznaczenie upadku, fiknięcie<br />
<b>FIL</b> - D.lm. od FILA - dzielnica starożytnych Aten<br />
<b>FIN</b> - jacht klasy Finn<br />
<b>FIS</b> - dźwięk f podwyższony o pół tonu<br />
<b>FIŚ</b> - mania, bzik<br />
<b>FIT</b> - koń dobrze przygotowany do wyścigu; w brydżu - uzupełnienie do 8 kart w kolorze, D.lm. od FITA - przyrząd do pomiaru średnicy drzew<br />
<b>FIU</b> - wykrzyknik naśladujący gwizd<br />
<b>FOB</b> - klauzula umowy w handlu morskim<br />
<b>FOK</b> - pierwszy od dołu żagiel na fokmaszcie (maszcie przednim), D.lm. od FOKA - zwierzę morskie<br />
<b>FOL</b> - D.lm. od FOLA - sieć do łowienia jesiotra<br />
<b>FON</b> - jednostka poziomu głośności<br />
<b>FOR</b> - przewaga przyznana słabszemu przeciwnikowi dla wyrównania szans<br />
<b>FOS</b> - D.lm. od FOSA - obronny rów z wodą<br />
<b>FOT</b> - jednostka natężenia oświetlenia powierzchni<br />
<b>FRR</b> - wyraz naśladujący furkot skrzydeł<br />
<b>FRU</b> - wyraz naśladujący furkot skrzydeł<br />
<b>FUG</b> - D.lm. od FUGA - szczelina między ułożonymi płytkami ceramicznymi, wypełniona zaprawą<br />
<b>FUJ</b> - okrzyk odrazy, wstrętu<br />
<b>FUL</b> - układ kart w pokerze (np. 3 walety i 2 asy)<br />
<b>FUM</b> - D.lm. od FUMA - dąs, grymas, próżność<br />
<b>FUR</b> - D.lm. od FURA - wóz do przewożenia ciężarów<br />
<b>FUS</b> - osad jakiegoś płynu opadły na dno<br />
</div>
<div id='tabs-g'>
<b>GAĆ</b> - materiał służący do gacenia, tr.rozk. od GACIĆ - uszczelniać budynek prze okładanie ścian mchem, słomą<br />
<b>GAD</b> - zwierzę<br />
<b>GAF</b> - ukośne ruchome drzewce służące do podnoszenia żagla, D.lm. od GAFA - nietakt<br />
<b>GAG</b> - dowcip sytuacyjny<br />
<b>GAI</b> - D.lp. od GAJA - lina do obracania bomu ładowniczego, 3.os.lp. od GAIĆ - ubierać zielenią, maić<br />
<b>GAJ</b> - niewielki las, tr.rozk. od GAIĆ<br />
<b>GAL</b> - pierwiastek chemiczny<br />
<b>GAŁ</b> - D.lm. od GAŁA - duża gałka<br />
<b>GAM</b> - D.lm. od GAMA - szereg dźwięków<br />
<b>GAŃ</b> - tr.rozk. od GANIĆ - krytykować<br />
<b>GAP</b> - człowiek przypatrujący się czemuś bezmyślnie, D.lm. od GAPA - fujara, oferma, tr.rozk. od GAPIĆ się - przyglądać się<br />
<b>GAR</b> - duży garnek, D.lm. od GARA - rośnięcie ciasta<br />
<b>GAŚ</b> - tr.rozk. od GASIĆ - tłumić palenie<br />
<b>GAY</b> - homoseksualista (gej)<br />
<b>GAZ</b> - substancja lotna, D.lm. od GAZA - płótno opatrunkowe<br />
<b>GAŻ</b> - D.lm. od GAŻA - uposażenie aktora<br />
<b>GĄB</b> - D.lm. od GĘBA - twarz<br />
<b>GDY</b> - spójnik<br />
<b>GEJ</b> - homoseksualista<br />
<b>GEM</b> - część seta w tenisie<br />
<b>GEN</b> - odcinek DNA będący jednostką dziedziczenia<br />
<b>GES</b> - dźwięk g obniżony o pół tonu<br />
<b>GEZ</b> - uczestnik holenderskiego ruchu oporu podczas II wojny światowej, D.lm. od GEZA - skała osadowa<br />
<b>GĘB</b> - D.lm. od GĘBA - twarz<br />
<b>GĘG</b> - głos wydawany przez gęsi<br />
<b>GĘŚ</b> - ptak domowy<br />
<b>GHI</b> - indyjskie masło z mleka bawolic<br />
<b>GIB</b> - tr.rozk. od GIBAĆ się - zginać, wyginać się<br />
<b>GID</b> - osoba oprowadzająca turystów, przewodnik<br />
<b>GIE</b> - nazwa litery G<br />
<b>GIF</b> - obrazek komputerowy<br />
<b>GIG</b> - jednokonny powozik dwukołowy używany w XIX w. w Anglii, D.lm. od GIGA - ludowy taniec staroangielski<br />
<b>GIK</b> - drzewce przytwierdzone jednym końcem do masztu, drugim końcem zawieszone na linie<br />
<b>GIL</b> - gatunek ptaka<br />
<b>GIN</b> - wódka o smaku i zapachu jałowcowym<br />
<b>GIŃ</b> - tr.rozk. od GINĄĆ - umierać<br />
<b>GIR</b> - D.lm. od GIRA - noga<br />
<b>GIS</b> - dźwięk g podwyższony o pół tonu<br />
<b>GIT</b> - członek polskiej subkultury młodzieżowej lat siedemdziesiątych<br />
<b>GNA</b> - 3.os.lp. od GNAĆ - pędzić<br />
<b>GNĄ</b> - 3.os.lm. od GIĄĆ - zginać<br />
<b>GNĘ</b> - 1.os.lp. od GIĄĆ - zginać<br />
<b>GNU</b> - antylopa ze wschodniej Afryki<br />
<b>GOI</b> - 3.os.lp. od GOIĆ - powodować zabliźnienie ran<br />
<b>GOJ</b> - żydowskie określenie innowiercy, zwłaszcza chrześcijanina<br />
<b>GOL</b> - zdobycie bramki w grach zespołowych, tr.rozk. od GOLIĆ - usuwać zarost<br />
<b>GON</b> - ściganie zwierzyny przez psy gończe<br />
<b>GOŃ</b> - tr.rozk. od GONIĆ - biec za kimś<br />
<b>GÓJ</b> - tr.rozk. od GOIĆ - powodować zabliźnienie ran<br />
<b>GÓL</b> - tr.rozk. od GOLIĆ - usuwać zarost<br />
<b>GÓR</b> - D.lm. od GÓRA - duże wzniesienie terenu<br />
<b>GRA</b> - zabawa towarzyska, 3.os.lp. od GRAĆ - brać udział w grach<br />
<b>GRĄ</b> - N.lp. od GRA<br />
<b>GRĘ</b> - B.lp. od GRA<br />
<b>GRO</b> - W.lp. od GRA<br />
<b>GRY</b> - M.lm. od GRA<br />
<b>GUB</b> - tr.rozk. od GUBIĆ - tracić<br />
<b>GUL</b> - u ludów muzułmańskich: demon, który pożera ludzi i zwłoki na cmentarzach, D.lm. od GULA - narośl, guz<br />
<b>GUM</b> - D.lm. od GUMA - produkt z kauczuku<br />
<b>GUŃ</b> - D.lm. od GUNIA - ciepłe okrycie wierzchnie górali<br />
<b>GUR</b> - kolorowa bawełna używana na Podlasiu do przetykania białego lnianego płótna<br />
<b>GUZ</b> - obrzmienie<br />
<b>GZA</b> - gżenie się (ruja), D.lp. od GIEZ - owad<br />
<b>GZĄ</b> - N.lp. od GZA<br />
<b>GZĘ</b> - B.lp. od GZA<br />
<b>GZI</b> - 3.os.lp. od GZIĆ się - o niektórych zwierzętach: zaspokajać popęd płciowy<br />
<b>GZO</b> - W.lp. od GZA<br />
<b>GZY</b> - M.lm. od GZA i GIEZ<br />
<b>GŻĄ</b> - 3.os.lm. od GZIĆ się - o niektórych zwierzętach: zaspokajać popęd płciowy<br />
<b>GŻĘ</b> - 1.os.lp. od GZIĆ się - o niektórych zwierzętach: zaspokajać popęd płciowy<br />
</div>
<div id='tabs-h'>
<b>HAF</b> - zatoka na południowym wybrzeżu Bałtyku<br />
<b>HAI</b> - D.lm. od HAJ<br />
<b>HAJ</b> - stan euforii pod wpływem narkotyków<br />
<b>HAK</b> - metalowy pręt do zawieszania czegoś<br />
<b>HAL</b> - D.lm. od HALA - wielka sala<br />
<b>HAN</b> - pierwiastek chemiczny<br />
<b>HAO</b> - moneta wietnamska - 1/10 donga<br />
<b>HAS</b> - pierwiastek chemiczny<br />
<b>HAU</b> - wyraz naśladujący szczekanie psa<br />
<b>HEC</b> - D.lm. od HECA - awantura<br />
<b>HEJ</b> - okrzyk używany jako przywołanie<br />
<b>HEL</b> - pierwiastek chemiczny<br />
<b>HEM</b> - barwny składnik krwi<br />
<b>HEN</b> - <i>przestarz.</i> gdzieś, daleko<br />
<b>HEP</b> - tr.rozk. od HEPAĆ - czkać<br />
<b>HER</b> - D.lm. od HERA - heroina<br />
<b>HET</b> - <i>przestarz.</i> daleko<br />
<b>HIF</b> - <i>pot.</i> wirus HIV<br />
<b>HIP</b> - wykrzyknik (hip, hip, hurra)<br />
<b>HIS</b> - dźwięk h podwyższony o pół tonu<br />
<b>HIT</b> - przebój, szlagier<br />
<b>HIW</b> - część ładunku podnoszona jednorazowo przez dźwig<br />
<b>HOC</b> - wykrzyknik wyrażający wesołość<br />
<b>HOI</b> - D.lp. od HOJA, HOYA - ozdobna roślina doniczkowa<br />
<b>HOJ</b> - D.lm. od HOJA, HOYA - ozdobna roślina doniczkowa<br />
<b>HOL</b> - przedpokój<br />
<b>HOP</b> - wykrzyknik zachęcający do skoku<br />
<b>HOR</b> - D.lm. od HORA - rumuński taniec ludowy, w którym tańczący tworzą koło, trzymając się za ręce<br />
<b>HOT</b> - w jazzie: wzmożona ekspresja wykonania<br />
<b>HOY</b> - D.lm. od HOYA - ozdobna roślina doniczkowa<br />
<b>HUB</b> - D.lm. od HUBA - grzyb pasożytujący na drzewach<br />
<b>HUF</b> - w średniowieczu: zwarty oddział wojska składający się z kilku chorągwi<br />
<b>HUJ</b> - <i>wulg.</i> męski członek<br />
<b>HUK</b> - odgłos wybuchu<br />
<b>HUN</b> - D.lm. od HUNA - tradycyjna hawajska wiedza ezoteryczna<br />
<b>HUT</b> - D.lm. od HUTA - zakład produkujący metale z rud<br />
<b>HYC</b> - okrzyk towarzyszący skokowi<br />
<b>HYR</b> - pogłoska, opinia, plotka<br />
<b>HYŚ</b> - mania, bzik (hyź)<br />
<b>HYZ</b> - wiatr wiejący w żagle z tyłu pod kątem prostym<br />
<b>HYŹ</b> - mania, bzik (hyś)<br />
</div>
<div id='tabs-i'>
<b>IBN</b> - "syn" w imionach arabskich<br />
<b>ICH</b> - D. zaimka ONI<br />
<b>IDĄ</b> - 3.os.lm. od IŚĆ<br />
<b>IDĘ</b> - 1.os.lp. od IŚĆ<br />
<b>IDO</b> - sztuczny język międzynarodowy utworzony z esperanta<br />
<b>IDU</b> - D.lp. od ID - jedna ze sfer osobowości<br />
<b>IDY</b> - M.lm. od ID - jedna ze sfer osobowości, w kalendarzu rzymskim: 15 dzień marca, maja, lipca i października, w pozostałych miesiącach - dzień 13<br />
<b>IDŹ</b> - tryb rozkazujący od IŚĆ<br />
<b>IGR</b> - D.lm. od IGRA - gra, zabawa<br />
<b>III</b> - wykrzyknik oznaczający lekceważenie<br />
<b>IKR</b> - D.lm. od IKRA - rybie jaja<br />
<b>IKS</b> - nazwa litery X<br />
<b>IKT</b> - akcent metryczny w wierszu iloczasowym, padający na mocną sylabę w stopie<br />
<b>ILE</b> - zaimek pytajny<br />
<b>ILU</b> - zaimek pytajny<br />
<b>IŁA</b> - D.lp. od IŁ - samolot Ił<br />
<b>IŁU</b> - D.lp. od IŁ - skała osadowa<br />
<b>IŁY</b> - D.lm. od IŁ<br />
<b>IMA</b> - 3.os.lp. od IMAĆ - chwytać, brać<br />
<b>IMĄ</b> - 3.os.lm. od JĄĆ<br />
<b>IMĆ</b> - dawny tytuł grzecznościowy (skrót zwrotu <i>jego miłość, jegomość</i>)<br />
<b>IMĘ</b> - 1.os.lp. od JĄĆ<br />
<b>IND</b> - pierwiastek chemiczny<br />
<b>INR</b> - D.lm. od INRO - niewielka japońska drewniana szkatułka na lekarstwa, bogato zdobiona<br />
<b>IRG</b> - D.lm. od IRGA - krzew różowaty<br />
<b>ISK</b> - grupa pszczół wylatujących z ula w celu wyszukania osiedla dla nowego roju<br />
<b>IŚĆ</b> - posuwać się stawiając kroki, tr. rozk. od IŚCIĆ<br />
<b>ITR</b> - pierwiastek chemiczny<br />
<b>IWA</b> - wierzba palmowa<br />
<b>IWĄ</b> - N.lp. od IWA<br />
<b>IWĘ</b> - B.lp. od IWA<br />
<b>IWO</b> - W.lp. od IWA<br />
<b>IWY</b> - M.lm. od IWA<br />
<b>IZB</b> - D.lm. od IZBA - pomieszczenie<br />
<b>IZM</b> - ironiczna nazwa doktryn, teorii, określanych terminami kończącymi się na -izm<br />
</div>
<div id='tabs-j'>
<b>JAD</b> - wydzielina toksyczna zawarta w gruczołach żmij, pszczół<br />
<b>JAG</b> - D.lm. od JAGA - wiedźma<br />
<b>JAJ</b> - D.lm. od JAJO, JAJE - jajko<br />
<b>JAK</b> - zaimek, zwierzę krętorogie z Tybetu, D.lm. od JAKA - krótkie okrycie wierzchnie<br />
<b>JAM</b> - dżem, D.lm. od JAMA - nora<br />
<b>JAP</b> - D.lm. od JAPA - twarz<br />
<b>JAR</b> - wąwóz, parów<br />
<b>JAŚ</b> - odmiana fasoli<br />
<b>JAT</b> - D.lm. od JATA - szałas, buda, szopa<br />
<b>JAW</b> - stan świadomego reagowania na bodźce (JAWA), D.lm. od JAWA - motocykl marki Jawa, tr.rozk. od JAWIĆ się - ukazywać się<br />
<b>JAZ</b> - zapora na rzece<br />
<b>JAŹ</b> - gatunek ryby<br />
<b>JĄĆ</b> - zacząć, począć<br />
<b>JĄŁ</b> - 3.os.lp. cz.przeszłego od JĄĆ<br />
<b>JEB</b> - tr.rozk. od JEBAĆ - <i>wulg.</i> uprawiać seks<br />
<b>JEJ</b> - D. zaimka ONA<br />
<b>JEM</b> - 1.os.lp. od JEŚĆ<br />
<b>JEN</b> - waluta Japonii, D.lm. od JENA - szkło jenajskie<br />
<b>JER</b> - odmiana zięby; półsamogłoska w językach prasłowiańskich (jor)<br />
<b>JEŻ</b> - zwierzę, tr.rozk. od JEŻYĆ - stroszyć<br />
<b>JĘĆ</b> - D.lm. od JĘCIE - rzeczownik odsłowny od JĄĆ<br />
<b>JĘK</b> - żałosny głos<br />
<b>JĘT</b> - D.lm. od JĘTA - belka, na której opierają się krokwie dachu<br />
<b>JIG</b> - taniec irlandzki<br />
<b>JIN</b> - w chińskiej filozofii klasycznej: żeński, negatywny pierwiastek
zawarty w przyrodzie (YIN)<br />
<b>JOD</b> - pierwiastek chemiczny<br />
<b>JOG</b> - hinduski asceta, praktyk jogi, D.lm. od JOGA - jeden z 6 systemów indyjskiej filozofii<br />
<b>JOJ</b> - D.lm. od JOJO - zabawka na sznurku<br />
<b>JOL</b> - żaglowiec dwumasztowy z ożaglowaniem skośnym, D.lm. od JOLA - mały jacht żaglowy<br />
<b>JON</b> - atom obdarzony ładunkiem elektrycznym<br />
<b>JOR</b> - półsamogłoska (jer)<br />
<b>JOT</b> - D.lm. od JOTA - nazwa greckiej litery ι, Ι<br />
<b>JUG</b> - D.lm. od JUGA - w hinduizmie: każdy o okresów czteroczęściowego cyklu kosmicznego<br />
<b>JUK</b> - skórzany worek na grzbiecie zwierzęcia jucznego, D.lm. od JUKA - roślina włóknista<br />
<b>JUN</b> - moneta północnokoreańska - 1/100 wona<br />
<b>JUR</b> - D.lm. od JURA - drugi okres ery mezozoicznej<br />
<b>JUS</b> - w cyrylicy: nazwa litery odpowiadającej samogłosce ą i ę<br />
<b>JUT</b> - pokład rufy okrętu wzniesiony nad poziom ogólny pokładu, D.lm. od JUTA - roślina włóknista<br />
<b>JUZ</b> - aparat telegraficzny z klawiaturą, prawzór dalekopisu wynaleziony w 1885 r.<br />
<b>JUŻ</b> - wyraz uwydatniający granicę w czasie<br />
</div>
<div id='tabs-k'>
<b>KAC</b> - złe samopoczucie po wypiciu większej ilości alkoholu<br />
<b>KAŁ</b> - odchody<br />
<b>KAM</b> - D.lm. od KAMA - antylopa z południowej Afryki<br />
<b>KAN</b> - D.lm. od KANA - bańka na mleko<br />
<b>KAŃ</b> - D.lm. od KANIA - ptak<br />
<b>KAP</b> - D.lm. od KAPA - narzuta, D.lm. od KAPO - więzień pełniący funkcję dozorcy innych więźniów w obozach hitlerowskich, tr.rozk. od KAPAĆ - spadać kroplami<br />
<b>KAR</b> - kotlina polodowcowa, D.lm. od KARA - represja, D.lm. od KARO - jeden z 4 kolorów w kartach, oznaczony czerwonym rombem<br />
<b>KAS</b> - D.lm. od KASA - skrzynka na pieniądze<br />
<b>KAT</b> - wykonawca wyroków śmierci, D.lm. od KATA - krzew z Bliskiego Wschodu, o narkotyzujących liściach<br />
<b>KAW</b> - D.lm. od KAWA - napój<br />
<b>KAŹ</b> - tr.rozk. od KAZIĆ - psuć, szpecić<br />
<b>KAŻ</b> - tr.rozk. od KAZAĆ - wydawać rozkaz<br />
<b>KĄP</b> - tr.rozk. od KĄPAĆ - myć w wodzie<br />
<b>KĄT</b> - przestrzeń między stykającymi się ścianami<br />
<b>KEI</b> - D.lp. od KEJA - nabrzeże portowe wyposażone w urządzenia cumownicze<br />
<b>KEJ</b> - D.lm. od KEJA - nabrzeże portowe wyposażone w urządzenia cumownicze<br />
<b>KEK</b> - użytkowe odpady siarkowe<br />
<b>KEM</b> - pagórek lub wał polodowcowy<br />
<b>KER</b> - polski kauczuk syntetyczny<br />
<b>KET</b> - jednomasztowy statek żaglowy, D.lm. od KETA - łosoś z Morza Arktycznego<br />
<b>KĘP</b> - D.lm. od KĘPA - skupisko krzewów<br />
<b>KĘS</b> - kawałek czegoś jadalnego<br />
<b>KIA</b> - samochód marki Kia<br />
<b>KIĄ</b> - N.lp. od KIA<br />
<b>KIC</b> - skok zająca<br />
<b>KIĆ</b> - więzienie, D.lm. od KICIA - pieszczotliwie o kocie<br />
<b>KIE</b> - M.lm. od KIA<br />
<b>KIĘ</b> - B.lp. od KIA<br />
<b>KII</b> - D.lp. od KIA<br />
<b>KIJ</b> - patyk, drąg, D.lm. od KIA<br />
<b>KIL</b> - belka na dnie kadłuba statku, do dziobu do rufy, zapewniająca sztywność i stabilność konstrukcji<br />
<b>KIŁ</b> - D.lm. od KIŁA - choroba weneryczna<br />
<b>KIM</b> - zaimek (KTO)<br />
<b>KIN</b> - chiński instrument muzyczny, D.lm. od KINO - miejce projekcji filmów, D.lm. od KINA - waluta Papui Nowej Gwinei<br />
<b>KIO</b> - W.lp. od KIA<br />
<b>KIP</b> - waluta Laosu, D.lm. od KIPA - kadź farbiarska, tr.rozk. od KIPIEĆ - wrzeć<br />
<b>KIR</b> - czarny materiał symbolizujący żałobę<br />
<b>KIŚ</b> - tr.rozk. od KISIĆ - poddawać fermentacji<br />
<b>KIT</b> - masa do mocowania szyb w ramach, D.lm. od KITA - lisi ogon<br />
<b>KIŹ</b> - D.lm. od KIZIA - pieszczotliwie o kocie<br />
<b>KLE</b> - Ms.lp. od KIEŁ - ząb<br />
<b>KLO</b> - ubikacja, klozet<br />
<b>KŁA</b> - D.lp. od KIEŁ - ząb<br />
<b>KŁY</b> - M.lm. od KIEŁ - ząb<br />
<b>KOC</b> - płat tkaniny jako przykrycie<br />
<b>KOĆ</b> - tr.rozk. od KOCIĆ się - wydawać potomstwo (o kotach, zającach, kozach, owcach)<br />
<b>KOD</b> - system umownych znaków, D.lm. od KODA - zakończenie kompozycji muzycznej<br />
<b>KOG</b> - dawny żaglowiec, D.lm. od KOGA - drewniany żaglowiec handlowy<br />
<b>KOI</b> - D.lp. od KOJA - łóżko na statku, 3.os.lp. od KOIĆ - uspokajać<br />
<b>KOK</b> - włosy upięte w węzeł; kucharz na statku, D.lm. od KOKA - krzew kokainowy<br />
<b>KOL</b> - D.lm. od KOLO - taniec serbski tańczony w półokręgu, tr.rozk. od KŁUĆ - przebijać czymś spiczastym<br />
<b>KOM</b> - D.lm. od KOMA - śpiączka<br />
<b>KOŃ</b> - zwierzę<br />
<b>KOP</b> - kopniak, D.lm. od KOPA - 60 sztuk czegoś, tr.rozk. od KOPAĆ - uderzać nogą, tr.rozk. od KOPIĆ - składać siano w kopy<br />
<b>KOR</b> - D.lm. od KORA - tkanka okrywająca pień drzewa<br />
<b>KOS</b> - ptak, D.lm. od KOSA - narzędzie do ścinania zboża<br />
<b>KOŚ</b> - tr.rozk. od KOSIĆ - ścinać zboże<br />
<b>KOT</b> - zwierzę, D.lm. od KOTA - liczba na mapie oznaczająca wysokość punktu nad poziomem morza<br />
<b>KÓJ</b> - tr.rozk. od KOIĆ - uspokajać<br />
<b>KÓŁ</b> - pal zaostrzony na końcu, żerdź, D.lm. od KOŁO - część płaszczyzny ograniczona okręgiem<br />
<b>KÓP</b> - D.lm. od KOPA - 60 sztuk czegoś<br />
<b>KÓZ</b> - D.lm. od KOZA - zwierzę<br />
<b>KPA</b> - D.lp. od KIEP - głupek<br />
<b>KPI</b> - 3.os.lp. od KPIĆ - szydzić<br />
<b>KPY</b> - M.lm. od KIEP - głupek<br />
<b>KRA</b> - tafla lodu na rzece<br />
<b>KRĄ</b> - N.lp. od KRA<br />
<b>KRĘ</b> - B.lp. od KRA<br />
<b>KRO</b> - W.lp. od KRA<br />
<b>KRY</b> - M.lm. od KRA<br />
<b>KSI</b> - nazwa greckiej litery ξ, Ξ<br />
<b>KTO</b> - zaimek<br />
<b>KUB</b> - sześcian<br />
<b>KUC</b> - mały, krępy koń<br />
<b>KUĆ</b> - obrabiać plastycznie wyrób z metalu, D.lm. od KUCIA - kutia (potrawa wigilijna)<br />
<b>KUF</b> - D.lm. od KUFA - wielka drewniana beczka używana w piwowarstwie<br />
<b>KUJ</b> - tr.rozk. od KUĆ<br />
<b>KUK</b> - kucharz na statku<br />
<b>KUL</b> - D.lm. od KULA - bryła geometryczna, tr.rozk. od KULIĆ - kulić, chować<br />
<b>KUŁ</b> - 3.os.lp. cz.przeszłego od KUĆ<br />
<b>KUM</b> - ojciec chrzestny, D.m. od KUMA - matka chrzestna<br />
<b>KUN</b> - D.lm. od KUNA - zwierzę<br />
<b>KUP</b> - D.lp. od KUPA - dużo czegoś, tr.rozk. od KUPIĆ - nabyć za pieniądze<br />
<b>KUR</b> - kogut, choroba (różyczka), D.lm. od KURA - ptak domowy<br />
<b>KUŚ</b> - tr.rozk. od KUSIĆ - wabić, zachęcać<br />
<b>KWA</b> - wyraz naśladujący głos kaczki<br />
</div>
<div id='tabs-l'>
<b>LAB</b> - D.lm. od LABA - odpoczynek<br />
<b>LAĆ</b> - powodować płynięcie cieczy<br />
<b>LAD</b> - D.lm. od LADA - stół sklepowy<br />
<b>LAG</b> - D.lm. od LAGA - długa, gruba laska<br />
<b>LAI</b> - średniowieczny francuski poemat liryczny<br />
<b>LAK</b> - substancja do pieczętowania, D.lm. od LAKA - żywica stosowana do zdobienia przedmiotów<br />
<b>LAL</b> - D.lm. od LALA - lalka<br />
<b>LAŁ</b> - 3.os.lp. cz.przeszlego od LAĆ<br />
<b>LAM</b> - D.lm. od LAMA - zwierzę juczne z Ameryki Południowej<br />
<b>LAŃ</b> - D.lm. od LANIE - bicie, chłosta<br />
<b>LAR</b> - małpa wąskonosa z Półwyspu Malajskiego (gibon białoręki)<br />
<b>LAS</b> - duże skupisko drzew, D.lm. od LASA - krata do przesiewania żwiru, suszenia lnu<br />
<b>LAT</b> - D.lm. od LATO - pora roku, D.lm. od ROK<br />
<b>LAW</b> - D.lm. od LAWA - płynna masa skalna z wulkanów<br />
<b>LĄC</b> - lęgnąć (rodzić, wydawać na świat)<br />
<b>LĄD</b> - kontynent<br />
<b>LĄG</b> - wylęganie się zwierząt<br />
<b>LEC</b> - położyć się z ulgą<br />
<b>LEĆ</b> - tr.rozk. od LECIEĆ<br />
<b>LEE</b> - odzież marki Lee<br />
<b>LEG</b> - D.lm. od LEGA - arkusz introligatorski (16 stron)<br />
<b>LEI</b> - D.lm. od LEJ, D.lp. od LEJA - waluta Rumunii<br />
<b>LEJ</b> - wyrwa w ziemi; waluta Rumunii, tr.rozk. od LAĆ<br />
<b>LEK</b> - medykament; waluta Albanii<br />
<b>LEN</b> - roślina<br />
<b>LEŃ</b> - próżniak, tr.rozk. od LENIĆ się - próżnować<br />
<b>LEP</b> - lepka substancja syntetyczna, tr.rozk. od LEPIĆ - kleić<br />
<b>LEU</b> - waluta Rumunii<br />
<b>LEW</b> - zwierzę; waluta Bułgarii, D.lm. od LEWA - wziątka w grze w karty<br />
<b>LEŹ</b> - tr.rozk. od LEŹĆ - wolno iść<br />
<b>LEŻ</b> - tr.rozk. od LEŻEĆ - być w pozycji poziomej, D.lm. od LEŻA - miejsce przygotowane do spania<br />
<b>LĘG</b> - wylęganie się zwierząt<br />
<b>LĘK</b> - strach<br />
<b>LIC</b> - D.lm. od LICO, LICE - twarz, policzek<br />
<b>LID</b> - wstęp w tekście prasowym<br />
<b>LIG</b> - D.lm. od LIGA - wydzielona grupa drużyn, rozgrywających mistrzostwa każdy z każdym<br />
<b>LIK</b> - wzmocniony brzeg żagla<br />
<b>LIM</b> - D.lm. od LIMA - drzewo cytrusowe, D.lm. od LIMO - siniec pod okiem<br />
<b>LIN</b> - gatunek ryby, D.lm. od LINA - gruby sznur<br />
<b>LIP</b> - D.lm. od LIPA - drzewo<br />
<b>LIR</b> - waluta Włoch, D.lm. od LIRA - starogrecki instrument strunowy<br />
<b>LIS</b> - zwierzę<br />
<b>LIT</b> - pierwiastek chemiczny, waluta Litwy<br />
<b>LIŻ</b> - tr.rozk. od LIZAĆ - przesuwać po czymś językiem<br />
<b>LNU</b> - D.lp. od LEN<br />
<b>LNY</b> - M.lm. od LEN<br />
<b>LOB</b> - w tenisie, piłce nożnej: przerzucenie piłki wysokim łukiem ponad przeciwnikiem<br />
<b>LOG</b> - urządzenie nawigacyjne do mierzenia drogi przebytej przez statek<br />
<b>LOK</b> - pukiel włosów<br />
<b>LOR</b> - D.lm. od LORA - odkryty wagon towarowy<br />
<b>LOS</b> - dola, koleje życia<br />
<b>LOT</b> - poruszanie się w powietrzu; sonda - ciężarek mierzący głębokość wody<br />
<b>LÓD</b> - woda w stanie stałym<br />
<b>LÓŻ</b> - D.lm. od LOŻA - wydzielona część sali widowiskowej<br />
<b>LUB</b> - spójnik (albo), tr.rozk. od LUBIĆ - czuć sympatię<br />
<b>LUD</b> - tłum ludzi<br />
<b>LUF</b> - D.lm. od LUFA - część broni palnej<br />
<b>LUK</b> - zamykany klapą otwór w pokładzie statku, D.lm. od LUKA - przerwa<br />
<b>LUM</b> - moneta armeńska - 1/100 drama<br />
<b>LUN</b> - D.lm. od LUNA - księżyc<br />
<b>LUŃ</b> - tr.rozk. od LUNĄĆ - chlusnąć<br />
<b>LUP</b> - D.lm. od LUPA - szkło powiększające<br />
<b>LUR</b> - dęty instrument muzyczny (długa metalowa rura zgięta spiralnie), D.lm. od LURA - słaba kawa<br />
<b>LUT</b> - stop do lutowania<br />
<b>LUZ</b> - wolne miejsce<br />
<b>LWA</b> - D.lp. od LEW (zwierzę)<br />
<b>LWI</b> - przymiotnik od LEW (zwierzę)<br />
<b>LWU</b> - C.lp. od LEW (zwierzę)<br />
<b>LWY</b> - M.lm. od LEW (zwierzę)<br />
<b>LŻĄ</b> - 3.os.lm. od LŻYĆ - znieważać<br />
<b>LŻĘ</b> - 1.os.lp. od LŻYĆ - znieważać<br />
<b>LŻY</b> - 3.os.lp. od LŻYĆ - znieważać<br />
</div>
<div id='tabs-ł'>
<b>ŁAD</b> - porządek, D.lm. od ŁADA - samochód marki Łada<br />
<b>ŁAJ</b> - tr.rozk. od ŁAJAĆ - besztać, ganić<br />
<b>ŁAM</b> - liczba wierszy tekstu o wysokości kolumny (szpalta), tr.rozk. od ŁAMAĆ - rozrywać<br />
<b>ŁAN</b> - ziemia uprawna zasiana zbożem<br />
<b>ŁAŃ</b> - D.lm. od ŁANIA - samica jelenia<br />
<b>ŁAP</b> - D.lm. od ŁAPA - kończyna zwierzęcia, tr.rozk. od ŁAPAĆ - chwytać<br />
<b>ŁAŚ</b> - tr.rozk. od ŁASIĆ się - przymilać się<br />
<b>ŁAT</b> - waluta Łotwy, D.lm. od ŁATA - kawałek tkaniny przykrywający dziurę<br />
<b>ŁAW</b> - D.lm. od ŁAWA - mebel do siedzenia<br />
<b>ŁAŹ</b> - tr.rozk. od ŁAZIĆ - niezgrabnie chodzić<br />
<b>ŁĄG</b> - podmokła łąka w dolinie rzeki (ŁĘG)<br />
<b>ŁĄK</b> - D.lm. od ŁĄKA - teren porośnięty trawą<br />
<b>ŁBA</b> - D.lp. od ŁEB<br />
<b>ŁBU</b> - C.lp. od ŁEB<br />
<b>ŁBY</b> - M.lm. od ŁEB<br />
<b>ŁEB</b> - głowa zwierzęcia<br />
<b>ŁEZ</b> - D.lm. od ŁZA<br />
<b>ŁĘG</b> - podmokła łąka w dolinie rzeki (ŁĄG)<br />
<b>ŁĘK</b> - przednia część siodła wygięta do góry<br />
<b>ŁĘT</b> - łodyga kartofli<br />
<b>ŁKA</b> - 3.os.lp. od ŁKAĆ - płakać<br />
<b>ŁOI</b> - 3.os.lp. od ŁOIĆ - bić, D.lm. od ŁÓJ<br />
<b>ŁOM</b> - drąg stalowy<br />
<b>ŁON</b> - D.lm. od ŁONO - dolna część brzucha<br />
<b>ŁOŚ</b> - zwierzę z rodziny jeleni<br />
<b>ŁÓJ</b> - tłuszcz zwierzęcy, tr.rozk. od ŁOIĆ - bić<br />
<b>ŁÓW</b> - polowanie, tr.rozk. od ŁOWIĆ - łapać ryby<br />
<b>ŁÓZ</b> - D.lm. od ŁOZA - wierzba szara<br />
<b>ŁÓŻ</b> - D.lm. od ŁOŻE - duże wspaniałe łóżko, tr.rozk. od ŁOŻYĆ - ponosić koszty czegoś<br />
<b>ŁUB</b> - pas kory drzewa<br />
<b>ŁUG</b> - żrący roztwór wodny wodorotlenku sodowego lub potasowego<br />
<b>ŁUK</b> - linia półkolista<br />
<b>ŁUN</b> - D.lm. od ŁUNA - odblask pożaru<br />
<b>ŁUP</b> - zdobycz, D.lm. od ŁUPA - łupina, skorupa, tr.rozk. od ŁUPAĆ - trzaskać, tr.rozk. od ŁUPIĆ - rabować<br />
<b>ŁUT</b> - dawna jednostka wagi (1/32 funta)<br />
<b>ŁUZ</b> - D.lm. od ŁUZA - otwór w stole bilardowym<br />
<b>ŁYD</b> - D.lm. od ŁYDA - gruba łydka<br />
<b>ŁYK</b> - przełknięcie płynu; mieszczuch, D.lm. od ŁYKO - tkanka podkorowa drzew<br />
<b>ŁYP</b> - tr.rozk. od ŁYPAĆ - rzucać okiem<br />
<b>ŁZA</b> - kropla z oka<br />
<b>ŁZĄ</b> - N.lp. od ŁZA<br />
<b>ŁZĘ</b> - B.lp. od ŁZA<br />
<b>ŁZO</b> - W.lp. od ŁZA<br />
<b>ŁZY</b> - M.lm. od ŁZA<br />
<b>ŁŻĄ</b> - 3.os.lm. od ŁGAĆ - kłamać<br />
<b>ŁŻE</b> - 3.os.lp. od ŁGAĆ - kłamać<br />
<b>ŁŻĘ</b> - 1.os.lp. od ŁGAĆ - kłamać<br />
</div>
<div id='tabs-m'>
<b>MAC</b> - D.lm. od MACA - cienki placek z przaśnego ciasta spożywany podczas żydowskiego święta Paschy<br />
<b>MAĆ</b> - <i>daw.</i> matka<br />
<b>MAD</b> - D.lm. od MADA - tłusta gleba w dolinie rzeki<br />
<b>MAG</b> - czarnoksiężnik<br />
<b>MAI</b> - 3.os.lp. od MAIĆ - zdobić kwiatami, D.lp. od MAJA - owalny krab z Morza Śródziemnego (pająk morski)<br />
<b>MAJ</b> - piąty miesiąc w roku, tr.rozk. od MAIĆ - zdobić kwiatami<br />
<b>MAK</b> - roślina<br />
<b>MAM</b> - 1.os.lp. od MIEĆ - posiadać, D.lm. od MAMA - matka, tr.rozk. od MAMIĆ - łudzić<br />
<b>MAN</b> - D.lm. od MANA - bezosobowa nadnaturalna siła w wierzeniach ludów Oceanii<br />
<b>MAŃ</b> - tr.rozk. od MANIĆ - mamić, oszukiwać<br />
<b>MAP</b> - D.lm. od MAPA - płaski obraz powierzchni Ziemi<br />
<b>MAR</b> - D.lm. od MARA - urojenie, widzenie senne<br />
<b>MAS</b> - D.lm. od MASA - ciężar<br />
<b>MAT</b> - brak połysku; najniższy stopień podoficerski w marynarce wojennej, D.lm. od MATA - plecionka ze słomy<br />
<b>MAŹ</b> - kleista substancja<br />
<b>MAŻ</b> - tr.rozk. od MAZAĆ - brudzić<br />
<b>MĄĆ</b> - tr.rozk. od MĄCIĆ - czynić mętnym<br />
<b>MĄK</b> - D.lm. od MĄKA - produkt mielenia zbóż, D.lm. od MĘKA<br />
<b>MĄT</b> - osad, zawiesina (MĘT)<br />
<b>MĄŻ</b> - małżonek<br />
<b>MEE</b> - wyraz naśladujący głos kozy<br />
<b>MEJ</b> - D. zaimka MOJA (MOJEJ)<br />
<b>MEL</b> - jednostka wysokości dźwięku<br />
<b>MEN</b> - <i>pot.</i> facet<br />
<b>MER</b> - przewodniczący rady miasta we Francji; podstawowa jednostka makrocząsteczki<br />
<b>MES</b> - D.lm. od MESA - jadalnia na statku<br />
<b>MET</b> - D.lm. od META - końcowy punkt wyścigu<br />
<b>MEW</b> - D.lm. od MEWA - ptak morski<br />
<b>MĘT</b> - zawiesina w płynie (MĄT), chuligan<br />
<b>MIĘ</b> - B. zaimka JA<br />
<b>MIG</b> - znak porozumiewawczy, samolot MIG<br />
<b>MIK</b> - D.lm. od MIKA - minerał<br />
<b>MIL</b> - tysięczna część cala, D.lm. od MILA - jednostka długości używana w krajach anglosaskich (równa 1609 m)<br />
<b>MIM</b> - aktor pantomimy; starożytne farsowe widowisko sceniczne<br />
<b>MIN</b> - D.lm. od MINA - wyraz twarzy<br />
<b>MIŃ</b> - tr.rozk. od MINĄĆ - przejść obok<br />
<b>MIR</b> - poważanie, szacunek, D.lm. od MIRA - wonna żywica (mirra)<br />
<b>MIS</b> - D.lm. od MISA - duża miska<br />
<b>MIŚ</b> - pluszowy niedźwiadek<br />
<b>MIT</b> - podanie, legenda<br />
<b>MNĄ</b> - 3.os.lm. od MIĄĆ - gnieść, N. zaimka JA<br />
<b>MNĘ</b> - 1.os.lp. od MIĄĆ - gnieść<br />
<b>MOA</b> - wymarły ptak nielatający z Nowej Zelandii<br />
<b>MOB</b> - wojskowa mobilizacja<br />
<b>MOC</b> - siła<br />
<b>MOD</b> - ultranowoczesny, awangardowy (przymiotnik)<br />
<b>MOI</b> - zaimek<br />
<b>MOL</b> - jednostka ilości materii, D.lm. od MOLO - pomost nad wodą<br />
<b>MON</b> - D.lm. od MONA - małpa afrykańska<br />
<b>MOP</b> - szczotka do mycia podłóg - długi kij z umocowanymi paskami pochłaniającymi wodę<br />
<b>MOR</b> - D.lm. od MORA - tkanina jedwabna o falisto mieniącym się deseniu<br />
<b>MÓC</b> - być w stanie<br />
<b>MÓD</b> - D.lm. od MODA - powszechnie przyjęty zwyczaj<br />
<b>MÓJ</b> - zaimek dzierżawczy<br />
<b>MÓL</b> - owad<br />
<b>MÓR</b> - zaraza bydlęca<br />
<b>MÓW</b> - D.lm. od MOWA - mówienie, tr.rozk. od MÓWIĆ - wypowiadać słowa<br />
<b>MÓŻ</b> - tr.rozk. od MÓC<br />
<b>MRĄ</b> - 3.os.lm. od MRZEĆ - przestawać żyć<br />
<b>MRĘ</b> - 1.os.lp. od MRZEĆ - przestawać żyć<br />
<b>MRU</b> - część wyrażenia "ani mru mru"<br />
<b>MUC</b> - lichy koń<br />
<b>MUF</b> - D.lm. od MUFA - pierścień służący do łączenia rur, kabli<br />
<b>MUL</b> - jadalny małż morski, tr.rozk. od MULIĆ - pokrywać mułem<br />
<b>MUŁ</b> - zwierzę; osad rzeczny<br />
<b>MUN</b> - grzyb rosnący na drzewach (używany w kuchni chińskiej i wietnamskiej)<br />
<b>MUR</b> - ściana z cegieł<br />
<b>MUS</b> - obowiązek<br />
<b>MUŚ</b> - tr.rozk. od MUSIEĆ - podlegać przymusowi<br />
<b>MUU</b> - wyraz naśladujący głos krowy<br />
<b>MUZ</b> - D.lm. od MUZA - głos natchnienia<br />
<b>MYC</b> - D.lm. od MYCA - okrągła czapeczka bez daszka<br />
<b>MYĆ</b> - usuwać brud za pomocą wody<br />
<b>MYJ</b> - tr.rozk. od MYĆ<br />
<b>MYK</b> - wykrzyknik na oznaczenie szybkiego ruchu<br />
<b>MYL</b> - tr.rozk. od MYLIĆ - wprowadzać w błąd<br />
<b>MYŁ</b> - 3.os.lp. cz.przeszłego od MYĆ<br />
<b>MYM</b> - N. zaimka MÓJ (MOIM)<br />
<b>MYT</b> - D.lm. od MYTO - opłata za przejazd drogą, mostem<br />
<b>MŻĄ</b> - 3.os.lm. od MŻYĆ - kropić (o deszczu)<br />
<b>MŻĘ</b> - 1.os.lp. od MŻYĆ - kropić (o deszczu)<br />
<b>MŻY</b> - 3.os.lp. od MŻYĆ - kropić (o deszczu)<br />
</div>
<div id='tabs-n'>
<b>NAĆ</b> - liście i łodygi roślin okopowych<br />
<b>NAD</b> - przyimek<br />
<b>NAM</b> - C. zaimka MY<br />
<b>NAŃ</b> - na niego<br />
<b>NAP</b> - D.lm. od NAPA - okrągły, metalowy lub plastikowy zatrzask w ubraniach<br />
<b>NAR</b> - D.lm. od NARY - prycza<br />
<b>NAS</b> - D. zaimka MY<br />
<b>NAT</b> - szew biegnący wzdłuż korpusu włoka<br />
<b>NAW</b> - D.lm. od NAWA - część kościoła, odgraniczona rzędami kolumn<br />
<b>NEF</b> - średniowieczny statek handlowy używany na Morzu Północnym<br />
<b>NEM</b> - jednostka wartości odżywczej pokarmu dla niemowląt<br />
<b>NEP</b> - polityka gospodarcza w Rosji radzieckiej wprowadzona w 1921 r. z inicjatywy W.Lenina<br />
<b>NER</b> - D.lm. od NERA - nerka<br />
<b>NET</b> - dotknięcie piłką siatki przy serwie w tenisie, D.lm. od NETA - sieć zastawna do połowu ryb dennych<br />
<b>NĘĆ</b> - tr.rozk. od NĘCIĆ - wabić, kusić<br />
<b>NIĄ</b> - N. zaimka ONA<br />
<b>NIC</b> - zaprzeczenie istnienia czegokolwiek, D.lm. od NICA - lewa strona ubrania<br />
<b>NIĆ</b> - cienki sznureczek używany do szycia<br />
<b>NIE</b> - zaprzeczenie, B. zaimka ONE<br />
<b>NIL</b> - rodzaj kroju czcionek drukarskich (odmiana pisma egipskiego)<br />
<b>NIM</b> - N. zaimka ON<br />
<b>NIP</b> - D.lm. od NIPA - krzaczasta palma z Indii<br />
<b>NIT</b> - metalowy łącznik; jednostka ilości informacji stosowana w badaniach teoretycznych<br />
<b>NIW</b> - D.lm. od NIWA - ziemia uprawna<br />
<b>NIŻ</b> - obszar niskiego ciśnienia barometrycznego, tr.rozk. od NIZAĆ - nawlekać<br />
<b>NOC</b> - czas od zachodu do wschodu słońca<br />
<b>NOK</b> - wolny koniec drzewc omasztowania statku<br />
<b>NOM</b> - jednostka administracyjna w starożytnym Egipcie<br />
<b>NON</b> - D.lm. od NONA - interwał muzyczny złożony z oktawy i sekundy<br />
<b>NOR</b> - D.lm. od NORA - jama<br />
<b>NOS</b> - narząd powonienia<br />
<b>NOŚ</b> - tr.rozk. od NOSIĆ - mieć coś przy sobie<br />
<b>NOT</b> - D.lm. od NOTA - ocena<br />
<b>NÓG</b> - D.lm. od NOGA - kończyna dolna<br />
<b>NÓW</b> - ostatnia faza Księżyca<br />
<b>NÓŻ</b> - narzędzie do krojenia<br />
<b>NUĆ</b> - tr.rozk. od NUCIĆ - podśpiewywać<br />
<b>NUD</b> - D.lm. od NUDA - nudna kobieta<br />
<b>NUL</b> - zero<br />
<b>NUR</b> - ptak wodny<br />
<b>NUT</b> - wyżłobienie w drzewie służące do spojenia, D.lm. od NUTA - znak graficzny oznaczający dźwięk<br />
<b>NUŻ</b> - wyraz oznaczający nagłe rozpoczęcie czynności, tr.rozk. od NUŻYĆ - nudzić<br />
<b>NYG</b> - D.lm. od NYGA - komar (gwara wielkopolska)<br />
<b>NYS</b> - D.lm. od NYSA - samochód marki Nysa<br />
<b>NYŻ</b> - D.lm. od NYŻA - nisza (wnęka w fasadzie budynku)<br />
</div>
<div id='tabs-o'>
<b>OAZ</b> - D.lm. od OAZA - obszar z wodą na pustyni<br />
<b>OBA</b> - liczebnik<br />
<b>OBI</b> - japoński jedwabny pas do kimona<br />
<b>OBŁ</b> - D.lm. od OBŁO - część kadłuba statku<br />
<b>OBU</b> - D. liczebnika OBA<br />
<b>OBY</b> - partykuła nadająca zdaniu charakter życzenia<br />
<b>OCH</b> - wykrzyknik wyrażający radość, wzruszenie<br />
<b>OĆM</b> - D.lm. od OĆMA - mrok<br />
<b>ODA</b> - utwór poetycki<br />
<b>ODĄ</b> - N.lp. od ODA<br />
<b>ODE</b> - przyimek (od)<br />
<b>ODĘ</b> - B.lp. od ODA<br />
<b>ODM</b> - D.lm. od ODMA - dostanie się powietrza między tkanki lub narządy<br />
<b>ODO</b> - W.lp. od ODA<br />
<b>ODR</b> - D.lm. od ODRA - choroba zakaźna wieku dziecęcego<br />
<b>ODY</b> - M.lm. od ODA<br />
<b>OFF</b> - nurt teatrów niekomercyjnych, awangardowych i eksperymentalnych<br />
<b>OGI</b> - składany wachlarz japoński, zdobiony malowidłami<br />
<b>OGR</b> - ludożerczy olbrzym ze średniowiecznych podań ludowych<br />
<b>OHM</b> - OM (jednostka oporu elektrycznego)<br />
<b>OHO</b> - wykrzyknik wzmacniający wypowiedź<br />
<b>OKA</b> - D.lp. od OKO<br />
<b>OKO</b> - narząd wzroku; pętelka w sieci<br />
<b>OKR</b> - D.lm. od OKRA - roślina warzywna (ketmia jadalna)<br />
<b>OKU</b> - Ms.lp. od OKO<br />
<b>OLE</b> - wykrzyknik rodem z Hiszpanii<br />
<b>OLS</b> - las bagienny z olszą i jesionem<br />
<b>OMA</b> - D.lm. od OM - jednostka oporu elektrycznego<br />
<b>OMO</b> - proszek do prania marki Omo<br />
<b>OMU</b> - D.lm. od OM - jednostka oporu elektrycznego<br />
<b>OMY</b> - M.lm. od OM - jednostka oporu elektrycznego<br />
<b>ONA</b> - zaimek wskazujący (ten)<br />
<b>ONĄ</b> - N. zaimka ONA<br />
<b>ONE</b> - zaimek wskazujący<br />
<b>ONI</b> - zaimek wskazujący<br />
<b>ONO</b> - zaimek wskazujący<br />
<b>OPU</b> - D.lp. od OP - rodzaj sztuki abstrakcyjnej<br />
<b>OPY</b> - M.lm. od OP - rodzaj sztuki abstrakcyjnej<br />
<b>ORA</b> - D.lp. od ORO<br />
<b>ORD</b> - D.lm. od ORDA - obóz wojsk tatarskich<br />
<b>ORF</b> - D.lm. od ORFA - ryba (jaź złoty)<br />
<b>ORK</b> - mityczny olbrzym, potwór występujący w powieściach fantastycznych<br />
<b>ORO</b> - ludowy taniec Słowian południowych, tańczony w półokręgu<br />
<b>ORT</b> - drobna moneta srebrna bita w wielu krajach europejskich w XVI-XVIII w.<br />
<b>ORU</b> - C.lp. od ORO<br />
<b>ORZ</b> - tr.rozk. od ORAĆ - przygotowywać glebę pod zasiew<br />
<b>OSA</b> - owad<br />
<b>OSĄ</b> - N.lp. od OSA<br />
<b>OSĘ</b> - B.lp. od OSA<br />
<b>OSI</b> - przymiotnik od OSA, D.lp. od OŚ<br />
<b>OSM</b> - pierwiastek chemiczny<br />
<b>OSO</b> - W.lp. od OSA<br />
<b>OSP</b> - D.lm. pd OSPA - ostra choroba zakaźna<br />
<b>OSY</b> - M.lm. od OSA<br />
<b>OŚĆ</b> - cienka kostka mięsna ryb<br />
<b>OTO</b> - wyraz kierujący uwagę na kogoś lub coś<br />
<b>OUT</b> - w grach sportowych: wyjście piłki poza boisko (AUT)<br />
<b>OWA</b> - zaimek (ta)<br />
<b>OWĄ</b> - zaimek (tą)<br />
<b>OWE</b> - zaimek (te)<br />
<b>OWI</b> - zaimek (ci)<br />
<b>OWO</b> - zaimek (to)<br />
<b>OZU</b> - D.lp. od OZ - wał z piasków i żwirów polodowcowych<br />
<b>OZY</b> - M.lm. od OZ - wał z piasków i żwirów polodowcowych<br />
</div>
<div id='tabs-ó'>
<b>ÓCZ</b> - D.lm. od OKO<br />
</div>
<div id='tabs-p'>
<b>PAC</b> - <i>reg.</i> wielki szczur, D.lm. od PACA - narzędzie murarskie do wyrównywania zaprawy na murze<br />
<b>PAĆ</b> - D.lm. od PACIA - paćka, breja<br />
<b>PAD</b> - szybkie przypadnięcie ciałem do ziemi<br />
<b>PAF</b> - wyraz naśladujący odgłos strzału<br />
<b>PAI</b> - D.lp. od PAJA - gęba<br />
<b>PAJ</b> -  D.lm. od PAJA - gęba<br />
<b>PAK</b> - wieloletni pływający lód morski na obszarach Arktyki, D.lm. od PAKA - duża paczka<br />
<b>PAL</b> - słup z zaostrzonym końcem, tr.rozk. od PALIĆ - rozniecać ogień<br />
<b>PAŁ</b> - D.lm. od PAŁA - gruby kij<br />
<b>PAN</b> - mężczyzna<br />
<b>PAŃ</b> - D.lm. od PANI - kobieta<br />
<b>PAP</b> - D.lm. od PAPA - materiał do krycia dachów<br />
<b>PAR</b> - członek brytyjskiej Izby Lordów, D.lm. od PARA - dwie sztuki czegoś<br />
<b>PAS</b> - wąski kawałek tkaniny; odzywka w brydżu<br />
<b>PAŚ</b> - tr.rozk. od PAŚĆ - tuczyć, karmić zwierzęta<br />
<b>PAT</b> - remisowa sytuacja w szachach<br />
<b>PAW</b> - ptak<br />
<b>PAŹ</b> - młody dworzanin; motyl<br />
<b>PĄK</b> - zawiązek kwiatu<br />
<b>PĄS</b> - rumieniec<br />
<b>PEL</b> - D.lm. od PELA - miękkie i cienkie włókno jedwabne<br />
<b>PER</b> - część wyrażenia "mówić per ty, per pan", D.lm. od PERA - ziemniak (PYRA)<br />
<b>PET</b> - niedopałek<br />
<b>PĘD</b> - duża prędkość<br />
<b>PĘK</b> - wiązka, bukiet<br />
<b>PĘT</b> - D.lm. od PĘTO - wiązadło z rzemienia zakładane zwierzętom na nogi<br />
<b>PFE</b> - wykrzyknik wyrażający odrazę<br />
<b>PFF</b> - wykrzyknik wyrażający lekceważenie<br />
<b>PFU</b> - wykrzyknik wyrażający obrzydzenie<br />
<b>PHI</b> - wykrzyknik wyrażający lekceważenie<br />
<b>PHU</b> - wykrzyknik wyrażający pogardę<br />
<b>PHY</b> - wykrzyknik wyrażający lekceważenie<br />
<b>PIA</b> - moneta birmańska - 1/100 kiata<br />
<b>PIC</b> - blaga, bujda<br />
<b>PIĆ</b> - połykać płyn<br />
<b>PIF</b> - wyraz naśladujący odgłos strzału<br />
<b>PIJ</b> - tr.rozk. od PIĆ<br />
<b>PIK</b> - kolor w kartach oznaczony czarnym listkiem bzu, D.lm. od PIKA - broń piechoty (drzewce z grotem)<br />
<b>PIL</b> - D.lm. od PILA - krótki wyrostek komórek bakterii, tr.rozk. od PILIĆ - ponaglać<br />
<b>PIŁ</b> - 3.os.lp. cz.przeszłego od PIĆ, D.lm. od PIŁA - narzędzie do cięcia<br />
<b>PIN</b> - <i>elektron.</i>wyprowadzenie układu scalonego (nóżka)<br />
<b>PIP</b> - małe okienko na ekranie telewizora, pokazujące co się dzieje na innym kanale, D.lm. od PIPA - mandolina chińska<br />
<b>PIT</b> - D.lm. od PITA - chleb syryjski<br />
<b>PIU</b> - muzyczna wskazówka: więcej, bardziej<br />
<b>PIW</b> - D.lm. od PIWO - napój alkoholowy<br />
<b>PNĄ</b> - 3.os.lm. od PIĄĆ - posuwać się w górę<br />
<b>PNĘ</b> - 1.os.lp. od PIĄĆ - posuwać się w górę<br />
<b>PNI</b> - D.lm. od PIEŃ - nadziemna część drzewa<br />
<b>POĆ</b> - tr.rozk. od POCIĆ się - wydzielać pot<br />
<b>POD</b> - przyimek<br />
<b>POI</b> - 3.os.lp. od POIĆ - dawać pić<br />
<b>POŃ</b> - po niego<br />
<b>POP</b> - duchowny prawosławny; odmiana muzyki rozrywkowej<br />
<b>POR</b> - roślina warzywna, ujście gruczołu potowego<br />
<b>POT</b> - wydzielina gruczołów potowych<br />
<b>PÓJ</b> - tr.rozk. od POIĆ - dawać pić<br />
<b>PÓL</b> - D.lm. od POLE - ziemia uprawna<br />
<b>PÓŁ</b> - połowa, D.lm. od PÓŁA - duża półka, D.lm. od POŁA - dolny fragment jednej z dwóch części ubioru rozpinającego się z przodu<br />
<b>PÓR</b> - D.lm. od PORA - czas na coś<br />
<b>PÓZ</b> - D.lm. od POZA - układ ciała, postawa<br />
<b>PRĄ</b> - 3.os.lm. od PRZEĆ - silnie pchać<br />
<b>PRĘ</b> - 1.os.lp. od PRZEĆ - silnie pchać<br />
<b>PRO</b> - o argumentach, dowodach: przemawiający za kimś lub za czymś<br />
<b>PRR</b> - zawołanie zatrzymujące konia<br />
<b>PSA</b> - D.lp. od PIES - zwierzę<br />
<b>PSI</b> - przymiotnik od PIES - zwierzę<br />
<b>PSS</b> - wyraz oznaczający nakaz ściszenia głosu<br />
<b>PST</b> - wyraz oznaczający nakaz ściszenia głosu<br />
<b>PSU</b> - C.lp. od PIES - zwierzę<br />
<b>PSY</b> - M.lm. od PIES - zwierzę<br />
<b>PUB</b> - bar z napojami alkoholowymi<br />
<b>PUC</b> - blaga, oszustwo, D.lm. od PUCA - twarz o wydatnych policzkach<br />
<b>PUD</b> - dawna rosyjska jednostka masy (40 funtów = 16,38 kg)<br />
<b>PUF</b> - miękki niski taboret<br />
<b>PUH</b> - D.lm. od PUHA - bicz, batog<br />
<b>PUK</b> - wyraz naśladujący odgłos pukania<br />
<b>PUL</b> - porozumienie przedsiębiorstw zawierane w celu zrealizowania konkretnych przedsięwzięć, D.lm. od PULA - stawka w grze w karty<br />
<b>PUM</b> - D.lm. od PUMA - zwierzę drapieżne z Ameryki<br />
<b>PUN</b> - D.lm. od PUNA - półpustynia w Ameryce Południowej<br />
<b>PUP</b> - D.lm. od PUPA - pośladki<br />
<b>PYK</b> - wyraz naśladujący odgłos wypuszczania dymu z fajki<br />
<b>PYL</b> - tr.rozk. od PYLIĆ - wzniecać pył<br />
<b>PYŁ</b> - kurz<br />
<b>PYR</b> - D.lm. od PYRA - ziemniak<br />
<b>PYT</b> - D.lm. od PYTA - bicz, rzemień do bicia<br />
<b>PYZ</b> - D.lm. od PYZA - rodzaj kluski<br />
</div>
<div id='tabs-r'>
<b>RAB</b> -  niewolnik, sługa<br />
<b>RAC</b> - D.lm. od RACA, fajerwerk<br />
<b>RAD</b> - zadowolony, pierwiastek chemiczny; jednostka pochłoniętej dawki promieniowania jonizującego, D.lm. od RADA - wskazówka<br />
<b>RAF</b> - D.lm. od RAFA - skała podwodna<br />
<b>RAG</b> - D.lm. od RAGA - model melodyczny w muzyce indyjskiej<br />
<b>RAI</b> - D.lp. od RAJA - ryba; w Turcji osmańskiej: poddany płacący najwyższą daninę, 3.os.lp. od RAIĆ - polecać, swatać<br />
<b>RAJ</b> - kraina szczęśliwości, tr.rozk. od RAIĆ - polecać, swatać<br />
<b>RAK</b> - skorupiak<br />
<b>RAM</b> - D.lm. od RAMA - oprawa<br />
<b>RAN</b> - D.lm. od RANA - skaleczenie, D.lm. od RANO - ranek<br />
<b>RAŃ</b> - tr.rozk. od RANIĆ - kaleczyć<br />
<b>RAP</b> - ryba; gatunek muzyki<br />
<b>RAS</b> - wysoki dostojnik w Etiopii, D.lm. od RASA - zespół osobników o wspólnych cechach<br />
<b>RAT</b> - D.lm. od RATA - część spłaty należności<br />
<b>RAZ</b> - uderzenie, cios; wyraz oznaczający powtarzanie czegoś<br />
<b>RAŹ</b> - tr.rozk. od RAZIĆ - sprawiać przykre wrażenie<br />
<b>RĄB</b> - zaokrąglony lub zaostrzony koniec młota, przeciwległy płaskiemu obuchowi; wycinka drzew, tr.rozk. od RĄBAĆ - łupać<br />
<b>RĄK</b> - D.lm. od RĘKA, kończyna górna<br />
<b>RĄŚ</b> - D.lm. od RĄSIA - rączka<br />
<b>REB</b> - tytuł grzecznościowy używany przez Żydów, odpowiednik formy <i>pan</i><br />
<b>RED</b> - D.lm. od REDA - obszar wodny przed wejściem do portu<br />
<b>REF</b> - rząd krótkich linek na dole żagla, służących do zmniejszania jego powierzchni<br />
<b>REI</b> - D.lp. od REJ, D.lp. od REJA - poziome drzewce omasztowania statku<br />
<b>REJ</b> - <i>daw.</i> szereg, rząd (dziś w wyrażeniu "wodzić rej", D.lm. od REJA - poziome drzewce omasztowania statku<br />
<b>REK</b> - poziomy drążek do ćwiczeń gimnastycznych<br />
<b>REM</b> - jednostka pochłoniętej dawki promieniowania jonizującego<br />
<b>REN</b> - pierwiastek chemiczny; renifer<br />
<b>REP</b> - wyga, rytyniarz; lina żeglarska<br />
<b>RET</b> - piętro w triasie górnym<br />
<b>REW</b> - D.lm. od REWA - podwodny wał piasku, D.lm. od RWA<br />
<b>REŻ</b> - żyto<br />
<b>RHO</b> - nazwa greckiej litery ρ, Ρ<br />
<b>ROB</b> - D.lm. od ROBA - wykwintna, strojna suknia<br />
<b>ROD</b> - pierwiastek chemiczny<br />
<b>ROI</b> - 3.os.lp. od ROIĆ - marzyć, fantazjować<br />
<b>ROK</b> - jednostka rachuby czasu; posiedzenie sądowe w dawnej Polsce<br />
<b>ROL</b> - D.lm. od ROLA - duża rolka<br />
<b>ROŃ</b> - tr.rozk. od RONIĆ - tracić, gubić<br />
<b>ROP</b> - D.lm. od ROPA - surowiec kopalny do produkcji benzyny<br />
<b>ROS</b> - D.lm. od ROSA - kropelki wody gromadzące się na roślinach<br />
<b>ROŚ</b> - tr.rozk. od ROSIĆ - moczyć, zwilżać<br />
<b>ROT</b> - D.lm. od ROTA - podstawowy oddział w dawnym wojsku polskim<br />
<b>RÓB</b> - tr.rozk. od ROBIĆ - wykonywać<br />
<b>RÓD</b> - zespół ludzi spokrewnionych ze sobą<br />
<b>RÓG</b> - kostno-skórna wyniosłość u przeżuwaczy<br />
<b>RÓJ</b> - pszczoły zamieszkujące jeden ul, tr.rozk. od ROIĆ - marzyć, fantazjować<br />
<b>RÓL</b> - D.lm. od ROLA - grunt uprawny<br />
<b>RÓW</b> - podłużny dół wykopany w ziemi<br />
<b>RÓŻ</b> - kolor różowy, D.lm. od RÓŻA - kwiat<br />
<b>RUD</b> - D.lm. od RUDA - surowiec mineralny<br />
<b>RUF</b> - D.lm. od RUFA - tylna część statku<br />
<b>RUG</b> - <i>daw.</i> śledztwo sądowe, D.lm. od RUGA - besztanie<br />
<b>RUI</b> - D.lp. od RUJA - okres wzmożonego popędu płciowego u ssaków<br />
<b>RUJ</b> - D.lm. od RUJA - okres wzmożonego popędu płciowego u ssaków<br />
<b>RUM</b> - napój alkoholowy<br />
<b>RUN</b> - masowe dążenie do zdobycia czegoś, D.lm. od RUNO - owcza wełna, D.lm. od RUNY - najstarsze pismo północnoeuropejskie<br />
<b>RUŃ</b> - wschodzące, zielone jeszcze pędy traw, zboża, tr.rozk. od RUNĄĆ - przewrócić się<br />
<b>RUR</b> - D.lm. od RURA - walec do przeprowadzania cieczy, gazów<br />
<b>RUS</b> - dawna gra w karty<br />
<b>RUT</b> - D.lm. od RUTA - roślina<br />
<b>RWA</b> - choroba (rwa kulszowa)<br />
<b>RWĄ</b> - N.lp. od RWA, 3.os.lm. od RWAĆ - dzielić szarpiąc<br />
<b>RWĘ</b> - B.lp. od RWA, 1.os.lp. od RWAĆ - dzielić szarpiąc<br />
<b>RWO</b> - W.lp. od RWA<br />
<b>RWY</b> - M.lm. od RWA<br />
<b>RYB</b> - D.lm. od RYBA - zwierzę<br />
<b>RYĆ</b> - kopać, grzebać<br />
<b>RYG</b> - zespół urządzeń używanych do wierceń, D.lm. od RYGA - papier pomagający w równym pisaniu<br />
<b>RYJ</b> - pysk świni, tr.rozk. od RYĆ<br />
<b>RYK</b> - donośny głos zwierząt<br />
<b>RYŁ</b> - 3.os.lp. cz.przeszłego od RYĆ, D.lm. od RYŁO - <i>wulg.</i> twarz<br />
<b>RYM</b> - współbrzmienie wersów w poezji<br />
<b>RYP</b> - tr.rozk. od RYPAĆ - walić, uderzać<br />
<b>RYS</b> - charakterystyczna cecha, D.lm. od RYSA - skaza, zadrapanie<br />
<b>RYŚ</b> - zwierzę<br />
<b>RYT</b> - wycinanie motywów ostrymi narzędziami w drzewie, metalu, kości<br />
<b>RYZ</b> - D.lm. od RYZA - 500 arkuszy papieru<br />
<b>RYŻ</b> - roślina zbożowa<br />
<b>RŻĄ</b> - 3.os.lm. od RŻEĆ - o koniu: wydawać głos, N.lp. od REŻ<br />
<b>RŻE</b> - M.lm. od REŻ<br />
<b>RŻĘ</b> - 1.os.lp. od RŻEĆ - o koniu: wydawać głos<br />
<b>RŻY</b> - 3.os.lp. od RŻEĆ - o koniu: wydawać głos, D.lp. od REŻ<br />
</div>
<div id='tabs-s'>
<b>SAD</b> - ogród owocowy<br />
<b>SAG</b> - D.lm. od SAGA - staroskandynawska opowieść legendarno-historyczna, D.lm. od SAGO - mączka z sagowców<br />
<b>SAK</b> - worek podróżny, D.lm. od SAKO - język programowania pierwszych polskich maszyn cyfrowych<br />
<b>SAL</b> - D.lm. od SALA - pomieszczenie<br />
<b>SAM</b> - zaimek, sklep samoobsługowy<br />
<b>SAN</b> - autobus marki San<br />
<b>SAŃ</b> - D.lm. od SANIE - pojazd śniegowy ciągniony przez konie<br />
<b>SAP</b> - podmokła piaszczysta gleba, D.lm. od SAPA - ryba, tr.rozk. od SAPAĆ - głęboko, z trudem oddychać przez nos<br />
<b>SAS</b> - część wyrażenia "od sasa do lasa"<br />
<b>SĄD</b> - organ wymiaru sprawiedliwości<br />
<b>SĄG</b> - stos drewna<br />
<b>SEM</b> - termin z dziedziny teorii znaków<br />
<b>SEN</b> - spanie; moneta japońska - 1/100 jena<br />
<b>SEP</b> - dawna danina w ziarnie<br />
<b>SER</b> - produkt spożywczy z mleka<br />
<b>SET</b> - część meczu w tenisie, D.lm. od SETA - <i>pot. </i>100 ml wódki<br />
<b>SĘK</b> - dolna część uciętej gałęzi pozostała przy pniu<br />
<b>SĘP</b> - ptak drapieżny, tr.rozk. od SĘPIĆ - czynić posępnym<br />
<b>SIA</b> - zaimek (ta, owa)<br />
<b>SIĄ</b> - zaimek (tą, ową)<br />
<b>SIC</b> - <i>łac.</i> "tak" - uwaga umieszczana po podaniu zaskakującej informacji w celu zwrócenia na nią uwagi<br />
<b>SIE</b> - zaimek (te)<br />
<b>SIĘ</b> - zaimek zwrotny<br />
<b>SIL</b> - masa mineralna wypełniająca szczeliny w skale, tr.rozk. od SILIĆ się - natężać siły, usiłować<br />
<b>SIŁ</b> - D.lm. od SIŁA - energia fizyczna<br />
<b>SIM</b> - zamiek (tym), D.lm. od SIMA - warstwa litosfery bogata w krzem i magnez<br />
<b>SIO</b> - zaimek (to); okrzyk wydawany przy odpędzaniu kur<br />
<b>SIR</b> - angielski tytuł grzecznościowy<br />
<b>SIT</b> - roślina bagnista, D.lm. od SITO - przyrząd do przesiewania<br />
<b>SIU</b> - część wyrażenia "to tu, to siu"<br />
<b>SIW</b> - tr.rozk. od SIWIĆ - farbować bieliznę; zabarwiać siwo<br />
<b>SKA</b> - styl w muzyce tanecznej, pochodzący z Jamajki<br />
<b>SKI</b> - <i>przestarz.</i> narty<br />
<b>SNĄ</b> - 3.os.lm. od SNĄĆ - o rybach: zdychać<br />
<b>SNĘ</b> - 1.os.lp. od SNĄĆ - o rybach: zdychać<br />
<b>SNU</b> - D.lp. od SEN<br />
<b>SNY</b> - M.lm. od SEN<br />
<b>SOC</b> - nazwa domeny internetowej (sprawy społeczne)<br />
<b>SOF</b> - D.lm. od SOFA - niska, miękka kanapa bez poręczy<br />
<b>SOI</b> - D.lp. od SOJA - roślina oleista<br />
<b>SOK</b> - płyn wyciśnięty z owoców<br />
<b>SOL</b> - waluta Peru, D.lm. od SOLO - utwór solowy, nazwadźwiękug<br />
<b>SOM</b> - waluta Kirgistanu, D.lm. od SOMA - ciało człowieka (w przeciwieństwie do psyche - duszy)<br />
<b>SON</b> - jednostka głośności dźwięku<br />
<b>SOS</b> - gęsty płyn - przyprawa do potraw<br />
<b>SOU</b> - drobna moneta francuska równa 1/20 franka (SU)<br />
<b>SÓD</b> - pierwiastek chemiczny, D.lm. od SODA - węglan sodowy<br />
<b>SÓL</b> - chlorek sodu, tr.rozk. od SOLIĆ - posypywać solą<br />
<b>SÓW</b> - D.lm. od SOWA - ptak nocny<br />
<b>SPA</b> - zabiegi pielęgnacyjne mające na celu nawilżenie i odżywienie skóry<br />
<b>SRA</b> - 3.os.lp. od SRAĆ - <i>wulg.</i> wypróżniać się<br />
<b>SSĄ</b> - 3.os.lm. od SSAĆ - pić, wyciągając płyn ustami<br />
<b>SSĘ</b> - 1.os.lp. od SSAĆ - pić, wyciągając płyn ustami<br />
<b>STO</b> - liczebnik oznaczający 100<br />
<b>STU</b> - D. liczebnika STO<br />
<b>SUK</b> - dzielnica handlowa w miastach Bliskiego Wschodu (bazar), D.lm. od SUKA - samica psa<br />
<b>SUM</b> - ryba, D.lm. od SUMA - wynik dodawania<br />
<b>SUŃ</b> - tr.rozk. od SUNĄĆ - płynnie się poruszać, D.lm. od SUNIA - suczka<br />
<b>SUR</b> - D.lm. od SURA - rozdział Koranu<br />
<b>SUS</b> - skok<br />
<b>SUW</b> - część cyklu pracy silnika<br />
<b>SWA</b> - zaimek (swoja)<br />
<b>SWĄ</b> - zaimek (swoją)<br />
<b>SWE</b> - zaimek (swoje)<br />
<b>SYĆ</b> - tr.rozk. od SYCIĆ - czynić sytym<br />
<b>SYF</b> - choroba weneryczna; nieporządek, tr.rozk. od SYFIĆ - brudzić<br />
<b>SYK</b> - głos węża<br />
<b>SYN</b> - męski potomek<br />
<b>SYP</b> - tr.rozk. od SYPAĆ - rzucać czymś sypkim<br />
<b>SYT</b> - syty (najedzony), D.lm. od SYTA - miód rozpuszczony w wodzie, dawany pszczołom jako pokarm<br />
<b>SZA</b> - wykrzyknik nakazujący zachowanie ciszy<br />
<b>SZU</b> - wyraz naśladujący szelest<br />
</div>
<div id='tabs-ś'>
<b>ŚLĄ</b> - 3.os.lm. od SŁAĆ - wysyłać kogoś, coś<br />
<b>ŚLE</b> - 3.os.lp. od SŁAĆ - wysyłać kogoś, coś<br />
<b>ŚLĘ</b> - 1.os.lp. od SŁAĆ - wysyłać kogoś, coś<br />
<b>ŚMO</b> - część wyrażenia "to i śmo"<br />
<b>ŚNI</b> - 3.os.lp. od ŚNIĆ - widzieć coś we śnie<br />
<b>ŚPI</b> - 3.os.lp. od SPAĆ - być pogrążonym we śnie<br />
</div>
<div id='tabs-t'>
<b>TAB</b> - klawisz Tab (tabulator)<br />
<b>TAC</b> - D.lm. od TACA - płaskie naczynie do podawania potraw<br />
<b>TAF</b> - D.lm. od TAFA - torbacz z Australii (myszowór)<br />
<b>TAG</b> - znacznik w tekstach komputerowych<br />
<b>TAI</b> - 3.os.lp. od TAIĆ - trzymać w sekrecie<br />
<b>TAJ</b> - tr.rozk. od TAIĆ - trzymać w sekrecie, tr.rozk. od TAJAĆ - topnieć<br />
<b>TAK</b> - w ten sposób<br />
<b>TAL</b> - pierwiastek chemiczny<br />
<b>TAM</b> - wyraz określający miejsce, D.lm. od TAMA - zapora na rzece<br />
<b>TAN</b> - taniec<br />
<b>TAO</b> - w taoizmie: bezosobowe bóstwo, ład kosmiczny, zbiór praw rządzących przyrodą (DAO)<br />
<b>TAR</b> - zwierzę krętorogie z wysokich gór Azji, D.lm. od TARA - dawny przyrząd do prania<br />
<b>TAŚ</b> - okrzyk, którym zwołuje się kaczki, mający naśladować ich głos<br />
<b>TAU</b> - nazwa greckiej litery τ, Τ<br />
<b>TAŻ</b> - zaimek osobowy z partykułą wzmacniającą<br />
<b>TĄP</b> - tr.rozk. od TĄPAĆ - o skałach: pękać<br />
<b>TĄŻ</b> - zaimek osobowy z partykułą wzmacniającą<br />
<b>TEJ</b> - D. zaimka TA<br />
<b>TEK</b> - drzewo tropikalne, D.lm. od TEKA - duża teczka<br />
<b>TEŁ</b> - D.lm. od TŁO<br />
<b>TEN</b> - zaimek<br />
<b>TER</b> - mieszanina smoły, kalafonii i tłuszczu, D.lm. od TERA - świątynia buddyjska w Japonii<br />
<b>TEZ</b> - D.lm. od TEZA - twierdzenie, założenie<br />
<b>TEŻ</b> - partykuła uwydatniająca podobieństwo; zaimek osobowy z partykułą wzmacniającą<br />
<b>TĘP</b> - tr.rozk. od TĘPIĆ - czynić tępym<br />
<b>TĘŻ</b> - zaimek osobowy z partykułą wzmacniajacą<br />
<b>TFU</b> - wyraz naśladujący odgłos splunięcia<br />
<b>TIK</b> - mimowolny skurcz mięśni twarzy<br />
<b>TIR</b> - duży samochód ciężarowy<br />
<b>TKA</b> - 3.os.lp. od TKAĆ - sporządzać tkaninę<br />
<b>TLĄ</b> - 3.os.lm. od TLIĆ się - żarzyć się<br />
<b>TLE</b> - Ms.lp. od TŁO<br />
<b>TLĘ</b> - 1.os.lp. od TLIĆ się - żarzyć się<br />
<b>TLI</b> - 3.os.lp. od TLIĆ się - żarzyć się<br />
<b>TŁA</b> - M.lm. od TŁO<br />
<b>TŁO</b> - dalszy plan w przestrzeni<br />
<b>TŁU</b> - C.lp. od TŁO<br />
<b>TNĄ</b> - 3.os.lm. od CIĄĆ - dzielić ostrym narzędziem<br />
<b>TNĘ</b> - 1.os.lp. od CIĄĆ - dzielić ostrym narzędziem<br />
<b>TOĆ</b> - <i>przestarz.</i> przecież, wszakże<br />
<b>TOG</b> - D.lm. od TOGA - strój adwokatów, sędziów<br />
<b>TOK</b> - bieg sprawy<br />
<b>TOM</b> - część większego dzieła literackiego<br />
<b>TON</b> - dźwięk muzyczny, D.lm. od TONA - 1000 kg<br />
<b>TOŃ</b> - głębia wody, tr.rozk. od TONĄĆ - pogrążać się w wodzie<br />
<b>TOP</b> - szczyt popularności, tr.rozk. od TOPIĆ - zanurzać w wodzie<br />
<b>TOR</b> - szyny kolejowe; dawna jednostka ciśnienia, D.lm. od TORA - zwój z tekstem Pięcioksięgu, przechowywany w synagodze<br />
<b>TOŚ</b> - część wyrażenia "po toś" (np. przyszedł)<br />
<b>TOŻ</b> - zaimek osobowy z partykułą wzmacniającą<br />
<b>TÓG</b> - D.lm. od TOGA - strój adwokatów, sędziów<br />
<b>TRA</b> - część zwrotu "tra ta ta" naśladującego warkot<br />
<b>TRĄ</b> - 3.os.lm. od TRZEĆ - pocierać<br />
<b>TRĘ</b> - 1.os.lp. od TRZEĆ - pocierać<br />
<b>TRI</b> - handlowa nazwa trójchloroetylenu, używanego do wywabiania plam i czyszczenia<br />
<b>TRY</b> - część wyrażenia "w try miga" (natychmiast)<br />
<b>TSS</b> - wyraz nakazujący zachowanie ciszy<br />
<b>TUB</b> - duża gumowa misa do mycia się, D.lm. od TUBA - podłużne opakowanie<br />
<b>TUF</b> - lekka, porowata skała wulkaniczna<br />
<b>TUI</b> - D.lp. od TUJA - drzewo iglaste<br />
<b>TUJ</b> - D.lm. od TUJA - drzewo iglaste<br />
<b>TUK</b> - tłuszcz wytopiony, D.lm. od TUKA - rodzaj sieci włokowej, ciągnionej przez statki<br />
<b>TUL</b> - pierwiastek chemiczny, tr.rozk. od TULIĆ - trzymać w objęciach<br />
<b>TUM</b> - średniowieczny kościół katedralny<br />
<b>TUP</b> - tr.rozk. od TUPAĆ - mocno uderzać stopami o ziemię<br />
<b>TUR</b> - wymarły ssak rogaty, D.lm. od TURA - kolejka, etap, runda<br />
<b>TUŚ</b> - część wyrażenia "tuś mi bratku"<br />
<b>TUT</b> - D.lm. od TUTA - przyrząd wiertniczy do wydobywania urwanych narzędzi wiertniczych<br />
<b>TUZ</b> - najwyższa karta, as; osoba znakomita w jakiejś dziedzinie<br />
<b>TUŻ</b> - w bezpośredniej bliskości<br />
<b>TWA</b> - zaimek (twoja)<br />
<b>TWĄ</b> - zaimek (twoją)<br />
<b>TWE</b> - zaimek (twoje)<br />
<b>TYĆ</b> - przybierać na wadze<br />
<b>TYJ</b> - tr.rozk. od TYĆ<br />
<b>TYK</b> - czerwone płótno na wsypy, D.lm. od TYKA - gruba tyczka<br />
<b>TYŁ</b> - strona czegoś przeciwległa do przodu, 3.os.lp. cz.przeszłego od TYĆ<br />
<b>TYM</b> - N. zaimka TEN<br />
<b>TYN</b> - płot z gałęzi<br />
<b>TYP</b> - model, wzór; facet<br />
<b>TYŚ</b> - zaimek TY + końcówka osobowa<br />
</div>
<div id='tabs-u'>
<b>UCH</b> - D.lm. od UCHO - uchwyt, D.lm. od UCHA - rosyjska zupa rybna<br />
<b>UCZ</b> - tr.rozk. od UCZYĆ - przekazywać wiedzę<br />
<b>UDA</b> - 3.os.lp. od UDAĆ się - wybrać się gdzieś, D.lp. od UDO<br />
<b>UDO</b> - część kończyny dolnej<br />
<b>UDU</b> - C.lp. od UDO<br />
<b>UED</b> - sucha dolina na pustyniach, wypełniająca się wodą w porze deszczowej<br />
<b>UFA</b> - 3.os.lp. od UFAĆ - mieć przekonanie, że na kimś można polegać<br />
<b>UFF</b> - westchnienie ulgi<br />
<b>UFO</b> - niezidentyfikowany obiekt latający<br />
<b>UHA</b> - okrzyk wyrażający wesołość<br />
<b>UHM</b> - wyraz oznaczający potwierdzenie<br />
<b>UHU</b> - wyraz naśladujący głos sowy<br />
<b>UJE</b> - 3.os.lp. od UJEŚĆ - zjeść odrobinę<br />
<b>UJM</b> - D.lm. od UJMA - to, co uchybia czyjejś godności<br />
<b>ULA</b> - D.lp. od UL<br />
<b>ULE</b> - M.lm. od UL<br />
<b>ULG</b> - D.lm. od ULGA - poczucie odprężenia<br />
<b>ULI</b> - D.lm. od UL<br />
<b>ULU</b> - Ms.lp. od UL<br />
<b>ULW</b> - D.lm. od ULWA - glon morski<br />
<b>URN</b> - D.lm. od URNA - naczynie na popioły zmarłych<br />
<b>UST</b> - D.lm. od USTA - jama ustna<br />
<b>UTA</b> - japońska forma poetycka, nierymowany utwór liryczny złożony z 5 wersów<br />
<b>UTĄ</b> - N.lp. od UTA<br />
<b>UTĘ</b> - B.lp. od UTA<br />
<b>UTO</b> - W.lp. od UTA<br />
<b>UTY</b> - M.lm. od UTA<br />
<b>UZD</b> - D.lm. od UZDA - część uprzęży<br />
<b>UZI</b> - izraelski pistolet maszynowy<br />
</div>
<div id='tabs-w'>
<b>WAB</b> - wabik, rzecz używana do wabienia zwierzyny, tr.rozk. od WABIĆ - nęcić, o zwierzętach: przyzywać głosem<br />
<b>WAD</b> - minerał, D.lm. od WADA - ujemna cecha, usterka<br />
<b>WAG</b> - D.lm. od WAGA - przyrząd do pomiaru masy<br />
<b>WAL</b> - wieloryb, tr.rozk. od WALIĆ - uderzać<br />
<b>WAŁ</b> - nasyp; głupek<br />
<b>WAM</b> - C. zaimka WY<br />
<b>WAN</b> - samochód osobowy przeznaczony do przewozu większej liczby osób<br />
<b>WAR</b> - jednostka mocy elektrycznej; wrzątek<br />
<b>WAS</b> - D. zaimka WY<br />
<b>WAT</b> - jednostka mocy, D.lm. od WATA - materiał opatrunkowy<br />
<b>WAZ</b> - D.lm. od WAZA - naczynie na zupę<br />
<b>WAŻ</b> - tr.rozk. od WAŻYĆ - określać ciężar<br />
<b>WĄS</b> - zarost nad górną wargą u mężczyzn<br />
<b>WĄŻ</b> - gad<br />
<b>WDA</b> - 3.os.lp. od WDAĆ - szyjąc przymarszczyć jedną część tkaniny tak, aby zszyć ją równo z drugą, krótszą częścią<br />
<b>WEB</b> - D.lm. od WEBA - bardzo cienkie płótno lniane, używane na bieliznę pościelową<br />
<b>WEK</b> - słoik na przetwory, D.lm. od WEKA - <i>reg.</i> podłużna bułka<br />
<b>WEN</b> - D.lm. od WENA - zapał twórczy, werwa<br />
<b>WEŃ</b> - w niego<br />
<b>WET</b> - odpłacenie lub odwzajemnienie się komuś, D.lm. od WETO - sprzeciw<br />
<b>WEŹ</b> - tr.rozk. od WZIĄĆ - ująć, chwycić<br />
<b>WĘD</b> - D.lm. od WĘDA - linka z haczykiem, ciągnięta za statkiem<br />
<b>WĘZ</b> - D.lm. od WĘZA - cienki arkusz wosku pszczelego, zaczątek plastra<br />
<b>WIC</b> - żart, dowcip<br />
<b>WIĆ</b> - długa cienka gałązka wierzbowa, formować coś plotąc, skręcając<br />
<b>WID</b> - część wyrażeń "ani widu, ani słychu" "w pijanym widzie"<br />
<b>WIE</b> - 3.os.lp. od WIEDZIEĆ - być świadomym czegoś<br />
<b>WIG</b> - członek stronnictwa angielskiego, przekształconego w XIX w. w Partię Liberalną<br />
<b>WII</b> - D.lm. od WIJ<br />
<b>WIJ</b> - stawonóg, tr. rozk. od WIĆ<br />
<b>WIŁ</b> - <i>daw.</i> szaleniec, szaławiła, 3 os. lp. rm. od WIĆ<br />
<b>WIN</b> - D.lm. od WINA - wykroczenie, występek, D.lm. od WINO - napój alkoholowy<br />
<b>WIŃ</b> - tr.rozk. od WINIĆ - przypisywać komuś winę<br />
<b>WIO</b> - okrzyk poganiający konia<br />
<b>WIR</b> - szybki ruch obrotowy cząsteczek wody<br />
<b>WIS</b> - polski pistolet<br />
<b>WIŚ</b> - tr.rozk. od WISIEĆ - być zaczepionym<br />
<b>WIZ</b> - D.lm. od WIZA - zezwolenie na przekroczenie granicy<br />
<b>WOJ</b> - dawny wojownik, rycerz<br />
<b>WOK</b> - duża głęboka patelnia używana w kuchni chińskiej<br />
<b>WON</b> - waluta Korei<br />
<b>WOŃ</b> - przyjemny zapach<br />
<b>WOW</b> - wykrzyknik pochodzenia angielskiego<br />
<b>WOŹ</b> - tr.rozk. od WOZIĆ - transportować pojazdem<br />
<b>WÓD</b> - D.lm. od WODA - toń rzeki, jeziora, morza, D.lm. od WÓDA - wódka<br />
<b>WÓL</b> - tr.rozk. od WOLEĆ - preferować<br />
<b>WÓŁ</b> - kastrowany buhaj<br />
<b>WÓR</b> - duży worek<br />
<b>WÓZ</b> - pojazd<br />
<b>WÓŹ</b> - tr.rozk. od WOZIĆ - transportować pojazdem<br />
<b>WRĄ</b> - 3.os.lm. od WRZEĆ - gotować się, przemieniać w parę<br />
<b>WRE</b> - 3.os.lp. od WRZEĆ - gotować się, przemieniać w parę<br />
<b>WRĘ</b> - 1.os.lp. od WRZEĆ - gotować się, przemieniać w parę<br />
<b>WSI</b> - D.lp. od WIEŚ - osada rolnicza<br />
<b>WTE</b> - część wyrażenia "wte i wewte"<br />
<b>WUJ</b> - brat matki<br />
<b>WYĆ</b> - wrzeszczeć, ryczeć<br />
<b>WYG</b> - D.lm. od WYGA - cwaniak, spryciarz<br />
<b>WYJ</b> - tr.rozk. od WYĆ<br />
<b>WYK</b> - D.lm. od WYKA - roślina pastewna<br />
<b>WYŁ</b> - 3.os.lp. cz.przeszłego od WYĆ<br />
<b>WYR</b> - D.lm. od WYRO - łóżko<br />
<b>WYZ</b> - ryba żyjąca w Morzu Czarnym i Kaspijskim (bieługa)<br />
<b>WYŻ</b> - obszar wysokiego ciśnienia<br />
</div>
<div id='tabs-y'>
<b>YAM</b> - roślina podzwrotnikowa o jadalnych bulwach<br />
<b>YIN</b> - w chińskiej filozofii klasycznej: żeński, negatywny pierwiastek zawarty w przyrodzie (JIN)<br />
</div>
<div id='tabs-z'>
<b>ZAD</b> - pośladki dużych ssaków<br />
<b>ZAŃ</b> - za niego<br />
<b>ZAŚ</b> - natomiast<br />
<b>ZĄB</b> - twór kostny do chwytania i rozdrabniania pokarmu<br />
<b>ZDA</b> - 3.os.lp. od ZDAĆ - złożyć pomyślnie egzamin<br />
<b>ZEK</b> - więzień w Związku Radzieckim<br />
<b>ZEŁ</b> - D.lm. od ZŁO<br />
<b>ZEN</b> - japońska odmiana buddyzmu<br />
<b>ZEŃ</b> - z niego<br />
<b>ZER</b> - D.lm. od ZERO - liczba 0<br />
<b>ZET</b> - nazwa litery Z<br />
<b>ZEW</b> - wezwanie, wołanie do czynu<br />
<b>ZEZ</b> - wada ustawienia gałek ocznych<br />
<b>ZĘZ</b> - D.lm. od ZĘZA - najniższa część kadłuba statku<br />
<b>ZIŁ</b> - samochód marki ZIŁ<br />
<b>ZIM</b> - D.lm. od ZIMA - pora roku<br />
<b>ZIN</b> - gazetka fanów, sympatyków czegoś<br />
<b>ZIP</b> - rodzaj napędu do nagrywania i odtwarzania dyskietek komputerowych o dużej pojemności, tr.rozk. od ZIPAĆ - ciężko oddychać<br />
<b>ZIS</b> - samochód marki ZIS<br />
<b>ZJE</b> - 3.os.lp. od ZJEŚĆ - spożyć pokarm<br />
<b>ZŁA</b> - r.ż. przymiotnika ZŁY (niedobra), D.lp. od ZŁO<br />
<b>ZŁĄ</b> - N.lp.rż. od ZŁY (niedobrą)<br />
<b>ZŁE</b> - M.lm.rż. od ZŁY (niedobre)<br />
<b>ZŁO</b> - przeciwieństwo dobra<br />
<b>ZŁU</b> - C.lp. od ZŁO<br />
<b>ZŁY</b> - niedobry, szkodliwy, nieetyczny<br />
<b>ZNA</b> - 3.os.lp. od ZNAĆ - mieć pewien zasób wiadomości o kimś<br />
<b>ZOL</b> - roztwór koloidowy (substancja gazowa, ciekła lub stała zawieszona w cieczy)<br />
<b>ZON</b> - D.lm. od ZONA - strefa<br />
<b>ZOO</b> - ogród zoologiczny<br />
<b>ZUP</b> - D.lm. od ZUPA - potrawa płynna<br />
<b>ZWĄ</b> - 3.os.lm. od ZWAĆ - określać kogoś imieniem<br />
<b>ZWĘ</b> - 1.os.lp. od ZWAĆ - określać kogoś imieniem<br />
<b>ZWU</b> - D.lp. od ZEW<br />
<b>ZWY</b> - M.lm. od ZEW<br />
<b>ZYG</b> - wyraz używany przy drażnieniu się z kimś<br />
<b>ZYS</b> - orzeł przedni<br />
<b>ZZA</b> - przyimek<br />
</div>
<div id='tabs-ź'>
<b>ŹGA</b> - 3.os.lp. od ŹGAĆ - kłuć, żgać<br />
<b>ŹLE</b> - niedobrze<br />
<b>ŹLI</b> - M.lm. od ZŁY (niedobrzy)<br />
</div>
<div id='tabs-ż'>
<b>ŻAB</b> - D.lm. od ŻABA - płaz<br />
<b>ŻAK</b> - student, D.lm. od ŻAKO - afrykańska papuga popielata<br />
<b>ŻAL</b> - smutek, tr.rozk. od ŻALIĆ - skarżyć się, narzekać<br />
<b>ŻAR</b> - rozżarzone węgle<br />
<b>ŻĄĆ</b> - ścinać zboże<br />
<b>ŻĄŁ</b> - 3.os.lp. cz.przeszłego od ŻĄĆ<br />
<b>ŻĄP</b> - dolna część szybu górniczego<br />
<b>ŻEL</b> - instrument perkusyjny; galaretowata substancja<br />
<b>ŻEN</b> - podstawowa zasada konfucjanizmu - właściwy, humanitarny stosunek do bliźniego<br />
<b>ŻEŃ</b> - tr.rozk. od ŻENIĆ - dawać komuś kogoś za żonę<br />
<b>ŻER</b> - pożeranie<br />
<b>ŻET</b> - nazwa litery Ż<br />
<b>ŻĘĆ</b> - D.lm. od ŻĘCIE - rzeczownik odsłowny od ŻĄĆ<br />
<b>ŻGA</b> - 3.os.lp. od ŻGAĆ - kłuć, źgać<br />
<b>ŻNĄ</b> - 3.os.lm. od ŻĄĆ<br />
<b>ŻNĘ</b> - 1.os.lp. od ŻĄĆ<br />
<b>ŻON</b> - D.lm. od ŻONA - małżonka<br />
<b>ŻRĄ</b> - 3.os.lm. od ŻREĆ - jeść bez umiaru<br />
<b>ŻRE</b> - 3.os.lp. od ŻREĆ - jeść bez umiaru<br />
<b>ŻRĘ</b> - 1.os.lp. od ŻREĆ - jeść bez umiaru<br />
<b>ŻUĆ</b> - rozgniatać pokarm w ustach<br />
<b>ŻUJ</b> - tr.rozk. od ŻUĆ<br />
<b>ŻUK</b> - chrząszcz<br />
<b>ŻUL</b> - łobuz, oszust, złodziej<br />
<b>ŻUŁ</b> - 3.os.lp. cz.przeszłego od ŻUĆ<br />
<b>ŻUP</b> - D.lm. od ŻUPA - kopalnia soli<br />
<b>ŻUR</b> - zupa na zakwasie<br />
<b>ŻYĆ</b> - istnieć<br />
<b>ŻYD</b> - wyznawca judaizmu<br />
<b>ŻYJ</b> - tr.rozk. od ŻYĆ<br />
<b>ŻYŁ</b> - 3.os.lp. cz.przeszłego od ŻYĆ, D.lm. od ŻYŁA - naczynie krwionośne<br />
<b>ŻYR</b> - D.lm. od ŻYRO - poręczenie weksla<br />
<b>ŻYT</b> - D.lm. od ŻYTO - roślina zbożowa<br />
<b>ŻYW</b> - żywy, tr.rozk. od ŻYWIĆ - karmić
</div>
<div id='tabs-wszystkie' class='regular'>
ABO ABY ACH ACZ AFT AGA AGĄ AGĘ AGF AGI AGO AHA AIR AIS AKR AKT ALA ALĄ ALB ALD ALE ALĘ ALF ALG ALI ALK ALO ALP ALT AMF ANA ANI ANO ANS ANT ARA ARĄ ARB ARĘ ARF ARK ARO ARS ARY ASA ASY ATA ATU ATY AUŁ AUR AUT<br /><br />

BAB BAĆ BAD BAI BAJ BAK BAL BAŁ BAM BAN BAŃ BAR BAS BAT BAW BAZ BĄK BEE BEG BEJ BEK BEL BEN BER BET BEZ BEŻ BĘC BIB BIĆ BID BIG BIJ BIL BIŁ BIM BIS BIT BIZ BOA BOB BOĆ BOD BOI BOJ BOK BOL BOM BON BOR BOT BOY BÓB BÓG BÓJ BÓL BÓR BRR BRU BRY BUC BUD BUF BUG BUK BUL BUŁ BUM BUR BUS BUT BUZ BUŹ BYĆ BYK BYŁ BYM BYŚ BYT BZU BZY<br /><br />

CAB CAF CAL CAP CAR CEK CEL CEŁ CEN CEŃ CEP CER CES CEW CEZ CHA CHE CHI CIE CIĘ CIF CIP CIS CIŹ CIŻ CLĄ CLE CLĘ CLI CŁA CŁO CŁU CNA CNĄ CNE CNI CNY COB COD COM COŚ CÓR CÓŻ CUĆ CUD CUG CUM CUP CYC CYG CYK CYM CYN CYT CZE CZI CZY<br /><br />

ĆMA ĆMĄ ĆMĘ ĆMI ĆMO ĆMY ĆPA<br /><br />

DAĆ DAG DAJ DAL DAŁ DAM DAN DAŃ DAO DAR DAT DĄB DĄĆ DĄŁ DĄS DĄŻ DBA DEJ DEK DEM DEN DER DES DĘB DĘĆ DĘG DIP DIS DIW DLA DMĄ DMĘ DNA DNĄ DNĘ DNI DNO DNU DNY DOC DOG DOI DOK DOL DOM DON DOŃ DOZ DÓB DÓJ DÓL DÓŁ DÓZ DRĄ DRĘ DUD DUG DUH DUM DUO DUP DUR DUŚ DWA DWU DYB DYG DYL DYM DYN DYŃ DYZ<br /><br />

ECH ECU EDU EGO EHE EIS ELF EMU END EON ERA ERĄ ERB ERĘ ERG ERO ERY ESU ESY ETA ETĄ ETĘ ETO ETY EWA EWĄ EWE EWĘ EWO EWY<br /><br />

ĘSI<br /><br />

FAG FAI FAJ FAL FAŁ FAM FAN FAR FAS FAZ FEN FES FET FEZ FIG FIK FIL FIN FIS FIŚ FIT FIU FOB FOK FOL FON FOR FOS FOT FRR FRU FUG FUJ FUL FUM FUR FUS<br /><br />

GAĆ GAD GAF GAG GAI GAJ GAL GAŁ GAM GAŃ GAP GAR GAŚ GAY GAZ GAŻ GĄB GDY GEJ GEM GEN GES GEZ GĘB GĘG GĘŚ GHI GIB GID GIE GIF GIG GIK GIL GIN GIŃ GIR GIS GIT GNA GNĄ GNĘ GNU GOI GOJ GOL GON GOŃ GÓJ GÓL GÓR GRA GRĄ GRĘ GRO GRY GUB GUL GUM GUŃ GUR GUZ GZA GZĄ GZĘ GZI GZO GZY GŻĄ GŻĘ<br /><br />

HAF HAI HAJ HAK HAL HAN HAO HAS HAU HEC HEJ HEL HEM HEN HEP HER HET HIF HIP HIS HIT HIW HOC HOI HOJ HOL HOP HOR HOT HOY HUB HUF HUJ HUK HUN HUT HYC HYR HYŚ HYZ HYŹ<br /><br />

IBN ICH IDĄ IDĘ IDO IDU IDY IDŹ IGR III IKR IKS IKT ILE ILU IŁA IŁU IŁY IMA IMĄ IMĆ IMĘ IND INR IRG ISK IŚĆ ITR IWA IWĄ IWĘ IWO IWY IZB IZM<br /><br />

JAD JAG JAJ JAK JAM JAP JAR JAŚ JAT JAW JAZ JAŹ JĄĆ JĄŁ JEB JEJ JEM JEN JER JEŻ JĘĆ JĘK JĘT JIG JIN JOD JOG JOJ JOL JON JOR JOT JUG JUK JUN JUR JUS JUT JUZ JUŻ<br /><br />

KAC KAŁ KAM KAN KAŃ KAP KAR KAS KAT KAW KAŹ KAŻ KĄP KĄT KEI KEJ KEK KEM KER KET KĘP KĘS KIA KIĄ KIC KIĆ KIE KIĘ KII KIJ KIL KIŁ KIM KIN KIO KIP KIR KIŚ KIT KIŹ KLE KLO KŁA KŁY KOC KOĆ KOD KOG KOI KOK KOL KOM KOŃ KOP KOR KOS KOŚ KOT KÓJ KÓŁ KÓP KÓZ KPA KPI KPY KRA KRĄ KRĘ KRO KRY KSI KTO KUB KUC KUĆ KUF KUJ KUK KUL KUŁ KUM KUN KUP KUR KUŚ KWA<br /><br />

LAB LAĆ LAD LAG LAI LAK LAL LAŁ LAM LAŃ LAR LAS LAT LAW LĄC LĄD LĄG LEC LEĆ LEE LEG LEI LEJ LEK LEN LEŃ LEP LEU LEW LEŹ LEŻ LĘG LĘK LIC LID LIG LIK LIM LIN LIP LIR LIS LIT LIŻ LNU LNY LOB LOG LOK LOR LOS LOT LÓD LÓŻ LUB LUD LUF LUK LUM LUN LUŃ LUP LUR LUT LUZ LWA LWI LWU LWY LŻĄ LŻĘ LŻY<br /><br />

ŁAD ŁAJ ŁAM ŁAN ŁAŃ ŁAP ŁAŚ ŁAT ŁAW ŁAŹ ŁĄG ŁĄK ŁBA ŁBU ŁBY ŁEB ŁEZ ŁĘG ŁĘK ŁĘT ŁKA ŁOI ŁOM ŁON ŁOŚ ŁÓJ ŁÓW ŁÓZ ŁÓŻ ŁUB ŁUG ŁUK ŁUN ŁUP ŁUT ŁUZ ŁYD ŁYK ŁYP ŁZA ŁZĄ ŁZĘ ŁZO ŁZY ŁŻĄ ŁŻE ŁŻĘ<br /><br />

MAC MAĆ MAD MAG MAI MAJ MAK MAM MAN MAŃ MAP MAR MAS MAT MAŹ MAŻ MĄĆ MĄK MĄT MĄŻ MEE MEJ MEL MEN MER MES MET MEW MĘT MIĘ MIG MIK MIL MIM MIN MIŃ MIR MIS MIŚ MIT MNĄ MNĘ MOA MOB MOC MOD MOI MOL MON MOP MOR MÓC MÓD MÓJ MÓL MÓR MÓW MÓŻ MRĄ MRĘ MRU MUC MUF MUL MUŁ MUN MUR MUS MUŚ MUU MUZ MYC MYĆ MYJ MYK MYL MYŁ MYM MYT MŻĄ MŻĘ MŻY<br /><br />

NAĆ NAD NAM NAŃ NAP NAR NAS NAT NAW NEF NEM NEP NER NET NĘĆ NIĄ NIC NIĆ NIE NIL NIM NIP NIT NIW NIŻ NOC NOK NOM NON NOR NOS NOŚ NOT NÓG NÓW NÓŻ NUĆ NUD NUL NUR NUT NUŻ NYG NYS NYŻ<br /><br />

OAZ OBA OBI OBŁ OBU OBY OCH OĆM ODA ODĄ ODE ODĘ ODM ODO ODR ODY OFF OGI OGR OHM OHO OKA OKO OKR OKU OLE OLS OMA OMO OMU OMY ONA ONĄ ONE ONI ONO OPU OPY ORA ORD ORF ORK ORO ORT ORU ORZ OSA OSĄ OSĘ OSI OSM OSO OSP OSY OŚĆ OTO OUT OWA OWĄ OWE OWI OWO OZU OZY<br /><br />

ÓCZ<br /><br />

PAC PAĆ PAD PAF PAI PAJ PAK PAL PAŁ PAN PAŃ PAP PAR PAS PAŚ PAT PAW PAŹ PĄK PĄS PEL PER PET PĘD PĘK PĘT PFE PFF PFU PHI PHU PHY PIA PIC PIĆ PIF PIJ PIK PIL PIŁ PIN PIP PIT PIU PIW PNĄ PNĘ PNI POĆ POD POI POŃ POP POR POT PÓJ PÓL PÓŁ PÓR PÓZ PRĄ PRĘ PRO PRR PSA PSI PSS PST PSU PSY PUB PUC PUD PUF PUH PUK PUL PUM PUN PUP PYK PYL PYŁ PYR PYT PYZ<br /><br />

RAB RAC RAD RAF RAG RAI RAJ RAK RAM RAN RAŃ RAP RAS RAT RAZ RAŹ RĄB RĄK RĄŚ REB RED REF REI REJ REK REM REN REP RET REW REŻ RHO ROB ROD ROI ROK ROL ROŃ ROP ROS ROŚ ROT RÓB RÓD RÓG RÓJ RÓL RÓW RÓŻ RUD RUF RUG RUI RUJ RUM RUN RUŃ RUR RUS RUT RWA RWĄ RWĘ RWO RWY RYB RYĆ RYG RYJ RYK RYŁ RYM RYP RYS RYŚ RYT RYZ RYŻ RŻĄ RŻE RŻĘ RŻY<br /><br />

SAD SAG SAK SAL SAM SAN SAŃ SAP SAS SĄD SĄG SEM SEN SEP SER SET SĘK SĘP SIA SIĄ SIC SIE SIĘ SIL SIŁ SIM SIO SIR SIT SIU SIW SKA SKI SNĄ SNĘ SNU SNY SOC SOF SOI SOK SOL SOM SON SOS SOU SÓD SÓL SÓW SPA SRA SSĄ SSĘ STO STU SUK SUM SUŃ SUR SUS SUW SWA SWĄ SWE SYĆ SYF SYK SYN SYP SYT SZA SZU<br /><br />

ŚLĄ ŚLE ŚLĘ ŚMO ŚNI ŚPI<br /><br />

TAB TAC TAF TAG TAI TAJ TAK TAL TAM TAN TAO TAR TAŚ TAU TAŻ TĄP TĄŻ TEJ TEK TEŁ TEN TER TEZ TEŻ TĘP TĘŻ TFU TIK TIR TKA TLĄ TLE TLĘ TLI TŁA TŁO TŁU TNĄ TNĘ TOĆ TOG TOK TOM TON TOŃ TOP TOR TOŚ TOŻ TÓG TRA TRĄ TRĘ TRI TRY TSS TUB TUF TUI TUJ TUK TUL TUM TUP TUR TUŚ TUT TUZ TUŻ TWA TWĄ TWE TYĆ TYJ TYK TYŁ TYM TYN TYP TYŚ<br /><br />

UCH UCZ UDA UDO UDU UED UFA UFF UFO UHA UHM UHU UJE UJM ULA ULE ULG ULI ULU ULW URN UST UTA UTĄ UTĘ UTO UTY UZD UZI<br /><br />

WAB WAD WAG WAL WAŁ WAM WAN WAR WAS WAT WAZ WAŻ WĄS WĄŻ WDA WEB WEK WEN WEŃ WET WEŹ WĘD WĘZ WIC WIĆ WID WIE WIG WII WIJ WIŁ WIN WIŃ WIO WIR WIS WIŚ WIZ WOJ WOK WON WOŃ WOW WOŹ WÓD WÓL WÓŁ WÓR WÓZ WÓŹ WRĄ WRE WRĘ WSI WTE WUJ WYĆ WYG WYJ WYK WYŁ WYR WYZ WYŻ<br /><br />

YAM YIN<br /><br />

ZAD ZAŃ ZAŚ ZĄB ZDA ZEK ZEŁ ZEN ZEŃ ZER ZET ZEW ZEZ ZĘZ ZIŁ ZIM ZIN ZIP ZIS ZJE ZŁA ZŁĄ ZŁE ZŁO ZŁU ZŁY ZNA ZOL ZON ZOO ZUP ZWĄ ZWĘ ZWU ZWY ZYG ZYS ZZA<br /><br />

ŹGA ŹLE ŹLI<br /><br />

ŻAB ŻAK ŻAL ŻAR ŻĄĆ ŻĄŁ ŻĄP ŻEL ŻEN ŻEŃ ŻER ŻET ŻĘĆ ŻGA ŻNĄ ŻNĘ ŻON ŻRĄ ŻRE ŻRĘ ŻUĆ ŻUJ ŻUK ŻUL ŻUŁ ŻUP ŻUR ŻYĆ ŻYD ŻYJ ŻYŁ ŻYR ŻYT ŻYW
</div>
</div>
<? require_once "files/php/bottom.php"; ?>
</body>
</html>
