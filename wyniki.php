<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Kalendarz 2010</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
</head>

<body>
<?include_once "files/php/menu.php";?>

<pre>
<?php

if (($type = $_GET['type']) && $_GET['id']) {
    $data = pfs_select_one (array (
        table   => $DB_TABLES[tours],
        fields  => array ($type),
        where   => array ( id => $_GET['id'] )
    ));

    print $data->$type;
}
?>
</pre>

<?include_once "files/php/bottom.php";?>
</body>
</html>
