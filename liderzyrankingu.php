<?include_once "files/php/funkcje.php";?>
<html>
<head>
    <title>Polska Federacja Scrabble :: Scrabbliści : Liderzy rankingu</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style-scrabblisci.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/jquery-scrabblisci.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("scrabblisci","liderzyrankingu");</script>
</head>
<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Liderzy rankingu")</script></h1>

<?
$sql_conn = pfs_connect ();
$result = mysql_query("SELECT * FROM $DB_TABLES[players] WHERE typ='LR' ORDER BY rok DESC");

print "<div id='slideshow'><div id='slidesContainer'>";

while($row = mysql_fetch_array($result)){
    print "<div class='slide'><h2>".$row['osoba']."</h2><p>";

    $photo = pfs_photo_read ($DB_TABLES[players], $row[id]);
    if ($photo) {
        print "<img class='onright' src='foto_action.php?id=$photo->id' style='width:$photo->photo_width"."px;' alt='$row[osoba]' />";
    }

    print sierotki($row['notka']);
    print "<br><br><a href='http://scrabble.stats.org.pl/zindex.php?p=".$row['statnr']."' target='_blank'>Statystyki gracza</a>";
    print "</p></div>";
}
print "</div></div>";

print "<ul id='thumbscontainer'>";
$result = mysql_query("SELECT * FROM $DB_TABLES[players] WHERE typ='LR' ORDER BY rok ASC");
while($row = mysql_fetch_array($result)){
    $photo = pfs_photo_read ($DB_TABLES[players], $row[id]);
    print "<li class='thumb'><img src='foto_action.php?id=$photo->id' alt='$row[osoba]' /><span>".$row['rok']."</span></li>";
}
print "</ul>";

require_once "files/php/bottom.php"?>
</body>
</html>
