<?
include_once "files/php/funkcje.php";
$tour = pfs_select_one (array (
    table   => $DB_TABLES[tours],
    where   => array ( id => $_GET['id'] )
));
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: <?print $tour->nazwa;?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu ("turnieje","kalendarz");</script>
    <script>
    var url, page;

    page = 'info';
    url  = document.location.toString();

    if (url.match ('#')) {
      page = url.split ('#')[1];
      if ($.inArray (page, [ 'info', 'relacja', 'klasyfikacja', 'hh', 'rundy', 'ranking', 'przebieg' ]) == -1)
        page = 'info';
    }

    $(document).ready(function(){
        $('a#listaucz').bind('click', function(event){
            $('div#lista').toggle();
            event.preventDefault();
        }).toggle(function() { $(this).text('schowaj listę uczestników'); }, function() { $(this).text("pokaż listę uczestników"); });
        $('div#lista table tr:last td').css({"border-bottom" : "none"});

        $("a.przycisk").click (function (e) {
            if ($(this).attr ('href') !== '#')
                return true;

            e.preventDefault ();

            id       = this.id;
            this.id += 'tmp';
            window.location.hash = id;
            this.id  = id;

            $(".page").hide ();
            $(".page#p_" + id).show ();

            $("a.przycisk").removeClass ('active');
            $(this).addClass ('active');

            return false;
        });

        $("a.przycisk#" + page).click ();
        $('html, body').animate ({ scrollTop: 0 }, 0);
    });
    </script>
  <style type="text/css">
    .page {
        display: none;
    }
    #szczegoly{
        width: 100%;
        border-spacing: 0;
        margin-top: 30px;
        text-align: left;
        font-size: inherit;
    }
    #szczegoly td{
        padding: 10px 20px 10px 0;
        vertical-align: top;
    }
    #szczegoly td:first-child{
        font-weight: bold;
        width: 160px;
    }
    div#lista{
        display:none;
        margin-top: 15px;
    }
    div#lista table{
        margin-top: 10px;
        text-align: left;
        font-size: inherit;
    }
    div#lista table td{
        padding: 3px 8px;
        border-bottom: 1px dashed #1576AE;
    }
    div#lista table th{
        padding: 0px 5px 10px 5px;
    }
    div#lista table td.ptaszek{
        text-align: center;
        background: url('files/img/tick.png') no-repeat center center;
    }
    div#lista table th{
        white-space: nowrap;
        width: auto;
    }
    div#lista table td:first-child, div#lista table th:first-child{
        text-align:right;
        width: auto;
    }
    </style>
</head>

<body>
<?
require_once "files/php/menu.php";

if ($tour) {
    print "<h1>$tour->nazwa<span>".
        ($tour->rank == $TOUR_STATUS['gp'] ? "<img src='files/img/map-2.png' style='vertical-align: bottom;' title='Grand Prix'> " : "").
        "$tour->miasto, " . wyswietlDate ($tour->data_od, $tour->data_do, true).
        ($tour->rank == $TOUR_STATUS['norank'] ? " (turniej&nbsp;nierankingowy)" : "").
        ($tour->rank == $TOUR_STATUS['vacation'] ? " (wczasy&nbsp;scrabblowe)" : "").
        "</span></h1>";

    menuTurnieju ($tour);

    print "<div id='p_relacja' class='page'>";
    if ($tour->status) {
        pokazRelacje ($tour);
    }
    print "</div>";

    $prev_tours = pfs_select (array (
        table   => $DB_TABLES[tours],
        where   => array ( typ => $tour->typ, '!typ' => '', '<data_od' => $tour->data_od ),
        order   => array ( '!data_od' ),
        limit   => 3
    ));

    print "<div id='p_info' class='page'>";
    if ($prev_tours) {
        print "Zobacz, jak było na poprzednich turniejach:<br>";

        foreach ($prev_tours as $t) {
            print "<a href='turniej.php?id=$t->id'>$t->nazwa</a><br>";
        }
    }

    print "<br><br>$tour->dodatkowe_przed<table id='szczegoly' class='ramkadolna'>".
        ($tour->rodzaj      ? "<tr><td>Rodzaj turnieju</td><td>$tour->rodzaj</td></tr>"         : "").
        ($tour->sposob      ? "<tr><td>Sposób rozgrywania</td><td>$tour->sposob</td></tr>"      : "").
        ($tour->miejsce     ? "<tr><td>Miejsce rozgrywek</td><td>$tour->miejsce</td></tr>"      : "").
        ($tour->obronca     ? "<tr><td>Obrońca tytułu</td><td>$tour->obronca</td></tr>"         : "").
        ($tour->sedzia      ? "<tr><td>Sędzia</td><td>$tour->sedzia</td></tr>"                  : "").
        ($tour->stronawww   ? "<tr><td>Strona www turnieju</td><td>$tour->stronawww</td></tr>"  : "").
        ($tour->harmonogram ? "<tr><td>Harmonogram</td><td>$tour->harmonogram</td></tr>"        : "").
        ($tour->wpisowe     ? "<tr><td>Wpisowe</td><td>$tour->wpisowe</td></tr>"                : "").
        ($tour->dojazd      ? "<tr><td>Dojazd</td><td>$tour->dojazd</td></tr>"                  : "").
        ($tour->wyzywienie  ? "<tr><td>Wyżywienie</td><td>$tour->wyzywienie</td></tr>"          : "").
        ($tour->noclegi     ? "<tr><td>Noclegi</td><td>$tour->noclegi</td></tr>"                : "").
        ($tour->nagrody     ? "<tr><td>Nagrody</td><td>$tour->nagrody</td></tr>"                : "").
        ($tour->atrakcje    ? "<tr><td>Dodatkowe atrakcje</td><td>$tour->atrakcje</td></tr>"    : "");

    if ($tour->formularz) {
        print "<tr><td>Zgłoszenia uczestników</td><td>".
            ($tour->zgloszenia ? "$tour->zgloszenia<br>" : "").
            "<a href='formularze/krok1.php?idt=$tour->id'>formularz zgłoszeniowy</a><br><a href='' id='listaucz'>pokaż listę uczestników</a>
            <div id='lista'>
            <table>";

        $zapisy = pfs_select (array (
            table   => $DB_TABLES[registrations],
            where   => array ( id_turnieju => $tour->id, potwierdzenie => 1 ),
            order   => array ( 'id' )
        ));

        $lp = 1;

        foreach ($zapisy as $row) {
            print "<tr>
                <td>". $lp++ ."</td>
                <td>$row->osoba</td>
                <td>$row->miasto</td>
                <td style='font-style:italic;border-left:#1576AE 1px dashed;'>$row->uwagi</td>
                </tr>";
        }
        print "</table>
            </div>
            </td></tr>";

    }
    else if ($tour->zgloszenia || $tour->uczestnicy) {
        print "<tr><td>Zgłoszenia uczestników</td><td>";
        $br = "";
        if ($tour->zgloszenia) {
            print $tour->zgloszenia;
            $br = "<br>";
        }
        if ($tour->uczestnicy) {
            print "$br<a href='' id='listaucz'>pokaż listę uczestników</a>
                <div id='lista'>
                <table>". addslashes ($tour->uczestnicy). "</table>
                </div>";
        }
        print "</td></tr>";
    }

    print
        ($tour->organizatorzy ? "<tr><td>Organizatorzy</td><td>$tour->organizatorzy</td></tr>"  : "").
        ($tour->sponsorzy     ? "<tr><td>Sponsorzy</td><td>$tour->sponsorzy</td></tr>"          : "").
        "</table><br /><br />".
        $tour->dodatkowe;

    print "</div>";

    print "<div id='p_klasyfikacja' class='page'><pre>$tour->klasyfikacja</pre></div>";
    print "<div id='p_hh' class='page'><pre>$tour->hh</pre></div>";
    print "<div id='p_rundy' class='page'><pre>$tour->rundy</pre></div>";
    print "<div id='p_ranking' class='page'><pre>$tour->ranking</pre></div>";

    print "<div id='p_przebieg' class='page'>

    </div>";
}

require_once "files/php/bottom.php";
?>
</body>
</html>
