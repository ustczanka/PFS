LISTA S��W DO��CZONYCH ZE "S�OWNIKA J�ZYKA POLSKIEGO" POD REDAKCJ� WITOLDA DOROSZEWSKIEGO 

(podano niekt�re, mog�ce wzbudza� w�tpliwo�ci, formy, oraz imies�owy bierne dla czasownik�w przechodnich) 

Rzeczowniki: 

ASANI lp DCMsW asani, BN asani�; lm M asanie D asa� 
AUREOLKA Dlm -lek 
BABU�CIA Wlp -u; Dlm -�� 
BADYLEK Dlp -lka 
BALECIK Dlp -u 
BANIACZEK Dlp -czka 
BIDULA Wlp -o; Dlm -l 
BIDULKA Dlm -lek 
DEKOLCIK Dlp -a/-u 
DOLINKA Dlm -nek 
DRASKA Dlm -sek 
GZA Dlm giez 
HYR Dlp -u 
KOKOTKA Dlm -tek 
KOMSOMO� Dlp -u 
KSIʯULO lp DB -a, C -owi, N -em, MsW -u; lm M -owie/-e, D -�w 
�YSO� Dlp -nia; Dlm -ni/-ni�w 
PREZESOWA odm. jak przymiotnik, Wlp -owo 
R�WNINKA Dlm -nek 
SACZEK Dlp -a 
SADE�KO Dlm -�ek 
SIEDZISKO Dlm -isk 
SIU�KI blp, Dlm -k�w 
SKOBELEK Dlp -lka 
SPAZMATYK Dlp -a; Mlm -cy/-ki 
SRAKA 
SROMOTA Dlm -ot 
SZPADELEK Dlp -lka 
SZWED Dlp -a 
WI�SKO -Dlm wi�sk 
WSZYWKA Dlm -wek 
WYBIJAK Dlp -a 
WZI�TKA Dlm -tek 
�ӣWICA 

Czasowniki: 

DOKUWA� ndk I, DOKUWANY 
DOKU� dk Xa, DOKUTY 
DONISZCZY� dk VIb, DONISZCZONY 
DOPI�OWA� dk IV, DOPI�OWANY 
DORYWA� ndk I, DORYWANY 
GIBA� ndk I lub IX, GIBANY 
GIBN�� dk Va, GIBNI�TY 
NADUSI� dk VIa, NADUSZONY 
NALE�� dk, nalez�, nalaz�, nale�li, nale� 
NAMY� dk Xa, NAMYTY 
NAWCISKA� dk I, NAWCISKANY 
NAWYWIJA� dk I, NAWYWIJANY 
NA�AZI� ndk VIa 
O�PA� (SI�) dk I 
OMANI� dk VIa, OMANIONY 
PODZIWI� (SI�) dk VIa 
POKIMA� ndk I 
POKNOCI� dk VIa, POKNOCONY 
PONADYMA� dk I, PONADYMANY 
PON�CI� dk VIa, PON�CONY 
POOBLEKA� dk I, POOBLEKANY 
POOCIERA� dk I, POOCIERANY 
POODWIJA� dk I, POODWIJANY 
POOK�ADA� dk I, POOK�ADANY 
POOP�DZA� dk I, POOP�DZANY 
POOSADZA� dk I, POOSADZANY 
PO�GA� dk I, PO�GANY 
PRZYBIE�E� dk VIIb 
RYCHTOWA� ndk IV, RYCHTOWANY 
SKISI� dk VIa, SKISZONY 
SKOPCI� dk VIa, SKOPCONY 
WGRA� dk I, WGRANY 
WYCZEPI� dk VIa, WYCZEPIONY 
ZBA�WANI� dk VIa, ZBA�WANIONY 
ZWADZI� dk VIa, ZWADZONY 

Przymiotniki: 

ANTYLOPI 
BEZOKI 
HECNY 
NASADOWY 
TRZMIELI 
WSZAWY 

Przys��wki: 

BAROKOWO 
BLADAWO 
MENTALNIE 
MUSOWO 
NADOBNIE st.w. -niej 
NAMOLNIE st.w. -niej 
SIWO st.w. -wiej 
SI�OWO 
ZNAJOMO 
  
  
 