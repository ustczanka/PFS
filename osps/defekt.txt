LISTA CZASOWNIK�W DEFEKTYWNYCH 

Tylko bezokoliczniki, formy trzeciej osoby rodzaju nijakiego i rzeczowniki ods�owne: 
BRAKN��, CNI�, CKNI�, DNIE�, POWODZI�, PRZYSTA�, ROZEDNIE�, ZABRAKN��, ZADNIE�, ZBRAKN��.
Uwaga! Czasowniki POWODZI� i PRZYSTA� maj� te� znaczenia, w kt�rych s� w pe�ni odmienne. 

Brak form nieosobowych czasu przesz�ego: 
DOJ��, NADEJ��, NAJ��, OBEJ��, ODEJ��, PODEJ��, P�J��, PRZEJ��, PRZYJ��, ROZEJ��, UJ��, WEJ��, WYJ��, WZEJ��, ZAJ��, ZEJ��. 

Brak form nieosobowych czasu przesz�ego oraz rzeczownik�w ods�ownych:
I��, DOSI���, OBSI���, OSI���, POSI���, PRZESI���, PRZYSI���, ROZSI���, SI���, USI���, WSI���, WYSI���, ZASI���, ZSI���

WINIEN - tylko formy czasu przesz�ego w trybie orzekaj�/-cym 
POWINIEN - tylko formy czasu przesz�ego w trybie orzekaj�/-cym 
S�YCHA� - tylko bezokolicznik i formy imies�owu biernego 
WIDA� - tylko bezokolicznik i formy imies�owu biernego 
WNIJ�� - tylko formy czasu przysz�ego i trybu rozkazuj�/-cego 
ZWYKN�� - tylko formy czasu przesz�ego