﻿<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Turnieje : Zgłoszenie turnieju</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("turnieje","kalendarz");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Zgłoszenie turnieju")</script></h1>
Organizatorzy zgłaszający turniej są zobowiązani wypełnić <a href="rozne/zgloszenie_turnieju.doc">zgłoszenie</a> i wysłać je (elektronicznie) do <a onclick="sendMail('pfs','pfs.org.pl')">PFS</a><!-- lub wypełnić <a href="zgloszenie_turnieju_form.php">zgłoszenie online</a>-->. Przysłanie zgłoszenia jest warunkiem umieszczenia turnieju w kalendarzu imprez.<br />
Termin turnieju można zarezerwować wcześniej, podając datę oraz miejscowość, natomiast dane szczegółowe o turnieju 
należy uzupełnić najpóźniej na <b>30 dni przed jego rozpoczęciem</b>.<br /><br />

W otwartych turniejach organizowanych przez PFS lub pod egidą PFS może wziąć udział każdy (chyba, że turniej ma ograniczoną i wyczerpaną liczbę miejsc), kto zgłosi się w miejscu rozgrywania turnieju najpóźniej 30 minut przed jego rozpoczęciem. Można zagrać kilka, a nawet tylko jedną partię.<br /><br />
<a href="uchwala.php?rok=2010#uchwala30">Uchwałą nr 30/2010</a> Zarząd PFS ustalił <b>maksymalną opłatę wpisowego</b> za każdy dzień rozgrywek na turniejach rankingowych organizowanych pod egidą PFS na poziomie:
<ul>
	<li><b>12,5 zł</b> dla członków PFS z aktualnie opłaconymi składkami,</li>
	<li><b>20 zł</b> (19 zł + 1 zł na cele statutowe PFS) dla pozostałych osób,</li>
	<li><b>7,5 zł</b> dla młodzieży szkolnej (do liceum włącznie),</li>
	<li>debiutanci są zwolnieni z wpisowego.</li>
</ul>
Organizator ma prawo ustalić wpisowe na niższym poziomie, w szczególności decyduje o ewentualnych ulgach dla 
uczestników turnieju (ulga nie dotyczy składki 1 zł na cele statutowe PFS). Uchwała nie dotyczy wpisowego na 
turniejach szczebla centralnego i w turniejach specjalnych.<br /><br />

<!--<a href="zgloszenie_gp2009_form.php">Zgłoszenie turnieju Grand Prix 2009</a><br />-->
<a href="uchwala.php?rok=2004#uchwala9">Uchwała Zarzadu PFS w sprawie turniejów rankingowych</a><br />
<a href="sedziowie.php">Sędziowie PFS</a>


<?require_once "files/php/bottom.php"?>
</body>
</html>

