<?php
include_once "files/php/funkcje.php";
$sql_conn = pfs_connect ();
?>
<?
require_once "files/php/funkcje.php";

$gp_tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( 'YEAR(data_od)' => '2012', 'rank' => $TOUR_STATUS[gp]),
    order   => array ( 'data_od' )
));

$gp_results = pfs_select (array (
    table   => $DB_TABLES[gp2012],
    order   => array ( '!suma' )
));
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Grand Prix Polski</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("turnieje","archiwum");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
    <style type="text/css">
        table.linki{margin: 20px auto 0 auto;}
        table.linki td{ padding: 8px;vertical-align: top;}
    </style>
</head>

<body>
<? require_once "files/php/menu.php"; ?>
<h1><script>naglowek("Grand Prix Polski")</script></h1>

<div id="tabs" style="display:none;">
<?
$act_year = date('Y');
print '<ul>';
for ($year = 2012; $year >= 1998; $year--) {
    print '<li><a href="#tabs-'.$year.'">' . $year . '</a></li>';
}
print '</ul>';
?>

<div id='tabs-2012'>
    <h2>Grand Prix 2012</h2>
    Cykl Grand Prix 2012 obejmuje 11 turniejów:
<table class="linki ramkadolna">
<?php
$cnt = 1;
foreach ($gp_tours as $tour) {
    print "<tr><td>" . ($cnt++) . ".</td><td><a href='../turniej.php?id=$tour->id'>$tour->nazwa</a></td></tr>";
}
?>
</table>
<br><br>

    <h2>Zasady punktacji</h2>
    <ol>
    <li>Punkty do klasyfikacji łącznej Grand Prix zdobywa tylu uczestników ile wynosi część całkowita liczby obliczonej według wzoru (liczba uczestników którzy rozegrali min. 50% gier w turnieju / 3).<br>
    Zawodnik który zajmie ostatnie punktowane miejsce otrzymuje 1 punkt, każdy kolejny o jeden punkt więcej.<br>
    Dodatkowo zdobywcy 3 czołowych miejsc otrzymują premię: 5 punktów za miejsce pierwsze, 3 punkty za drugie i 1 punkt za trzecie.<br>
    (przykład 1: przy 62 uczestnikach: 20 miejsce - 1 punkt, 19 - 2, 18 - 3, (...) 4 - 17, 3 - 18+1, 2 - 19+3, 1 - 20+5)<br>
    (przykład 2: przy 85 uczestnikach: 28 miejsce - 1 punkt, 27 - 2, 26 - 3, (...) 4 - 25, 3 - 26+1, 2 - 27+3, 1 - 28+5)<br>
    <li>Minimalna liczba uczestników zdobywająca punkty do klasyfikacji Grand Prix wynosi 20 (tzn. w przypadku turnieju w którym mniej niż 60 osób rozegrało 50% gier, zawodnik za miejsce 20 otrzymuje 1 punkt, za 19 - 2, (...), za 4 - 17, za 3 - 18+1, za 2 -19+3. za 1 - 20+5)
	<li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Grand Prix 2012 do końcowej klasyfikacji zalicza się osiem najlepszych wyników.
	<li>Przy równej liczbie punktów w klasyfikacji końcowej o kolejności decyduje wyższa zdobycz punktowa w pojedynczym turnieju, a jeśli to nie da efektu (gracze mieli identyczny wynik) w kolejnym najlepszym turnieju, itd.<br>
    Jeżeli w ten sposób nie uda się ustalić kolejności, decyduje losowanie.

</ol>

    <h2>Nagrody</h2>
Organizatorzy każdego turnieju rangi Grand Prix wpłacają do puli nagród Grand Prix po 200 zł.<br />
Pula na nagrody w cyklu wynosi <b>2200 zł</b>.<br />
Nagrody w Grand Prix 2012 wynoszą:
<ul>
    <li>za I miejsce 1000 zł</li>
    <li>za II miejsce 600 zł</li>
    <li>za III miejsce 300 zł</li>
    <li>za IV miejsce 200 zł</li>
    <li>za V miejsce 100 zł</li>
</ul>

    <h2>Klasyfikacja cyklu Grand Prix 2012</h2>
<table class="klasyfikacja">
    <tr>
        <td></td>
        <td>Imię i nazwisko</td>
        <td class='suma'>SUMA</td>
<?
$cnt = 0;
foreach ($gp_tours as $tour) {
    $style = ($cnt++ % 2 ? " class='lighter'" : '');
    print "<td$style>$tour->miasto</td>";
}
?>
    </tr>
<?
$i          = 1;
$prev_suma  = 0;

foreach ($gp_results as $person) {
    print "<tr>";
    print "<td class='lp'>" . ($prev_suma == $person->suma ? '&nbsp;' : $i) ."</td>";
    $i++;
    $prev_suma = $person->suma;
    print "<td class='osoba'>$person->osoba</td>";
    print "<td class='suma'>$person->suma</td>";

    foreach ($gp_tours as $tour) {
        $miasto = strtolower (no_pl_chars ($tour->miasto));
        $style = ($cnt++ % 2 ? " class='lighter'" : '');
        print "<td$style>".$person->$miasto."</td>";
    }
    print "</tr>";
}
?>
</table>
</div>



<div id='tabs-2011'>
    <h2>Grand Prix 2011</h2>
    Cykl Grand Prix 2011 obejmuje 9 turniejów:
    <ol style="margin:20px;">
        <li><a href="turniej.php?id=550">IV Walentynki w Sulęcinie</a></li>
        <li><a href="turniej.php?id=570">XIII Mistrzostwa Szczecina</a></li>
        <li><a href="turniej.php?id=569">III Mistrzostwa Puław</a></li>
		<li><a href="turniej.php?id=559">II Mazurski Turniej o Puchar Prezydenta Miasta Ełku</a></li>
		<li><a href="turniej.php?id=548">XI Otwarte Mistrzostwa Ziemi Ostródzkiej "Blanki w Szranki"</a></li>
        <li><a href="turniej.php?id=555">XV Otwarte Mistrzostwa Krakowa w Scrabble</a></li>
        <li><a href="turniej.php?id=546">XV Mistrzostwa Ziemi Słupskiej</a></li>
        <li><a href="turniej.php?id=562">VI Mistrzostwa Jaworzna w Scrabble</a></li>
        <li><a href="turniej.php?id=571">VII Mistrzostwa Wrocławia w Scrabble</a></li>
    </ol>

    <h2>Zasady punktacji</h2>
    <ol>
        <li>Zawodnicy, którzy zajęli w turniejach Grand Prix miejsca od pierwszego do dwudziestego, zdobywają punkty do klasyfikacji łącznej Grand Prix - od 20 za I miejsce (ze zniżką o 1 punkt za każde kolejne) do 1 pkt za 20 miejsce. Zdobywcy 3 czołowych miejsc otrzymują dodatkowo: 5 punktów za miejsce pierwsze (razem 25), 3 punkty za drugie (razem 22) i 1 punkt za trzecie (razem 19).
        <li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Grand Prix 2011 do końcowej klasyfikacji zalicza się siedem najlepszych wyników.
		</li>
        <li>Przy równej liczbie punktów o kolejności decyduje wyższe miejsce w najlepszym dla danego gracza turnieju, a jeśli to nie da efektu (gracze mieli identyczne najwyższe miejsce) - w kolejnym najlepszym turnieju, itd. Jeżeli w ten sposób nie uda się ustalić kolejności, decyduje losowanie.
        </li>
    </ol>

    <h2>Nagrody</h2>
    Organizatorzy każdego turnieju rangi Grand Prix wpłacają do puli nagród Grand Prix po 200 zł.<br />
    Pula na nagrody w cyklu wynosi <b>1800zł</b>.<br />
    Nagrody w Grand Prix 2010 wynoszą:
    <ul>
        <li>za I miejsce 700zł</li>
        <li>za II miejsce 500zł</li>
        <li>za III miejsce 300zł</li>
        <li>za IV miejsce 200zł </li>
        <li>za V miejsce 100zł</li>
    </ul>

    <h2>Klasyfikacja cyklu Grand Prix 2011</h2>
    <table class="klasyfikacja">
        <tr>
            <td></td>
            <td>Imię i nazwisko</td>
            <td class='suma'>SUMA</td>
            <td>Sulęcin</td>
            <td class='lighter'>Szczecin</td>
			<td>Puławy</td>
            <td class='lighter'>Ełk</td>
            <td>Ostróda</td>
			<td class='lighter'>Kraków</td>
			<td>Słupsk</td>
            <td class='lighter'>Jaworzno</td>
            <td>Wrocław</td>
            </tr>

    <?
    $result = mysql_query("SELECT * FROM gp2011 ORDER BY suma DESC");
    $i = 1;
    $pop_suma = "0";

    while($row = mysql_fetch_array($result)){
        if($pop_suma != $row['suma'])
            print "<td class='lp'>".$i."</td>";
        else
            print "<td class='lp'>&nbsp;</td>";
        $i++;
        $pop_suma = $row['suma'];
        print "<td class='osoba'>".$row['osoba']."</td>";
        print "<td class='suma'>".$row['suma']."</td>";
        print "<td>".$row['sulecin']."</td>";
        print "<td class='lighter'>".$row['szczecin']."</td>";
		print "<td>".$row['pulawy']."</td>";
        print "<td class='lighter'>".$row['elk']."</td>";
        print "<td>".$row['ostroda']."</td>";
		print "<td class='lighter'>".$row['krakow']."</td>";
		print "<td>".$row['slupsk']."</td>";
		print "<td class='lighter'>".$row['jaworzno']."</td>";
        print "<td>".$row['wroclaw']."</td></tr>";
        
    }
    ?>
    </table>
</div>






<div id='tabs-2010'>
    <h2>Grand Prix 2010</h2>
    Cykl Grand Prix 2010 obejmuje 12 turniejów:
    <ol style="margin:20px;">
        <li><a href="turniej.php?id=501">Walentynki w Sulęcinie</a></li>
        <li><a href="turniej.php?id=513">XII Mistrzostwa Szczecina</a></li>
        <li><a href="turniej.php?id=503">V Mistrzostwa Milanówka "Truskawki w Milanówku</a></li>
        <li><a href="turniej.php?id=509">X Otwarte Mistrzostwa Ziemi Ostródzkiej "Blanki w Szranki"</a></li>
        <li><a href="turniej.php?id=507">VI Mistrzostwa Ziemi Wałeckiej "Wałeckie Scrabblobranie"</a></li>
        <li><a href="turniej.php?id=520">XV Mistrzostwa Górnego Śląska i Zagłębia</a></li>
        <li><a href="turniej.php?id=502">II Mistrzostwa Puław</a></li>
        <li><a href="turniej.php?id=522">XIV Otwarte Mistrzostwa Krakowa w Scrabble</a></li>
        <li><a href="turniej.php?id=498">XIV Mistrzostwa Ziemi Słupskiej</a></li>
        <li><a href="turniej.php?id=518">VI Mistrzostwa Wrocławia w Scrabble</a></li>
        <li><a href="turniej.php?id=517">V Mistrzostwa Jaworzna w Scrabble</a></li>
        <li><a href="turniej.php?id=517">II Otwarte Mistrzostwa Łomży</a></li>
    </ol>

    <h2>Zasady punktacji</h2>
    <ol>
        <li>Zawodnicy, którzy zajęli w turniejach Grand Prix miejsca od pierwszego do dwudziestego, zdobywają punkty do klasyfikacji łącznej Grand Prix - od 20 za I miejsce (ze zniżką o 1 punkt za każde kolejne) do 1 pkt za 20 miejsce. Zdobywcy 3 czołowych miejsc otrzymują dodatkowo: 5 punktów za miejsce pierwsze (razem 25), 3 punkty za drugie (razem 22) i 1 punkt za trzecie (razem 19).
            <ul>
                <li>Turnieje tegorocznego cyklu Grand Prix zostaną połączone w sześć par, według położenia geograficznego oraz terminu rozgrywania (podział przedstawiono w powyższej tabeli).</li>
                <li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Grand Prix 2010 do końcowej klasyfikacji zalicza się lepszy wynik uzyskany z każdej tak połączonej pary.</li>
            </ul>
        </li>
        <li>Przy równej liczbie punktów o kolejności decyduje wyższe miejsce w najlepszym dla danego gracza turnieju, a jeśli to nie da efektu (gracze mieli identyczne najwyższe miejsce) - w kolejnym najlepszym turnieju, itd. Jeżeli w ten sposób nie uda się ustalić kolejności, decyduje losowanie.
        </li>
    </ol>

    <h2>Nagrody</h2>
    Organizatorzy każdego turnieju rangi Grand Prix wpłacają do puli nagród Grand Prix po 200 zł.<br />
    Pula na nagrody w cyklu wynosi <b>2400zł (2000zł)</b>.<br />
    Nagrody w Grand Prix 2010 wynoszą:
    <ul>
        <li>za I miejsce 1000zł (800zł)</li>
        <li>za II miejsce 700zł (550zł)</li>
        <li>za III miejsce 400zł (350zł)</li>
        <li>za IV miejsce 200zł (200zł)</li>
        <li>za V miejsce 100zł (100zł)</li>
    </ul>

    <h2>Klasyfikacja cyklu Grand Prix 2010</h2>
    <table class="klasyfikacja">
        <tr>
            <td></td>
            <td>Imię i nazwisko</td>
            <td class='suma'>SUMA</td>
            <td>Sulęcin</td>
            <td>Puławy</td>
            <td class='lighter'>Szczecin</td>
            <td class='lighter'>Kraków</td>
            <td>Milanówek</td>
            <td>Słupsk</td>
            <td class='lighter'>Ostróda</td>
            <td class='lighter'>Wrocław</td>
            <td>Wałcz</td>
            <td>Jaworzno</td>
            <td class='lighter'>Zabrze</td>
            <td class='lighter'>Łomża</td>
        </tr>

    <?
    $result = mysql_query("SELECT * FROM gp2010 ORDER BY suma DESC");
    $i = 1;
    $pop_suma = "0";

    while($row = mysql_fetch_array($result)){
        if($pop_suma != $row['suma'])
            print "<td class='lp'>".$i."</td>";
        else
            print "<td class='lp'>&nbsp;</td>";
        $i++;
        $pop_suma = $row['suma'];
        print "<td class='osoba'>".$row['osoba']."</td>";
        print "<td class='suma'>".$row['suma']."</td>";
        print "<td>".$row['sulecin']."</td>";
        print "<td>".$row['pulawy']."</td>";
        print "<td class='lighter'>".$row['szczecin']."</td>";
        print "<td class='lighter'>".$row['krakow']."</td>";
        print "<td>".$row['milanowek']."</td>";
        print "<td>".$row['slupsk']."</td>";
        print "<td class='lighter'>".$row['ostroda']."</td>";
        print "<td class='lighter'>".$row['wroclaw']."</td>";
        print "<td>".$row['walcz']."</td>";
        print "<td>".$row['jaworzno']."</td>";
        print "<td class='lighter'>".$row['zabrze']."</td>";
        print "<td class='lighter'>".$row['lomza']."</td></tr>";
    }
    ?>
    </table>
</div>

<div id='tabs-2009'>
    <h2>Grand Prix 2009</h2>
    W roku 2009 cykl Grand Prix składał się z 10 turniejów
<ol style="margin:20px;">
    <li><a href="turniej.php?id=447">II Turniej o Puchar Burmistrza Sulęcina</a>, Sulęcin (14-15 lutego)</li>
    <li><a href="turniej.php?id=464">XI Mistrzostwa Szczecina &bdquo;Zanim Zakwitną Magnolie&rdquo;</a>, Szczecin (7-8 marca)</li>
    <li><a href="turniej.php?id=443">VIII Mistrzostwa Wybrzeża</a>, Rumia (16-17 maja)</li>
    <li><a href="turniej.php?id=454">XIV Mistrzostwa Górnego Śląska i Zagłębia</a>, Zabrze (6-7 czerwca) </li>
    <li><a href="turniej.php?id=470">XIII Mistrzostwa Krakowa</a>, Kraków (25-26 lipca)</li>
    <li><a href="turniej.php?id=437">XIII Mistrzostwa Ziemi Słupskiej</a>, Słupsk (1-2 sierpnia)</li>
    <li><a href="turniej.php?id=450">V Mistrzostwa Wałcza &bdquo;Wałeckie Scrabblobranie&rdquo;</a>, Wałcz (15-16 sierpnia)  </li>
    <li><a href="turniej.php?id=465">XIII Mistrzostwa Warszawy</a>, Warszawa (3-4 października)</li>
    <li><a href="turniej.php?id=466">V Mistrzostwa Wrocławia</a>, Wrocław (17-18 października)</li>
    <li><a href="turniej.php?id=460">XV Mistrzostwa Łodzi</a>, Łódź (7-8 listopada)</li>
</ol>

<h2>Zasady punktacji</h2>
<ol>
    <li>Zawodnicy, którzy zajęli w turniejach Grand Prix miejsca od pierwszego do dwudziestego, zdobywają punkty do klasyfikacji łącznej Grand Prix - od 20 za I miejsce (ze zniżką o 1 punkt za każde kolejne) do 1 pkt za 20 miejsce. Zdobywcy 3 czołowych miejsc otrzymują dodatkowo: 5 punktów za miejsce pierwsze (razem 25), 3 punkty za drugie (razem 22) i 1 punkt za trzecie (razem 19).
    <li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Grand Prix 2009 do końcowej klasyfikacji zaliczy się 7 najlepszych turniejów.</li>
    <li>Przy równej liczbie punktów o kolejności decyduje wyższe miejsce w najlepszym dla danego gracza turnieju, a jeśli to nie da efektu (gracze mieli identyczne najwyższe miejsce) - w kolejnym najlepszym turnieju, itd. Jeżeli w ten sposób nie uda się ustali_ kolejności, decyduje losowanie.
    </li>
</ol>

<h2>Nagrody</h2>
Organizatorzy każdego turnieju rangi Grand Prix wpłacają do puli nagród Grand Prix po 200 zł.<br />
Pula na nagrody w cyklu wynosi <b>2000 zł</b>.<br />
Nagrody w Grand Prix 2009 wynoszą:
<ul>
    <li>I miejsce &mdash; 800 zł</li>
    <li>II miejsce &mdash; 550 zł</li>
    <li>III miejsce &mdash; 350 zł</li>
    <li>IV miejsce &mdash; 200 zł</li>
    <li>V miejsce &mdash; 100 zł</li>
</ul>
<a name="klasyfikacja"></a>
<h2>Klasyfikacja cyklu Grand Prix 2009</h2>
<table class="klasyfikacja">
    <tr>
        <td></td>
        <td>Imię i nazwisko</td>
        <td class='suma'>SUMA</td>
        <td>Sulęcin</td>
        <td class='lighter'>Szczecin</td>
        <td>Rumia</td>
        <td class='lighter'>Zabrze</td>
        <td>Kraków</td>
        <td class='lighter'>Słupsk</td>
        <td>Wałcz</td>
        <td class='lighter'>Warszawa</td>
        <td>Wrocław</td>
        <td class='lighter'>Łódź</td>
    </tr>
<?
$result = mysql_query("SELECT * FROM $DB_TABLES[gp2009] ORDER BY suma DESC");
$i = 1;
$pop_suma = "0";

while($row = mysql_fetch_array($result)){
    if($pop_suma != $row['suma'])
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop_suma = $row['suma'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td class='suma'>".$row['suma']."</td>";
    print "<td>".$row['sulecin']."</td>";
    print "<td class='lighter'>".$row['szczecin']."</td>";
    print "<td>".$row['rumia']."</td>";
    print "<td class='lighter'>".$row['zabrze']."</td>";
    print "<td>".$row['krakow']."</td>";
    print "<td class='lighter'>".$row['slupsk']."</td>";
    print "<td>".$row['walcz']."</td>";
    print "<td class='lighter'>".$row['warszawa']."</td>";
    print "<td>".$row['wroclaw']."</td>";
    print "<td class='lighter'>".$row['lodz']."</td></tr>";
}
?>
</table>
</div>

<div id='tabs-2008'>
    <h2>Grand Prix 2008</h2>
    W roku 2008 cykl Grand Prix składał się z 10 turniejów pogrupowanych w 5 par. Do końcowej klasyfikacji zaliczano 5 turniejów - po 1 (z lepszym wynikiem) z każdej pary.
    <ol style="margin:20px;">
        <li>X Mistrzostwa Szczecina „Zanim Zakwitną Magnolie” Szczecin (8-9 marca)</li>
        <li>XII Mistrzostwa Warszawy Warszawa (12-13 kwietnia)</li>
        <li>V Mistrzostwa Katowic Katowice (26-27 kwietnia)</li>
        <li>VII Mistrzostwa Wybrzeża Rumia (3-4 maja)</li>
        <li>XIII Mistrzostwa Górnego Śląska i Zagłębia Zabrze (31 maja-1 czerwca)</li>
        <li>VIII Mistrzostwa Ziemi Ostródzkiej „Blanki w szranki” Ostróda (5-6 lipca)</li>
        <li>XII Mistrzostwa Krakowa Kraków (26-27 lipca)</li>
        <li>XII Mistrzostwa Ziemi Słupskiej Słupsk (9-10 sierpnia)</li>
        <li>IV Mistrzostwa Wałcza „Wałeckie Scrabblobranie” Wałcz (15-17 sierpnia)</li>
        <li>IV Mistrzostwa Wrocławia Wrocław (8-9 listopada)</li>
    </ol>

    W jedenastym cyklu Grand Prix poraz trzeci juz zwyciężył <b>Mariusz Skrobosz</b>. W nagrodę otrzymał pamiątkową tabliczkę oraz nagrodę pieniężną - 800 zł.<br />
    Na podium stanęli także: <b>Kamil Górka</b> z Krakowa oraz <b>Dawid Pikul</b> z Poznania.<br />
    Za kolejne miejsca nagrody pieniężne otrzymali: <b>Mateusz Żbikowski</b> i <b>Kazimierz Merklejn jr</b>.<br /><br />

    <table class="klasyfikacja">
        <tr>
            <td></td>
            <td>Imię i nazwisko</td>
            <td class='suma'>SUMA</td>
            <td>Szczecin</td>
            <td>Zabrze</td>
            <td class='lighter'>Warszawa</td>
            <td class='lighter'>Słupsk</td>
            <td>Katowice</td>
            <td>Wałcz</td>
            <td class='lighter'>Rumia</td>
            <td class='lighter'>Kraków</td>
            <td>Ostróda</td>
            <td>Wrocław</td>
        </tr>

    <?
    $result = mysql_query("SELECT * FROM $DB_TABLES[gp2008] ORDER BY suma DESC");
    $i = 1;
    $pop_suma = "0";

    while($row = mysql_fetch_array($result)){
        if($pop_suma != $row['suma'])
            print "<td class='lp'>".$i."</td>";
        else
            print "<td class='lp'>&nbsp;</td>";
        $i++;
        $pop_suma = $row['suma'];
        print "<td class='osoba'>".$row['osoba']."</td>";
        print "<td class='suma'>".$row['suma']."</td>";
        print "<td>".$row['szczecin']."</td>";
        print "<td>".$row['zabrze']."</td>";
        print "<td class='lighter'>".$row['warszawa']."</td>";
        print "<td class='lighter'>".$row['slupsk']."</td>";
        print "<td>".$row['katowice']."</td>";
        print "<td>".$row['walcz']."</td>";
        print "<td class='lighter'>".$row['rumia']."</td>";
        print "<td class='lighter'>".$row['krakow']."</td>";
        print "<td>".$row['ostroda']."</td>";
        print "<td>".$row['wroclaw']."</td></tr>";
    }
    ?>
    </table>
</div>

<div id='tabs-2007'>
    <h2>Grand Prix 2007</h2>
    Identycznie jak w roku ubiegłym cykl Grand Prix składał się z 8 turniejów, z czego 6 najlepszych zaliczano do końcowej klasyfikacji:
    <ul>
        <li>IX Mistrzostwa Szczecina "Zanim Zakwitną Magnolie"</li>
        <li>VI Mistrzostwa Wybrzeża</li>
        <li>XII Mistrzostwa Górnego Śląska i Zagłębia</li>
        <li>VII Mistrzostwa Ziemi Ostródzkiej "Blanki w szranki"</li>
        <li>XI Mistrzostwa Krakowa</li>
        <li>XI Mistrzostwa Ziemi Słupskiej</li>
        <li>III Mistrzostwa Wałcza "Wałeckie Scrabblobranie"</li>
        <li>III Mistrzostwa Wrocławia</li>
    </ul>
    W jubileuszowym - dziesiątym - cyklu Grand Prix do grona zwycięzców dołączył czwarty zawodnik. W tym roku najlepszy okazał się <b>Kazimierz Merklejn jr</b>. W nagrodę otrzymał pamiątkową tabliczkę oraz nagrodę pieniężną - 650 zł.<br />
    Na podium stanęli także: <b>Kamil Górka</b> z Krakowa i <b>Tomasz Zwoliński</b> (Skierdy).<br />
    Za kolejne miejsca nagrody pieniężne otrzymali: <b>Mariusz Skrobosz</b> i <b>Justyna Fugiel</b>.<br /><br />

    Klasyfikacja końcowa:<br /><br />
    <table class="klasyfikacja">
        <tr><td x:str="Msc">Msc</td><td x:str="Imię i nazwisko">Imię i nazwisko</td><td x:str="SUMA">SUMA</td><td x:str="Szczecin">Szczecin</td><td x:str="Rumia">Rumia</td><td x:str="Zabrze">Zabrze</td><td x:str="Ostróda">Ostróda</td><td x:str="Kraków">Kraków</td><td x:str="Słupsk">Słupsk</td><td x:str="Wałcz">Wałcz</td><td>Wrocław</td></tr>
        <tr><td>1</td><td x:str="Kazimierz.J Merklejn">Kazimierz.J Merklejn</td><td >106</td><td>25</td><td>13</td><td>6</td><td>25</td><td></td><td>11</td><td>13</td><td>19</td></tr>
        <tr><td>2</td><td x:str="Kamil Górka">Kamil Górka</td><td>97</td><td></td><td>19</td><td>22</td><td>19</td><td>11</td><td>16</td><td>10</td><td></td></tr>
        <tr><td>3</td><td x:str="Tomasz Zwoliński">Tomasz Zwoliński</td><td>94</td><td>17</td><td>2</td><td>11</td><td></td><td>19</td><td>15</td><td>22</td><td>10</td></tr>
        <tr><td>4</td><td x:str="Mariusz Skrobosz">Mariusz Skrobosz</td><td>85</td><td>4</td><td>14</td><td>16</td><td>13</td><td>13</td><td>25</td><td></td><td></td></tr>
        <tr><td>5</td><td x:str="Justyna Fugiel">Justyna Fugiel</td><td>80</td><td></td><td>22</td><td>12</td><td>17</td><td>12</td><td></td><td></td><td>17</td></tr>
        <tr><td>6</td><td x:str="Krzysztof Obremski">Krzysztof Obremski</td><td>77</td><td></td><td></td><td>9</td><td>11</td><td>25</td><td>17</td><td>15</td><td></td></tr>
        <tr><td>7</td><td x:str="Dawid Pikul">Dawid Pikul</td><td>66</td><td>10</td><td>11</td><td>13</td><td></td><td>7</td><td></td><td>25</td><td></td></tr>
        <tr><td>8</td><td x:str="Dariusz Kosz">Dariusz Kosz</td><td>64</td><td></td><td></td><td></td><td></td><td></td><td>22</td><td>17</td><td>25</td></tr>
        <tr><td>9</td><td x:str="Stanisław Rydzik">Stanisław Rydzik</td><td>56</td><td>3</td><td></td><td>7</td><td>9</td><td>17</td><td>7</td><td></td><td>13</td></tr>
        <tr><td>10</td><td x:str="Marek Roszak">Marek Roszak</td><td>54</td><td>22</td><td>12</td><td>8</td><td></td><td></td><td></td><td>12</td><td></td></tr>
        <tr><td>11</td><td x:str="Andrzej Oleksiak">Andrzej Oleksiak</td><td>53</td><td></td><td>17</td><td></td><td>22</td><td></td><td></td><td>14</td><td></td></tr>
        <tr><td>12</td><td x:str="Mateusz Żbikowski">Mateusz Żbikowski</td><td>52</td><td>14</td><td>7</td><td>15</td><td></td><td>16</td><td></td><td></td><td></td></tr>
        <tr><td>13</td><td x:str="Mariusz Wrześniewski">Mariusz Wrześniewski</td><td>40</td><td></td><td></td><td></td><td></td><td>10</td><td></td><td>8</td><td>22</td></tr>
        <tr><td>14</td><td x:str="Przemysław Herdzina">Przemysław Herdzina</td><td>37</td><td></td><td></td><td>5</td><td></td><td>22</td><td>10</td><td></td><td></td></tr>
        <tr><td>15</td><td x:str="Marek Salamon">Marek Salamon</td><td>34</td><td></td><td></td><td>25</td><td>3</td><td></td><td></td><td></td><td>6</td></tr>
        <tr><td>16</td><td x:str="Rafał Dąbrowski">Rafał Dąbrowski</td><td>34</td><td></td><td>5</td><td></td><td>10</td><td></td><td></td><td>19</td><td></td></tr>
        <tr><td>17</td><td x:str="Krzysztof Mówka">Krzysztof Mówka</td><td>32</td><td></td><td></td><td></td><td>16</td><td></td><td></td><td>16</td><td></td></tr>
        <tr><td>18</td><td x:str="Marek Reda">Marek Reda</td><td>31</td><td></td><td></td><td></td><td>8</td><td></td><td></td><td>9</td><td>14</td></tr>
        <tr><td>19</td><td x:str="Jakub Zaryński">Jakub Zaryński</td><td>30</td><td>19</td><td></td><td></td><td></td><td></td><td></td><td>11</td><td></td></tr>
        <tr><td>20</td><td x:str="Andrzej Kowalski">Andrzej Kowalski</td><td>29</td><td>13</td><td>16</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>21</td><td x:str="Jolanta Chłopińska">Jolanta Chłopińska</td><td>29</td><td></td><td>15</td><td></td><td></td><td></td><td>14</td><td></td><td></td></tr>
        <tr><td>22</td><td x:str="Joanna Juszczak">Joanna Juszczak</td><td>25</td><td></td><td>25</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>23</td><td x:str="Piotr Pietuchowski">Piotr Pietuchowski</td><td>25</td><td></td><td></td><td></td><td>14</td><td>4</td><td></td><td>7</td><td></td></tr>
        <tr><td>24</td><td x:str="Irena Sołdan">Irena Sołdan</td><td>25</td><td>11</td><td>10</td><td></td><td></td><td></td><td>4</td><td></td><td></td></tr>
        <tr><td>25</td><td x:str="Jan Kozłowski">Jan Kozłowski</td><td>24</td><td></td><td></td><td>19</td><td></td><td>5</td><td></td><td></td><td></td></tr>
        <tr><td>26</td><td x:str="Krzysztof Langiewicz">Krzysztof Langiewicz</td><td>21</td><td>15</td><td>1</td><td></td><td></td><td></td><td></td><td>5</td><td></td></tr>
        <tr><td>27</td><td x:str="Rafał Lenartowski">Rafał Lenartowski</td><td>19</td><td></td><td></td><td></td><td></td><td></td><td>19</td><td></td><td></td></tr>
        <tr><td>28</td><td x:str="Waldemar Czerwoniec">Waldemar Czerwoniec</td><td>19</td><td></td><td></td><td>17</td><td></td><td>2</td><td></td><td></td><td></td></tr>
        <tr><td>29</td><td x:str="Wojciech Sołdan">Wojciech Sołdan</td><td>19</td><td>8</td><td></td><td></td><td>5</td><td></td><td>6</td><td></td><td></td></tr>
        <tr><td>30</td><td x:str="Łukasz Tuszyński">Łukasz Tuszyński</td><td>16</td><td>16</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Jakub Nalepa">Jakub Nalepa</td><td>16</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>16</td></tr>
        <tr><td>32</td><td x:str="Sławomir Kucia">Sławomir Kucia</td><td>15</td><td></td><td></td><td></td><td></td><td>15</td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Robert Kuszpit">Robert Kuszpit</td><td>15</td><td></td><td></td><td></td><td>15</td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Mirosław Uglik">Mirosław Uglik</td><td>15</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>15</td></tr>
        <tr><td>35</td><td x:str="Łukasz Bobowski">Łukasz Bobowski</td><td>14</td><td></td><td></td><td></td><td></td><td>14</td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Bartosz Morawski">Bartosz Morawski</td><td>14</td><td></td><td></td><td>14</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>37</td><td x:str="Ewa Ciszewska">Ewa Ciszewska</td><td>14</td><td></td><td></td><td></td><td></td><td></td><td></td><td>2</td><td>12</td></tr>
        <tr><td>38</td><td x:str="Jarosław Kamienik">Jarosław Kamienik</td><td>13</td><td></td><td></td><td></td><td></td><td></td><td>13</td><td></td><td></td></tr>
        <tr><td>39</td><td x:str="Artur Irzyk">Artur Irzyk</td><td>13</td><td></td><td></td><td></td><td></td><td>6</td><td></td><td></td><td>7</td></tr>
        <tr><td>40</td><td x:str="Rafał Wesołowski">Rafał Wesołowski</td><td>12</td><td>12</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Dariusz Kuć">Dariusz Kuć</td><td>12</td><td></td><td></td><td></td><td>12</td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Łukasz Baranowski">Łukasz Baranowski</td><td>12</td><td></td><td></td><td></td><td></td><td></td><td>12</td><td></td><td></td></tr>
        <tr><td>43</td><td x:str="Paweł Janaś">Paweł Janaś</td><td>12</td><td></td><td></td><td></td><td></td><td></td><td>8</td><td></td><td>4</td></tr>
        <tr><td>44</td><td x:str="Radosław Konca">Radosław Konca</td><td>11</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>11</td></tr>
        <tr><td>45</td><td x:str="Janusz Krupiński">Janusz Krupiński</td><td>10</td><td></td><td></td><td>10</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>46</td><td x:str="Szymon Fidziński">Szymon Fidziński</td><td>9</td><td></td><td></td><td></td><td></td><td>9</td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Michał Trumpus">Michał Trumpus</td><td>9</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>9</td></tr>
        <tr><td></td><td x:str="Katarzyna Sołdan">Katarzyna Sołdan</td><td>9</td><td></td><td></td><td></td><td></td><td></td><td>9</td><td></td><td></td></tr>
        <tr><td></td><td x:str="Eugeniusz Wróblewski">Eugeniusz Wróblewski</td><td>9</td><td></td><td>9</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Andrzej Rechowicz">Andrzej Rechowicz</td><td>9</td><td>9</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>51</td><td x:str="Marek Syczuk">Marek Syczuk</td><td>8</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>8</td></tr>
        <tr><td></td><td x:str="Krzysztof Leśniak">Krzysztof Leśniak</td><td>8</td><td></td><td></td><td></td><td></td><td>8</td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Halina Roszak">Halina Roszak</td><td>8</td><td></td><td>8</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>54</td><td x:str="Karol Wyrębkiewicz">Karol Wyrębkiewicz</td><td>8</td><td></td><td></td><td>2</td><td></td><td></td><td>1</td><td></td><td>5</td></tr>
        <tr><td>55</td><td x:str="Mariusz Margalski">Mariusz Margalski</td><td>7</td><td></td><td></td><td></td><td>7</td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Anna Niedzielko">Anna Niedzielko</td><td>7</td><td>7</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>57</td><td x:str="Krzysztof Sporczyk">Krzysztof Sporczyk</td><td>7</td><td>6</td><td></td><td></td><td></td><td></td><td></td><td></td><td>1</td></tr>
        <tr><td>58</td><td x:str="Grzegorz Koczkodon">Grzegorz Koczkodon</td><td>6</td><td></td><td>6</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Grzegorz Święcki">Grzegorz Święcki</td><td>6</td><td></td><td></td><td></td><td>6</td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Kuba Koisar">Kuba Koisar</td><td>6</td><td></td><td></td><td></td><td></td><td></td><td></td><td>6</td><td></td></tr>
        <tr><td>61</td><td x:str="Marta Szeliga-Frynia">Marta Szeliga-Frynia</td><td>6</td><td></td><td></td><td>4</td><td>2</td><td></td><td></td><td></td><td></td></tr>
        <tr><td>62</td><td x:str="Wojciech Usakiewicz">Wojciech Usakiewicz</td><td>5</td><td>5</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Adam Klimont">Adam Klimont</td><td>5</td><td></td><td></td><td></td><td></td><td></td><td>5</td><td></td><td></td></tr>
        <tr><td>64</td><td x:str="Paweł Jackowski">Paweł Jackowski</td><td>5</td><td></td><td>4</td><td></td><td></td><td>1</td><td></td><td></td><td></td></tr>
        <tr><td>65</td><td x:str="Bartosz Orłowski">Bartosz Orłowski</td><td>4</td><td></td><td></td><td></td><td>4</td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Andrzej Kwiatkowski">Andrzej Kwiatkowski</td><td>4</td><td></td><td></td><td></td><td></td><td></td><td></td><td>4</td><td></td></tr>
        <tr><td>67</td><td x:str="Przemysław Pawlic">Przemysław Pawlic</td><td>3</td><td></td><td></td><td></td><td></td><td></td><td>3</td><td></td><td></td></tr>
        <tr><td></td><td x:str="Patrycja Śliwińska">Patrycja Śliwińska</td><td>3</td><td></td><td>3</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Magdalena Kublik">Magdalena Kublik</td><td>3</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>3</td></tr>
        <tr><td></td><td x:str="Kamil Nawrocki">Kamil Nawrocki</td><td>3</td><td></td><td></td><td></td><td></td><td></td><td></td><td>3</td><td></td></tr>
        <tr><td></td><td x:str="Anna Kowalska-Demczyszyn">Anna Kowalska-Demczyszyn</td><td>3</td><td></td><td></td><td>3</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Agnieszka Goniowska">Agnieszka Goniowska</td><td>3</td><td></td><td></td><td></td><td></td><td>3</td><td></td><td></td><td></td></tr>
        <tr><td>73</td><td x:str="Krzysztof Usakiewicz">Krzysztof Usakiewicz</td><td>2</td><td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Jerzy Matyasik">Jerzy Matyasik</td><td>2</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>2</td></tr>
        <tr><td></td><td x:str="Jan Ziółkowski">Jan Ziółkowski</td><td>2</td><td></td><td></td><td></td><td></td><td></td><td>2</td><td></td><td></td></tr>
        <tr><td>76</td><td x:str="Marek Herzig">Marek Herzig</td><td>1</td><td>1</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Maciej Perzyński">Maciej Perzyński</td><td>1</td><td></td><td></td><td></td><td></td><td></td><td></td><td>1</td><td></td></tr>
        <tr><td></td><td x:str="Katarzyna Klimała">Katarzyna Klimała</td><td>1</td><td></td><td></td><td>1</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td></td><td x:str="Jolanta Kosmowska">Jolanta Kosmowska</td><td>1</td><td></td><td></td><td></td><td>1</td><td></td><td></td><td></td><td></td></tr>
    </table>
</div>

<div id='tabs-2006'>
    <h2>Grand Prix 2006</h2>
    W 2006 roku cykl Grand Prix składał się z 8 turniejów, z czego 6 najlepszych zaliczano do końcowej klasyfikacji: <br />
    <ul>
        <li>VIII Mistrzostwa Szczecina "Zanim Zakwitną Magnolie"</li>
        <li>XI Mistrzostwa Górnego Śląska i Zagłębia</li>
        <li>VI Mistrzostwa Ziemi Ostródzkiej "Blanki w szranki"</li>
        <li>X Mistrzostwa Krakowa</li>
        <li>X Mistrzostwa Ziemi Słupskiej</li>
        <li>II Mistrzostwa Wałcza</li>
        <li>V Mistrzostwa Bydgoszczy</li>
        <li>VIII Mistrzostwa Podlasia</li>
    </ul>
    Już trzeci raz z rzędu zwycięzcą klasyfikacji Grand Prix został <b>Mateusz Żbikowski</b>. W nagrodę otrzymał pamiątkową tabliczkę oraz nagrodę pieniężną - 650 zł. W roku 2006 prowadził on od początku cyklu, wygrywając turniej w Szczecinie, do końca, zapewniając sobie tryumf
    już na dwa turnieje przed końcem Grand Prix. Bardziej zacięta za to walka toczyła się o kolejne miejsca i dopiero ostatni turniej GP 2006 zadecydował o podziale pozostałych miejsc na podium i przydziale nagród pieniężnych. Ostatecznie drugie miejsce wywalczył <b>Mariusz Skrobosz</b> (400 zł) ze stratą 36 punktów do Mateusza, zaś najniższe miejsce podium Grand Prix 2006 przypadło <b>Dawidowi Pikulowi</b> (300 zł). Jeszcze na turniej przed końcem drugi był <b>Kazimierz.J Merklejn</b>, jednak turniej w Bielsku nie poszedł mu najlepiej i spadł aż na 6 miejsce, tracąc nie tylko podium, ale i szansę na nagrodę pieniężną. Czwarte miejsce i 150 zł zdobył <b>Paweł Jackowski</b>, a ostatnia nagroda pieniężna za 5 miejsce stała się łupem <b>Tomasza Zwolińskiego</b> (100 zł).</p>
    Oto klasyfikacja końcowa:<br /><br />

    <table class="klasyfikacja">
        <tr><td>Msc</td><td>Imię i nazwisko</td><td>SUMA</td><td>Szczecin</td><td>Zabrze</td><td>Ostróda</td><td>Kraków</td><td>Słupsk</td><td>Wałcz</td><td>Bydgoszcz</td><td>Bielsk</td></tr>
        <tr><td>1</td><td>Mateusz Żbikowski</td><td x:fmla="=SUM(D2:K2)">134</td><td>25</td><td>15</td><td>22</td><td>22</td><td>25</td><td>25</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>2</td><td>Mariusz Skrobosz</td><td x:fmla="=SUM(D3:K3)">98</td><td>17</td><td>19</td><td>&nbsp;</td><td>14</td><td>9</td><td>14</td><td>&nbsp;</td><td>25</td></tr>
        <tr><td>3</td><td>Dawid Pikul</td><td x:fmla="=SUM(D4:K4)">93</td><td>13</td><td>6</td><td>&nbsp;</td><td>16</td><td>22</td><td>17</td><td>&nbsp;</td><td>19</td></tr>
        <tr><td>4</td><td>Paweł Jackowski</td><td x:fmla="=SUM(D5:K5)">88</td><td>12</td><td>8</td><td>8</td><td>25</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td>22</td></tr>
        <tr><td>5</td><td>Tomasz Zwoliński</td><td x:fmla="=SUM(D6:K6)">86</td><td>10</td><td>16</td><td>&nbsp;</td><td>15</td><td>19</td><td>15</td><td>&nbsp;</td><td>11</td></tr>
        <tr><td>6</td><td>Kazimierz.J Merklejn</td><td x:fmla="=SUM(D7:K7)-8">85</td><td>14</td><td>&nbsp;</td><td>14</td><td>17</td><td>17</td><td>12</td><td>11</td><td>8</td></tr>
        <tr><td>7</td><td>Stanisław Rydzik</td><td x:fmla="=SUM(D8:K8)">73</td><td>22</td><td>&nbsp;</td><td>17</td><td>&nbsp;</td><td>&nbsp;</td><td>19</td><td>&nbsp;</td><td>15</td></tr>
        <tr><td>8</td><td>Krzysztof Mówka</td><td x:fmla="=SUM(D9:K9)">42</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td></tr>
        <tr><td>9</td><td>Karol Wyrębkiewicz</td><td x:fmla="=SUM(D10:K10)">41</td><td>&nbsp;</td><td>25</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>10</td><td>Robert Duczemiński</td><td x:fmla="=SUM(D11:K11)">41</td><td>19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>22</td><td>&nbsp;</td></tr>
        <tr><td>11</td><td>Andrzej Kowalski</td><td x:fmla="=SUM(D12:K12)">40</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>19</td><td>&nbsp;</td></tr>
        <tr><td>12</td><td>Irena Sołdan</td><td x:fmla="=SUM(D13:K13)">40</td><td>&nbsp;</td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>17</td></tr>
        <tr><td>13</td><td>Radosław Konca</td><td x:fmla="=SUM(D14:K14)">38</td><td>&nbsp;</td><td>22</td><td>16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>14</td><td>Miłosz Wrzałek</td><td x:fmla="=SUM(D15:K15)">36</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>25</td><td>&nbsp;</td></tr>
        <tr><td>15</td><td>Justyna Fugiel</td><td x:fmla="=SUM(D16:K16)">30</td><td>&nbsp;</td><td>17</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>16</td><td>Kamil Górka</td><td x:fmla="=SUM(D17:K17)">29</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td>19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>17</td><td>Filip Warzecha</td><td x:fmla="=SUM(D18:K18)">29</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td>10</td><td>5</td><td>4</td><td>&nbsp;</td></tr>
        <tr><td>18</td><td>Andrzej Oleksiak</td><td x:fmla="=SUM(D19:K19)">26</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td></tr>
        <tr><td>19</td><td>Robert Kamiński</td><td x:fmla="=SUM(D20:K20)">25</td><td>&nbsp;</td><td>&nbsp;</td><td>25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>20</td><td>Marek Syczuk</td><td x:fmla="=SUM(D21:K21)">25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>17</td><td>3</td></tr>
        <tr><td>21</td><td>Wojciech Sołdan</td><td x:fmla="=SUM(D22:K22)">24</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>3</td><td>12</td><td>&nbsp;</td></tr>
        <tr><td>22</td><td>Piotr Pietuchowski</td><td x:fmla="=SUM(D23:K23)">23</td><td>&nbsp;</td><td>7</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>14</td></tr>
        <tr><td>23</td><td>Grzegorz Koczkodon</td><td x:fmla="=SUM(D24:K24)">22</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>22</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>24</td><td>Kuba Koisar</td><td x:fmla="=SUM(D25:K25)">20</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>15</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>25</td><td>Artur Koszel</td><td x:fmla="=SUM(D26:K26)">19</td><td>&nbsp;</td><td>&nbsp;</td><td>19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>26</td><td>Małgorzata Jóźwiak</td><td x:fmla="=SUM(D27:K27)">18</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>27</td><td>Aleksandra Merklejn-Kaczanowska</td><td x:fmla="=SUM(D28:K28)">17</td><td>16</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>28</td><td>Katarzyna Sołdan</td><td x:fmla="=SUM(D29:K29)">17</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>5</td></tr>
        <tr><td>29</td><td>Sławomir Kordjalik</td><td x:fmla="=SUM(D30:K30)">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Piotr Adamczyk</td><td x:fmla="=SUM(D31:K31)">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Łukasz Święcki</td><td x:fmla="=SUM(D32:K32)">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td></tr>
        <tr><td>32</td><td>Ireneusz Moralewicz</td><td x:fmla="=SUM(D33:K33)">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>33</td><td>Zdzisław Szota</td><td x:fmla="=SUM(D34:K34)">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>7</td></tr>
        <tr><td>34</td><td>Rafał Dąbrowski</td><td x:fmla="=SUM(D35:K35)">15</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>35</td><td>Marek Roszak</td><td x:fmla="=SUM(D36:K36)">15</td><td>2</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>36</td><td>Łukasz Tuszyński</td><td x:fmla="=SUM(D37:K37)">15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td></tr>
        <tr><td>37</td><td>Bartosz Morawski</td><td x:fmla="=SUM(D38:K38)">14</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>38</td><td>Przemysław Herdzina</td><td x:fmla="=SUM(D39:K39)">13</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Anna Kowalska-Demczyszyn</td><td x:fmla="=SUM(D40:K40)">13</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Jarosław Kamienik</td><td x:fmla="=SUM(D41:K41)">13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Krzysztof Obremski</td><td x:fmla="=SUM(D42:K42)">13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Jarosław Borowski</td><td x:fmla="=SUM(D43:K43)">13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td></tr>
        <tr><td>43</td><td>Andrzej Rechowicz</td><td x:fmla="=SUM(D44:K44)">13</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>44</td><td>Grzegorz Święcki</td><td x:fmla="=SUM(D45:K45)">12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td></tr>
        <tr><td>45</td><td>Sławomir Kucia</td><td x:fmla="=SUM(D46:K46)">12</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>46</td><td>Ewa Ciszewska</td><td x:fmla="=SUM(D47:K47)">12</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>47</td><td>Maciej Rzychoń</td><td x:fmla="=SUM(D48:K48)">11</td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Maciej Sancewicz</td><td x:fmla="=SUM(D49:K49)">11</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Dariusz Maciejuk</td><td x:fmla="=SUM(D50:K50)">11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>50</td><td>Jarosław Puchalski</td><td x:fmla="=SUM(D51:K51)">11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td></tr>
        <tr><td>51</td><td>Jerzy Matyasik</td><td x:fmla="=SUM(D52:K52)">10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Rafał Wesołowski</td><td x:fmla="=SUM(D53:K53)">10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Ryszard Dębski</td><td x:fmla="=SUM(D54:K54)">10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td></tr>
        <tr><td>54</td><td>Krzysztof Usakiewicz</td><td x:fmla="=SUM(D55:K55)">9</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Mariusz Wrześniewski</td><td x:fmla="=SUM(D56:K56)">9</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Małgorzata Kublik</td><td x:fmla="=SUM(D57:K57)">9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td></tr>
        <tr><td>57</td><td>Ewa Strzelczyk</td><td x:fmla="=SUM(D58:K58)">8</td><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Aleksander Puchalski</td><td x:fmla="=SUM(D59:K59)">8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td></tr>
        <tr><td>59</td><td>Jarosław Krzemiński</td><td x:fmla="=SUM(D60:K60)">7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Marek Reda</td><td x:fmla="=SUM(D61:K61)">7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>61</td><td>Bogusław Książek</td><td x:fmla="=SUM(D62:K62)">7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>62</td><td>Leszek Jóźwik</td><td x:fmla="=SUM(D63:K63)">6</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Maciej Perzyński</td><td x:fmla="=SUM(D64:K64)">6</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Łukasz Kozub</td><td x:fmla="=SUM(D65:K65)">6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Mikołaj Przybyłowicz</td><td x:fmla="=SUM(D66:K66)">6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Bogdan Kozłowski</td><td x:fmla="=SUM(D67:K67)">6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td></tr>
        <tr><td>67</td><td>Joanna Zadrożna</td><td x:fmla="=SUM(D68:K68)">5</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Michał Trumpus</td><td x:fmla="=SUM(D69:K69)">5</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Renata Kowalska</td><td x:fmla="=SUM(D70:K70)">5</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Tomasz Wiśniewski</td><td x:fmla="=SUM(D71:K71)">5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Sławomir Buk</td><td x:fmla="=SUM(D72:K72)">5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td></tr>
        <tr><td>72</td><td>Jakub Zaryński</td><td x:fmla="=SUM(D73:K73)">4</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Piotr Rosół</td><td x:fmla="=SUM(D74:K74)">4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Weronika Rudnicka</td><td x:fmla="=SUM(D75:K75)">4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Jolanta Kosmowska</td><td x:fmla="=SUM(D76:K76)">4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td></tr>
        <tr><td>76</td><td>Halina Roszak</td><td x:fmla="=SUM(D77:K77)">3</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Andrzej Grabiński</td><td x:fmla="=SUM(D78:K78)">3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Michał Szymkowski</td><td x:fmla="=SUM(D79:K79)">3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Piotr Łasowski</td><td x:fmla="=SUM(D80:K80)">3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td></tr>
        <tr><td>80</td><td>Jerzy Luter</td><td x:fmla="=SUM(D81:K81)">2</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Dariusz Kuć</td><td x:fmla="=SUM(D82:K82)">2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Janusz Kaczor</td><td x:fmla="=SUM(D83:K83)">2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td></tr>
        <tr><td>83</td><td>Rafał Lenartowski</td><td x:fmla="=SUM(D84:K84)">2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td></tr>
        <tr><td>84</td><td x:str="Magdalena Merklejn">Magdalena Merklejn</td><td x:fmla="=SUM(D85:K85)">1</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Michał Jaszczak</td><td x:fmla="=SUM(D86:K86)">1</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>&nbsp;</td><td>Beata Piotrowska</td><td x:fmla="=SUM(D87:K87)">1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td></tr>
    </table>
</div>

<div id='tabs-2005'>
    <h2>Grand Prix 2005 </h2>
    W 2005 roku cykl Grand Prix składał się z 9 turniejów, z czego 7 najlepszych zaliczano do końcowej klasyfikacji: <br />
    <ul>
        <li>X Mistrzostwa Warszawy</li>
        <li>IX Mistrzostwa Ziemi Kujawskiej</li>
        <li>IV Mistrzostwa Bydgoszczy</li>
        <li>X Mistrzostwa Górnego Śląska i Zagłębia</li>
        <li>V Mistrzostwa Ziemi Ostródzkiej</li>
        <li>IX Mistrzostwa Krakowa</li>
        <li>IV Mistrzostwa Lublina</li>
        <li>VIII Mistrzostwa Podbeskidzia</li>
        <li>XI Mistrzostwa Łodzi</li>
    </ul>
    Drugi raz z rzędu zwycięzcą klasyfikacji Grand Prix został <b>Mateusz Żbikowski</b>! W nagrodę otrzymał pamiątkową paterę oraz nagrodę pieniężną - 648 zł.
    Przed Łodzią mógł triumfować on lub <b>Kazimierz Merklejn jr</b>. Większe szanse miał Kazik, gdyż odpisywał mu się turniej za 8 punktów i była szansa na większą
    zdobycz. Jednak forma ostatnio trochę się pogorszyła i Kazik zdobył w Łodzi tylko 6 punktów (był jednak jedynym graczem, który punktował we wszystkich
    turniejach GP). Mateusz też nie poprawił wyniku - za 7 miejsce otrzymał 14 punktów. Kazik był zatem drugi i otrzymał w nagrodę 405 zł. Trzecie miejsce w Łodzi i trzecie miejsce w GP wywalczył <b>Mariusz Skrobosz</b> (nagroda 324 zł). Jego konkurenci do podium spisali się słabo - <b>Dawid Pikul </b> zdobył tylko siedem punktów, ale utrzymał czwartą pozycję i otrzymał 162 zł, a <b>Sławek Kordjalik </b>zajął dalekie 17 miejsce (4 punkty) i spadł w GP na siódmą pozycję. Dzięki
    22 punktom w Łodzi na 5 miejsce w klasyfikacji końcowej GP awansował <b>Tomasz Zwoliński</b> i otrzymał ostatnią z nagród - 81 zł<br> Oto klasyfikacja końcowa:<br /><br />

    <table class="klasyfikacja">
        <tr><td></td><td>&nbsp;</td><td>7 najl.</td><td>Warszawa</td><td>Inowrocław</td><td>Bydgoszcz</td><td>Zabrze</td><td>Ostróda</td><td>Kraków</td><td>Lublin</td><td>Bielsko-Biała</td><td>Łódź</td></tr>
        <tr><td>1</td><td>Mateusz Żbikowski</td><td>123</td><td>&nbsp;</td><td>14</td><td>15</td><td x:fmla="=18+1">19</td><td x:fmla="=22+3">25</td><td>15</td><td>16</td><td x:fmla="=18+1">19</td><td>14</td></tr>
        <tr><td>2</td><td>Kazimierz Merklejn jr</td><td>121</td><td>10</td><td x:str="'(6)">(6)</td><td>14</td><td x:fmla="=19+3">22</td><td>8</td><td>17</td><td>25</td><td x:fmla="=20+5">25</td><td>6</td></tr>
        <tr><td>3</td><td>Mariusz Skrobosz</td><td>118</td><td>19</td><td>15</td><td x:fmla="=18+1">19</td><td>13</td><td>&nbsp;</td><td>12</td><td>19</td><td>14</td><td>19</td></tr>
        <tr><td>4</td><td>Dawid Pikul</td><td>103</td><td>11</td><td>17</td><td>17</td><td>&nbsp;</td><td x:fmla="=18+1">19</td><td>11</td><td>17</td><td>11</td><td>7</td></tr>
        <tr><td>5</td><td>Tomasz Zwoliński</td><td>100</td><td>&nbsp;</td><td>11</td><td>4</td><td>5</td><td>12</td><td x:fmla="=20+5">25</td><td>15</td><td>10</td><td>22</td></tr>
        <tr><td>6</td><td>Stanisław Rydzik</td><td x:fmla="=D7+E7+F7+G7+H7+I7+K7+J7+L7">98</td><td>&nbsp;</td><td>16</td><td>13</td><td>16</td><td>&nbsp;</td><td x:fmla="=18+1">19</td><td>22</td><td>&nbsp;</td><td>12</td></tr>
        <tr><td>7</td><td>Sławomir Kordjalik</td><td x:fmla="=D8+E8+F8+G8+H8+I8+K8+J8+L8">96</td><td>&nbsp;</td><td x:fmla="=20+5">25</td><td x:fmla="=19+3">22</td><td>17</td><td>7</td><td>13</td><td>8</td><td>&nbsp;</td><td>4</td></tr>
        <tr><td>8</td><td>Kamil Górka</td><td x:fmla="=D9+E9+F9+G9+H9+I9+K9+J9+L9">85</td><td>22</td><td>&nbsp;</td><td>12</td><td>12</td><td>&nbsp;</td><td x:fmla="=19+3">22</td><td>&nbsp;</td><td>17</td><td>&nbsp;</td></tr>
        <tr><td>9</td><td>Mariusz Wrześniewski</td><td x:fmla="=D10+E10+F10+G10+H10+I10+K10+J10+L10">55</td><td>25</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>10</td><td>Krzysztof Mówka</td><td x:fmla="=D11+E11+F11+G11+H11+I11+K11+J11+L11">51</td><td>12</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td x:fmla="=19+3">22</td><td>&nbsp;</td></tr>
        <tr><td>11</td><td>Justyna Fugiel</td><td x:fmla="=D12+E12+F12+G12+H12+I12+K12+J12+L12">49</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td x:fmla="=20+5">25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td></tr>
        <tr><td>12</td><td>Marek Syczuk</td><td x:fmla="=D13+E13+F13+G13+H13+I13+K13+J13+L13">48</td><td>&nbsp;</td><td>13</td><td>1</td><td>14</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td></tr>
        <tr><td>13</td><td>Jarosław Puchalski</td><td x:fmla="=D14+E14+F14+G14+H14+I14+K14+J14+L14">40</td><td>6</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>14</td><td>Aleksander Puchalski</td><td x:fmla="=D15+E15+F15+G15+H15+I15+K15+J15+L15">39</td><td>&nbsp;</td><td>&nbsp;</td><td>25</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>15</td><td>Karol Wyrębkiewicz</td><td x:fmla="=D16+E16+F16+G16+H16+I16+K16+J16+L16">34</td><td>&nbsp;</td><td>2</td><td>11</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>17</td></tr>
        <tr><td>16</td><td>Andrzej Oleksiak</td><td x:fmla="=D17+E17+F17+G17+H17+I17+K17+J17+L17">31</td><td>&nbsp;</td><td x:fmla="=19+3">22</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>17</td><td>Paweł Frynia</td><td x:fmla="=D18+E18+F18+G18+H18+I18+K18+J18+L18">29</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td></tr>
        <tr><td>18</td><td>Radosław Konca</td><td x:fmla="=D19+E19+F19+G19+H19+I19+K19+J19+L19">28</td><td>17</td><td>3</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>19</td><td>Jarosław Michalak</td><td x:fmla="=D20+E20+F20+G20+H20+I20+K20+J20+L20">26</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>15</td></tr>
        <tr><td>20</td><td>Andrzej Kwiatkowski</td><td x:fmla="=D21+E21+F21+G21+H21+I21+K21+J21+L21">26</td><td>9</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>21</td><td>Wojciech Zemło</td><td x:fmla="=D22+E22+F22+G22+H22+I22+K22+J22+L22">25</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>25</td></tr>
        <tr><td>22</td><td>Artur Irzyk</td><td x:fmla="=D23+E23+F23+G23+H23+I23+K23+J23+L23">24</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td></tr>
        <tr><td>23</td><td>Krzysztof Usakiewicz</td><td x:fmla="=D24+E24+F24+G24+H24+I24+K24+J24+L24">22</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td x:fmla="=19+3">22</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>24</td><td>Szymon Fidziński</td><td x:fmla="=D25+E25+F25+G25+H25+I25+K25+J25+L25">20</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>25</td><td>Andrzej Kowalski</td><td x:fmla="=D26+E26+F26+G26+H26+I26+K26+J26+L26">19</td><td>&nbsp;</td><td x:fmla="=18+1">19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>26</td><td>Robert Kamiński</td><td x:fmla="=D27+E27+F27+G27+H27+I27+K27+J27+L27">19</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>27</td><td>Jerzy Matyasik</td><td x:fmla="=D28+E28+F28+G28+H28+I28+K28+J28+L28">18</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>16</td><td>&nbsp;</td></tr>
        <tr><td>28</td><td>Bogusław Puchalski</td><td x:fmla="=D29+E29+F29+G29+H29+I29+K29+J29+L29">18</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td></tr>
        <tr><td>29</td><td>Mariusz Sylwestrzuk</td><td x:fmla="=D30+E30+F30+G30+H30+I30+K30+J30+L30">18</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Michał Waszkiewicz</td><td x:fmla="=D31+E31+F31+G31+H31+I31+K31+J31+L31">18</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>11</td></tr>
        <tr><td>30</td><td>Joanna Juszczak</td><td x:fmla="=D32+E32+F32+G32+H32+I32+K32+J32+L32">18</td><td>8</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>31</td><td>Piotr Pietuchowski</td><td x:fmla="=D33+E33+F33+G33+H33+I33+K33+J33+L33">17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>32</td><td>Anna Kowalska</td><td x:fmla="=D34+E34+F34+G34+H34+I34+K34+J34+L34">17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td></tr>
        <tr><td>33</td><td>Maciej Perzyński</td><td x:fmla="=D35+E35+F35+G35+H35+I35+K35+J35+L35">16</td><td>16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Sławomir Kucia</td><td x:fmla="=D36+E36+F36+G36+H36+I36+K36+J36+L36">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>35</td><td>Radosław Sowiński</td><td x:fmla="=D37+E37+F37+G37+H37+I37+K37+J37+L37">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td></tr>
        <tr><td>36</td><td>Łukasz Kozub</td><td x:fmla="=D38+E38+F38+G38+H38+I38+K38+J38+L38">16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td></tr>
        <tr><td>37</td><td>Daniel Czernatowicz</td><td x:fmla="=D39+E39+F39+G39+H39+I39+K39+J39+L39">15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>38</td><td>Grzegorz Pukniel</td><td x:fmla="=D40+E40+F40+G40+H40+I40+K40+J40+L40">14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Mariola Osajda-Matczak</td><td x:fmla="=D41+E41+F41+G41+H41+I41+K41+J41+L41">14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>40</td><td>Przemysław Herdzina</td><td x:fmla="=D42+E42+F42+G42+H42+I42+K42+J42+L42">14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Waldemar Czerwoniec</td><td x:fmla="=D43+E43+F43+G43+H43+I43+K43+J43+L43">13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Ryszard Dębski</td><td x:fmla="=D44+E44+F44+G44+H44+I44+K44+J44+L44">13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Andrzej Rechowicz</td><td x:fmla="=D45+E45+F45+G45+H45+I45+K45+J45+L45">13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td></tr>
        <tr><td>44</td><td>Paweł Jackowski</td><td x:fmla="=D46+E46+F46+G46+H46+I46+K46+J46+L46">13</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td></tr>
        <tr><td>45</td><td>Tomasz Ciejka</td><td x:fmla="=D47+E47+F47+G47+H47+I47+K47+J47+L47">12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td></tr>
        <tr><td>46</td><td>Jolanta Kosmowska</td><td x:fmla="=D48+E48+F48+G48+H48+I48+K48+J48+L48">12</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>47</td><td>Grzegorz Koczkodon</td><td x:fmla="=D49+E49+F49+G49+H49+I49+K49+J49+L49">11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>48</td><td>Joanna Stępień</td><td x:fmla="=D50+E50+F50+G50+H50+I50+K50+J50+L50">10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>49</td><td>Andrzej Kroc</td><td x:fmla="=D51+E51+F51+G51+H51+I51+K51+J51+L51">9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td></tr>
        <tr><td>50</td><td>Bartosz Morawski</td><td x:fmla="=D52+E52+F52+G52+H52+I52+K52+J52+L52">9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Filip Sosin</td><td x:fmla="=D53+E53+F53+G53+H53+I53+K53+J53+L53">9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>52</td><td>Sergiusz Puchalski</td><td x:fmla="=D54+E54+F54+G54+H54+I54+K54+J54+L54">9</td><td>&nbsp;</td><td>4</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>53</td><td>Marta Szeliga-Frynia</td><td x:fmla="=D55+E55+F55+G55+H55+I55+K55+J55+L55">8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td></tr>
        <tr><td></td><td>Miłosz Wrzałek</td><td x:fmla="=D56+E56+F56+G56+H56+I56+K56+J56+L56">8</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Michał Musielak</td><td x:fmla="=D57+E57+F57+G57+H57+I57+K57+J57+L57">8</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>56</td><td>Paweł Stefaniak</td><td x:fmla="=D58+E58+F58+G58+H58+I58+K58+J58+L58">7</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Łukasz Bryła</td><td x:fmla="=D59+E59+F59+G59+H59+I59+K59+J59+L59">7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>58</td><td>Dariusz Ołdakowski</td><td x:fmla="=D60+E60+F60+G60+H60+I60+K60+J60+L60">6</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Rafał Matczak</td><td x:fmla="=D61+E61+F61+G61+H61+I61+K61+J61+L61">6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>60</td><td>Beata Piotrowska</td><td x:fmla="=D62+E62+F62+G62+H62+I62+K62+J62+L62">6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>5</td></tr>
        <tr><td>61</td><td>Mariusz Makuch</td><td x:fmla="=D63+E63+F63+G63+H63+I63+K63+J63+L63">5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Edward Stefaniak</td><td x:fmla="=D64+E64+F64+G64+H64+I64+K64+J64+L64">5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>63</td><td>Janusz Kaczor</td><td x:fmla="=D65+E65+F65+G65+H65+I65+K65+J65+L65">4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Marek Charytoniuk</td><td x:fmla="=D66+E66+F66+G66+H66+I66+K66+J66+L66">4</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Iwona Kucia</td><td x:fmla="=D67+E67+F67+G67+H67+I67+K67+J67+L67">4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>66</td><td>Renata Kowalska</td><td x:fmla="=D68+E68+F68+G68+H68+I68+K68+J68+L68">4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td></tr>
        <tr><td>67</td><td>Andrzej Szepczyński</td><td x:fmla="=D69+E69+F69+G69+H69+I69+K69+J69+L69">3</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Marek Roszak</td><td x:fmla="=D70+E70+F70+G70+H70+I70+K70+J70+L70">3</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Filip Klar</td><td x:fmla="=D71+E71+F71+G71+H71+I71+K71+J71+L71">3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Ewa Strzelczyk</td><td x:fmla="=D72+E72+F72+G72+H72+I72+K72+J72+L72">3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Tomasz Suchanek</td><td x:fmla="=D73+E73+F73+G73+H73+I73+K73+J73+L73">3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td></tr>
        <tr><td>72</td><td>Krzysztof Langiewicz</td><td x:fmla="=D74+E74+F74+G74+H74+I74+K74+J74+L74">2</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Krzysztof Sporczyk</td><td x:fmla="=D75+E75+F75+G75+H75+I75+K75+J75+L75">2</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Marcin Skowron</td><td x:fmla="=D76+E76+F76+G76+H76+I76+K76+J76+L76">2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Halina Roszak</td><td x:fmla="=D77+E77+F77+G77+H77+I77+K77+J77+L77">2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Piotr Adamczyk</td><td x:fmla="=D78+E78+F78+G78+H78+I78+K78+J78+L78">2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Lech Szymczak</td><td x:fmla="=D79+E79+F79+G79+H79+I79+K79+J79+L79">2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td>78</td><td>Marek Dudkiewicz</td><td x:fmla="=D80+E80+F80+G80+H80+I80+K80+J80+L80">2</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td></tr>
        <tr><td>79</td><td>Robert Soszka</td><td x:fmla="=D81+E81+F81+G81+H81+I81+K81+J81+L81">1</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Zbigniew Wietecki</td><td x:fmla="=D82+E82+F82+G82+H82+I82+K82+J82+L82">1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Michał Trumpus</td><td x:fmla="=D83+E83+F83+G83+H83+I83+K83+J83+L83">1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        <tr><td></td><td>Krzysztof Jaworski</td><td x:fmla="=D84+E84+F84+G84+H84+I84+K84+J84+L84">1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td></tr>
    </table>
</div>

<div id='tabs-2004'>
    <h2>Grand Prix 2004</h2>
    W 2004 roku cykl Grand Prix składał się z 9 turniejów, z czego 7 najlepszych zaliczano do końcowej klasyfikacji:
    <ul>
        <li>IX Mistrzostwa Warszawy</li>
        <li>VIII Mistrzostwa Ziemi Kujawskiej</li>
        <li>VI Mistrzostwa Podlasia</li>
        <li>IX Mistrzostwa Górnego Śląska</li>
        <li>IV Mistrzostwa Ziemi Ostródzkiej</li>
        <li>VIII Mistrzostwa Krakowa</li>
        <li>III Mistrzostwa Lublina</li>
        <li>VII Mistrzostwa Podbeskidzia</li>
        <li>IX Mistrzostwa Łodzi
    </ul>
    W roku 2004 zwycięzca Grand Prix był znany już przed ostatnim turniejem. <b>Mateusz Żbikowski </b>nawet jakby opuścił Mistrzostwa Łodzi, wygrałby jednym punktem - ale nie opuścił, lecz zdobył dodatkowe 7 punktów, a w nagrodę za cykl Grand Prix - 1000 zł i pamiątkową paterę. Natomiast za jego plecami odbywała się zacięta walka o kolejne miejsca na podium. Ostatecznie drugie miejsce zajął (i 500 zł wygrał) triumfator sprzed roku - <b>Mariusz Skrobosz,</b> a na trzecie miejsce, dzięki dobremu występowi w Łodzi, wskoczył <b>Stanisław Rydzik</b> (300 zł).<br />
    Oto klasyfikacja końcowa:
    <table class="klasyfikacja">
        <tr><td></td><td></td><td><b>suma</b></td><td><b>Warszawa</b></td><td><b>Inowrocław</b></td><td><b>Bielsk Podlaski</b></td><td><b>Zabrze</b></td><td><b>Ostróda</b></td><td><b>Kraków</b></td><td><b>Lublin</b></td><td><b>Bielsko-Biała</b></td><td><b>Łódź</b></td></tr>
        <tr><td><b>1</b></td><td><b>Mateusz Żbikowski</b></td><td><b>132</b></td><td>14</td><td>12</td><td>25</td><td>(4)</td><td>25</td><td>19</td><td>22</td><td>15</td><td>(7)</td></tr>
        <tr><td><b>2</b></td><td><b>Mariusz Skrobosz</b></td><td><b>122</b></td><td>(12)</td><td>16</td><td>15</td><td>25</td><td>&nbsp;</td><td>13</td><td>15</td><td>22</td><td>16</td></tr>
      <tr><td><b>3</b></td><td><b>Stanisław Rydzik</b></td><td><b>115</b></td><td>19</td><td>11</td><td>5</td><td>17</td><td>&nbsp;</td><td>(1)</td><td>25</td><td>19</td><td>19</td></tr>
      <tr><td>4</td><td>Krzysztof Mówka</td><td><b>113</b></td><td>25</td><td>13</td><td>14</td><td>(13)</td><td>(10)</td><td>15</td><td>19</td><td>14</td><td>13</td></tr>
      <tr><td>5</td><td>Kazimierz Merklejn jr</td><td><b>108</b></td><td>9</td><td>(6)</td><td>17</td><td>22</td><td>(4)</td><td>8</td><td>17</td><td>25</td><td>10</td></tr>
      <tr><td>6</td><td>Kamil Górka</td><td><b>104</b></td><td>22</td><td>22</td><td>&nbsp;</td><td>19</td><td>&nbsp;</td><td>11</td><td>9</td><td>17</td><td>&nbsp;</td></tr>
      <tr><td>7</td><td>Karol Wyrębkiewicz</td><td><b>101</b></td><td>&nbsp;</td><td>5</td><td>4</td><td>12</td><td>&nbsp;</td><td>25</td><td>14</td><td>16</td><td>25</td></tr>
      <tr><td>8</td><td>Tomasz Zwoliński</td><td><b>96</b></td><td>16</td><td>7</td><td>22</td><td>15</td><td>&nbsp;</td><td>17</td><td>12</td><td>7</td><td>&nbsp;</td></tr>
      <tr><td>9</td><td>Justyna Fugiel</td><td><b>75</b></td><td>15</td><td>19</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>6</td><td>13</td><td>&nbsp;</td><td>8</td></tr>
      <tr><td>10</td><td>Wojciech Usakiewicz</td><td><b>73</b></td><td>17</td><td>25</td><td>19</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td></tr>
    </table>
</div>

<div id='tabs-2003'>
    <h2>Grand Prix 2003</h2>
    W 2003 roku cykl Grand Prix składał się z 7 turniejów:
    <ul>
        <li>VIII Mistrzostwa Warszawy</li>
        <li>VII Mistrzostwa Częstochowy</li>
        <li>VII Mistrzostwa Ziemi Kujawskiej</li>
        <li>VIII Mistrzostwa Górnego Śląska i Zagłębia</li>
        <li>VII Mistrzostwa Krakowa</li>
        <li>III Mistrzostwa Poznania</li>
        <li>IX Mistrzostwa Łodzi</li>
    </ul>
    Do końcowej klasyfikacji zaliczało się 6 najlepszych dla każdego gracza turniejów.<br />
    Walka o GP 2003 była niezwykle zacięta i trwała do ostatniego turnieju, a nawet do ostatniej rundy. Przed GP Łodzi prowadzili ex-aequo Mariusz Skrobosz i Karol Wyrębkiewicz (po 80 pkt), przed Dariuszem Banaszkiem (73 pkt), Wojciechem Usakiewiczem (72 pkt) i Mariuszem Wrześniewskim (72 pkt). O tryumfie mogli myśleć
    również Tomasz Zwoliński (65 pkt), Krzysztof Mówka (65 pkt) i Kamil Górka (63 pkt.). W ostatniej decydującej partii (na dalekim 6 stole) <b>Mariusz
    Skrobosz</b> pokonał Dariusza Banaszka 336:319 i ponownie zwyciężył w klasyfikacji Grand Prix. Karola Wyrębkiewicza i Dariusza Banaszka wyprzedził w GP Wojciech Usakiewicz, a dzięki zajętemu drugiemu miejscu w turnieju, trzecie miejsce w GP zajął Tomasz Zwoliński.<br />
    Nagrody w Grand Prix 2003 (organizatorzy  turniejów GP wpłacali do puli po 200 zł)<br />
    I - 1000 zł + puchar<br />
    II - 300 zł<br />
    III - 100 zł<br /><br />

    Oto klasyfikacja końcowa:
    <table class="klasyfikacja">
        <tr><td></td><td></td><td><b>suma</b></td><td>Warszawa</td><td>Częstochowa</td><td>Inowrocław</td><td>Zabrze</td><td>Kraków</td><td>Poznań</td><td>Łódź</td></tr>
        <tr><td>1</td><td><b>Mariusz Skrobosz</b></td><td x:fmla="=D2+E2+F2+G2+H2+I2+J2"><b>94</b></td><td>15</td><td><b>25</b></td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td><b>25</b></td><td>14</td></tr>
        <tr><td>2</td><td><b>Wojciech Usakiewicz</b></td><td x:fmla="=D3+E3+F3+G3+H3+I3+J3"><b>88</b></td><td>&nbsp;</td><td>19</td><td>17</td><td>4</td><td>15</td><td>17</td><td>16</td></tr>
        <tr><td>3</td><td><b>Tomasz Zwoliński</b></td><td x:fmla="=D4+E4+F4+G4+H4+I4+J4"><b>87</b></td><td>&nbsp;</td><td>1</td><td>8</td><td x:fmla="=20+5"><b>25</b></td><td>17</td><td>14</td><td>22</td></tr>
        <tr><td>4</td><td>Kamil Górka</td><td x:fmla="=D5+E5+F5+G5+H5+I5+J5"><b>80</b></td><td>16</td><td>5</td><td><b>25</b></td><td>8</td><td></td><td>9</td><td>17</td></tr>
        <tr><td>5</td><td>Karol Wyrębkiewicz</td><td x:fmla="=D6+E6+F6+G6+H6+I6+J6"><b>80</b></td><td x:fmla="=18+1">19</td><td>16</td><td>5</td><td>5</td><td x:fmla="=19">19</td><td>16</td><td></td></tr>
        <tr><td>6</td><td>Dariusz Banaszek</td><td x:fmla="=D7+E7+F7+G7+H7+I7+J7"><b>75</b></td><td></td><td>12</td><td>16</td><td>17</td><td>9</td><td>19</td><td>2</td></tr>
        <tr><td>7</td><td>Mariusz Wrześniewski</td><td x:fmla="=D8+E8+F8+G8+H8+I8+J8"><b>72</b></td><td>12</td><td>8</td><td></td><td>16</td><td>14</td><td>22</td><td></td></tr>
        <tr><td>8</td><td>Krzysztof Mówka</td><td x:fmla="=D9+E9+F9+G9+H9+I9+J9"><b>65</b></td><td></td><td>10</td><td>4</td><td x:fmla="=19+3">22</td><td>16</td><td>13</td><td></td></tr>
        <tr><td>9</td><td>Mateusz Żbikowski</td><td x:fmla="=D10+E10+F10+G10+H10+I10+J10"><b>61</b></td><td></td><td></td><td>19</td><td></td><td x:fmla="=19+3">22</td><td>15</td><td>5</td></tr>
        <tr><td>10</td><td>Justyna Fugiel</td><td x:fmla="=D11+E11+F11+G11+H11+I11+J11"><b>52</b></td><td></td><td></td><td>9</td><td></td><td>8</td><td>10</td><td><b>25</b></td></tr>
        <tr><td>11</td><td>Szymon Fidziński</td><td x:fmla="=D12+E12+F12+G12+H12+I12+J12"><b>52</b></td><td>8</td><td>17</td><td>10</td><td>7</td><td>10</td><td></td><td></td></tr>
        <tr><td>12</td><td>Paweł Dawidson</td><td x:fmla="=D13+E13+F13+G13+H13+I13+J13"><b>50</b></td><td x:fmla="=19+3">22</td><td>15</td><td></td><td>13</td><td></td><td></td><td></td></tr>
        <tr><td>13</td><td>Krzysztof Usakiewicz</td><td x:fmla="=D14+E14+F14+G14+H14+I14+J14"><b>41</b></td><td>7</td><td></td><td></td><td></td><td><b>25</b></td><td></td><td>9</td></tr>
        <tr><td>14</td><td>Maciej Czupryniak</td><td x:fmla="=D15+E15+F15+G15+H15+I15+J15"><b>38</b></td><td x:fmla="=20+5"><b>25</b></td><td>13</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>15</td><td>Marek Syczuk</td><td x:fmla="=D16+E16+F16+G16+H16+I16+J16"><b>36</b></td><td></td><td>22</td><td></td><td>14</td><td></td><td></td><td></td></tr>
        <tr><td>16</td><td>Jarosław Puchalski</td><td x:fmla="=D17+E17+F17+G17+H17+I17+J17"><b>35</b></td><td>9</td><td>11</td><td>13</td><td></td><td></td><td>2</td><td></td></tr>
        <tr><td>17</td><td>Radosław Konca</td><td x:fmla="=D18+E18+F18+G18+H18+I18+J18"><b>32</b></td><td>17</td><td></td><td>15</td><td></td><td></td><td></td><td></td></tr>
        <tr><td>18</td><td>Kazimierz Merklejn jr</td><td x:fmla="=D19+E19+F19+G19+H19+I19+J19"><b>32</b></td><td>10</td><td></td><td></td><td>3</td><td>6</td><td></td><td>13</td></tr>
        <tr><td>19</td><td>Dawid Pikul</td><td x:fmla="=D20+E20+F20+G20+H20+I20+J20"><b>29</b></td><td></td><td>7</td><td></td><td></td><td>12</td><td></td><td>10</td></tr>
        <tr><td>20</td><td>Małgorzata Kania</td><td x:fmla="=D21+E21+F21+G21+H21+I21+J21"><b>25</b></td><td></td><td></td><td>6</td><td x:fmla="=18+1">19</td><td></td><td></td><td></td></tr>
        <tr><td>21</td><td>Andrzej Rechowicz</td><td x:fmla="=D22+E22+F22+G22+H22+I22+J22"><b>24</b></td><td>14</td><td>2</td><td></td><td></td><td></td><td></td><td>8</td></tr>
        <tr><td>22</td><td>Szymon Zaleski</td><td x:fmla="=D23+E23+F23+G23+H23+I23+J23"><b>22</b></td><td></td><td></td><td>22</td><td></td><td></td><td></td><td></td></tr>
        <tr><td>23</td><td>Andrzej Rakoczy</td><td x:fmla="=D24+E24+F24+G24+H24+I24+J24"><b>21</b></td><td></td><td></td><td></td><td>10</td><td>11</td><td></td><td></td></tr>
        <tr><td>24</td><td>Mariola Osajda-Matczak</td><td x:fmla="=D25+E25+F25+G25+H25+I25+J25"><b>19</b></td><td></td><td></td><td></td><td></td><td></td><td></td><td>19</td></tr>
        <tr><td>25</td><td>Marcin Kozyra</td><td x:fmla="=D26+E26+F26+G26+H26+I26+J26"><b>17</b></td><td></td><td></td><td></td><td></td><td></td><td>6</td><td>11</td></tr>
        <tr><td>26</td><td>Maciej Gałecki</td><td x:fmla="=D27+E27+F27+G27+H27+I27+J27"><b>16</b></td><td>5</td><td>9</td><td>2</td><td></td><td></td><td></td><td></td></tr>
        <tr><td>27</td><td>Iwona Baran</td><td x:fmla="=D28+E28+F28+G28+H28+I28+J28"><b>15</b></td><td></td><td></td><td></td><td></td><td></td><td></td><td>15</td></tr>
        <tr><td>28</td><td>Marek Dudkiewicz</td><td x:fmla="=D29+E29+F29+G29+H29+I29+J29"><b>15</b></td><td></td><td></td><td>12</td><td></td><td>3</td><td></td><td></td></tr>
        <tr><td>29</td><td>Aleksander Puchalski</td><td x:fmla="=D30+E30+F30+G30+H30+I30+J30"><b>15</b></td><td></td><td></td><td>14</td><td></td><td></td><td></td><td>1</td></tr>
        <tr><td>30</td><td>Radosław Sowiński</td><td x:fmla="=D31+E31+F31+G31+H31+I31+J31"><b>14</b></td><td></td><td>14</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>31</td><td>Marcin Kamiński</td><td x:fmla="=D32+E32+F32+G32+H32+I32+J32"><b>13</b></td><td>13</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>32</td><td>Grzegorz Matysiak</td><td x:fmla="=D33+E33+F33+G33+H33+I33+J33"><b>13</b></td><td></td><td></td><td></td><td></td><td>13</td><td></td><td></td></tr>
        <tr><td>33</td><td>Sławomir Kordjalik</td><td x:fmla="=D34+E34+F34+G34+H34+I34+J34"><b>12</b></td><td></td><td></td><td></td><td></td><td></td><td></td><td>12</td></tr>
        <tr><td>34</td><td>Joanna Stępień</td><td x:fmla="=D35+E35+F35+G35+H35+I35+J35"><b>12</b></td><td></td><td></td><td></td><td>12</td><td></td><td></td><td></td></tr>
        <tr><td>35</td><td>Stanisław Rydzik</td><td x:fmla="=D36+E36+F36+G36+H36+I36+J36"><b>12</b></td><td></td><td>4</td><td>7</td><td></td><td>1</td><td></td><td></td></tr>
        <tr><td>36</td><td>Andrzej Owsiany</td><td x:fmla="=D37+E37+F37+G37+H37+I37+J37"><b>12</b></td><td></td><td></td><td></td><td></td><td></td><td>12</td><td></td></tr>
        <tr><td>37</td><td>Adam Weremczuk</td><td x:fmla="=D38+E38+F38+G38+H38+I38+J38"><b>11</b></td><td>11</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>38</td><td>Paweł Jackowski</td><td x:fmla="=D39+E39+F39+G39+H39+I39+J39"><b>11</b></td><td></td><td></td><td>11</td><td></td><td></td><td></td><td></td></tr>
        <tr><td>39</td><td>Filip Sosin</td><td x:fmla="=D40+E40+F40+G40+H40+I40+J40"><b>11</b></td><td></td><td></td><td></td><td>11</td><td></td><td></td><td></td></tr>
        <tr><td>40</td><td>Sławomir Nosek</td><td x:fmla="=D41+E41+F41+G41+H41+I41+J41"><b>11</b></td><td></td><td></td><td></td><td></td><td></td><td>11</td><td></td></tr>
        <tr><td>41</td><td>Dariusz Białobrzewski</td><td x:fmla="=D42+E42+F42+G42+H42+I42+J42"><b>9</b></td><td></td><td></td><td></td><td>9</td><td></td><td></td><td></td></tr>
        <tr><td>42</td><td>Jarosław Borowski</td><td x:fmla="=D43+E43+F43+G43+H43+I43+J43"><b>8</b></td><td></td><td></td><td></td><td></td><td></td><td>8</td><td></td></tr>
        <tr><td>43</td><td>Jarosław Michalak</td><td x:fmla="=D44+E44+F44+G44+H44+I44+J44"><b>8</b></td><td>4</td><td></td><td></td><td></td><td></td><td></td><td>4</td></tr>
        <tr><td>44</td><td>Jerzy Luter</td><td x:fmla="=D45+E45+F45+G45+H45+I45+J45"><b>7</b></td><td></td><td></td><td></td><td></td><td></td><td></td><td>7</td></tr>
        <tr><td>45</td><td>Robert Duczemiński</td><td x:fmla="=D46+E46+F46+G46+H46+I46+J46"><b>7</b></td><td></td><td></td><td></td><td></td><td></td><td>7</td><td></td></tr>
        <tr><td>46</td><td>Małgorzata Sulejewska</td><td x:fmla="=D47+E47+F47+G47+H47+I47+J47"><b>7</b></td><td></td><td></td><td></td><td></td><td>7</td><td></td><td></td></tr>
        <tr><td>47</td><td>Dorota Zielińska</td><td x:fmla="=D48+E48+F48+G48+H48+I48+J48"><b>6</b></td><td></td><td></td><td></td><td></td><td></td><td></td><td>6</td></tr>
        <tr><td>48</td><td>Ryszard Dębski</td><td x:fmla="=D49+E49+F49+G49+H49+I49+J49"><b>6</b></td><td>6</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>49</td><td>Maciej Śliwa</td><td x:fmla="=D50+E50+F50+G50+H50+I50+J50"><b>6</b></td><td></td><td>6</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>50</td><td>Jacek Pietruszka</td><td x:fmla="=D51+E51+F51+G51+H51+I51+J51"><b>6</b></td><td></td><td></td><td></td><td>6</td><td></td><td></td><td></td></tr>
        <tr><td>51</td><td>Maciej Perzyński</td><td x:fmla="=D52+E52+F52+G52+H52+I52+J52"><b>5</b></td><td></td><td></td><td></td><td></td><td></td><td>5</td><td></td></tr>
        <tr><td>52</td><td>Sławomir Kucia</td><td x:fmla="=D53+E53+F53+G53+H53+I53+J53"><b>5</b></td><td>3</td><td></td><td></td><td>2</td><td></td><td></td><td></td></tr>
        <tr><td>53</td><td>Łukasz Kozub</td><td x:fmla="=D54+E54+F54+G54+H54+I54+J54"><b>5</b></td><td></td><td></td><td></td><td></td><td>5</td><td></td><td></td></tr>
        <tr><td>54</td><td>Michał Waszkiewicz</td><td x:fmla="=D55+E55+F55+G55+H55+I55+J55"><b>5</b></td><td></td><td></td><td></td><td></td><td>2</td><td></td><td>3</td></tr>
        <tr><td>55</td><td>Andrzej Oleksiak</td><td x:fmla="=D56+E56+F56+G56+H56+I56+J56"><b>4</b></td><td></td><td></td><td></td><td></td><td></td><td>4</td><td></td></tr>
        <tr><td>56</td><td>Robert Drózd</td><td x:fmla="=D57+E57+F57+G57+H57+I57+J57"><b>4</b></td><td></td><td></td><td></td><td></td><td>4</td><td></td><td></td></tr>
        <tr><td>57</td><td>Marek Roszak</td><td x:fmla="=D58+E58+F58+G58+H58+I58+J58"><b>3</b></td><td></td><td></td><td></td><td></td><td></td><td>3</td><td></td></tr>
        <tr><td>58</td><td>Krzysztof Bukowski</td><td x:fmla="=D59+E59+F59+G59+H59+I59+J59"><b>3</b></td><td></td><td>3</td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>59</td><td>Sergiusz Puchalski</td><td x:fmla="=D60+E60+F60+G60+H60+I60+J60"><b>3</b></td><td></td><td></td><td>3</td><td></td><td></td><td></td><td></td></tr>
        <tr><td>60</td><td>Anna Kowalska</td><td x:fmla="=D61+E61+F61+G61+H61+I61+J61"><b>2</b></td><td>2</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>61</td><td>Aleksandra Merklejn</td><td x:fmla="=D62+E62+F62+G62+H62+I62+J62"><b>1</b></td><td></td><td></td><td></td><td></td><td></td><td>1</td><td></td></tr>
        <tr><td>62</td><td>Joanna Juszczak</td><td x:fmla="=D63+E63+F63+G63+H63+I63+J63"><b>1</b></td><td>1</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        <tr><td>63</td><td>Kamil Nawrocki</td><td x:fmla="=D64+E64+F64+G64+H64+I64+J64"><b>1</b></td><td></td><td></td><td>1</td><td></td><td></td><td></td><td></td></tr>
        <tr><td>64</td><td>Lucyna Korga</td><td x:fmla="=D65+E65+F65+G65+H65+I65+J65"><b>1</b></td><td></td><td></td><td></td><td>1</td><td></td><td></td><td></td></tr>
    </table>
</div>

<div id='tabs-2002'>
    <h2>Grand Prix 2002</h2>
    W 2002 roku odbyło się 7 turniejów z cyklu Grand Prix:
    <ul>
        <li>VII Mistrzostwa Warszawy</li>
        <li>VI Mistrzostwa Częstochowy</li>
        <li>VI Mistrzostwa Kujaw</li>
        <li>VI Mistrzostwa Krakowa</li>
        <li>VI Mistrzostwa Piastowa</li>
        <li>VII Mistrzostwa Górnego Śląska i Zagłębia</li>
        <li>VIII Mistrzostwa Łodzi</li>
    </ul>
    Do końcowej punktacji brano pod uwagę udział w sześciu turniejach. Po latach dominacji Tomka Zwolińskiego nastąpiła zmiana na górze - Grand Prix zdobył <b>
    Mariusz Skrobosz</b> - 2 turnieje wygrał, w 3 był trzeci, a w dwóch nie zdobył punktów.
</div>

<div id='tabs-2001'>
    <h2>Grand Prix 2001</h2>
    W 2001 roku odbyło się 7 turniejów z cyklu Grand Prix:
    <ul>
        <li>VI Mistrzostwa Warszawy</li>
        <li>V Mistrzostwa Częstochowy</li>
        <li>V Mistrzostwa Krakowa</li>
        <li>V Mistrzostwa Kujaw</li>
        <li>V Mistrzostwa Piastowa</li>
        <li>VI Mistrzostwa Górnego Śląska i Zagłębia</li>
        <li>VII Mistrzostwa Łodzi</li>
    </ul>
    Do końcowej punktacji brano pod uwagę udział w sześciu turniejach. Po raz czwarty cykl wygrał <b>Tomasz Zwoliński</b>.
</div>

<div id='tabs-2000'>
    <h2>Grand Prix 2000</h2>
    W roku 2000 cykl Grand Prix składał się z 7 turniejów:
    <ul>
        <li>V Mistrzostwa Warszawy</li>
        <li>IV Mistrzostwa Częstochowy</li>
        <li>IV Mistrzostwa Krakowa</li>
        <li>IV Mistrzostwa Kujaw</li>
        <li>IV Mistrzostwa Piastowa</li>
        <li>V Mistrzostwa Górnego Śląska i Zagłębia</li>
        <li>VI Mistrzostwa Łodzi</li>
    </ul>
    Od tego roku zmieniły się zasady punktacji - w każdym turnieju GP punkty zdobywało 20 najlepszych graczy - od 1 pkt za 20 miejsce do 20 pkt za 1 miejsce.
    Dodatkowo przyznawane są punkty za miejsce pierwsze (+5 pkt, razem 25), drugie (+3 pkt, razem 22) i trzecie (+1 pkt, razem 19).<br />
    W roku 2000 do końcowej klasyfikacji brano pod uwagę udział w sześciu najlepszych dla danego gracza turniejach. Ponownie najlepszym okazał się <b>Tomasz Zwoliński</b>. <br />
    Oto klasyfikacja końcowa:
    <table class="klasyfikacja">
        <tr><td>Miejsce</td><td>Zawodnik</td><td><b>Suma</b></td><td>Warsz.</td><td>Częst.</td><td>Kraków</td><td>Inowr.</td><td>Piastów</td><td>Katow.</td><td>Łódź</td><td>Wynik</td></tr>
        <tr><td>1</td><td>Tomasz Zwoliński</td><td><b>144</b></td><td>25</td><td>25</td><td>22</td><td>22</td><td>25</td><td>6</td><td>19</td><td><b>138</b></td></tr>
        <tr><td>2</td><td>Mariusz Orzeł</td><td><b>103</b></td><td>19</td><td>&nbsp;</td><td>16</td><td>17</td><td>13</td><td>13</td><td>25</td><td><b>103</b></td></tr>
        <tr><td>3</td><td>Dariusz Banaszek</td><td><b>104</b></td><td>16</td><td>6</td><td>25</td><td>25</td><td>16</td><td>2</td><td>14</td><td><b>102</b></td></tr>
        <tr><td>4</td><td>Marcin Mroziuk</td><td><b>94</b></td><td>17</td><td>14</td><td>19</td><td>19</td><td>10</td><td>&nbsp;</td><td>15</td><td><b>94</b></td></tr>
        <tr><td>5</td><td>Paweł Dawidson</td><td><b>65</b></td><td>&nbsp;</td><td>22 </td><td>&nbsp;</td><td>&nbsp;</td><td>14</td><td>7</td><td>22</td><td><b>65</b></td></tr>
        <tr><td>6</td><td>Wojciech Usakiewicz</td><td><b>63</b></td><td>&nbsp;</td><td>13</td><td>17</td><td>10</td><td>1</td><td>22</td><td>&nbsp;</td><td><b>63</b></td></tr>
        <tr><td>7</td><td>Mariusz Skrobosz</td><td><b>55</b></td><td>14</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>22</td><td>4</td><td>&nbsp;</td><td><b>55</b></td></tr>
        <tr><td>8</td><td>Zdzisław Szota</td><td><b>54</b></td><td>15</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td>17</td><td>12</td><td>&nbsp;</td><td><b>54</b></td></tr>
        <tr><td>9</td><td>Paweł Stanosz</td><td><b>51</b></td><td>&nbsp;</td><td>19</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>14</td><td>16</td><td><b>51</b></td></tr>
        <tr><td>10</td><td>Krzysztof Usakiewicz</td><td><b>49</b></td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>16</td><td>6</td><td>19</td><td>&nbsp;</td><td><b>49</b></td></tr>
        <tr><td>11</td><td>Mariusz Kubicki</td><td><b>40</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13 </td><td>&nbsp;</td><td>17</td><td>10</td><td><b>40</b></td></tr>
        <tr><td>12</td><td>Dariusz Puton</td><td><b>36</b></td><td>9</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td><b>36</b></td></tr>
        <tr><td>13</td><td>Paweł Stefaniak</td><td><b>35</b></td><td>22</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>13</td><td><b>35</b></td></tr>
        <tr><td>14</td><td>Jarosław Michalak</td><td><b>27</b></td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>11</td><td>11</td><td><b>27</b></td></tr>
        <tr><td>15</td><td>Waldemar Czerwoniec</td><td><b>26</b></td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>19</td><td>&nbsp;</td><td>6</td><td><b>26</b></td></tr>
        <tr><td>16</td><td>Aleksandra Merklejn</td><td><b>25</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>25</td><td>&nbsp;</td><td><b>25</b></td></tr>
        <tr><td>&nbsp;</td><td>Wojciech Zemło</td><td><b>25</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>17</td><td><b>25</b></td></tr>
        <tr><td>18</td><td>Joanna Stępień</td><td><b>24</b></td><td>&nbsp;</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td><td><b>24</b></td></tr>
        <tr><td>19</td><td>Maciej Gałecki</td><td><b>20</b></td><td>8</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td><b>20</b></td></tr>
        <tr><td>&nbsp;</td><td>Zbigniew Wietecki</td><td><b>20</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td><b>20</b></td></tr>
        <tr><td>21</td><td>Piotr Jachimowicz</td><td><b>19</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td><b>19</b></td></tr>
        <tr><td>22</td><td>Jerzy Matyasik</td><td><b>19</b></td><td>&nbsp;</td><td>16</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td><b>19</b></td></tr>
        <tr><td>23</td><td>Katarzyna Stępień</td><td><b>18</b></td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td><b>18</b></td></tr>
        <tr><td>24</td><td>Andrzej Kwiatkowski</td><td><b>17</b></td><td>&nbsp;</td><td>17</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>17</b></td></tr>
        <tr><td>25</td><td>Ryszard Niżewski</td><td><b>16</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>16</td><td>&nbsp;</td><td><b>16</b></td></tr>
        <tr><td>26</td><td>Krzysztof Bukowski</td><td><b>15</b></td><td>2</td><td>&nbsp;</td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>15</b></td></tr>
        <tr><td>&nbsp;</td><td>Sławomir Nosek</td><td><b>15</b></td><td>&nbsp;</td><td>4</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>15</b></td></tr>
        <tr><td>&nbsp;</td><td>Robert Duczemiński</td><td><b>15</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>15</b></td></tr>
        <tr><td>&nbsp;</td><td>Maciej Czupryniak</td><td><b>15</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>15</td><td>&nbsp;</td><td>&nbsp;</td><td><b>15</b></td></tr>
        <tr><td>&nbsp;</td><td>Jakub Kozłowski</td><td><b>15</b></td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td><b>15</b></td></tr>
        <tr><td>&nbsp;</td><td>Maciej Michalak</td><td><b>15</b></td><td>&nbsp;</td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td><b>15</b></td></tr>
        <tr><td>32</td><td>Stanisław Rydzik</td><td><b>14</b></td><td>4</td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>14</b></td></tr>
        <tr><td>&nbsp;</td><td>Józef Burzyński</td><td><b>14</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>14</b></td></tr>
        <tr><td>&nbsp;</td><td>Stefan Dawidson</td><td><b>14</b></td><td>&nbsp;</td><td>&nbsp;</td><td>14</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>14</b></td></tr>
        <tr><td>35</td><td>Jarosław Borowski</td><td><b>13</b></td><td>13</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp; </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>13</b></td></tr>
        <tr><td>&nbsp;</td><td>Radosław Konca</td><td><b>13</b></td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>9</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td><b>13</b></td></tr>
        <tr><td>&nbsp;</td><td>Mateusz Żbikowski</td><td><b>13</b></td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td><b>13</b></td></tr>
        <tr><td>&nbsp;</td><td>Karol Wyrębkiewicz</td><td><b>13</b></td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td><b>13</b></td></tr>
        <tr><td>39</td><td>Andrzej Lożyński</td><td><b>12</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>12</b></td></tr>
        <tr><td>&nbsp;</td><td>Ryszard Dębski</td><td><b>12</b></td><td>12</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>12</b></td></tr>
        <tr><td>41</td><td>Adam Słapiński</td><td><b>11</b></td><td>11</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>11</b></td></tr>
        <tr><td>&nbsp;</td><td>Marek Charytoniuk</td><td><b>11</b></td><td>10</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td><b>11</b></td></tr>
        <tr><td>&nbsp;</td><td>Szymon Fidziński</td><td><b>11</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>2</td><td><b>11</b></td></tr>
        <tr><td>44</td><td>Mariola Osajda-Matczak</td><td><b>10</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>10</td><td>&nbsp;</td><td><b>10</b></td></tr>
        <tr><td>&nbsp;</td><td>Wiktor Stępień</td><td><b>10</b></td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td><b>10</b></td></tr>
        <tr><td>46</td><td>Rafał Matczak</td><td><b>9</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td><b>9</b></td></tr>
        <tr><td>&nbsp;</td><td>Robert Kaciński</td><td><b>9</b></td><td>&nbsp;</td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>9</b></td></tr>
        <tr><td>&nbsp;</td><td>Grażyna Puton</td><td><b>9</b></td><td>&nbsp;</td><td>9</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>9</b></td></tr>
        <tr><td>&nbsp;</td><td>Andrzej Rechowicz</td><td><b>9</b></td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td><b>9</b></td></tr>
        <tr><td>50</td><td>Piotr Mossakowski</td><td><b>8</b></td><td>&nbsp;</td><td>8</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>8</b></td></tr>
        <tr><td>51</td><td>Grzegorz Święcki</td><td><b>7</b></td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>7</b></td></tr>
        <tr><td>&nbsp;</td><td>Anna Kowalska</td><td><b>7</b></td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>7</b></td></tr>
        <tr><td>&nbsp;</td><td>Katarzyna Żuławska</td><td><b>7</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td><b>7</b></td></tr>
        <tr><td>&nbsp;</td><td>Krzysztof Noskowicz</td><td><b>7</b></td><td>&nbsp; </td><td>&nbsp;</td><td>&nbsp;</td><td>7</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>7</b></td></tr>
        <tr><td>55</td><td>Adam Grzona</td><td><b>6</b></td><td>&nbsp; </td><td>&nbsp;</td><td>&nbsp;</td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>6</b></td></tr>
        <tr><td>&nbsp;</td><td>Stanisław Gasik</td><td><b>6</b></td><td>6</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>6</b></td></tr>
        <tr><td>57</td><td>Renata Kowalska</td><td><b>5</b></td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>5</b></td></tr>
        <tr><td>&nbsp;</td><td>Jolanta Hoffmann</td><td><b>5</b></td><td>5</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>5</b></td></tr>
        <tr><td>&nbsp;</td><td>Magdalena Merklejn</td><td><b>5</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>5</td><td><b>5</b></td></tr>
        <tr><td>60</td><td>Andrzej Szepczyński</td><td><b>4</b></td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>4</b></td></tr>
        <tr><td>&nbsp;</td><td>Andrzej Szmidtke</td><td><b>4</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td><b>4</b></td></tr>
        <tr><td>&nbsp;</td><td>Andrzej Tyczyno</td><td><b>4</b></td><td>&nbsp; </td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>4</b></td></tr>
        <tr><td>63</td><td>Eugeniusz Wróblewski</td><td><b>3</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp; </td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>3</b></td></tr>
        <tr><td>&nbsp;</td><td>Krzysztof Pypno</td><td><b>3</b></td><td>&nbsp;</td><td>&nbsp;</td><td>3</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>3</b></td></tr>
        <tr><td>65</td><td>Jerzy Luter</td><td><b>2</b></td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>2</b></td></tr>
        <tr><td>&nbsp;</td><td>Mirosław Przepiórkowski</td><td><b>2</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>2</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>2</b></td></tr>
        <tr><td>67</td><td>Wojciech Gołęcki</td><td><b>1</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>1</b></td></tr>
        <tr><td>&nbsp;</td><td>Krystyna Augustyniak</td><td><b>1</b></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>1</td><td>&nbsp;</td><td><b>1</b></td></tr>
    </table>
</div>

<div id='tabs-1999'>
    <h2>Grand Prix 1999</h2>
    W roku 1999 cykl Grand Prix składał się z 14 turniejów. Do punktacji każdemu graczowi zaliczało się 1/2 turniejów + 1, czyli najlepszych 8. Zwycięzcą został
    ponownie <b>Tomasz Zwoliński</b>.
</div>

<div id='tabs-1998'>
    <h2>Grand Prix 1998</h2>
    W 1998 roku cykl Grand Prix składał się z 12 turniejów:
    <ul>
        <li>III Mistrzostwa Warszawy</li>
        <li>II Mistrzostwa Częstochowy</li>
        <li>II Mistrzostwa Krakowa</li>
        <li>IV Mistrzostwa Łodzi</li>
        <li>II Mistrzostwa Kujaw</li>
        <li>II Mistrzostwa Ziemi Szczecińskiej</li>
        <li>II Mistrzostwa Piastowa</li>
        <li>II Mistrzostwa Podlasia</li>
        <li>II Mistrzostwa Ziemi Słupskiej</li>
        <li>I Mistrzostwa Tczewa</li>
        <li>I Mistrzostwa Ziemi Sandomierskiej</li>
        <li>III Mistrzostwa Górnego Śląska i Zagłębia</li>
    </ul>
    Do końcowej klasyfikacji zaliczało się 7 najlepszych dla każdego gracza turniejów. Punkty były przyznawane za każde miejsce - ich ilość zależała
    od liczby uczestników. Oto klasyfikacja końcowa:

    <table class="klasyfikacja">
        <tr><td>1.</td><td>Tomasz Zwoliński(Warszawa)</td><td>320 pkt.</td></tr>
        <tr><td>2.</td><td>Paweł Stanosz(Warszawa)</td><td>271 pkt.</td></tr>
        <tr><td>3.</td><td>Dariusz Banaszek (Sokołów Podlaski)</td><td>266 pkt.</td></tr>
        <tr><td>4.</td><td>Małgorzata Kania(Milanówek)</td><td>229 pkt.</td></tr>
        <tr><td>5.</td><td>Dariusz Puton (Ruda Śląska)</td><td>201 pkt.</td></tr>
        <tr><td>6.</td><td>Wojciech Usakiewicz (Warszawa)</td><td>180 pkt.</td></tr>
        <tr><td>7.</td><td>Zdzisław Szota(Warszawa)</td><td>178 pkt.</td></tr>
        <tr><td>8.</td><td>Maciej Czupryniak(Milanówek)</td><td>165 pkt.</td></tr>
        <tr><td>9.</td><td>Michał Derlacki(Warszawa)</td><td>150 pkt.</td></tr>
        <tr><td>10.</td><td>Sławomir Nosek(Mielec)</td><td>144 pkt.</td></tr>
        <tr><td>11.</td><td>Stefan Dawidson(Częstochowa)</td><td>133 pkt.</td></tr>
        <tr><td  x:str="12.">12.</td><td x:str="Jolanta Hoffmann (Kraków)">Jolanta Hoffmann(Kraków)</td><td>128 pkt.</td></tr>
        <tr><td  x:str="13.">13.</td><td x:str="Andrzej Rechowicz (Katowice)">Andrzej Rechowicz(Katowice)</td><td>125 pkt.</td></tr>
        <tr><td  x:str="14.">14.</td><td x:str="Mariusz Orzeł (Nowe Miasto Lubawskie)">MariuszOrzeł (Nowe Miasto Lubawskie)</td><td>110 pkt.</td></tr>
        <tr><td  x:str="15.">15.</td><td x:str="Marek Roszak (Tczew)">Marek Roszak (Tczew)</td><td>103 pkt.</td></tr>
        <tr><td  x:str="16.">16.</td><td x:str="Paweł Stefaniak (Biała Podlaska)">Paweł Stefaniak(Biała Podlaska)</td><td>99 pkt.</td></tr>
        <tr><td  x:str="17.">17.</td><td x:str="Marcin Skrzyniarz (Warszawa)">Marcin Skrzyniarz(Warszawa)</td><td>93 pkt.</td></tr>
        <tr><td  x:str="18.">18.</td><td x:str="Marcin Mroziuk (Strzegom)">Marcin Mroziuk(Strzegom)</td><td>92 pkt.</td></tr>
        <tr><td  x:str="19.">19.</td><td x:str="Jacek Pietruszka (Kraków)">Jacek Pietruszka(Kraków)</td><td>85 pkt.</td></tr>
        <tr><td  x:str="20.">20.</td><td x:str="Waldemar Czerwoniec (Warszawa)">Waldemar Czerwoniec (Warszawa)</td><td>74 pkt.</td></tr>
        <tr><td  x:str="21.">21.</td><td x:str="Robert Duczemiński (Gorzów Wielkopolski)">Robert Duczemiński (Gorzów Wielkopolski)</td><td>73 pkt.</td></tr>
        <tr><td  x:str="22.">22.</td><td x:str="Grzegorz Święcki (Białystok)">Grzegorz Święcki(Białystok)</td><td>70 pkt.</td></tr>
        <tr><td  x:str="23.">23.</td><td x:str="Leszek Stecuła (Bytom)">Leszek Stecuła(Bytom)</td><td>63 pkt.</td></tr>
        <tr><td  x:str="24.">24.</td><td x:str="Jerzy Matyasik (Kraków)">Jerzy Matyasik(Kraków)</td><td>60 pkt.</td></tr>
        <tr><td  x:str="25.">25.</td><td x:str="Katarzyna Stępień (Katowice)">Katarzyna Stępień(Katowice)</td><td>54 pkt.</td></tr>
        <tr><td  x:str="26.">26.</td><td x:str="Zbigniew Wietecki (Gorzów Wielkopolski)">Zbigniew Wietecki (Gorzów Wielkopolski)</td><td>52 pkt.</td></tr>
        <tr><td  x:str="27.">27.</td><td x:str="Grzegorz Wiączkowski (Kielce)">GrzegorzWiączkowski (Kielce)</td><td>51 pkt.</td></tr>
        <tr><td  x:str="28.">28.</td><td x:str="Marek Antosik (Nowy Dwór Mazowiecki)">Marek Antosik (Nowy Dwór Mazowiecki)</td><td>48 pkt.</td></tr>
        <tr><td  x:str="29.">29.</td><td x:str="Mariusz Skrobosz (Warszawa)">Mariusz Skrobosz(Warszawa)</td><td>47 pkt.</td></tr>
        <tr><td  x:str="30.">30.</td><td x:str="Halina Roszak (Tczew)">Halina Roszak (Tczew)</td><td>47 pkt.</td></tr>
        <tr><td  x:str="31.">31.</td><td x:str="Mariusz Wójcik (Warszawa)">Mariusz Wójcik(Warszawa)</td><td>42 pkt.</td></tr>
        <tr><td  x:str="32.">32.</td><td x:str="Robert Kaciński (Kazimierza Wielka)">Robert Kaciński (Kazimierza Wielka)</td><td>36 pkt.</td></tr>
        <tr><td  x:str="33.">33.</td><td x:str="Wiktor Stępień (Katowice)">Wiktor Stępień(Katowice)</td><td>33 pkt.</td></tr>
        <tr><td  x:str="34.">34.</td><td x:str="Stanisław Gasik (Warszawa)">Stanisław Gasik(Warszawa)</td><td>32 pkt.</td></tr>
        <tr><td  x:str="35.">35.</td><td x:str="Józef Burzyński (Ełk)">Józef Burzyński (Ełk)</td><td>32 pkt.</td></tr>
        <tr><td  x:str="36.">36.</td><td x:str="Mariusz Gancarczyk (Mielec)">Mariusz Gancarczyk(Mielec)</td><td>31 pkt.</td></tr>
        <tr><td  x:str="37.">37.</td><td x:str="Paweł Graczykowski (Łódź)">Paweł Graczykowski(Łódź)</td><td>30 pkt.</td></tr>
        <tr><td  x:str="38.">38.</td><td x:str="Andrzej Kwiatkowski (Warszawa)">Andrzej Kwiatkowski (Warszawa)</td><td>30 pkt.</td></tr>
        <tr><td  x:str="39.">39.</td><td x:str="Edward Stefaniak (Biała Podlaska)">Edward Stefaniak (Biała Podlaska)</td><td>29 pkt.</td></tr>
        <tr><td  x:str="40.">40.</td><td x:str="Joanna Stępień (Katowice)">Joanna Stępień(Katowice)</td><td>29 pkt.</td></tr>
        <tr><td  x:str="41.">41.</td><td x:str="Ryszard Żydaczewski (Lubin)">Ryszard Żydaczewski(Lubin)</td><td>27 pkt.</td></tr>
        <tr><td  x:str="42.">42.</td><td x:str="Maurycy Piasecki (Tychy)">Maurycy Piasecki(Tychy)</td><td>26 pkt.</td></tr>
        <tr><td  x:str="43.">43.</td><td x:str="Marcin Kamiński (Warszawa)">Marcin Kamiński(Warszawa)</td><td>24 pkt.</td></tr>
        <tr><td  x:str="44.">44.</td><td x:str="Tomasz Szymański (Gorzów Wielkopolski)">TomaszSzymański (Gorzów Wielkopolski)</td><td>24 pkt.</td></tr>
        <tr><td  x:str="45.">45.</td><td x:str="Roman Figiel (Zawichost)">Roman Figiel(Zawichost)</td><td>24 pkt.</td></tr>
        <tr><td  x:str="46.">46.</td><td x:str="Katarzyna Żuławska (Katowice)">Katarzyna Żuławska(Katowice)</td><td>22 pkt.</td></tr>
        <tr><td  x:str="47.">47.</td><td x:str="Mariusz Kubicki (Inowrocław)">Mariusz Kubicki(Inowrocław)</td><td>22 pkt.</td></tr>
        <tr><td  x:str="48.">48.</td><td x:str="Andrzej Lożyński (Piastów)">Andrzej Lożyński(Piastów)</td><td>22 pkt.</td></tr>
        <tr><td  x:str="49.">49.</td><td x:str="Magdalena Merklejn (Szczecin)">Magdalena Merklejn(Szczecin)</td><td>22 pkt.</td></tr>
        <tr><td  x:str="50.">50.</td><td x:str="Ryszard Dębski (Biała Podlaska)">Ryszard Dębski(Biała Podlaska)</td><td>21 pkt.</td></tr>
        <tr><td  x:str="51.">51.</td><td x:str="Adam Paszek (Tarnowskie Góry)">Adam Paszek(Tarnowskie Góry)</td><td>21 pkt.</td></tr>
        <tr><td  x:str="52.">52.</td><td x:str="Andrzej Chmielewski (Warszawa)">Andrzej Chmielewski (Warszawa)</td><td>21 pkt.</td></tr>
        <tr><td  x:str="53.">53.</td><td x:str="Leszek Nowak (Myślibórz)">Leszek Nowak(Myślibórz)</td><td>21 pkt.</td></tr>
        <tr><td  x:str="54.">54.</td><td x:str="Adam Grzona (Toruń)">Adam Grzona (Toruń)</td><td>20 pkt.</td></tr>
        <tr><td  x:str="55.">55.</td><td x:str="Paweł Dawidson (Częstochowa)">Paweł Dawidson(Częstochowa)</td><td>19 pkt.</td></tr>
        <tr><td  x:str="56.">56.</td><td x:str="Walenty Siemaszko (Lipiany)">Walenty Siemaszko(Lipiany)</td><td>18 pkt.</td></tr>
        <tr><td  x:str="57.">57.</td><td x:str="Krzysztof Usakiewicz (Warszawa)">Krzysztof Usakiewicz (Warszawa)</td><td>17 pkt.</td></tr>
        <tr><td  x:str="58.">58.</td><td x:str="Rafał Matczak (Pruszków)">Rafał Matczak(Pruszków)</td><td>15 pkt.</td></tr>
        <tr><td  x:str="59.">59.</td><td x:str="Jarosław Michalak (Łódź)">Jarosław Michalak(Łódź)</td><td>14 pkt.</td></tr>
        <tr><td  x:str="60.">60.</td><td x:str="Renata Kowalska (Bielsko-Biała)">Renata Kowalska(Bielsko-Biała)</td><td>14 pkt.</td></tr>
        <tr><td  x:str="61.">61.</td><td x:str="Irena Stefaniak (Biała Podlaska)">Irena Stefaniak(Biała Podlaska)</td><td>13 pkt.</td></tr>
        <tr><td  x:str="62.">62.</td><td x:str="Kazimierz M. Merklejn (Szczecin)">Kazimierz M. Merklejn (Szczecin)</td><td>13 pkt.</td></tr>
        <tr><td  x:str="63.">63.</td><td x:str="Wiesław Olewiński (Warszawa)">Wiesław Olewiński(Warszawa)</td><td>12 pkt.</td></tr>
        <tr><td  x:str="64.">64.</td><td x:str="Grzegorz Kulawiak (Warszawa)">Grzegorz Kulawiak(Warszawa)</td><td>10 pkt.</td></tr>
        <tr><td  x:str="65.">65.</td><td x:str="Krystyna Augustyniak (Łódź)">Krystyna Augustyniak(Łódź)</td><td>7 pkt.</td></tr>
        <tr><td  x:str="66.">66.</td><td x:str="Jacek Gigoła (Gorzów Wielkopolski)">Jacek Gigoła(Gorzów Wielkopolski)</td><td>7 pkt.</td></tr>
        <tr><td  x:str="67.">67.</td><td x:str="Krzysztof Bukowski (Nowe Miasto Lubawskie)">Krzysztof Bukowski (Nowe Miasto Lubawskie)</td><td>6 pkt.</td></tr>
        <tr><td  x:str="68.">68.</td><td x:str="Witold Kurowski (Łódź)">Witold Kurowski(Łódź)</td><td>2 pkt.</td></tr>
        <tr><td  x:str="69.">69.</td><td x:str="Aleksandra Merklejn (Szczecin)">Aleksandra Merklejn (Szczecin)</td><td>2 pkt.</td></tr>
        <tr><td  x:str="70.">70.</td><td x:str="Kazimierz Merklejn (Lądek Zdrój)">Kazimierz Merklejn (Lądek Zdrój)</td><td>2 pkt.</td></tr>
        <tr><td  x:str="71.">71.</td><td x:str="Ireneusz Kurzok (Mikołów)">Ireneusz Kurzok(Mikołów)</td><td>2 pkt.</td></tr>
        <tr><td  x:str="72.">72.</td><td x:str="Bogusław Koszarski (Lądek Zdrój)">Bogusław Koszarski (Lądek Zdrój)</td><td>1 pkt.</td></tr>
    </table>
</div>
</div>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
