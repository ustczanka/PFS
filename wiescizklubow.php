<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Kluby : Wieści z klubów</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("kluby","wiescizklubow");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Wieści z klubów")</script></h1>


<a href="#news14">Drugie urodziny Sonata Scrabble (09.02.2014)</a><br />
<a href="#news13">W Świno tez gramy! (20.01.2012)</a><br />
<a href="#news12">Scrabble's Day, czyli w słowach nie oszukujemy (4.04.2011)</a><br />
<a href="#news11">Kolejny jubileusz w "Siódemce" - 300. turniej (19.06.2010)</a><br />
<a href="#news10">Festiwal Scrabble - Siódemka w Empiku (22.05.2010)</a><br />
<a href="#news9">Klubowe potyczki - Blank : Same Premie (31.05.2009)</a><br />
<a href="#news8">Klubowe potyczki - WKS Siódemka : ŁKMS (14-15.03.2009)</a><br />
<a href="#news7">Jubileusz we Wrocławskim Klubie Scrabble "Siódemka" (7-8.03.2009)</a><br />
<a href="#news6">ŁKMS - WKS Siódemka (63:80) (1-2.03.2008)</a><br />
<a href="#news5">15-literówka na 510 spotkanie Górnośląskiego Klubu Scrabble (16.01.2008)</a><br />
<a href="#news4">500 spotkanie Górnośląskiego Klubu Scrabble (18.10.2007)</a><br />
<a href="#news3">Klubowe potyczki - Blank : Same Premie (17.12.2006)</a><br />
<a href="#news2">Klubowe potyczki - Blank Szczecin : SCRABBLISTA Trójmiasto (11-12.03.2006)</a><br />
<a href="#news1">Klubowe potyczki - Blank : Same Premie (19.02.2006)</a><br />


<h2 id="news14">Drugie urodziny Sonata Scrabble </h2>
<p class="relacja">
<img src="../rozne/sonata2lecie.jpg" width="300" alt="" class="onleft"/>

Obchody drugiego jubileuszu Sonaty Scrabble rozpoczęły się już w sobotę 08.02. O godzinie 19. W kawiarni Sonata czekała na nas gościnna jak zawsze <b>Hania Mazowiecka</b> z pysznymi przekąskami i wszelkiej maści napojami. Impreza trwała do białego rana, a czas mijał szybko pośród wspominek i dyskusji, m.in. na temat tego, kto założył świnoujski klub scrabble, gdyż, jak się okazało, zasługę tę przypisuje sobie kilku świnoujścian (pozdrawiamy Łukasza i Pawła ;)).  Nie zabrakło również elementu kulturalnego w postaci  koncertu fortepianowego, którym spontanicznie uraczył nas utalentowany przyjaciel klubu z Polic - <b>Michał Landowski</b>, zdobywając serca wszystkich pań i podziw panów (nie bez domieszki zazdrości). <br><br> 
 Mimo iż wielu uczestników sobotnich uroczystości udało się na spoczynek o świcie, wszyscy dzielnie stawili się na jubileuszowy turniej w Sonacie o godzinie 12. Nie zabrakło mocnej ekipy reprezentującej szczeciński klub scrabble <b>Blank</b>, która, jak się póżniej okazało, postanowiła unaocznić "Sonatczanom", że długoletnia praktyka i kawałek życia poświęcony na naukę słówek ewidentnie przekładają się na wyniki. Na szczęście w klubie Sonata Scrabble nikomu nie brakuje najważniejszego elementu niezbędnego do osiągnięcia sukcesu, czyli zapału do gry, więc optymistycznie można stwierdzić, że efekty przyjdą z czasem. :)<br><br>
Do niedzielnych potyczek przystąpiło 17 zawodników (zawody tenisa stołowego tudzież powrót syna z Ameryki stanęły na przeszkodzie, by było ich 19). Już po 6 rundach wyłonił się zwycięzca, którym, po wygraniu wszystkich rozegranych partii bezapelacyjnie został "Blankowicz" - <b>Maciej Grąbczewski</b>, gromadząc jednocześnie 3216 małych punktów. Drugie miejsce zajęła również przedstawicielka szczecińskiego klubu - <b>Dorota Kuć</b>, tym samym zdobywając miano najlepszej scrabblistki turnieju. Trzecie miejsce udało się wywalczyć piszącej tę relację. Spośród pozostałych przedstawicieli świnoujskiego klubu wyróżniające się wyniki osiągnęli <b>Paweł Wdziękoński</b> i <b>Łukasz Przyszlak</b>, zajmując odpowiednio 5. i 7. pozycję. Natomiast w jednej z rund imponującą liczbę małych punktów (651) zdobył <b>Jakub Zaryński</b>. Zwycięzca nie odjechał z pustymi rękami - w nagrodę otrzymał upatrzony trunek z kawiarnianej oferty - taka bowiem była obietnica hojnej gospodyni.<br><br> 
Podsumowując, drugie urodziny Sonaty Scrabble przebiegły w sympatycznej, rodzinnej atmosferze (albo, pożytkując się niebanalnym scrabblowym słówkiem ułożonym w niedzielę przez Macieja - było ALRIGHT! :)) Nie potrzeba żadnych nadprzyrodzonych zdolności by przewidzieć, że duch walki i pasja utrzyma się w Sonacie przez długie lata, zwłaszcza w tak sprzyjających towarzyskich okolicznościach, z których świnoujski klub słynie od samego początku swego istnienia. Za rok mamy jedynie nadzieję gościć jeszcze więcej scrabblistów spoza krainy 44 wysp.
<br> <br>
<i> Relacja: Natalia Woźniak (with a little help from my friends)</i><br> <br>

<a href="https://picasaweb.google.com/111288975904311020713/DwulecieKlubuSonata#" target="_blank">Galeria zdjęć</a>
<br><br></p>


<h2 id="news13">W Świno też gramy! </h2>
<p class="relacja">
<img src="../rozne/scrabble_uznam.jpg" width="330" alt="" class="onleft"/>
U nas nad morzem to krótka piłka jest. Tu działamy, organizujemy się. Nie marudzimy, nie miauczymy. To pewnie ten jod w naszym powietrzu...<br><br>
<p>No ale po kolei!<br><br>
Coroczną świnoujską tradycją jest Towarzyski Turniej Scrabble rozgrywany dzięki zaangażowaniu i gościnności Hani Mazowieckiej - dysponującej tyleż dobrą wolą i energią organizacyjną, co i przytulną kawiarnią w prestiżowej lokalizacji. Kilkanaście osób, które przewinęły się przez tegoroczne rozgrywki znamionowało klubowy potencjał -  tej energii, tego materiału nie wolno było zmarnować! Spotkanie organizacyjne, konkretne decyzje - i w konsekwencji pierwszy z cyklicznych, cotygodniowych klubowych turniejów. Lekko stremowani - obecnością chyba wszystkich lokalnych mediów, lekko nadwyrężeni - walką z zegarami pamiętającymi chyba jeszcze Mistrzostwa RWPG, ale z otwartymi głowami i w świetnej atmosferze przebrnęliśmy przez historyczne cztery rundy pierwszego oficjalnego turnieju w świnoujskim klubie.<br><br>
Teraz gramy co tydzień, uczymy się, integrujemy. Nie rozpierzchamy się i nie garbimy przed komputerem.<br>
Wiadomo - jod!<br><br></p>

<i>Jakub Zaryński<br>
Bartosz Turlejski (foto)</i><br><br>
<a href="http://tvswinoujscie.pl/201201201498/scrabblisci-czekaja-na-was.html" target="_blank">Relacja w Telewizji Świnoujście</a><br><br>

<!--<a href="http://scrabble.rootnode.net/sonata/" target="_blank">Wyniki turniejów i statystyki klubowe</a><br><br>-->

</p>










<h2 id="news12s">Scrabble's Day, czyli w słowach nie oszukujemy</h2>
<p class="relacja">
1 kwietnia w tym roku w naszym klubie był dniem wyjątkowo na serio. Wraz z Pasażem Grunwaldzkim i agencją Royal Brand PR podjęliśmy się organizacji scrabblowego eventu w galerii handlowej. Chyba nikt z licznie zgromadzonych klubowiczów nie spodziewał się aż takiego zainteresowania imprezą.<br><br>

Można śmiało stwierdzić, że była to największa i najlepsza reklama dla naszego klubu w jego wieloletniej historii. Ludzi przyciągnęły do nas nie tylko atrakcyjne nagrody (stuzłotowe bony do Empiku w Pasażu Grunwaldzkim), ale także ciekawość ludzi. Wielu z nich grała w scrabble nie raz i nie dwa w zaciszu domowego ogniska, gdzie zdarzało się im konkurować z babcią, tatą, córką czy też ze znajomymi podczas mniej lub bardziej udanych imprez.<br><br>

Konkursy wymyślone przez Aztecha oraz Boda były rozwiązywane na tak wysokim poziomie, że sami twórcy byli pod wrażeniem. Wystarczy tu wspomnieć, że dwunastolatek z wyrazu OCIEPLENIE wysubanagramował PLIOCEN, czyli drugą epokę neogenu, a konkurs, w którym trzeba było odnaleźć nazwy jak największej ilości sklepów, ku zazdrości konkursowiczek wygrał mężczyzna!<br><br>

Tak duże zainteresowanie spowodowane było na pewno także dzięki dwójce konferansjerów, którzy niczym znany z Tour de France duet Jaroński-Wyrzykowski nie trzymali się konwenansów i potrafili zainteresować klientów pasażu. Wobrem i Pixel, bo o nich mowa zachęcali ludzi do zwrócenia uwagi na naszą akcję, ale to Beata okazała się osobą, która potrafiła przekonać nawet najbardziej zatwardziałych przeciwników gier losowych, do spróbowania swoich sił w konkursach.<br><br>

5 godzin, podczas których pokazaliśmy wrocławianom, że scrabble to naprawdę dobra zabawa zleciało nam w try miga. Nikt z nas się nie nudził, ponieważ cały czas ktoś nas zaczepiał i pytał o scrabble, bądź o możliwość towarzyskiej partii. Wielu odwiedzających Pasaż zapowiedziało się, że wpadnie do nas do klubu, by zagrać bardziej na poważnie przy scrabblownicy.<br><br>

Impreza nie doszłaby do skutku gdyby nie chęci i wsparcie Pasażu Grunwaldzkiego oraz agencji Royal Brand, jednak przede wszystkim nie byłoby o czym mówić bez scrabblistów z naszego klubu. Siódemka po raz kolejny pokazała, że popularyzacja scrabbli przychodzi jej bardzo łatwo.<br><br>

<a href="https://picasaweb.google.com/wks.siodemka/ScrabbleSDayWPasazuGrunwaldzkim#" target="_blank">Zdjęcia ze Scrabble's Day.</a><br><br>


</p>





<h2 id="news11">Kolejny jubileusz w "Siódemce" - 300. turniej </h2>
<p class="relacja">
Zamiast maratonu scrabblowego zapowiadanego na miniony weekend na Dolnym Śląsku Siódemkowicze, zarówno ci zrzeszeni jak i nie (oraz przyjaciele Siódemki) mieli okazję spotkać się w Górach Bystrzyckich. Wszystko to za sprawą Rysia Korpalskiego, który przygarnął do swojej górskiej chaty dolnoślązaków oraz zbłąkanego mazowszanina.<br><br>

Spotkanie rozpoczęło się w piątek od grilla na którym smażyły się indyki i karkówki. Oprócz zakąszania zebrani w Wójtowicach zajmowali się piciem różnych rodzajów trunków oraz zasypianiem przy meczu Anglia - Algieria. Impreza nie trwała do późna, gdyż już następnego dnia wszyscy dość wcześnie zebrali się na śniadaniu, zbierając siły na wyprawę ścieżką dedaktyczno-edukacyjną. Dzięki ścieżce i tablicom dedaktycznym poznaliśmy wiele rodzajów szyszek. Długą i nieciekawą podróż (bo w końcu wszystkie widoki zasłaniały góry) umilaliśmy sobie grając w ulubioną grę Siódemkowiczów. I tak Marcin był niespodzianką, Fotu niedzielą palmową, a Labi nie był wbrew jego domysłom powiązany z wielorybami, ani z biciem Niemca po kasku.<br><br>

W wesołej atmosferze dotraliśmy do Spalonej, chociaż nie widziałem na miejscu żadnych zgliszczy. Nawet jakby coś się tam jeszcze jarało, to wody było pod dostatkiem w pobliskich stawach. Ze stawów Rysio, Marcin i Wojtek dzielnie złowili nam obiad, wygrywając tym samym zawody pstrongmenów. Powrót był znacznie krótszy dla niemal wszystkich z wyjątkiem Greena i Asi, którzy bardzo lubią adrenalinę i postanowili zabłądzić w górach. Szczęśliwie obyło się bez telefonu do GOPR-u, gdyż wrócili na tyle szybko, że nikt nie zdążył się jeszcze stresować.<br><br>

Z zakwasami w nogach rozpoczęliśmy 300. turniej siódemki. Najlepszą taktykę zdecydowanie zastosował Suchy, który nie poszedłszy w góry zaoszczędził tak dużo sił, że ogrywał niemal wszystkich jak leci. Do ósmej rundy nie miał porażki na koncie, ale po piętach deptała mu Fotu. Dla jeszcze większych emocji Tomek przegrał w przedostatniej rundzie, jednak w ostatniej nie dał sobie zabrać zwycięstwa w turnieju. Na uwagę zastępuje także dobry występ debiutującego imiennika kandydata na urząd Prezydenta Rzeczypospolitej Polskiej - Bronka Parady. Z pięciu partii wygrał trzy zaskakując wielu doświadczonych graczy swoimi umiejętnościami. Ponadto polaniczanin-zdrojanin zapowiedział swój udział w turnieju PFS we Wrocławiu. Niestety, nie wiedzieć czemu nie zechciał skorzystać z zaproszenia na turniej do Ełku.<br><br>

Po wyczerpujących 10 rundach turnieju część poległa od razu w łóżkach, inni zaś jeszcze o czymś debatowali (ale sam tego nie wiem :P). Następnego dnia odbył się kolejny 301. turniej. Wiele osób przestrzegało Wieloryba przed wyczynem Dominika, który ongiś wygrał turniej, a na następnym spotkaniu był ostatni. Jednak Tomek szedł jak anagram ze słowa arbuz i przed ostatnią rundą już tylko wołominianin mógł mu przeszkodzić w drugim triumfie z rzędu. Za sprawą dziewięciokrotnych wigwamów tak się nie stało i z kompletem zwycięstw Aztech wygrał 301. turniej. Potem był znowu grill, jednak niektórzy już musieli się zbierać do Wrocławia i tam oddać głos w wyborach na prezydenta, dlatego w tym miejscu musi skończyć się opowieść o tym, co działo się w Wójtowicach.<br><br>

Mimo to, mogę opowiedzieć o tym, co działo się we Wrocławiu, o izopletach i innych ciekawostkach, jednak póki co potrzebujemy wraz ze śledczym Grzegorzem W. większego materiału dowodowego.<br><br>

<span class="autor">Marek Reda(ktor)</span>
</p>






<h2 id="news10">Festiwal Scrabble - Siódemka w Empiku</h2>
<p class="relacja">
Przygotowany przez WKS "Siódemka" Festiwal Scrabble był pierwszym tego typu wydarzeniem w Polsce. Uczestnicy festiwalu mogli wystartować w wielu konkursach, zasiąść w kąciku nauki gry, zmierzyć się z Mistrzem Polski. Największym zainteresowaniem cieszyła się belgijka. Najlepszym w tej konkurencji okazał się Paweł Mazurek z Polanicy-Zdroju. Paweł odnosił również sukcesy w pozostałych konkursach i być może wkrótce zobaczymy tego zawodnika na jednym z ogólnopolskich turniejów. Na razie zapowiada swoje przybycie na klubowy turniej w Wójtowicach w czerwcu.<br><br>

Wśród konkursów-anagramów furorę zrobił konkurs polegający na anagramowaniu nazwisk piłkarzy i klubów oraz połączenie ich odpowiednio w pary. Płeć piękna również startowała w tej konkurencji próbując odgadnąć, jaki klub reprezentuje Mirosław Szopek :)<br><br>

Anagramy kwiatów nie sprawiały uczestnikom festiwalu większych trudności, kłopoty wystąpiły jedynie przy odgadywaniu "TRUMANIU" i "LOLAGIDA" - nawet Siódemkowicze musieli się nad tym trochę głowić. Zwycięzcą toczącego się cały dzień konkursu na najdroższe otwarcie został Miłosz Futyna z Wrocławia. Dziecko szczęścia (jak sam siebie nazywa) ułożyło wyraz "SPOŻYJĘ" za 94 punkty. Atrakcją dnia była symultana z Mistrzem Polski Darkiem Koszem. Aktualny Mistrz Scrabble zmierzył się z 11 przeciwnikami, wygrywając 8 partii. Gratulujemy!<br><br>

Specjalne podziękowania należą się Empikowi, który pełnił rolę gospodarza festiwalu we wrocławskiej RENOMIE oraz ufundował liczne nagrody dla uczestników imprezy. Festiwal nie odbyłby się, gdyby nie wielkie zaangażowanie i praca Grzegorza Wiączkowskiego oraz prowadzącego festiwal Wojtka Obremskiego - dziękujemy.<br><br>

Pierwsze koty za płoty :)<br><br>

PS na deser anagram piłkarza autorstwa kobrema:<br>

NDABENKULU NCUBE - klub: DIKSZIEDOBPE SKLEIBO-ŁAIBA - powodzenia!<br>

Uwaga: pierwszeństwo w zgadywaniu ma Marek Reda z Wołomina - pozdrawiamy :)<br>


<span class="autor">Beata Wiączkowska</span>
</p>



<h2 id="news9">Klubowe potyczki - Blank : Same Premie</h2>
<p class="relacja">
<b>Okiem gorzowianina:</b><br /><br />
<img src="kluby/szczecin_gorzow_0609.jpg" alt="" class="onright"/>
W ostatnią niedzielę maja 2009r., po dwu i półrocznej przerwie odbył się kolejny mecz między
zaprzyjaźnionymi klubami ze Szczecina i Gorzowa. Po raz kolejny gospodarzem był Grodzki
Dom Kultury w Gorzowie z Sergiuszem Sokołowem na czele. Tym razem kluby wystawiły aż
po ośmiu graczy, to aż dotyczy szczególnie GKS Same Premie ponieważ na poniedziałkowych
klubowych spotkaniach trudno o tak wysoką frekwencję.<br />
Spotkanie zaczęło się o 10 z minutami w słonecznym poranku, zaplanowanych zostało 8 rund,
każdy z każdym. Rundę 1 wygrał BLANK 6:2, w kolejnej lepszym okazali się gracze Samych
Premii (Dorota, Agata, Józek, Radek, Marcin, Sławek, Robert i Zbyszek) wygrywając 5:3,
w kolejnych trzech rundach zdecydowanie lepszymi byli Ola, Magda, Monika, Basia, Jarek,
Piotrek, Maciek i wypożyczony Walenty. Po 5 rundach Blankowicze prowadzili 28:12, w szóstej
rundzie Gorzów wygrał nieznacznie 4,5 : 3,5 i było jasne, że ostatnie dwie rundy należałoby wygrać
po 8:0 żeby wygrać cały mecz, okazało się to marzeniem "ściętej głowy".
Ostatecznie Szczeciński Klub Scrabble "BLANK" wygrał po raz drugi z GKS "Same Premie"
41:23 ( w tym dwa remisy). Na pociechę zostaje tylko fakt, że najlepszym indywidualnie graczem,
dzięki małym pkt., został piszący te słowa FIDO68..<br />
Tuż przed wyjazdem miłych Blankowiczów niebo rozpłakało się nad "ściętymi głowami" Samych Premii.
Mam nadzieję że następnym razem aura będzie Nam przyjazna przez cały dzień zawodów.<br /><br />

<table class="klasyfikacja">
<td style="height: 15pt; width: 133pt;" width="177" height="20">Zawodnik</td>
  <td style="width: 74pt;" width="98">Klub</td>
  <td style="width: 38pt;" width="51">DP</td>
  <td style="width: 40pt;" width="53">MP</td>
 </tr>
 <tr style="height: 15pt;" height="20">

  <td style="height: 15pt;" height="20">Wietecki Zbigniew</td>
  <td>Same Premie</td>
  <td>7</td>
  <td>3205</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Żuliński Jarosław</td>

  <td>Blank</td>
  <td>7</td>
  <td>3120</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Kupczyńska Magdalena</td>
  <td>Blank</td>

  <td>7</td>
  <td>3102</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Siemaszko Walenty</td>
  <td>Blank</td>
  <td>5,5</td>
  <td>2993</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Wierzchowski Piotr</td>
  <td>Blank</td>
  <td>5,5</td>
  <td>2925</td>

 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">GrąbczewskiMaciej</td>
  <td>Blank</td>
  <td>5</td>
  <td>3084</td>
 </tr>

 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Kaczanowska Aleksandra</td>
  <td>Blank</td>
  <td>4</td>
  <td>3107</td>
 </tr>
 <tr style="height: 15pt;" height="20">

  <td style="height: 15pt;" height="20">Gawron-Cieślik Monika</td>
  <td>Blank</td>
  <td>4</td>
  <td>2742</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Duczemiński Robert</td>

  <td>Same Premie</td>
  <td>3</td>
  <td>3104</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Wietecki Józef</td>
  <td>Same Premie</td>

  <td>3</td>
  <td>2866</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Piotrowski Sławomir</td>
  <td>Same Premie</td>
  <td>3</td>
  <td>2827</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Paprota Marcin</td>
  <td>Same Premie</td>
  <td>3</td>
  <td>2568</td>

 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Turbo Barbara</td>
  <td>Blank</td>
  <td>3</td>
  <td>2664</td>
 </tr>

 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Pastusiak Radosław</td>
  <td>Same Premie</td>
  <td>2,5</td>
  <td>2546</td>
 </tr>
 <tr style="height: 15pt;" height="20">

  <td style="height: 15pt;" height="20">Pastusiak Agata</td>
  <td>Same Premie</td>
  <td>1</td>
  <td>2309</td>
 </tr>
 <tr style="height: 15pt;" height="20">
  <td style="height: 15pt;" height="20">Franków Dorota</td>

  <td>Same Premie</td>
  <td>0,5</td>
  <td>2397</td>
</table>
<span class="autor">Zbyszek Wietecki</span>
</p>




<h2 id="news8">WKS Siódemka - ŁKMS (67:65)</h2>
<SCRIPT language = javascript>
function toggle(pole) {
if (document.getElementById(pole).style.display == 'none')
{ document.getElementById(pole).style.display = 'block'; }
else
{ document.getElementById(pole).style.display = 'none'; }
}

</SCRIPT>
<img src="kluby/siodemka-lkms2009.jpg" alt="" class="onright"/>
<p class="relacja">
	W dniach 14-15 marca br. odbył się w Załęczu scrabblowy mecz pomiędzy Łódzkim Klubem Miłośników Scrabble oraz WKS Siódemka. Pierwszy taki mecz odbył się rok temu, a zwycięstwo z wynikiem 80:63 odniósł klub wrocławski.
</p>
<p class="relacja">
	"Działony" żądne były rewanżu, o czym prędko przekonaliśmy się na miejscu. Ale opowieść zacznijmy od mglistego sobotniego poranka. Jakimś cudem tym razem żaden z naszych samochodów się nie zepsuł, żadne zwalone huraganem drzewo nie zagrodziło nam drogi - dojechaliśmy na czas. Siódemkowiczów stawiło się dwanaścioro jak jeden mąż i jedna żona (a i nawet taka para tam była). Działonom zaś uciekł był jeden zapowiedziany wcześniej zawodnik (ze strachu zapewne) - przyjechali niczym piłkarska jedenastka.
</p>
<p class="relacja">
	Na miejscu czekały na nas wypasione pokoje znajdujące się w hotelowej części kompleksu. Tu jednak poremontowa rzeczywistość głęboko zajrzała nam w oczy i nozdrza. Na szczęście rześkie marcowe przedpołudnie rozwiało wszelkie wątpliwości i o elementach tablicy Mendelejewa można było rozmawiać już tylko w kontekście zasobu słów do ułożenia. Przestronna sala gry wyposażona była we wszelkie potrzebne sprzęty i, po małym przemeblowaniu, można było zasiąść do rozgrywek. Wrocławscy scrabbliści, z nielicznymi wyjątkami, wystąpili w tradycyjnych zielonych polówkach z czerwono-żółtymi kotylionami (brawo Gosia), drużyna łódzka pojawiła się na placu boju w tradycyjnych białych T-shirtach.
</p>
<p class="relacja">
	Dzielny siódemkowy prezes przygotował wszystkorobiący plik do zapisu wyników, magicznie rozstawiający pary po mniej magicznym losowaniu rundy. Na dodatek udało się uniknąć powtórzeń w rozstawieniach, co dla niektórych uczestników było pewnego rodzaju niespodzianką. Mieliśmy też swego rodzaju relację na żywo, czyli bieżący podgląd wyników grupowych i indywidualnych dzięki rzutnikowi.
</p>
<p class="relacja">
	Zaczęło się...
</p>
<p class="relacja"><strong>I część relacji subiektywnym wrocławskim okiem</strong> - <a href="#" onclick="toggle('lkmswks1')">rozwiń</a></p>
<div id="lkmswks1" style="display: none; padding: 0 15px; border: 1px solid #226767; margin: 0 15px;">
<p class="relacja">
	<strong>Runda 1</strong><br />
	Działony: 6 zwycięstw i 4093 małych punktów<br />
	Siódmiaki: 5 zwycięstw i 3892 małych punktów<br />
	Przy stole nr 3 Dominik zmagał się z BYE'm.<br />
	Pierwsza runda dla Działonów, tego się nie spodziwaliśmy. Na szczęście to tylko 1 zwycięstwo więcej.
</p>
<p class="relacja">
	<strong>Runda 2</strong><br />
	Działony: 5 zwycięstw i 4099 małych punktów<br />
	Siódmiaki: 6 zwycięstw i 3977 małych punktów<br />
	Przy stole nr 10 Beata zmagała się z BYE'em.<br />
	Tym razem również zdobyliśmy mniej małych punktów niż Działony, ale za to 6 zwycięstw było po naszej stronie. Po 2 rundach był remis, po 11.
</p>
<p class="relacja">
	<strong>Runda 3</strong><br />
	Działony: 4 zwycięstwa i 3976 małych punktów<br />
	Siódmiaki: 7 zwycięstw i 4094 małych punktów<br />
	Przy stole nr 5 Ryszard "Trener" Korpalski trenował z BYE'em.<br />
	+3 dla nas. Było dobrze.
</p>
<p class="relacja">
	<strong>Runda 4</strong><br />
	Działony: 5 zwycięstwa i 4066 małych punktów<br />
	Siódmiaki: 6 zwycięstw i 3981 małych punktów<br />
	Przy stole nr 12 Gosia walczyła z BYE'em.<br />
	Po raz kolejny ugraliśmy sumarycznie mniej punktów, ale po raz kolejny więcej meczów wygraliśmy.
</p>
<p class="relacja">
	<strong>Runda 5</strong><br />
	Działony: 4 zwycięstwa i 3870 małych punktów<br />
	Siódmiaki: 7 zwycięstw i 4445 małych punktów<br />
	Przy stole nr 9 Grzegorz vel. lalujący "Szafasz" kibicował.<br />
	Świetna runda, odskoczyliśmy w sumie na 4 punkty!
</p>
<p class="relacja">
	<strong>Runda 6</strong><br />
	Działony: 6 zwycięstwa i 4028 małych punktów<br />
	Siódmiaki: 5 zwycięstw i 3970 małych punktów<br />
	Pauzował Michał Puchowski.<br />
	Minimalna przegrana, 6 do 5. Bodo szalał. Pierwszy "wielki" mecz Michała "Słonika" Ozimińskiego. Za to co zrobił w partii z Asią, ku naszej uciesze, otrzyma scrabblową Malinę roku 2009. 
</p>
<p class="relacja">
	<strong>Runda 7</strong><br />
	Działony: 6 zwycięstwa i 4063 małych punktów<br />
	Siódmiaki: 5 zwycięstw i 3962 małych punktów<br />
	Tomek walczył z BYE'em.<br />
	Znów przegraliśmy 6 do 5. Bodo na fali. Znów fantastyczny wynik powyżej 450 pkt.
</p>
</div>


<p class="relacja">
	Warto dodać, że w czasie wyjazdu dokarmiano nas wyśmienicie i prądu tym razem nie zabrakło. Pogoda dopisała na tyle, że po sobotnim obiedzie rozegraliśmy 10-minutówkę w piłkę nożną, podczas której niektóre scrabblistki mijały się z piłką w pięknym stylu, a niektórzy scrabbliści odbijali ją ręką, nogą i główką.
</p>
<p class="relacja">
	Po pierwszym dniu rozgrywek, obydwa kluby przygotowały relaksujące gry i zabawy logiczne, nie wiedzieć czemu okołoscrabblowe i ściśle powiązane z nickami zawodników, nazwami klubów, miast itp.
</p>
<p class="relacja">
	Ciekawe, czy w jakimkolwiek innym sporcie ludzie potrafią się tak dobrowolnie torturować do upadłego - my tak :) Ale niektórych instynkt samozachowawczy podpowiedział, że nie tylko scrabblealiami żyje człowiek... W szczególności <em>homo cantores</em> lub taki, któremu wydaje się że potrafi śpiewać... a nawet lalać. :) Scrabble Karaoke, bo o tej zabawie mowa, to w najogólniejszym skrócie lalanie piosenek dwuliterówką, tak by słuchacze odgadli, co też nuci nucący. Przy czym nucący naiwnie myśli, że nuci zgodnie z tym, co słyszy w słuchawkach. :P Giganci dyscypliny dokładali też choreografię. Oj, działo się działo, nie wspominając o myleniu Krystyny Giżowskiej z Ich Troje oraz Krawczyka z Kombi. Zdarzało się, że żeńska część publiczności spadała z krzeseł turlając się ze śmiechu po podłodze. Zdarzało się walczyć ze śpiewającym, aby przestał. Dopiero świt zakończył równoległe pojedynki z nutami i planszą.
</p>
<p class="relacja">
	Siłą woli dalszej gry pojawiliśmy się rano na śniadaniu w komplecie.
</p>

<p class="relacja"><strong>II część relacji subiektywnym wrocławskim okiem</strong> - <a href="#" onclick="toggle('lkmswks2')">rozwiń</a></p>
<div id="lkmswks2" style="display: none; padding: 0 15px; border: 1px solid #226767; margin: 0 15px;">
<p class="relacja">
	<strong>Runda 8</strong><br />
	Działony: 6 zwycięstwa i 4116 małych punktów<br />
	Siódmiaki: 5 zwycięstw i 4148 małych punktów<br />
	Adam, wiecie co :)<br />
	Zgadnijcie ile przegraliśmy :). Zgadnijcie co Bodo zrobił z przeciwnikiem i ile punktów ugrał.
</p>
<p class="relacja">
	<strong>Runda 9</strong><br />
	Działony: 2 zwycięstwa i 3747 małych punktów<br />
	Siódmiaki: 9 zwycięstw i 4301 małych punktów<br />
	BYE dla Asi - 8 stół.<br />
	Zdenerwowali nas tym swoim ciułaniem punktów, zmiażdżyliśmy ich 9 do 2! To było fantastyczne! Ostatnią partię w tej rundzie rozgrywał Korry z Frodem. Nie wiemy wciąż jak oni to zrobili, ale zajęli tylko połowę planszy, układając prawie wszystkie swoje ruchy po prawej (górnej) stronie od przekątnej planszy. 
</p>

<p class="relacja">
	<strong>Runda 10</strong><br />
	Działony: 7 zwycięstwa i 4245 małych punktów<br />
	Siódmiaki: 4 zwycięstw i 3892 małych punktów<br />
	Przemek lala sobie z BYE'm<br />
	Działony wytoczyły swoje działa. Dołożyli nam 7 do 4, ale Bogusia nam nie zestrzelili!
</p>

<p class="relacja">
	<strong>Runda 11</strong><br />
	Działony: 8 zwycięstw i 4355 małych punktów<br />
	Siódmiaki: 3 zwycięstw i 3823 małych punktów<br />
	Odpoczywa Zosia.<br />
	Wkurzyli się, odrabiali straty i, co gorsze, ustrzelili Bogusia. Sytuacja zaczęła się robić nerwowa. W tej rundzie w głównych rolach wystąpił Słonik i Aztech. Ich "popisy" śledziła większość osób. Tomek po świetnym goniącym scrabblu IZOLARZE dolosował zestaw GGACRKO. Zagrał GRAC, zostawiając sobie na stojaku słowo KOG (nie było nań miejsca). Przeciwnik źle Tomka wykreślił, nie wiedział również czy to ta IZOLARA (i stąd IZOLARZE), czy to też ten IZOLARZ, nie chciał ryzykować przedłużki IZOLARZEM. Tomek w międzyczasie zrobił stratę KEG (sic!), na co Słonik położył... YM/MY :). Aztech mógł zremisować, gdyby położył w końcówce słowo GO... w innym miejscu, a tak, przegrał na odpisie.
</p>
<p class="relacja">
	<strong>Runda 12</strong><br />
	Działony: 6 zwycięstw i 4241 małych punktów<br />
	Siódmiaki: 5 zwycięstw i 3829 małych punktów<br />
	Boguś, nasze główne działo przeciw Działonom pauzował, w końcu w poprzedniej rundzie został ustrzelony.<br />
	Tymczasem na planszach horror. Bili naszych, Fotu biegała i złowieszczyła.
	Niespodziewanie Labi wygrał z Mordą headshotem OWLOKŁOM, wcześniej sobie wystawiając ten ruch. Tymczasem przy stoliku nr 1 trwała walka pomiędzy Kubą Szymczakiem a Aztechem. Wszystkie znaki na niebie i ziemi wskazywały na to, że Aztech przegra. Okazało się jednak, że wszystkie decyzje w końcówce, które podjął, okazały się strzałem w dziesiątkę: zrezygnował ze zbierania do scrabbla, wykreślił przeciwnika; następnie jednym średniodrogim ruchem za 22pkt wystawił sobie na ruch za 32pkt (pozbywając się w nim liter H i W!), a następnie dobił przeciwnika, ku uciesze kibicujących Siódmiaków. Wygrana WKS stała się faktem. Ale trzeba pamiętać, że gdyby nie fantastyczna forma Bodo, ta partia, nie miałaby znaczenia!
</p>
</div>

<p class="relacja">
	Zacięty był to bój, bo ostatnia runda rozstrzygnęła wszystko - do ostatniej minuty ostatniej rundy ważył się wynik. Do historii przejdzie kunsztowna "blokada Aztecha", która zapewniła autorowi zwycięstwo, pogrążając w rozpaczy DJ_Koalara.
</p>
<p class="relacja">
	Siódemka wygrała druzgocącą psychicznie przewagą 2 pkt 67:65.
</p>
<p class="relacja">
	Łodzianie przygotowali wspaniałe bycze medale, wrocławianie - skromne gadżety scrabblowo-łódkowe. W miłej atmosferze, utwierdzając się że jesteśmy dwoma najlepszymi i najfajniejszymi klubami w Polsce, a i dziękując sobie za grę, rozstaliśmy się śpiewając: "Zostańmy razem", ale i "Strzeż się tych miejsc".
	<span class="autor">Fotu, Bodo & Aztech</span>
</p>



<h2 id="news7">Jubileusz we Wrocławskim Klubie Scrabble "Siódemka"</h2>
<p class="relacja">
<img src="kluby/siodemka200.jpg" alt="" class="onright"/>
W nocy z soboty na niedzielę odbył się 200. turniej Siódemki - w tym historycznym wydarzeniu wzięło udział ponad 20 osób.<br>
Rozpoczęliśmy spotkanie o godz. 20 polonezem na 11 par ;) w składzie:
<ul>
  <li>Sąsiedzi rewizyjni</li>
  <li>Adam i Asia</li>
  <li>Załoga G</li>
  <li>Ziemek &amp; Mateusz</li>
  <li>Sławek, Darek i Marysia</li>
  <li>Żądełko</li>
  <li>Super Mario Brosv </li>
  <li>Marcin &amp; Karol</li>
  <li>Psiepolki</li>
  <li>Rysiu &amp; Michał</li>
  <li>i tańczący samotnie na belgijskim parkiecie Przemysław Pawlic</li>
</ul><br>

Podium zajęli:
<ol type="1">
  <li>Sąsiedzi Rewizyjni - 644 pkt.</li>
  <li>Adam i Asia - 614 pkt</li>
  <li>Załoga G - 584 pkt</li>
</ol><br>
Po takiej rozgrzewce z zapałem przystąpiliśmy do 6-rundowego turnieju, który wygrała, ostatnio rzadko zaglądająca 
na cotygodniowe turnieje, Monika Laskowska - widać trenuje w ukryciu :) II miejsce wywalczył Adam, III Karol - gratulacje!<br>
Jubileusz świętowaliśmy nie tylko przy planszach, nie zabrakło tradycyjnego już tortu i szampana.<br>
Ryszard Korry Korpalski - gospodarz Paśnika przygotował na tę okazję dopalacze z kofeiną i teiną oraz kaloryczne 
przekąski, czuwał też dzielnie przy barze do białego rana :) <br>
Po północy, gdy lał się szampan Panowie wystąpili z niespodzianką dla Pań (na świecie rozpoczął się właśnie 
Dzień Kobiet). Zapachniało wiosną w środku nocy :)<br>
Ponieważ pracujemy nad formą przed czekającym nas wkrótce meczem rewanżowym z Działonami nie mogło 
zabraknąć ćwiczeń z anagromawania. Klasówka przygotowana przez Marcina Radwańskiego okazała się jednak lekko za trudna... Ciekawe jakby sobie poradził z nią lider i wicelider naszej listy rankingowej?<br>
EUGENOL, OKTAEDR, KALUMET, BUNRAKU, LOOPING i zwyczajny NADZIAK - trzeba tu przypomnieć, że wszyscy jadący do 
Załęcza będą z tych słów odpytywani ;)<br>
Druga klasówka, autorstwa wielkiego nieobecnego tych zawodów - Tomka Aztecha Suchanka, którą mieli okazję rozwiązywać o świcie jedynie najwytrwalsi z Siódemkowiczów okazała się pestką w porównaniu z klasówką przygotowaną przez debiutującego w roli nauczyciela - Marcina. Najlepszy wynik zanotował Boguś.<br>
Nie wiadomo czy ten dobry rezultat (90 znalezionych siódemek) pozwoli mu nadal na przynależność do drużyny Frajerów :)<br>
Może nocne granie wejdzie na stałe do programu spotkań Siódemki?<br>
Do zobaczenia na kolejnym turnieju, zanim się obejrzymy będzie kolejny jubileusz, kolejny tort i na spotkaniu będzie nas nie 24 a 124 :)
<span class="autor">b.</span>
</p>


<h2 id="news6">ŁKMS - WKS Siódemka (63:80)</h2>
<p class="relacja">
<img src="kluby/lodz-wroclaw.jpg" alt="" class="onright"/>
W pokojowej i przyjaznej atmosferze, ale z zachowaniem wszelkich rygorów gry, łódzcy i wrocławscy scrabbliści rozegrali pasjonujący, zacięty i wyrównany bój, z którego to ci ostatni wrócili z tarczą. Dość powiedzieć, że <b>Gosia Szczygieł</b>, najlepsza indywidualnie Siódemkowiczka, po jedenastu grach miała bilans <b>11:0</b>. Jakby tego było mało, jej mąż <b>Wojtek</b>, absolutny debiutant jeśli chodzi o zorganizowane granie w Scrabble, również miał niemały, merytoryczny udział w załęczańskim triumfie wrocławian.<br />Z kolei w łódzkiej ekipie najlepszym indywidualistą okazał się... <i>straniero</i> wypożyczony z Katowic, <b>Andrzej Rechowicz</b> z dorobkiem 9 oczek. Uczciwie trzeba jednak przypomnieć, że ze względu na nierówną liczebność obu ekip, każdy z łodzian pauzował przez dwie z trzynastu rund.<br /><br />

Ale nie tylko na kwadratowej planszy Siódemkowicze mierzyli się z zawodnikami ŁKMS-u i nie tylko z zawodnikami ŁKMS-u mierzyli się na kwadratowej planszy. Kiedy w sobotę, w trakcie VIII rundy, szalejący po Polsce huragan <b>Emma</b> powodował kolejne przerwy w dostawie prądu, wrocławianie i łodzianie dzielnie toczyli swoje pojedynki przy świetle telefonów komórkowych i kieszonkowych latarek. Oczywiście nie były to warunki znośne i, po kilku próbach dogrania rundy do końca, ogłoszona została przerwa.<br /><br />
Liczne przykłady z historii sportu pokazują, że takie wydarzenia zmieniają losy meczów. Na prowadzących graczy WKS-u nie miało to raczej większego wpływu. ;)<br />Nieplanowana przerwa rozbudziła za to w zawodnikach obu klubów chęć skrzyżowania rękawic w innej dyscyplinie, mianowicie - kalamburach po ciemku. Bez fałszywej skromności trzeba przyznać, że zestaw haseł do wylosowania prezentował się okazale, ale i reprezentanci klubów stawili im czoła. „Co na to NATO? NATO na to nic!” (Pogodno), „imponderabilia” - nie ma haseł nie do pokazania. Trzeba tylko odpowiednio poprzebierać rączkami. :)
W tym miejscu warto wspomnieć o popisowym numerze <b>Przemka Pawlica</b>, którego interpretacja sufiksu „-anie” zapisana zost-anie złotymi zgłoskami w annałach kalamburowej humorystyki. Kto widział, ten wie. :)<br /><br />

Wszyscy uczestnicy załęczańskich gier wyjechali obdarowani. Tu ogromne <i>chapeau bas</i> dla ekipy z Łodzi, która przygotowała rywalom imienne kubki, a także inne wartościowe niespodzianki, w arcyciekawy sposób zaprezentowane przez <b>Jerzego Ciesielskiego</b>, hojnego darczyńcę z łódzkiej ekipy. Wrocławianie zrewanżowali się upominkami wręczanymi między rundami i... a jakże - kubkami.<br /><br />

Na koniec wielkie podziękowania dla ludzi bez których pomysłu i determinacji nic by się nie udało: <b>Jarka Michalaka</b> i <b>Ani Stefańskiej</b> ze strony łódzkiej i <b>Małgosi Szczygieł</b> z ekipy wrocławskiej. Obyśmy w rewanżowym meczu za pół roku spotkali się w gronie co najmniej nie mniejszym. Do zobaczenia!
<span class="autor">Bogusław Szyszka</span>
</p>
<a href="kluby/lodzwroclaw.xls">Pełne i szczegółowe wyniki meczu ŁKMS-WKS „Siódemka”</a><br />
<a href="http://www.augjer.toya.net.pl/galerie/zalecze08/index.html" target="_blank">Zdjęcia z meczu</a>

<h2 id="news5">15-literówka na 510 spotkanie GKS</h2>
<p class="relacja">
<img src="kluby/kat15.jpg" alt=""  class="onleft" />
Lepiej późno niż wcale. Na 510 spotkaniu dwójka młodych adeptów scrabblowania wpisała się do historii
<b>Górnośląskiego Klubu Scrabble</b> w Katowicach układając „nierozbiegające”, czyli pierwszą 15-literówkę na Śląsku. Byli to <b>Kasia Pawluch</b> z Zabrza i <b>Łukasz Sornek</b> z Mikołowa. Partia zakończyła się porażką 433-425 dla dżentelmeńsko grającego Łukasza. Spotkanie klubowe zakończyło się zwycięstwem 5-0 <b>Darka Putona</b> przed <b>Ewą Ciszewską</b> i <b>Leszkiem Stecułą</b>. Grało 9 osób. Na trwające obecnie ferie GKS zaprasza wszystkich chętnych ze Śląska i nie tylko, ponieważ liczni sponsorzy klubu przygotowali atrakcyjne nagrody rzeczowe dla amatorów klubowego scrabblowania.
<span class="autor">Leszek Stecuła</span>
</p>


<h2 id="news4">500 spotkanie Górnośląskiego Klubu Scrabble</h2>
<p class="relacja">
<img src="kluby/kat500.jpg" alt="" class="onright" />
Jak zwykle w czwartek na <b>Uniwersytecie Śląskim</b>, u niezwykle gościnnego profesora <b>Jerzego Paszka</b>
po ponad 10 - letniej działalności udało się rozegrać <b>500</b> spotkanie klubowe.Przy okazji zarejestrowano kolejnych dwóch nowych członków <b>Łukasza Sornka</b> z Mikołowa oraz <b>Andrzeja Bębena</b> z Katowic. Na najwyższych stopniach podium stanęły utytułowane gwiazdy rozgrywek scrabblowych w realu i w necie. Wygrał drużynowy MP z 2006 r. <b>Maciej Rzychoń</b> vel Krewny Królika rezultatem 5-0. Drugie miejsce zajął niedawny MP lekarzy <b>Leszek Stecuła</b> vel Strongliterman 4-1. Trzecie miejsce przypadło drużynowemu MP z 2006 i wice MP indywidualnemu z 1996 r. oraz absolutnemu koryfeuszowi i znawcy ponad 2 i pół miliona słów <b>Darkowi Putonowi</b> vel Gradarowi z wynikiem 3-2 , co nie zmieniło jego pierwszej lokaty w rankingu klubowym. Nagrodę za przekroczenie 500 punktów w partii otrzymała <b>Kasia Żuławska</b>. Dla pozostałych jak zwykle liczni sponsorzy klubu ufundowali nagrody do wyboru wg osiągniętych rezultatów. Jubileuszowe spotkanie uświetniło 11 zawodników z całego Śląska na aktualnie 51 zarejestrowanych. Na koniec należy dodać, że klub ma charakter otwarty, są prowadzone liczne statystyki i rekordy
do których pobicia zachęcamy zawodników innych klubów scrabblowych.<br />
<span class="autor">Leszek Stecuła</span>
</p>

<a href="kluby/rankkat500.txt">Ranking górnośląskiego klubu po 500 spotkaniach</a><br />
<a href="kluby/kat500.zip">Komplet statystyk klubowych</a>

<h2 id="news3">Klubowe potyczki - Blank : Same Premie</h2>
<p class="relacja">
<b>Okiem szczecinian - LANDSBERG WZIĘTY!</b><br /><br />
Po niefortunnej - i jak niektórzy mówią - przypadkowej porażce w lutowej konfrontacji z gorzowskim klubem „Same premie”, w szczecińskim „Blanku” nie gasły nastroje rewanżystowskie. Szczęśliwie, już w niespełna rok od tego wydarzenia, nadarzyła się okazja podreperowania nieco nadszarpniętej klubowej reputacji, a tym samym potwierdzenia dominacji szczecinian na zachodnich
rubieżach RP. W niedzielę 17. grudnia umówiliśmy się na mecz rewanżowy! <br /><br />

Nie powiem, były pewne obawy, że gorzowianie wzmocnieni duchowo sukcesami Sebastiana Świderskiego i Kazimierza Marcinkiewicza tanio skóry nie sprzedadzą: tak, tak - z Gorzowa juz nie można się bezkarnie naigrywać... <br /><br />

Ale nic - rano, ciemno, zimno, ale ruszamy! Ruszamy i od razu falstart: w ostatniej chwili dowiadujemy się, że brakuje nam pełnego składu: z samego rana zaniemogła nam Szatanka Niedzielko! Nie powiem, za bardzo się nie zdziwiliśmy - w końcu adwent, Boże Narodzenie za pasem, a tu taka obraza boska i to w biały dzień! Niech się dziewczyna cieszy, że grypa a nie na przykład piorun z jasnego nieba... Ale nic! - Jarek z Irkiem wpadli na genialny pomysł zaanektowania do naszego składu, mieszkającego dokładnie w połowie drogi między Szczecinem a Gorzowem, Walentego Siemaszko - uznanego mistrza Lipian i najbliższych okolic.
Ponadto, Walenty zna Kamila Durczoka, co było ostateczną rekomendacją. <br /><br />

O 11:00 zaczęło się: 6 rund, każdy z każdym. W siódmej umówiliśmy się na swego rodzaju duńczyka: najlepsi z obu drużyn mieli zagrać na pierwszym stole, drudzy na drugim, etc. Co to dużo opowiadać: szczecinianie wygrali trzy rundy (6:0, 5:1, 5:1), dwie zremisowali, i w dwóch ponieśli nieznaczną porażkę (2:4, 2:4). Ostateczny bilans rozgrywki to 26:16 na korzyść klubu „Blank”. <br /><br />

Na uwagę zasługuje trochę słabsza postawa lidera gorzowian Roberta Duczemińskiego, który doznał aż trzech porażek, ostatecznie gromadząc 4 punkty. Wobec powyższego, najlepszym zawodnikiem GKS „Same Premie” został z pięcioma zwycięstwami Marek Herzig. Podkreślić należy dzielną postawę Agaty Pastusiak, która w trzech konfrontacjach pokonała nominalnie dużo lepszych od siebie
zawodników. <br /><br />

Blankowicze zagrali bardzo równo - każdy zakończył spotkanie z dodatnim bilansem: Ola, Jarek, Irek, Łukasz i Walenty zdobyli po 4 punkty. Z szyku wyłamał się nieco Kuba (który to?, hehe), który wygrał 6 gier. <br /><br />

Były pewne obawy, szacunek dla przeciwnika i pamięć o porażce, ale ostatecznie dobry duch „Blanka” w połączeniu z coraz większymi umiejętnościami klubowiczów przesądził o wyniku konfrontacji. No tak, w końcu - i co uświadomiłem sobie w drodze powrotnej - Sebastian Świderski i Kazimierz Marcinkiewicz to specjaliści od drugich miejsc... 
</p>

<p class="relacja">
<b>Okiem gorzowian</b><br /><br />
17 grudnia, w przedświątecznym nastroju, doszło do spotkania dwóch zaprzyjaźnionych klubów scrabblowych: szczecińskiego „BLANK”, oraz gorzowskiego „SAME PREMIE”. Wszystko odbyło się ponownie w „Willi Pauckscha” Grodzkiego Domu Kultury w Gorzowie Wlkp. Nie zabrakło „słodkości” i gorących napojów dzięki niezastąpionym: Zbigniewowi Wieteckiemu oraz Sergiuszowi Sokołowowi. Drużynę gości reprezentowali: Łukasz Tuszyński, Aleksandra Merklejn-Kaczanowska, Jakub Zaryński, Walenty Siemaszko, Ireneusz Moralewicz oraz Jarosław Żuliński. Grono gospodarzy to: Robert Duczemiński, Zbigniew Wietecki, Marek Herzig, Radosław Pastusiak, Agata Pastusiak. Niestety z powodu nieobecności jednego z graczy, zmuszeni byliśmy do wykorzystania zawodnika rezerwowego Sergiusza Sokołowa. Wszyscy walczyli dzielnie. Po I rundzie prowadziliśmy 4:2 . Drużyna gości jednak szybko „odbiła piłeczkę” i II runda zakończyła się nasza „sromotną porażką”, czyli wynikiem 0:6. III runda to remis 3:3, jednak ciężko nam było odrobić straty. Po IV rundzie „zabłysła” nadzieja, gdyż różnica to tylko 2 pkt. Niestety, ani posiłek regeneracyjny ani też zmiana zawodnika rezerwowego i zasilenie szeregów osobą Józefa Wieteckiego, nie pomogła nam uniknąć porażki. Spotkanie rewanżowe zakończyło się wynikiem: 26 pkt BLANK i 16 pkt SAME PREMIE. Najlepszym zawodnikiem drużyny szczecińskiej okazał się Jakub Zaryński z 6-cioma wygranymi , zaś w drużynie gorzowskiej - Marek Herzig z 5-cioma wygranymi. Humory wszystkim dopisywały, więc liczymy na kolejne spotkanie, lecz już w przyszłym roku, może tym razem w Szczecinie?, kto to wie.. .ważne, że obie rozgrywki pokazały wysoki poziom obu drużyn.
<span class="autor">Agata i Radosław Pastusiak</span>
</p>

<h2 id="news2">Klubowe potyczki - Blank Szczecin : SCRABBLISTA Trójmiasto</h2>
<p class="relacja">
<img src="kluby/szcztroj.jpg" alt="" class="onleft" />
Kolejna potyczka klubów scrabble z północnej Polski miała miejsce 11-12 marca w uroczej wsi <b>Łącko</b>, niedaleko Ustki, położonej dokładnie w połowie drogi między <b>Gdańskiem</b>, a <b>Szczecinem</b>. Małe gospodarstwo agroturystyczne mieszczące się w 100-letnim domu okazało się idealnym miejscem do gry i jeszcze lepszym do integracji. Oba klubu wystawiły po 7 zawodników, którzy rozegrali 11 rund. Pierwszy 7-rundowy dzień zakończył się wyraźnym prowadzeniem <b>Trójmiasta</b> 26,5:22,5.
„Koń pociągowy” <b>Szczecina</b>, czyli <b>Kazik Merklejn</b>, wygrał wszystkie 7 partii, dzielnie wspomagał go też <b>Darek Kuć</b> dostarczając 6 punktów, ale to jednak drużyna z Trójmiasta grała równiej. Integracja szkrablistów, którzy często znali się do tej pory tylko z internetu, trwała do białego rana, po czym przystąpiono do rundy rewanżowej. Szkrabliści ze Szczecina znacznie lepiej przetrwali noc i przystąpili w dobrej formie do emocjonującego pościgu. Przed ostatnią rundą Trójmiasto prowadziło 36,5 do 33,5, ale w małych miało 70 punktów straty do Szczecina (znowu robota Kazika). Tylko wygrana w ostatniej rundzie 5:2 dawała im szanse zwycięstwa. Szala przechylała się dosłownie do ostatniej kostki. Szczecin prowadzi już 4:2 w ostatniej rundzie, gdy na ostatnim stole <b>Darek Białobrzewski</b> grając z <b>Dorotą Kuć</b> znajduje pięknego scrabbla „mikrobowy”. Wianuszek trójmiejskich kibiców wokół ostatniego stołu już się cieszy, jednak Dorota odpowiada siódemką „słyszysz”
w ostatnim ruchu i wygrywa partię. Teraz cieszą się szczecinianie, bo są pewni, że w małych punktach wygrali.
Sumowanie punktów i... wygrywa Trójmiasto o 43 małe punkty!<br />
<b>Wynik końcowy:    38,5 : 38,5 !!</b><br />
W małych punktach Szczecin 28237, a Trójmiasto 28280.<br />
Szanujmy małe punkty! To morał z tego meczu, który dostarczył nam wielu niezapomnianych emocji i z pewnością będzie początkiem wielu przyjaźni.
</p>
<b>Klasyfikacja indywidualna:</b>
<pre style="clear:both;">
	Gracz			Klub			Duże pkt.	Małe pkt.
	-------------------------------------------------------------------------
	Kazik Merklejn		Szczecin		10		4706
	Darek Kuć		Szczecin		9		4314
	Irenka Sołdan		Trójmiasto		8		4355
	Andrzej Kowalski	Trójmiasto		8		4093
	Kasia Sołdan		Trójmiasto		6,5		3925
	Ola Merklejn		Szczecin		6		4010
	Darek Białobrzewski	Trójmiasto		5		4256
	Jakub Zaryński		Szczecin		5		4216
	Gosia Puternicka	Trójmiasto		5		4043
	Dorota Kuć		Szczecin		4		3806
	Alina Białobrzewska	Trójmiasto		4		3725
	Irek Moralewicz		Szczecin		3,5		3774
	Wojtek Sołdan		Trójmiasto		2		883
	Gosia Szmuksta		Szczecin		1		3411
	
</pre>
Pełne wyniki i przebieg meczu w pliku xls dostępne są <a href="kluby/szczecintrojmiasto.xls">tutaj</a>.

<h2 id="news1">Klubowe potyczki - Blank : Same Premie</h2>
<p class="relacja">
<img src="kluby/gorzow-szczecin.jpg" alt="" class="onright" />
<b>Okiem gorzowian</b><br /><br />
W ostatnią niedzielę 19.02.2006 dwa zaprzyjaźnione ze sobą kluby scrabblowe - <b>szczeciński „Blank”</b> oraz <b>gorzowski „Same premie”</b> rozegrały ze sobą po raz pierwszy w historii towarzyski drużynowy turniej. Oba kluby ugościł Grodzki Dom Kultury w Gorzowie, a organizacyjnie turniej przygotowali niezmordowani: <b>Sergiusz Sokołow i Zbigniew Wietecki</b>. Nie zabrakło niczego: ani słodkości ani kawy czy herbaty; warunki do gry były doskonałe.<br />
Mimo że szczecinianie przyjechali bez swojego asa w osobie Kazia Merklejna, to łatwo skóry nie sprzedali. Początek zapowiadał, że będzie to mecz do jednej bramki ponieważ po dwóch rundach był wynik 10 do 4 dla klubu „Blanki”. Drużyna gorzowska wykazała się jednak odpornością psychiczną i po fatalnym początku odzyskała wigor i w trzeciej rundzie doprowadziła do stanu 10:9, co zwiastowało duże emocje w następnych rundach. Co stało się też faktem - po czwartej rundzie po raz pierwszy gorzowianie objęli prowadzenie 15:13, którego nie oddali już do końca, mimo fantastycznej postawy gości ze Szczecina. Przed siódmą, ostatnią rundą „Same premie” prowadziły 22:20 więc wynik końcowy był sprawą otwartą. Losy spotkania ważyły się do ostatniej rundy, a można też
powiedzieć że do ostatniego ruchu, gdyż niektóre końcowe partie rozstrzygały się po ostatnim wyłożeniu płytek. Ostatnia runda okazała się jednak szczęśliwa dla zespołu z <b>Gorzowa</b> - wygrali tę rundę 4 do 3 i całe spotkanie <b>26:23</b>. Po spotkaniu wszyscy byli radośni i obiecywali sobie w niedalekiej przyszłości rewanż. Trzeba dodać, że był to świetny pomysł, impreza trafiona w dziesiątkę. Czekamy na kontynuację.
<span class="autor">Robert Duczemiński</span>
</p>

<p class="relacja">
<b>Okiem szczecinian</b><br /><br />
Radosnym, niedzielnym porankiem 19 lutego roku pańskiego 2006, ruszyliśmy stawić czoło członkom (sic!) zaprzyjaźnionego klubu scrabblowego z Gorzowa Wielkopolskiego. Miejscem rozgrywek była - znana nam już z Mistrzostw Ziemi Gorzowskiej - <b>Willa Pauckscha</b>, siedziba Grodzkiego Domu Kultury. W miłej atmosferze, raczeni kawą i ciastem rozegraliśmy 7 rund: siedmiu blankowiczów kontra siedmiu gorzowian (BTW: ze składu GORZOWIAN jest jeszcze ROGOWIZNA!). Sprawa zwycięstwa ważyła się nieomal do ostatniej gry - ostatecznie ulegliśmy 26 do 23 (gdyż kurtuazji nigdy za wiele). Naszym najmocniejszym ogniwem była <b>Olka</b> - zdobywczyni 5 punktów, tuz za nią uplasował się <b>Łukasz</b> i - będąca w zgodnej opinii wszystkich, największą
niespodzianką turnieju - <b>Angelika</b>, oboje zdobyli po 4 punkty. W drużynie przeciwnej bez niespodzianek: <b>Robert
Duczemiński</b> i <b>Zbyszek Wietecki</b> zdobyli po 6 punktów, walnie przyczyniając się do triumfu własnej drużyny.<br />
No cóż, zima wasza...
<span class="autor">Jakub Zaryński</span>
</p>

<?require_once "files/php/bottom.php"?>
</body>
</html>
