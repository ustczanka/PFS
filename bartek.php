<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polish Scrabble Federation :: Kibicujemy Bartkowi!</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("english","bartek");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Bartek w Johor Bahru")</script></h1>


<h2>Pakorn Nemitrmansuk mistrzem świata</h2>
<table class="klasyfikacja">
	<tr><td></td><td>Zwycięstwa</td><td>Partia 1</td><td>Partia 2</td><td>Partia 3</td><td>Partia 4</td></tr>
	<tr><td>Pakorn Nemitrmansuk (Tajlandia)</td><td>3</td><td>425</td><td>670</td><td>394</td><td>499</td></tr>
	<tr><td>Nigel Richards (Nowa Zelandia)</td><td>1</td><td>419</td><td>303</td><td>443</td><td>480</td></tr></table><br>

Gratulacje dla obu finalistów za piękną walkę. Aż szkoda, że to już koniec. Następny turniej mistrzowski za dwa lata, miejmy nadzieję, że gdzieś bliżej, a przede wszystkim w mniej oddalonej od nas strefie czasowej.<br><br>
Szczególnie efektowna wydała mi się ostatnia, czwarta partia, w której obaj gracze wykazali się naprawdę niesamowitą wyobraźnią słowotwórczą. Ze składu ABIEMNO nie ma siódemki, ósemka jest jedna, przez H! Wolnych liter na planszy było sporo, a mimo to Richards znalazł BOHEMIAN (typowy dla bohemy). Nemitrmansuk odgryzł się już wkrótce, mając AABCNOT. Nie dość, że ułożył jedyną z tego składu ósemkę BOTANICA (wbrew pozorom nie "botanika", lecz "sklep z ziołami"), to jeszcze w dwóch miejscach utworzył nowe słowa przez przyłożenie.<br><br>
Obaj zbliżają się do komputerowego ideału. Bardzo często ich decyzje pokrywają się z wyborem programu symulującego, który szuka najmocniejszych ruchów, analizując teoretycznie prawdopodobne kontynuacje. Tym milej dla obserwatorów, że czasem zdarzają się takie lapsusy, jak IR Richardsa. A więc to jednak grają ludzie, uff!<br><br>
Jeszcze milsze jest to, że werdykt komputera też nie ma wartości absolutnej. Analiza pozycji scrabble'owej opiera się przecież na prawdopodobieństwie. Nikt nie może być pewien, że właśnie następny stojak gracza nie okaże się równie prawdopodobny jak szóstka w totka. Dzięki temu wciąż można uniknąć absolutnego dyktatu maszyny, a nawet pozwolić sobie na bardzo ludzkie błędy. Taj, który w pierwszej partii pokonał Richardsa, na samym początku ułożył nieistniejące słowo i stracił kolejkę.<br><br>
Ciekawy jest też w tym roku obraz konfliktu pokoleń. Po Mumbaju wydawało się, że najmłodsi (Hubert Wee, Singapur, 7. miejsce; David Eldar, Australia, 11.; Suanne Ong, Malezja, 12.) za dwa lata zaatakują ławą. Tymczasem wynik w Johor Bahru jest dość zaskakujący. Najlepszy ze wspomnianej trójki Hubert Wee zajął tym razem 40. miejsce, a Suanne Ong skończyła na 98. miejscu! Również innym graczom z tego pokolenia nie wiodło się najlepiej. Tymczasem najgorszy (i najmłodszy) z pięciu posiadaczy najwyższego tytułu scrabble'owego, Panupol Sujjayakorn, zajął 25 miejsce. Uczestnik wszystkich turniejów mistrzowskich, Joel Wapnick, mimo przestoju w środkowych rundach wywindował się na 9. miejsce. Stare repy górą!<br><br> 
Konia z rzędem temu, kto w tak kapryśnej grze potrafi wskazać prawdziwą przyczynę zwycięstwa starych nad młodymi. Osobiście doszukiwałbym się jej jednak w wiekowym sporze między sztuką a rzemiosłem. W pewnym momencie rozwoju gry olbrzymie znaczenie uzyskały komputerowo tworzone listy wyrazów dobieranych według najrozmaitszych kryteriów. Być może młodzież, jak wiadomo najbardziej zaprzyjaźniona z maszynerią, osiągnęła dzięki niej słownikową przewagę nad starszymi. Wystarczyły dwa lata, by starsi nadgonili. I teraz, przy równych szansach słownikowych, znów najważniejsze okazało się to coś, czego tajemnicę wszyscy próbują bezskutecznie zgłębić. Nie wiem, czy dobrze zgadłem, ale na pewno jest to wyjaśnienie miłe memu sercu. Lubię myśl, że człowiek jest ważniejszy od maszyny.

<h2>Runda 24 (7 : 17)</h2>
<b>BP - Chris Abordo (Filipiny) 326 : 413</b><br><br>
Koniec zmagań. Bartkowi dziękujemy za walkę i emocje. Sto piąte miejsce nie wygląda może imponująco, ale siedem punktów to wbrew pozorom wynik całkiem dobry. Odkąd pierwszy raz wzięliśmy udział w mistrzostwach świata, poziom gry bardzo się podniósł i teraz dostarczycieli punktów już nie ma. O każde zwycięstwo trzeba walczyć do ostatniej płytki. A my po prostu za mało gramy z najlepszymi...<br><br> 
Nigel Richards wygrał eliminacje! Aktualny mistrz świata pokonał po emocjonującej końcówce Dave'a Wieganda. W finale spotka się z Tajem Pakornem Nemitrmansukiem, który w ostatniej partii okazał się wyraźnie lepszy od swojego rodaka. Tak więc niespodzianki nie ma. W walce o tytuł wicemistrz świata z lat 2003 i 2005 spotka się jutro z aktualnym mistrzem świata. Początek o godzinie 3 w nocy, zawodnicy grają do trzech wygranych partii. 

<h2>Runda 23 (7 : 16)</h2>
<b>BP - Charnrit Khongthanarat (Tajlandia, mistrz świata juniorów) 438 : 510</b><br><br>
Młodość górą. Bartek mimo wysokiego wyniku przegrywa wyraźnie. Ale może będzie ósmy punkt. Jest jeszcze jedna szansa. Trzeba ograć Filipińczyka Chrisa Abordo...<br><br>
Nadal nie wszystko jasne. Nemitrmansuk przegrał po zaciętym pojedynku. Na zablokowanej planszy Wiegand zdołał wycisnąć ze stojaka LNNOPRS dosatecznie dużo, by utrzymać dwupunktową przewagę i wygrać przeciwko IVIED 458 : 450. Na prowadzenie wyszedł Nigel Richards, który rozbił Davida Boysa 509 : 319. Nowozelandczyk, Taj i Amerykanin mają po 17 punktów. W ostatniej rundzie Richards (17) gra z Wiegandem (17), Nemitrmansuk (17) z Panyasophonlertem (16) i Boys (16) z Tajem Marutem Siriwangso (15). Nawet Richards mimo zdecydowanie najlepszych małych punktów nie może jeszcze być pewien finału.


<h2>Runda 22 (7 : 15)</h2>
<b>BP - Michael Quao (Ghana) 510 : 438</b><br><br>
Bartek - Afryka 2 : 2! Jest kolejny punkt. Jeszcze jeden! Jeszcze jeden! Ale nie będzie łatwo. Przeciwnikiem Bartka będzie teraz aktualny mistrz świata juniorów, 15-letni Charnrit Khongthanarat z Tajlandii.<br><br> 
Piętnastego zwycięstwa z rzędu nie było. Nemitrmansuk uległ Naweenowi Fernando. Przegrał również Wiegand, a ponieważ dwaj pozostali zawodnicy wygrali, pierwsza czwórka pozostaje bez zmian, tyle że teraz w kolejności Nemitrmansuk, Richards, Boys, Wiegand. Ten pierwszy ma punkt przewagi. Minimalne szanse na finał zachowują jeszcze Komol Panyasophonlert i Naween Fernando o dwa punkty za liderem. Nawiasem mówiąc, gdyby liczyć tylko pojedynki pierwszej czwórki między sobą kolejność wyglądałaby następująco: Nemitrmansuk 3, Boys 2, Wiegand 1, Richards 0. To jednak nie ma praktycznego znaczenia, ważna jest natomiast różnica małych punktów, a tę zdecydowanie najlepszą ma Richards. Po następnej rundzie wiele powinno się wyjaśnić. Pierwsza czwórka gra między sobą - Wiegand z Nemitrmansukiem, a Richards z Boysem. 


<h2>Runda 21 (6 : 15)</h2>

<b>BP - Rohaina Tanveer (Kuwejt) 381 : 425</b><br><br>
Proszę sobie nie wyobrażać, że gracze z ostatnich miejsc w turnieju grają słabo! To raczej poziom czołówki przez ostatnie dziesięć lat kosmicznie się podniósł. W każdym razie Bartek będzie musiał szukać swoich szans w trzech ostatnich partiach, bo tym razem się nie udało. Następny przeciwnik to Michael Quao z Ghany.<br><br> 
Nemitrmansuk pobił rekord Grahama! Czternasta partia bez porażki i w sumie już siedemnaście punktów. Coraz bliżej miejsca w finale i trzeciej szansy na tytuł mistrza. Spośród graczy czyhających na potknięcie lidera odpadł Naween Fernando, który potknął się sam, przegrywając z Jasonem Katzem-Browem ze Stanów Zjednoczonych. Ma teraz ostatnią szansę powrotu do gry, ale musi przerwać niesamowitą passę Taja na pierwszym stole.


<h2>Rundy 17-20 (6 : 14)</h2>

<b>BP - Patrick Mulemena Mpundu (Zambia) 390 : 455<br>
BP - Warodom Geamsakul (Japonia) 415 : 309<br>
BP - Ricardo Gonzales (Arabia Saudyjska) 349 : 455<br>
BP - Vannitha Balasingam (Malezja) 432 : 446</b><br><br>

 Strona działa już jak należy, a tymczasem Bartek dodał do swojego stanu posiadania jedno zwycięstwo i ma ich sześć. Teraz będzie grał z reprezentantką Kuwejtu. Przed nim jeszcze cztery partie.<br><br>
Niesamowita passa Taja, Pakorna Nemitrmansuka trwa. Wygrał trzynaście partii z rzędu i wyrównał tym samym rekord mistrzostw Amerykanina Matta Grahama z 1997 roku. Mimo to po dwudziestu rundach prowadzi przed Amerykaninem Dave'em Wiegandem tylko o jeden punkt. Za nimi z czternastoma punktami trzech graczy - aktualny mistrz świata Nigel Richards z Nowej Zelandii, były mistrz świata David Boys z Kanady i Naween Fernando z Australii. Zapewne w tej piątce znajdują się dwaj szczęścliwcy, którzy zagrają jutro. Zawodników z trzynastoma punktami jest aż dziewięcioro, ale ich szanse są chyba bardziej teoretyczne.<br><br> 
Skład czołowej grupy okazuje się bardzo tradycyjny - 5 osób ze Stanów Zjednoczonych, 6 z innych krajów anglojęzycznych (2 - Szkocja i Australia, 1 - Nowa Zelandia i Kanada), 3 Tajów reprezentujących Daleki Wschód i na dodatek Sherwin Rodrigues z Indii na 11. miejscu. Nie ma żadnego Afrykanina, nie ma też - o dziwo - tych, którzy mieli do miejsca gry najbliżej, czyli Singapurczyków i Malezyjczyków. Tylko piorunującym finiszem mogą uratować honor Anglicy, z których najlepszy jest teraz na 17 miejscu...

<h2>Runda 17</h2>
<b>BP - Vannitha Balasingam (Malezja)</b><br><br>

W tej chwili kończy się runda 18, niestety duża część strony mistrzostw jest nieodświeżana. W siedemnastej rundzie Pakorn Nemitrmansuk pokonał na pierwszym stole Nigela Richardsa i został samodzielnym liderem. Było to jego dziesiąte zwycięstwa z rzędu. W osiemnastej rundzie jego przeciwnikiem jest Australijczyk David Eldar, mający trzy punkty straty do prowadzącego. O losach Bartka chwilowo niczego nie wiemy. W tej sytuacji zapraszamy po godzinie 8 rano na relację z decydującej fazy trzeciego dnia. Może tymczasem organizatorzy się odkorkują i będzie można śledzić mistrzostwa po angielsku...
 
<h2>Po 16. rundzie</h2> 
Ani się obejrzeliśmy i drugi dzień za nami. Dla Bartka był on trudny, ale najważniejsze, że przyszły w końcu zwycięstwa. Duch walki żyje! Tymczasem jutrzejsza przeciwniczka Bartka należy do największych pechowców tych mistrzostw. Po pierwsze w czternastej rundzie przegrała, ułożywszy 530 punktów (jej przeciwnik z Ugandy miał ich 627)! Po drugie przegrywała swoje partie najniżej ze wszystkich zawodników - sześć porażek kosztowało ją w sumie zaledwie -97 punktów różnicy. Nie mamy nic przeciwko jej kolejnej niewielkiej porażce w siedemnastej rundzie.<br><br>
W porównaniu z tabelą sprzed ośmiu rund zmieniła się dokładnie połowa pierwszej dziesiątki. Co ciekawe, dwaj z trzech prowadzących graczy byli po pierwszym dniu dalej, za to z wczorajszej pierwszej szóstki oprócz Boysa nie ma już w dziesiątce nikogo! Obecnie znajduje się w niej po trzech Tajów i Amerykanów, a ponadto Kanadyjczyk, Australijczyk, Nowozelandczyk i Kenijczyk. Z przeciwników Bartka najwyżej zawędrowali Amerykanin Thevenot (8. miejsce) i Pakistańczyk Khatri (34.). Z graczy dominujących w pierszej częsci turnieju Jighere i Fisher mają po 10 punktów, a Wapnick 8.<br><br>
Mamy nadzieję, że kondycji na trzeci wieczór kibicom nie zabraknie. Pamiętajcie, o drugiej w nocy szaliki w górę! Bartek! Bartek! Bartek!

<h2>Runda 16 (5 : 11)</h2>
<b>BP - Ralph Lobo (Zjednoczone Emiraty Arabskie) 459 : 357</b><br><br>
Kolejne zwycięstwo Bartka znów nieco poprawia jego sytuację i pozwala myśleć o marszu w górę nazajutrz. Czy to właśnie o tym dniu Szekspir napisał "Wszystko dobre, co kończy się dobrze"? Jutro kolejne spotkanie Bartka z Malezyjką, tym razem czeka na niego Vannitha Balasingam.<br><br> 
Dzień kończy się prowadzeniem trójki graczy z dwunastoma punktami. To Nemitrmansuk, Wiegand i Boys. Czyżby Taj przypomniał sobie, że do trzech razy sztuka? Już dwa razy był w finale i przegrał. W ósemce graczy z jedenastką znajduje się przedstawiciel Afryki, ale nie Wellington Jighere, którego wyraźnie opuściło szczęście, lecz Kenijczyk Michael Gongolo. Przewodzi tej grupie aktualny mistrz świata, Nigel Richards, a przy jej końcu jest też były czempion - Panupol Sujjayakorn.


<h2>Runda 15 (4 : 11)</h2>
<b>BP - Cecil Fernandes (Oman)  373 : 362</b><br><br>
Jest tak potrzebne zwycięstwo Bartka na przełamanie złej passy! Brawo! Łatwiej będzie grać w ostatniej partii dnia z Ralphem Lobo ze Zjednoczonych Emiratów Arabskich.<br><br> 
Nemitrmansuk pomścił kolegę. Po pięknie zagranym w ostatniej chwili SEDARIM (seder, wieczerza obrzędowa podczs Paschy) pokonał Boysa 412 : 369. W ten sposób szczyt góry znów się spłaszczył, bo dwaj prowadzący - Wiegand i Boys - mają już trzy porażki. Za nimi dwaj Tajowie - Nemitrmansuk i Panyasophonlert - z jednym punktem mniej i olbrzymia, piętnastoosobowa grupa pościgowa ze stratą dwóch punktów i Nigelem Richardsem na czele. 


<h2>Runda 14 (3 : 11)</h2>
<B>BP - Anthony Gomes (Katar) 370 : 436</b><br><br>
Żmija nie przeminęła. Pilnie potrzebny skuteczny środek na gady. W następnej partii przeciwnikiem Bartka będzie Omańczyk.<br><br> 
David Boys rozbił Panyasophonlerta. Ucieszyłem się, że widzę z jego pierwszego składu LArGEST, on jednak zagrał TALwEGS. Potem było już tylko lepiej, przeciwnik właściwie nie zagroził mu ani przez chwilę, do czego przyczynił się również drugi blank we właściwym momencie. Dwaj pozostali Tajowie z czołówki jednak wygrali i teraz zajmują czwarte, szóste i siódme miejsce, a najwyżej jest Nemitrmansuk. Czołówka zaczyna się rozciągać. Boys ma dwanaście punktów, Wiegand - jedenaście, a za nim ośmiu graczy z dziesięcioma, najwyżej Naween Fernando, Lankijczyk reprezentujący Australię. 


<h2>Runda 13 (3 : 10)</h2>
<b>BP - Rodney Judd (Pakistan) 336 : 400</b><br><br>
Trzeba mocno trzymać kciuki, bo zła passa niebezpiecznie się ciągnie. Ale jak kiedyś zauważył mistrz fraszki Jan Sztaudynger: "Wszystko przemija, nawet najdłuższa żmija". No więc jesteśmy, dopingujemy dalej (z piękniejącej strony PFS)!<br><br>
W meczu mistrzów Boys pokonał Richardsa 471 : 400 i utrzymał pozycję lidera. Po wysokim zwycięstwie nad jednym z Amerykanów na medalowej pozycji pojawił się najmniej utytułowany z Tajów w czołówce, Komol Panyasophonlert. Wyprzedza go jeszcze niedawny lider Dave Wiegand. Mistrzowie trzymają się mocno: Richards, Nyman i Sujjayakorn odpowiednio na piątej, dziewiątej i dziesiątej pozycji.


<h2>Po 12. rundzie</h2>
Wbrew moim przewidywaniom z rundy na rundę wyłonił się samodzielny lider. Niewiele to jednak znaczy, bo ma on bardzo szerokie zaplecze - dwudziestu graczy traci do niego punkt lub dwa punkty. Trwa przewaga przedstawicieli krajów anglojęzycznych, stanowiących 70% tej grupy. Na pozycję wicelidera powrócił urzędujący mistrz świata Nigel Richards. Bartek przeżywa w piątkowe przedpołudnie ciężkie chwile. Seryjne przegrywanie stanowi jednak nieodłączną część tej gry, o czym przekonał się na własnej skórze zapewne każdy z czytelników tej relacji. Ważne, by nie dać się zdołować i przełamać złą passę. Bartek! Bartek! Bartek!


<h2>Runda 12 (3 : 9)</h2>
<b>BP - Suanne Ong (Malezja) 399 : 512</b><br><br>
Trafić z niewielką liczbą punktów na tak mocnego przeciwnika byłoby doprawdy wielkim pechem, gdyby przeciwnik nie był tak uroczy. Jednak mimo niezaprzeczalnego uroku osobistego Suanne nie pozwoliła Bartkowi zrobić niespodzianki i zepchnęła go na sto czwarte miejsce. Po przerwie czas na kolejnego Pakistańczyka, potem w perspektywie Katarczyk.<br><br>
Pogrom na szczycie. Z pięciu posiadaczy dziewięciu punktów tylko jden wygrał partię. David Boys ponownie został liderem, pokonawszy Dave'a Wieganda. Za nim jeszcze ciaśniej niż było - aż dziewięciu graczy z dziewięcioma punktami, wśród nich czterech reprezentantów Stanów Zjednoczonych. Wapnick wreszcie przełamał złą passę, ale ma już trzy punkty straty do lidera.





<h2>Runda 11 (3 : 8)</h2>
<b>BP - Dielle Saldanha (Kanada) 309 : 417 </b><br><br>
Niestety, dzisiaj nie idzie. Ale tak to bywa w tej grze. Nawet sam Joel Wapnick właśnie przegrał czwartą partię z rzędu i bardzo oddalił się od czołówki... A Bartka czeka w następnej rundzie pojedynek z rewelacją mistrzostw w Mumbaju, Suanne Ong z Malezji.<br><br>
Zdaje się, że mamy nowego lidera. Dave Wiegand ze Stanów Zjednoczonych wskoczył na pierwsze miejsce, z tych, którzy jeszcze nie skończyli może mu zagrozić praktycznie tylko David Boys. Ale graczy z dziewięcioma punktami jest już trzech, a będzie sporo więcej. Ciasno w czołówce i chyba nieprędko wyłoni się zdecydowany lider.<br><br>
Na ciąg dalszy relacji zapraszam ok. godziny 8.30, a niecierpliwych zapraszam na stronę WSC.




<h2>Runda 10 (3 : 7)</h2>
<b>BP - Waseen Khatri (Pakistan) 321 : 553</b><br><br>
Teoria się nie sprawdziła. Najwidoczniej była to jedna z tych irytujących gier, których wygrać po prostu nie można. Trzymaj się, Bartek! Turniej trwa. Teraz Kanadyjka.<br><br>
Zmiana jak w kalejdoskopie. Na medalowych pozycjach nagle dwóch mistrzów: Boys prowadzi, za nim Richards. Obaj mają po osiem punktów, a z tym samym wynikiem jeszcze sześciu innych graczy. To mogą być mistrzostwa powrotu do tradycyjnego układu sił. W czołówce tymczasem trzech Amerykanów, Kanadyjczyk, Nowozelandczyk, Anglik, Australijczyk i tylko Nigeryjczyk Jighere zakłóca ten obraz. Młodzieży na razie turniej niezbyt się udaje. David Eldar z Australii na dwudziestym pierwszym miejscu z sześcioma punktami. Reszta dalej.




<h2>Runda 9 (3:6)</h2>
<b>BP - Ayorinde Saidu (Nigeria) 374 : 417)</b><br><br>
Nigeryjczyk jednak mocniejszy, choć niewątpliwie po walce. Dla Bartka na pocieszenie teoreytcznie łatwiejszy przeciwnik w następnej rundzie: Waseem Khatri z Pakistanu<br><br>
Pierwsza porażka lidera, były mistrz świata górą. Jeśli wierzyć internetowemu zapisowi partii, po skrablu Jighere z czeskim błędem Boys w końcówce spasował . Nie przeszkodziło mu to wygrać. Na potknięciu Nigeryjczyka skorzystał Australijczyk Fisher, który dwoma punktami pokonał Allana z Anglii i po dziewiątej rundzie wyszedł na prowadzenie. Aktualny mistrz świata Richards wrócił do ścisłej czołówki i jest piąty.


<h2>Runda 9 (wynik ok. godziny 3.00)</h2>
<b>BP - Ayorinde Saidu (Nigeria)</b><br><br>
Nigeryjczyk ma ranking ponad 2000, a to oznacza mocnego gracza, ale w Johor Bahru wyraźnie mu nie idzie. Na dwoje babka wróżyła...


<h2>Po 8. rundzie</h2>
Wygląda na to, że Anglicy i Amerykanie rzeczywiście wzięli sobie do serca porażkę w Mumbaju. W pierwszej dwudziestce mają po pięciu reprezentantów. Daleki Wschód tymczasem rozczarowuje - honor regionu ratuje dwóch Tajów. Afrykanin jest w dwudziestce tylko jeden, ale za to na prowadzeniu. Jeśli chodzi o kraje nienależące do tradycyjnych potęg scrabble'owych, najwyżej sklasyfikowano Adriana Tamasa z Rumunii (6 pkt., 14. miejsce) i Sherwina Rodriguesa z Indii (6, 16.). Trzy punkty Bartka to zdecydowanie dobry wynik, a jeśli wziąć pod uwagę niedostatek blanków - nawet bardzo dobry. Trzeba pamiętać, że początek turnieju dla niżej sklasyfikowanych zawodników oznacza przede wszystkim grę z tuzami rankingowymi, jeśli więc jutro wróżka nie będzie wrrrrróżką (anglojęzyczni skrabliści zamiast Pana Woreczkowego mają "tile fairy") i opamięta się z blankami, zadanie powinno być nieco łatwiejsze.<br><br>
A my o drugiej w nocy według naszego czasu znów unosimy szaliki... Bartek! Bartek! Bartek!


<h2>Runda 8 (3 : 5)</h2>
<b>BP - Allan Oyende (Kenia) 457 : 441</b> <br><br>
Dobrze! Zwycięstwo na zakończenie dnia i w sumie 88 miejsce. Końcówka najwyraźniej była zacięta i może wreszcie uśmiechnęło się do Bartka szczęście, bo z blogu wynika, że w siedmiu partiach miał tylko trzy blanki.<br><br> 
Wellington Jighere zwycięzcą pierwszego dnia. Nigeryjczyk dotarł do jednej trzeciej dystansu bez porażki. Za nim Andrew Fisher z Australii i - pierwszy raz w trójce - Paul Allan z Anglii, obaj po 7 zwycięstw. W klasyku na 5. stole Richards pokonał Wapnicka, a wynik partii (514:415) ucieszyłby pewnie numerologów. Wielcy mistrzowie się nie starzeją. Boys, mimo słabego startu, dotarł na czwarte miejsce, Richards zajmuje siódme, o oczko przed Sujjayakornem, a Wapnick jest dwunasty - wszyscy wygrali po sześć razy. Taki sam wynik osiągnął reprezentant Rumunii Adrian Tamas, najlepszy spośród graczy "egzotycznych".


<h2>Runda 7 (2 : 5)</h2>
<b>BP - Leslie Charles (Trynidad) 438 : 498</b><br><br>
 Bartek przegrał mimo ułożenia 438 punktów. To chyba trochę moja wina. W Melbourne grałem z przesympatycznym Trynidadczykiem ostatnią partię i na cztery kolejki przed końcem miałem do odrobienia 150 punktów z jednym miejscem na skrabla. Psim swędem odrobiłem. Najwyraźniej teraz los zrekompensował Trynidadczykowi tamtego psikusa, kierując się barwami narodowymi. Warto dodać, że Leslie Charles jest najstarszym z graczy w Johor Bahru (65 lat).<br><br>

Zaczynają się gry, których wyniki będą mieć duże znaczenie dla ostatecznego układu tabeli. Na trzech pierwszych stołach mecze na szczycie. W partii Wellingtona Jighere z Jimem Kramerem ten ostatni zagrał dość makabrycznie wyglądającego skrabla YEELINS (rówieśnicy) i tanie, ale miłe dla oka TOITOI (nowozelandzka trawa na strzechy!). Pasjonująca końcówka (warto obejrzeć) zakończyła się zwycięstwem Nigeryjczyka na odjęciu. Jighere został samodzielnym liderem, ponieważ Thevenot przegrał. Za nim - z jednym punktem straty - ośmiu graczy, wśród nich Fisher, Sujjayakorn, Thevenot, Dave Wiegand (St. Zj.) i Wapnick. Na końcu stawki Katarczyk wygrał, więc nie ma już gracza bez zwycięstwa. To dobrze.



<h2>Runda 6 (2 : 4)</h2>
<b>BP - Marc Roddis (Szwecja) 457 : 399</b><br><br>
Jest kolejny punkt! Jak widać, słabych graczy tu nie ma, bałtycki sąsiad walczył dzielnie, tym większe gratulacje dla Bartka. Awans na 91. miejsce. Teraz rozstawienia powinny być już łaskawsze i o następne zdobycze pewnie będzie trochę łatwiej.<br><br>
Obaj liderzy nadaj pozostają bez porażki. Thevenot przed Jighere. Na trzecie miejsce wyszedł Sujjayakorn, więc na pudle w tej chwili przedstawiciele trzech głównych sił scrabble'owych na świecie. Bez zwycięstwa pozostaje już tylko gracz z Kataru, wśród pechowców z jednym zwycięstwem jest reprezentant Stanów Zjednoczonych Jason Idalski.


<h2>Runda 5 (1 : 4)</h2>
<b>BP - Theresa Brousson (Malta) 356 : 468</b><br><br>
Bartek wciąż obraca się w doborowym towarzystwie zawodników z bardzo wysokimi rankingami. Nic dziwnego, że niełatwo ich ograć. Maltance do tej pory tu nie szło, ale rywalizację w Mumbaju zakończyła na dobrym 26 miejscu jako czwarty gracz z Europy. Ranking ponad 1900 jest wymowny.<br><br> 
Już tylko dwóch graczy z kompletem zwycięstw: Geoff Thevenot i Wellington Jighere. W pierwszej jedenastce aż czterech mistrzów świata, wszyscy z czterema punktami, tylko David Boys o punkt słabszy. Młodemu pokoleniu lideruje tymczasem David Eldar mimo czteropunktowej porażki po zaciętym boju z Wellingtonem Jighere (komentujący wyniki John Chew napisał o tej partii "an exciting nailbiter", czyli mniej więcej "obgryzacz paznokci"; można ją obejrzeć w Internecie).

<h2>Po 4. rundzie</h2>
Wśród 110 zawodników Bartek z jednym zwycięstwem na razie na setnym miejscu. W pierwszej piątce z kompletem punktów jest dwóch Amerykanów, Nigeryjczyk, Kenijczyk i Australijczyk. Prowadzi ten ostatni, David Eldar, były mistrz świata juniorów. Wśród Amerykanów znajduje się niedawny przeciwnik Bartka, Geoff Thevenot. Na razie więc górują kraje anglojęzyczne i Afryka, ale to dopiero sam początek. Grupa pościgowa z 3 punktami składa się z 29 osób. Trzech zamykających ją graczy mimo tak dobrego wyniku ma ujemną różnicę małych punktów. Ze znanych graczy słabo wystartowali Suanne Ong z Malezji, Akshay Bhandarkar z Emiratów Arabskich i Harshan Lambadusuriya z Wielkiej Brytanii.

<h2>Runda 4 (1 : 3)</h2>
<b>BP - Hubert Wee (Singapur) 390 : 543</b><br><br>
Pozornie wysoka porażka, ale Huberta Wee pamiętam z Mumbaju. Niezwykle chudy młody człowiek, za którym mama biegała z jabłkami. Widać coś w tych jabłkach było, bo skończył na siódmym miejscu, jako najlepszy z młodego pokolenia. Gdyby istniała na świecie sprawiedliwość, to Singapurczyk musiałby przyjechać zagrać po polsku z - powiedzmy - Kaziem Merklejnem...<br><br>
Bić mistrza! Panupol przegrał z Australijczykiem Andrew Fisherem. W świetnej formie nadal Joel Wapnick, który kolejny raz wygrał. Wellington Jighere, brązowy medalista z Mumbaju, zwycięsko wyszedł z bratobójczego nigeryjskiego pojedynku i ma cztery zwycięstwa. Zabrakło ostatecznie w stawce graczy srebnego medalisty z Mumbaju Ganesha Asirvathama z Malezji.



<h2>Runda 3 (1 : 2)</h2>
<b>BP - Michael Tang (Malezja) 347 : 443</b> <br><br>
Tym razem nie tylko reprezentant gospodarzy, lecz również jeden z głównych sprawców scrabble'owego boomu na Dalekim Wschodzie. Nie dość że organizuje duże turnieje, to jeszcze dobrze gra...<br><br>
Mistrzowie świata górą. Nigel Richards zlał Adriana Tamasa, również pozostali zanotowali zwycięstwa. Między innymi Brytyjczyk Mark Nyman wygrał z Tajem Pakornem Nemitrmansukiem, który dwukrotnie był w finale mistrzostw świata i dwukrotnie przegrał. To chyba największy pechowiec w historii tej imprezy. Cieszy się Rumunia - trzecie zwycięstwo Mihaia Pantisa! Jeszcze trzynastu graczy z kompletem punktów, prowadzi na razie były mistrz świata Panupol Sujjayakorn z Tajlandii.


<h2>Runda 2 (1 : 1)</h2>
<b>BP - Geoff Thevenot (St. Zj.) 410 : 498</b><br><br>
Kolejny native i kolejny wynik Bartka ponad 400 punktów. Tym razem bez zwycięstwa, ale jeszcze 22 rundy przed nami. Amerykańskiego gracza można obejrzeć w akcji na stronie WSC, w meczu 3. rundy przeciwko utytułowanemu Naweenowi Fernando, reprezentującemu Australię.<br><br>
Nigel Richards odegrał się za porażkę z pierwszej rundy, w czym pomógł mu sympatyczny LEMUROID. Z czterech byłych mistrzów świata wygrał tylko Panupol Sujjayakorn z Tajlandii. Za to najmłodszy uczestnik mistrzostw, William Kang, odegrał się za porażkę, a do tego pokazał, co umie: ułożył aż 574 punkty. Drugie zwycięstwo odniósł Mihai Pantis, jego rodak Adrian Tamas minimalnie pokonał Suanne Ong z Malezji. 


<h2>Runda 1 (1 : 0)</h2>
<B>BP - Peter Kougi (Australia) 419 : 401</B><br><br>
Fantastycznie! Native na dzień dobry i od razu zwycięstwo. Siódmy gracz rankingu australijskiego to podobnie jak Bartek debiutant na mistrzostwach świata, ale angielskiego zaczął się uczyć zdecydowanie wcześniej...<br><br>
Tymczasem urzędujący mistrz świata przegrał. Nigela Richardsa pokonał dużo niżej klasyfikowany Pichai Limprasert z Tajlandii. Czy to tylko wypadek przy pracy, czy może wynik dalekowschodniej ofensywy, zacznie być wiadomo pewnie około dziesiątej rundy. Również były mistrz świata David Boys z Kanady przegrał z graczem z Dalekiego Wschodu. Natomiast w meczu międzypokoleniowym stary mistrz Joel Wapnick z Kanady rozbił 14-letniego Williama Kanga z Malezji różnicą 268 punktów. Z niespodzianek warto odnotować zwycięstwo Rumuna Mihaia Pantisa nad Helen Gipson ze Szkocji.


<h2>Czekamy, czekamy...</h2>
U nas robi się późno, w Malezji powoli zaczna się dzień. Ech, ta różnica czasu... Za dwie i pół godziny rozpoczną się gry, niestety nie ma jeszcze grafiku do pierwszej rundy, więc trudno przewidzieć, kogo los przydzieli Bartkowi. Na poprzednich mistrzostwach system był raczej trudny do odcyfrowania, ponieważ jednak tymczaem nabrał rozmachu ranking Światowej Anglojęzycznej Organizacji Graczy w Scrabble (WESPA), więc zapewne zostanie wykorzystany.<br><br>

Niedawno oglądaliśmy Bartka w "Teleexpressie", teraz być może pojawi się również w BBC. W każdym razie brytyjscy dziennikarze chyba w scrabble nie grają, bo inaczej w spocie z Bartkiem wykorzystaliby nie słowo POLAND, którego w scrabble ułożyć nie wolno, lecz dozwolone POLISH (polski, Polak; pasta do butów, połysk) albo POLE (Polak; biegun).<br><br>

Wkrótce na oficjalnej stronie mistrzostw powinny się pojawić linki do wyników kolejnych rund, a także najciekawsze partie. Tych, którzy późno chodzą spać, zapraszamy do ich śledzenia... A rano przekonamy się, kto najlepiej wystartował w tym maratonie.


<h2>Uwaga, w czwartek zaczynają się emocje! Bartek gra od drugiej w nocy!</h2>
<img src="rozne/2009-wsc.png" class="onleft">
Bartek Pięta, nasz tegoroczny reprezentant na mistrzostwa świata, już rozpoczyna aklimatyzację w Johor Bahru, o czym można poczytać na <a href="http://www.literaxx.blogspot.com/" target="_blank">jego blogu</a>.<br><br>
Dla porządku przypomnijmy tymczasem, że anglojęzyczne Mistrzostwa Świata w Scrabble (World Scrabble Championship) odbywają się po raz dziesiąty, tytułu broni Nigel Richards z Nowej Zelandii, a chętnych do zajęcia jego miejsca na scrabble'owym tronie będzie ponad stu. Przez trzy dni zawodnicy rozegrają 24 rundy dość dziwacznym szwajcarem, a dwaj najlepsi powalczą w niedzielę o tytuł do trzech wygranych.<br><br>
Bartek wygrał tegoroczne eliminacje bardzo zdecydowanie, wszystko wskazuje więc na to, że powinien poprawić nasze dotychczasowe najlepsze, choć niezbyt dobre osiągnięcie (10 wygranych partii). Nieliczne, lecz bardzo zapalone grono polskich graczy w angielskie scrabble mocno w to wierzy. Nawiasem mówiąc, szkoda, że entuzjastów angielskiej wersji gry nie ma w u nas więcej, bo układane słowa wcale nie ustępują swą dziwacznością polskim. Wystarczy wspomnieć, że pierwszym alfabetycznie możliwym do ułożenia skrablem jest AARDVARK, a ostatnim ZYZZYVA (z dwoma blankami za Z, które w angielskim komplecie jest jedno).<br><br> 
Jeśli w scrabble można w ogóle cokolwiek sensownie przewidywać, to tegoroczne mistrzostwa powinny się odbywać - podobnie jak poprzednie - pod znakiem rywalizacji native'ów z reprezentantami Dalekiego Wschodu i Nigerii. Medaliści sprzed dwóch lat - oprócz Nowozelandczyka również Ganesh Asirvatham z Malezji i Jighere Wellington z Nigerii - na pewno tanio skóry nie sprzedadzą. Atakować ich będzie młode pokolenie, w tym chudy jak patyk Hubert Wee z Singapuru, równie urocza, jak świetnie grająca Suanne Ong z Malezji, były mistrz świata juniorów David Eldar z Australii i jego następca, 15-letni (!) Charnrit Khongthanarat z Tajlandii. Na drugim biegunie znajduje się 63-leni nestor elity, mistrz świata z 1999 roku, Joel Wapnick z Kanady, który w Mumbaju za czwarte miejsce dostał od sali standing ovation. Wiele do udowodnienia mają Anglicy i Amerykanie, którzy przed dwoma laty ponieśli klęskę. Ci pierwsi stracili tymczasem swoją liderkę (Helen Gipson gra w tym roku w barwach Szkocji), ale za to radykalnie odmłodzili skład, a ponieważ Lewis Mackay przewodzi światowemu rankingowi, mogą mieć nadzieję przynajmniej na miejsca w pierwszej dziesiątce. Ci drudzy wydają się nie mieć zdecydowanego lidera, za to jest ich w Malezji aż czternastu. Z graczy „egzotycznych” chętnie zrobiliby niespodziankę Theresa Brousson z Malty i Adrian Tamas z Rumunii. A może...<br><br>
Mistrzostwa będziemy relacjonować, zapraszamy również na cztery dni uczty scrabble'owej pod adresem:<br><br>
<a href="http://www.wscgames.com/2009/index.html" target="_blank">http://www.wscgames.com/2009/index.html</a><br><br>
A teraz szaliki w górę... Bartek! Bartek! Bartek!<br><br>

<span class="autor">relacjonuje: Wojtek Usakiewicz</span>


<?require_once "files/php/bottom.php"?>
</body>
</html>
