<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Scrabble : Reguły gry w SCRABBLE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("turnieje","rekordy");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
    <style type="text/css">
        .rekord {
            margin: 16px 50px;
            text-align: center;
        }
        .rekord h3 {
            color: #E7484F;
            font-size: 13px;
            font-weight: bold;
            margin-bottom: 4px;
        }
        a.zapis{
            float: none;
            display: inline-block;
            vertical-align: middle;
        }
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Rekordy turniejowe")</script></h1>

Notowane są wyniki uzyskane podczas oficjalnych turniejów Scrabble, w partiach granych o zwycięstwo (nie systemem „high&nbsp;score”).<br /><br />

<div id="tabs" style="display:none;">
    <ul>
        <li><a href='#tabs-wynik'>Wynik</a></li>
        <li><a href='#tabs-suma'>Suma wyników</a></li>
        <li><a href='#tabs-remis'>Remis</a></li>
        <li><a href='#tabs-pokonany'>Wynik pokonanego</a></li>
        <li><a href='#tabs-otwarcie'>Otwarcie</a></li>
        <li><a href='#tabs-ruch'>Ruch</a></li>
        <li><a href='#tabs-lubudu'>Skrable na otwarciu/zamknięciu</a></li>
        <li><a href='#tabs-siodemki'>Ilość skrabli</a></li>
        <li><a href='#tabs-rzut'>Rzut na taśmę</a></li>
        <li><a href='#tabs-male'>Średnia mp w 12 rundach</a></li>
        <li><a href='#tabs-dwanascie'>12 rund bez porażki</a></li>
        <li><a href='#tabs-seria'>Seria zwycięstw</a></li>
        <li><a href='#tabs-tur'>Wygrane turnieje z rzędu</a></li>
        <li><a href='#tabs-wynik-samo'>Wynik samodzielny</a></li>
        <li><a href='#tabs-solo'>Gra jednoosobowa</a></li>
    </ul>

<div id='tabs-wynik'>
<h2>Najwyższy wynik w partii pełnej</h2>
<b>Partia pełna</b> — gdy obaj gracze grają do końca (nikomu nie skończył się czas)<br />
    <div class="rekord">
        <h3>721 punktów</h3>
        <a href="http://www.pfs.org.pl/zapis.php?id=13335" target="_blank" class="zapis"></a>
        <b>Michał Alabrudziński</b> 2 czerwca 2011 w Milanówku, w 4. rundzie VII Mistrzostwa Milanówka „Truskawki w Milanówku”

    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li>719 pkt. — Krzysztof Mówka (8 sierpnia 2004, Słupsk, 11. runda VIII Mistrzostw Ziemi Słupskiej)</li>
		<li>707 pkt. — Maciej Czupryniak (11 sierpnia 2001, Katowice, 1. runda VI Mistrzostw Górnego Śląska i Zagłębia)</li>
        <li>662 pkt. — Paweł Stanosz (24 kwietnia 1999, Szczecin, 1. runda III Mistrzostw Ziemi Szczecińskiej)</li>
        <li>637 pkt. — Paweł Stanosz (16 listopada 1997, Warszawa, 5. runda „Turnieju Frustratów” V Mistrzostw Polski)</li>
        <li>606 pkt. — Paweł Stanosz (15 sierpnia 1997, Warszawa, 2. runda II Mistrzostw Warszawy)</li>
    </ul>
    </div>
</div>

<div id='tabs-suma'>
<h2>Najwyższa suma wyników</h2>
    <div class="rekord">
        <h3>1099 punków</h3>
        <a href="zapis.php?id=47" target="_blank" class="zapis"></a><b>Tomasz Zwoliński</b> i <b>Magdalena Kupczyńska</b> 13 lutego 2010 w Sulęcinie, w 6. rundzie Turnieju Walentynkowego.<br />Partię wygrał Tomasz Zwoliński 712:387.
    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li>1069 pkt. — Grzegorz Święcki - Wiktor Stępień  685:384 (24 listopada 2002, Warszawa, 5. runda Turnieju Frustratów)</li>
        <li>1036 pkt. — Walenty Siemaszko - Szymon Zaleski 404:632 (6 października 2002, Poznań, 8. runda II Mistrzostw Poznania)</li>
        <li class="zapis"><a href="zapisy/zbi-gor.html" target="_blank" class="zapis"></a>1031 pkt. — Mateusz Żbikowski - Kamil Górka 577:454 (2 marca 2002, Warszawa, 5. runda VII Mistrzostw Warszawy)</li>
        <li>1004 pkt. — Szymon Zaleski - Robert Duczemiński 587:417 (15 października 2001, Szczecin, 7. runda IV Mistrzostw Szczecina)</li>
        <li>992 pkt. — Krzysztof Usakiewicz - Wojciech Usakiewicz 533:459 (8 kwietnia 2001, Inowrocław, 8. runda V Mistrzostw Ziemi Kujawskiej)</li>
        <li class="zapis"><a href="zapisy/514-468.html" target="_blank" class="zapis"></a>982 pkt. — Małgorzata Kania - Marcin Kamiński 514:468 (14 listopada 1999, Warszawa, 1. runda Turnieju Frustratów VII Mistrzostw Polski)</li>
        <li>974 pkt. — Paweł Stanosz - Krzysztof Bukowski 581:393 (23 sierpnia 1998, Tczew, 7. runda I Mistrzostw Tczewa)</li>
        <li class="zapis"><a href="zapisy/545-421.html" target="_blank" class="zapis"></a>966 pkt. — Małgorzata Kania - Zdzisław Szota 545:421 (7 września 1997, Bydgoszcz, 2. runda I Mistrzostw Bydgoszczy)</li>
        <li class="zapis"><a href="zapisy/529-436.html" target="_blank" class="zapis"></a> 965 pkt. — Edward Stefaniak - Jolanta Hoffmann 529:436 (16 sierpnia 1997, Warszawa, 11. runda II Mistrzostw Warszawy)</li>
        <li>911 pkt. — Dariusz Banaszek - Robert Duczemiński 486:425 (28 maja 1995, Warszawa, 1/8 finału I Turnieju o Puchar Polski)</li>
        </ul>
    </div>
</div>

<div id='tabs-remis'>
<h2>Najwyższy remis</h2>
    <div class="rekord">
        <h3>474:474</h3>
        <a href="http://www.pfs.org.pl/zapisy/474-474.html" target="_blank" class="zapis"></a><b>Dariusz Białobrzewski</b> i <b>Tomasz Wiśniewski</b> 21 września 2003 w 7. rundzie II Mistrzostw Torunia
    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li>433:433 — Grzegorz Koczkodon - Miłosz Wrzałek (27 kwietnia 2003, Bydgoszcz, 9. runda I Otwarte Mistrzostw Bydgoszczy)</li>
        <li>423:423 — Magdalena Merklejn - Mateusz Żbikowski (8 sierpnia 1999, Tczew, 8. runda II Mistrzostw Tczewa)</li>
        <li>415:415 — Michał Derlacki - Paweł Stanosz (6 czerwca 1998, Sudomie, 5. runda II Mistrzostw Kaszub)</li>
        <li>415:415 — Marek Roszak - Maciej Czupryniak (19 kwietnia 1998, Inowrocław, 10. runda II Mistrzostw Kujaw)</li>
        <li>388:388 — Jolanta Hoffmann - Paweł Stanosz (14 września 1997, Rzeszów, 2. runda I Mistrzostw Rzeszowa)</li>
        <li>387:387 — Paweł Stefaniak - Marcin Skrzyniarz (26 listopada 1995, Warszawa, finał III Mistrzostw Polski)</li>
        </ul>
    </div>
</div>

<div id='tabs-pokonany'>
<h2>Najwyższy wynik pokonanego</h2>
    <div class="rekord">
        <h3>515 punktów</h3>
        <a target="_blank" href="zapisy/mow-skr.html" class="zapis"></a><b>Krzysztof Mówka</b> w partii przegranej z Mariuszem Skroboszem 515:518<br />(5 lutego 2005, Łódź, 6. runda turnieju Wielka Łódka 2005 — Zima).<br><br>

	    Rekord wyrównał <b>Jakub Zaryński</b> 18 marca 2012 w Szczecinie w 11 rundzie XIV Mistrzostwa Szczecina "Zanim zakwitną magnolie", w partii przeciwko Maciejowi Grąbczewskiemu (wynik: 515:532).  </div>

	<div class="historia">Historia rekordu:
    <ul>
        <li>495 pkt. — Stanisław Rydzik w partii przegranej z Andrzejem Kwiatkowskim 495:502(15 września 2002, Lublin, 9. runda I Mistrzostw Lublina)</li>
        <li>476 pkt. — Jolanta Hoffmann w partii przegranej ze Sławomirem Noskiem 476:480 (17 sierpnia 1997, Warszawa, 16. runda II Mistrzostw Warszawy)</li>
        <li>436 pkt. — Jolanta Hoffmann w partii przegranej z Edwardem Stefaniakiem 436:529 (16 sierpnia 1997, Warszawa, 11. runda II Mistrzostw Warszawy)</li>
        <li>425 pkt. — Robert Duczemiński w partii przegranej z Dariuszem Banaszkiem 425:486 (28 maja 1995, Warszawa, 1/8 finału I Turnieju o Puchar Polski)</li>
    </ul>
    </div>
</div>

<div id='tabs-otwarcie'>
<h2>Najlepsze otwarcie</h2>
    <div class="rekord">
        <h3>112 punktów</h3>
        <b>Radosław Sowiński</b> 11 maja 2002 w Katowicach, podczas II Maratonu Scrabble, rozpoczynając partię przeciwko Adamowi Jurkowskiemu od słowa ŹREBIEŃ (H4-H10, blank za E).<br />Partię wygrał Radosław Sowiński 438:253.
    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li>100 pkt. — Wojciech Zemło za ŹGNIĘTE (8D-8J, blank za T) w partii przegranej z Aleksandrą Merklejn 459:460 (20 lutego 2000, Warszawa, 12. runda  VI Mistrzostw Warszawy)</li>
        <li>100 pkt. — Zdzisław Szota za KLUCZEŃ (8F-8L) w partii wygranej z Andrzejem Rechowiczem 392:353 (5 kwietnia 1998, Łódź, 12. runda IV Mistrzostw Łodzi)</li>
        <li>100 pkt. — Stefan Dawidson za UŚWIŃŻE (H8-H14, blank za Ż) w partii wygranej z Grzegorzem Wiączkowskim 390:384 (11 maja 1997, Wrocław, 4. runda I Mistrzostw Wrocławia)</li>

    </ul>
    </div>
</div>

<div id='tabs-ruch'>
<h2>Najwyżej punktowany ruch</h2>
    <div class="rekord">
        <h3>347 punktów</h3>
        <a target="_blank" href="zapis.php?id=14" class="zapis"></a><b>Sebastian Orzelski</b> 15 listopada 2009 w Łomży w 13. rundzie I Otwartych Mistrzostw Łomży w partii przeciwko Krzysztofowi Sporczykowi za słowo <b>SPÓŹNIŁY</b> przechodzące przez dwa pola potrójnej premii słownej (O1-O8).<br />Partię wygrał Sebastian Orzelski 669:299.
    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li class="zapis"><a target="_blank" href="zapisy/bor-las.html" class="zapis"></a>257 pkt. — Jarosław Borowski za PODŹGANO w partii wygranej z Mariuszem Łasiewickim 691:207 (29 maja 2005, Bielsk Podlaski, 8. runda VII Mistrzostw Podlasia) ułożone między dwoma polami potrójnej premii słownej (A8-A15)</li>
        <li class="zapis"><a target="_blank" href="zapisy/szm-wol.html" class="zapis"></a>257 pkt. — Andrzej Szmidtke za DRŻĄCZKA w partii wygranej z Marcelą Wołowiec 648:347 (28 sierpnia 2004, Jurata, 3. runda II Mistrzostw Bałtyku) ułożone między dwoma polami potrójnej premii słownej (O1-O8)</li>
        <li class="zapis"><a href="zapisy/puc-zwo.html" target="_blank" class="zapis"></a>249 pkt. — Jarosław Puchalski za CHŁOŚCIE w partii wygranej z Tomaszem Zwolińskim 590:460 (7 marca 2004, Zakopane, 4. runda IV Mistrzostw Podhala) ułożone między dwoma polami potrójnej premii słownej (H1-O1)</li>
        <li>212 pkt. — Leszek Stecuła za GROMNICĄ w partii wygranej z Jarosławem Michalakiem 652:264 (2 maja 2003, Łódź, 3. runda Maratonu Scrabblowego) ułożone między dwoma polami potrójnej premii słownej (A1-H1)</li>
        <li>212 pkt. — Anna Pendowska za OKRĘCONY w partii wygranej z Dominiką Śniadach 544:343 (7 kwietnia 2001, Inowrocław, 2. runda V Mistrzostw Ziemi Kujawskiej) ułożone między dwoma polami potrójnej premii słownej (A1-A8)</li>
        <li>212 pkt. — Sławomir Nosek za ZANOŚCIE (15H-15O) w partii wygranej z Jackiem Gigołą 463:398 (14 listopada 1998, Warszawa, 7. runda eliminacji VI Mistrzostw Polski)</li>
        <li>212 pkt. — Maciej Czupryniak za DOGASZEŃ (A1-A8) w partii wygranej z Mariuszem Gancarczykiem 571:275 (24 maja 1998, Biała Podlaska, 10. runda II Mistrzostw Podlasia)</li>
        <li>194 pkt. — Tomasz Zwoliński za SKASUJMY (A8-A15, blank za M) w partii wygranej z Bogdanem Pokojskim 598:241 (9 listopada 1997, Piastów, 4. runda I Mistrzostw Piastowa)</li>
        <li>185 pkt. — Paweł Stanosz za ZARYBIAĆ (O8-O15, blank za A) w partii wygranej z Pawłem Dawidsonem 427:379 (15 sierpnia 1997, Warszawa, 3. runda II Mistrzostw Warszawy)</li>
        <li>176 pkt. — Dariusz Banaszek za HISUJESZ w partii przegranej z Tomaszem Zwolińskim 388:469 (9 marca 1997, Płock, 3. runda I Mistrzostw Mazowsza)</li>
        </ul>
    </div>
</div>

<div id='tabs-lubudu'>
<h2>Najwięcej skrabli na otwarciu</h2>
    <div class="rekord">
        <h3>sześć</h3>
        <a href="http://www.pfs.org.pl/zapis.php?id=124" target="_blank" class="zapis"></a>Sześć skrabli w swoich sześciu pierwszych ruchach ułożyła <b>Irena Sołdan</b> 20 listopada 2011 w Warszawie (14 runda Turnieju Towarzyszącego XIX Mistrzostw Polski), w partii przeciwko Agnieszce Goniowskiej. Były to słowa: BUDOWAŁ, NIEROJNĄ, ZMĘCZYŁO, LUTERANY, DZIEWKA oraz ŻONKOSIA.</div>

    <div class="historia">Historia rekordu:
    <ul>
        <li class="zapis"><a href="zapisy/5siodemekotwarcie.html" target="_blank" class="zapis"></a>pięć — Mateusz Żbikowski (13 sierpnia 2006 w Wałczu, w partii z Bogusławem Książkiem: OPISAŁA, NIESPITY, ENKLAWĄ, NIELUŹNI oraz BASICAMI)</li>
        <li class="zapis"><a href="zapisy/mow-szm.html" target="_blank" class="zapis"></a>cztery — Krzysztof Mówka (8 sierpnia 2004 w Słupsku, w partii z Andrzejem Szmidtke: IZOHELA, ROPIENIE, ZADAWALI, SIARKOM. Partię wygrał (ułożywszy w sumie 7 siódemek) Krzysztof Mówka 719:308.)</li>
        <li>cztery — Karol Wyrębkiewicz (11 listopada 2001 w Łodzi, w partii z Pawłem Dawidsonem. Partię wygrał Karol Wyrębkiewicz 524:384.)</li>
        <li class="zapis"><a href="zapisy/cze-ban1.html" target="_blank" class="zapis"></a>cztery — Dariusz Banaszek (21 lutego 1999 w Warszawie, w partii z Waldemarem Czerwońcem: SMAŻCIE, NASIEDŹ, FRANKACH i ROTOROWI. Partię wygrał Dariusz Banaszek 479:300.)</li>
    </ul>
    </div>

<h2>Najwięcej skrabli na zamknięciu</h2>
    <div class="rekord">
        <h3>cztery</h3>
        Cztery skrable w swoich czterech ostatnich ruchach ułożył <b>Paweł Jackowski</b> 20 listopada 2004 w Warszawie, w 7. rundzie XII Mistrzostw Polski, w partii przeciwko Kazimierzowi Merklejnowi jr. Były to słowa: ŁAPSZAMI, DUMANIE, NIEZBYTĄ i SAGOWIEC. Partię wygrał Paweł Jackowski 531:275.</div>
</div>

<div id='tabs-siodemki'>
    <h2>Najwięcej skrabli w partii:</h2>
    <div class="rekord">
        <h3>siedem</h3>
        <a href="zapisy/mow-szm.html" target="_blank" class="zapis"></a><b>Krzysztof Mówka</b> 8 sierpnia 2004 w Słupsku, w 11. rundzie VIII Mistrzostw Ziemi Słupskiej, w partii przeciwko Andrzejowi Szmidtke. Były to słowa: IZOHELA, ROPIENIE, ZADAWALI, SIARKOM, NADETNĄ, TROCKIE, CEWIŁOM. Partię wygrał Krzysztof Mówka 719:308.<br><br>

    <a href="http://www.pfs.org.pl/zapis.php?id=124" target="_blank" class="zapis"></a><b>Irena Sołdan</b> 20 listopada 2011 w Warszawie w 14 rundzie Turnieju Towarzyszącego XIX Mistrzostw Polski, w partii przeciwko Agnieszce Goniowskiej. Były to słowa: BUDOWAŁ, NIEROJNĄ, ZMĘCZYŁO, LUTERANY, DZIEWKA, ŻONKOSIA, TEMATYCE. Partię wygrała Irena Sołdan 690:274 (błąd w liczeniu, powinno być 690:264).  </div>



    <div class="historia">Historia rekordu:
    <ul>
        <li>sześć — Małgorzata Kania (14 grudnia 2003, Warszawa, 19. runda III 24-godzinnego Turnieju Non-Stop, w partii wygranej z Iwoną Baran 562:308) — ZAGNIJMY, NIERYBIE, NIEZMYTA, SALONOWI, RZEPAKOM, POWIEWAŁ.</li>
        <li class="zapis"><a href="zapisy/zwo-gos.html" target="_blank" class="zapis"></a>sześć — Tomasz Zwoliński (17 listopada 2002, Warszawa, 7. runda I fazy X Mistrzostw Polski, w partii wygranej z Andrzejem Gostomskim 644:277) — KWIETNA, BESZTAĆ, ROPIENIE, SZPACIE, ZAPASKO, NAMACHA.</li>
        <li class="zapis"><a href="zapisy/skb-mad.html" target="_blank" class="zapis"></a>sześć — Mariusz Skrobosz (25 sierpnia 2001, Słupsk, 4. runda VI Mistrzostw Ziemi Słupskiej, w partii wygranej z Beatą Madej 590:248) — SŁOJAMI, RIUSZEK, SPRASZAM, PIDŻYNIE, OBRONIMY, SZALENI.</li>
        <li>sześć — Mariusz Skrobosz (24 kwietnia 1999, Grójec, 3. runda I Mistrzostw Grójca, w partii wygranej z Mateuszem Żbikowskim 629:218) — WYDOLNE, TAJSKIE (niedozwolone wówczas, lecz nie sprawdzone), ZAGONIKI, ZEPRANY, HURYSIE i ZWOŹCIEŻ.</li>
        </ul>
    </div>
</div>

<div id='tabs-rzut'>
<h2>Najlepszy rzut na taśmę:</h2>
    <div class="rekord">
        <h3>-169/+65 punktów</h3>
        <b>Sławomir Nosek</b> 14 listopada maja 1998 w Warszawie, w 7. rundzie eliminacji VI Mistrzostw Polski, przegrywając 240:409, ułożył, zakrywając dwa pola potrójnej premii słownej, słowo ZANOŚCIE (15H-15O) za 212 punktów. W woreczku nie było już liter i po uwzględnieniu 11 punktów za litery na stojaku przeciwnika rekordzista wygrał 463:398. Pechowym przeciwnikiem rekordzisty był Jacek Gigoła.
    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li>-102/+23 pkt. — Dariusz Banaszek (18 maja 1997, Poznań, 3. runda I Mistrzostw Poznania), który przegrywając 308:410, wykonał ruch SZANUJĘ/SNÓW (1F-1L, blank za N) za 115 punktów, co po uwzględnieniu 5 punktów za litery na stojaku przeciwnika dało zwycięstwo 428:405. Przeciwnikiem Dariusza Banaszka był Tomasz Zwoliński.</li>
        <li>-96/+2 pkt. — Janusz Grodzki (9 marca 1997, Płock, 2. runda I Mistrzostw Mazowsza), który przegrywając 253:349, wykonał ruch OKTANIE/OZ za 78 punktów, co po uwzględnieniu 10 punktów za litery na stojaku przeciwnika dało zwycięstwo 341:339. Przeciwnikiem Janusza Grodzkiego był Maciej Czupryniak.</li>
    </ul>
    </div>
</div>

<div id='tabs-male'>
    <h2>Największa średnia małych punktów w turnieju 12-rundowym i dłuższym</h2>
    <div class="rekord">
        <h3>467,17 punkty</h3>
        (5606 w 12 rundach)<br>
<b>Michał Alabrudziński</b> (<a href="relacja.php?id=609">VII Mistrzostwa Milanówka „Truskawki w Milanówku”, 2012</a>)
    </div>
    <div class="historia">Historia rekordu:
    <ol>

		<li value="8">463,67 (5564 w 12 rundach) — Stanisław Rydzik (<a href="relacja.php?id=549">II Otwarte Mistrzostwa Raciborza w Scrabble, 2011</a>)</li>
        <li value="7">460,00 (5980 w 13 rundach) — Mariusz Skrobosz (<a href="relacja.php?id=570">XIII Mistrzostwa Szczecina, 2011</a>)</li>
        <li value="6">457,00 (5941 w 13 rundach) — Kazimierz Merklejn jr (<a href="relacja.php?id=464">XI Mistrzostwa Szczecina, 2009</a>)</li>
        <li value="5">452,83 (5434 w 12 rundach) — Stanisław Rydzik (<a href="relacja.php?id=137">III Mistrzostwa Katowic, 2006</a>)</li>
        <li value="4">451,75 (5421 w 12 rundach) — Dawid Pikul (<a href="relacja.php?id=152">I Mistrzostwa Wałcza, 2005</a>)</li>
        <li value="3">450,33 (5404 w 12 rundach) — Mateusz Żbikowski (<a href="relacja.php?id=202">IV Mistrzostwa Ziemi Ostródzkiej, 2004</a>)</li>
        <li value="2">448,42 (5391 w 12 rundach) — Tomasz Zwoliński (<a href="relacja.php?id=260">VI Mistrzostwa Ziemi Słupskiej, 2002</a>)</li>
        <li value="1">437,08 (5245 w 12 rundach) — Dariusz Banaszek (<a href="relacja.php?id=351">Turniej Otwarcia Sezonu, 1999</a>)</li>
    </ol><br>
<a href="http://scrabble.stats.org.pl/kstat.php?k=maxmp12" target="_blank">Najwyższe średnie punktowe w turniejach 12 rundowych i dłuższych</a>
    </div>
</div>

<div id='tabs-dwanascie'>
<h2>Zwycięzcy 12-rundowych turniejów bez porażki</h2>
    <div class="historia">
    <ol>
        <li>Paweł Stanosz — <a href="relacja.php?id=388">II Mistrzostwa Górnego Śląska (1997)</a>, skalpy 2057, ranking 171,42</li>
        <li>Tomasz Zwoliński — <a href="relacja.php?id=308">IV Mistrzostwa Ziemi Słupskiej (2000)</a>, skalpy 1935, ranking 161,25</li>
        <li>Radosław Konca — <a href="relacja.php?id=284">V Mistrzostwa Ziemi Słupskiej (2001)</a>, skalpy 2039, ranking 169,92</li>
        <li>Mariusz Skrobosz — <a href="relacja.php?id=256">V Mistrzostwa Warmii i Mazur (2002)</a>, skalpy 2121, ranking 176,75</li>
        <li>Mariusz Skrobosz — <a href="relacja.php?id=211">III Mistrzostwa Wybrzeża (2004)</a>, skalpy 2046, ranking 170,50</li>
        <li>Justyna Fugiel — <a href="relacja.php?id=169">X Mistrzostwa Górnego Śląska i Zagłębia (2005)</a>, skalpy 2183, ranking 181,92</li>
        <li>Andrzej Oleksiak — <a href="relacja.php?id=168">IV Mistrzostwa Dolnego Śląska (2005)</a>, skalpy 1934, ranking 161,17</li>
        <li>Marek Syczuk — <a href="relacja.php?id=131">V Mistrzostwa Wybrzeża (2006)</a>, skalpy 1977 (z 11-0), ranking 179,73</li>
        <li>Kazimierz.J Merklejn — <a href="relacja.php?id=84">VII Mistrzostwa Ziemi Ostródzkiej „Blanki w szranki” (2007)</a>, skalpy 2153, ranking 179,42</li>
        <li>Karol Wyrębkiewicz — <a href="relacja.php?id=17">Wielka Łódka 2008 — Wiosna „10 Lat Minęło” (2008)</a>, skalpy 2114, ranking 176,17</li>
        <li>Mateusz Żbikowski — <a href="relacja.php?id=26">VIII Mistrzostwa Ziemi Ostródzkiej „Blanki w szranki” (2008)</a>, skalpy 2107, ranking 175,58</li>
        <li>Marek Reda — <a href="relacja.php?id=567">I Turniej Via Baltica (2011)</a>, skalpy 2099, ranking 174,92</li>
        <li>Grzegorz Kurowski — <a href="relacja.php?id=677">XV Otwarte Mistrzostwa Nowego Dworu Mazowieckiego (2012)</a>, skalpy 2200, ranking 183,33</li>

    </ol>
    </div>
</div>

<div id='tabs-seria'>
<h2>Najdłuższa seria zwycięstw</h2>
    <div class="rekord">
        <h3>21 partii</h3>
        Tyle partii z rzędu wygrał w oficjalnych turniejach <b>Karol Wyrębkiewicz</b><br />od 27 kwietnia 2008 (Katowice) do 1 czerwca 2008 (Zabrze).<br><br>
		Rekord wyrównał <b>Grzegorz Kurowski</b> od 24 października 2010 (Legnica) do 14 listopada 2010 (Mistrzostwa Polski)
    </div>

    <div class="historia">Historia rekordu:
    <ul>
        <li>19 partii — Paweł Stanosz (od 12 października 1997 — Wrocław do 9 listopada 1997 — Piastów)</li>
		<li>16 partii — Tomasz Zwoliński (od 26 stycznia 1997 — Bielsko-Biała do 16 lutego 1997 — Białystok)</li>
	</ul>
    </div>
</div>

<div id='tabs-tur'>
<h2>Największa ilość wygranych turniejów z rzędu</h2>
    <div class="rekord">
        <h3>6 turniejów</h3>
        Tyle turniejów z rzędu wygrał <b>Tomek Zwoliński</b> w 2001 roku.<br />
Mistrzostwa Warszawy, Turniej o Puchar radia Plus, Mistrzostwa Ziemi Kujawskiej,<br />Mistrzostwa Piastowa, Mistrzostwa Krakowa, Turniej „Blanki w szranki”</div>
</div>

<div id='tabs-wynik-samo'>
<h2>Najwyższy wynik w partii samodzielnej</h2>
    <b>Partia samodzielna</b> — gdy jednemu z graczy skończył się czas, w związku z czym drugi rozgrywa część partii sam, co znacznie ułatwia osiągnięcie wysokiego wyniku (można np. przygotowywać sobie bezpieczne wystawki).<br />
    <div class="rekord">
        <h3>911 punktów</h3>
        <a href="zapisy/911.html" target="_blank" class="zapis"></a><b>Sławomir Kordjalik</b> 29 maja 2004 w Bielsku Podlaskim, w 1. rundzie VI Mistrzostw Podlasia.
    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li>717 pkt. — Tomasz Zwoliński 9 lutego 1997 w Gdyni, w 1. rundzie I Mistrzostw Trójmiasta</li>
    </ul>
    </div>
</div>

<div id='tabs-solo'>
<h2>Rekord Polski w grze jednoosobowej</h2>
    <div class="rekord">
        <h3>2819 punktów</h3>
        Taki wynik uzyskał <b>Zdzisław Szota</b> podczas próby przeprowadzonej 21 listopada 1998 w Warszawie.
    </div>
    <div class="historia">Historia rekordu:
    <ul>
        <li>2353 pkt. — Michał Derlacki, 11 czerwca 1998, Sudomie, kluczowe słowa: TOWIAŃSZCZYŹNIE (27-krotność) i UCZĘSTOWALIŚCIE (27-krotność)</li>
        <li>2292 pkt. — Tomasz Zwoliński, 7 czerwca 1998, Sudomie, kluczowe słowa: UKOŃCZYŁYBYŚCIE (27-krotność) i GRÓDŹMYŻ (9-krotność)</li>
        <li>1882 pkt. — Michał Derlacki, 30 maja 1998, Warszawa, kluczowe słowo: PODŹWIGNĘLIŚCIE (27-krotność)</li>
        <li>1819 pkt. — Tomasz Zwoliński, 23 maja 1998, Biała Podlaska, kluczowe słowo: UKOŃCZYŁYBYŚCIE (27-krotność)</li>

    </ul>
    </div>
</div>

</div>
<? require_once "files/php/bottom.php"; ?>
</body>
</html>
