<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Wszystko dla scrabblisty</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("roznosci","download");</script>
</head>


<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Wszystko dla scrabblisty")</script></h1>

Jeśli posiadasz materiały, które mogą przydać się innym scrabblistom i chcesz się nimi podzielić — <a onClick="sendMail('pfs','pfs.org.pl')">napisz do nas!</a><br><br>

<a href="osps_aktualizacje.php">Aktualizacje do OSPS</a><br>
<a href="rozne/pfs_logo.png">Logo PFS (format PNG, 600x385)</a><br>

<?require_once "files/php/bottom.php"?>
</body>
</html>

