<? require_once "files/php/funkcje.php";

function show_calendar_tours ($year) {
    global $DB_TABLES, $TOUR_STATUS;

    print "<table id='bottompanel'>";
    for ($month = 1; $month <= 12; $month++) {
        $tours = pfs_select (array (
            table   => $DB_TABLES[tours],
            fields  => array ( 'nazwa', 'data_od', 'data_do', 'miasto', 'id', 'rank' ),
            where   => array ( 'YEAR(`data_od`)' => $year, 'MONTH(`data_od`)' => $month ),
            order   => array ( 'data_od' )
        ));

        if (!count ($tours)) {
            continue;
        }

        print "<tr><td colspan='3'><h2>". ucfirst (strftime ("%B", mktime (0, 0, 0, $month, 1, $year))) ."</h2></td></tr>";

        foreach ($tours as $tour) {
            print "<tr>
                <td>". wyswietlDate ($tour->data_od, $tour->data_do, false) ."</td>
                <td>$tour->miasto</td>
                <td><a href='../turniej.php?id=$tour->id'>$tour->nazwa ";

            switch ($tour->rank) {
                case $TOUR_STATUS['gp']:        print " <span class='gp'>Grand&nbsp;Prix&nbsp;$year</span>"; break;
                case $TOUR_STATUS['norank']:    print " <b>(turniej&nbsp;nierankingowy)</b>"; break;
                case $TOUR_STATUS['rank']:      print " <b>(turniej&nbsp;rankingowy)</b>"; break;
                case $TOUR_STATUS['vacation']:  print " <b>(wczasy&nbsp;scrabblowe)</b>"; break;
                default: break;
            }
            print "</tr>";
        }
    }
    print "</table>";
}
?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Kalendarz</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu ("turnieje","kalendarz");
        $(function() {
            $( "#tabs" ).tabs ().show ();
        });

    $(function () {
        $('area').
            each (function (i, o) {
                id = $(o).attr ('name');
                $(o).attr ('href', '../turniej.php?id=' + id);
            }).
            mouseenter (infoTurnieju).
            mouseleave (wyczyscInfoTurnieju);
    });

    var turnieje = new Array();
    <?
        $tours = pfs_select (array (
            table   => $DB_TABLES[tours],
            fields  => array ( 'nazwa', 'data_od', 'data_do', 'miasto', 'id' ),
            where   => array ( '>=YEAR(`data_od`)' => 2008 ),
            order   => array ( 'data_od' )
        ));

        foreach ($tours as $tour) {
            print "turnieje[$tour->id] = '<i>$tour->nazwa</i><br><b>". wyswietlDate ($tour->data_od, $tour->data_do, false) .", $tour->miasto</b>';\n";
        }
   ?>
    </script>
    <style type="text/css">
        #bottompanel{
            padding-top: 10px;
            clear: both;
            width: 100%;
            font-size: inherit;
        }
        #bottompanel td{
            text-align: left;
            padding: 6px 0 0 10px;
            vertical-align:top;
        }
        #bottompanel td:first-child{
            font-weight: bold;
            width: 140px;
        }
        #tourInfo {
            position: absolute;
            -moz-border-radius: 8px;
            border-radius: 8px;
            background: #EDF0F4;
            opacity: .88;
            padding: 12px 20px;
            max-width: 250px;
            display: none;
            border: 1px solid #7CA0AF;
            text-align: left;
        }
        #tourInfo img {
            vertical-align: middle;
            margin-right: 3px;
        }
  </style>
</head>

<body>
<? require_once "files/php/menu.php"; ?>
<h1><script>naglowek ("Kalendarz turniejowy" )</script></h1>

<p align="right">
<a href="http://www.pfs.org.pl/zgloszenie_turnieju.php">Zgłoszenie turnieju</a></p><br>

<div id="tabs" style="display:none;">
<?
print '<ul>';
for ($y = 2014; $y >= 1993; $y--) {
    print "<li><a href='#tabs-$y'>$y</a></li>";
}
print '</ul>';
?>
<div id="tourInfo"></div>

<div id='tabs-2014'>

<img src="files/mapa2014.png" usemap="#mapa2014" alt="kalendarz" />
<map id="mapa2014" name="mapa2014">
    <area shape="circle" coords="376,182, 5" name='779'/> <!-- Biała Podlaska-->
    <area shape="circle" coords="372,114, 5" name='794'/> <!-- Białystok-->
    <area shape="circle" coords=" 169,136, 5" name='763'/> <!-- Inowrocław-->
    <area shape="circle" coords=" 210,300, 5" name='781'/> <!-- Jaworzno-->
    <area shape="circle" coords=" 211,95, 5" name='777'/> <!-- Konojady-->
    <area shape="circle" coords=" 243,314, 5" name='770'/> <!-- Kraków-->
    <area shape="circle" coords=" 214,168, 5" name='768'/> <!-- Kutno-->
    <area shape="circle" coords="80,239, 5" name='791'/> <!-- Legnica-->
    <area shape="circle" coords="221,203, 5" name='793'/> <!-- Łódź-->
    <area shape="circle" coords="257,166, 5" name='767'/> <!-- Milanówek-->
    <area shape="circle" coords="226,86, 5" name='771'/> <!-- Nowe Miasto Lubawskie-->
    <area shape="circle" coords="304,255, 5" name='784'/> <!-- Ostrowiec Świętokrzyski-->
    <area shape="circle" coords="253,71, 5" name='772'/> <!-- Ostróda-->
    <area shape="circle" coords="295,76, 5" name='783'/> <!-- Piastuno-->
    <area shape="circle" coords="98, 278, 5" name='759'/> <!-- Polanica Zdrój-->
    <area shape="circle" coords="173,311, 5" name='792'/> <!-- Racibórz-->
    <area shape="circle" coords="180, 16, 5" name='778'/> <!-- Rumia-->
    <area shape="circle" coords=" 123,25, 5" name='766'/> <!-- Słupsk-->
    <area shape="circle" coords="8,60, 5" name='786'/> <!-- Świnoujście-->
    <area shape="circle" coords=" 87,101, 5" name='773'/> <!-- Wałcz MPN -->
    <area shape="circle" coords=" 95,101, 5" name='774'/> <!-- Wałcz-->
    <area shape="circle" coords="288,162, 5" name='761'/> <!-- Warszawa Alfred-->
    <area shape="circle" coords="297,162, 5" name='762'/> <!-- Warszawa MW-->
    <area shape="circle" coords="306,162, 5" name='785'/> <!-- Warszawa Lotnicy-->
    <area shape="circle" coords="169,3, 5" name='789'/> <!-- Władysławowo-->
    <area shape="circle" coords="176,3, 5" name='790'/> <!-- Władysławowo Wczasy-->
    <area shape="circle" coords="104,295, 5" name='764'/> <!-- Wójtowice-->
    <area shape="circle" coords="116,248, 5" name='760'/> <!-- Wrocław-->
    <area shape="circle" coords="349,123, 5" name='769'/> <!-- Wysokie Mazowieckie-->

</map>

<? show_calendar_tours (2014);?>
</div>

<div id='tabs-2013'>

<img src="files/mapa2013.png" usemap="#mapa2013" alt="kalendarz" />
<map id="mapa2013" name="mapa2013">
    <area shape="circle" coords="296,174, 5" name='755'/> <!-- Baniocha-->
    <area shape="circle" coords="73,258, 5" name='738'/> <!-- Betlejem Wczasy-->
    <area shape="circle" coords="64,258, 5" name='752'/> <!-- Betlejem Turniej -->
    <area shape="circle" coords="376,182, 5" name='704'/> <!-- Biała Podlaska-->
    <area shape="circle" coords=" 171, 80, 5" name='740'/> <!-- Bory Tucholskie-->
    <area shape="circle" coords=" 337,65, 5" name='716'/> <!-- Ełk-->
    <area shape="circle" coords="221,318, 5" name='725'/> <!-- Graboszyce-->
    <area shape="circle" coords=" 289,256, 5" name='691'/> <!-- Huta Szklana-->
    <area shape="circle" coords=" 281,256, 5" name='712'/> <!-- Huta Szklana-->
    <area shape="circle" coords=" 169,136, 5" name='709'/> <!-- Inowrocław-->
    <area shape="circle" coords=" 210,300, 5" name='710'/> <!-- Jaworzno-->
    <area shape="circle" coords="198,284, 5" name='754'/> <!-- Katowice-->
    <area shape="circle" coords=" 211,95, 5" name='693'/> <!-- Konojady-->
    <area shape="circle" coords=" 243,314, 5" name='719'/> <!-- Kraków-->
    <area shape="circle" coords=" 252,314, 5" name='751'/> <!-- Kraków MP ENG-->
    <area shape="circle" coords=" 214,168, 5" name='713'/> <!-- Kutno-->
    <area shape="circle" coords=" 80,239, 5" name='731'/> <!-- Legnica PP-->
    <area shape="circle" coords="221,203, 5" name='739'/> <!-- Łódź -->
    <area shape="circle" coords=" 273,273, 5" name='748'/> <!-- Mąchocice-->
    <area shape="circle" coords="257,166, 5" name='702'/> <!-- Milanówek-->
    <area shape="circle" coords="226,86, 5" name='714'/> <!-- Nowe Miasto Lubawskie-->
    <area shape="circle" coords="257,134, 5" name='749'/> <!-- Nowy Dwór Mazowiecki-->
    <area shape="circle" coords="253,71, 5" name='723'/> <!-- Ostróda-->
    <area shape="circle" coords="156,206, 5" name='705'/> <!-- Ostrów Wielkopolski-->
    <area shape="circle" coords=" 272,168, 5" name='732'/> <!-- Piastów-->
    <area shape="circle" coords="295, 76, 5" name='724'/> <!-- Piasutno-->
    <area shape="circle" coords="98, 278, 5" name='727'/> <!-- Polanica Zdrój-->
    <area shape="circle" coords="39,290, 5" name='765'/> <!-- Praga-->
    <area shape="circle" coords="173,311, 5" name='720'/> <!-- Racibórz-->
    <area shape="circle" coords="180, 16, 5" name='703'/> <!-- Rumia-->
    <area shape="circle" coords="170, 16, 5" name='753'/> <!-- Rumia MDK-->
    <area shape="circle" coords=" 123,25, 5" name='692'/> <!-- Słupsk-->
    <area shape="circle" coords=" 20, 92, 5" name='726'/> <!-- Szczecin-->
    <area shape="circle" coords=" 219, 249, 5" name='728'/> <!-- Szczepocice KMP-->
    <area shape="circle" coords=" 8,60, 5" name='715'/> <!-- Świnoujście-->
    <area shape="circle" coords="223,303, 5" name='756'/> <!-- Trzebinia-->
    <area shape="circle" coords=" 87,101, 5" name='733'/> <!-- Wałcz MPN -->
    <area shape="circle" coords=" 95,101, 5" name='734'/> <!-- Wałcz-->
    <area shape="circle" coords="288,162, 5" name='708'/> <!-- Warszawa Gimnazjum 12-->
    <area shape="circle" coords="305,162, 5" name='717'/> <!-- Warszawa Alfred-->
    <area shape="circle" coords="297,162, 5" name='741'/> <!-- Warszawa Ochota-->
    <area shape="circle" coords="314,162, 5" name='744'/> <!-- Warszawa Polonistyka-->
    <area shape="circle" coords="323,162, 5" name='742'/> <!-- Warszawa MP ESP -->
    <area shape="circle" coords="332,162, 5" name='757'/> <!-- Warszawa Mikołajki-->
    <area shape="circle" coords="273,142, 5" name='729'/> <!-- Warszawa-Babice MP I faza -->
    <area shape="circle" coords="282,142, 5" name='730'/> <!-- Warszawa-Babice MP II faza -->
    <area shape="circle" coords="291,142, 5" name='758'/> <!-- Warszawa-Babice MP TT -->
    <area shape="circle" coords="104,295, 5" name='699'/> <!-- Wójtowice-->
    <area shape="circle" coords="116,248, 5" name='718'/> <!-- Wrocław-->
    <area shape="circle" coords="125,248, 5" name='747'/> <!-- Wrocław Majówka-Scrabblówka-->
    <area shape="circle" coords="132,248, 5" name='736'/> <!-- Wrocław MarcoweHarce-->

</map>

<? show_calendar_tours (2013);?>
</div>

<div id='tabs-2012'>

<img src="files/mapa2012.png" usemap="#mapa2012" alt="kalendarz" />

<map id="mapa2012" name="mapa2012">
    <area shape="circle" coords="257,166, 5" name='609'/> <!-- Milanówek-->

    <!--<area shape="circle" coords=" 53,162, 5" name='572'/> <!-- Sulęcin LeMans-->
    <!--<area shape="circle" coords=" 43,162, 5" name='000'/> <!-- Sulęcin-->
    <area shape="circle" coords="244,359, 5" name='624'/> <!-- Kościelisko wczasy-->
    <area shape="circle" coords="253,359, 5" name='625'/> <!-- Kościelisko-->
    <area shape="circle" coords="249,368, 5" name='683'/> <!-- Zakopane - igrzyska lekarskie-->

    <area shape="circle" coords="290,162, 5" name='637'/> <!-- Warszawa Gimnazjum Lotników-->
    <area shape="circle" coords="302,162, 5" name='671'/> <!-- Warszawa Zespół Szkół 59 -->
    <area shape="circle" coords="312,162, 5" name='672'/> <!-- Warszawa MP ESP -->
    <area shape="circle" coords="323,162, 5" name='674'/> <!-- Warszawa KMMazowsza -->
    <area shape="circle" coords="333,162, 5" name='680'/> <!-- Warszawa IwszystkoGRA -->
    <area shape="circle" coords="283,171, 5" name='688'/> <!-- Warszawa-Babice Le Mans -->
    <area shape="circle" coords="277,165, 5" name='644'/> <!-- Warszawa-Babice MP I faza -->
    <area shape="circle" coords="342,162, 5" name='689'/> <!-- Warszawa Mikołajki-->

    <!--<area shape="circle" coords="233, 74, 5" name='553'/> <!-- Ostróda KMP-->
    <area shape="circle" coords="244, 74, 5" name='633'/> <!-- Ostróda-->
    <area shape="circle" coords="335, 44, 5" name='669'/> <!-- Olecko-->

    <area shape="circle" coords="167,  4, 5" name='641'/> <!-- Władysławowo turniej-->
    <area shape="circle" coords="177,  4, 5" name='640'/> <!-- Władysławowo wczasy-->

    <area shape="circle" coords=" 70,103, 5" name='611'/> <!-- KMP -->
    <area shape="circle" coords=" 78,102, 5" name='643'/> <!-- Wałcz Turniej Drużyn -->
    <area shape="circle" coords=" 88,102, 5" name='622'/> <!-- Wałcz MPN -->
    <area shape="circle" coords=" 98,102, 5" name='623'/> <!-- Wałcz-->
    <area shape="circle" coords="108,102, 5" name='645'/> <!-- Wałcz MP finał-->
    <area shape="circle" coords="118,102, 5" name='646'/> <!-- Wałcz MP turniej towarzyszący-->

    <area shape="circle" coords=" 22, 93, 5" name='631'/> <!-- Szczecin-->
    <area shape="circle" coords=" 215, 254, 5" name='662'/> <!-- Szczepocice-->
    <area shape="circle" coords=" 206, 254, 5" name='661'/> <!-- Szczepocice Masters-->
    <area shape="circle" coords=" 197, 254, 5" name='694'/> <!-- Szczepocice Mikołaj-->
    <!--<area shape="circle" coords=" 33, 94, 5" name='586'/> <!-- Szczecin MPS Ponadgimnazjalnych-->

    <!--<area shape="circle" coords=" 89, 20, 5" name='603'/> <!-- Wicie Liga Morska-->
    <!--<area shape="circle" coords=" 99, 20, 5" name='598'/> <!-- Wicie turniej-->
    <!--<area shape="circle" coords=" 94, 11, 5" name='575'/> <!-- Wicie wczasy-->

    <area shape="circle" coords="297, 76, 5" name='584'/> <!-- Krzyże wczasy-->
    <area shape="circle" coords="307, 76, 5" name='595'/> <!-- Krzyże maraton-->

    <area shape="circle" coords="109,295, 5" name='613'/> <!-- Wójtowice-->
    <area shape="circle" coords="223, 88, 5" name='659'/> <!-- NML-->
    <area shape="circle" coords="172, 19, 5" name='628'/> <!-- Rumia-->
    <area shape="circle" coords="334, 59, 5" name='617'/> <!-- Ełk-->
    <area shape="circle" coords="248,313, 5" name='630'/> <!-- Kraków-->
    <area shape="circle" coords="123, 20, 5" name='612'/> <!-- Słupsk-->
    <area shape="circle" coords="178,313, 5" name='636'/> <!-- Racibórz-->
    <area shape="circle" coords="222,302, 5" name='634'/> <!-- Jaworzno-->
    <!--<area shape="circle" coords="213,  4, 5" name='567'/> <!-- Prom Stena-Lina-->
    <area shape="circle" coords="169,135, 5" name='616'/> <!-- Inowrocław-->
    <!--<area shape="circle" coords="324,221, 5" name='569'/> <!-- Puławy-->
    <area shape="circle" coords="128,248, 5" name='632'/> <!-- Wrocław-->
    <area shape="circle" coords="217,171, 5" name='658'/> <!-- Kutno-->
    <!--<area shape="circle" coords="161,113, 5" name='574'/> <!-- Bydgoszcz-->
    <area shape="circle" coords=" 81,237, 5" name='642'/> <!-- Legnica PP-->
    <!--<area shape="circle" coords="219, 90, 5" name='588'/> <!-- NML-->
    <area shape="circle" coords="228,319, 5" name='653'/> <!-- Graboszyce-->
    <area shape="circle" coords="225,203, 5" name='656'/> <!-- Łódź -->
    <area shape="circle" coords="265,171, 5" name='655'/> <!-- Piastów-->
    <!--<area shape="circle" coords="159,157, 5" name='596'/> <!-- Skorzecin-->
    <area shape="circle" coords="369,104, 5" name='635'/> <!-- Białystok-->

    <!--<area shape="circle" coords=" 98,102, 5" name='526'/> <!-- IMP-->
    <area shape="circle" coords="324,103, 5" name='629'/> <!-- Łomża-->
    <!--<area shape="circle" coords="199,291, 5" name='520'/> <!-- Zabrze-->
    <!--<area shape="circle" coords="189,291, 5" name='521'/> <!-- Zabrze MPS-->
    <!--<area shape="circle" coords="150, 50, 5" name='540'/> <!-- Sudomie wczasy-->
    <!--<area shape="circle" coords="160, 50, 5" name='541'/> <!-- Sudomie turniej-->
    <area shape="circle" coords="257,144, 5" name='677'/> <!-- NDM-->
    <!--<area shape="circle" coords="203,315, 5" name='545'/> <!-- Pszczyna-->

    <area shape="circle" coords="208, 88, 5" name='614'/> <!-- Konojady-->
    <area shape="circle" coords="182, 26, 5" name='647'/> <!-- Gdańsk-->
    <area shape="circle" coords="223,249, 5" name='651'/> <!-- Radomsko-->
</map>
<? show_calendar_tours (2012);?>
</div>

<div id='tabs-2011'>
<img src="files/mapa2011.png" usemap="#mapa2011" alt="kalendarz" />
<map id="mapa2011" name="mapa2011">
    <area shape="circle" coords=" 43,162, 5" name='550'/> <!-- Sulęcin-->
    <area shape="circle" coords=" 53,162, 5" name='572'/> <!-- Sulęcin LeMans-->

    <area shape="circle" coords="244,359, 5" name='563'/> <!-- Kościelisko wczasy-->
    <area shape="circle" coords="253,359, 5" name='564'/> <!-- Kościelisko-->
    <area shape="circle" coords="263,359, 5" name='600'/> <!-- Zakopane - igrzyska lekarskie-->

    <area shape="circle" coords="290,162, 5" name='585'/> <!-- Warszawa Gimnazjum Lotników-->
    <area shape="circle" coords="300,162, 5" name='554'/> <!-- Warszawa Mistrzostwa-->
    <area shape="circle" coords="310,162, 5" name='593'/> <!-- Warszawa MPA -->
    <area shape="circle" coords="320,162, 5" name='605'/> <!-- Warszawa I wszystko gra -->
    <area shape="circle" coords="330,162, 5" name='601'/> <!-- Warszawa turniej parkowy -->
    <area shape="circle" coords="340,162, 5" name='587'/> <!-- Warszawa WSC -->
    <area shape="circle" coords="350,162, 5" name='592'/> <!-- Warszawa MP II faza -->
    <area shape="circle" coords="360,162, 5" name='565'/> <!-- Warszawa Mikołajki-->

    <area shape="circle" coords="233, 74, 5" name='553'/> <!-- Ostróda KMP-->
    <area shape="circle" coords="244, 74, 5" name='548'/> <!-- Ostróda-->

    <area shape="circle" coords="167,  4, 5" name='561'/> <!-- Władysławowo turniej-->
    <area shape="circle" coords="177,  4, 5" name='560'/> <!-- Władysławowo wczasy-->

    <area shape="circle" coords=" 78,102, 5" name='590'/> <!-- Wałcz MP Radców -->
    <area shape="circle" coords=" 88,102, 5" name='602'/> <!-- Wałcz MPN -->
    <area shape="circle" coords=" 98,102, 5" name='604'/> <!-- Wałcz Primus Inter Pares-->
    <area shape="circle" coords="108,102, 5" name='566'/> <!-- Wałcz-->
    <area shape="circle" coords="118,102, 5" name='557'/> <!-- Wałcz MP I faza-->

    <area shape="circle" coords=" 22, 93, 5" name='570'/> <!-- Szczecin-->
    <area shape="circle" coords=" 33, 94, 5" name='586'/> <!-- Szczecin MPS Ponadgimnazjalnych-->

    <area shape="circle" coords=" 89, 20, 5" name='603'/> <!-- Wicie Liga Morska-->
    <area shape="circle" coords=" 99, 20, 5" name='598'/> <!-- Wicie turniej-->
    <area shape="circle" coords=" 94, 11, 5" name='575'/> <!-- Wicie wczasy-->

    <area shape="circle" coords="297, 76, 5" name='584'/> <!-- Krzyże wczasy-->
    <area shape="circle" coords="307, 76, 5" name='595'/> <!-- Krzyże maraton-->

    <area shape="circle" coords="109,295, 5" name='556'/> <!-- Wójtowice-->
    <area shape="circle" coords="172, 19, 5" name='628'/> <!-- Rumia-->
    <area shape="circle" coords="257,166, 5" name='552'/> <!-- Milanówek-->
    <area shape="circle" coords="334, 59, 5" name='559'/> <!-- Ełk-->
    <area shape="circle" coords="248,313, 5" name='555'/> <!-- Kraków-->
    <area shape="circle" coords="123, 20, 5" name='546'/> <!-- Słupsk-->
    <area shape="circle" coords="178,313, 5" name='549'/> <!-- Racibórz-->
    <area shape="circle" coords="222,302, 5" name='562'/> <!-- Jaworzno-->
    <area shape="circle" coords="213,  4, 5" name='567'/> <!-- Prom Stena-Lina-->
    <area shape="circle" coords="169,135, 5" name='568'/> <!-- Inowrocław-->
    <area shape="circle" coords="324,221, 5" name='569'/> <!-- Puławy-->
    <area shape="circle" coords="128,248, 5" name='571'/> <!-- Wrocław-->
    <area shape="circle" coords="217,171, 5" name='573'/> <!-- Kutno-->
    <area shape="circle" coords="161,113, 5" name='574'/> <!-- Bydgoszcz-->
    <area shape="circle" coords=" 81,237, 5" name='579'/> <!-- Legnica PP-->
    <area shape="circle" coords="219, 90, 5" name='588'/> <!-- NML-->
    <area shape="circle" coords="228,319, 5" name='589'/> <!-- Graboszyce-->
    <area shape="circle" coords="225,203, 5" name='591'/> <!-- Łódź -->
    <area shape="circle" coords="265,171, 5" name='594'/> <!-- Piastów-->
    <area shape="circle" coords="159,157, 5" name='596'/> <!-- Skorzecin-->
    <area shape="circle" coords="369,104, 5" name='599'/> <!-- Białystok-->
</map>
<? show_calendar_tours (2011);?>
</div>

<div id='tabs-2010'>
<img src="files/mapa2010.png" usemap="#mapa2010" alt="kalendarz" />
<map id="mapa2010" name="mapa2010">
    <area shape="circle" coords="109,295, 5" name='492'/> <!-- Wójtowice-->
    <area shape="circle" coords="123, 20, 5" name='498'/> <!-- Słupsk-->
    <area shape="circle" coords=" 43,162, 5" name='501'/> <!-- Sulęcin-->
    <area shape="circle" coords="324,221, 5" name='502'/> <!-- Puławy-->
    <area shape="circle" coords="257,166, 5" name='503'/> <!-- Milanówek-->
    <area shape="circle" coords="300,162, 5" name='505'/> <!-- Mistrzostwa Warszawy-->
    <area shape="circle" coords=" 78,102, 5" name='506'/> <!-- MPN-->
    <area shape="circle" coords=" 88,102, 5" name='507'/> <!-- Wałcz-->
    <area shape="circle" coords=" 98,102, 5" name='526'/> <!-- IMP-->
    <area shape="circle" coords="108,102, 5" name='528'/> <!-- IMP towarzyszący-->
    <area shape="circle" coords="172, 19, 5" name='508'/> <!-- Rumia-->
    <area shape="circle" coords="238, 74, 5" name='509'/> <!-- Ostróda-->
    <area shape="circle" coords="244,359, 5" name='511'/> <!-- Kościelisko wczasy-->
    <area shape="circle" coords="253,359, 5" name='510'/> <!-- Kościelisko-->
    <area shape="circle" coords=" 22, 93, 5" name='513'/> <!-- Szczecin-->
    <area shape="circle" coords="310,162, 5" name='504'/> <!-- Mikolajki (Warszawa)-->
    <area shape="circle" coords="167,  4, 5" name='514'/> <!-- Władek-->
    <area shape="circle" coords="177,  4, 5" name='515'/> <!-- Władek wczasy-->
    <area shape="circle" coords="169,135, 5" name='516'/> <!-- Inowrocław-->
    <area shape="circle" coords="222,302, 5" name='517'/> <!-- Jaworzno-->
    <area shape="circle" coords="324,103, 5" name='519'/> <!-- Łomża-->
    <area shape="circle" coords="199,291, 5" name='520'/> <!-- Zabrze-->
    <area shape="circle" coords="189,291, 5" name='521'/> <!-- Zabrze MPS-->
    <area shape="circle" coords="248,313, 5" name='522'/> <!-- Kraków-->
    <area shape="circle" coords="161,113, 5" name='523'/> <!-- Bydgoszcz-->
    <area shape="circle" coords="128,248, 5" name='518'/> <!-- Wrocław-->
    <area shape="circle" coords=" 81,237, 5" name='525'/> <!-- Puchar Polski-->
    <area shape="circle" coords="217,171, 5" name='530'/> <!-- Kutno-->
    <area shape="circle" coords="228,319, 5" name='531'/> <!-- Graboszyce-->
    <area shape="circle" coords="225,203, 5" name='532'/> <!-- Łódź PFS13-->
    <area shape="circle" coords="265,171, 5" name='534'/> <!-- Piastów-->
    <area shape="circle" coords="178,313, 5" name='535'/> <!-- Racibórz-->
    <area shape="circle" coords="235,203, 5" name='536'/> <!-- Łódź-->
    <area shape="circle" coords="190,220, 5" name='537'/> <!-- KMP Burzenin-->
    <area shape="circle" coords="334, 59, 5" name='538'/> <!-- Ełk-->
    <area shape="circle" coords="116,248, 5" name='539'/> <!-- Wrocław EMPIK-->
    <area shape="circle" coords="150, 50, 5" name='540'/> <!-- Sudomie wczasy-->
    <area shape="circle" coords="160, 50, 5" name='541'/> <!-- Sudomie turniej-->
    <area shape="circle" coords="266,152, 5" name='543'/> <!-- NDM-->
    <area shape="circle" coords="219, 90, 5" name='544'/> <!-- NML-->
    <area shape="circle" coords="203,315, 5" name='545'/> <!-- Pszczyna-->
</map>
<? show_calendar_tours (2010);?>
</div>

<div id='tabs-2009'>
<img src="files/mapa2009.png" usemap="#mapa2009" alt="kalendarz" />
<map id="mapa2009" name="mapa2009">
    <area shape="circle" coords="123, 20, 5" name='437'/> <!-- Słupsk-->
    <area shape="circle" coords=" 88,102, 5" name='438'/> <!-- MPN-->
    <area shape="circle" coords=" 98,102, 5" name='450'/> <!-- Wałcz-->
    <area shape="circle" coords="109,295, 5" name='441'/> <!-- Wójtowice-->
    <area shape="circle" coords="320,162, 5" name='442'/> <!-- Mikolajki (Warszawa)-->
    <area shape="circle" coords="172, 19, 5" name='443'/> <!-- Rumia-->
    <area shape="circle" coords="324,221, 5" name='444'/> <!-- Puławy-->
    <area shape="circle" coords="238, 74, 5" name='446'/> <!-- Ostróda-->
    <area shape="circle" coords=" 43,162, 5" name='447'/> <!-- Sulęcin-->
    <area shape="circle" coords="244,359, 5" name='451'/> <!-- Kościelisko wczasy-->
    <area shape="circle" coords="253,359, 5" name='452'/> <!-- Kościelisko-->
    <area shape="circle" coords="189,291, 5" name='453'/> <!-- Zabrze MPS-->
    <area shape="circle" coords="199,291, 5" name='454'/> <!-- Zabrze-->
    <area shape="circle" coords="167,  4, 5" name='455'/> <!-- Władek-->
    <area shape="circle" coords="177,  4, 5" name='456'/> <!-- Władek wczasy-->
    <area shape="circle" coords="212,199, 5" name='457'/> <!-- Łódka Zima-->
    <area shape="circle" coords="222,199, 5" name='458'/> <!-- Łódka Wiosna-->
    <area shape="circle" coords="212,209, 5" name='459'/> <!-- Łódka Lato-->
    <area shape="circle" coords="222,209, 5" name='460'/> <!-- Łódka Jesień-->
    <area shape="circle" coords="280,162, 5" name='463'/> <!-- LeMans-->
    <area shape="circle" coords="310,162, 5" name='462'/> <!-- Mistrzostwa Polski-->
    <area shape="circle" coords="135,187, 5" name='461'/> <!-- Puchar Polski -->
    <area shape="circle" coords=" 22, 93, 5" name='464'/> <!-- Szczecin-->
    <area shape="circle" coords="300,162, 5" name='465'/> <!-- Mistrzostwa Warszawy-->
    <area shape="circle" coords="120,248, 5" name='466'/> <!-- Wrocław-->
    <area shape="circle" coords="257,166, 5" name='467'/> <!-- Milanówek-->
    <area shape="circle" coords="121,165, 5" name='468'/> <!-- Koziołki-->
    <area shape="circle" coords="161,113, 5" name='469'/> <!-- Bydgoszcz-->
    <area shape="circle" coords="248,313, 5" name='470'/> <!-- Kraków-->
    <area shape="circle" coords="169,135, 5" name='471'/> <!-- Inowrocław-->
    <area shape="circle" coords="228,319, 5" name='472'/> <!-- Graboszyce-->
    <area shape="circle" coords="111, 15, 5" name='473'/> <!-- Ustka-->
    <area shape="circle" coords="217,171, 5" name='474'/> <!-- Kutno-->
    <area shape="circle" coords="202,307, 5" name='475'/> <!-- Hanysy-->
    <area shape="circle" coords="190,220, 5" name='478'/> <!-- KMP-->
    <area shape="circle" coords="267,172, 5" name='479'/> <!-- Wrocław-->
    <area shape="circle" coords="150, 50, 5" name='481'/> <!-- Sudomie wczasy-->
    <area shape="circle" coords="160, 50, 5" name='482'/> <!-- Sudomie turniej-->
    <area shape="circle" coords="290,162, 5" name='483'/> <!-- angielskie-->
    <area shape="circle" coords="286,311, 5" name='484'/> <!-- spartakiada-->
    <area shape="circle" coords="266,152, 5" name='488'/> <!-- NDM-->
    <area shape="circle" coords="219, 90, 5" name='490'/> <!-- NML-->
    <area shape="circle" coords="261,359, 5" name='493'/> <!-- Zakopane (ig.lek.)-->
    <area shape="circle" coords=" 81,237, 5" name='496'/> <!-- Legnica-->
    <area shape="circle" coords="324,103, 5" name='497'/> <!-- Łomża-->
    <area shape="circle" coords="222,302, 5" name='494'/> <!-- Jaworzno-->
</map>
<? show_calendar_tours (2009);?>
</div>

<div id='tabs-2008'>
<img src="files/mapa2008.png" usemap="#mapa2008" alt="kalendarz" />
<map id="mapa2008" name="mapa2008">
    <area shape="circle" coords="120,  20, 5" name='29'/>
    <area shape="circle" coords="107, 101, 5" name='31'/>
    <area shape="circle" coords="118, 101, 5" name='30'/>
    <area shape="circle" coords="218, 349, 5" name='32'/>
    <area shape="circle" coords=" 23,  92, 5" name='10'/>
    <area shape="circle" coords="176,  25, 5" name='16'/>
    <area shape="circle" coords="211, 307, 5" name='15'/>
    <area shape="circle" coords="222, 307, 5" name='12'/>
    <area shape="circle" coords="233, 330, 5" name='11'/>
    <area shape="circle" coords="308, 153, 5" name='23'/>
    <area shape="circle" coords="318, 153, 5" name='39'/>
    <area shape="circle" coords="278, 188, 5" name='4'/>
    <area shape="circle" coords="175, 136, 5" name='13'/>
    <area shape="circle" coords="168,   9, 5" name='37'/>
    <area shape="circle" coords="139, 245, 5" name='5'/>
    <area shape="circle" coords="195, 287, 5" name='22'/>
    <area shape="circle" coords="236,  70, 5" name='26'/>
    <area shape="circle" coords="247, 319, 5" name='27'/>
    <area shape="circle" coords="296, 153, 5" name='14'/>
    <area shape="circle" coords="255, 358, 5" name='6'/>
    <area shape="circle" coords="245, 358, 5" name='7'/>
    <area shape="circle" coords="126, 245, 5" name='9'/>
    <area shape="circle" coords="228, 205, 5" name='8'/>
    <area shape="circle" coords="238, 205, 5" name='17'/>
    <area shape="circle" coords="228, 215, 5" name='28'/>
    <area shape="circle" coords="238, 215, 5" name='2'/>
    <area shape="circle" coords="277, 142, 5" name='3'/>
    <area shape="circle" coords="217, 180, 5" name='18'/>
    <area shape="circle" coords="223,  90, 5" name='1'/>
    <area shape="circle" coords="206, 287, 5" name='21'/>
    <area shape="circle" coords="166, 112, 5" name='34'/>
    <area shape="circle" coords=" 48, 149, 5" name='38'/>
    <area shape="circle" coords="194, 246, 5" name='20'/>
    <area shape="circle" coords="290, 171, 5" name='19'/>
    <area shape="circle" coords="178,   9, 5" name='35'/>
    <area shape="circle" coords="326, 153, 5" name='445'/>
    <area shape="circle" coords="153,  48, 5" name='25'/>
    <area shape="circle" coords="335, 153, 5" name='24'/>
    <area shape="circle" coords="209, 349, 5" name='33'/>
    <area shape="circle" coords="265, 358, 5" name='36'/>
</map>
<? show_calendar_tours (2008);?>
</div>

<?
for ($year = 2007; $year >= 1993; $year--) {
    print "<div id='tabs-$year'>";
    show_calendar_tours ($year);
    print "</div>";
}
?>

</div>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
