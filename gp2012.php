﻿<?
require_once "files/php/funkcje.php";

$gp_tours = pfs_select (array (
    table   => $DB_TABLES[tours],
    where   => array ( 'YEAR(data_od)' => '2012', 'rank' => $TOUR_STATUS[gp]),
    order   => array ( 'data_od' )
));

$gp_results = pfs_select (array (
    table   => $DB_TABLES[gp2012],
    order   => array ( '!suma' )
));
?>
<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Grand Prix 2012</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("turnieje","gp2012")</script>
    <style type="text/css">
        table.linki{margin: 20px auto 0 auto;}
        table.linki td{ padding: 8px;vertical-align: top;}
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Grand Prix 2012")</script></h1>

<h2>Turnieje Grand Prix 2012</h2>
Cykl Grand Prix 2012 obejmuje 11 turniejów:
<table class="linki ramkadolna">
<?php
$cnt = 1;
foreach ($gp_tours as $tour) {
    print "<tr><td>" . ($cnt++) . ".</td><td><a href='../turniej.php?id=$tour->id'>$tour->nazwa</a></td></tr>";
}
?>
</table>
<br><br>

<h2>Zasady punktacji</h2>
<ol>
    <li>Punkty do klasyfikacji łącznej Grand Prix zdobywa tylu uczestników ile wynosi część całkowita liczby obliczonej według wzoru (liczba uczestników którzy rozegrali min. 50% gier w turnieju / 3).<br>
    Zawodnik który zajmie ostatnie punktowane miejsce otrzymuje 1 punkt, każdy kolejny o jeden punkt więcej.<br>
    Dodatkowo zdobywcy 3 czołowych miejsc otrzymują premię: 5 punktów za miejsce pierwsze, 3 punkty za drugie i 1 punkt za trzecie.<br>
    (przykład 1: przy 62 uczestnikach: 20 miejsce - 1 punkt, 19 - 2, 18 - 3, (...) 4 - 17, 3 - 18+1, 2 - 19+3, 1 - 20+5)<br>
    (przykład 2: przy 85 uczestnikach: 28 miejsce - 1 punkt, 27 - 2, 26 - 3, (...) 4 - 25, 3 - 26+1, 2 - 27+3, 1 - 28+5)<br>
    <li>Minimalna liczba uczestników zdobywająca punkty do klasyfikacji Grand Prix wynosi 20 (tzn. w przypadku turnieju w którym mniej niż 60 osób rozegrało 50% gier, zawodnik za miejsce 20 otrzymuje 1 punkt, za 19 - 2, (...), za 4 - 17, za 3 - 18+1, za 2 -19+3. za 1 - 20+5)
	<li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Grand Prix 2012 do końcowej klasyfikacji zalicza się osiem najlepszych wyników.
	<li>Przy równej liczbie punktów w klasyfikacji końcowej o kolejności decyduje wyższa zdobycz punktowa w pojedynczym turnieju, a jeśli to nie da efektu (gracze mieli identyczny wynik) w kolejnym najlepszym turnieju, itd.<br>
    Jeżeli w ten sposób nie uda się ustalić kolejności, decyduje losowanie.

</ol>

<h2>Nagrody</h2>
Organizatorzy każdego turnieju rangi Grand Prix wpłacają do puli nagród Grand Prix po 200 zł.<br />
Pula na nagrody w cyklu wynosi <b>2200 zł</b>.<br />
Nagrody w Grand Prix 2012 wynoszą:
<ul>
    <li>za I miejsce 1000 zł</li>
    <li>za II miejsce 600 zł</li>
    <li>za III miejsce 300 zł</li>
    <li>za IV miejsce 200 zł</li>
    <li>za V miejsce 100 zł</li>
</ul>

<h2>Klasyfikacja cyklu Grand Prix 2012</h2>
<table class="klasyfikacja">
    <tr>
        <td></td>
        <td>Imię i nazwisko</td>
        <td class='suma'>SUMA</td>
<?
$cnt = 0;
foreach ($gp_tours as $tour) {
    $style = ($cnt++ % 2 ? " class='lighter'" : '');
    print "<td$style>$tour->miasto</td>";
}
?>
    </tr>
<?
$i          = 1;
$prev_suma  = 0;

foreach ($gp_results as $person) {
    print "<tr>";
    print "<td class='lp'>" . ($prev_suma == $person->suma ? '&nbsp;' : $i) ."</td>";
    $i++;
    $prev_suma = $person->suma;
    print "<td class='osoba'>$person->osoba</td>";
    print "<td class='suma'>$person->suma</td>";

    foreach ($gp_tours as $tour) {
        $miasto = strtolower (no_pl_chars ($tour->miasto));
        $style = ($cnt++ % 2 ? " class='lighter'" : '');
        print "<td$style>".$person->$miasto."</td>";
    }
    print "</tr>";
}
?>
</table>

<?include "files/php/bottom.php"?>
</body>
</html>
