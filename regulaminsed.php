<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Przepisy : Regulamin sędziowski</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("turnieje","regulaminsedziowski");</script>
  <style type="text/css">
  	p.rozdzial{
		text-align:center;
		font-size:12px;
		font-weight:bold;
		margin-top:30px;
	}
	p.paragraf{
		text-align: center;
		font-size: 12px;
		margin-top: 16px;
		margin-bottom: 6px;
	}
	div.wzor{
		margin: 12px auto;
		height: 60px;
		width: 180px;
	}
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Regulamin sędziowski")</script></h1>

<p class="rozdzial">Rozdział I<br />ZAKRES REGULACJI</p>
<p class="paragraf">§ 1.</p>
Niniejszy regulamin sędziowski ustala zasady nabywania, zawieszania i utraty licencji sędziowskich uprawniających do sędziowania turniejów scrabble, organizowanych pod auspicjami Polskiej Federacji Scrabble (zwanej dalej PFS) oraz reguły prowadzenia turniejów przez sędziów PFS.

<p class="rozdzial">Rozdział II<br />NABYWANIE, ZAWIESZANIE I UTRATA UPRAWNIEŃ SĘDZIOWSKICH</p>
<p class="paragraf">§ 2.</p>
<ol>
	<li>Uprawnienia sędziego PFS nadaje, zawiesza i odbiera Zarząd PFS — w trybie i na zasadach określonych w niniejszym regulaminie.</li>
	<li>Rozróżnia się uprawnienia (licencje) I i II stopnia. Licencja II stopnia uprawnia do sędziowania turniejów zaliczanych do rankingów PFS oprócz turniejów z cyklu Grand Prix, Mistrzostw Polski i Pucharu Polski. Licencja I stopnia uprawnia do sędziowania wszystkich turniejów.</li>
</ol>

<p class="paragraf">§ 3.</p>
<ol>
	<li>Kandydat na Sędziego PFS powinien:
		<ol type="a">
			<li>mieć ukończone 21 lat</li>
			<li>być nieprzerwanie od roku członkiem PFS</li>
			<li>co najmniej przez rok uczestniczyć w turniejach scrabble zaliczanych do rankingu PFS jako zawodnik i rozegrać co najmniej 120 partii</li>
		</ol>
	</li>
	<li>Kandydaci na sędziów PFS zgłaszają swoje kandydatury do Zarządu PFS. Jeśli kandydat spełnia warunki formalne uzyskuje on zgodę Zarządu PFS i może rozpocząć staż.</li>
	<li>Chęć asystowania na turniejach PFS winna być zgłoszona Zarządowi nie później niż na 7 dni przed deklarowanym terminem odbywania stażu. Wówczas Zarząd zobowiązany jest poinformować sędziego wymienionego turnieju o osobie odbywającej staż.</li>
	<li>W jednym turnieju może asystować tylko jedna osoba. Kandydat na sędziego asystuje przy całym turnieju. Nie można asystować w turniejach szczebla centralnego oraz turniejach specjalnych (PP, MP, Le Mans) — ostateczną decyzję o możliwości stażu podejmuje Zarząd PFS.<br />
	O pierwszeństwie do odbywania stażu decyduje kolejność zgłoszeń kandydatów za wyjątkiem sytuacji, gdy:
	<ul>
		<li>którykolwiek z kandydatów zgłosi chęć odbycia egzaminu sędziowskiego — wówczas to on uzyskuje pierwszeństwo w rozważanym terminie</li>
		<li>osoba pierwsza w kolejce ma już wystarczającą ilość rund stażowych potrzebnych do dopuszczenia do egzaminu sędziowskiego — wówczas kolejna osoba chętna na staż w rozważanym terminie zajmuje jej miejsce.</li>
	</ul>
	</li>
	<li>Zarząd PFS nadaje uprawnienia sędziowskie II stopnia kandydatowi, który spełnił następujące warunki:
		<ol type="a">
			<li>asystował w charakterze sędziego pomocniczego w ciągu co najmniej 24 rund podczas turniejów PFS (w tym nie mniej niż dwa i nie więcej niż cztery turnieje)</li>
			<li>zdał egzamin sędziowski, w skład którego wchodzą:
				<ul>
					<li>egzamin praktyczny — samodzielne poprowadzenie turnieju PFS (co najmniej 12 rund) i otrzymanie pozytywnej oceny u
sędziego, pod którego nadzorem odbywał się egzamin,</li>
					<li>egzamin teoretyczny — prawidłowa odpowiedź na trzy losowo wybrane pytania z puli <a href="pytania_sedz.php">pytań egzaminacyjnych</a> na sędziów PFS. Egzamin w formie pisemnej przeprowadza sędzia główny danego turnieju.</li>
				</ul>
			</li>
		</ol>
		Wynik egzaminu podaje sędzia główny zaraz po jego zakończeniu. Ocena pracy kandydata powinna być poparta pisemnym uzasadnieniem.
	</li>
	<li>Egzamin sędziowski odbywa się podczas wybranego przez kandydata turnieju po uprzednim zgłoszeniu tego faktu Zarządowi PFS (nie później niż na dwa tygodnie przed deklarowanym terminem egzaminu).</li>
	<li><a href="pytania_sedz.php">Pula pytań</a> na egzamin teoretyczny na sędziego PFS jest z góry znana i dostępna na stronie PFS oraz dostarczana kandydatowi z chwilą rozpoczęcia stażu wraz z elektroniczną wersją programu sędziowskiego, na którym odbywają się turnieje PFS.</li>
	<li>Kandydat na sędziego ma prawo odwołać się od wyniku egzaminu do Zarządu PFS w terminie 14 dni. Ostateczną decyzję o przyznaniu uprawnień
sędziowskich podejmuje Zarząd PFS w ciągu 21 dni od daty otrzymania odwołania.</li>
	<li>Po otrzymaniu licencji II stopnia sędzia nabywa automatycznie licencję I stopnia po przeprowadzeniu dwóch turniejów.</li>
	<li>W wyjątkowych sytuacjach Zarząd PFS może nadać tymczasowo, na okres trwania zawodów uprawnienia sędziego PFS osobie będącej członkiem PFS, która zna bardzo dobrze zarówno regulamin turniejowy jak i regulamin sędziowski oraz potrafi obsługiwać program sędziowski.</li>
</ol>

<p class="paragraf">§ 4.</p>
<ol>
	<li>W przypadku naruszenia niniejszego Regulaminu lub podjęcia przy wykonywaniu obowiązków sędziowskich decyzji sprzecznej z zasadami gry w
scrabble, w szczególności z obowiązującymi regulaminami turniejowymi Zarząd PFS może nałożyć na sędziego, w zależności od wagi przewinienia następujące kary:
		<ul>
			<li>ostrzeżenie,</li>
			<li>zawieszenie uprawnień I stopnia (do sędziowania turniejów z cyklu Grand Prix, Mistrzostw Polski i Pucharu Polski) na okres od 30 dni do 1 roku,</li>
			<li>zawieszenie wszystkich uprawnień sędziowskich na okres od 30 dni do 1 roku,</li>
			<li>pozbawienie uprawnień sędziego PFS wraz z zakazem przystępowania do egzaminu sędziowskiego przez co najmniej rok od chwili
pozbawienia uprawnień.</li>
		</ul>
	</li>
	<li>Osoby pozbawione przez Zarząd PFS uprawnień sędziego PFS mogą ponownie uzyskać uprawnienia sędziowskie po zdaniu egzaminu sędziowskiego, nie wcześniej niż po odbyciu kary dodatkowej, o której mowa w ustępie poprzedzającym.</li>
	<li>W przypadku, gdy karze podlega sędzia będący jednocześnie członkiem Zarządu PFS — nie ma on prawa udziału w podejmowaniu decyzji o karach.</li>
	<li>W terminie 14 dni od otrzymaniu decyzji o pozbawianiu uprawnień sędzia ma prawo odwołać się do Walnego Zgromadzenia. Do czasu rozpatrzenia sprawy przez Walne Zgromadzenie decyzja Zarządu pozostaje w mocy.</li>
</ol>

<p class="paragraf">§ 5.</p>
Uprawnienia Sędziego PFS zostają zwieszone na okres, w którym nie jest on członkiem PFS.

<p class="rozdzial">Rozdział III<br />OBOWIĄZKI SĘDZIEGO PFS</p>
<p class="paragraf">§ 6.</p>
<ol>
	<li>Sędzia zobowiązany jest do:
		<ul>
			<li>wydawania bezstronnych orzeczeń zgodnych z przepisami PFS i regulaminem zawodów,</li>
			<li>sprawnego przeprowadzenia turnieju,</li>
			<li>spokojnego i jednoznacznego wydawania decyzji.</li>
		</ul>
	</li>
	<li>Do obowiązków sędziego pomocniczego należą:
		<ul>
			<li>wydawanie orzeczeń w pierwszej instancji,</li>
			<li>odpowiedzialność za przebieg gry zgodnie z jej regułami oraz regulaminem turnieju,</li>
			<li>dbałość, w miarę możliwości, o właściwe warunki gry.</li>
		</ul>
	</li>
	<li>Do obowiązków sędziego głównego należy:
		<ul>
			<li>przeprowadzenie odprawy technicznej przed turniejem, w szczególności sprawdzenie czy na komputerach zainstalowana jest aktualna wersja OSPS</li>
			<li>przeprowadzenie turnieju zgodnie z przepisami PFS i regulaminem,</li>
			<li>interpretacja reguł gry oraz regulaminu zawodów,</li>
			<li>pełnienie roli najwyższej instancji odwoławczej od decyzji sędziego pomocniczego,</li>
			<li>ogłoszenie wyników turnieju i przygotowanie pełnego sprawozdania (relacji i bazy turniejowej) oraz przesłanie ich do administratora strony internetowej PFS (na adres pfs@pfs.org.pl) nie później niż jeden dzień po zakończeniu rozgrywek lub przekazanie dokumentacji turnieju organizatorom, którzy we wskazanym powyżej terminie wypełnią ten obowiązek,</li>
			<li>zapewnienie relacji "na żywo" z turnieju w internecie jeśli tylko pozwalają na to warunki techniczne,</li>
			<li>w przypadku, gdy sędzia główny jest jedynym sędzią turnieju, należy do niego również wykonywanie obowiązków specyficznych dla roli sędziego pomocniczego.</li>
		</ul>
	</li>
	<li>Zabrania się sędziom PFS (stażystom na sędziów) brania udziału w turnieju podczas pełnienia funkcji sędziego (sędziego pomocniczego).</li>
</ol>

<p class="rozdzial">Rozdział IV<br />ZASADY PRZEPROWADZANIA TURNIEJÓW</p>
<p class="paragraf">§ 7.</p>
<ol>
	<li>Graczom zgłaszającym się do turnieju nadawane są numery startowe według rankingu turniejowego PFS, a graczom z identycznym rankingiem (np. z rankingiem poczatkowym 100) według alfabetu.</li>
	<li>Wyrównania:<br />
		Gracze, którzy dopisują się do turnieju w trakcie jego trwania otrzymują takie wyrównanie, aby przed daną rundą zajmowali miejsce wynikające z poniższego wzoru (z wyliczonej wartości należy wziąć część całkowitą). Realizuje się to poprzez wyrównanie takie, by dopisywany zawodnik miał identyczną ilość dużych punktów i powiększoną o jeden ilość małych, co zawodnik, który to miejsce zajmował (przed dopisaniem tego gracza). Jeśli kilka osób dopisuje się jednocześnie w tej samej rundzie, to obliczenia dla kolejnych osób wykonuje się niezależnie — tak więc, jeśli w kolejnej rundzie przystępują do turnieju debiutanci, to zajmują oni miejsce ex aequo.<br />
		<div class="wzor"></div>
	<b>A</b> — miejsce zajmowane przed ostatnim wycofaniem,<br />
	<b>B</b> — miejsce odpowiadające numerowi startowemu (dla debiutantów liczba ta = część całkowita z liczby 0,7*liczba uczestników),<br />
	<b>N</b> — liczba rund granych przed ostatnim wycofaniem,<br />
	<b>M</b> — liczba rund opuszczonych od ostatniego wycofania.
	</li>
	<li>Sędzia główny ma obowiązek przeprowadzić turniej systemem wpisanym w zgłoszeniu turnieju, chyba, że rozstawienie takim systemem jest niemożliwe.
	</li>
	<li>Sędzia powinien zmienić system rozgrywania turnieju przeprowadzanego sposobem połówkowym, jeśli:
		<ul>
			<li>liczba uczestników turnieju wynosi co najwyżej 90 osób i nie udaje się rozstawić żądanym systemem poszerzając zasięg grupy co 0,5
pkt do 2 punktów włącznie</li>
			<li>liczba uczestników turnieju przekracza 90 osób i nie udaje się rozstawić żądanym systemem poszerzając zasięg grupy co 0,5
pkt do 2,5 punktu włącznie</li>
		</ul>
	</li>
	<li>W przypadku wystąpienia sytuacji opisanej w powyższym punkcie należy wprowadzić system połówkowy z powtórzeniami o rozszerzonym zasięgu grupy na 1,5 punktu, a w kolejnej rundzie, jeśli sytuacja się powtórzy, należy wprowadzić system szwajcarski — i tak na zmianę (bez użycia tego samego systemu w dwóch kolejnych rundach).</li>
	<li>Zawodnikom, którzy w ciągu swojej kariery rozegrali co najmniej 30 gier turniejowych (ale nie znajdują się w ostatnio opublikowanym rankingu), na czas turnieju nadaje się ranking turniejowy złożony z maksymalnie tylu ostatnich rozegranych turniejów, aby suma gier wyniosła co najmniej 30. 
Sędzia ma obowiązek po zakończeniu turnieju, a przed przesłaniem plików z turnieju do administratorów strony internetowej oraz pozostałych sędziów PFS, wprowadzić wyżej wymienione poprawki. Jeżeli sędzia zawodów ma pewność co do wystąpienia wyżej wymienionej sytuacji u któregoś z graczy zapisujących się do turnieju lub zostanie mu to zgłoszone, powinien taką poprawkę nanieść przed rozpoczęciem turnieju. 
        <li>Ranking publikowany jest zawsze z datą na dzień po zakończonym turnieju.

	</li>
</ol>

<p class="rozdzial">Rozdział V<br />PRZEPISY PRZEJŚCIOWE I KOŃCOWE</p>
<p class="paragraf">§ 8.</p>
<ol>
	<li>Ustala się, iż osoby posiadające uprawnienia do sędziowania oficjalnych turniejów PFS w dniu wejścia w życie niniejszej uchwały, otrzymują licencje I stopnia.</li>
	<li>Osobom, które rozpoczęły staż sędziowski w okresie obowiązywania uchwały Zarządu PFS nr 10 z 2004 roku zalicza się wszystkie, nie więcej jednak niż 24 rundy turniejowe, w których asystowali sędziemu głównemu na poczet stażu w rozumieniu niniejszego Regulaminu.</li>
	<li>Osoby, które ukończyły staż sędziowski w okresie obowiązywania uchwały Zarządu PFS nr 10 z 2004 roku otrzymują licencję II stopnia sędziego PFS po uprzednim zaakceptowaniu kandydatury przez Zarząd PFS.</li>
</ol>
		
<p class="paragraf">§ 9.</p>
Regulamin wchodzi w życie z dniem 8 września 2005 roku.

<?require_once "files/php/bottom.php"?>
</body>
</html>
