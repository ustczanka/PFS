<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Przepisy : Regulamin turniejowy</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("turnieje","regulaminturniejowy");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
    <style type="text/css">
        h3 {
            margin: 20px 0 4px 0;
            font-weight: bold;
            font-size: 12px;
        }
        div.h4{
            margin: 8px 0px 8px 30px;
        }
    </style>
</head>

<body>
<? require_once "files/php/menu.php"; ?>
<h1><script>naglowek("Regulamin turniejowy")</script><span>po poprawkach z dnia 21.09.2005 oraz 30.10.2013</span></h1>

<p style="margin-bottom:16px;">
We wszystkich partiach turniejowych obowiązują zasady podane w instrukcji do gry.<br />
Ponadto zawodnicy mają obowiązek przestrzegać niżej opisanych reguł.<br><br>
<a href="rozne/RegulaminTurniejow.pdf">Cały regulamin w formacie PDF</a>
</p>

<div id="tabs" style="display:none;">
    <ul>
        <li><a href='#tabs-przygotowanie'>Przygotowanie</a></li>
        <li><a href='#tabs-poczatek'>Rozpoczęcie</a></li>
        <li><a href='#tabs-ruch'>Ruch</a></li>
        <li><a href='#tabs-elementy'>Elementy ruchu</a></li>
        <li><a href='#tabs-koniec'>Zakończenie</a></li>
        <li><a href='#tabs-porzadek'>Zasady porządkowe</a></li>
        <li><a href='#tabs-lamanie'>Łamanie przepisów</a></li>
        <li><a href='#tabs-system'>System rozgrywek</a></li>
        <li><a href='#tabs-solo'>Gra jednoosobowa</a></li>
    </ul>


<div id='tabs-przygotowanie'>
<h2>1. Przygotowanie do gry</h2>
<h3>1.1. Komplet do gry</h3>
<div class="h4">1.1.1. Przed przystąpieniem do rozegrania partii gracze mają obowiązek sprawdzić, czy dysponują pełnym zestawem płytek. Sprawdzenie to odbywa się przez ułożenie na planszy z płytek kwadratu o wymiarach 10x10. Pod żadnym warunkiem nie można wprowadzać jakichkolwiek korekt w zestawie już po rozpoczęciu partii. Po skończonej partii gracze zobowiązani są do ponownego
ułożenia z płytek kwadratu 10x10.</div>
<div class="h4">1.1.2. Plansza do gry powinna być ułożona bokiem do graczy, zegar powinien znajdować się z jednej strony planszy, a woreczek z literkami z drugiej strony. Plansze, zegary i woreczki powinny być ustawione w ten sam sposób na wszystkich stołach. Obracanie planszy jest dozwolone za zgodą obu graczy.</div>

<h3>1.2. Czas gry</h3>
W turniejach rankingowych obaj gracze mają na wykonanie wszystkich ruchów po 20 minut. Czas mierzy się za pomocą zegara szachowego (mechanicznego lub elektronicznego). Przed przystąpieniem do rozegrania partii gracze muszą sprawdzić, czy zegar jest nakręcony i zatrzymany (zneutralizowany), muszą też odpowiednio go ustawić (duże wskazówki w pozycji „za 20 minut pełna godzina”). Czas gry w zegarach elektronicznych należy ustawić na 20 minut.

<h3>1.3 Zapis</h3>
<div class="h4">1.3.1. Zapis jest prowadzony na kartach zapisu dostarczonych przez organizatora lub na czystych kartkach. Dozwolone jest stosowanie przez graczy własnych wzorów kart zapisu. Obowiązkiem każdego gracza jest notowanie wartości poszczególnych ruchów, wykonanych przez niego i przeciwnika, sumy punktów po każdym ruchu, wymian, pauz i strat. Obowiązkiem obu graczy jest dopilnowanie zapisywania na osobnej kartce
(lub na karcie zapisu, gdy brak osobnych kartek) wyrazów z blankiem (patrz 4.1).</div>
<div class="h4">1.3.2. W ważnych turniejach sędzia ma prawo wprowadzić tzw. pełny zapis, w którym gracz notuje również własne litery przed każdym ruchem, wszystkie wyrazy układane na planszy przez niego i przeciwnika oraz końcową pozycję partii. Do prowadzenia pełnego zapisu konieczna jest odpowiednia karta zapisu.</div>

<h3>1.4. Pomoce indywidualne</h3>
Gracz może korzystać z przygotowanego przed partią wykazu liter (wykreślanki), aby wykreślać z niego podczas gry te litery, które zostały już wykorzystane. Po rozpoczęciu partii może również prowadzić zapiski dowolnej treści. Zabronione jest natomiast korzystanie podczas gry z innych pomocy poza wykreślanką, a w szczególności z jakichkolwiek wykazów słów.
</div>

<div id='tabs-poczatek'>
<h2>2. Rozpoczęcie partii</h2>

<h3>2.1 Losowanie rozpoczynającego</h3>
Gracza rozpoczynającego partię wyznacza się za pomocą komputerowego rozstawienia lub za pomocą losowania liter. W przypadku losowania liter partię rozpoczyna gracz, który wylosował literę bliższą początkowi alfabetu. Blank jest równoznaczny z literą „A”. W przypadku wylosowania jednakowych liter losowanie powtarza się do skutku.
<h3>2.2 Zajęcie miejsca przy stole</h3>
W przypadku konfliktu między graczami co do zajmowanych przez nich miejsc przy stole prawo wyboru uzyskuje ten gracz, który wygra losowanie liter opisane w podpunkcie 2.1.
<h3>2.3 Dobieranie płytek</h3>
Gracz, który wygrał losowanie, dobiera losowo z woreczka siedem płytek i układa je na stojaku. Podczas dobierania płytek woreczek należy trzymać na wysokości ramienia. Płytki należy dobierać bez ociągania się, w taki sposób, by w sumie nie wyciągnąć ich z woreczka więcej niż siedem.
<h3>2.4 Włączenie zegara w momencie rozpoczęcia gry</h3>
Przeciwnik gracza rozpoczynającego włącza zegar w chwili, gdy gracz rozpoczynający zobaczył literę (lub blank) na pierwszej płytce, natomiast swoje siedem płytek dobiera z woreczka dopiero po włączeniu zegara. Włączenie zegara w tej sytuacji jest jednoznaczne z rozpoczęciem partii. Włączenie
zegara graczowi nieobecnemu w chwili rozpoczęcia rundy nie rozpoczyna partii.
</div>

<div id='tabs-ruch'>
<h2>3. Ruch</h2>

<h3>3.1 Porządek czynności</h3>
Na ruch gracza składają się kolejno:
<ol type="a">
    <li>ułożenie płytek na planszy lub zgłoszenie wymiany albo pauzy;</li>
    <li>zatrzymanie zegara;</li>
    <li>podanie głośno wartości punktowej ruchu;</li>
    <li>ewentualne sprawdzenie wyrazu lub wyrazów przez przeciwnika;</li>
    <li>zapisanie ruchu przez gracza (wartości punktowej danego ruchu i sumy punktów po ruchu);</li>
    <li>zapisanie ruchu przez przeciwnika lub zdjęcie skutecznie sprawdzonego wyrazu przez gracza wykonującego ruch;</li>
    <li>włączenie zegara przeciwnikowi;</li>
    <li>dobranie płytek;</li>
    <li>ewentualne wykreślenie liter na wykreślance.</li>
</ol>
Kolejność podpunktów d) i e) może być odwrotna.

<h3>3.2. Zatrzymanie ruchu</h3>
Zatrzymanie ruchu to chwila, po której nie wolno zmienić ułożenia płytek na planszy ani odwołać decyzji o wymianie lub pauzie. Oznaką zatrzymania ruchu jest zatrzymanie zegara lub — w grze bez zegara — podanie głośno wartości punktowej ruchu.
</div>

<div id='tabs-elementy'>
<h2>4. Elementy ruchu</h2>

<h3>4.1 Ułożenie płytek na planszy</h3>
<div class="h4">4.1.1. Płytki ułożone na planszy muszą tworzyć wyraz (wyrazy) zgodnie z regułami gry.</div>
<div class="h4">4.1.2. Jeśli wśród wyłożonych płytek jest blank, gracz musi wyraźnie określić, jaką literę alfabetu blank zastępuje i zapisać wyraz z blankiem
na osobnej kartce (lub na karcie zapisu gdy brak osobnych kartek). Kartka ta powinna leżeć przez całą partię w miejscu dobrze widocznym dla obu graczy.</div>
<div class="h4"></div>4.1.3. Jeśli powstaną kontrowersje co do litery, za którą został podstawiony blank, a wyraz z blankiem nie został wcześniej zapisany na osobnej kartce (lub na karcie zapisu), blank staje się martwą płytką do końca partii i żadnemu z graczy nie wolno ułożyć wyrazu, którego częścią byłby ten blank.

<h3>4.2 Wymiana</h3>
<div class="h4">4.2.1. Każdy gracz może wykonać do trzech wymian podczas partii. Przy wymianie w woreczku musi znajdować się przynajmniej siedem płytek, nawet wtedy gdy wymiana obejmuje mniejszą ich liczbę.</div>
<div class="h4">4.2.2. Aby dokonać wymiany, gracz:
    <ol type="a">
        <li>mówi „Wymiana”</li>
        <li>zdejmuje wymieniane płytki ze stojaka, kładzie je na stoliku i podaje liczbę wymienianych płytek;</li>
        <li>zatrzymuje zegar na czas zapisania wymiany przez obu graczy (po zatrzymaniu zegara nie wolno mu zmienić przygotowanych do wymiany płytek);</li>
        <li>włącza zegar przeciwnikowi;</li>
        <li>dobiera z woreczka potrzebną liczbę płytek i kładzie je tak, by nie pomieszały się z wymienianymi;</li>
        <li>wrzuca wymieniane płytki do woreczka.</li>
    </ol>
</div>
<div class="h4">4.2.3. Gracz traci ruch, jeżeli zgłosi wymianę i zatrzyma zegar, ale nie może jej wykonać ponieważ:
<ol type="a">
    <li>wcześniej dokonał już trzech wymian</li>
    <li>w woreczku znajduje się mniej niż siedem płytek.</li>
</ol>
</div>
<div class="h4">4.2.4. Jeżeli gracz nie zdejmie płytek przed zatrzymaniem zegara lub nie powie „Wymiana”, przeciwnik ma prawo włączyć mu zegar i przypomnieć o wykonaniu pominiętej czynności.</div>

<h3>4.3 Pauza</h3>
<div class="h4">4.3.1. Pauza jest dozwolona w każdym momencie gry.</div>
<div class="h4">4.3.2. Gracz pauzujący: mówi „Pauza”, wyłącza zegar na czas zapisania pauzy przez obu graczy, włącza zegar przeciwnika.</div>
<div class="h4">4.3.3. Cztery kolejne pauzy oznaczają zakończenie partii. Wymiana ani strata kolejki nie jest pauzą.</div>

<h3>4.4 Zatrzymanie zegara</h3>
<div class="h4">4.4.1. Zatrzymanie zegara zawsze powoduje zatrzymanie ruchu, tzn. nie wolno potem wprowadzać żadnych zmian w ułożeniu płytek na planszy ani zmieniać decyzji o wymianie lub pauzie.</div>
<div class="h4">4.4.2. Wolno zatrzymać zegar wyłącznie w celu:
<ol type="a">
    <li>dania znaku, że wykonany ruch jest ostateczny;</li>
    <li>usunięcia ze stojaka nadmiernej liczby płytek;</li>
    <li>wezwania sędziego;</li>
    <li>wezwania osoby do policzenia płytek w woreczku.</li>
</ol>
</div>
<div class="h4">4.4.3. Jeśli gracz wykonujący ruch poda i zapisze jego wartość, ale zapomni wyłączyć zegar, przeciwnik powinien zgodnie z duchem fair play zwrócić mu na to uwagę. Jeśli jednak przeciwnik tego nie zrobi, nie ma prawa sprawdzić wyrazu gracza wykonującego ruch ani ułożyć własnego wyrazu, dopóki zegar nie zostanie zatrzymany lub dopóki gracz wykonujący ruch nie przekroczy czasu przeznaczonego na partię.</div>
<div class="h4">4.4.4. Jeśli gracz wykonujący ruch zapisze jego wartość, ale nie wyłączy zegara i od razu przystąpi do losowania płytek, ma obowiązek — na żądanie przeciwnika — wrzucić dobrane płytki z powrotem do woreczka, przy czym jeżeli zaczął układać płytki na stojaku, przeciwnik ma prawo zażądać wrzucenia do woreczka wszystkich płytek. Po wrzuceniu płytek do woreczka gracz wykonujący ruch zatrzymuje zegar, a przeciwnik zapisuje wartość ruchu lub sprawdza ułożony wyraz (ułożone wyrazy).</div>

<h3>4.5 Podanie wartości ruchu</h3>
Wartość ruchu należy podawać wyraźnie, aby przeciwnik nie miał kłopotów z zapisaniem.
<h3>4.6 Zapisanie wartości ruchu</h3>
<div class="h4">4.6.1. Po zapisaniu wartości ruchu przez obu graczy zaleca się sprawdzenie, czy z ich obliczeń wynikają takie same sumy punktów.</div>
<div class="h4">4.6.2. Błąd w obliczeniach wartości ruchu lub sumy punktów po ruchu można skorygować w każdym momencie partii, gdy jest zatrzymany zegar. Wymagana jest do tego zgoda obu graczy. W innym przypadku należy wezwać sędziego, który na podstawie zapisów i sytuacji na planszy ustali, jaki jest wynik partii w danym momencie.</div>
<div class="h4">4.6.3. Błąd w obliczeniach wartości ruchu lub sumy punktów nie może być korygowany po podpisaniu przez obu graczy protokołu zakończenia partii, nawet jeżeli obaj gracze wyrażą na to zgodę. Nie dotyczy to jedynie sytuacji, gdy wynik zgodnie potwierdzany przez obu graczy został błędnie wpisany do protokołu. Poprawkę taką można wnieść do momentu rozstawienia przez sędziego następnej rundy (lub do chwili oficjalnego zakończenia turnieju, jeśli błąd dotyczy ostatniej rundy), a za zgodą sędziego do zakończenia danego dnia zawodów.</div>

<h3>4.7 Sprawdzanie wyrazów</h3>
<div class="h4">4.7.1. Wyraz (wyrazy) można sprawdzić wyłącznie w okresie między zatrzymaniem ruchu przez gracza wykonującego ruch a zapisaniem wartości tego ruchu przez jego przeciwnika.</div>
<div class="h4">4.7.2. Jeżeli w wyniku ruchu powstał więcej niż jeden wyraz, gracz sprawdzający może sprawdzić dowolną liczbę wyrazów.</div>
<div class="h4">4.7.3. Gracz sprawdzający mówi wyraźnie „Sprawdzam”. Jeżeli wskutek ruchu jego przeciwnika powstały przynajmniej dwa wyrazy, gracz ma obowiązek po słowie „Sprawdzam” wskazać wszystkie sprawdzane wyrazy. Po ogłoszeniu decyzji o sprawdzeniu gracz nie może już jej zmienić, tzn. nie może zrezygnować ze sprawdzenia ani zmienić zestawu wyrazów do sprawdzenia.</div>
<div class="h4">4.7.4. Jeżeli żądanie sprawdzenia zostało zgłoszone przed zatrzymaniem ruchu, nie ma ono mocy, a gracz wykonujący ruch ma prawo zmienić swój ruch.</div>
<div class="h4">4.7.5. Jeżeli żądanie sprawdzenia zostało zgłoszone po zapisaniu wartości ruchu przez gracza sprawdzającego, nie ma ono mocy, nawet jeżeli kwestionowany wyraz jest niedozwolony.</div>
<div class="h4">4.7.6. Jeżeli na turnieju są sędziowie pomocniczy, gracz sprawdzający zapisuje sprawdzany wyraz (sprawdzane wyrazy) na kartce, pokazuje przeciwnikowi dla stwierdzenia, czy na kartce nie ma błędów, po czym wręcza kartkę sędziemu pomocniczemu.</div>
<div class="h4">4.7.7. Jeżeli na turnieju nie ma sędziów pomocniczych, gracze mają obowiązek razem sprawdzić wyraz (wyrazy) na ekranie komputera za pomocą Oficjalnego Słownika Polskiego Scrabblisty (OSPS-u) z aktualnym update'em lub — jeśli nie ma OSPS-u — w słownikach.</div>
<div class="h4">4.7.8. Jeżeli sprawdzeniu podlega więcej niż jeden wyraz, wynik sprawdzania podaje się łącznie. Oznacza to, że przy sprawdzaniu komputerowym na ekranie komputera muszą znaleźć się jednocześnie wszystkie sprawdzane wyrazy i dopiero wtedy można je sprawdzić. Jeśli natomiast sprawdzenia dokonują sędziowie pomocniczy, to w razie stwierdzenia błędnego ruchu wielowyrazowego nie mogą oni informować żadnego z graczy, który wyraz w zestawie jest błędny.</div>
<div class="h4">4.7.9. Wpisanie wyrazów do komputera jest obowiązkiem gracza sprawdzającego. Przy sprawdzaniu komputerowym wyraz (wyrazy) należy sprawdzać dokładnie w takiej formie, w jakiej znajduje się (znajdują się) na planszy.</div>
<div class="h4">4.7.10. Jeżeli na turnieju nie ma sędziów pomocniczych, to gracz, który rezygnuje z udziału w sprawdzaniu, nie ma prawa kwestionować wyniku sprawdzania, nawet jeśli jego przeciwnik popełnił błąd (np. błędnie wpisał słowo do komputera lub źle odmienił formę hasłową wyrazu ze słownika).</div>
<div class="h4">4.7.11. Jeśli gracz sprawdzający wpisze do komputera wyraz w innej formie niż ta, która znajduje się na planszy, należy wezwać sędziego. Jeżeli sędzia stwierdzi niezgodność form wyrazu na planszy i na ekranie komputera, karze gracza sprawdzającego odebraniem możliwości sprawdzenia danego wyrazu (danych wyrazów).</div>
<div class="h4">4.7.12. Jeżeli na turnieju są sędziowie pomocniczy, a wyraz (wyrazy) do sprawdzenia przepisano z planszy z błędami, wtedy można zażądać jego (ich) ponownego sprawdzenia.</div>
<div class="h4">4.7.13. W razie wątpliwości co do dopuszczalności sprawdzanego wyrazu decyzję podejmuje sędzia. Jego decyzja jest ostateczna. W szczególności sędzia ma prawo zaproponować niestandardowe rozwiązania w razie stwierdzenia, że komputer używany do sprawdzania wyrazów podaje błędne wyniki (np. wskutek niedostępności części bazy danych, braku update'u lub braku dopuszczalnego wyrazu w OSPS-ie).</div>

<h3>4.8 Zdjęcie niepoprawnego wyrazu</h3>
Jeśli sprawdzenie wyrazu (wyrazów) było skuteczne, to gracz, który ułożył niepoprawny wyraz, może włączyć zegar przeciwnikowi dopiero po zdjęciu swoich płytek z planszy i zapisaniu straty przez obu graczy.

<h3>4.9 Włączanie zegara</h3>
<div class="h4">4.9.1. W trakcie gry włączanie zegara jest obowiązkiem gracza, który jako ostatni wykonał zapisany ruch, z wyjątkiem sytuacji podanej w następnym punkcie.</div>
<div class="h4">4.9.2. Włączenie zegara po usunięciu ze stojaka nadmiernej liczby płytek jest obowiązkiem gracza, który miał nadmiar płytek, natomiast po wezwaniu sędziego — gracza, który wezwał sędziego.</div>
<div class="h4">4.9.3. Jeżeli po usunięciu ze stojaka nadmiernej liczby płytek lub po wezwaniu sędziego włączenie zegara jest obowiązkiem przeciwnika gracza mającego ruch, ale zapomni on to zrobić, gracz mający ruch ma prawo wznowić grę przy zatrzymanym zegarze. O zatrzymaniu jego ruchu decyduje wtedy
podanie wartości ruchu lub zgłoszenie wymiany albo pauzy.</div>
<div class="h4">4.9.4 Jeżeli gracz mający ruch wezwał sędziego, a następnie zapomniał włączyć zegar, przeciwnik ma prawo zrobić to za niego.</div>

<h3>4.10 Dobieranie płytek</h3>
<div class="h4">4.10.1. Jeśli gracz dobierze nadmierną liczbę płytek, to po zatrzymaniu zegara jego przeciwnik losuje x+2 wybrane płytki spośród wszystkich jego płytek (x oznacza liczbę nadmiarowo wylosowanych płytek). Przeciwnik ma prawo obejrzeć wylosowane w ten sposób płytki i następnie odrzucić do worka wybrane x płytek. </div>
<div class="h4">4.10.2. Zasadę opisaną w punkcie 4.10.1 stosuje się niezależnie od tego czy gracz obejrzał nadmiarowo wylosowane płytki, czy też nie zdążył ich zobaczyć. </div>
<div class="h4">4.10.3. Jeśli gracz, mający na stojaku nadmierną liczbę płytek, wykona i zatrzyma ruch, to na żądanie przeciwnika zdejmuje te płytki z planszy i kładzie je ponownie na stojaku, a jego przeciwnik postępuje tak, jak w punkcie 4.10.1. Gracz, który wykonał i zatrzymał ruch, mimo że miał na stojaku ponad siedem płytek, traci kolejkę. Jeśli zachodzi podejrzenie, że nadmiar płytek na stojaku trwał dłużej niż jedną kolejkę, należy wezwać sędziego.</div>
<div class="h4">4.10.4. W razie szybkiego wykonywania ruchów gracze mogą nie nadążać z dobieraniem płytek na czasie przeciwnika. W takiej sytuacji gracz ma prawo dokończyć dobierania płytek przy zatrzymanym zegarze.</div>

<h3>4.11 Wykreślanie liter</h3>
<div class="h4">4.11.1. Wzór wykreślanki jest indywidualną sprawą gracza, nie może ona jednak zawierać nic oprócz zestawu liter używanych do gry.</div>
<div class="h4">4.11.2. Niedopuszczalne jest wykreślanie liter w czasie, gdy zegar jest zatrzymany.</div>
</div>

<div id='tabs-koniec'>
<h2>5. Zakończenie partii</h2>

<h3>5.1 Sposoby zakończenia partii</h3>
<div class="h4">5.1.1. Gra kończy się gdy:
<ol type="a">
    <li>jeden z graczy wyłoży na planszę ostatnia płytkę ze stojaka, ruch zostaje zaakceptowany, a woreczek jest już pusty;</li>
    <li>nastąpią cztery pauzy z rzędu (w przypadku straty kolejki/kolejek łączna liczba strat i pauz nie może przekroczyć pięciu)</li>
    <li>obaj zawodnicy przekroczą czas.</li>
</ol>
</div>
<div class="h4">5.1.2. Po położeniu przez gracza ostatniej płytki ze stojaka na planszy, gdy woreczek jest już pusty, przeciwnik musi potwierdzić zakończenie partii przez zapisanie wyniku albo zażądać sprawdzenia ostatniego ruchu przeciwnika.</div>
<div class="h4">5.1.3. Jeżeli gracz, na którego przypada kolejność wykonania ruchu, uważa, że żaden ruch nie jest możliwy, ma obowiązek zgłosić pauzę.</div>
<div class="h4">5.1.4. Jeżeli partia kończy się czterema pauzami z rzędu lub jeśli obaj gracze przekroczą czas, każdy z graczy odejmuje całkowitą wartość niewykorzystanych liter ze swojego stojaka od sumy punktów uzyskanych przez siebie w dotychczasowych ruchach.</div>

<h3>5.2 Protokół partii</h3>
Po ustaleniu wyniku partii obaj gracze mają obowiązek czytelnie wpisać go do protokołu partii i przekazać protokół sędziemu. Wynik podany w protokole jest ostateczny, z wyjątkiem sytuacji opisanej w punkcie 4.6.3. Jeżeli w protokole są poprawki, obaj gracze powinni osobiście potwierdzić je u sędziego.
</div>

<div id='tabs-porzadek'>
<h2>6. Zasady porządkowe</h2>
<h3>6.1 Przekroczenie czasu gry</h3>
<div class="h4">6.1.1. Jeśli któryś z graczy przekroczy przeznaczony dla niego czas (tzn. chorągiewka zegara mechanicznego spadnie, a zegar elektroniczny pokaże 00:00, zanim gracz pozbędzie się ostatniej płytki ze stojaka), traci prawo do następnych ruchów. Przeciwnik gra dalej sam do wyłożenia ostatniej płytki, do wyczerpania swojego czasu, zgłoszenia dwóch kolejnych pauz lub zanotowania trzech kolejnych ruchów będących przemieszaniem strat i pauz.</div>
<div class="h4">6.1.2. Jeżeli chorągiewka zegara spadnie graczowi (lub gdy zegar elektroniczny pokaże 00:00) w trakcie ruchu, przed zatrzymaniem przez niego zegara, wtedy gracz cofa ruch w trakcie którego skończył się czas nawet wówczas, gdy ułożył już cały wyraz na planszy.</div>
<div class="h4">6.1.3. Gracz, który przekroczył limit czasu musi towarzyszyć przeciwnikowi do zakończenia partii i zachowuje prawo sprawdzania wyrazów przeciwnika zgodnie z zasadami.</div>

<h3>6.2 Spóźnienia i nieobecność</h3>
<div class="h4">6.2.1. Każdy uczestnik turnieju ma obowiązek być obecnym podczas ogłaszania godziny rozpoczęcia kolejnej partii.</div>
<div class="h4">6.2.2. Jeśli gracz jest nieobecny w momencie rozpoczęcia rundy, przeciwnik ma prawo — a na żądanie sędziego obowiązek — włączyć mu zegar. Gdy spóźniony gracz nadejdzie, zatrzymuje zegar. Następnie obaj gracze rozpoczynają partię zgodnie z punktem 2 regulaminu (gracz spóźniony ma czas na partię pomniejszony o okres spóźnienia).</div>
<div class="h4">6.2.3. Jeśli obaj gracze są nieobecni w momencie rozpoczęcia rundy, wtedy sędzia włącza zegar. Gdy pierwszy z graczy nadejdzie, zatrzymuje zegar, a sędzia ustawia na obu zegarach czas, który upłynął, po czym włącza zegar jego przeciwnikowi. Przeciwnik po nadejściu zatrzymuje zegar. Następnie obaj gracze rozpoczynają partię zgodnie z punktem 2 regulaminu (obaj mają czas pomniejszony o okres swojego spóźnienia).</div>

<h3>6.3 Odejście od planszy</h3>
Gracz może odejść od planszy jedynie za zgodą przeciwnika lub sędziego i bez przerywania partii. Oznacza to, że odchodzi on od planszy w czasie, gdy nad swoim ruchem myśli przeciwnik. Przeciwnik może pod jego nieobecność wykonać ruch, a następnie zatrzymać zegar, nie wolno mu jednak dobrać płytek. Gracz, który wraca sprawdza lub zapisuje ruch przeciwnika. Dalej partia toczy się normalnie.Jeżeli nieobecność gracza przekroczy 5 minut przeciwnik ma
prawo włączyć mu zegar i zgłosić to sędziemu. Od tej decyzji przysługuje graczowi odwołanie do sędziego.

<h3>6.4 Liczenie płytek w woreczku</h3>
<div class="h4">6.4.1. Podczas gry z użyciem jednolitych płytek (wszystkie gładkie, lub wszystkie niegładkie) w każdej chwili można sprawdzić liczbę płytek pozostających w woreczku. Podczas gry z użyciem niejednolitych płytek można sprawdzić liczbę płytek pozostających w woreczku wyłącznie za zgodą przeciwnika. Jeśli przeciwnik nie wyrazi na to zgody, płytki w woreczku może policzyć osoba trzecia. Zatrzymuje się wtedy zegar do czasu nadejścia osoby liczącej i ponownie go włącza w momencie, gdy zaczyna ona liczyć płytki w woreczku. Sprawdzenia należy dokonać na swoim czasie.</div>
<div class="h4">6.4.2. Podczas liczenia płytek w woreczku należy trzymać go na wysokości ramienia. Przed przystąpieniem do liczenia i po jego zakończeniu należy pokazać przeciwnikowi, iż nie trzyma się w ręku żadnych płytek.</div>

<h3>6.5 Zachowanie przy planszy</h3>
<div class="h4">6.5.1. Po rozpoczęciu partii gracze nie powinni odzywać się do siebie ponad konieczność wynikającą z regulaminu.</div>
<div class="h4">6.5.2. Niewskazane jest zapowiadanie ruchu (np. „Myślę, że spróbuję ułożyć wyraz...”), pokazywanie przeciwnikowi swojego stojaka z literami itp. Gracze nie powinni przeszkadzać grającym przy sąsiednich stolikach ani tym bardziej komentować toczących się tam partii.</div>
<div class="h4">6.5.3. Jeśli po ułożeniu wyrazu (wyrazów) przez gracza i zatrzymaniu zegara przeciwnik zwleka z zapisaniem lub zakwestionowaniem ruchu, wykorzystując ten czas do myślenia nad swoim wyrazem, należy wezwać sędziego.</div>
<div class="h4">6.5.4. Zabronione jest używanie w czasie rozgrywanej partii, bez wyraźnej zgody przeciwnika, telefonów komórkowych oraz innych podręcznych urządzeń
elektronicznych.</div>
<div class="h4">6.5.5. Na sali gry palenie jest zabronione. Jeżeli to możliwe, organizator powinien dopilnować, aby były odrębne miejsca do odpoczynku między partiami dla palących i niepalących.</div>
<div class="h4">6.5.6. W trakcie rozgrywania partii obowiązuje całkowity zakaz spożywania jakichkolwiek napojów alkoholowych pod rygorem usunięcia przez sędziego z turnieju.</div>
<div class="h4">6.5.7. Po zakończeniu partii gracze powinni opuścić salę lub pozostać przy swoim stoliku i zachować ciszę, aby nie przeszkadzać tym graczom, którzy jeszcze nie skończyli grać.</div>
</div>

<div id='tabs-lamanie'>
<h2>7. Łamanie przepisów</h2>
<h3>7.1 Wykroczenia</h3>
Gracz przyłapany na świadomym naruszaniu obowiązujących przepisów może zostać ukarany przez sędziego różnymi karami, w tym upomnieniem, odjęciem wielokrotności 50 punktów (w zależności od wagi wykroczenia), wykluczeniem z danej partii (walkowerem), wykluczeniem z dalszych rozgrywek. Walkower przyznaje się w stosunku 400:1 (zaliczany do rankingu). Jeżeli jednak w momencie przyznania walkowera przeciwnik wykluczonego gracza ma ponad 400 punktów, zachowuje on swoje punkty. W razie wykluczenia gracza przyznaje się walkower w partii, podczas której został on wykluczony, natomiast poprzednie jego wyniki pozostają ważne.

<h3>7.2 Następstwa wykluczenia</h3>
Na wniosek sędziego turnieju Zarząd PFS może podjąć decyzję o okresowym zakazie dopuszczania do turniejów PFS gracza wykluczonego z turnieju.
</div>

<div id='tabs-system'>
<h2>System rozgrywania turniejów</h2>
Większość turniejów Scrabble rozgrywana jest, znanym choćby z szachów, systemem szwajcarskim (połówkowym), nieco modyfikowanym dla potrzeb scrabble.<br />
Załóżmy, że do turnieju zgłosiło się 40 graczy, z czego 30 ma ranking, a 10 nie ma. Przed pierwszą rundą graczy ustawia się według kolejności na podstawie posiadanych rankingów (10 graczy bez rankingu ustawianych jest alfabetycznie wg, nazwisk na miejscach od 31 do 40). Następnie listę startową dzieli się na pół i zestawia obie połowy tak, że 1 gra z 21, 2 z 22, 3 z 23 itd.<br />
Po każdej rundzie graczy ustawia się od pierwszego do ostatniego według bilansu zwycięstw i porażek, a w drugiej kolejności — sumarycznej liczby zdobytych punktów (skoru) i rozstawienie następuje jak w pierwszej rundzie w obrębie grup utworzonych ilością zwycięstw w poprzednich rundach, przy czym ci sami gracze nie mogą się ze sobą spotkać ponownie — by tego uniknąć robi się roszady wśród sąsiadujących par. Końcową kolejność ustala się na podstawie ilości zwycięstw, a w drugiej kolejności — sumarycznej liczby zdobytych małych punktów.<br />
W dłuższych turniejach, w ostatnich rundach stosuje się system „duński”, to znaczy gracze są kojarzeni dokładnie według osiągniętych w poprzednich
rundach wyników niezależnie od tego czy już się spotkali.
</div>

<div id='tabs-solo'>
<h2>Regulamin ustanawiania rekordu w grze jednoosobowej</h2>
Do próby ustanowienia rekordu Polski w grze jednoosobowej może przystąpić każdy pełnoletni obywatel RP który spełni następujące warunki:
<ol>
    <li>Zgłosi chęć ustanowienia rekordu Polski przynajmniej na dwa tygodnie przed planowanym terminem</li>
    <li>Wpłaci kwotę 50 zł.</li>
    <li>Zaakceptuje bez zastrzeżeń niniejszy regulamin.</li>
</ol><br />
Do próby ustanowienia rekordu mogą przystąpić niepełnoletni pod warunkiem przedstawienia pisemnej zgody rodziców lub opiekunów. Próba ustanowienia rekordu odbywa się pod nadzorem specjalnej dwuosobowej komisji wyłanianej każdorazowo spośród 10 najlepszych graczy z listy rankingowej oraz zarządu PFS.<br />
Za rekord uważa się najwyższy wynik punktowy osiągnięty w pojedynczej partii z zachowaniem podanych niżej zasad szczegółowych. <br />
Podział kwoty wpłaconej w związku ze zgłoszeniem próby ustanowienia rekordu będzie następujący:
<ul style="list-style:none;">
    <li>Komisja — 20 zł</li>
    <li>PFS — 30 zł</li>
</ul>
Terminy w których mogą być podjęte próby będą ogłaszane z co najmniej 7-dniowym wyprzedzeniem.<br /><br />
Szczegółowe warunki przeprowadzenia próby:
<ul>
    <li>zasady doboru liter i ich wykładania jak w normalnej partii turniejowej</li>
    <li>czas rozgrywania partii — 40 min.</li>
    <li>ilość dopuszczalnych wymian — 6</li>
    <li>dopuszczalna ilość wyrazów zdjętych (nieprawidłowych i zakwestionowanych przez komisję sędziowską) — 3</li>
</ul>
Podejmujący próbę sam prowadzi pełen zapis partii.<br /><br />

Obowiązki komisji sędziowskiej:
<ol>
    <li>Obserwacja przebiegu partii.</li>
    <li>Kwestionowanie wątpliwych wyrazów i sprawdzanie ich w OSPS.</li>
    <li>Włączanie zegara (wyłączenia dokonuje sam podejmujący próbę po dokonaniu ruchu).</li>
    <li>Pilnowanie prawidłowości przebiegu partii i zachowania ewentualnych kibiców.</li>
</ol><br />
Po zakończeniu próby komisja sporządza protokół z przebiegu próby a do protokołu zostaje załączony zapis partii.<br />
Na zakończenie komisja stwierdza czy próba była udana i rekord Polski został ustanowiony. W tym przypadku podejmującemu próbę przysługuje tytuł <b>Rekordzisty Polski Scrabble w Grze Jednoosobowej.</b>
</div>
</div>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
