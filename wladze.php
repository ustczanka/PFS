<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Władze PFS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("pfs","wladze");</script>
  <style type="text/css">
    ol li{
    line-height:24px;
    }
    td{
    padding-right:8px;
    }
  </style>
</head>


<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Władze PFS")</script></h1>
Aktualne władze Federacji zostały wybrane przez Walne Zgromadzenie Członków 18 stycznia 2014 r.

<h2>Zarząd</h2>
<table>
    <tr> <td>1.</td><td>Karol Wyrębkiewicz (Warszawa) - prezes</td>                    <td><a onclick="sendMail('k.wyrebkiewicz','pfs.org.pl')" class="email"></a></td>            <td>telefon: 691 664 325</td> <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=3211214" alt="" /></td> <td><a href="gg:3211214">3211214</a></td> <tr>
    <tr> <td>2.</td><td>Justyna Górka (Kraków) - wiceprezes</td>    <td><a onclick="sendMail('j.gorka','pfs.org.pl')" class="email"></a></td>  <td>telefon: 606 288 378</td> <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=2340768" alt="" /></td> <td><a href="gg:2340768">2340768</a></td> <tr>
    <tr> <td>3.</td><td>Krzysztof Bukowski (Milanówek) - skarbnik</td>          <td><a onclick="sendMail('k.bukowski','pfs.org.pl')" class="email"></a></td>        <td>telefon: 792 304 104</td> <td></td> <td></td> <tr>
    <tr> <td>4.</td><td>Kamil Górka (Kraków)</td>                             <td><a onclick="sendMail('k.gorka','pfs.org.pl')" class="email"></a></td>           <td>telefon: 602 443 800</td> <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=1547595" alt="" /></td> <td><a href="gg:1547595">1547595</a></td> <tr>
    <tr> <td>5.</td><td>Joanna Jewtuch (Warszawa)</td>                               <td><a onclick="sendMail('j.jewtuch','pfs.org.pl')" class="email"></a></td>          <td>telefon: 698 300 148</td> <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=2697042" alt="" /></td> <td><a href="gg:2697042">2697042</a></td> <tr>
    <tr> <td>6.</td><td>Krzysztof Mówka (Warszawa)</td>                             <td><a onclick="sendMail('k.mowka','pfs.org.pl')" class="email"></a></td>           <td>telefon: 691 710 143</td> <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=5444876" alt="" /></td> <td><a href="gg:5444876">5444876</a></td> <tr>
    <tr> <td>7.</td><td>Krzysztof Obremski (Wrocław)</td>                               <td><a onclick="sendMail('k.obremski','pfs.org.pl')" class="email"></a></td>       <td>telefon: 728 499 749</td> <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=4215677" alt="" /></td> <td><a href="gg:4215677">4215677</a></td> <tr>
</table>

<h2>Komisja Rewizyjna</h2>

<table>
   <tr> <td>1.</td><td height="21">Mariola Osajda-Matczak</td>    <tr>
   <tr> <td>2.</td><td height="21">Katarzyna Ślusarska</td>   <tr>
   <tr> <td>3.</td><td height="21">Rafał Wesołowski</td>       <tr>
   
</table>

<h2>Sąd Koleżeński</h2>

<table>
   <tr> <td>1.</td><td height="21">Marek Reda (przewodniczący)</td>     <tr>
   <tr> <td>2.</td><td height="21">Rafał Matczak</td>     <tr>
   <tr> <td>3.</td><td height="21">Miłosz Wrzałek</td>       <tr>
   
</table>

<div class="alignright"><a href="sad_kolezenski.php">Regulamin Sądu Koleżeńskiego</a></div>

<?require_once "files/php/bottom.php"?>
</body>
</html>
