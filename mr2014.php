
<html>
<head>
    <title>Polska Federacja Scrabble :: Turnieje : Mistrzostwa Regionów 2014</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("turnieje","mr2014")</script>
    <style type="text/css">
        table.linki{margin: 20px auto 0 auto;}
        table.linki td{ padding: 8px;vertical-align: top;}
    </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Mistrzostwa Regionów 2014")</script></h1>

<h2>Mistrzostwa Regionów 2014</h2>
Cykl turniejów Mistrzostwa Regionów 2014 stanowi zawody towarzyszące cyklowi Grand Prix 2014
<h2>Klasyfikacje</h2>
W Mistrzostwach Regionów 2014 prowadzone będą trzy niezależne klasyfikacje – oddzielnie w każdym regionie. Cykl Mistrzostwa Regionów 2014 obejmuje:<br> <br><ol>
<li>Region Pomorski
</li><li>Region Południowy
</li><li>Region  Północno-Wschodni
</li></ol>
<h2>Turnieje</h2>
Cykl Mistrzostwa Regionów 2014 obejmuje 15 turniejów podzielonych na 3 regiony:
<table class="linki ramkadolna">
<tbody><tr><td colspan=2><b>Region Pomorski</b></td></tr><tr><td>1.</td><td><a href="http://pfs.org.pl/turniej.php?id=796">I Mistrzostwa Stargardu Szczecińskiego </a></td></tr><tr><td>2.</td><td><a href="http://pfs.org.pl/turniej.php?id=778">XIII Mistrzostwa Wybrzeża</a></td></tr><tr><td>3.</td><td><a href="http://pfs.org.pl/turniej.php?id=774">X Mistrzostwa Ziemi Wałeckiej "Wałeckie Scrabblobranie"</a></td></tr><tr><td>4.</td><td><a href="http://pfs.org.pl/turniej.php?id=766">XVIII Mistrzostwa Ziemi Słupskiej w Scrabble</a></td></tr><tr><td>5.</td><td><a href="http://pfs.org.pl/turniej.php?id=786">II Otwarte Mistrzostwa Świnoujścia</a></td></tr>
<tr><td colspan=2></td></tr><td colspan=2><b>Region Południowy</b></td></tr><tr><td>1.</td><td><a href="http://pfs.org.pl/turniej.php?id=759">II Uzdrowiskowe słów cięcie-gięcie</a></td></tr><tr><td>2.</td><td><a href="http://pfs.org.pl/turniej.php?id=770">XVIII Otwarte Mistrzostwa Krakowa w Scrabble </a></td></tr><tr><td>3.</td><td><a href="http://pfs.org.pl/turniej.php?id=781">IX Mistrzostwa Jaworzna w Scrabble</a></td></tr><tr><td>4.</td><td><a href="http://pfs.org.pl/turniej.php?id=760">X Mistrzostwa Wrocławia w Scrabble</a></td></tr><tr><td>5.</td><td><a href="http://pfs.org.pl/turniej.php?id=792">V Otwarte Mistrzostwa Raciborza w Scrabble</a></td></tr>
<tr><td colspan=2></td></tr><td colspan=2><b>Region Północno-Wschodni</b></td></tr><tr><td>1.</td><td><a href="http://pfs.org.pl/turniej.php?id=762">XVI Mistrzostwa Warszawy</a></td></tr><tr><td>2.</td><td><a href="http://pfs.org.pl/turniej.php?id=767">IX Mistrzostwa Milanówka – Truskawki w Milanówku  </a></td></tr><tr><td>3.</td><td><a href="http://pfs.org.pl/turniej.php?id=777">III Otwarte Mistrzostwa Konojad </a></td></tr><tr><td>4.</td><td><a href="http://pfs.org.pl/turniej.php?id=772">XIV Otwarte Mistrzostwa Ziemi Ostródzkiej „Blanki w Szranki”</a></td></tr><tr><td>5.</td><td><a href="http://pfs.org.pl/turniej.php?id=794">IV Otwarte Mistrzostwa Białegostoku o Puchar Przewodniczącego Rady Miasta</a></td></tr></tbody></table>
<br><br>

<h2>Zasady punktacji</h2>
<ol>
        <li>Punkty do klasyfikacji Mistrzostw Regionu zdobywa tylu uczestników, ile wynosi część całkowita liczby obliczonej według wzoru: liczba uczestników, którzy uczestniczyli co najmniej w 5 rundach turnieju, dzielona przez 3.
	</li><li>Zawodnik, który zajmie ostatnie punktowane miejsce, otrzymuje 1 punkt, każdy kolejny o jeden punkt więcej.
	</li><li>Minimalna liczba uczestników zdobywających punkty do klasyfikacji Mistrzostw Regionu wynosi 20 (tzn. w przypadku turnieju, w którym uczestniczyło mniej niż 60 osób, minimum w pięciu rundach turnieju, zawodnik za miejsce 20 otrzymuje 1 punkt, za 19–2, itd., a za 4–17, za 3– 18, za 2–19, za 1–20).
	</li><li>Maksymalna liczba uczestników zdobywających punkty do klasyfikacji Mistrzostw Regionu wynosi 30 (tzn. w przypadku turnieju, w którym uczestniczyło więcej niż 90 osób, minimum w pięciu rundach turnieju, zawodnik za miejsce 30 otrzymuje 1 punkt, za 29–2, itd., za 4–27, za 3–28, za 2–29 i za 1–30).
	</li><li>Każdemu zawodnikowi biorącemu udział w cyklu rozgrywek Mistrzostwa Regionów 2014 do końcowej klasyfikacji zalicza się cztery najlepsze wyniki z pięciu turniejów w danym regionie.
	</li><li>Przy równej liczbie punktów w klasyfikacji końcowej o kolejności decyduje wyższa zdobycz punktowa w najlepszym turnieju, a jeśli to nie da efektu (gracze mieli identyczny wynik) w kolejnym najlepszym turnieju, itd. Jeśli i to nie przyniesie rozstrzygnięcia, zawodnicy zajmują miejsce ex-aequo, a nagrody finansowe za ich miejsca są sumowane i dzielone po równo.
	 
</li></ol>

<h2>Nagrody</h2>
Nagrody w cyklu Mistrzostwa Regionów 2014 wynoszą:
<ul>
    <li>za pierwsze miejsce w Mistrzostwach Regionu Południowego – 200 zł</li>
    <li>za pierwsze miejsce w Mistrzostwach Regionu Pomorskiego – 200 zł</li>
    <li>za pierwsze miejsce w Mistrzostwach Regionu Północno-Wschodniego – 200 zł</li>
</ul>

<a name="klasyfikacja"></a>
<h2>Klasyfikacja Mistrzostw Regionu Pomorskiego</h2>
<h2>Klasyfikacja Mistrzostw Regionu Południowego</h2>
<h2>Klasyfikacja Mistrzostw Regionu Północno-Wschodniego</h2>

<?include "files/php/bottom.php"?>
</body>
</html>
