<?php
include_once "files/php/funkcje.php";
$sql_conn = pfs_connect ();
?>

<html>
<head>
  <title>Polska Federacja Scrabble :: Roczne podsumowania sezonów</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />

    <link rel="stylesheet" href="files/css/jquery-ui.css" type="text/css" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-ui.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>
        jSubmenu("turnieje","archiwum");
        $(function() { $( "#tabs" ).tabs ().show (); });
    </script>
</head>

<body>
<? require_once "files/php/menu.php"; ?>
<h1><script>naglowek("Roczne podsumowania sezonów")</script></h1>

<div id="tabs" style="display:none;">
<?
$act_year = date('Y');
print '<ul>';
for ($year = 2013; $year >= 2004; $year--) {
    print '<li><a href="#tabs-'.$year.'">' . $year . '</a></li>';
}
print '</ul>';
?>

<div id='tabs-2013'>

<h2 id="turnieje">Turnieje</h2>
W 2013 r. odbyło się <b>35</b> turniejów zaliczanych do rankingu PFS. Wzięło w nich udział <b>
512</b> osób.
W tym roku najwięcej uczestników przyjechało na
<a href="http://www.pfs.org.pl/turniej.php?id=730">XXI Mistrzostwa Polski</a> - 117.
Podobnie jak przed rokiem, na drugim miejscu tej klasyfikacji są
<a href="http://www.pfs.org.pl/turniej.php?id=751">XVII Mistrzostwa Krakowa</a>, które zgromadziły 91 uczestników, zaś na trzecim miejscu pod względem frekwencji znalazł się Wałcz - 90 osób wzięło udział w <a href="http://www.pfs.org.pl/turniej.php?id=734">
IX Mistrzostwach Ziemi Wałeckiej "Wałeckie scrabblobranie"</a>. <br>

Na turniejowej mapie Polski pojawiło się aż pięć nowych miejscowości: <a href="http://www.pfs.org.pl/turniej.php?id=712">Huta Szklana</a>, <a href="http://www.pfs.org.pl/turniej.php?id=727">Polanica Zdrój</a>, <a href="http://www.pfs.org.pl/turniej.php?id=705">Ostrów Wielkopolski</a>, <a href="http://www.pfs.org.pl/turniej.php?id=715">Świnoujście</a> oraz <a href="http://www.pfs.org.pl/turniej.php?id=724">Piasutno</a>. Do klasyfikacji tej nie wliczamy <a href="http://www.pfs.org.pl/turniej.php?id=765">Pragi</a> w Czechach - w grudniu odbył się tam specjalny turniej rankingowy, który towarzyszył Mistrzostwom Świata po Angielsku.<br>
<a href="http://www.pfs.org.pl/turniej.php?id=728">VIII Klubowe Mistrzostwa Polski</a>
odbyły się w Szczepocicach i Radomsku. Nauczyciele również walczyli jak co roku w
<a href="http://www.pfs.org.pl/turniej.php?id=733">Wałczu</a>, uczniowie szkół podstawowych, gimnazjów i szkół ponadgimnazjalnych 
w
<a href="http://www.pfs.org.pl/turniej.php?id=713">Kutnie</a>.<br>
W <a href="http://www.pfs.org.pl/turniej.php?id=757">VI Turnieju Mikołajkowym</a> wzięło udział
93 młodych scrabblistów ze szkolnych kółek.<p>Liczba uczestników turniejów
przedstawia się następująco:
</p>

<table class="zapisy">

<tr><td><p align="center">Turniej</td>
    <td class='alignright' align="right">
    <p align="center">Liczba<br>
    uczestników</td>
    <td class='alignright' align="left">
    <p align="center">Zwycięzca</td></tr>


</td>
<tr><td>
 XXI Mistrzostwa Polski w Scrabble</td>
  <td class='alignright' align="right">117</td>
  <td class='alignright' align="left">Dominik Urbacki</td>
 </tr>

<tr><td>
XVII Otwarte Mistrzostwa Krakowa </td>
  <td class='alignright' align="right">91</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 IX Mistrzostwa Ziemi Wałeckiej "Wałeckie scrabblobranie"</td>
  <td class='alignright' align="right">90</td>
  <td class='alignright' align="left">Kamil Górka</td>
 </tr>
<tr><td>
 IX Mistrzostwa Wrocławia<span
  style='mso-spacerun:yes'> </span></td>
  <td class='alignright' align="right">79</td>
  <td class='alignright' align="left">Łukasz Tuszyński</td>
 </tr>
<tr><td>
 I Mistrzostwa Ostrowa Wielkopolskiego</td>
  <td class='alignright' align="right">77</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 VIII Mistrzostwa Jaworzna</td>
  <td class='alignright' align="right">76</td>
  <td class='alignright' align="left">Bartosz Morawski</td>
 </tr>
<tr><td>
 VIII Mistrzostwa Milanówka "Truskawki w Milanówku"</td>
  <td class='alignright' align="right">74</td>
  <td class='alignright' align="left">Paweł Zaręba</td>
 </tr>
<tr><td>
 Urodziny Alfreda</td>
  <td class='alignright' align="right">69</td>
  <td class='alignright' align="left">Mariusz Skrobosz</td>
 </tr>
<tr><td>
 I Mistrzostwa Świnoujścia</td>
  <td class='alignright' align="right">65</td>
  <td class='alignright' align="left">Bartosz Morawski</td>
 </tr>
<tr><td>
  XII Mistrzostwa Wybrzeża<span
  style='mso-spacerun:yes'> </span></td>
  <td class='alignright' align="right">63</td>
  <td class='alignright' align="left">Mateusz Borsuk</td>
 </tr>
<tr><td>
 XV Mistrzostwa Szczecina "Zanim zakwitną magnolie"</td>
  <td class='alignright' align="right">63</td>
  <td class='alignright' align="left">Kazimierz Merklejn</td>
 </tr>
<tr><td>
 XIII Mistrzostwa Ziemi Ostródzkiej "Blanki w Szranki"</td>
  <td class='alignright' align="right">62</td>
  <td class='alignright' align="left">Szymon Płachta</td>
 </tr>
<tr><td>
 II Mistrzostwa Konojad</td>
  <td class='alignright' align="right">60</td>
  <td class='alignright' align="left">Dawid Pikul</td>
 </tr>
<tr><td>
XIV Turniej o Puchar Dyrektora MDK w Rumi</td>
  <td class='alignright' align="right">59</td>
  <td class='alignright' align="left">Natalia Woźniak</td>
 </tr>
<tr><td>
 VIII Mistrzostwa Doliny Karpia</td>
  <td class='alignright' align="right">58</td>
  <td class='alignright' align="left">Stanisław Rydzik</td>
 </tr>
<tr><td>
 XVI Mistrzostwa Warmii i Mazur</td>
  <td class='alignright' align="right">56</td>
  <td class='alignright' align="left">Paweł Jackowski</td>
 </tr>
<tr><td>
 Turniej Tysiąclecia w Katowicach</td>
  <td class='alignright' align="right">52</td>
  <td class='alignright' align="left">Dawid Adamczyk</td>
 </tr>
<tr><td>
 XVI Otwarte Mistrzostwa Nowego Dworu Mazowieckiego </td>
  <td class='alignright' align="right">51</td>
  <td class='alignright' align="left">Tomasz Zwoliński</td>
 </tr>
<tr><td>
 XIX Puchar Polski</td>
  <td class='alignright' align="right">48</td>
  <td class='alignright' align="left">Krzysztof Obremski</td>
 </tr>
<tr><td>
 XIX Mistrzostwa Łodzi</td>
  <td class='alignright' align="right">48</td>
  <td class='alignright' align="left">Krzysztof Usakiewicz</td>
 </tr>
<tr><td>
 IV Otwarte Mistrzostwa Raciborza </td>
  <td class='alignright' align="right">47</td>
  <td class='alignright' align="left">Dawid Adamczyk</td>
 </tr>
<tr><td>
 XII Turniej 24h Non Stop Le Mans </td>
  <td class='alignright' align="right">46</td>
  <td class='alignright' align="left">Kamil Górka</td>
 </tr>
<tr><td>
 XVII Mistrzostwa Ziemi Kujawskiej "O Kryształowe Jajo Świąteczne"</td>
  <td class='alignright' align="right">46</td>
  <td class='alignright' align="left">Tomasz Zwoliński</td>
 </tr>
<tr><td>
 Turniej dla Ambitnych - Oczko </td>
  <td class='alignright' align="right">45</td>
  <td class='alignright' align="left">Joanna Jewtuch</td>
 </tr>
<tr><td>
 VIII Klubowe Mistrzostwa Polski </td>
  <td class='alignright' align="right">45</td>
  <td class='alignright' align="left">GHOST Kraków</td>
 </tr>
<tr><td>
 III Otwarte Mistrzostwa Białej Podlaskiej</td>
  <td class='alignright' align="right">45</td>
  <td class='alignright' align="left">Grzegorz Kurowski</td>
 </tr>
<tr><td>
 IV Mazurski Turniej o Puchar Prezydenta Miasta Ełku</td>
  <td class='alignright' align="right">43</td>
  <td class='alignright' align="left">Dawid Pikul</td>
 </tr>
<tr><td>
 XVII Mistrzostwa Ziemi Słupskiej</td>
  <td class='alignright' align="right">39</td>
  <td class='alignright' align="left">Dariusz Kosz</td>
 </tr>
<tr><td>
 I Turniej Scrabble nad Jeziorem</td>
  <td class='alignright' align="right">39</td>
  <td class='alignright' align="left">Irena Sołdan</td>
 </tr>
<tr><td>
 Uzdrowiskowe słów gięcie-cięcie</td>
  <td class='alignright' align="right">34</td>
  <td class='alignright' align="left">Paweł Mazurek</td>
 </tr>
<tr><td>
 I Mistrzostwa Gór Swiętokrzyskich "O miotłę czarownicy" </td>
  <td class='alignright' align="right">28</td>
  <td class='alignright' align="left">Krzysztof Mówka</td>
 </tr>
<tr><td>
 XI Urodziny Siódemki - V Mistrzostwa Gór Bystrzyckich</td>
  <td class='alignright' align="right">24</td>
  <td class='alignright' align="left">Kamil Górka</td>
 </tr>
<tr><td>

 Turniej Towarzyszący Mistrzostwom Świata w Scrabble </td>
  <td class='alignright' align="right">8</td>
  <td class='alignright' align="left">Kazimierz Merklejn</td>
 </tr>
<tr><td>
II Turniej Książąt Świdnickich</td>
  <td class='alignright' align="right">8</td>
  <td class='alignright' align="left">Stanisław Rydzik</td>
 </tr>
  <tr>
    <td>&nbsp;</td>
    <td class='alignright' align="right">&nbsp;</td>
    <td class='alignright' align="left">&nbsp;</td>
  </tr>
<tr><td>
XI Ogólnopolskie Mistrzostwa Szkół Kutno '2013 </td>
  <td class='alignright' align="right">102</td>
  <td class='alignright' align="left">Szkoła Podstawowa nr 11 w Ostrowie Wielkopolskim <br>Gimnazjum nr 112 im. Króla Jana III Sobieskiego w Warszawie<br>I Liceum Ogólnokształcące nr 1 w Kutnie</td>
 </tr>

<tr><td>
 VI Turniej Mikołajkowy</td>
  <td class='alignright' align="right">93</td>
  <td class='alignright' align="left">Juliusz Czupryniak</td>
 </tr>
<tr><td>
III Turniej o Puchar Gimnazjum Niepublicznego nr 12 im. Lotników Amerykańskich</td>
  <td class='alignright' align="right">58</td>
  <td class='alignright' align="left">Juliusz Czupryniak</td>
 </tr>
 <tr><td>
Marcowe harce na planszy czyli I Turniej SCRABBLE na Wielkiej Wyspie</td>
  <td class='alignright' align="right">42</td>
  <td class='alignright' align="left">Łukasz Goriaczko</td>
 </tr>
 <tr><td>
IX Mistrzostwa Polski Nauczycieli</td>
  <td class='alignright' align="right">20</td>
  <td class='alignright' align="left">Aleksandra Kaczanowska</td>
 </tr>
 <tr><td>
II turniej motoSCRABBLE</td>
  <td class='alignright' align="right">14</td>
  <td class='alignright' align="left">Mariusz Sylwestrzuk</td>
 </tr>
<tr><td>
 II Mistrzostwa Polski w Scrabble po Hiszpańsku,
eliminacje do Mistrzostw Świata w Scrabble po Hiszpańsku</td>
  <td class='alignright' align="right">4</td>
	<td class='alignright' align="left">Mariusz Sylwestrzuk</td>
 </tr>
 <tr><td>
 VI Mistrzostwa Polski w Scrabble po Angielsku — Eliminacje do Mistrzostw Świata po Angielsku</td>
  <td class='alignright' align="right">3</td>
	<td class='alignright' align="left">Rafał Dominiczak</td>
 </tr>
<tr><td>
 XI Mensański Turniej Scrabble</td>
  <td class='alignright' align="right">?</td>
  <td class='alignright' align="left">Marta Kleinrok</td>
 </tr>
  <tr>
    <td>&nbsp;</td>
    <td class='alignright' align="right">&nbsp;</td>
    <td class='alignright' align="left">&nbsp;</td>
  </tr>

  <tr><td class='alignright' align="left"><b>razem 2191 osób (turnieje rankingowe: 1855)</b></td>
  </tr>

 </tr>

</table>



<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>23</b> graczy, z czego ośmiu zanotowało więcej niż jedno zwycięstwo.
Klasyfikację medalową wygrał <b>Kamil Górka</b> (3-2-1), przed Michałem Alabrudzińskim (2-3-2) i Bartoszem Morawskim  (2-2-1). Najwięcej - siedem - razy na podium stawali <b>Michał Alabrudziński</b> i <b>Dawid Pikul</b>. <br><br>
W cyklu Grand Prix zwyciężył <b>Michał Alabrudziński</b>, przed Bartoszem Morawskim i Dawidem Pikulem.<br><br>
Mistrzem Polski został <b>Dominik Urbacki</b> a wicemistrzem podobnie jak przed rokiem Kazimierz Jr. Merklejn. Trzecie miejsce zajął Bartosz Morawski. <br><br>
W 2013 roku na liście zwycięzców turniejowych pojawiły się aż cztery nowe nazwiska: Paweł Zaręba, Paweł Mazurek, Szymon Płachta i Natalia Woźniak.<br>
Aż trzynastu zawodników (Paweł Zaręba, Paweł Mazurek, Natalia Woźniak, Katarzyna Ślusarska, Kacper Zegadło, Maciej Rzychoń, Joanna Suchanek, Dorota Stachowska, Maciej Grąbczewski, Mikołaj Fidziński, Krzysztof Sporczyk, Natalia Ogierman, Marzena Zawierska) "zaliczyło" pierwsze miejsca na podium. Warto wspomnieć, że Marzena Zawierska zajęła 3 miejsce w swoim debiucie!<br><br>
</b>Tytuł Klubowych Mistrzów Polski zdobył po raz pierwszy <b>klub Ghost z Krakowa.</b>.<br>
<br>
Oto <b>klasyfikacja "medalowa"</b> za rok 2013:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<tr><td class='lp'>1</td><td class='osoba'>Kamil Górka</td><td>3</td><td>2</td><td>1</td></tr><tr><td class='lp'>2</td><td class='osoba'>Michał Alabrudziński</td><td>2</td><td>3</td><td>2</td></tr><tr><td class='lp'>3</td><td class='osoba'>Bartosz Morawski</td><td>2</td><td>2</td><td>1</td></tr><tr><td class='lp'>4</td><td class='osoba'>Kazimierz Merklejn jr</td><td>2</td><td>1</td><td>1</td></tr><tr><td class='lp'>5</td><td class='osoba'>Dawid Pikul</td><td>2</td><td>0</td><td>5</td></tr><tr><td class='lp'>6</td><td class='osoba'>Tomasz Zwoliński</td><td>2</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Stanisław Rydzik</td><td>2</td><td>0</td><td>1</td></tr><tr><td class='lp'>8</td><td class='osoba'>Dawid Adamczyk</td><td>2</td><td>0</td><td>0</td></tr><tr><td class='lp'>9</td><td class='osoba'>Łukasz Tuszyński</td><td>1</td><td>2</td><td>1</td></tr><tr><td class='lp'>10</td><td class='osoba'>Paweł Jackowski</td><td>1</td><td>1</td><td>1</td></tr><tr><td class='lp'>11</td><td class='osoba'>Grzegorz Kurowski</td><td>1</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Dominik Urbacki</td><td>1</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Dariusz Kosz</td><td>1</td><td>1</td><td>0</td></tr><tr><td class='lp'>14</td><td class='osoba'>Mariusz Skrobosz</td><td>1</td><td>0</td><td>2</td></tr><tr><td class='lp'>15</td><td class='osoba'>Krzysztof Obremski</td><td>1</td><td>0</td><td>1</td></tr><tr><td class='lp'>16</td><td class='osoba'>Mateusz Borsuk</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Irena Sołdan</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Paweł Zaręba</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Paweł Mazurek</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Szymon Płachta</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Krzysztof Usakiewicz</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Natalia Woźniak</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Krzysztof Mówka</td><td>1</td><td>0</td><td>0</td></tr><tr><td class='lp'>24</td><td class='osoba'>Justyna Górka</td><td>0</td><td>3</td><td>0</td></tr><tr><td class='lp'>25</td><td class='osoba'>Łukasz Bobowski</td><td>0</td><td>2</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Jakub Zaryński</td><td>0</td><td>2</td><td>0</td></tr><tr><td class='lp'>27</td><td class='osoba'>Miłosz Wrzałek</td><td>0</td><td>1</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Andrzej Kwiatkowski</td><td>0</td><td>1</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Rafał Lenartowski</td><td>0</td><td>1</td><td>1</td></tr><tr><td class='lp'>30</td><td class='osoba'>Katarzyna Ślusarska</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Jolanta Chłopińska</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Andrzej Rechowicz</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Piotr Broda</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Kacper Zegadło</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Maciej Rzychoń</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Marek Dudkiewicz</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Marek Reda</td><td>0</td><td>1</td><td>0</td></tr><tr><td class='lp'>38</td><td class='osoba'>Mirosław Uglik</td><td>0</td><td>0</td><td>2</td></tr><tr><td class='lp'>39</td><td class='osoba'>Joanna Suchanek</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Dorota Stachowska</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Mariusz Wrześniewski</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Mariola Osajda-Matczak</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Piotr Pietuchowski</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Maciej Grąbczewski</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Mikołaj Fidziński</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Krzysztof Sporczyk</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Bogusław Szyszka</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Natalia Ogierman</td><td>0</td><td>0</td><td>1</td></tr><tr><td class='lp'>&nbsp;</td><td class='osoba'>Marzena Zawierska</td><td>0</td><td>0</td><td>1</td></tr></table>

<b>"Klasyfikację wszech czasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa zwiększyła się o <b>21</b> osób. Obecnie jest na niej <b>
333</b> graczy, w poczekalni - 331 (rok temu 363). <br><br>
Cały rok 2013 na pozycji lidera rankingu spędził Michał Alabrudziński.<br><br> Rok w pierwszej dziesiątce poza liderem rozpoczęli i zakończyli Bartosz Morawski, Krzysztof Mówka, Dariusz Kosz, Mariusz Skrobosz i Dominik Urbacki. 
Z czołowej dziesiątki wypadli: Karol Wyrębkiewicz, Mirosław Uglik, Maciej Czupryniak i Łukasz Tuszyński. Zastąpili ich: Kamil Górka, Kazimierz Merklejn, Dawid Pikul i Paweł Jackowski.<br>
W ciągu roku w pierwszej dziesiątce byli także: Miłosz Wrzałek (5), Stanisław Rydzik (7) i Marek Reda (10).<br><br>

Oto czołówka rankingu i porównanie z końcem roku .
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2012</b></td><td><b>ranking</b></td><td><b>
    koniec 2013</b></td><td><b>ranking</b></td></tr>

  <tr><td>1.</td><td class="osoba">Michał Alabrudziński </td><td>
    158.84</td><td class="osoba"> Michał Alabrudziński</td><td>
    153.30</td></tr>
  <tr><td>2.</td><td class="osoba">
    Bartosz Morawski</td><td>155.00</td><td class="osoba">
    Bartosz Morawski</td><td>153.20</td></tr>
  <tr><td>3.</td><td class="osoba"> Dariusz Kosz </td><td>
    153.58</td><td class="osoba">Krzysztof Mówka </td><td>
    152.05</td></tr>
  <tr><td>4.</td><td class="osoba">
    Karol Wyrębkiewicz </td><td>152.98</td><td class="osoba">
    Dariusz Kosz</td><td>151.87</td></tr>
  <tr><td>5.</td><td class="osoba">
    Dominik Urbacki </td><td>150.59</td><td class="osoba">
    Mariusz Skrobosz</td><td>151.44</td></tr>
  <tr><td>6.</td><td class="osoba"> Krzysztof Mówka </td><td>
    150.37</td><td class="osoba"> Kamil Górka</td><td>
    151.02</td></tr>
  <tr><td>7.</td><td class="osoba">
    Mirosław Uglik </td><td>149.85</td><td class="osoba">
   Kazimierz Merklejn </td><td>150.88</td></tr>
  <tr><td>8.</td><td class="osoba"> Mariusz Skrobosz </td><td>
    149.26</td><td class="osoba"> Dawid Pikul</td><td>
    150.79</td></tr>
  <tr><td>9.</td><td class="osoba">
    Maciej Czupryniak </td><td>148.98</td><td class="osoba">
    Paweł Jackowski</td><td>149.84</td></tr>
  <tr><td>10</td><td class="osoba"> Łukasz Tuszyński </td><td>
    146.52</td><td class="osoba"> Dominik Urbacki</td><td>
    149.37</td></tr>
</table>

<h2>Rankingi osób na miejscach</h2>
<p>Wysokość rankingu potrzebna by zająć dane miejsce na liście rankingowej
dalej rośnie. Na koniec roku 8 osób miało ranking powyżej 150, a 33 osoby powyżej 140.</p>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td><td><b>koniec 2007</b></td><td><b>koniec 2008</b></td><td><b>koniec 2009</b></td><td><b>koniec 2010</b></td><td><b>koniec 2011</b></td><td><b>koniec 2012</b></td><td><b>koniec 2013</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td><td>146,74</td><td>149,38</td><td>150,70</td><td>153,59</td><td>153,01</td><td>158.84</td><td>153.30</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td><td>140,96</td><td>141,28</td><td>142,49</td><td>144,37</td><td>146,78</td><td>146.52</td><td>149.37</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td><td>137,50</td><td>138,11</td><td>137,87</td><td>141,65</td><td>142,48</td><td>144.35</td><td>144.61</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td><td>129,53</td><td>131,09</td><td>131,83</td><td>132,09</td><td>133,72</td><td>135.64</td><td>136.09</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td><td>122,52</td><td>123,78</td><td>124,24</td><td>124,63</td><td>125,41</td><td>125.64</td><td>129.19</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td><td>116,48</td><td>116,93</td><td>117,80</td><td>118,57</td><td>118,91</td><td>119.63</td><td>121.02</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td><td>111,60</td><td>112,13</td><td>111,97</td><td>112,55</td><td>114,08</td><td>113.70</td><td>116.07</td></tr>
</table>

<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych,
poczekalnia nie jest brana pod uwagę)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2012 - ranking 2013</b></td></tr>
  <tr><td align="left">Juliusz Czupryniak</td><td>19.50</td><td>101.28 - 120.78</td></tr>
  <tr><td align="left">Jakub Błażejewski</td><td>19.01</td><td>103.70 - 122.71</td></tr>
  <tr><td align="left">Marek Charytoniuk</td><td>12.25</td><td>113.56 - 125.81</td></tr>
  <tr><td align="left">Dorota Maciejuk</td><td>12.17</td><td>117.40 - 129.57</td></tr>
  <tr><td align="left">Dariusz Maciejuk</td><td>11.20</td><td>119.91 - 131.11</td></tr>
  <tr><td align="left">Tomasz Zwoliński</td><td>10.27</td><td>138.95 - 149.22</td></tr>
  <tr><td align="left">Aleksander Puchalski</td><td>10.20</td><td>133.05 - 143.25</td></tr>
  <tr><td align="left">Marta Sztajnert</td><td>10.12</td><td>109.32 - 119.44</td></tr>
  <tr><td align="left">Paweł Jankowski</td><td>9.68</td><td>60.58 - 70.26</td></tr>
  <tr><td align="left">Adam Jurkowski</td><td>9.22</td><td>121.54 - 130.76</td></tr>

  </table>


<h2>Najlepsze nabytki listy rankingowej (tylko debiutanci, powroty na listę rankingową nie są brane pod uwagę)</h2>
&nbsp;<table class="klasyfikacja">
  <tr><td align="left">gracz</td><td>"wejście"</td><td>koniec roku</td></tr>
  <tr><td align="left">Daniel Mundyk</td><td>134.86</td><td>134.86</td></tr>
  <tr><td align="left">Natalia Woźniak</td><td>129.83</td><td>133.40</td></tr>
  <tr><td align="left">Wojciech Chodakiewicz</td><td>129.53</td><td>127.19</td></tr>
  <tr><td align="left">Michał Rogalewicz</td><td>127.87</td><td>127.87</td></tr>
  <tr><td align="left">Patryk Wolniewicz</td><td>125.27</td><td>-</td></tr>
  <tr><td align="left">Paweł Zbrowski</td><td>122.59</td><td>122.59</td></tr>
  <tr><td align="left">Dawid Brandys</td><td>115.61</td><td>122.59</td></tr>
  <tr><td align="left">Bartłomiej Pawlak</td><td>113.79</td><td>113.87</td></tr>
  <tr><td align="left">Agnieszka Matus</td><td>107.27</td><td>109.74</td></tr>
 
  </table>

<h2>Ranking z partii rozegranych w 2013 roku</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td>ranking</td><td align="center">liczba gier</td></tr>
  <tr><td align="left">Kazimierz Merklejn</td><td>154.73</td><td align="center">108</td></tr>
  <tr><td align="left">Michał Alabrudziński</td><td>154.23</td><td align="center">237</td></tr>
  <tr><td align="left">Bartosz Morawski</td><td>153.06</td><td align="center">191</td></tr>
  <tr><td align="left">Mariusz Skrobosz</td><td>152.86</td><td align="center">88</td></tr>
  <tr><td align="left">Krzysztof Mówka</td><td>152.28</td><td align="center">85</td></tr>
  <tr><td align="left">Paweł Jackowski</td><td>151.30</td><td align="center">162</td></tr>
  <tr><td align="left">Dawid Pikul</td><td>150.79</td><td align="center">194</td></tr>
  <tr><td align="left">Paweł Zaręba</td><td>150.69</td><td align="center">78</td></tr>
  <tr><td align="left">Dariusz Kosz</td><td>150.14</td><td align="center">126</td></tr>
  <tr><td align="left">Łukasz Bobowski</td><td>149.35</td><td align="center">97</td></tr>
</table>

&nbsp;<h2>Najwięcej rozegranych partii w 2013 r.</h2>
  Najwięcej partii (ponad 300) rozegrali Rafał Wesołowski i Irena Sołdan.
 <table class="klasyfikacja">
 <tr><td align="left">Rafał Wesołowski</td><td>344</td></tr>
 <tr><td align="left">Irena Sołdan</td><td>301</td></tr>
 <tr><td align="left">Krzysztof Sporczyk</td><td>277</td></tr>
 <tr><td align="left">Stanisław Rydzik</td><td>276</td></tr>
 <tr><td align="left">Kamil Górka</td><td>262</td></tr>
 <tr><td align="left">Michał Alabrudziński</td><td>237</td></tr>
 <tr><td align="left">Tomasz Zwoliński</td><td>235</td></tr>
 <tr><td align="left">Justyna Górka</td><td>226</td></tr>
 <tr><td align="left">Grzegorz Kurowski</td><td>213</td></tr>
 <tr><td align="left">Miłosz Wrzałek</td><td>210</td></tr>
  </table>


<h2>Statystyka</h2>
<table class="klasyfikacja">
<br>

<tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td><td><b>2007</b></td><td><b>2008</b></td><td><b>2009</b></td><td><b>2010</b></td> <td><b>2011</b></td><td><b>2012</b></td><td><b>2013</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td> <td>32</td><td>30</td><td>34</td><td>32</td><td>34</td><td>36</td><td>35</td></tr>
  <tr><td>Gracze</td><td>399</td><td>505</td><td>511</td><td>595</td><td>574</td> <td>520</td><td>524</td><td>525</td><td>540</td><td>515</td><td>495</td><td>512</td></tr>
  <tr><td>Gry rankingowe</td><td>9 888</td><td>11 544</td><td>11 447</td><td>14 496</td><td>12 667</td><td>12 501</td><td>12 190</td><td>12 201</td><td>12 620</td><td>13 075</td><td>11 672</td><td>11 754</td></tr>
  <tr><td>Średnia gier na gracza</td><td>24,8</td><td>22,9</td><td>22,4</td><td>24,4</td><td>22,1</td><td>24,0</td><td>23,3</td> <td>23,2</td><td>23,4</td><td>25,4</td><td>23,6</td><td>23,0</td></tr>
  <tr><td>Uczestnicy<br>(turnieje rankingowe)</td><td>1914</td><td>2016</td><td>2002</td><td>2602</td><td>2318</td><td>2129</td> <td>2085</td><td>2067</td><td>2111</td><td>2086</td><td>1826</td><td>1870</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>74</td><td>70</td><td>80</td><td>79</td><td>72</td><td>67</td><td>70</td><td>61</td> <td>66</td><td>61</td><td>50,7</td><td>53,4</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td><td>3</td><td>2</td><td>2</td> <td>3</td><td>4</td><td>4</td><td>2</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td><td>9</td> <td>13</td><td>11</td><td>13</td><td>17</td><td>10</td><td>8</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td><td>58</td> <td>56</td><td>64</td><td>65</td><td>68</td><td>56</td><td>55</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td><td>92</td> <td>88</td><td>77</td><td>103</td><td>99</td><td>49</td><td>89</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td><td>346</td> <td>366</td><td>372</td><td>437</td><td>416</td><td>346</td><td>358</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td><td>368</td> <td>343</td><td>345</td><td>331</td><td>330</td><td>312</td><td>333</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td><td>337</td> <td>337</td><td>340</td><td>376</td><td>371</td><td>363</td><td>331</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td><td>705</td> <td>681</td><td>685</td><td>707</td><td>701</td><td>675</td><td>664</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td><td>116</td> <td>131</td><td>102</td><td>146</td><td>116</td><td>108</td><td>102</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td><td>Wałcz - 146</td><td>Ostróda - 132</td><td>Wałcz - 125</td><td>MP - 126</td><td>Kraków - 113</td><td>MP - 111</td><td>MP - 117</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td><td>1</td> <td>0</td><td>5</td><td>3</td><td>4</td><td>3</td><td>5</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td><td>6</td> <td>8</td><td>8</td><td>9</td><td>0</td><td>3</td><td>4</td></tr>
</table>
</div>



<div id='tabs-2012'>

<h2 id="turnieje">Turnieje</h2>
W 2012 r. odbyło się <b>36 </b> turniejów zaliczanych do rankingu PFS. Wzięło w nich udział <b>
495</b> osób.
W tym roku najwięcej uczestników przyjechało na
<a href="http://www.pfs.org.pl/turniej.php?id=644">XX Mistrzostwa Polski</a> - 111.
9 osób mniej odwiedziło Kraków przy okazji
<a href="http://www.pfs.org.pl/turniej.php?id=630">XVI Mistrzostw Krakowa</a>. Na trzecim miejscu pod względem frekwencji znalazł się Wałcz - 89 osób wzięło udział w <a href="http://www.pfs.org.pl/turniej.php?id=623">

VIII Mistrzostwach Ziemi Wałeckiej "Wałeckie scrabblobranie"</a>. <br>
Na turniejowej mapie Polski pojawiły się trzy nowe miejscowości: <a href="http://www.pfs.org.pl/turniej.php?id=651">Szczepocice/Radomsko</a>, <a href="http://www.pfs.org.pl/turniej.php?id=647">Gdańsk</a> oraz <a href="http://www.pfs.org.pl/turniej.php?id=614">Konojady</a>. <br>
<a href="http://www.pfs.org.pl/turniej.php?id=611#relacja">VII Klubowe Mistrzostwa Polski</a>
odbyły się w Wałczu. Nauczyciele również walczyli jak co roku w
<a href="http://www.pfs.org.pl/turniej.php?id=622">Wałczu</a>, uczniowie szkół podstawowych, gimnazjów i szkół ponadgimnazjalnych 
w
<a href="http://www.pfs.org.pl/turniej.php?id=658">Kutnie</a>.<br>
W <a href="http://www.pfs.org.pl/turniej.php?id=565">V Turnieju Mikołajkowym</a> wzięło udział
74 młodych scrabblistów ze szkolnych kółek.<p>Liczba uczestników turniejów
przedstawia się następująco:
</p>

<table class="zapisy">

<tr><td><p align="center">Turniej</td>
    <td class='alignright' align="right">
    <p align="center">Liczba<br>
    uczestników</td>
    <td class='alignright' align="left">
    <p align="center">Zwycięzca</td></tr>


</td>
<tr><td>
 XX Mistrzostwa Polski w Scrabble</td>
  <td class='alignright' align="right">111</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>

<tr><td>
XVI Otwarte Mistrzostwa Krakowa </td>
  <td class='alignright' align="right">102</td>
  <td class='alignright' align="left">Kazimierz.J Merklejn</td>
 </tr>
<tr><td>
 VIII Mistrzostwa Ziemi Wałeckiej "Wałeckie scrabblobranie"</td>
  <td class='alignright' align="right">86</td>
  <td class='alignright' align="left">Dawid Pikul</td>
 </tr>
<tr><td>
 VIII Mistrzostwa Wrocławia<span
  style='mso-spacerun:yes'> </span></td>
  <td class='alignright' align="right">82</td>
  <td class='alignright' align="left">Bartosz Morawski</td>
 </tr>
<tr><td>
 XII Otwarte Mistrzostwa Ziemi Ostródzkiej "Blanki w Szranki"</td>
  <td class='alignright' align="right">77</td>
  <td class='alignright' align="left">Dominik Urbacki </td>
 </tr>
<tr><td>
 I Otwarte Mistrzostwa Gdańska o Puchar Rektora Uniwersytetu Gdańskiego</td>
  <td class='alignright' align="right">76</td>
  <td class='alignright' align="left">Krzysztof Mówka</td>
 </tr>
<tr><td>
 XIV Mistrzostwa
  Szczecina "Zanim zakwitną magnolie"</td>
  <td class='alignright' align="right">75</td>
  <td class='alignright' align="left">Dominik Urbacki</td>
 </tr>
<tr><td>
 XII Turniej 24h Non Stop "Le Mans"</td>
  <td class='alignright' align="right">69</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 VII Mistrzostwa Milanówka "Truskawki w Milanówku"</td>
  <td class='alignright' align="right">62</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
  VII Mistrzostwa Jaworzna (XVII Mistrzostwa Górnego Ślaska i Zagłębia)<span
  style='mso-spacerun:yes'> </span></td>
  <td class='alignright' align="right">60</td>
  <td class='alignright' align="left">Paweł Jackowski</td>
 </tr>
<tr><td>
 III Otwarte Mistrzostwa Białegostoku</td>
  <td class='alignright' align="right">57</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 III Otwarte Mistrzostwa Raciborza</td>
  <td class='alignright' align="right">57</td>
  <td class='alignright' align="left">Dariusz Puton</td>
 </tr>
<tr><td>
 I Radomszczański Pojedynek Scrabblowy o Puchar Prezydenta Radomska</td>
  <td class='alignright' align="right">57</td>
  <td class='alignright' align="left">Justyna Górka</td>
 </tr>
<tr><td>
III Mazurski Turniej o Puchar Prezydenta Miasta Ełku</td>
  <td class='alignright' align="right">56</td>
  <td class='alignright' align="left">Piotr Pietuchowski</td>
 </tr>
<tr><td>
 XI Mistrzostwa Wybrzeża</td>
  <td class='alignright' align="right">54</td>
  <td class='alignright' align="left">Rafał Lenartowski</td>
 </tr>
<tr><td>
 III Turniej o Puchar Prezydenta Miasta Łomży</td>
  <td class='alignright' align="right">54</td>
  <td class='alignright' align="left">Dawid Pikul</td>
 </tr>
<tr><td>
 VII Mistrzostwa Doliny Karpia </td>
  <td class='alignright' align="right">54</td>
  <td class='alignright' align="left">Artur Irzyk</td>
 </tr>
<tr><td>
 XV Mistrzostwa Warmii i Mazur </td>
  <td class='alignright' align="right">51</td>
  <td class='alignright' align="left">Mirosław Uglik</td>
 </tr>
<tr><td>
 VII Klubowe Mistrzostwa Polski </td>
  <td class='alignright' align="right">50</td>
  <td class='alignright' align="left">Mikrus MDK</td>
 </tr>
<tr><td>
 XVI Mistrzostwa Piastowa </td>
  <td class='alignright' align="right">49</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 XV Otwarte Mistrzostwa Nowego Dworu Mazowieckiego </td>
  <td class='alignright' align="right">48</td>
  <td class='alignright' align="left">Grzegorz Kurowski</td>
 </tr>
<tr><td>
 XVI Mistrzostwa Ziemi Kujawskiej "O Kryształowe Jajo Świąteczne" </td>
  <td class='alignright' align="right">47</td>
  <td class='alignright' align="left">Dariusz Kosz</td>
 </tr>
<tr><td>
 XVI Mistrzostwa Ziemi Słupskiej</td>
  <td class='alignright' align="right">46</td>
  <td class='alignright' align="left">Dominik Urbacki</td>
 </tr>
<tr><td>
 XVIII Puchar Polski </td>
  <td class='alignright' align="right">44</td>
  <td class='alignright' align="left">Dariusz Kosz</td>
 </tr>
<tr><td>
 Towarzysze 2012 </td>
  <td class='alignright' align="right">43</td>
  <td class='alignright' align="left">Bartosz Morawski</td>
 </tr>
<tr><td>
 XVIII Mistrzostwa Łodzi </td>
  <td class='alignright' align="right">43</td>
  <td class='alignright' align="left">Dominik Urbacki</td>
 </tr>
<tr><td>
 IX Mistrzostwa Bałtyku </td>
  <td class='alignright' align="right">38</td>
  <td class='alignright' align="left">Weronika Rudnicka</td>
 </tr>
<tr><td>
 I Otwarte Mistrzostwa Konojad</td>
  <td class='alignright' align="right">32</td>
  <td class='alignright' align="left">Irena Sołdan</td>
 </tr>
<tr><td>
 IV Mistrzostwa Gór Bystrzyckich - X Urodziny Siódemki</td>
  <td class='alignright' align="right">29</td>
  <td class='alignright' align="left">Kamil Górka</td>
 </tr>
<tr><td>
 Scrabble nad Wartą - Turniej o Puchar Gazety Radomszczańskiej</td>
  <td class='alignright' align="right">28</td>
  <td class='alignright' align="left">Tomasz Zwoliński</td>
 </tr>
<tr><td>
 "O Puchar Św. Mikołaja" - Turniej towarzyszący </td>
  <td class='alignright' align="right">18</td>
  <td class='alignright' align="left">Janusz Krupiński</td>
 </tr>
<tr><td>
 I Turniej Dzikich Drużyn (turniej towarzyszący KMP)</td>
  <td class='alignright' align="right">18</td>
  <td class='alignright' align="left">Babki z Rodzynkiem</td>
 </tr>
<tr><td>
 XII Mistrzostwa Podhala </td>
  <td class='alignright' align="right">15</td>
  <td class='alignright' align="left">Tomasz Zwoliński</td>
 </tr>
<tr><td>
 Masters 2012, Turniej o Puchar Polskiej Federacji Scrabble </td>
  <td class='alignright' align="right">12</td>
  <td class='alignright' align="left">Dariusz Puton</td>
 </tr>
<tr><td>
 Mistrzostwa Puszczy
  Piskiej</td>
  <td class='alignright' align="right">12</td>
  <td class='alignright' align="left">Tomasz Zwoliński</td>
 </tr>
  <tr>
    <td>&nbsp;</td>
    <td class='alignright' align="right">&nbsp;</td>
    <td class='alignright' align="left">&nbsp;</td>
  </tr>
<tr><td>
 

X Ogólnopolskie Mistrzostwa Szkół Kutno '2012 </td>
  <td class='alignright' align="right">80</td>
  <td class='alignright' align="left">Szkoła Podstawowa nr 11 w Ostrowie Wielkopolskim <br>Gimnazjum nr 112 im. Króla Jana III Sobieskiego w Warszawie<br>I Liceum Ogólnokształcące w Ostrowie Wielkopolskim</td>
 </tr>

<tr><td>
 V Turniej Mikołajkowy</td>
  <td class='alignright' align="right">74</td>
  <td class='alignright' align="left">Szymon Dąbrowski</td>
 </tr>
<tr><td>
II Turniej o Puchar Gimnazjum Niepublicznego nr 12 im. Lotników Amerykańskich</td>
  <td class='alignright' align="right">38</td>
  <td class='alignright' align="left">Dominik Urbacki</td>
 </tr>
<tr><td>
 I Klubowe Mistrzostwa Mazowsza</td>
  <td class='alignright' align="right">19</td>
  <td class='alignright' align="left"> Macaki I</td>
 </tr>
<tr><td>
VIII Mistrzostwa Polski Nauczycieli</td>
  <td class='alignright' align="right">14</td>
  <td class='alignright' align="left">Magdalena Kupczyńska</td>
 </tr>
 <tr><td>
 

X Letnie Igrzyska Lekarskie </td>
  <td class='alignright' align="right">10</td>
  <td class='alignright' align="left">Waldemar Czerwoniec</td>
 </tr>
<tr><td>
 

X Mensański Turniej Scrabble</td>
  <td class='alignright' align="right">8</td>
  <td class='alignright' align="left">Dariusz Jarosławski</td>
 </tr>
<tr><td>
 I Mistrzostwa Polski w Scrabble po Hiszpańsku,
eliminacje do Mistrzostw Świata w Scrabble po Hiszpańsku</td>
  <td class='alignright' align="right">4</td>
	<td class='alignright' align="left">Mariusz Sylwestrzuk</td>
 </tr>
  <tr>
    <td>&nbsp;</td>
    <td class='alignright' align="right">&nbsp;</td>
    <td class='alignright' align="left">&nbsp;</td>
  </tr>

  <tr><td class='alignright' align="left"><b>razem 2061 osób (turnieje rankingowe: 1824)</b></td>
  </tr>

 </tr>

</table>




<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>20</b> graczy, ale tylko siedmiu zanotowało więcej niż jedno zwycięstwo.
Klasyfikację medalową wygrał <b>Michał Alabrudziński</b> (5-1-0), przed Dominikiem Urbackim (4-0-1) i Tomaszem Zwolińskim  (3-3-2). To właśnie <b>Tomasz Zwoliński</b> najwięcej razy stawał na podium. <br><br>
W cyklu Grand Prix zwyciężył <b>Dawid Pikul</b>, przed Tomaszem Zwolińskim i Dominikiem Urbackim.<br><br>
Mistrzem Polski został <b>Michał Alabrudziński</b> pokonując w finale Kazimierza Jr. Merklejna. <br><br>
W 2012 roku na liście zwycięzców turniejowych pojawiły się aż trzy nowe nazwiska. Przede wszystkim trzeba wspomnieć o <b>Dominiku Urbackim</b>, który wygrał aż 4 turnieje. Oprócz niego swoje pierwsze zwycięstwo w turnieju zanotowali Artur Irzyk i Weronika Rudnicka.<br>
Dziewięciu zawodników (Grzegorz Łukasik, Sławomir Piotrowski, Krzysztof Jaworski, Andrzej Kroc, Mateusz Królikowski, Andrzej Rakoczy, Tomasz Lewiński, Adam Janicki i Krzysztof Kałuża) "zaliczyło" pierwsze miejsca na podium.<br><br>
</b>Tytuł Klubowych Mistrzów Polski zdobył <b>Mikrus MDK z Rumi.</b>.<br>
<br>
Oto <b>klasyfikacja "medalowa"</b> za rok 2012:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2012' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszech czasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa zmniejszyła się o <b>18</b> osób. Obecnie jest na niej <b>
312</b> graczy, w poczekalni - 363 (rok temu 371). <br><br>
Rok 2012 na pozycji lidera rankingu rozpoczął <b>Stanisław Rydzik</b>, jednakże nie okazał się to przychylny rok dla Staszka - nie wygrał on w nim żadnego turnieju. W marcu na fotel lidera wskoczył po raz pierwszy w karierze <b>Michał Alabrudziński</b>. Liderowanie jego trwało jednak tylko tydzień, ponieważ po turnieju w Graboszycach przejął je <b>Dariusz Kosz</b>. Sytuacja zmieniła się po Klubowych Mistrzostwach Polski, kiedy to w rankingu przewodzić zaczął (również po raz pierwszy w historii) <b>Bartosz Morawski</b>. Bartek utrzymał pozycję do Pucharu Polski, kiedy to w wyniku "szczęśliwych" odpisów starych partii, liderem został, z 37 partiami w rankingu, <b>Karol Wyrębkiewicz</b>. Kiedy Karol wreszcie wziął udział w turnieju (MP), oddał tytuł lidera Dariuszowi Koszowi, jednakże już tydzień później, po zdobyciu Mistrzostwa Polski, na czele rankingu stanął Michał Alabrudziński. To on pozostał liderem przez pozostałą część roku 2012.  <br><br>
Z czołowej dziesiątki wypadli: Stanisław Rydzik, Kamil Górka, Rafał Lenartowski, Krzysztof Obremski i Miłosz Wrzałek. Zastąpili ich: Karol Wyrębkiewicz, Dominik Urbacki, Mirosław Uglik, Maciej Czupryniak i Łukasz Tuszyński. <br>
W ciągu roku w pierwszej dziesiątce byli także: Justyna Górka (6), Dawid Adamczyk (6), Tomasz Zwoliński (7), Tomasz Ciejka (8), Marek Reda (9), Grzegorz Święcki (9), Dawid Pikul (9), Paweł Jackowski (10).<br><br>

Oto czołówka rankingu i porównanie z końcem roku 2011.
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2011</b></td><td><b>ranking</b></td><td><b>
    koniec 2012</b></td><td><b>ranking</b></td></tr>

  <tr><td>1.</td><td class="osoba"> Stanisław Rydzik </td><td>
    153,01</td><td class="osoba"> Michał Alabrudziński</td><td>
    158.84</td></tr>
  <tr><td>2.</td><td class="osoba">
    Kamil Górka</td><td>152,49</td><td class="osoba">
    Bartosz Morawski</td><td>155.00</td></tr>
  <tr><td>3.</td><td class="osoba"> Dariusz Kosz </td><td>
    151,51</td><td class="osoba">Dariusz Kosz </td><td>
    153.58</td></tr>
  <tr><td>4.</td><td class="osoba">
    Michał Alabrudziński </td><td>150,77</td><td class="osoba">
    Karol Wyrębkiewicz</td><td>152.98</td></tr>
  <tr><td>5.</td><td class="osoba">
    Bartosz Morawski </td><td>150,09</td><td class="osoba">
    Dominik Urbacki</td><td>150.59</td></tr>
  <tr><td>6.</td><td class="osoba"> Krzysztof Mówka </td><td>
    147,92</td><td class="osoba"> Krzysztof Mówka</td><td>
    150.37</td></tr>
  <tr><td>7.</td><td class="osoba">
    Rafał Lenartowski </td><td>147,62</td><td class="osoba">
   Mirosław Uglik </td><td>149.85</td></tr>
  <tr><td>8.</td><td class="osoba"> Krzysztof Obremski </td><td>
    147,48</td><td class="osoba"> Mariusz Skrobosz</td><td>
    149.26</td></tr>
  <tr><td>9.</td><td class="osoba">
    Mariusz Skrobosz </td><td>147,28</td><td class="osoba">
    Maciej Czupryniak</td><td>148.98</td></tr>
  <tr><td>10</td><td class="osoba"> Miłosz Wrzałek </td><td>
    146,78</td><td class="osoba"> Łukasz Tuszyński</td><td>
    146.52</td></tr>
</table>

<h2>Rankingi osób na miejscach</h2>
<p>Wysokość rankingu potrzebna by zająć dane miejsce na liście rankingowej
dalej rośnie. Na koniec roku 6 osób miało ranking powyżej 150, a 31 osób powyżej 140.</p>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td><td><b>koniec 2007</b></td><td><b>koniec 2008</b></td><td><b>koniec 2009</b></td><td><b>koniec 2010</b></td><td><b>koniec 2011</b></td><td><b>koniec 2012</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td><td>146,74</td><td>149,38</td><td>150,70</td><td>153,59</td><td>153,01</td><td>158.84</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td><td>140,96</td><td>141,28</td><td>142,49</td><td>144,37</td><td>146,78</td><td>146.52</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td><td>137,50</td><td>138,11</td><td>137,87</td><td>141,65</td><td>142,48</td><td>144.35</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td><td>129,53</td><td>131,09</td><td>131,83</td><td>132,09</td><td>133,72</td><td>135.64</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td><td>122,52</td><td>123,78</td><td>124,24</td><td>124,63</td><td>125,41</td><td>125.64</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td><td>116,48</td><td>116,93</td><td>117,80</td><td>118,57</td><td>118,91</td><td>119.63</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td><td>111,60</td><td>112,13</td><td>111,97</td><td>112,55</td><td>114,08</td><td>113.70</td></tr>
</table>

<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych,
poczekalnia nie jest brana pod uwagę)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2011 - ranking 2012</b></td></tr>
  <tr><td align="left">Dominik Urbacki</td><td>18.20</td><td>132.39 - 150.59</td></tr>
  <tr><td align="left">Paweł Mazurek</td><td>17.89</td><td>106.91 - 124.80</td></tr>
  <tr><td align="left">Grzegorz Łukasik</td><td>14.14</td><td>124.69 - 138.83</td></tr>
  <tr><td align="left">Leokadia Litwinowicz</td><td>12.46</td><td>115.93 - 128.39</td></tr>
  <tr><td align="left">Karol Wyrębkiewicz</td><td>12.08</td><td>140.90 - 152.98</td></tr>
  <tr><td align="left">Jacek Pietruszka</td><td>11.33</td><td>111.16 - 122.49</td></tr>
  <tr><td align="left">Tomasz Siwiński</td><td>11.10</td><td>113.83 - 124.93</td></tr>
  <tr><td align="left">Andrzej Rakoczy</td><td>10.96</td><td>127.39 - 138.35</td></tr>
  <tr><td align="left">Rafał Polak</td><td>10.80</td><td>106.18 - 116.98</td></tr>
  <tr><td align="left">Lucyna Śniadach</td><td>10.41</td><td>108.40 - 118.81</td></tr>

  </table>


<h2>Najlepsze nabytki listy rankingowej (tylko debiutanci, powroty na listę rankingową nie są brane pod uwagę)</h2>
&nbsp;<table class="klasyfikacja">
  <tr><td align="left">gracz</td><td>"wejście"</td><td>koniec roku</td></tr>
  <tr><td align="left">Michał Piotr Waszkiewicz</td><td>125.00</td><td>125.00</td></tr>
  <tr><td align="left">Dariusz Socha</td><td>121.35</td><td>119.33</td></tr>
  <tr><td align="left">Michał Mikołajczyk</td><td>117.25</td><td>117.25</td></tr>
  <tr><td align="left">Marta Świątkowska</td><td>103.30</td><td>114.44</td></tr>
  <tr><td align="left">Katarzyna Ślusarska</td><td>109.74</td><td>113.76</td></tr>
  <tr><td align="left">Juliusz Czupryniak</td><td>100.34</td><td>101.28</td></tr>
 
  </table>

<h2>Ranking z partii rozegranych w 2012 roku</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td>ranking</td><td align="center">liczba gier</td></tr>
  <tr><td align="left">Bartosz Morawski</td><td>156.73</td><td align="center">135</td></tr>
  <tr><td align="left">Michał Alabrudziński</td><td>155.48</td><td align="center">253</td></tr>
  <tr><td align="left">Dariusz Kosz</td><td>153.25</td><td align="center">154</td></tr>
  <tr><td align="left">Dominik Urbacki</td><td>151.62</td><td align="center">220</td></tr>
  <tr><td align="left">Krzysztof Mówka</td><td>151.59</td><td align="center">49</td></tr>
  <tr><td align="left">Mariusz Skrobosz</td><td>149.26</td><td align="center">199</td></tr>
  <tr><td align="left">Kazimierz Jr. Merklejn</td><td>149.26</td><td align="center">80</td></tr>
  <tr><td align="left">Maciej Czupryniak</td><td>148.98</td><td align="center">42</td></tr>
  <tr><td align="left">Mariusz Wrześniewski</td><td>148.83</td><td align="center">66</td></tr>
  <tr><td align="left">Łukasz Tuszyński</td><td>148.65</td><td align="center">79</td></tr>
</table>

&nbsp;<h2>Najwięcej rozegranych partii w 2012 r.</h2>
  Najwięcej partii rozegrali Tomasz Zwoliński i Rafał Wesołowski.
 <table class="klasyfikacja">
 <tr><td align="left">Tomasz Zwoliński</td><td>355</td></tr>
 <tr><td align="left">Rafał Wesołowski</td><td>335</td></tr>
 <tr><td align="left">Stanisław Rydzik</td><td>309</td></tr>
 <tr><td align="left">Renata Andracka</td><td>308</td></tr>
 <tr><td align="left">Irena Sołdan</td><td>280</td></tr>
 <tr><td align="left">Piotr Broda</td><td>277</td></tr>
 <tr><td align="left">Krzysztof Sporczyk</td><td>255</td></tr>
 <tr><td align="left">Grzegorz Kurowski</td><td>254</td></tr>
 <tr><td align="left">Michał Alabrudziński</td><td>253</td></tr>
 <tr><td align="left">Kamil Górka</td><td>240</td></tr>
  </table>


<h2>Statystyka</h2>
<table class="klasyfikacja">
<br>

<tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td><td><b>2007</b></td><td><b>2008</b></td><td><b>2009</b></td><td><b>2010</b></td> <td><b>2011</b></td><td><b>2012</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td> <td>32</td><td>30</td><td>34</td><td>32</td><td>34</td><td>36</td></tr>
  <tr><td>Gracze</td><td>399</td><td>505</td><td>511</td><td>595</td><td>574</td> <td>520</td><td>524</td><td>525</td><td>540</td><td>515</td><td>495</td></tr>
  <tr><td>Gry rankingowe</td><td>9 888</td><td>11 544</td><td>11 447</td><td>14 496</td><td>12 667</td><td>12 501</td><td>12 190</td><td>12 201</td><td>12 620</td><td>13 075</td><td>11 672</td></tr>
  <tr><td>Średnia gier na gracza</td><td>24,8</td><td>22,9</td><td>22,4</td><td>24,4</td><td>22,1</td><td>24,0</td><td>23,3</td> <td>23,2</td><td>23,4</td><td>25,4</td><td>23,6</td></tr>
  <tr><td>Uczestnicy<br>(turnieje rankingowe)</td><td>1914</td><td>2016</td><td>2002</td><td>2602</td><td>2318</td><td>2129</td> <td>2085</td><td>2067</td><td>2111</td><td>2086</td><td>1826</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>74</td><td>70</td><td>80</td><td>79</td><td>72</td><td>67</td><td>70</td><td>61</td> <td>66</td><td>61</td><td>50,7</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td><td>3</td><td>2</td><td>2</td> <td>3</td><td>4</td><td>4</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td><td>9</td> <td>13</td><td>11</td><td>13</td><td>17</td><td>10</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td><td>58</td> <td>56</td><td>64</td><td>65</td><td>68</td><td>56</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td><td>92</td> <td>88</td><td>77</td><td>103</td><td>99</td><td>49</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td><td>346</td> <td>366</td><td>372</td><td>437</td><td>416</td><td>346</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td><td>368</td> <td>343</td><td>345</td><td>331</td><td>330</td><td>312</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td><td>337</td> <td>337</td><td>340</td><td>376</td><td>371</td><td>363</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td><td>705</td> <td>681</td><td>685</td><td>707</td><td>701</td><td>675</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td><td>116</td> <td>131</td><td>102</td><td>146</td><td>116</td><td>108</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td><td>Wałcz - 146</td><td>Ostróda - 132</td><td>Wałcz - 125</td><td>MP - 126</td><td>Kraków - 113</td><td>MP - 111</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td><td>1</td> <td>0</td><td>5</td><td>3</td><td>4</td><td>3</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td><td>6</td> <td>8</td><td>8</td><td>9</td><td>0</td><td>3</td></tr>
</table>
</div>


<div id='tabs-2011'>

<h2 id="turnieje">Turnieje</h2>
W 2011 r. odbyło się <b>35 </b> turniejów zaliczanych do rankingu PFS. Wzięło w nich udział <b>
515</b> osób.
W tym roku najwięcej uczestników przyjechało na
<a href="http://www.pfs.org.pl/turniej.php?id=555">XV Mistrzostwa Krakowa</a> - 113.
2 osoby mniej odwiedziły Wałcz przy okazji
<a href="http://www.pfs.org.pl/turniej.php?id=566">VII Mistrzostw Wałcza
&quot;Wałeckie Scrabblobranie&quot;</a>. Na trzecim miejscu pod względem frekwencji znalazł się również Wałcz - 101 osób wzięło udział w eliminacjach do <a href="http://www.pfs.org.pl/turniej.php?id=557">XIX Mistrzostw Polski</a>. <br>
Po raz pierwszy rozegrano turniej na promie <a href="http://www.pfs.org.pl/turniej.php?id=567">
Via Baltica</a>. Na turniejowej mapie Polski pojawiły się dwie nowe miejscowości: <a href="http://www.pfs.org.pl/turniej.php?id=595">Krzyże</a> i <a href="http://www.pfs.org.pl/turniej.php?id=598">Wicie</a>, a po wielu latach powrócił na nią <a href="http://www.pfs.org.pl/turniej.php?id=599">Białystok</a>.<br>
<a href="http://www.pfs.org.pl/relacja.php?id=553">VI Klubowe Mistrzostwa Polski</a>
odbyły się w Ostródzie. Nauczyciele walczyli jak co roku w
<a href="http://www.pfs.org.pl/turniej.php?id=602">Wałczu</a>, gimnazjaliści w
<a href="http://www.pfs.org.pl/turniej.php?id=573">Kutnie</a> a uczniowie Szkół
Ponadgimnazjalnych w <a href="http://www.pfs.org.pl/turniej.php?id=586">Szczecinie</a>.<br>
W <a href="http://www.pfs.org.pl/turniej.php?id=565">IV Turnieju Mikołajkowym</a> wzięło udział
74 młodych scrabblistów ze szkolnych kółek.<p>Liczba uczestników turniejów
przedstawia się następująco:
</p>

<table class="zapisy">

<tr><td><p align="center">Turniej</td>
    <td class='alignright' align="right">
    <p align="center">Liczba<br>
    uczestników</td>
    <td class='alignright' align="left">
    <p align="center">Zwycięzca</td></tr>


</td>
<tr><td>
 XV Otwarte Mistrzostwa
  Krakowa w&nbsp;Scrabble</td>
  <td class='alignright' align="right">113</td>
  <td class='alignright' align="left">Dariusz Kosz</td>
 </tr>

<tr><td>
 VII Mistrzostwa Ziemi
  Wałeckiej w&nbsp;Scrabble „Wałeckie scrabblobranie”</td>
  <td class='alignright' align="right">111</td>
  <td class='alignright' align="left">Łukasz Tuszyński</td>
 </tr>
<tr><td>
 XIX Mistrzostwa Polski</td>
  <td class='alignright' align="right">101</td>
  <td class='alignright' align="left">Mirosław Uglik</td>
 </tr>
<tr><td>
XI Otwarte Mistrzostwa Ziemi Ostródzkiej „Blanki w&nbsp;Szranki”<span
  style='mso-spacerun:yes'> </span></td>
  <td class='alignright' align="right">98</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 XV Mistrzostwa Warszawy</td>
  <td class='alignright' align="right">90</td>
  <td class='alignright' align="left">Krzysztof Mówka<br>Jan Mrozowski<br>Dominik Urbacki</td>
 </tr>
<tr><td>
 VI Mistrzostwa Milanówka
  „Truskawki w&nbsp;Milanówku”</td>
  <td class='alignright' align="right">80</td>
  <td class='alignright' align="left">Paweł Jackowski</td>
 </tr>
<tr><td>
 XIII Mistrzostwa
  Szczecina „Zanim zakwitną magnolie”</td>
  <td class='alignright' align="right">80</td>
  <td class='alignright' align="left">Mariusz Skrobosz</td>
 </tr>
<tr><td>
 VII Mistrzostwa
  Wrocławia w&nbsp;Scrabble</td>
  <td class='alignright' align="right">78</td>
  <td class='alignright' align="left">Dariusz Kosz</td>
 </tr>
<tr><td>
 VI Mistrzostwa Jaworzna</td>
  <td class='alignright' align="right">72</td>
  <td class='alignright' align="left">Stanisław Rydzik</td>
 </tr>
<tr><td>
  II Mazurski Turniej o&nbsp;Puchar Prezydenta Miasta Ełku<span
  style='mso-spacerun:yes'> </span></td>
  <td class='alignright' align="right">71</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 Turniej Towarzyszący XIX
  Mistrzostw Polski</td>
  <td class='alignright' align="right">69</td>
  <td class='alignright' align="left">Bartosz Morawski</td>
 </tr>
<tr><td>
 XVII Puchar Polski</td>
  <td class='alignright' align="right">64</td>
  <td class='alignright' align="left">Kamil Górka</td>
 </tr>
<tr><td>
 IV Walentynki
  w&nbsp;Sulęcinie</td>
  <td class='alignright' align="right">64</td>
  <td class='alignright' align="left">Karol Wyrębkiewicz</td>
 </tr>
<tr><td>
 X Mistrzostwa Wybrzeża</td>
  <td class='alignright' align="right">62</td>
  <td class='alignright' align="left">Justyna Górka</td>
 </tr>
<tr><td>
 VI Mistrzostwa Doliny
  Karpia</td>
  <td class='alignright' align="right">61</td>
  <td class='alignright' align="left">Kamil Górka</td>
 </tr>
<tr><td>
 XI Turniej 24h Non Stop
  „Le Mans”</td>
  <td class='alignright' align="right">58</td>
  <td class='alignright' align="left">Kamil Górka</td>
 </tr>
<tr><td>
 II Otwarte Mistrzostwa
  Raciborza w&nbsp;Scrabble</td>
  <td class='alignright' align="right">57</td>
  <td class='alignright' align="left">Stanisław Rydzik</td>
 </tr>
<tr><td>
 II Otwarte Mistrzostwa
  Białegostoku o&nbsp;Puchar Przewodniczącego Rady Miejskiej</td>
  <td class='alignright' align="right">56</td>
  <td class='alignright' align="left">Grzegorz Święcki</td>
 </tr>
<tr><td>
 XIV Otwarte Mistrzostwa
  Nowego Dworu Mazowieckiego</td>
  <td class='alignright' align="right">55</td>
  <td class='alignright' align="left">Mariusz Skrobosz</td>
 </tr>
<tr><td>
 XV Mistrzostwa Ziemi
  Słupskiej w&nbsp;Scrabble</td>
  <td class='alignright' align="right">54</td>
  <td class='alignright' align="left">Dariusz Puton</td>
 </tr>
<tr><td>
 XV Mistrzostwa Ziemi
  Kujawskiej „O Kryształowe Jajo Świąteczne”</td>
  <td class='alignright' align="right">54</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 III Mistrzostwa Puław</td>
  <td class='alignright' align="right">53</td>
  <td class='alignright' align="left">Robert Kamiński</td>
 </tr>
<tr><td>
 VIII Mistrzostwa
  Bydgoszczy</td>
  <td class='alignright' align="right">52</td>
  <td class='alignright' align="left">Miłosz Wrzałek</td>
 </tr>
<tr><td>
 XV Mistrzostwa Piastowa</td>
  <td class='alignright' align="right">48</td>
  <td class='alignright' align="left">Mariusz Skrobosz</td>
 </tr>
<tr><td>
 XIV Mistrzostwa Warmii
  i&nbsp;Mazur</td>
  <td class='alignright' align="right">43</td>
  <td class='alignright' align="left">Rafał Lenartowski</td>
 </tr>
<tr><td>
 XII turniej
  o&nbsp;Puchar Dyrektora MDK w&nbsp;Rumi</td>
  <td class='alignright' align="right">42</td>
  <td class='alignright' align="left">Miłosz Wrzałek</td>
 </tr>
<tr><td>
 VIII Mistrzostwa Bałtyku</td>
  <td class='alignright' align="right">42</td>
  <td class='alignright' align="left">Miłosz Wrzałek</td>
 </tr>
<tr><td>
 XI Mistrzostwa Podhala</td>
  <td class='alignright' align="right">38</td>
  <td class='alignright' align="left">Stanisław Rydzik</td>
 </tr>
<tr><td>
 IX Urodziny Siódemki</td>
  <td class='alignright' align="right">38</td>
  <td class='alignright' align="left">Tomasz Ciejka</td>
 </tr>
<tr><td>
 VI Klubowe Mistrzostwa
  Polski</td>
  <td class='alignright' align="right">37</td>
  <td class='alignright' align="left">Siódemka Wrocław</td>
 </tr>
<tr><td>
 XVII Mistrzostwa Łodzi</td>
  <td class='alignright' align="right">33</td>
  <td class='alignright' align="left">Mariusz Skrobosz</td>
 </tr>
<tr><td>
 I Turniej Via Baltica</td>
  <td class='alignright' align="right">30</td>
  <td class='alignright' align="left">Marek Reda</td>
 </tr>
<tr><td>
 I Turniej Charytatywny
  „Scrabble w&nbsp;szkole”</td>
  <td class='alignright' align="right">27</td>
  <td class='alignright' align="left">Łukasz Tuszyński</td>
 </tr>
<tr><td>
 Turniej Mistrzów „Primus
  Inter Pares 2011”</td>
  <td class='alignright' align="right">21</td>
  <td class='alignright' align="left">Michał Alabrudziński</td>
 </tr>
<tr><td>
 Mistrzostwa Puszczy
  Piskiej — V Maraton Scrabblowy</td>
  <td class='alignright' align="right">18</td>
  <td class='alignright' align="left">Stanisław Rydzik</td>
 </tr>
  <tr>
    <td>&nbsp;</td>
    <td class='alignright' align="right">&nbsp;</td>
    <td class='alignright' align="left">&nbsp;</td>
  </tr>

<tr><td>
 IV Turniej Mikołajkowy</td>
  <td class='alignright' align="right">74</td>
  <td class='alignright' align="left">Paweł Mazurek</td>
 </tr>
<tr><td>
 IX Mistrzostwa Polski
  Gimnazjów w Scrabble</td>
  <td class='alignright' align="right">42</td>
  <td class='alignright' align="left">Wojciech Stawicki<br>Hubert Smoliński</td>
 </tr>
<tr><td>
 VIII Mistrzostwa Polski
  Szkół Ponadgimnazjalnych w Scrabble</td>
  <td class='alignright' align="right">30</td>
  <td class='alignright' align="left">Natalia Połomska<br>Paula Szałata</td>
 </tr>
<tr><td>
 I Turniej motoSCRABBLE</td>
  <td class='alignright' align="right">28</td>
  <td class='alignright' align="left">Zuzia Bieńko</td>
 </tr>
<tr><td>
Turniej o Puchar Gim. Niepublicznego nr 12 im. Lotników Amerykańskich</td>
  <td class='alignright' align="right">24</td>
  <td class='alignright' align="left">Dominik Urbacki</td>
 </tr>
<tr><td>
 Liga morska</td>
  <td class='alignright' align="right">18</td>
  <td class='alignright' align="left">Waldemar Czerwoniec</td>
 </tr>
<tr><td>
 I Polonijny Turniej
  Scrabble</td>
  <td class='alignright' align="right">15</td>
	<td class='alignright' align="left">Dominika Malinowska</td>
 </tr>
<tr><td>
 VII Mistrzostwa Polski
  Nauczycieli w Scrabble</td>
  <td class='alignright' align="right">13</td>
  <td class='alignright' align="left">Piotr Broda</td>
 </tr>
<tr><td>
 IX Mensański Turniej
  Scrabble</td>
  <td class='alignright' align="right">10</td>
  <td class='alignright' align="left">Tomasz Łapka</td>
 </tr>
<tr><td>
 IX Letnie Igrzyska
  Lekarskie</td>
  <td class='alignright' align="right">9</td>
  <td class='alignright' align="left">Waldemar Czerwoniec</td>
 </tr>
<tr><td>
 V Mistrzostwa Polski w
  Scrabble po angielsku</td>
  <td class='alignright' align="right">6</td>
  <td class='alignright' align="left">Wojciech Usakiewicz</td>
 </tr>
  <tr>
    <td>&nbsp;</td>
    <td class='alignright' align="right">&nbsp;</td>
    <td class='alignright' align="left">&nbsp;</td>
  </tr>

  <tr><td class='alignright' align="left"><b>razem 2 339 osób (turnieje rankingowe: 2 070)</b></td>
  </tr>

 </tr>

</table>




<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>18</b> graczy, ale tylko siedmiu zanotowało więcej niż jedno zwycięstwo.
Klasyfikację medalową wygrał <b>Stanisław Rydzik</b> (4-3-2), przed Michałem Alabrudzińskim (4-2-1) i Mariuszem Skroboszem (4-2-0). Najwięcej razy na podium (dziewięć) stawał <b>Stanisław Rydzik</b>.<br><br>
W cyklu Grand Prix zwyciężył <b>Michał Alabrudziński</b>, przed Stanisławem Rydzikiem i Krzysztofem Obremskim.<br><br>
Mistrzem Polski został <b>Mirosław Uglik</b> pokonując w finale Stanisława Rydzika. <br><br>
W 2011 roku po raz pierwszy od wielu lat na liście zwycięzców turniejowych nie przybyło żadne nowe nazwisko.<br>
Dwóch zawodników (Jan Mrozowski i Marek Herzig) "zaliczyli" pierwsze miejsca na podium.<br><br>
</b>Tytuł Klubowych Mistrzów Polski zdobyła <b>Siódemka Wrocław.</b>.<br>
<br>
Oto <b>klasyfikacja "medalowa"</b> za rok 2011:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2011' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa zmniejszyła się o <b>jedną</b> osobę. Obecnie jest na niej <b>
330</b> graczy, w poczekalni - 371 (rok temu 376). <br><br>
Rok 2011 na pozycji lidera rankingu rozpoczął <b>Dariusz Kosz</b> i utrzymywał pierwszą pozycję aż do połowy listopada. Po eliminacjach Mistrzostw Polski wyprzedził go <b>Stanisław Rydzik</b>, który utrzymał "fotel lidera" do końca roku.<br><br>
Z czołowej dziesiątki wypadli: Kazimierz jr. Merklejn, Mariusz Wrześniewski, Dawid Pikul, Michał Makowski i Tomasz Zwoliński. Zastąpili ich: Michał Alabrudziński, Krzysztof Mówka, Rafał Lenartowski, Krzysztof Obremski i Miłosz Wrzałek. <br>
W ciągu roku w pierwszej dziesiątce byli także: Rafał Dąbrowski (7), Mirosław Uglik (7), Wojciech Zemło (8), Grzegorz Kurowski (9), Dawid Adamczyk (9), Jan Kozłowski (10) i Mateusz Żbikowski (10).<br><br>

Oto czołówka rankingu i porównanie z końcem roku 2010, a także wykresy
przedstawiające zmiany w czołówce (pod względem miejsc i rankingu).
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2010</b></td><td><b>ranking</b></td><td><b>
    koniec 2011</b></td><td><b>ranking</b></td></tr>

  <tr><td>1.</td><td class="osoba">Dariusz Kosz</td><td>
    153,59</td><td class="osoba">Stanisław Rydzik</td><td>
    153,01</td></tr>
  <tr><td>2.</td><td class="osoba">
    Stanisław Rydzik</td><td>151,07</td><td class="osoba">
    Kamil Górka</td><td>152,49</td></tr>
  <tr><td>3.</td><td class="osoba">Kamil Górka</td><td>
    148,10</td><td class="osoba">Dariusz Kosz</td><td>
    151,51</td></tr>
  <tr><td>4.</td><td class="osoba">
    Kazimierz jr. Merklejn</td><td>147,97</td><td class="osoba">
    Michał Alabrudziński</td><td>150,77</td></tr>
  <tr><td>5.</td><td class="osoba">
    Mariusz Wrześniewski</td><td>145,95</td><td class="osoba">
    Bartosz Morawski</td><td>150,09</td></tr>
  <tr><td>6.</td><td class="osoba">Bartosz Morawski</td><td>
    145,61</td><td class="osoba">Krzysztof Mówka</td><td>
    147,92</td></tr>
  <tr><td>7.</td><td class="osoba">
    Dawid Pikul</td><td>145,09</td><td class="osoba">
    Rafał Lenartowski</td><td>147,62</td></tr>
  <tr><td>8.</td><td class="osoba">Michał Makowski</td><td>
    144,77</td><td class="osoba">Krzysztof Obremski</td><td>
    147,48</td></tr>
  <tr><td>9.</td><td class="osoba">
    Tomasz Zwoliński</td><td>144,73</td><td class="osoba">
    Mariusz Skrobosz</td><td>147,28</td></tr>
  <tr><td>10</td><td class="osoba">Mariusz Skrobosz</td><td>
    144,37</td><td class="osoba">Miłosz Wrzałek</td><td>
    146,78</td></tr>
</table>
<img border="0" src="rozne/rank2011m.jpg">

<img border="0" src="rozne/rank2011r.jpg">

<h2>Rankingi osób na miejscach</h2>
<p>Wysokość rankingu potrzebna by zająć dane miejsce na liście rankingowej
systematycznie rośnie. Na koniec roku 5 osób miało ranking powyżej 150, a 27 osób powyżej 140.</p>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td><td><b>koniec 2007</b></td><td><b>koniec 2008</b></td><td><b>koniec 2009</b></td><td><b>koniec 2010</b></td><td><b>koniec 2011</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td><td>146,74</td><td>149,38</td><td>150,70</td><td>153,59</td><td>153,01</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td><td>140,96</td><td>141,28</td><td>142,49</td><td>144,37</td><td>146,78</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td><td>137,50</td><td>138,11</td><td>137,87</td><td>141,65</td><td>142,48</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td><td>129,53</td><td>131,09</td><td>131,83</td><td>132,09</td><td>133,72</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td><td>122,52</td><td>123,78</td><td>124,24</td><td>124,63</td><td>125,41</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td><td>116,48</td><td>116,93</td><td>117,80</td><td>118,57</td><td>118,91</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td><td>111,60</td><td>112,13</td><td>111,97</td><td>112,55</td><td>114,08</td></tr>
</table>

<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych,
poczekalnia nie jest brana pod uwagę)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2010 - ranking 2011</b></td></tr>
  <tr><td align="left">Michał Dąbrowski</td><td>19,48</td><td>71,08 - 90,56</td></tr>
  <tr><td align="left">Michał Ozimiński</td><td>15,67</td><td>121,76 - 137,43</td></tr>
  <tr><td align="left">Bernadetta Kudlińska</td><td>15,17</td><td>61,45 - 76,62</td></tr>
  <tr><td align="left">Jan Mrozowski</td><td>14,90</td><td>123,24 - 138,14</td></tr>
  <tr><td align="left">Marianna Kozłowska</td><td>14,29</td><td>100,96 - 115,25</td></tr>
  <tr><td align="left">Krzysztof Gibas</td><td>13,00</td><td>108,83 - 121,83</td></tr>
  <tr><td align="left">Kamil Kister</td><td>12,71</td><td>126,43 - 139,14</td></tr>
  <tr><td align="left">Piotr Broda</td><td>12,07</td><td>114,84 - 126,91</td></tr>
  <tr><td align="left">Szymon Dąbrowski</td><td>11,78</td><td>94,63 - 106,41</td></tr>
  <tr><td align="left">Mikołaj Fidziński</td><td>11,61</td><td>112,94 - 124,55</td></tr>

  </table>


<h2>Najlepsze nabytki listy rankingowej (tylko debiutanci, powroty na listę rankingową nie są brane pod uwagę)</h2>
&nbsp;<table class="klasyfikacja">
  <tr><td align="left">gracz</td><td>"wejście"</td><td>koniec roku</td></tr>
  <tr><td align="left">Dominik Urbacki</td><td>138,08</td><td>132,39</td></tr>
  <tr><td align="left">Bronisław Parada</td><td>126,24</td><td>126,24</td></tr>
  <tr><td align="left">Paweł Zaręba</td><td>120,76</td><td>120,76</td></tr>
  <tr><td align="left">Grzegorz Łukasik</td><td>120,39</td><td>124,69</td></tr>
  <tr><td align="left">Paulina Mech</td><td>117,46</td><td>118,80</td></tr>
  <tr><td align="left">Krzysztof Bonda</td><td>116,47</td><td>116,81</td></tr>
  <tr><td align="left">Ewa Radzikowska</td><td>115,49</td><td>115,49</td></tr>
  </table>

<h2>Ranking z partii rozegranych w 2011 roku</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td>ranking</td><td align="center">liczba gier</td></tr>
  <tr><td align="left">Karol Wyrębkiewicz</td><td>156,84</td><td align="center">37</td></tr>
  <tr><td align="left">Dariusz Kosz</td><td>155,78</td><td align="center">63</td></tr>
  <tr><td align="left">Kamil Górka</td><td>150,75</td><td align="center">271</td></tr>
  <tr><td align="left">Bogusław Szyszka</td><td>150,34</td><td align="center">71</td></tr>
  <tr><td align="left">Michał Alabrudziński</td><td>149,85</td><td align="center">265</td></tr>
  <tr><td align="left">Bartosz Morawski</td><td>149,83</td><td align="center">112</td></tr>
  <tr><td align="left">Krzysztof Mówka</td><td>149,56</td><td align="center">75</td></tr>
  <tr><td align="left">Rafał Lenartowski</td><td>148,90</td><td align="center">171</td></tr>
  <tr><td align="left">Mariusz Skrobosz</td><td>148,23</td><td align="center">258</td></tr>
  <tr><td align="left">Krzysztof Obremski</td><td>147,82</td><td align="center">185</td></tr>
</table>

&nbsp;<h2>Najwięcej rozegranych partii w 2011 r.</h2>
  Najwięcej partii rozegrali tradycyjnie Stanisław Rydzik i Tomasz Zwoliński
 <table class="klasyfikacja">
 <tr><td align="left">Stanisław Rydzik</td><td>389</td></tr>
 <tr><td align="left">Tomasz Zwoliński</td><td>382</td></tr>
 <tr><td align="left">Irena Sołdan</td><td>352</td></tr>
 <tr><td align="left">Grzegorz Kurowski</td><td>307</td></tr>
 <tr><td align="left">Mirosław Uglik</td><td>296</td></tr>
 <tr><td align="left">Krzysztof Sporczyk</td><td>273</td></tr>
 <tr><td align="left">Kamil Górka</td><td>271</td></tr>
 <tr><td align="left">Piotr Broda</td><td>269</td></tr>
 <tr><td align="left">Michał Alabrudziński</td><td>265</td></tr>
 <tr><td align="left">Mariusz Skrobosz</td><td>258</td></tr>
  </table>


<h2>Statystyka</h2>
<table class="klasyfikacja">
(<i>Poprawiono wcześniej błędnie obliczaną liczbę gier i inne drobne nieścisłości</i>)<br>

<tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td><td><b>2007</b></td><td><b>2008</b></td><td><b>2009</b></td><td><b>2010</b></td> <td><b>2011</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td> <td>32</td><td>30</td><td>34</td><td>32</td><td>34</td></tr>
  <tr><td>Gracze</td><td>399</td><td>505</td><td>511</td><td>595</td><td>574</td> <td>520</td><td>524</td><td>525</td><td>540</td><td>515</td></tr>
  <tr><td>Gry rankingowe</td><td>9 888</td><td>11 544</td><td>11 447</td><td>14 496</td><td>12 667</td><td>12 501</td><td>12 190</td><td>12 201</td><td>12 620</td><td>13 075</td></tr>
  <tr><td>Średnia gier na gracza</td><td>24,8</td><td>22,9</td><td>22,4</td><td>24,4</td><td>22,1</td><td>24,0</td><td>23,3</td> <td>23,2</td><td>23,4</td><td>25,4</td></tr>
  <tr><td>Uczestnicy<br>(turnieje rankingowe)</td><td>1914</td><td>2016</td><td>2002</td><td>2602</td><td>2318</td><td>2129</td> <td>2085</td><td>2067</td><td>2111</td><td>2070</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>74</td><td>70</td><td>80</td><td>79</td><td>72</td><td>67</td><td>70</td><td>61</td> <td>66</td><td>61</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td><td>3</td><td>2</td><td>2</td> <td>3</td><td>4</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td><td>9</td> <td>13</td><td>11</td><td>13</td><td>17</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td><td>58</td> <td>56</td><td>64</td><td>65</td><td>68</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td><td>92</td> <td>88</td><td>77</td><td>103</td><td>99</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td><td>346</td> <td>366</td><td>372</td><td>437</td><td>416</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td><td>368</td> <td>343</td><td>345</td><td>331</td><td>330</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td><td>337</td> <td>337</td><td>340</td><td>376</td><td>371</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td><td>705</td> <td>681</td><td>685</td><td>707</td><td>701</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td><td>116</td> <td>131</td><td>102</td><td>146</td><td>116</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td><td>Wałcz - 146</td><td>Ostróda - 132</td><td>Wałcz - 125</td><td>MP - 126</td><td>Kraków - 113</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td><td>1</td> <td>0</td><td>5</td><td>3</td><td>4</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td><td>6</td> <td>8</td><td>8</td><td>9</td><td>0</td></tr>
</table>
</div>






<div id='tabs-2010'>

<h2 id="turnieje">Turnieje</h2>
W 2010 r. odbyły się <b>32 </b> turnieje zaliczane do rankingu PFS. Wzięło w nich udział <b>
541</b> osób.
W tym roku najwięcej uczestników przyjechało na
<a href="http://www.pfs.org.pl/turniej.php?id=526">XVIII Mistrzostwa Polski</a> - 126.
2 osoby mniej odwiedziły to samo miejsce, ale przy okazji innego turnieju -
<a href="http://www.pfs.org.pl/turniej.php?id=507">VI Mistrzostw Wałcza
&quot;Wałeckie Scrabblobranie&quot;</a>. Na trzecim miejscu pod względem frekwencji
znalazł się ponownie <a href="http://www.pfs.org.pl/turniej.php?id=522">Kraków</a>. <br>
Po raz pierwszy rozegrano <a href="http://www.pfs.org.pl/turniej.php?id=535">
Mistrzostwa Raciborza</a>, <a href="http://www.pfs.org.pl/turniej.php?id=545">
Pszczyny</a> i <a href="http://www.pfs.org.pl/turniej.php?id=538">Ełku</a>.<br>
<a href="http://www.pfs.org.pl/relacja.php?id=478">V Klubowe Mistrzostwa Polski</a>
odbyły się ponownie w Burzeninie. Nauczyciele walczyli w
<a href="http://www.pfs.org.pl/turniej.php?id=506">Wałczu</a>, gimnazjaliści w
<a href="http://www.pfs.org.pl/turniej.php?id=530">Kutnie</a> a uczniowie Szkół
Ponadgimnazjalnych w <a href="http://www.pfs.org.pl/turniej.php?id=506">Zabrzu</a>.<br>
W <a href="http://www.pfs.org.pl/turniej.php?id=504">III Turnieju Mikołajkowym</a> wzięło udział
70
młodych scrabblistów z 13 szkolnych kółek.<p>Liczba uczestników turniejów
przedstawia się następująco:
</p>

<table class="zapisy" width="621">
  <tr><td width="366">Turniej</td>
    <td class='alignright' align="right" width="74">
    <p align="center">Liczba<br>
    uczestników</td>
    <td class='alignright' align="left" width="167">
    <p align="center">Zwycięzca</td></tr>
  <tr>
    <td width="366">XVIII Mistrzostwa Polski</td>
    <td class='alignright' align="right" width="74">126</td>
    <td class='alignright' align="left" width="167">Bartosz Morawski</td>
  </tr>
  <tr><td width="366">VI Mistrzostwa Wałcza</td>
    <td class='alignright' align="right" width="74">124</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td></tr>
  <tr><td width="366">XIV Mistrzostwa Krakowa</td>
    <td class='alignright' align="right" width="74">118</td>
    <td class='alignright' align="left" width="167">Dawid Adamczyk</td></tr>
  <tr>
    <td width="366">XIV Mistrzostwa Warszawy</td>
    <td class='alignright' align="right" width="74">97</td>
    <td class='alignright' align="left" width="167">Michał Alabrudziński</td>
  </tr>
  <tr>
    <td width="366">VI Mistrzostwa Wrocławia</td>
    <td class='alignright' align="right" width="74">82</td>
    <td class='alignright' align="left" width="167">Kazimierz Merklejn jr</td>
  </tr>
  <tr>
    <td width="366">X Mistrzostwa Ziemi Ostródzkiej &quot;Blanki w szranki&quot;</td>
    <td class='alignright' align="right" width="74">81</td>
    <td class='alignright' align="left" width="167">Mariusz Skrobosz</td>
  </tr>
  <tr>
    <td width="366">V Mistrzostwa Doliny Karpia - Graboszyce</td>
    <td class='alignright' align="right" width="74">81</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td>
  </tr>
  <tr>
    <td width="366">XII Mistrzostwa Szczecina &quot;Zanim zakwitną magnolie&quot;</td>
    <td class='alignright' align="right" width="74">75</td>
    <td class='alignright' align="left" width="167">Tomasz Zwoliński</td>
  </tr>
  <tr>
    <td width="366">XIV Mistrzostwa Ziemi Kujawskiej</td>
    <td class='alignright' align="right" width="74">72</td>
    <td class='alignright' align="left" width="167">Dawid Pikul</td>
  </tr>
  <tr>
    <td width="366">XVI Puchar Polski</td>
    <td class='alignright' align="right" width="74">71</td>
    <td class='alignright' align="left" width="167">Kamil Górka</td>
  </tr>
  <tr>
    <td width="366">II Mistrzostwa Puław</td>
    <td class='alignright' align="right" width="74">68</td>
    <td class='alignright' align="left" width="167">Kamil Górka</td>
  </tr>
  <tr>
    <td width="366">I Mistrzostwa Raciborza</td>
    <td class='alignright' align="right" width="74">66</td>
    <td class='alignright' align="left" width="167">Marek Syczuk</td>
  </tr>
  <tr>
    <td width="366">V Mistrzostwa Jaworzna</td>
    <td class='alignright' align="right" width="74">65</td>
    <td class='alignright' align="left" width="167">Tomasz Ciejka</td>
  </tr>
  <tr>
    <td width="366">IX Mistrzostwa Wybrzeża</td>
    <td class='alignright' align="right" width="74">64</td>
    <td class='alignright' align="left" width="167">Kazimierz Merklejn jr</td>
  </tr>
  <tr>
    <td width="366">X Le Mans</td>
    <td class='alignright' align="right" width="74">63</td>
    <td class='alignright' align="left" width="167">Mirosław Uglik</td>
  </tr>
  <tr>
    <td width="366">XV Mistrzostwa Górnego Śląska i Zagłębia</td>
    <td class='alignright' align="right" width="74">62</td>
    <td class='alignright' align="left" width="167">Kamil Górka</td>
  </tr>
  <tr>
    <td width="366">V Mistrzostwa Milanówka &quot;Truskawki w Milanówku&quot;</td>
    <td class='alignright' align="right" width="74">57</td>
    <td class='alignright' align="left" width="167">Robert Kamiński</td>
  </tr>
  <tr>
    <td width="366">Turniej z okazji XIII-lecia PFS</td>
    <td class='alignright' align="right" width="74">56</td>
    <td class='alignright' align="left" width="167">Jakub Szymczak</td>
  </tr>
  <tr>
    <td width="366">XIV Mistrzostwa Ziemi Słupskiej</td>
    <td class='alignright' align="right" width="74">52</td>
    <td class='alignright' align="left" width="167">Kamil Górka</td>
  </tr>
  <tr>
    <td width="366">XVI Mistrzostwa Łodzi</td>
    <td class='alignright' align="right" width="74">48</td>
    <td class='alignright' align="left" width="167">Grzegorz Kurowski</td>
  </tr>
  <tr>
    <td width="366">II Mistrzostwa Łomży</td>
    <td class='alignright' align="right" width="74">48</td>
    <td class='alignright' align="left" width="167">Piotr Domański</td>
  </tr>
  <tr>
    <td width="366">III Mistrzostwa Sulęcina</td>
    <td class='alignright' align="right" width="74">47</td>
    <td class='alignright' align="left" width="167">Kazimierz Merklejn jr</td>
  </tr>
  <tr>
    <td width="366">VII Mistrzostwa Bałtyku</td>
    <td class='alignright' align="right" width="74">47</td>
    <td class='alignright' align="left" width="167">Tomasz Zwoliński</td>
  </tr>
  <tr>
    <td width="366">XIII Mistrzostwa Nowego Dworu Mazowieckiego</td>
    <td class='alignright' align="right" width="74">45</td>
    <td class='alignright' align="left" width="167">Michał Alabrudziński</td>
  </tr>
  <tr>
    <td width="366">I Turniej o Puchar Burmistrza Pszczyny</td>
    <td class='alignright' align="right" width="74">45</td>
    <td class='alignright' align="left" width="167">Justyna Górka</td>
  </tr>
  <tr>
    <td width="366">XIII Mistrzostwa Warmi i Mazur</td>
    <td class='alignright' align="right" width="74">44</td>
    <td class='alignright' align="left" width="167">Rafał Lenartowski</td>
  </tr>
  <tr>
    <td width="366">Mazurski Turniej o Puchar Prezydenta Miasta Ełku</td>
    <td class='alignright' align="right" width="74">40</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td>
  </tr>
  <tr>
    <td width="366">V Klubowe Mistrzostwa Polski</td>
    <td class='alignright' align="right" width="74">35</td>
    <td class='alignright' align="left" width="167">MDK Mikrus</td>
  </tr>
  <tr>
    <td width="366">XIV Mistrzostwa Piastowa</td>
    <td class='alignright' align="right" width="74">35</td>
    <td class='alignright' align="left" width="167">Michał Makowski</td>
  </tr>
  <tr>
    <td width="366">II Mistrzostwa Gór Bystrzyckich - Wójtowice</td>
    <td class='alignright' align="right" width="74">25</td>
    <td class='alignright' align="left" width="167">Justyna Górka</td>
  </tr>
  <tr>
    <td width="366">X Mistrzostwa Podhala</td>
    <td class='alignright' align="right" width="74">20</td>
    <td class='alignright' align="left" width="167">Tomasz Zwoliński</td>
  </tr>
  <tr>
    <td width="366">Turniej o puchar Wójta</td>
    <td class='alignright' align="right" width="74">15</td>
    <td class='alignright' align="left" width="167">Irena Sołdan</td>
  </tr>
  <tr>
    <td width="366">&nbsp;</td>
    <td class='alignright' align="right" width="74">&nbsp;</td>
    <td class='alignright' align="left" width="167">&nbsp;</td>
  </tr>
  <tr>
    <td width="366">III Turniej Mikołajkowy</td>
    <td class='alignright' align="right" width="74">70</td>
    <td class='alignright' align="left" width="167">Paweł Mazurek</td>
  </tr>
  <tr>
    <td width="366">VIII Mistrzostwa Polski Gimnazjów (nierankingowy)</td>
    <td class='alignright' align="right" width="74">36</td>
    <td class='alignright' align="left" width="167">Natalia Połomska, <br>
    Paula Szałata</td>
  </tr>
  <tr>
    <td width="366">VII Mistrzostwa Polski Szkół Ponadgimnazjalnych
    (nierankingowy)</td><td class='alignright' align="right" width="74">26</td>
    <td class='alignright' align="left" width="167">Marek Salamon,<br>
    Kinga Stecuła</td>
  </tr>
  <tr>
    <td width="366">IV Mistrzostwa Polski Nauczycieli (nierankingowy)</td>
    <td class='alignright' align="right" width="74">25</td>
    <td class='alignright' align="left" width="167">Piotr Pietuchowski</td>
  </tr>
  <tr><td width="366">&nbsp;</td>
    <td class='alignright' align="right" width="74">&nbsp;</td>
    <td class='alignright' align="left" width="167">&nbsp;</td></tr>
  <tr><td colspan='3' class='alignright'><b>razem: 2131 (turnieje rankingowe -
    1974)</b></td></tr>
</table>


<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>20</b> graczy, ale tylko siedmiu zanotowało więcej niż jedno zwycięstwo.
Klasyfikację medalową wygrał <b>Kamil Górka</b> z 4 zwycięstwami. Najwięcej razy
na podium (dziewięć) stawał <b>Tomasz Zwoliński</b>, z tego 3 razy na najwyższym
stopniu. Również 3 tryumfy odnieśli <b>Stanisław Rydzik</b> i <b>Kazimierz
Merklejn jr</b>. <br>
Kolejność w cyklu Grand Prix była podobna - <b>Górka, Merklejn, Zwoliński,
Rydzik</b>. Kamil miał szansę na zdobycie potrójnej korony (nikomu ten wyczyn
się jeszcze nie udał), ale w Mistrzostwach Polski zajął dopiero 10 miejsce. <br>
Mistrzem Polski został <b>Bartosz Morawski</b> - było to jego pierwsze
zwycięstwo turniejowe, czym powtórzył casus Mariusza Wrześniewskiego z 2004 r. Warto
przypomnieć, że Bartek był &quot;najlepszym nabytkiem listy rankingowej&quot; w sezonie...
<a href="http://www.pfs.org.pl/turnieje/2004/podsumowanie2004.php">2004</a>. <br>
Również pierwsze
zwycięstwa odnieśli <b>Mirosław Uglik</b>, <b>Dawid</b> <b>Adamczyk</b>, <b>
Jakub</b> <b>Szymczak</b>, <b>Michał Makowski</b>, <b>Piotr Domański</b>, <b>
Irena Sołdan</b>, <b>Grzegorz Kurowski</b> i <b>Tomasz Ciejka.<br>
</b>Tytuł Klubowych Mistrzów Polski zdobył <b>MDK Mikrus.</b>.<br>
<br>
Oto <b>klasyfikacja "medalowa"</b> za rok 2010:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2010' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa zmniejszyła się o <b>14</b> osób. Obecnie jest na niej <b>
331</b> graczy, w poczekalni - 376
(rok temu 340). <br>
Po raz pierwszy &quot;od niepamiętnych czasów&quot; przez cały rok tylko jedna okupowała
fotel lidera rankingu. Mistrz Polski <b>Dariusz Kosz</b> rozpoczął sezon z
rankingiem 150,7 i mimo, że w tym roku ani razu nie wygrał, systematycznie piął
się do góry. Aktualny rekord rankingu to <b>156,19</b>. Dopiero pod koniec roku
do rankingu Darka (153,59) zbliżył się <b>Stanislaw Rydzik</b> osiągając wynik
151,07 (a zaczynał sezon z rankingiem 138,25).<br>
Z czołowej dziesiątki wypadli: Krzysztof Obremski, Marcin Mroziuk, Miłosz
Wrzałek i Jakub Zaryński. Zastąpili ich: Stanisław Rydzik, Mariusz Wrześniewski,
Dawid Pikul i Tomasz Zwoliński (no cóż, nie są to nowe twarze...:)). W ciągu
roku bywali tam także Michał Alabrudziński (7), Justyna Górka (7) i Krzysztof Mówka (8).<br>
Obecnie w pierwszej dziesiątce jest ośmiu Mistrzów Polski. <br>
<br />
Oto czołówka rankingu i porównanie z końcem roku 2009, a także wykresy
przedstawiające zmiany w czołówce (pod względem miejsc i rankingu).
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2009</b></td><td><b>ranking</b></td><td><b>
    koniec 2010</b></td><td><b>ranking</b></td><td rowspan="11"><br />

    <img border="0" src="rozne/rank2010r.jpg" width="393" height="297"></a></td></tr>
  <tr><td>1.</td><td class="osoba">Dariusz Kosz</td><td>
    150,70</td><td class="osoba">Dariusz Kosz</td><td>
    153,59</td></tr>
  <tr><td>2.</td><td class="osoba">
    Krzysztof Obremski</td><td>147,73</td><td class="osoba">
    Stanisław Rydzik</td><td>151,07</td></tr>
  <tr><td>3.</td><td class="osoba">Mariusz Skrobosz</td><td>
    146,20</td><td class="osoba">Kamil Górka</td><td>
    148,10</td></tr>
  <tr><td>4.</td><td class="osoba">
    Michał Makowski</td><td>145,93</td><td class="osoba">
    Kazimierz Merklejn jr</td><td>147,97</td></tr>
  <tr><td>5.</td><td class="osoba">
    Marcin Mroziuk</td><td>145,41</td><td class="osoba">
    Mariusz Wrześniewski</td><td>145,95</td></tr>
  <tr><td>6.</td><td class="osoba">Kazimierz Merklejn jr</td><td>
    144,82</td><td class="osoba">Bartosz Morawski</td><td>
    145,61</td></tr>
  <tr><td>7.</td><td class="osoba">
    Miłosz Wrzałek</td><td>144,13</td><td class="osoba">
    Dawid Pikul</td><td>145,09</td></tr>
  <tr><td>8.</td><td class="osoba">Kamil Górka</td><td>
    143,38</td><td class="osoba">Michał Makowski</td><td>
    144,77</td></tr>
  <tr><td>9.</td><td class="osoba">
    Jakub Zaryński</td><td>142,69</td><td class="osoba">
    Tomasz Zwoliński</td><td>144,73</td></tr>
  <tr><td>10</td><td class="osoba">Bartosz Morawski</td><td>
    142,49</td><td class="osoba">Mariusz Skrobosz</td><td>
    144,37</td></tr>
</table>

<img border="0" src="rozne/rank2010.jpg" height="297">

<h2>Rankingi osób na miejscach</h2>
<p>Wysokość rankingu potrzebna by zająć dane miejsce na liście rankingowej
systematycznie rośnie. Na koniec roku aż 23 osoby miały ranking powyżej 140.</p>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td><td><b>koniec 2007</b></td><td><b>koniec 2008</b></td><td>
    <b>koniec 2009</b></td><td>
    <b>koniec 2010</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td><td>146,74</td><td>
    149,38</td><td>
    150,70</td><td>
    153,59</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td><td>140,96</td><td>
    141,28</td><td>
    142,49</td><td>
    144,37</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td><td>137,50</td><td>
    138,11</td><td>
    137,87</td><td>
    141,65</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td><td>129,53</td><td>
    131,09</td><td>
    131,83</td><td>
    132,09</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td><td>122,52</td><td>
    123,78</td><td>
    124,24</td><td>
    124,63</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td><td>116,48</td><td>
    116,93</td><td>
    117,80</td><td>
    118,57</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td><td>111,60</td><td>
    112,13</td><td>
    111,97</td><td>
    112,55</td></tr>
</table>

<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych,
poczekalnia nie jest brana pod uwagę)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2009 - ranking
    2010</b></td></tr>
  <tr>
    <td align="left">Dariusz Zębek</td><td>18,47</td><td>90,22 - 108,69</td>
    </tr>
    <tr>
        <td align="left">Mirosław Uglik</td><td>13,69</td><td>130,28 - 143,97</td>
    </tr>
    <tr>
        <td align="left">Rafał Matczak</td><td>13,30</td><td>121,17 - 134,47</td>
    </tr>
    <tr>
        <td align="left">Stanisław Rydzik</td><td>12,82</td><td>138,25 - 151,07</td>
    </tr>
    <tr>
        <td align="left">Michał Musielak</td><td>12,63</td><td>125,26 - 137,89</td>
    </tr>
    <tr>
        <td align="left">Magdalena Ambroszczyk</td><td>11,77</td><td>96,12 -
        107,89</td>
    </tr>
  <tr><td align="left">Sławomir Wolski</td><td>11,40</td><td>91,56 - 102,96</td></tr>
  <tr><td align="left">Łukasz Baranowski</td><td>10,79</td><td>113,84 - 124,63</td></tr>
  <tr><td align="left">Dominik Piasecki</td><td>10,52</td><td>121,28 - 131,80</td></tr>
  <tr><td align="left">Maciej Grąbczewski</td><td>10,46</td><td>110,81 - 121,27</td></tr>
  </table>


<h2>Najlepsze nabytki listy rankingowej (tylko debiutanci, powroty na listę
rankingową nie są brane pod uwagę)</h2>
&nbsp;<table class="klasyfikacja">
  <tr><td align="left">Jan Mrozowski</td><td>123,24</td></tr>
  <tr><td align="left">Dariusz Jarosławski</td><td>109,96</td></tr>
  <tr><td align="left">Adriana Gąbka</td><td>109,32</td></tr>
  <tr><td align="left">Krzysztof Gibas</td><td>108,83</td></tr>
  </table>

<h2>Ranking z partii rozegranych w 2010 roku</h2>
<table class="klasyfikacja" width="328">
  <tr><td>&nbsp;</td><td>ranking</td><td align="center" width="71">liczba gier</td></tr>
  <tr><td align="left">Kazimierz Merklejn jr</td><td>149,03</td><td align="center" width="71">
    237</td></tr>
  <tr><td align="left">Dariusz Kosz</td><td>148,82</td><td align="center" width="71">
    111</td></tr>
  <tr><td align="left">Stanisław Rydzik</td><td>147,84</td><td align="center" width="71">
    330</td></tr>
  <tr><td align="left">Mariusz Skrobosz</td><td>147,78</td><td align="center" width="71">
    164</td></tr>
  <tr>
    <td align="left">Krzysztof Mówka</td><td>146,88</td><td align="center" width="71">
    118</td>
    </tr>
    <tr>
        <td align="left">Mirosław Uglik</td><td>146,72</td><td align="center" width="71">
    162</td>
    </tr>
    <tr>
        <td align="left">Bartosz Morawski</td><td>146,38</td><td align="center" width="71">
        164</td>
    </tr>
    <tr>
        <td align="left">Mariusz Wrześniewski</td><td>146,33</td><td align="center" width="71">
        73</td>
    </tr>
    <tr>
        <td align="left">Dawid Adamczyk</td><td>146,03</td><td align="center" width="71">
        72</td>
    </tr>
    <tr>
        <td align="left">Dawid Pikul</td><td>145,70</td><td align="center" width="71">
        70</td>
    </tr>
</table>

&nbsp;<h2>Najwięcej rozegranych partii w 2010 r.</h2>
  Najwięcej partii rozegrali tradycyjnie Tomasz Zwoliński i Stanisław
Rydzik<table class="klasyfikacja">
  <tr><td align="left">Tomasz Zwoliński</td><td>359</td></tr>
  <tr>
    <td align="left">Stanisław Rydzik</td><td>330</td>
    </tr>
  <tr>
    <td align="left">Rafał Wesołowski</td><td>320</td>
    </tr>
  <tr><td align="left">Kamil Górka</td><td>291</td></tr>
  <tr><td align="left">Krzysztof Sporczyk</td><td>264</td></tr>
  <tr>
    <td align="left">Justyna Górka</td><td>254</td>
    </tr>
  <tr><td align="left">Irena Sołdan</td><td>250</td></tr>
  <tr><td align="left">Piotr Broda</td><td>245</td></tr>
  <tr>
    <td align="left">Kazimierz Merklejn jr</td><td>237</td>
    </tr>
    <tr>
        <td align="left">Michał Alabrudziński</td><td>226</td>
    </tr>
  </table>


<h2>Statystyka</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td><td><b>2007</b></td><td>
    <b>2008</b></td><td>
    <b>2009</b></td><td>
    <b>2010&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td><td>32</td><td>
    30</td><td>
    34</td><td>
    32</td></tr>
  <tr><td>Gracze</td><td>398</td><td>508</td><td>512</td><td>596</td><td>575</td><td>519</td><td>
    525</td><td>
    526</td><td>
    541</td></tr>
  <tr><td>Gry rankingowe</td><td>20 164</td><td>23 068</td><td>22 894</td><td>28 992</td><td>25334</td><td>25002</td><td>
    24378</td><td>
    24402</td><td>
    25276</td></tr>
  <tr><td>Średnia gier na gracza</td><td>50,66</td><td>45,41</td><td>44,71</td><td>48,64</td><td>44,06</td><td>48,17</td><td>
    46,43</td><td>
    46,39</td><td>
    46,72</td></tr>
  <tr><td>Uczestników razem (turnieje rankingowe)</td><td>1845</td><td>2016</td><td>1934</td><td>2487</td><td>2214</td><td>2078</td><td>
    2039</td><td>
    2007</td><td>
    1974</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>71</td><td>70</td><td>77</td><td>75</td><td>69</td><td>65</td><td>
    68</td><td>
    59</td><td>
    62</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td><td>3</td><td>
    2</td><td>
    2</td><td>
    &nbsp;</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td><td>9</td><td>
    13</td><td>
    11</td><td>
    &nbsp;</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td><td>58</td><td>
    56</td><td>
    64</td><td>
    &nbsp;</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td><td>92</td><td>
    88</td><td>
    77</td><td>
    &nbsp;</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td><td>346</td><td>
    366</td><td>
    372</td><td>
    &nbsp;</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td><td>368</td><td>
    343</td><td>
    345</td><td>
    331</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td><td>337</td><td>
    337</td><td>
    340</td><td>
    376</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td><td>705</td><td>
    681</td><td>
    685</td><td>
    707</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td><td>116</td><td>
    131</td><td>
    102</td><td>
    146</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td><td>Wałcz - 146</td><td>
    Ostróda - 132</td><td>
    Wałcz - 125</td><td>
    MP - 126</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td><td>1</td><td>
    0</td><td>
    5</td><td>
    3</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td><td>6</td><td>
    8</td><td>
    8</td><td>
    9</td></tr>
</table>
</div>

<div id='tabs-2009'>

<h2 id="turnieje">Turnieje</h2>
W 2009 r. odbyły się <b>34</b> turnieje zaliczane do rankingu PFS. Wzięło w nich udział <b>526</b> osób.
W tym roku najwięcej uczestników przyjechało na <a href="http://www.pfs.org.pl/http://www.pfs.org.pl/relacja.php?id=450">V
Mistrzostwa Wałcza</a> - 125. 111 osób wzięło udział w
<a href="http://www.pfs.org.pl/relacja.php?id=462">XVII Mistrzostwach Polski</a>.<br>
Po raz pierwszy rozegrano <a href="http://www.pfs.org.pl/relacja.php?id=441">Mistrzostwa Gór
Bystrzyckich w Wójtowicach</a>, <a href="http://www.pfs.org.pl/relacja.php?id=444">Mistrzostwa Puław</a>,
<a href="http://www.pfs.org.pl/relacja.php?id=473">Ustki</a>, <a href="http://www.pfs.org.pl/relacja.php?id=496">Ziemi
Legnickiej</a> i <a href="http://www.pfs.org.pl/relacja.php?id=497">Łomży</a><br>
Wałcz po raz piąty był gospodarzem <a href="http://www.pfs.org.pl/relacja.php?id=438">Mistrzostw Polski Nauczycieli</a>.
<a href="http://www.pfs.org.pl/relacja.php?id=478">IV Klubowe Mistrzostwa Polski</a> odbyły się
tym razem w Burzeninie.<br>
Prężnie rozwija się scrabble szkolne - <a href="http://www.pfs.org.pl/relacja.php?id=474">VII Mistrzostwa Polski Gimnazjów</a>
odbyły się
w Kutnie, <a href="http://www.pfs.org.pl/relacja.php?id=453">VI Mistrzostwa Polski Szkół Ponadgimnazjalnych</a> w
Zabrzu. W Warszawie odbył się turniej z okazji <a href="http://www.pfs.org.pl/relacja.php?id=480">
XX-lecia SLO nr 7 im. B. Geremka</a>, Poznań zaprosił na
<a href="http://www.pfs.org.pl/relacja.php?id=468">I Turniej o Tańczące Koziołki</a>. W
<a href="http://www.pfs.org.pl/relacja.php?id=442">II Turnieju Mikołajkowym</a> wzięło udział aż 81
młodych scrabblistów z 12 szkolnych kółek.<br>
Scrabble były
obecne na <a href="http://www.pfs.org.pl/relacja.php?id=493">Letnich Igrzyskach Lekarskich</a>,
<a href="http://www.pfs.org.pl/relacja.php?id=484">Spartakiadzie Prawników</a> oraz
<a href="http://www.pfs.org.pl/relacja.php?id=477">Przeglądzie Kabaretów &quot;PAKA&quot;</a>. Przy okazji
<a href="http://www.pfs.org.pl/relacja.php?id=466">V Mistrzostw Wrocławia</a> rozegrano II Akademickie Mistrzostwa
Polski i Mistrzostwa Politechniki Wrocławskiej oraz Turniej Osobistości, wygrany
ponownie przez Grzegorza Miecugowa z TVN24. <br>
<a href="http://www.pfs.org.pl/relacja.php?id=483">IV Mistrzostwa Polski po Angielsku</a> wyłoniły
reprezentanta na Mistrzostwa Świata w Malezji.<br>
Scrabbliści relaksowali się (przy scrabblach, ale nie tylko) na wczasach w
Zakopanem, Sudomiu i Władysławowie.<p>Liczba uczestników turniejów przedstawia się następująco:
</p>

<table class="zapisy" width="621">
  <tr><td width="366">Turniej</td>
    <td class='alignright' align="right" width="74">
    <p align="center">Liczba<br>
    uczestników</td>
    <td class='alignright' align="left" width="167">
    <p align="center">Zwycięzca</td></tr>
  <tr><td width="366">V Mistrzostwa Wałcza</td>
    <td class='alignright' align="right" width="74">125</td>
    <td class='alignright' align="left" width="167">Dariusz Kosz</td></tr>
  <tr><td width="366">XVII Mistrzostwa Polski</td>
    <td class='alignright' align="right" width="74">111</td>
    <td class='alignright' align="left" width="167">Dariusz Kosz</td></tr>
  <tr><td width="366">XIII Mistrzostwa Krakowa</td>
    <td class='alignright' align="right" width="74">98</td>
    <td class='alignright' align="left" width="167">Dariusz Kosz</td></tr>
  <tr><td width="366">VIII Mistrzostwa Ziemi Ostródzkiej &quot;Blanki w Szranki&quot;</td>
    <td class='alignright' align="right" width="74">95</td>
    <td class='alignright' align="left" width="167">Tomasz Zwoliński</td></tr>
  <tr><td width="366">III Mistrzostwa Wrocławia</td>
    <td class='alignright' align="right" width="74">90</td>
    <td class='alignright' align="left" width="167">Krzysztof Obremski</td></tr>
  <tr><td width="366">VIII Mistrzostwa Wybrzeża</td>
    <td class='alignright' align="right" width="74">89</td>
    <td class='alignright' align="left" width="167">Miłosz Wrzałek</td></tr>
  <tr><td width="366">IX Le Mans</td>
    <td class='alignright' align="right" width="74">88</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td></tr>
  <tr><td width="366">XIV Mistrzostwa Górnego Śląska i Zagłębia</td>
    <td class='alignright' align="right" width="74">79</td>
    <td class='alignright' align="left" width="167">Krzysztof Obremski</td></tr>
  <tr><td width="366">X Mistrzostwa Szczecina &quot;Zanim zakwitną magnolie&quot;</td>
    <td class='alignright' align="right" width="74">77</td>
    <td class='alignright' align="left" width="167">Dawid Pikul</td></tr>
  <tr><td width="366">XIII Mistrzostwa Warszawy</td>
    <td class='alignright' align="right" width="74">72</td>
    <td class='alignright' align="left" width="167">Dariusz Kosz</td></tr>
  <tr><td width="366">Wielka Łódka 2009 &#8211; Zima</td>
    <td class='alignright' align="right" width="74">60</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td></tr>
  <tr><td width="366">IV Mistrzostwa Doliny Karpia - Graboszyce</td>
    <td class='alignright' align="right" width="74">60</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td></tr>
  <tr><td width="366">IV Mistrzostwa Jaworzna</td>
    <td class='alignright' align="right" width="74">58</td>
    <td class='alignright' align="left" width="167">Krzysztof Mówka</td></tr>
  <tr><td width="366">XIV Puchar Polski</td>
    <td class='alignright' align="right" width="74">54</td>
    <td class='alignright' align="left" width="167">Krzysztof Obremski</td></tr>
  <tr><td width="366">XI Mistrzostwa Ziemi Kujawskiej</td>
    <td class='alignright' align="right" width="74">53</td>
    <td class='alignright' align="left" width="167">Mariusz Skrobosz</td></tr>
  <tr><td width="366">Wielka Łódka 2009 &#8211; Jesień, XV M. Łodzi</td>
    <td class='alignright' align="right" width="74">53</td>
    <td class='alignright' align="left" width="167">Dariusz Kosz</td></tr>
  <tr><td width="366">Wielka Łódka 2009 &#8211; Lato</td>
    <td class='alignright' align="right" width="74">53</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td></tr>
  <tr><td width="366">IV Mistrzostwa Milanówka &quot;Truskawki w Milanówku&quot;</td>
    <td class='alignright' align="right" width="74">53</td>
    <td class='alignright' align="left" width="167">Michał Alabrudziński</td></tr>
  <tr><td width="366">VII Mistrzostwa Bydgoszczy</td>
    <td class='alignright' align="right" width="74">52</td>
    <td class='alignright' align="left" width="167">Marek Dudkiewicz</td></tr>
  <tr><td width="366">II Mistrzostwa Sulęcina</td>
    <td class='alignright' align="right" width="74">52</td>
    <td class='alignright' align="left" width="167">Karol Wyrębkiewicz</td></tr>
  <tr><td width="366">XII Mistrzostwa Ziemi Słupskiej</td>
    <td class='alignright' align="right" width="74">50</td>
    <td class='alignright' align="left" width="167">Mariusz Skrobosz</td></tr>
  <tr><td width="366">IV Klubowe Mistrzostwa Polski</td>
    <td class='alignright' align="right" width="74">49</td>
    <td class='alignright' align="left" width="167">Macaki Warszawa</td></tr>
  <tr><td width="366">I Mistrzostwa Ziemi Legnickiej</td>
    <td class='alignright' align="right" width="74">47</td>
    <td class='alignright' align="left" width="167">Karol Waniek</td></tr>
  <tr><td width="366">I Mistrzostwa Puław</td>
    <td class='alignright' align="right" width="74">47</td>
    <td class='alignright' align="left" width="167">Piotr Broda</td></tr>
  <tr><td width="366">XIII Mistrzostwa Warmi i Mazur</td>
    <td class='alignright' align="right" width="74">43</td>
    <td class='alignright' align="left" width="167">Miłosz Wrzałek</td></tr>
  <tr><td width="366">VI Mistrzostwa Bałtyku</td>
    <td class='alignright' align="right" width="74">43</td>
    <td class='alignright' align="left" width="167">Krzysztof Obremski</td></tr>
  <tr><td width="366">I Mistrzostwa Łomży</td>
    <td class='alignright' align="right" width="74">42</td>
    <td class='alignright' align="left" width="167">Mateusz Borsuk</td></tr>
  <tr><td width="366">I Mistrzostwa Ustki</td>
    <td class='alignright' align="right" width="74">39</td>
    <td class='alignright' align="left" width="167">Kazimierz Merklejn jr</td></tr>
  <tr><td width="366">VII Mistrzostwa Polski Gimnazjów (nierankingowy)</td>
    <td class='alignright' align="right" width="74">36</td>
    <td class='alignright' align="left" width="167">Agnieszka Goniowska,
    Artemiusz Talar</td></tr>
  <tr><td width="366">Wielka Łódka 2009 &#8211; Wiosna</td>
    <td class='alignright' align="right" width="74">34</td>
    <td class='alignright' align="left" width="167">Stanisław Rydzik</td></tr>
  <tr><td width="366">XII Mistrzostwa Nowego Dworu Mazowieckiego</td>
    <td class='alignright' align="right" width="74">34</td>
    <td class='alignright' align="left" width="167">Mariusz Skrobosz</td></tr>
  <tr><td width="366">XIII Mistrzostwa Piastowa</td>
    <td class='alignright' align="right" width="74">34</td>
    <td class='alignright' align="left" width="167">Miłosz Wrzałek</td></tr>
  <tr><td width="366">IV Mistrzostwa Polski Nauczycieli (nierankingowy)</td>
    <td class='alignright' align="right" width="74">28</td>
    <td class='alignright' align="left" width="167">Krzysztof Langiewicz</td></tr>
  <tr><td width="366">I Mistrzostwa Gór Bystrzyckich - Wójtowice</td>
    <td class='alignright' align="right" width="74">27</td>
    <td class='alignright' align="left" width="167">Kamil Górka</td></tr>
  <tr><td width="366">VI Mistrzostwa Polski Szkół Ponadgimnazjalnych
    (nierankingowy)</td><td class='alignright' align="right" width="74">26</td>
    <td class='alignright' align="left" width="167">Rafał Dąbrowski, Michał
    Kolaj</td></tr>
  <tr><td width="366">IX Mistrzostwa Podhala</td>
    <td class='alignright' align="right" width="74">23</td>
    <td class='alignright' align="left" width="167">Kamil Górka</td></tr>
  <tr><td width="366">Turniej o puchar Wójta</td>
    <td class='alignright' align="right" width="74">23</td>
    <td class='alignright' align="left" width="167">Rafał Lenartowski</td></tr>
  <tr><td width="366">IV Mistrzostwa Polski po Angielsku (nierankingowy)</td>
    <td class='alignright' align="right" width="74">6</td>
    <td class='alignright' align="left" width="167">Bartosz Pięta</td></tr>
  <tr><td colspan='3' class='alignright'><b>razem: 2103 (turnieje rankingowe -
    2007)</b></td></tr>
</table>


<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>17</b> graczy, ale tylko sześciu zanotowało więcej niż jedno zwycięstwo.
Na czele klasyfikacji medalowej stanęli <b>Stanisław Rydzik</b> i <b>Dariusz
Kosz</b> z pięcioma wygranymi turniejami. Najwięcej razy (9) na podium stawał
jednak <b>Krzysztof Obremski</b>, z tego 4 razy na najwyższym stopniu<b>.</b> Po
3 zwycięstwa odnotowali Mariusz Skrobosz i Miłosz Wrzałek, a 2 - Kamil Górka.
Ten rok należał do Darka Kosza - został Mistrzem Polski, wygrał 3 turnieje Grand
Prix, co zapewniło mu triumf w całym cyklu oraz został liderem rankingu. <br>
Rok 2009 był obfity pod względem pierwszych zwycięstw turniejowych,
po raz pierwszy w karierze na najwyższym stopniu podium stanęło aż 8 graczy - <b>
Rafał Lenartowski, Michał Alanrudziński, Piotr Broda, Karol Waniek i Mateusz
Borsuk.</b> W sumie medale
zdobyło 45 osób<b>.</b>
<br>
Tytuł Klubowych Mistrzów Polski obroniły <b>Macaki Warszawa</b>.<br>
Oto <b>klasyfikacja "medalowa"</b> za rok 2009:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2009' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa zwiększyła się o <b>2</b> osoby. Obecnie jest na niej <b>345</b> graczy, w poczekalni - 340
(rok temu 337). <br>
Na pierwszym miejscu listy rankingowej rok rozpoczął <b>Kazimierz Merklejn jr</b>
z aż trzypunktową przewagą, która w kwietniu urosła do 4 punktów, a Kazik
osiągnął swój najwyższy ranking 150,61. W sierpniu na fotel lidera powrócił po
ponadrocznej przerwie <b>Mariusz Skrobosz</b>, który zaczynał sezon z rankingiem
136,94 a we wrześniu pobił rekord rankingu - <b>151,73</b>. W listopadzie został
wyprzedzony przez <b>Dariusza Kosza</b>. Darek zakończył rok na pierwszym
miejscu również z rankingiem ponad 150.<br>
Z czołowej dziesiątki wypadli: Dariusz Dzierża, Mariusz Wrześniewski, Karol
Wyrębkiewicz, Stanisław Rydzik i Krzosztof Mówka; zastąpili ich Krzysztof
Obremski, Mariusz Skrobosz, Miłosz Wrzałek, Jakub Zaryński i Bartosz Morwski.
Gościli tam także Tomasz Zwoliński (5) i Marek Syczuk (5).<br />
Oto czołówka rankingu i porównanie z końcem roku 2008, a także wykresy przedstawiające zmiany w czołówce (pod względem miejsc i rankingu).
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2008</b></td><td><b>ranking</b></td><td><b>koniec 2009</b></td><td><b>ranking</b></td><td rowspan="11"><br />
    <a href="rank2009r.gif">
    <img border="0" src="rozne/rank2009r.gif" width="393" height="297"></a></td></tr>
  <tr><td>1.</td><td class="osoba">Kazimierz Merklejn jr</td><td>
    149,38</td><td class="osoba">Dariusz Kosz</td><td>
    150,70</td></tr>
  <tr><td>2.</td><td class="osoba">
    Dariusz Dzierża</td><td>146,66</td><td class="osoba">
    Krzysztof Obremski</td><td>147,73</td></tr>
  <tr><td>3.</td><td class="osoba">Mariusz Wrześniewski</td><td>
    146,13</td><td class="osoba">Mariusz Skrobosz</td><td>
    146,20</td></tr>
  <tr><td>4.</td><td class="osoba">
    Michał Makowski</td><td>144,89</td><td class="osoba">
    Michał Makowski</td><td>145,93</td></tr>
  <tr><td>5.</td><td class="osoba">
    Karol Wyrębkiewicz</td><td>144,37</td><td class="osoba">
    Marcin Mroziuk</td><td>145,41</td></tr>
  <tr><td>6.</td><td class="osoba">Marcin Mroziuk</td><td>
    143,64</td><td class="osoba">Kazimierz Merklejn jr</td><td>
    144,82</td></tr>
  <tr><td>7.</td><td class="osoba">
    Kamil Górka</td><td>143,43</td><td class="osoba">
    Miłosz Wrzałek</td><td>144,13</td></tr>
  <tr><td>8.</td><td class="osoba">Stanisław Rydzik</td><td>
    143,38</td><td class="osoba">Kamil Górka</td><td>
    143,38</td></tr>
  <tr><td>9.</td><td class="osoba">
    Dariusz Kosz</td><td>142,43</td><td class="osoba">
    Jakub Zaryński</td><td>142,69</td></tr>
  <tr><td>10</td><td class="osoba">Krzysztof Mówka</td><td>
    141,28</td><td class="osoba">Bartosz Morawski</td><td>
    142,49</td></tr>
</table>
<a href="rank2009.gif">
<img border="0" src="rozne/rank2009.gif" width="510" height="265"></a>

<h2>Rankingi osób na miejscach</h2>
<p>Wysokość rankingu potrzebna by zająć dane miejsce na liście rankingowej
systematycznie rośnie. W sierpniu aż 19 osób miało ranking powyżej 140.</p>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td><td><b>koniec 2007</b></td><td><b>koniec 2008</b></td><td>
    <b>koniec 2009</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td><td>146,74</td><td>
    149,38</td><td>
    150,70</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td><td>140,96</td><td>
    141,28</td><td>
    142,49</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td><td>137,50</td><td>
    138,11</td><td>
    137,87</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td><td>129,53</td><td>
    131,09</td><td>
    131,83</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td><td>122,52</td><td>
    123,78</td><td>
    124,24</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td><td>116,48</td><td>
    116,93</td><td>
    117,80</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td><td>111,60</td><td>
    112,13</td><td>
    111,97</td></tr>
</table>

<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych,
poczekalnia nie jest brana pod uwagę)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2008 - ranking 2009</b></td></tr>
  <tr>
    <td align="left">Kamil Kister</td><td>16,68</td><td>101,05 - 117,73</td>
    </tr>
    <tr>
        <td align="left">Krzysztof Kodzis</td><td>16,34</td><td>102,33 - 118,67</td>
    </tr>
    <tr>
        <td align="left">Piotr Jakubiak</td><td>15,77</td><td>110,36 - 126,13</td>
    </tr>
    <tr>
        <td align="left">Dominik Piasecki</td><td>14,89</td><td>106,39 - 121,28</td>
    </tr>
    <tr>
        <td align="left">Arkadiusz Łydka</td><td>14,03</td><td>113,77 - 127,80</td>
    </tr>
    <tr>
        <td align="left">Anna Lenartowska</td><td>12,90</td><td>80,57 - 93,47</td>
    </tr>
  <tr><td align="left">Marta Dobrzańska</td><td>11,50</td><td>114,57 - 126,07</td></tr>
  <tr><td align="left">Tomasz Sienkiewicz</td><td>11,09</td><td>115,97 - 127,06</td></tr>
  <tr><td align="left">Grzegorz Grzeszczuk</td><td>11,07</td><td>110,29 - 121,36</td></tr>
  <tr><td align="left">Jan Kozłowski</td><td>10,90</td><td>125,20 - 136,10</td></tr>
  </table>


<h2>Najlepsze nabytki listy rankingowej (tylko debiutanci, powroty na listę
rankingową nie są brane pod uwagę)</h2>
&nbsp;<table class="klasyfikacja">
  <tr><td align="left">Maciej Michalski</td><td>111,97</td></tr>
  <tr><td align="left">Mateusz Królikowski</td><td>109,58</td></tr>
  <tr><td align="left">Karol Posielski</td><td>103,22</td></tr>
  </table>

<h2>Ranking z partii rozegranych w 2009 roku <span>(tylko ci gracze, którzy rozegrali w 2009
r. minimum 62 partie - 15% wszystkich)</span></h2>
<table class="klasyfikacja" width="328">
  <tr><td>&nbsp;</td><td>ranking</td><td align="center" width="71">liczba gier</td></tr>
  <tr><td align="left">Dariusz Kosz</td><td>159,39</td><td align="center" width="71">
    114</td></tr>
  <tr><td align="left">Michał Makowski</td><td>148,53</td><td align="center" width="71">
    62</td></tr>
  <tr><td align="left">Mariusz Skrobosz</td><td>146,60</td><td align="center" width="71">
    251</td></tr>
  <tr><td align="left">Kazimierz Merklejn jr</td><td>146,02</td><td align="center" width="71">
    260</td></tr>
  <tr>
    <td align="left">Krzysztof Obremski</td><td>145,47</td><td align="center" width="71">
    253</td>
    </tr>
    <tr>
        <td align="left">Dawid Pikul</td><td>144,72</td><td align="center" width="71">
    113</td>
    </tr>
    <tr>
        <td align="left">Kamil Górka</td><td>144,45</td><td align="center" width="71">
        228</td>
    </tr>
    <tr>
        <td align="left">Miłosz Wrzałek</td><td>144,13</td><td align="center" width="71">
        199</td>
    </tr>
    <tr>
        <td align="left">Jakub Zaryński</td><td>143,64</td><td align="center" width="71">
        88</td>
    </tr>
    <tr>
        <td align="left">Rafał Dąbrowski</td><td>143,48</td><td align="center" width="71">
        124</td>
    </tr>
</table>

Tu można znaleźć <a href="stats/skalp2009.xls">pełną tabelę (.xls)</a>, zawierającą skalpy wszystkich uczestników turniejów w 2008 r. W razie znalezienia błędów prosimy o <a onClick="sendMail('pfs','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />informacje</a>.

<h2>Najwięcej rozegranych partii w 2009 r.</h2>
W tym roku można było rozegrać 410 partii rankingowych, trochę więcej niż rok temu
(394). Najwięcej turniejów zaliczyli tradycyjnie Tomasz Zwoliński i Stanisław
Rydzik<table class="klasyfikacja">
  <tr><td align="left">Tomasz Zwoliński</td><td>365</td></tr>
  <tr>
    <td align="left">Stanisław Rydzik</td><td>347</td>
    </tr>
  <tr>
    <td align="left">Rafał Wesołowski</td><td>285</td>
    </tr>
  <tr><td align="left">Krzysztof Sporczyk</td><td>281</td></tr>
  <tr><td align="left">Kazimierz Merklejn jr</td><td>260</td></tr>
  <tr>
    <td align="left">Krzysztof Obremski</td><td>253</td>
    </tr>
  <tr><td align="left">Mariusz Skrobosz</td><td>251</td></tr>
  <tr><td align="left">Marek Reda</td><td>243</td></tr>
  <tr>
    <td align="left">Kamil Górka</td><td>228</td>
    </tr>
    <tr>
        <td align="left">Grzegorz Kurowski</td><td>226</td>
    </tr>
  </table>


<h2>Statystyka</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td><td><b>2007</b></td><td>
    <b>2008</b></td><td>
    <b>2009</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td><td>32</td><td>
    30</td><td>
    34</td></tr>
  <tr><td>Gracze</td><td>398</td><td>508</td><td>512</td><td>596</td><td>575</td><td>519</td><td>
    525</td><td>
    526</td></tr>
  <tr><td>Gry rankingowe</td><td>20 164</td><td>23 068</td><td>22 894</td><td>28 992</td><td>25334</td><td>25002</td><td>
    24378</td><td>
    24402</td></tr>
  <tr><td>Średnia gier na gracza</td><td>50,66</td><td>45,41</td><td>44,71</td><td>48,64</td><td>44,06</td><td>48,17</td><td>
    46,43</td><td>
    46,39</td></tr>
  <tr><td>Uczestników razem (turnieje rankingowe)</td><td>1845</td><td>2016</td><td>1934</td><td>2487</td><td>2214</td><td>2078</td><td>
    2039</td><td>
    2007</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>71</td><td>70</td><td>77</td><td>75</td><td>69</td><td>65</td><td>
    68</td><td>
    59</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td><td>3</td><td>
    2</td><td>
    2</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td><td>9</td><td>
    13</td><td>
    11</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td><td>58</td><td>
    56</td><td>
    64</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td><td>92</td><td>
    88</td><td>
    77</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td><td>346</td><td>
    366</td><td>
    372</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td><td>368</td><td>
    343</td><td>
    345</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td><td>337</td><td>
    337</td><td>
    340</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td><td>705</td><td>
    681</td><td>
    685</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td><td>116</td><td>
    131</td><td>
    102</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td><td>Wałcz - 146</td><td>
    Ostróda - 132</td><td>
    Wałcz - 125</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td><td>1</td><td>
    0</td><td>
    5</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td><td>6</td><td>
    8</td><td>
    5</td></tr>
</table>
</div>

<div id='tabs-2008'>

<h2 id="turnieje">Turnieje</h2>
W 2008 r. odbyło się <b>30</b> turniejów zaliczanych do rankingu PFS. Wzięło w nich udział <b>525</b> osób. Rekord liczby uczestników nie został pobity, ale
podobnie jak przed rokiem, 5 turniejów miało obsadę ponad 100 uczestników. <b>
Ostróda, Wrocław, Wałcz i Mistrzostwa Polski </b>już tradycyjnie są najchętniej
odwiedzane, w tym roku do elitarnego grona dołączył <b>Szczecin</b>. <br>
W nowej formule rozegrano cykl <b>Grand Prix</b> - status GP otrzymało 10
turniejów, które zostały połączone w pary (północ-południe), a punkty zaliczano
z najlepszego występu z każdej pary.<br>
Wałcz po raz czwarty był gospodarzem <b>Mistrzostw Polski Nauczycieli</b>. <b>
III Klubowe Mistrzostwa Polski</b> odbyły się w <b>Załęczu</b>, <b>VI Mistrzostwa Polski Gimnazjów</b>
w <b>Kutnie</b>, <b>V Mistrzostwa Polski Szkół Ponadgimnazjalnych</b> w <b>
Zabrzu</b>. Scrabble były
obecne na Letnich Igrzyskach Lekarskich oraz turnieju gier towarzyskich Warsaw
Open. Przy okazji IV Mistrzostw Wrocławia rozegrano I Akademickie Mistrzostwa
Polski i Mistrzostwa Politechniki Wrocławskiej oraz Turniej Osobistości, wygrany
przez Grzegorza Miecugowa z TVN.<br>
Scrabbliści relaksowali się (przy scrabblach, ale nie tylko) na wczasach w
Zakopanem, Sudomiu, Wiśle i Władysławowie.<p>Liczba uczestników turniejów przedstawia się następująco:
</p>

<table class="zapisy" width="437">
  <tr><td>VIII Mistrzostwa Ziemi Ostródzkiej &quot;Blanki w Szranki&quot;</td>
    <td class='alignright' align="right">132</td></tr>
  <tr><td>III Mistrzostwa Wrocławia</td><td class='alignright' align="right">127</td></tr>
  <tr><td>IV Mistrzostwa Wałcza</td><td class='alignright' align="right">123</td></tr>
  <tr><td>XVI Mistrzostwa Polski</td><td class='alignright' align="right">106</td></tr>
  <tr><td>X Mistrzostwa Szczecina &quot;Zanim zakwitną magnolie&quot;</td>
    <td class='alignright' align="right">102</td></tr>
  <tr><td>VIII Le Mans</td><td class='alignright' align="right">97</td></tr>
  <tr><td>XII Mistrzostwa Krakowa</td><td class='alignright' align="right">87</td></tr>
  <tr><td>XII Mistrzostwa Warszawy</td><td class='alignright' align="right">86</td></tr>
  <tr><td>VI Mistrzostwa Bydgoszczy</td><td class='alignright' align="right">81</td></tr>
  <tr><td>IV Mistrzostwa Katowic</td><td class='alignright' align="right">73</td></tr>
  <tr><td>XI Mistrzostwa Ziemi Kujawskiej</td>
    <td class='alignright' align="right">72</td></tr>
  <tr><td>XIII Mistrzostwa Górnego Śląska i Zagłębia</td>
    <td class='alignright' align="right">69</td></tr>
  <tr><td>VI Mistrzostwa Wybrzeża</td><td class='alignright' align="right">67</td></tr>
  <tr><td>Otwarcie Sezonu 2008 - Wrocław</td>
    <td class='alignright' align="right">65</td></tr>
  <tr><td>XIII Puchar Polski</td><td class='alignright' align="right">62</td></tr>
  <tr><td>XII Mistrzostwa Ziemi Słupskiej</td>
    <td class='alignright' align="right">62</td></tr>
  <tr><td>Wielka Łódka 2008 &#8211; Zima</td><td class='alignright' align="right">58</td></tr>
  <tr><td>Wielka Łódka 2008 &#8211; Jesień, IV Mistrzostwa Łodzi</td>
    <td class='alignright' align="right">58</td></tr>
  <tr><td>Wielka Łódka 2008 &#8211; Lato</td><td class='alignright' align="right">54</td></tr>
  <tr><td>Wielka Łódka 2008 &#8211; Wiosna</td><td class='alignright' align="right">53</td></tr>
  <tr><td>XI Mistrzostwa Warmii i Mazur</td>
    <td class='alignright' align="right">53</td></tr>
  <tr><td>III Mistrzostwa Doliny Karpia - Graboszyce</td>
    <td class='alignright' align="right">51</td></tr>
  <tr><td>II Mistrzostwa Milanówka &quot;Truskawki w Milanówku&quot;</td>
    <td class='alignright' align="right">45</td></tr>
  <tr><td>XI Mistrzostwa Nowego Dworu Mazowieckiego</td>
    <td class='alignright' align="right">41</td></tr>
  <tr><td>VI Mistrzostwa Ziemi Gorzowskiej</td>
    <td class='alignright' align="right">39</td></tr>
  <tr><td>III Klubowe Mistrzostwa Polski</td>
    <td class='alignright' align="right">37</td></tr>
  <tr><td>V Mistrzostwa Bałtyku</td><td class='alignright' align="right">36</td></tr>
  <tr><td>XII Mistrzostwa Piastowa</td><td class='alignright' align="right">30</td></tr>
  <tr><td>I Mistrzostwa Wisły</td><td class='alignright' align="right">26</td></tr>
  <tr><td>VIII Mistrzostwa Podhala</td><td class='alignright' align="right">25</td></tr>
  <tr><td>IV Mistrzostwa Polski Nauczycieli</td>
    <td class='alignright' align="right">22</td></tr>
  <tr><td>&nbsp;</td><td class='alignright' align="right">&nbsp;</td></tr>
  <tr><td>&nbsp;</td><td class='alignright' align="right">&nbsp;</td></tr>
  <tr><td colspan='2' class='alignright'><b>razem: 2039</b></td></tr>
</table>


<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>19</b> graczy, ale tylko sześciu zanotowało więcej niż jedno zwycięstwo.
Podobnie jak rok wcześniej na czele tej klasyfikacji stanął <b>
Kazimierz Merklejn jr</b> z 4 zwycięstwami. Najwięcej &quot;medali&quot;, w tym 3 złote
zdobył <b>
Stanisław Rydzik,</b> również 3 - <b>
Karol Wyrębkiewicz.</b> Po 2 zwycięstwa odnotowali Mariusz Skrobosz, Justyna
Górka i Kamil Górka. Mariusz triumfowal również w klasyfikacji Grand Prix (po
raz trzeci w historii). Rok 2008 był obfity pod względem pierwszych zwycięstw turniejowych,
po raz pierwszy w karierze na najwyższym stopniu podium stanęło aż 8 graczy - <b>
Piotr Pietuchowski, Marek Reda, Marek Dudkiewicz, Patryk Gadus, Andrzej
Rechowicz, Filip Warzecha, Anita Bruska, Michał Ozimiński.</b> W sumie medale
zdobyły 42 osoby<b>.</b>
<br>
Oto <b>klasyfikacja "medalowa"</b> za rok 2007:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2008' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa zmniejszyła się o <b>25</b> osób. Obecnie jest na niej <b>343</b> graczy, w poczekalni - 337 (rok temu
również 337). Na pierwszym miejscu listy rankingowej rok rozpoczął <b>Marcin
Mroziuk</b>, potem jednak wypadł z dziesiątki aby ostatecznie powrócić pod
koniec roku i ostatecznie zakończył rok na 6 miejscu. Marcina wyprzedził w maju
<b>Kazik Merklejn</b> i mimo chwilowych utrat tej pozycji (na rzecz <b>
Stanisława Rydzika, Mariusza Skrobosza</b> i <b>Mariusza Wrześniewskiego</b>) to
on otworzy sezon 2009 jako lider rankingu.<br />
Z czołowej dziesiątki wypadli: Mariusz Skrobosz, Mateusz Żbikowski (w obu
przypadkach po raz pierwszy od 2002 roku), Marek Syczuk, Sławomir Kucia i
Jarosław Borowski; awansowali zaś: Dariusz Dzierża (skończył na drugim miejscu),
Michał Makowski (dotarł równiez do drugiego miejsca), Karol Wyrębkiewicz, Kamil
Górka i Dariusz Kosz. Gościli tam także: Dawid Pikul (6), Tomasz Zwoliński (2),
Szymon Fidziński (7), Andrzej Oleksiak (9) i Justyna Górka (10).<br />
Oto czołówka rankingu i porównanie z końcem roku 2007, a także wykresy przedstawiające zmiany w czołówce (pod względem miejsc i rankingu).
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2007</b></td><td><b>ranking</b></td><td><b>koniec 2008</b></td><td><b>ranking</b></td><td rowspan="11"><br />
    <a href="rank2008r.gif">
    <img border="0" src="rozne/rank2008r.gif" width="393" height="297"></a></td></tr>
  <tr><td>1.</td><td class="osoba">Marcin Mroziuk</td><td>146,74</td><td class="osoba">Kazimierz Merklejn jr</td><td>
    149,38</td></tr>
  <tr><td>2.</td><td class="osoba">Kazimierz Merklejn jr</td><td>146,29</td><td class="osoba">
    Dariusz Dzierża</td><td>146,66</td></tr>
  <tr><td>3.</td><td class="osoba">Mariusz Skrobosz</td><td>145,53</td><td class="osoba">Mariusz Wrześniewski</td><td>
    146,13</td></tr>
  <tr><td>4.</td><td class="osoba">Stanisław Rydzik</td><td>145,46</td><td class="osoba">
    Michał Makowski</td><td>144,89</td></tr>
  <tr><td>5.</td><td class="osoba">Mariusz Wrześniewski</td><td>144,10</td><td class="osoba">
    Karol Wyrębkiewicz</td><td>144,37</td></tr>
  <tr><td>6.</td><td class="osoba">Mateusz Żbikowski</td><td>141,39</td><td class="osoba">Marcin Mroziuk</td><td>
    143,64</td></tr>
  <tr><td>7.</td><td class="osoba">Krzysztof Mówka</td><td>141,33</td><td class="osoba">
    Kamil Górka</td><td>143,43</td></tr>
  <tr><td>8.</td><td class="osoba">Marek Syczuk</td><td>141,15</td><td class="osoba">Stanisław Rydzik</td><td>
    143,38</td></tr>
  <tr><td>9.</td><td class="osoba">Sławomir Kucia</td><td>140,99</td><td class="osoba">
    Dariusz Kosz</td><td>142,43</td></tr>
  <tr><td>10</td><td class="osoba">Jarosław Borowski</td><td>140,96</td><td class="osoba">Krzysztof Mówka</td><td>
    141,28</td></tr>
</table>
<a href="rank2008.gif">
<img border="0" src="rozne/rank2008.gif" width="510" height="265"></a>

<h2>Rankingi osób na miejscach</h2>
<p>Wysokość rankingu potrzebna by zająć dane miejsce na liście rankingowej
systematycznie rośnie. Ranking, który wystarczał 5 lat temu na miejsce w
pierwszej setce, teraz ledwo starcza na miejsce w pierwszej dwusetce. We
wrześniu aż 15 osób miało ranking powyżej 140.</p>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td><td><b>koniec 2007</b></td><td><b>koniec 2008</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td><td>146,74</td><td>
    149,38</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td><td>140,96</td><td>
    141,28</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td><td>137,50</td><td>
    138,11</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td><td>129,53</td><td>
    131,09</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td><td>122,52</td><td>
    123,78</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td><td>116,48</td><td>
    116,93</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td><td>111,60</td><td>
    112,13</td></tr>
</table>

<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2007 - ranking 2008</b></td></tr>
  <tr>
    <td align="left">Anita Bruska</td><td>17,68</td><td>109,00 - 126,68</td>
    </tr>
    <tr>
        <td align="left">Dariusz Białobrzewski</td><td>14,68</td><td>107,24 - 121,92</td>
    </tr>
    <tr>
        <td align="left">Weronika Rudnicka</td><td>12,39</td><td>109,16 - 121,55</td>
    </tr>
    <tr>
        <td align="left">Sławomir Piotrowski</td><td>12,06</td><td>112,49 - 124,55</td>
    </tr>
    <tr>
        <td align="left">Dariusz Dzierża</td><td>11,64</td><td>135,02 - 146,66</td>
    </tr>
    <tr>
        <td align="left">Rafał Stachowski</td><td>11,53</td><td>96,26 - 107,79</td>
    </tr>
  <tr><td align="left">Joanna Żłobicka</td><td>11,03</td><td>94,31 - 105,34</td></tr>
  <tr><td align="left">Michał Alabrudziński</td><td>10,92</td><td>128,60 -
    139,52</td></tr>
  <tr><td align="left">Michał Lipka</td><td>10,76</td><td>116,77 - 127,53</td></tr>
  <tr><td align="left">Blanka Zwolińska</td><td>10,35</td><td>65,28 - 75,63</td></tr>
  </table>


<h2>Najlepsze nabytki listy rankingowej</h2>
W tym roku debiutanci nie poczynali sobie tak śmiało jak w 2007 (czyli nie
wygrali turnieju), ale widać, że treningi w klubach pod okiem mistrzów przynoszą
efekty.
<table class="klasyfikacja">
  <tr><td align="left">Szymon Płachta</td><td>119,64</td></tr>
  <tr><td align="left">Piotr Wierzchowski</td><td>112,52</td></tr>
  <tr><td align="left">Monika Sadurska</td><td>111,21</td></tr>
  <tr><td align="left">Aleksander Puzyna</td><td>111,20</td></tr>
  <tr><td align="left">Renata Andracka</td><td>110,95</td></tr>
  <tr><td align="left">Łukasz Sornek</td><td>110,15</td></tr>
  <tr><td align="left">Michał Kołodziejczyk</td><td>107,95</td></tr>
  </table>

<h2>Ranking z partii rozegranych w 2008 roku <span>(tylko ci gracze, którzy rozegrali w 2008 r. minimum
56 partii)</span></h2>
<table class="klasyfikacja" width="328">
  <tr><td>&nbsp;</td><td>ranking</td><td align="center" width="71">liczba gier</td></tr>
  <tr><td align="left">Kazimierz.J Merklejn</td><td>147,05</td><td align="center" width="71">
    249</td></tr>
  <tr><td align="left">Karol Wyrębkiewicz</td><td>144,05</td><td align="center" width="71">
    168</td></tr>
  <tr><td align="left">Michał Makowski</td><td>143,45</td><td align="center" width="71">
    65</td></tr>
  <tr><td align="left">Stanisław Rydzik</td><td>143,33</td><td align="center" width="71">
    343</td></tr>
  <tr>
    <td align="left">Kamil Górka</td><td>142,28</td><td align="center" width="71">
    217</td>
    </tr>
    <tr>
        <td align="left">Krzysztof Obremski</td><td>141,46</td><td align="center" width="71">
    143</td>
    </tr>
    <tr>
        <td align="left">Łukasz Tuszyński</td><td>140,35</td><td align="center" width="71">
        147</td>
    </tr>
    <tr>
        <td align="left">Tomasz Zwoliński</td><td>140,33</td><td align="center" width="71">
        330</td>
    </tr>
    <tr>
        <td align="left">Mateusz Żbikowski</td><td>140,26</td><td align="center" width="71">
        145</td>
    </tr>
    <tr>
        <td align="left">Robert Kuszpit</td><td>140,20</td><td align="center" width="71">
        88</td>
    </tr>
</table>

Tu można znaleźć <a href="stats/skalp2008.xls">pełną tabelę (.xls)</a>, zawierającą skalpy wszystkich uczestników turniejów w 2008 r. W razie znalezienia błędów prosimy o <a onClick="sendMail('pfs','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />informacje</a>.

<h2>Najwięcej rozegranych partii w 2008 r.</h2>
W tym roku można było rozegrać 394 partii rankingowych, trochę mniej niż rok temu
(399). Najwięcej turniejów zaliczył Mistrz Polski 2007 Stanisław Rydzik.
<table class="klasyfikacja">
  <tr><td align="left">Stanisław Rydzik</td><td>343</td></tr>
  <tr><td align="left">Tomasz Zwoliński</td><td>330</td></tr>
  <tr>
    <td align="left">Sławomir Piotrowski</td><td>263</td>
    </tr>
  <tr><td align="left">Mariusz Skrobosz</td><td>255</td></tr>
  <tr><td align="left">Miłosz Wrzałek </td><td>254</td></tr>
  <tr>
    <td align="left">Marek Reda</td><td>251</td>
    </tr>
  <tr><td align="left">Kazimierz Merklejn jr</td><td>249</td></tr>
  <tr><td align="left">Krzysztof Sporczyk</td><td>244</td></tr>
  <tr>
    <td align="left">Rafał Wesołowski</td><td>238</td>
    </tr>
    <tr>
        <td align="left">Dawid Pikul</td><td>233</td>
    </tr>
  </table>


<h2>Statystyka</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td><td><b>2007</b></td><td>
    <b>2008</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td><td>32</td><td>
    30</td></tr>
  <tr><td>Gracze</td><td>398</td><td>508</td><td>512</td><td>596</td><td>575</td><td>519</td><td>
    525</td></tr>
  <tr><td>Gry rankingowe</td><td>20 164</td><td>23 068</td><td>22 894</td><td>28 992</td><td>25334</td><td>25002</td><td>
    24378</td></tr>
  <tr><td>Średnia gier na gracza</td><td>50,66</td><td>45,41</td><td>44,71</td><td>48,64</td><td>44,06</td><td>48,17</td><td>
    46,43</td></tr>
  <tr><td>Uczestników razem</td><td>1845</td><td>2016</td><td>1934</td><td>2487</td><td>2214</td><td>2078</td><td>
    2039</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>71</td><td>70</td><td>77</td><td>75</td><td>69</td><td>65</td><td>
    68</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td><td>3</td><td>
    2</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td><td>9</td><td>
    13</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td><td>58</td><td>
    56</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td><td>92</td><td>
    88</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td><td>346</td><td>
    366</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td><td>368</td><td>
    343</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td><td>337</td><td>
    337</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td><td>705</td><td>
    681</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td><td>116</td><td>
    131</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td><td>Wałcz - 146</td><td>
    Ostróda - 132</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td><td>1</td><td>
    0</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td><td>6</td><td>
    8</td></tr>
</table>
</div>

<div id='tabs-2007'>
    <h2 id="turnieje">Turnieje</h2>
W 2007 r. odbyły się <b>32</b> turnieje zaliczane do rankingu PFS. Wzięło w nich udział <b>519</b> osób. Rekord liczby uczestników nie został pobity, ale było
blisko - w III Mistrzostwach Wałcza wzięło udział 146 graczy. Również w Wałczu rozegrano kolejne <b>Klubowe Mistrzostwa Polski</b> i <b>Mistrzostwa Polski Nauczycieli</b>. W Kutnie zaś odbyły się <b>V Mistrzostwa Polski Gimnazjów</b>. Po raz pierwszy scrabblistów gościł Sulęcin. <b>Mistrzostwa w Scrabble po Angielsku</b> wyłoniły
reprezentanta Polski na World Scrabble Championship w Mumbaju (Indie). Liczba uczestników na turniejach przedstawia się następująco:

<table class="zapisy">
  <tr><td>III Mistrzostwa Wałcza</td><td class='alignright'>146</td></tr>
  <tr><td>XV Mistrzostwa Polski</td><td class='alignright'>124</td></tr>
  <tr><td>VII LeMans</td><td class='alignright'>108</td></tr>
  <tr><td>VII Mistrzostwa Ziemi Ostródzkiej "Blanki w Szranki"</td><td class='alignright'>105</td></tr>
  <tr><td>III Mistrzostwa Wrocławia</td><td class='alignright'>101</td></tr>
  <tr><td>XI Mistrzostwa Krakowa</td><td class='alignright'>91</td></tr>
  <tr><td>II Mistrzostwa Doliny Karpia - Graboszyce</td><td class='alignright'>85</td></tr>
  <tr><td>VI Mistrzostwa Wybrzeża</td><td class='alignright'>84</td></tr>
  <tr><td>IX Mistrzostwa Szczecina "Zanim zakwitną magnolie"</td><td class='alignright'>83</td></tr>
  <tr><td>XII Puchar Polski</td><td class='alignright'>67</td></tr>
  <tr><td>IV Mistrzostwa Katowic</td><td class='alignright'>66</td></tr>
  <tr><td>XI Mistrzostwa Warszawy</td><td class='alignright'>66</td></tr>
  <tr><td>XI Mistrzostwa Ziemi Kujawskiej</td><td class='alignright'>64</td></tr>
  <tr><td>Turniej 5-lecia WKS Siódemka - Budopol Series 2007</td><td class='alignright'>62</td></tr>
  <tr><td>Wielka Łódka 2007 - Zima</td><td class='alignright'>60</td></tr>
  <tr><td>III Mistrzostwa Iławy</td><td class='alignright'>59</td></tr>
  <tr><td>II Mistrzostwa Milanówka "Truskawki w Milanówku"</td><td class='alignright'>58</td></tr>
  <tr><td>XIII Mistrzostwa Łodzi</td><td class='alignright'>55</td></tr>
  <tr><td>XII Mistrzostwa Górnego Śląska i Zagłębia</td><td class='alignright'>55</td></tr>
  <tr><td>X Mistrzostwa Podbeskidzia</td><td class='alignright'>54</td></tr>
  <tr><td>IV Mistrzostwa Bałtyku</td><td class='alignright'>52</td></tr>
  <tr><td>II Mistrzostwa Beskidów</td><td class='alignright'>50</td></tr>
  <tr><td>Wielka Łódka 2007 - Lato</td><td class='alignright'>50</td></tr>
  <tr><td>II Klubowe Mistrzostwa Polski</td><td class='alignright'>46</td></tr>
  <tr><td>XI Mistrzostwa Ziemi Słupskiej</td><td class='alignright'>46</td></tr>
  <tr><td>XI Mistrzostwa Piastowa</td><td class='alignright'>40</td></tr>
  <tr><td>X Mistrzostwa Warmii i Mazur</td><td class='alignright'>39</td></tr>
  <tr><td>VII Mistrzostwa Podhala</td><td class='alignright'>35</td></tr>
  <tr><td>X Mistrzostwa Nowego Dworu</td><td class='alignright'>32</td></tr>
  <tr><td>V Mistrzostwa Ziemi Gorzowskiej</td><td class='alignright'>31</td></tr>
  <tr><td>Turniej o puchar Wójta</td><td class='alignright'>28</td></tr>
  <tr><td>I Turniej o Puchar Burmistrza Sulęcina</td><td class='alignright'>18</td></tr>
  <tr><td>III Mistrzostwa Polski Nauczycieli</td><td class='alignright'>18</td></tr>
  <tr><td colspan='2' class='alignright'><b>razem: 2078</b></td></tr>
</table>


<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło (podobnie jak rok wcześniej) <b>20</b> graczy, ale tylko pięciu zanotowało więcej niż jedno zwycięstwo. Pierwsza trójka - <b>
Kazimierz Merklejn jr, Mariusz Skrobosz i Stanisław Rydzik</b> - wygrała po 4 turnieje. Ta trójka zdobyła też medale XV Mistrzostw Polski, lecz w innej kolejności - <b>Stanisław Rydzik (złoto), Kazimierz Merklejn jr (srebro), Mariusz Skrobosz (brąz)</b>. Kazik triumfowal również w klasyfikacji Grand Prix. Swoje pierwsze zwycięstwa turniejowe odniosło 6 graczy - <b>Joanna Juszczak, Dariusz Kosz</b> (najlepszy debiutant w 2006 r.), <b>Dariusz Dzierża, Marek Salamon, Krzysztof Obremski i Wojciech Sołdan.</b> Na podium w sumie stawało aż 46 osób<b>.</b>
Oto <b>klasyfikacja "medalowa"</b> za rok 2007:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2007' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa zmniejszyła się o <b>16</b> osób. Obecnie jest na niej <b>368</b> graczy, w poczekalni - 337 (rok temu 406).
<b>Mateusz Żbikowski</b> utrzymywal pierwsze miejsce do maja, odpierając ataki Kazika Merklejna. Niespodziewanie na czoło wysunął się jednak <b>Mariusz
Skrobosz</b> i uzyskał nawet 5-punktową przewagę. Do czasu - w sierpniu prowadzenie objął w końcu <b>Kazik Merklejn</b>, który jednak na finiszu został
wyprzedzony "o nos" przez <b>Marcina Mroziuka</b>. <br />
Z czołowej dziesiątki wypadli: Kamil Górka, Sławomir Kordjalik, Dawid Pikul i Dariusz Banaszek; awansowali zaś: Stanisław Rydzik (tradycyjne fluktuacje:
11-8-18-8-13-...-4), Mariusz Wrześniewski, Sławomir Kucia i Jarosław Borowski. Gościli tam także: Justyna Górka (8), Robert Duczemiński (6), Andrzej Oleksiak
(7), Radosław Konca (6), Wojciech Zemło (9).<br />
Oto czołówka rankingu i porównanie z końcem roku 2006, a także wykresy przedstawiające zmiany w czołówce (pod względem miejsc i rankingu).
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2006</b></td><td><b>ranking</b></td><td><b>koniec 2007</b></td><td><b>ranking</b></td><td rowspan="11"><br /><img src="rozne/rank2007r.gif" width="442" height="297"></td></tr>
  <tr><td>1.</td><td class="osoba">Mateusz Żbikowski</td><td>148,63</td><td class="osoba">Marcin Mroziuk</td><td>146,74</td></tr>
  <tr><td>2.</td><td class="osoba">Krzysztof Mówka</td><td>148,02</td><td class="osoba">Kazimierz Merklejn jr</td><td>146,29</td></tr>
  <tr><td>3.</td><td class="osoba">Marcin Mroziuk</td><td>144,88</td><td class="osoba">Mariusz Skrobosz</td><td>145,53</td></tr>
  <tr><td>4.</td><td class="osoba">Kazimierz Merklejn jr.</td><td>143,72</td><td class="osoba">Stanisław Rydzik</td><td>145,46</td></tr>
  <tr><td>5.</td><td class="osoba">Mariusz Skrobosz</td><td>141,82</td><td class="osoba">Mariusz Wrześniewski</td><td>144,10</td></tr>
  <tr><td>6.</td><td class="osoba">Kamil Górka</td><td>141,69</td><td class="osoba">Mateusz Żbikowski</td><td>141,39</td></tr>
  <tr><td>7.</td><td class="osoba">Sławomir Kordjalik</td><td>141,07</td><td class="osoba">Krzysztof Mówka</td><td>141,33</td></tr>
  <tr><td>8.</td><td class="osoba">Dawid Pikul</td><td>140,96</td><td class="osoba">Marek Syczuk</td><td>141,15</td></tr>
  <tr><td>9.</td><td class="osoba">Marek Syczuk</td><td>140,86</td><td class="osoba">Sławomir Kucia</td><td>140,99</td></tr>
  <tr><td>10</td><td class="osoba">Dariusz Banaszek</td><td>140,49</td><td class="osoba">Jarosław Borowski</td><td>140,96</td></tr>
</table>
<img src="rozne/rank2007.gif">

<h2>Rankingi osób na miejscach</h2>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td><td><b>koniec 2007</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td><td>146,74</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td><td>140,96</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td><td>137,50</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td><td>129,53</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td><td>122,52</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td><td>116,48</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td><td>111,60</td></tr>
</table>

<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2006 - ranking 2007</b></td><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2006 - ranking 2007</b></td></tr>
  <tr><td>Jacek Gasik</td><td>30,47</td><td>98,86 - 129,33</td><td>Jędrzej Florek</td><td>12,67</td><td>116,17 - 128,84</td></tr>
  <tr><td>Maciej Kuczyński</td><td>17,76</td><td>90,80 - 108,56</td><td>Rafał Dąbrowski</td><td>11,32</td><td>119,26 - 130,58</td></tr>
  <tr><td>Marek Reda</td><td>16,15</td><td>116,88 - 133,03</td><td>Mariusz Sylwestrzuk</td><td>11,25</td><td>115,07 - 126,32</td></tr>
  <tr><td>Przemysław Rybicki</td><td>15,95</td><td>103,86 - 119,81</td><td>Piotr Domański</td><td>10,92</td><td> 99,88 - 110,80</td></tr>
  <tr><td>Dariusz Dzierża</td><td>15,80</td><td>119,22 - 135,02</td><td>Daniel Tomczyk</td><td>10,90</td><td> 100,15 - 111,05</td></tr>
  <tr><td>Mikołaj Fidziński</td><td>15,41</td><td>94,86 - 110,27</td><td>Sławomir Buk</td><td>10,75</td><td> 109,88 - 120,63</td></tr>
  <tr><td>Jan Ziółkowski</td><td>14,79</td><td>99,74 - 114,53</td><td>Roman Figiel</td><td>10,65</td><td> 97,58 - 108,23</td></tr>
  <tr><td>Krzysztof Obremski</td><td>12,98</td><td>121,84 - 134,82</td><td>Anna Stefańska</td><td>10,47</td><td> 101,06 - 111,53</td></tr>
  <tr><td>Michał Makowski</td><td>12,92</td><td>126,28 - 139,20</td><td>Dorota Kuć</td><td>10,45</td><td> 102,53 - 112,98</td></tr>
  <tr><td>Robert Głowacki</td><td>12,81</td><td>107,54 - 120,35</td><td>Robert Kuszpit</td><td>10,31</td><td> 124,05 - 134,36</td></tr>
</table>


<h2>Najlepsze nabytki listy rankingowej</h2>
Młody narybek scrabblowy poczyna sobie coraz śmielej - <b>Marek Salamon</b> zadebiutował w kwietniu w Katowicach (zostając najlepszym debiutantem z 8 zwycięstwami), a w czerwcu wygrał w Zabrzu XII Mistrzostwa Górnego Śląska i Zagłębia. Mało brakowało, a spowodowałby spory przewrót w liście rankingowej. <b>Michał Alabrudziński</b> z Włocławka zajął 2 miejsce w kończącym sezon LeMansie.
<table class="klasyfikacja">
  <tr><td>Marek Salamon</td><td>129,53</td></tr>
  <tr><td>Michał Alabrudziński</td><td>128,60</td></tr>
  <tr><td>Mirosław Uglik</td><td>123,61</td></tr>
  <tr><td>Karol Waniek</td><td>121,00</td></tr>
  <tr><td>Grzegorz Kurowski</td><td>118,12</td></tr>
  <tr><td>Paweł Kowalski</td><td>115,18</td></tr>
  <tr><td>Tomasz Łapka</td><td>114,31</td></tr>
  <tr><td>Sławomir Piotrowski</td><td>112,49</td></tr>
  <tr><td>Łukasz Baranowski</td><td>111,60</td></tr>
  <tr><td>Alicja Kosińska</td><td>108,45</td></tr>
</table>

<h2>Ranking z partii rozegranych w 2007 roku <span>(tylko ci gracze, którzy rozegrali w 2007 r. minimum 60 partii)</span></h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td>ranking</td><td>liczba gier</td></tr>
  <tr><td>Mariusz Wrześniewski</td><td>152,89</td><td>90</td></tr>
  <tr><td>Kazimierz.J Merklejn</td><td>147,47</td><td>258</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>146,48</td><td>308</td></tr>
  <tr><td>Dariusz Kosz</td><td>143,87</td><td>119</td></tr>
  <tr><td>Stanisław Rydzik</td><td>141,56</td><td>313</td></tr>
  <tr><td>Mateusz Żbikowski</td><td>141,39</td><td>179</td></tr>
  <tr><td>Piotr Pietuchowski</td><td>140,67</td><td>83</td></tr>
  <tr><td>Karol Wyrębkiewicz</td><td>140,27</td><td>83</td></tr>
  <tr><td>Justyna Górka</td><td>140,21</td><td>157</td></tr>
  <tr><td>Aleksander Puchalski</td><td>139,62</td><td>63</td></tr>
</table>

Tu można znaleźć <a href="stats/skalp2007.xls">pełną tabelę (.xls)</a>, zawierającą skalpy wszystkich uczestników turniejów w 2007 r. W razie znalezienia błędów
prosimy o <a onClick="sendMail('pfs','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />informacje</a>.

<h2>Najwięcej rozegranych partii w 2007 r.</h2>
W tym roku można było rozegrać 399 partii rankingowych, trochę więcej niż rok temu. Po roku przerwy ponownie triumf w tej klasyfikacji święci
Tomasz Zwoliński. Ponad 300 partii zagrało jeszcze dwóch graczy.
<table class="klasyfikacja">
  <tr><td>Tomasz Zwoliński</td><td>341</td></tr>
  <tr><td>Stanisław Rydzik</td><td>313</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>308</td></tr>
  <tr><td>Krzysztof Sporczyk</td><td>275</td></tr>
  <tr><td>Kazimierz Merklejn jr </td><td>258</td></tr>
  <tr><td>Rafał Wesołowski</td><td>248</td></tr>
  <tr><td>Dawid Pikul</td><td>247</td></tr>
  <tr><td>Urszula Solarska</td><td>246</td></tr>
  <tr><td>Kamil Górka</td><td>236</td></tr>
  <tr><td>Andrzej Rechowicz</td><td>233</td></tr>
  <tr><td>Michał Ozimiński</td><td>205</td></tr>
</table>


<h2>Statystyka</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td><td><b>2007</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td><td>32</td></tr>
  <tr><td>Gracze</td><td>398</td><td>508</td><td>512</td><td>596</td><td>575</td><td>519</td></tr>
  <tr><td>Gry rankingowe</td><td>20 164</td><td>23 068</td><td>22 894</td><td>28 992</td><td>25334</td><td>25002</td></tr>
  <tr><td>Średnia gier na gracza</td><td>50,66</td><td>45,41</td><td>44,71</td><td>48,64</td><td>44,06</td><td>48,17</td></tr>
  <tr><td>Uczestników razem</td><td>1845</td><td>2016</td><td>1934</td><td>2487</td><td>2214</td><td>2078</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>71</td><td>70</td><td>77</td><td>75</td><td>69</td><td>65</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td><td>3</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td><td>9</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td><td>58</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td><td>92</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td><td>346</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td><td>368</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td><td>337</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td><td>705</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td><td>116</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td><td>Wałcz - 146</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td><td>1</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td><td>6</td></tr>
</table>
</div>

<div id='tabs-2006'>
    <h2 id="turnieje">Turnieje</h2>
W 2006 r. odbyły się <b>32</b> turnieje zaliczane do rankingu PFS. Wzięło w nich udział <b>575</b> osób. Rekord liczby uczestników nie został pobity. W Mistrzostwach Polski wzięło udział 127 graczy, w Ostródzie - 126. Po ośmiu latach przerwy wróciły do kalendarza <b>Mistrzostwa Wrocławia</b> i gościły aż 113 graczy.
Łódzki Klub Miłośników Scrabble zorganizował kolejną Wielką Łódkę - cykl 4 turniejów pod nazwą <b>"Wielka Łódka 2006"</b> - Zima, Wiosna, Lato, Jesień. Nowe miasta na turniejowej mapie to <b>Graboszyce</b>, <b>Milanówek</b> i <b>Wisła</b>. W Wałczu rozegrano po raz pierwszy <b>Klubowe Mistrzostwa Polski</b>a także <b>II Mistrzostwa Polski Nauczycieli</b>. Jubileusz dziesięciolecia świętowały turnieje w Inowrocławiu, Piastowie, Krakowie i Słupsku. Liczba uczestników na turniejach przedstawia się następująco:

<table class="zapisy">
  <tr><td>XIV Mistrzostwa Polski</td><td class='alignright'>127</td></tr>
  <tr><td>VI Mistrzostwa Ziemi Ostródzkiej</td><td class='alignright'>126</td></tr>
  <tr><td>II Mistrzostwa Wrocławia</td><td class='alignright'>113</td></tr>
  <tr><td>II Mistrzostwa Wałcza</td><td class='alignright'>100</td></tr>
  <tr><td>X Mistrzostwa Krakowa</td><td class='alignright'>97</td></tr>
  <tr><td>VI Le Mans</td><td class='alignright'>95</td></tr>
  <tr><td>VIII Mistrzostwa Szczecina "Zanim Zakwitną Magnolie"</td><td class='alignright'>89</td></tr>
  <tr><td>III Mistrzostwa Katowic</td><td class='alignright'>88</td></tr>
  <tr><td>Wałcz - Dobijak</td><td class='alignright'>82</td></tr>
  <tr><td>XII Puchar Polski</td><td class='alignright'>80</td></tr>
  <tr><td>Wielka Łódka 2006 - Zima</td><td class='alignright'>78</td></tr>
  <tr><td>I Mistrzostwa Milanówka</td><td class='alignright'>72</td></tr>
  <tr><td>X Mistrzostwa Ziemi Kujawskiej</td><td class='alignright'>66</td></tr>
  <tr><td>II Mistrzostwa Iławy</td><td class='alignright'>66</td></tr>
  <tr><td>XII Mistrzostwa Łodzi</td><td class='alignright'>65</td></tr>
  <tr><td>I Mistrzostwa Doliny Karpia</td><td class='alignright'>63</td></tr>
  <tr><td>I Mistrzostwa Doliny Karpia</td><td class='alignright'>62</td></tr>
  <tr><td>V Mistrzostwa Bydgoszczy</td><td class='alignright'>61</td></tr>
  <tr><td>V Mistrzostwa Wybrzeża</td><td class='alignright'>60</td></tr>
  <tr><td>VIII Mistrzostwa Podlasia</td><td class='alignright'>60</td></tr>
  <tr><td>Wielka Łódka 2006 - Wiosna</td><td class='alignright'>58</td></tr>
  <tr><td>IX Mistrzostwa Warmii i Mazur</td><td class='alignright'>58</td></tr>
  <tr><td>X Mistrzostwa Piastowa</td><td class='alignright'>56</td></tr>
  <tr><td>I Mistrzostwa Beskidów</td><td class='alignright'>55</td></tr>
  <tr><td>IX Mistrzostwa Podbeskidzia</td><td class='alignright'>55</td></tr>
  <tr><td>IX Mistrzostwa Nowego Dworu Mazowieckiego</td><td class='alignright'>54</td></tr>
  <tr><td>II Powitanie Lata</td><td class='alignright'>51</td></tr>
  <tr><td>Wielka Łódka Lato</td><td class='alignright'>48</td></tr>
  <tr><td>X Mistrzostwa Słupska</td><td class='alignright'>47</td></tr>
  <tr><td>VI Mistrzostwa Podhala</td><td class='alignright'>36</td></tr>
  <tr><td>I Mistrzostwa Polski Klubów</td><td class='alignright'>32</td></tr>
  <tr><td>II Mistrzostwa Polski Nauczycieli</td><td class='alignright'>14</td></tr>
  <tr><td colspan='2' class='alignright'><b>razem: 2214</b></td></tr>
</table>


<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>20</b> graczy. Klasyfikacja medalowa była bardzo wyrównana, w pierwszej połowie roku każdy turniej miał innego triumfatora, wyjątkami były tylko dwa zwycięstwa Dawida Pikula i Marka Syczuka. Ostatecznie na pierwsze miejsce wysunął się nowy Mistrz Polski <b>Krzysztof Mówka</b>, z 4 zwycięstwami.&nbsp; <b>3</b> zwycięstwa zanotowali <b>Dawid Pikul</b> i <b>Mateusz Żbikowski</b>. Swoje pierwsze zwycięstwa odniosło 4 graczy - <b>
Paweł Jackowski, Łukasz Tuszyński,&nbsp; Filip Sosin i Robert Kamiński.</b> Na podium w sumie stawało aż 37 osób.
<br /><br />
Oto <b>klasyfikacja "medalowa"</b> za rok 2006:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2006' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.


<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa powiększyła się o <b>27</b> osób. Obecnie jest na niej <b>384</b> graczy, w poczekalni - 406 (rok temu 387).
Na początku sezonu na pierwsze miejsce w rankingu wszedł ponownie <b>Mateusz Żbikowski</b> i prowadzenia nie oddał przez cały rok. Po drodze ustanowił nowy rekord
rankingu (<b>150.63</b>) i uzyskał sześciopunktową przewagę nad drugim zawodnikiem. W końcówce sezonu duży skok zanotował AMP <b>Krzysztof Mówka</b> i doszedł lidera na pół punktu. Wcześniej drugie miejsce osiągali <b>Mariusz Skrobosz</b>, <b>Marcin Mroziuk</b> i <b>Kazik Merklejn</b>. <br />Z czołowej dziesiątki wypadli: Szymon Fidziński, Wojciech Usakiewicz i Tomasz Zwoliński; awansowali zaś Marcin Mroziuk, Dawid Pikul (najwyższa pozycja - 3), Marek Syczuk. Stanisław Rydzik orbitował między miejscem 5 a 11. <br /><br />
Oto czołówka rankingu i porównanie z końcem roku 2005, oraz wykresy przedstawiające zmiany w czołówce (pod względem miejsc i rankingu)

<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2005</b></td><td><b>ranking</b></td><td><b>koniec 2006</b></td><td><b>ranking</b></td>
    <td rowspan="11"><img src="rozne/rank2006r.gif"></td></tr>
  <tr><td>1.</td><td>Kazimierz Merklejn jr.</td><td>146,00</td><td>Mateusz Żbikowski</td><td>148,63</td></tr>
  <tr><td>2.</td><td>Mateusz Żbikowski</td><td>145,21</td><td>Krzysztof Mówka</td><td>148,02</td></tr>
  <tr><td>3.</td><td>Szymon Fidziński</td><td>143,26</td><td>Marcin Mroziuk</td><td>144,88</td></tr>
  <tr><td>4.</td><td>Mariusz Skrobosz</td><td>142,51</td><td>Kazimierz Merklejn jr.</td><td>143,72</td></tr>
  <tr><td>5.</td><td>Kamil Górka</td><td>142,17</td><td>Mariusz Skrobosz</td><td>141,82</td></tr>
  <tr><td>6.</td><td>Wojciech Usakiewicz</td><td>141,32</td><td>Kamil Górka</td><td>141,69</td></tr>
  <tr><td>7.</td><td>Krzysztof Mówka</td><td>140,98</td><td>Sławomir Kordjalik</td><td>141,07</td></tr>
  <tr><td>8.</td><td>Sławomir Kordjalik</td><td>140,05</td><td>Dawid Pikul</td><td>140,96</td></tr>
  <tr><td>9.</td><td>Tomasz Zwoliński</td><td>139,56</td><td>Marek Syczuk</td><td>140,86</td></tr>
  <tr><td>10</td><td>Stanisław Rydzik</td><td>139,33</td><td>Dariusz Banaszek</td><td>140,49</td></tr>
</table>
<img src="rozne/rank2006.gif">


<h2>Rankingi osób na miejscach</h2>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td><td><b>koniec 2006</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td><td>148,63</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td><td>140,49</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td><td>135,96</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td><td>127,04</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td><td>120,23</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td><td>116,17</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td><td>110,90</td></tr>
</table>


<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2004 &#8594; ranking 2005</b></td><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2004 &#8594; ranking 2005</b></td></tr>
  <tr><td>Marek Reda</td><td>16,16</td><td>100,72 &#8594; 116,88</td><td>Jarosław Żuliński</td><td>11,09</td><td>97,02 &#8594; 108,29</td></tr>
  <tr><td>Wojciech Sołdan</td><td>15,28</td><td>112,38 &#8594; 127,66</td><td>Bartosz Orłowski</td><td>10,69</td><td>108,55 &#8594; 119,24</td></tr>
  <tr><td>Piotr Jakubiak</td><td>15,20</td><td>101,09 &#8594; 116,29</td><td>Miłosz Wrzałek</td><td>10,49</td><td>123,79 &#8594; 134,28</td></tr>
  <tr><td>Andrzej Kowalski</td><td>14,44</td><td>121,52 &#8594; 135,96</td><td>Leszek Stecuła</td><td>10,43</td><td> 116,11 &#8594; 126,54</td></tr>
  <tr><td>Piotr Łasowski</td><td>12,68</td><td>90,37 &#8594; 103,05</td><td>Remigiusz Skwarek</td><td>10,05</td><td> 104,92 &#8594; 114,97</td></tr>
  <tr><td>Mikołaj Przybyłowicz</td><td>12,61</td><td>102,26 &#8594; 114,87</td><td>Magdalena.Maja Rusak</td><td>9,65</td><td> 98,69 &#8594; 108,34</td></tr>
  <tr><td>Łukasz Miłosz</td><td>12,20</td><td>106,06 &#8594; 118,26</td><td>Damian Krężel</td><td>9,49</td><td> 110,23 &#8594; 119,72</td></tr>
  <tr><td>Urszula Solarska</td><td>11,81</td><td>82,93 &#8594; 94,74</td><td>Henryk Ryś</td><td>9,44</td><td> 95,97 &#8594; 105,41</td></tr>
  <tr><td>Wojciech Wróbel</td><td>11,27</td><td>86,35 &#8594; 97,62</td><td>Robert Kuszpit</td><td>9,40</td><td> 114,65 &#8594; 124,05</td></tr>
  <tr><td>Katarzyna Sołdan</td><td>11,14</td><td>108,00 &#8594; 119,14</td><td>Jakub Nalepa</td><td>9,27</td><td> 106,25 &#8594; 115,52</td></tr>
</table>


<h2>Najlepsze nabytki listy rankingowej</h2>
Wśród osiągnięć młodego narybku scrabblowego na największą uwagę zasługuje czwarte miejsce w Mistrzostwach Polski zdobyte przez <b>Pawła Janasia</b> z
Głogowa. Paweł debiutował w marcu w Szczecinie, rozegrał w tym roku ponad 150 partii i zakończył sezon z rankingiem 125,55. Z wyższym wynikiem zakończyli rok
2006 inni debiutanci - <b>Dariusz Kosz</b> (4 miejsce w Bielsku-Białej) i <b>Piotr Szałkiewicz </b>(14 miejsce w debiucie we Wrocławiu).
<table class="klasyfikacja">
  <tr><td>Dariusz Kosz</td><td>128,94</td></tr>
  <tr><td>Piotr Szałkiewicz</td><td>128,93</td></tr>
  <tr><td>Paweł Janaś</td><td>125,55</td></tr>
  <tr><td>Rafał Lenartowski</td><td>123,11</td></tr>
  <tr><td>Marta Dobrzańska</td><td>120,74</td></tr>
  <tr><td>Mariusz Grodek</td><td>118,19</td></tr>
  <tr><td>Magdalena Kublik</td><td>117,19</td></tr>
  <tr><td>Marek Herzig</td><td>115,67</td></tr>
  <tr><td>Piotr Szutenberg</td><td>115,00</td></tr>
  <tr><td>Leszek Jóźwik</td><td>109,95</td></tr>
</table>


<h2>Ranking z partii rozegranych w 2006 roku <span>(tylko ci gracze, którzy rozegrali w 2006 r. minimum 30 partii)</span></h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td>ranking</td><td>liczba gier</td></tr>
  <tr><td>Marcin Mroziuk</td><td>148,98</td><td>52</td></tr>
  <tr><td>Mateusz Żbikowski</td><td>148,63</td><td>199</td></tr>
  <tr><td>Krzysztof Mówka</td><td>148,02</td><td>188</td></tr>
  <tr><td>Jarosław Borowski</td><td>144,90</td><td>42</td></tr>
  <tr><td>Andrzej Oleksiak</td><td>143,76</td><td>75</td></tr>
  <tr><td>Dawid Pikul</td><td>143,18</td><td>312</td></tr>
  <tr><td>Kazimierz.J Merklejn</td><td>142,99</td><td>295</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>142,79</td><td>292</td></tr>
  <tr><td>Radosław Konca</td><td>142,44</td><td>63</td></tr>
  <tr><td>Kamil Górka</td><td>141,54</td><td>201</td></tr>
</table>

Tu można znaleźć <a href="stats/skalp2006.xls">pełną tabelę (.xls)</a>, zawierającą skalpy wszystkich uczestników turniejów w 2006 r. W razie znalezienia błędów
prosimy o <a onClick="sendMail('pfs','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />informacje</a>.


<h2>Najwięcej rozegranych partii w 2006 r.</h2>
W tym roku można było rozegrać 386 partii rankingowych, trochę mniej niż rok temu. Chyba po raz pierwszy w historii najwięcej partii rozegrał ktoś inny niż Tomasz Zwoliński :) Dawid Pikul miał na swoim koncie 312 partii, Tomek tylko jedną mniej, tyle samo co Stanisław Rydzik.
<table class="klasyfikacja">
  <tr><td>Dawid Pikul</td><td>312</td></tr>
  <tr><td>Tomasz Zwoliński</td><td>311</td></tr>
  <tr><td>Stanisław Rydzik</td><td>311</td></tr>
  <tr><td>Kazimierz Merklejn jr </td><td>295</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>292</td></tr>
  <tr><td>Andrzej Rechowicz</td><td>279</td></tr>
  <tr><td>Wojciech Sołdan</td><td>252</td></tr>
  <tr><td>Katarzyna Sołdan</td><td>246</td></tr>
  <tr><td>Urszula Solarska</td><td>246</td></tr>
  <tr><td>Rafał Wesołowski</td><td>233</td></tr>
  <tr><td>Joanna Ostrowska</td><td>233</td></tr>
</table>

<h2>Statystyka</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td><td><b>2006</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td><td>32</td></tr>
  <tr><td>Gracze</td><td>398</td><td>508</td><td>512</td><td>596</td><td>575</td></tr>
  <tr><td>Gry rankingowe</td><td>20 164</td><td>23 068</td><td>22 894</td><td>28 992</td><td>25334</td></tr>
  <tr><td>&brvbar;rednia gier na gracza</td><td>50,66</td><td>45,41</td><td>44,71</td><td>48,64</td><td>44,06</td></tr>
  <tr><td>Uczestników razem</td><td>1845</td><td>2016</td><td>1934</td><td>2487</td><td>2214</td></tr>
  <tr><td>&brvbar;rednia liczba uczestników turnieju</td><td>71</td><td>70</td><td>77</td><td>75</td><td>69</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td><td>3</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td><td>12</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td><td>46</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td><td>107</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td><td>407</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td><td>384</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td><td>406</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td><td>790</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td><td>172</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td><td>MP - 127</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td><td>3</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td><td>4</td></tr>
</table>
</div>

<div id='tabs-2005'>
    <h2 id="turnieje">Turnieje</h2>
W 2005 r. odbyły się <b>33</b> turnieje zaliczane do rankingu PFS, czyli aż 8 więcej niż rok temu. Wzięło w nich udział <b>596</b> osób. Turniej (ba! Megaturniej) w
<b>Ostródzie</b> zgromadził <b>167</b> osób! Łódzki Klub Miłośników Scrabble przygotował cykl 4 turniejów pod nazwą <b>"Wielka Łódka 2005"</b> - Zima, Wiosna, Lato, Jesień. Zagrało w nich 145 osób (łączna liczba uczestników cyklu - 300). Nowe miasta na turniejowej mapie to <b>Iława</b> i <b>Wałcz</b>. W Wałczu rozegrano także po raz pierwszy <b>Mistrzostwa Polski Nauczycieli</b>. W Nowym Mieście Lubawskim walczono o tytuł <b>Mistrza Polski w Scrabble po Angielsku</b> i wyjazd na Mistrzostwa Świata w Londynie. <br />Po roku przerwy rozegrano w Chojnowie <b>Mistrzostwa Dolnego Śląska</b></a> i <b>Maraton Scrabblowy</b> w Katowicach, po trzech - <b>Mistrzostwa Szczecina</b>, a po ośmiu - <b>Mistrzostwa Katowic</b>.<br /><br />
Liczba uczestników na turniejach przedstawia się następująco:

<table class="zapisy">
  <tr><td>V Mistrzostwa Ziemi Ostródzkiej "Blanki w szranki" (Grand Prix 2005)</td><td class='alignright'>167</td></tr>
  <tr><td>X Mistrzostwa Warszawy (Grand Prix 2005)</td><td class='alignright'>130</td></tr>
  <tr><td>Turniej Otwarcia Sezonu (Warszawa)</td><td class='alignright'>127</td></tr>
  <tr><td>XIII Mistrzostwa Polski</td><td class='alignright'>126</td></tr>
  <tr><td>XI Puchar Polski</td><td class='alignright'>108</td></tr>
  <tr><td>II Mistrzostwa Katowic</td><td class='alignright'>106</td></tr>
  <tr><td>IX Mistrzostwa Krakowa (Grand Prix 2005)</td><td class='alignright'>102</td></tr>
  <tr><td>V Turniej 24 Non Stop "Le Mans"</td><td class='alignright'>97</td></tr>
  <tr><td>IV Mistrzostwa Poznania</td><td class='alignright'>93</td></tr>
  <tr><td>Wielka Łódka 2005 - Zima</td><td class='alignright'>91</td></tr>
  <tr><td>IV Mistrzostwa Wybrzeża (Rumia)</td><td class='alignright'>87</td></tr>
  <tr><td>Powitanie lata (Warszawa)</td><td class='alignright'>86</td></tr>
  <tr><td>IX Mistrzostwa Ziemi Kujawskiej (Inowrocław, Grand Prix 2005)</td><td class='alignright'>79</td></tr>
  <tr><td>X Mistrzostwa Górnego Śląska i Zagłębia (Zabrze, Grand Prix 2005)</td><td class='alignright'>77</td></tr>
  <tr><td>IV Mistrzostwa Bydgoszczy (Grand Prix 2005)</td><td class='alignright'>74</td></tr>
  <tr><td>Wielka Łódka 2005 - Wiosna</td><td class='alignright'>74</td></tr>
  <tr><td>XI Mistrzostwa Łodzi (Grand Prix 2005)</td><td class='alignright'>72</td></tr>
  <tr><td>I Mistrzostwa Iławy</td><td class='alignright'>63</td></tr>
  <tr><td>Wielka Łódka 2005 - Lato</td><td class='alignright'>63</td></tr>
  <tr><td>IV Maraton Scrabblowy - Katowice</td><td class='alignright'>62</td></tr>
  <tr><td>VII Mistrzostwa Szczecina - Turniej Kwitnącej Magnolii</td><td class='alignright'>60</td></tr>
  <tr><td>IV Mistrzostwa Lublina (Grand Prix 2005)</td><td class='alignright'>58</td></tr>
  <tr><td>VII Mistrzostwa Podlasia (Bielsk Podlaski)</td><td class='alignright'>57</td></tr>
  <tr><td>VIII Mistrzostwa Nowego Dworu Mazowieckiego</td><td class='alignright'>55</td></tr>
  <tr><td>I Mistrzostwa Wałcza</td><td class='alignright'>53</td></tr>
  <tr><td>IV Mistrzostwa Ziemi Gorzowskiej</td><td class='alignright'>52</td></tr>
  <tr><td>VIII Mistrzostwa Podbeskidzia (Bielsko-Biała, Grand Prix 2005)</td><td class='alignright'>51</td></tr>
  <tr><td>IX Mistrzostwa Ziemi Słupskiej</td><td class='alignright'>47</td></tr>
  <tr><td>VIII Mistrzostwa Warmii i Mazur (Nowe Miasto Lubawskie)</td><td class='alignright'>44</td></tr>
  <tr><td>Puchar Wójta Kościerzyny (Szarlota)</td><td class='alignright'>40</td></tr>
  <tr><td>V Mistrzostwa Podhala (Zakopane)</td><td class='alignright'>32</td></tr>
  <tr><td>IV Mistrzostwa Dolnego Śląska (Chojnów)</td><td class='alignright'>32</td></tr>
  <tr><td>III Mistrzostwa Bałtyku (Jurata)</td><td class='alignright'>22</td></tr>
  <tr><td colspan='2' class='alignright'><b>razem: 2487</b></td></tr>
</table>
W tym roku odbyły się także dwa turnieje "Bomba w górę" zorganizowane przez warszawski Garden Club (niezaliczane do rankingu). Status turnieju nierankingowego miały również IX Mistrzostwa Piastowa.<br />
Scrabbliści wczasowali w Zakopanem, Szarlocie i Juracie.<br /><br />

<b>Wyniki ankiet po turniejach:</b>
<table class="klasyfikacja">
  <tr><td>(skala od 1 do 5)</td><td>Przygotowanie turnieju</td><td>Noclegi i wyżywienie</td><td>Nagrody</td><td>Sędziowanie</td><td>liczba</td><td>sędzia</td></tr>
  <tr><td>Turniej Otwarcia Sezonu</td><td>3,47</td><td>4,08</td><td>3,59</td><td>3,35</td><td>34</td><td>G. Wiączkowski</td></tr>
  <tr><td>Wielka Łódka Zima</td><td>4,03</td><td>3,78</td><td>3,08</td><td>4,72</td><td>29</td><td>A. Gostomski</td></tr>
  <tr><td>Warszawa</td><td>3,74</td><td>2,37</td><td>2,75</td><td>2,96</td><td>27</td><td>A. Lożyński</td></tr>
  <tr><td>Rumia</td><td>4,69</td><td>4,83</td><td>4,10</td><td>2,69</td><td>13</td><td>A. Lożyński</td></tr>
  <tr><td>Katowice</td><td>4,15</td><td>3,77</td><td>3,69</td><td>4,84</td><td>13</td><td>A. Gostomski</td></tr>
  <tr><td>Inowrocław</td><td>3,17</td><td>2,28</td><td>4,31</td><td>3,96</td><td>29</td><td>A. Lożyński</td></tr>
  <tr><td>Szczecin</td><td>4,64</td><td>4,53</td><td>4,33</td><td>4,29</td><td>31</td><td>A. Gostomski</td></tr>
  <tr><td>Bydgoszcz</td><td>4,41</td><td>4,24</td><td>4,82</td><td>4,00</td><td>17</td><td>A. Lożyński</td></tr>
  <tr><td>Wielka Łódka Wiosna</td><td>3,68</td><td>3,78</td><td>3,67</td><td>4,68</td><td>19</td><td>A. Gostomski</td></tr>
  <tr><td>Iława</td><td>4,73</td><td>4,66</td><td>4,35</td><td>4,11</td><td>37</td><td>A. Lożyński</td></tr>
  <tr><td>Bielsk Podlaski</td><td>4,36</td><td>4,08</td><td>4,32</td><td>4,64</td><td>25</td><td>A. Gostomski</td></tr>
  <tr><td>Zabrze</td><td>4,63</td><td>4,05</td><td>4,00</td><td>3,10</td><td>19</td><td>A. Lożyński</td></tr>
  <tr><td>Chojnów</td><td>4,80</td><td>4,38</td><td>4,60</td><td>4,90</td><td>20</td><td>A. Gostomski</td></tr>
  <tr><td>Ostróda</td><td>4,88</td><td>4,46</td><td>4,35</td><td>4,72</td><td>43</td><td>A. Gostomski</td></tr>
  <tr><td>Wielka Łódka Lato</td><td>3,64</td><td>3,43</td><td>3,24</td><td>4,70</td><td>17</td><td>A. Gostomski</td></tr>
  <tr><td>Słupsk</td><td>2,44</td><td>2,33</td><td>2,78</td><td>2,78</td><td>9</td><td>A. Lożyński</td></tr>
  <tr><td>Maraton Katowice</td><td>4,21</td><td>3,04</td><td>3,68</td><td>3,36</td><td>28</td><td>A. Lożyński</td></tr>
  <tr><td>Kraków</td><td>4,28</td><td>4,25</td><td>4,63</td><td>4,96</td><td>28</td><td>A. Gostomski</td></tr>
  <tr><td>Lublin</td><td>4,80</td><td>4,32</td><td>4,65</td><td>4,81</td><td>21</td><td>K. Wyrębkiewicz</td></tr>
  <tr><td>Bielsko-Biała</td><td>4,63</td><td>4,61</td><td>4,21</td><td>4,79</td><td>19</td><td>A. Gostomski</td></tr>
  <tr><td>Wałcz</td><td>4,98</td><td>4,96</td><td>4,88</td><td>4,88</td><td>46</td><td>K. Wyrębkiewicz</td></tr>
  <tr><td>Poznań</td><td>4,56</td><td>4,42</td><td>4,62</td><td>3,69</td><td>48</td><td>A. Lożyński</td></tr>
  <tr><td>Nowe Miasto Lubawskie</td><td>4,44</td><td>4,33</td><td>4,22</td><td>2,33</td><td>&nbsp;</td><td>A. Lożyński</td></tr>
  <tr><td>Nowy Dwór Mazowiecki</td><td>4,17</td><td>4,07</td><td>4,56</td><td>3,94</td><td>17</td><td>A. Lożyński</td></tr>
  <tr><td>Łódź</td><td>4,57</td><td>4,32</td><td>4,14</td><td>4,78</td><td>28</td><td>J. Puchalski</td></tr>
</table>


<h2 id="zwyciezcy">Zwycięzcy</h2>
Na najwyższym stopniu podium stanęło <b>18</b> graczy. Najwięcej turniejów - 5 - wygrał <b>Kazimierz Merklejn jr</b>, i to w imponującym stylu - po jednym w
lipcu, sierpniu, 2 we wrześniu i ostatni w październiku. 4 turnieje wygrał <b>Tomasz Zwoliński</b> (w tym Puchar Polski po raz drugi). <b>3</b> zwycięstwa zanotowali <b>Mariusz Skrobosz</b> (w tym Mistrzostwa Polski... po raz drugi) i <b>Stanisław Rydzik</b>. Swoje pierwsze zwycięstwa odniosło aż 7 graczy - <b> Sławomir Kordjalik, Aleksander Puchalski,&nbsp; Grzegorz Święcki, Miłosz Wrzałek, Michał Trumpus, Dawid Pikul i Grzegorz Koczkodon. </b>W dwóch turniejach triumfatorzy znokautowali rywali 12:0 - <b>Justyna Fugiel </b>uczyniła to w Zabrzu, a tydzień później <b>Andrzej Oleksiak</b> w Chojnowie.<br /><br />
Oto <b>klasyfikacja "medalowa"</b> za rok 2005:

<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2005' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.




<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa powiększyła się o <b>38</b> osób. Obecnie jest na niej <b>357</b> graczy, w poczekalni - 387 (rok temu 396). W czołówce rankingu sporo się działo. Rok 2005 na fotelu lidera rozpoczął ze sporą przewagą <b>Mateusz Żbikowski</b>, ale prowadził tylko do kwietnia. Przez chwilę na czoło stawki powrócił <b>Mariusz Skrobosz</b>, ale wkrótce wypadł nawet z dziesiątki, a także <b>Kazimierz Merklejn jr</b>. W maju po raz pierwszy liderem rankingu został <b>Szymon Fidziński</b> - spora przewaga (ok. 4 punkty) stopniała we wrześniu, kiedy to po serii zwycięstw na pierwsze miejsce wysforował się <b>Kazimierz Merklejn jr</b> i utrzymał je do końca roku (w międzyczasie osiągając ranking <b>148,72</b>).<br />
Z czołowej dziesiątki wypadł Mistrz Polski <b>Mariusz Wrześniewski</b>, <b>Szymon Zaleski</b> i <b>Maciej Gałecki</b>, zaś awansowali <b>Kamil Górka</b>, <b>Sławomir Kordjalik</b> i <b>Stanisław Rydzik.</b><br /><br />
Oto czołówka rankingu i porównanie z końcem roku 2004, oraz wykresy przedstawiające zmiany w czołówce (pod względem miejsc i rankingu)

<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2004</b></td><td><b>ranking</b></td><td><b>koniec 2005</b></td><td><b>ranking</b></td><td rowspan="11">
    <img src="rozne/rank2005r.gif"></td></tr>
  <tr><td>1.</td><td>Mateusz Żbikowski</td><td>145,73</td><td>Kazimierz Merklejn jr.</td><td>146,00</td></tr>
  <tr><td>2.</td><td>Kazimierz Merklejn jr.</td><td>143,06</td><td>Mateusz Żbikowski</td><td>145,21</td></tr>
  <tr><td>3.</td><td>Szymon Fidziński</td><td>141,59</td><td>Szymon Fidziński</td><td>143,26</td></tr>
  <tr><td>4.</td><td>Wojciech Usakiewicz</td><td>141,39</td><td>Mariusz Skrobosz</td><td>142,51</td></tr>
  <tr><td>5.</td><td>Krzysztof Mówka</td><td>141,12</td><td>Kamil Górka</td><td>142,17</td></tr>
  <tr><td>6.</td><td>Mariusz Skrobosz</td><td>140,77</td><td>Wojciech Usakiewicz</td><td>141,32</td></tr>
  <tr><td>7.</td><td>Szymon Zaleski</td><td>139,92</td><td>Krzysztof Mówka</td><td>140,98</td></tr>
  <tr><td>8.</td><td>Mariusz Wrześniewski</td><td>139,48</td><td>Sławomir Kordjalik</td><td>140,05</td></tr>
  <tr><td>9.</td><td>Maciej Gałecki</td><td>139,42</td><td>Tomasz Zwoliński</td><td>139,56</td></tr>
  <tr><td>10</td><td>Tomasz Zwoliński</td><td>139,23</td><td>Stanisław Rydzik</td><td>139,33</td></tr>
</table>
<img src="rozne/rank2005.gif"><br /><br/>
Najwyższy i najniższy ranking w czołówce są podobne jak rok temu.&nbsp; Jednak więcej osób przekroczyło ranking 140. W <a href="rank050612.txt">czerwcu</a>
aż 11 osób miało ranking ponad 140.<br /><br />


<h2>Rankingi osób na miejscach</h2>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td><td><b>koniec 2005</b></td></tr>
  <tr><td><b>1</b></td><td>143,47</td><td>144,94</td><td>145,73</td><td>146,00</td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td><td>139,33</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td><td>134,45</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td><td>126,02</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td><td>118,56</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td><td>113,84</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td><td>108,55</td></tr>
</table>



<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2004&#8594; ranking 2005</b></td><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2004&#8594; ranking 2005</b></td></tr>
  <tr><td>Barbara Szymańska</td><td>24,29</td><td>71,22 &#8594;95,51</td><td>Michał Trumpus</td><td>11,54</td><td>118,06 &#8594;129,60</td></tr>
  <tr><td>Paweł Czubla</td><td>18,53</td><td>99,44 &#8594;117,97</td><td>Damian Krężel</td><td>11,51</td><td>98,72 &#8594;110,23</td></tr>
  <tr><td>Iza Kraj</td><td>16,46</td><td>87,61 &#8594;104,07</td><td>Michał Sosin</td><td>11,51</td><td>105,47 &#8594;116,98</td></tr>
  <tr><td>Remigiusz Skwarek</td><td>16,32</td><td>88,60 &#8594;104,92</td><td>Artur Koszel</td><td>11,34</td><td> 114,68 &#8594;126,02</td></tr>
  <tr><td>Łukasz Miłosz</td><td>15,29</td><td>90,77 &#8594;106,06</td><td>Marek Rogalski</td><td>11,31</td><td> 100,88 &#8594;112,19 </td></tr>
  <tr><td>Anna Niedzielko</td><td>14,36</td><td>96,99 &#8594;111,35</td><td>Tomasz Ciejka</td><td>11,19</td><td> 104,55 &#8594;115,74</td></tr>
  <tr><td>Tomasz Lempart</td><td>13,46</td><td>106,71 &#8594;120,17</td><td>Filip Sosin</td><td>11,07</td><td> 119,68 &#8594;130,75</td></tr>
  <tr><td>Lucyna Korga</td><td>12,62</td><td>121,83 &#8594;134,45</td><td>Małgorzata Sameryt</td><td>10,99</td><td> 95,58 &#8594;106,57</td></tr>
  <tr><td>Łukasz Święcki</td><td>12,21</td><td>97,62 &#8594;109,83</td><td>Paweł Stefaniak</td><td>10,94</td><td> 111,37 &#8594;122,31</td></tr>
  <tr><td>Mariusz Skrzypczyński</td><td>12,14</td><td>105,83 &#8594;117,97</td><td>Przemysław Herdzina</td><td>10,92</td><td> 111,85 &#8594;122,77</td></tr>
</table>


<h2>Najlepsze nabytki listy rankingowej</h2>
Na II Mistrzostwach Katowic siódme miejsce z wynikiem 9:3 zajął debiutujący Michał Gawroński z Częstochowy. W pierwszej dziesiątce najlepszych nabytków listy rankingowej są aż cztery osoby ze Szczecińskiego Klubu Miłośników Scrabble "Blank" (Tuszyński, Zaryński, Zadrożna, Książek). Widać trening pod czujnym okiem Mistrza Polski 2003 i lidera rankingu Kazika Merklejna jr daje efekty :-)
<table class="klasyfikacja">
  <tr><td>Michał Gawroński</td><td>121,45</td></tr>
  <tr><td>Łukasz Tuszyński</td><td>121,18</td></tr>
  <tr><td>Jakub Zaryński</td><td>120,35</td></tr>
  <tr><td>Joanna Zadrożna</td><td>120,25</td></tr>
  <tr><td>Mateusz Zalega</td><td>119,86</td></tr>
  <tr><td>Bogusław Książek</td><td>116,92</td></tr>
  <tr><td>Tomasz Ruta</td><td>116,83</td></tr>
  <tr><td>Małgorzata Szczygieł</td><td>116,08</td></tr>
  <tr><td>Radosław Jachna</td><td>115,75</td></tr>
  <tr><td>Łukasz Szałkiewicz</td><td>114,93</td></tr>
</table>


<h2>Ranking z partii rozegranych w 2005 roku <span>(tylko ci gracze, którzy rozegrali w 2005 r. minimum 30 partii)</span></h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td>ranking</td><td>liczba gier</td></tr>
  <tr><td>Dariusz Banaszek</td><td>145,58</td><td>33</td></tr>
  <tr><td>Kazimierz Merklejn jr</td><td>145,01</td><td>348</td></tr>
  <tr><td>Mateusz Żbikowski</td><td>143,64</td><td>260</td></tr>
  <tr><td>Kamil Górka</td><td>142,17</td><td>190</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>142,03</td><td>320</td></tr>
  <tr><td>Krzysztof Mówka</td><td>141,12</td><td>207</td></tr>
  <tr><td>Sławomir Kordjalik</td><td>139,71</td><td>217</td></tr>
  <tr><td>Tomasz Zwoliński</td><td>138,54</td><td>352</td></tr>
  <tr><td>Justyna Fugiel</td><td>138,06</td><td>143</td></tr>
  <tr><td>Szymon Fidziński</td><td>138,00</td><td>68</td></tr>
</table>
Tu można znaleźć <a href="stats/skalp2005.xls">pełną tabelę (.xls)</a>, zawierającą skalpy wszystkich uczestników turniejów w 2005 r. W razie znalezienia błędów
prosimy o <a onClick="sendMail('pfs','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />informacje</a>.


<h2>Najwięcej rozegranych partii w 2005 r.</h2>
W tym roku można było rozegrać 408 partii rankingowych - aż o 30% więcej niż rok temu. Tradycyjnie najwięcej gier miał na swoim koncie Tomasz Zwoliński, tuż za nim był Kazimierz Merklejn jr (ale pod względem przejechanych kilometrów bije wszystkich :-)).
<table class="klasyfikacja">
  <tr><td>Tomasz Zwoliński</td><td>352</td></tr>
  <tr><td>Kazimierz Merklejn jr </td><td>348</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>320</td></tr>
  <tr><td>Dawid Pikul</td><td>317</td></tr>
  <tr><td>Katarzyna Sołdan</td><td>305</td></tr>
  <tr><td>Rafał Wesołowski</td><td>293</td></tr>
  <tr><td>Karol Wyrębkiewicz</td><td>273</td></tr>
  <tr><td>Stanisław Rydzik</td><td>264</td></tr>
  <tr><td>Mateusz Żbikowski</td><td>260</td></tr>
  <tr><td>Krzysztof Sporczyk</td><td>251</td></tr>
</table>

<h2>Statystyka</h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>2002</b></td><td><b>2003</b></td><td><b>2004</b></td><td><b>2005</b></td></tr>
  <tr><td>Turnieje rankingowe</td><td>26</td><td>29</td><td>25</td><td>33</td></tr>
  <tr><td>Gracze</td><td>398</td><td>508</td><td>512</td><td>596</td></tr>
  <tr><td>Gry rankingowe</td><td>20 164</td><td>23 068</td><td>22 894</td><td>28 992</td></tr>
  <tr><td>Średnia gier na gracza</td><td>50,66</td><td>45,41</td><td>44,71</td><td>48,64</td></tr>
  <tr><td>Uczestników razem</td><td>1845</td><td>2016</td><td>1934</td><td>2487</td></tr>
  <tr><td>Średnia liczba uczestników turnieju</td><td>71</td><td>70</td><td>77</td><td>75</td></tr>
  <tr><td>Gracze, którzy rozegrali:</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
  <tr><td>ponad 300 partii</td><td>1</td><td>3</td><td>1</td><td>5</td></tr>
  <tr><td>od 200 do 299 partii</td><td>13</td><td>13</td><td>16</td><td>12</td></tr>
  <tr><td>od 100 do 199 partii</td><td>47</td><td>49</td><td>51</td><td>63</td></tr>
  <tr><td>od 50 do 99 partii</td><td>82</td><td>80</td><td>81</td><td>116</td></tr>
  <tr><td>do 49 partii</td><td>255</td><td>363</td><td>363</td><td>400</td></tr>
  <tr><td>Lista rankingowa</td><td>220</td><td>302</td><td>319</td><td>357</td></tr>
  <tr><td>Poczekalnia</td><td>260</td><td>327</td><td>396</td><td>387</td></tr>
  <tr><td>Lista + poczekalnia</td><td>480</td><td>629</td><td>715</td><td>744</td></tr>
  <tr><td>Debiutanci</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>208</td></tr>
  <tr><td>Najliczniejszy turniej</td><td>Warszawa - 117</td><td>MP - 168</td><td>Kraków - 138</td><td>Ostróda - 167</td></tr>
  <tr><td>Nowe miasta turniejowe</td><td>&nbsp;</td><td>&nbsp;</td><td>0</td><td>2</td></tr>
  <tr><td>Nowi zwycięzcy turniejów</td><td>&nbsp;</td><td>&nbsp;</td><td>4</td><td>7</td></tr>
</table>
</div>

<div id='tabs-2004'>
    <h2 id="turnieje">Turnieje</h2>
W 2004 r. odbyło się 25 turniejów zaliczanych do rankingu PFS. Wzięło w nich udział 512 osób. Najwięcej uczestników zgromadziły turnieje w Krakowie i Ostródzie. Po dwóch latach przerwy ponownie na scrabblowej mapie Polski zagościł turniej w Gorzowie Wielkopolskim. Po raz pierwszy odbyły się rozgrywki Ligi Scrabblowej - wzięło w niej udział 14 drużyn (8 w I lidze, 6 w II), w sumie 70 osób. <br />
Przy tej okazji <b>Zarząd Polskiej Federacji Scrabble</b> składa wszystkim <b>organizatorom turniejów </b>w 2004, <b>sponsorom </b>oraz <b>innym osobom</b>, które przyczyniają się do rozwoju ruchu scrabblowego <b>serdeczne podziękowania</b>. Zarząd docenia organizacyjny trud włożony w przygotowanie imprez oraz ma nadzieję, że turnieje przez Was organizowane także w kolejnych latach znajdą się na mapie turniejów Scrabble.<br /><br />
Liczba uczestników na turniejach przedstawia się następująco:

<table class="zapisy">
  <tr><td>VIII Mistrzostwa Krakowa (GP)</td><td class='alignright'>138</td></tr>
  <tr><td>IV Mistrzostwa Ziemi Ostródzkiej (GP)</td><td class='alignright'>133</td></tr>
  <tr><td>IV Turniej 24h Non Stop "Le Mans"</td><td class='alignright'>118</td></tr>
  <tr><td>Turniej Otwarcia Sezonu (Warszawa)</td><td class='alignright'>115</td></tr>
  <tr><td>IX Mistrzostwa Warszawy (GP)</td><td class='alignright'>114</td></tr>
  <tr><td>XII Mistrzostwa Polski </td><td class='alignright'>104</td></tr>
  <tr><td>X Mistrzostwa Łodzi (GP)</td><td class='alignright'>100</td></tr>
  <tr><td>VIII Mistrzostwa Piastowa </td><td class='alignright'>89</td></tr>
  <tr><td>III Mistrzostwa Lublina (GP)</td><td class='alignright'>81</td></tr>
  <tr><td>IX Mistrzostwa Górnego &brvbar;ląska i Zagłębia (Zabrze, GP)</td><td class='alignright'>80</td></tr>
  <tr><td>VII Mistrzostwa Podbeskidzia (Bielsko-Biała, GP)</td><td class='alignright'>78</td></tr>
  <tr><td>VIII Mistrzostwa Ziemi Kujawskiej (Inowrocław, GP) </td><td class='alignright'>76</td></tr>
  <tr><td>X Puchar Polski </td><td class='alignright'>73</td></tr>
  <tr><td>Liga Scrabblowa (I i II)</td><td class='alignright'>70</td></tr>
  <tr><td>III Mistrzostwa Bydgoszczy</td><td class='alignright'>69</td></tr>
  <tr><td>VI Mistrzostwa Podlasia (Bielsk Podlaski, GP)</td><td class='alignright'>65</td></tr>
  <tr><td>II Mistrzostwa KKS Ghost</td><td class='alignright'>61</td></tr>
  <tr><td>III Mistrzostwa Wybrzeża (Rumia)</td><td class='alignright'>58</td></tr>
  <tr><td>VII Mistrzostwa Warmii i Mazur (Nowe Miasto Lubawskie)</td><td class='alignright'>58</td></tr>
  <tr><td>VII Mistrzostwa Nowego Dworu Mazowieckiego</td><td class='alignright'>55</td></tr>
  <tr><td>VIII Mistrzostwa Ziemi Słupskiej </td><td class='alignright'>51</td></tr>
  <tr><td>III Mistrzostwa Ziemi Gorzowskiej </td><td class='alignright'>45</td></tr>
  <tr><td>Turniej o Puchar Wójta (Szarlota)</td><td class='alignright'>36</td></tr>
  <tr><td>II Mistrzostwa Bałtyku (Jurata)</td><td class='alignright'>35</td></tr>
  <tr><td>IV Mistrzostwa Podhala (Zakopane)</td><td class='alignright'>32</td></tr>
  <tr><td colspan='2' class='alignright'><b>razem: 1934</b></td></tr>
</table>



<h2 id="zwyciezcy">Zwycięzcy</h2>
W 24 turniejach (bez Ligi) triumfowało 14 graczy. Najwięcej razy (4) na najwyższym stopniu podium ;-) stawał Stanisław Rydzik. Po 3 turnieje wygrali Mateusz Żbikowski i Karol Wyrębkiewicz. Swoje pierwsze zwycięstwa turniejowe zanotowali Jarosław Puchalski, Andrzej Oleksiak, Sławomir Kucia i Mariusz Wrześniewski - nowy Mistrz Polski, który w poprzednich latach pięciokrotnie zajmował drugie miejsca, aż w końcu zatriumfował, i to jak :-)<br />
Oto klasyfikacja "medalowa" za rok 2004:
<table class="klasyfikacja">
    <tr>
        <td>&nbsp;</td>
        <td class='osoba'>Imię i nazwisko</td>
        <td><img src="files/img/medal_gold.png" alt="1" /></td>
        <td><img src="files/img/medal_silver.png" alt="2" /></td>
        <td><img src="files/img/medal_bronze.png" alt="3" /></td>
    </tr>

<?
$result = mysql_query("SELECT * FROM medals WHERE rok='2004' ORDER BY zlote DESC, srebrne DESC, brazowe DESC");
$i = 1;
$pop1 = "0";
$pop2 = "0";
$pop3 = "0";

while($row = mysql_fetch_array($result)){
    if(($pop1 != $row['zlote']) || ($pop2 != $row['srebrne']) || ($pop3 != $row['brazowe']))
        print "<td class='lp'>".$i."</td>";
    else
        print "<td class='lp'>&nbsp;</td>";
    $i++;
    $pop1 = $row['zlote'];
    $pop2 = $row['srebrne'];
    $pop3 = $row['brazowe'];
    print "<td class='osoba'>".$row['osoba']."</td>";
    print "<td>".$row['zlote']."</td>";
    print "<td>".$row['srebrne']."</td>";
    print "<td>".$row['brazowe']."</td></tr>";
}
?>
</table>

<b>"Klasyfikację wszechczasów"</b> można zobaczyć <a href="medale.php#wszech">tutaj</a>.




<h2 id="ranking">Ranking</h2>
W porównaniu z rokiem ubiegłym lista rankingowa powiększyła się o 17 osób. Obecnie jest na niej 319 graczy, w poczekalni - 396 (rok temu 327). W czołówce rankingu zaszły spore zmiany. Do września liderem pozostawał Mariusz Skrobosz, który w maju osiągnął ranking 150 (149,58), uzyskując przewagę ponad 8 pkt nad drugim zawodnikiem. Zmiana na prowadzeniu nastąpiła dopiero we wrześniu, gdy wyprzedził go Mateusz Żbikowski, który zakończył rok 2004 na pozycji lidera z rankingiem 146. Największy awans w czołówce odnotował Krzysiek Mówka, który rok temu był na miejscu 25. Zwycięzca 4 turniejów Staszek Rydzik doszedł w rankingu nawet do 6. miejsca, ale rok zakończył na pozycji 12.<br />
Oto czołówka rankingu i porównanie z końcem roku 2003, oraz wykres przedstawiający zmiany w czołówce.

<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td><b>koniec 2003</b></td><td><b>ranking</b></td><td><b>koniec 2004</b></td><td><b>ranking</b></td><td rowspan="12"><img src="rozne/rank2004.gif"></td></tr>
  <tr><td>1.</td><td>Mariusz Skrobosz</td><td>144,94</td><td>Mateusz Żbikowski</td><td>145,73</td></tr>
  <tr><td>2.</td><td>Paweł Dawidson</td><td>142,13</td><td>Kazimierz Merklejn jr.</td><td>143,06</td></tr>
  <tr><td>3.</td><td>Wojciech Usakiewicz</td><td>141,12</td><td>Szymon Fidziński</td><td>141,59</td></tr>
  <tr><td>4.</td><td>Szymon Zaleski</td><td>140,92</td><td>Wojciech Usakiewicz</td><td>141,39</td></tr>
  <tr><td>5.</td><td>Tomasz Zwoliński</td><td>140,35</td><td>Krzysztof Mówka</td><td>141,12</td></tr>
  <tr><td>6.</td><td>Kazimierz Merklejn jr.</td><td>139,35</td><td>Mariusz Skrobosz</td><td>140,77</td></tr>
  <tr><td>7.</td><td>Mateusz Żbikowski</td><td>138,84</td><td>Szymon Zaleski</td><td>139,92</td></tr>
  <tr><td>8.</td><td>Dariusz Banaszek</td><td>137,19</td><td>Mariusz Wrześniewski</td><td>139,48</td></tr>
  <tr><td>9.</td><td>Kamil Górka</td><td>136,71</td><td>Maciej Gałecki</td><td>139,42</td></tr>
  <tr><td>10</td><td>Szymon Fidziński</td><td>136,73</td><td>Tomasz Zwoliński</td><td>139,23</td></tr>
</table>
Jak widać, w zeszłym roku aby być w pierwszej dziesiątce wystarczył ranking 136, w tym roku już 139. We wrześniu aż 9 osób miało ranking powyżej 140.<br />



<h2>Rankingi osób na miejscach</h2>
<table class="klasyfikacja">
  <tr><td><b>miejsce</b></td><td><b>koniec 2002</b></td><td><b>koniec 2003</b></td><td><b>koniec 2004</b></td></tr>
  <tr><td><b>10</b></td><td>135,37</td><td>136,73</td><td>139,23</td></tr>
  <tr><td><b>20</b></td><td>129,35</td><td>133,64</td><td>134,05</td></tr>
  <tr><td><b>50</b></td><td>120,79</td><td>123,38</td><td>124,02</td></tr>
  <tr><td><b>100</b></td><td>111,20</td><td>114,14</td><td>116,97</td></tr>
  <tr><td><b>150</b></td><td>101,67</td><td>107,78</td><td>110,69</td></tr>
  <tr><td><b>200</b></td><td>89,93</td><td>101,28</td><td>105,83</td></tr>
</table>



<h2>Największe awanse w rankingu <span>(mierzone przyrostem punktów rankingowych)</span></h2>
<table class="klasyfikacja">
  <tr><td><b>gracz</b></td><td><b>przyrost</b></td><td><b>ranking 2003 <&#8594; ranking 2004</b></td></tr>
  <tr><td>Rafał Wesołowski</td><td>22,20</td><td>90,48 <&#8594; 112,68</td></tr>
  <tr><td>Michał Sosin</td><td>17,19</td><td>88,28 <&#8594; 105,47</td></tr>
  <tr><td>Krzysztof Sporczyk</td><td>15,29</td><td>98,57 <&#8594; 113,86</td></tr>
  <tr><td>Grzegorz Matysiak</td><td>15,06</td><td>110,80 <&#8594; 125,84</td></tr>
  <tr><td>Robert Drózd</td><td>14,41</td><td>99,21 <&#8594; 113,62</td></tr>
  <tr><td>Joanna Brychcy</td><td>14,01</td><td>88,65 <&#8594; 102,66</td></tr>
  <tr><td>Michał Szkudlarek</td><td>13,08</td><td>101,73 <&#8594; 114,81</td></tr>
  <tr><td>Piotr Pietuchowski</td><td>11,67</td><td>115,22 <&#8594; 126,89</td></tr>
  <tr><td>Wojciech Sołdan</td><td>10,90</td><td>93,65 <&#8594; 104,55</td></tr>
  <tr><td>Filip Sosin</td><td>10,65</td><td>109,03 <&#8594; 119,68</td></tr>
  <tr><td>Renata Kowalska</td><td>10,12</td><td>107,32 <&#8594; 117,44</td></tr>
  <tr><td>Krzyszof Mówka</td><td>9,07</td><td>132,05 <&#8594; 141,12</td></tr>
</table>



<h2>Najlepsze nabytki listy rankingowej</h2>
Największą niespodzianką sezonu 2004 był młody Bartek Morawski z Gliwic, który debiutował na Mistrzostwach Polski w 2003 r. (9:6, miejsce 40), wszedł na listę rankingową po turniejach w Piastowie (m. 31) i Zabrzu (m. 10) z rankingiem 128, a na tegorocznych Mistrzostwach Polski zakwalifikował się do II fazy, zajmując 4 miejsce.

<table class="klasyfikacja">
  <tr><td>Bartosz Morawski</td><td>133,56</td></tr>
  <tr><td>Kuba Koisar</td><td>126,51</td></tr>
  <tr><td>Mariusz Margalski</td><td>123,02</td></tr>
  <tr><td>Łukasz Kozub</td><td>122,31</td></tr>
  <tr><td>Andrzej Kowalski</td><td>120,86</td></tr>
  <tr><td>Artur Irzyk</td><td>120,26</td></tr>
  <tr><td>Łukasz Bryła</td><td>120,15</td></tr>
  <tr><td>Krzysztof Langiewicz</td><td>118,53</td></tr>
  <tr><td>Andrzej Grabiński</td><td>118,08</td></tr>
  <tr><td>Grzegorz Wilk</td><td>117,95</td></tr>
</table>



<h2>Ranking z partii rozegranych w 2004 roku <span>(tylko ci gracze, którzy rozegrali w 2004 r. minimum 30 partii)</span></h2>
<table class="klasyfikacja">
  <tr><td>&nbsp;</td><td>ranking</td><td>liczba gier</td></tr>
  <tr><td>Szymon Fidziński</td><td>147,60</td><td>83</td></tr>
  <tr><td>Mateusz Żbikowski</td><td>143,73</td><td>284</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>142,77</td><td>272</td></tr>
  <tr><td>Wojciech Usakiewicz</td><td>141,32</td><td>121</td></tr>
  <tr><td>Kazimierz Merklejn jr</td><td>141,30</td><td>283</td></tr>
  <tr><td>Krzysztof Mówka</td><td>141,17</td><td>272</td></tr>
  <tr><td>Marcin Mrfziuk</td><td>141,00</td><td>39</td></tr>
  <tr><td>Mariusz Wrześniewski</td><td>140,40</td><td>161</td></tr>
  <tr><td>Kamil Górka</td><td>139,90</td><td>218</td></tr>
  <tr><td>Stanisław Rydzik</td><td>139,80</td><td>272</td></tr>
</table>

Tu można znaleźć <a href="stats/skalp2004.xls">pełną tabelę (.xls)</a>, zawierającą skalpy wszystkich uczestników turniejów w 2004 r. W razie znalezienia błędów proszę o <a onClick="sendMail('pfs','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />informacje</a>.



<h2>Najwięcej rozegranych partii w 2004 r.</h2>
Jak co roku żadnego turnieju nie opuścił Tomasz Zwoliński. Wszystkie 12-rundowe turnieje zaliczył Mateusz Żbikowski. Mistrz Polski z 2003 r. Kazimierz Merklejn
jr ze Szczecina zaszczycał swoją obecnością większość turniejów, nie zważając na fakt, że czasami musiał pokonać pociągami ponad 600 km (a powrót trwał niekiedy 4 dni).
<table class="klasyfikacja">
  <tr><td>Tomasz Zwoliński</td><td>305</td></tr>
  <tr><td>Mateusz Żbikowski</td><td>284</td></tr>
  <tr><td>Kazimierz Merklejn jr</td><td>283</td></tr>
  <tr><td>Karol Wyrębkiewicz</td><td>278</td></tr>
  <tr><td>Krzysztof Mówka</td><td>272</td></tr>
  <tr><td>Stanisław Rydzik</td><td>272</td></tr>
  <tr><td>Mariusz Skrobosz</td><td>272</td></tr>
  <tr><td>Dawid Pikul</td><td>263</td></tr>
  <tr><td>Krzysztof Sporczyk</td><td>236</td></tr>
  <tr><td>Jarosław Puchalski</td><td>223</td></tr>
</table>
</div>
</div>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
