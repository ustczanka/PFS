<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Mattel Poland</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("scrabble","mattel");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Scrabble")</script></h1>

<a href="http://scrabble.mattel.com/" target="_blank"><img src="files/img/scrabble.gif">
<div style="clear:both;"></div><br />
<b>SCRABBLE®</b></a> jest znakiem towarowym zarejestrowanym w polskim Urzędzie Patentowym na rzecz firmy J.W.&nbsp;Spear&nbsp;&amp;&nbsp;Sons&nbsp;PLC, Enfield, Middlesex, England, należącej do <a href="http://www.mattel.com/" target="_blank"><b>Mattel Company</b></a>.<br /><br />

Polskim dystrybutorem Scrabble jest:<br /><br />
<b>Mattel Poland Sp. z o.o.</b><br />
ul. Chłodna 51<br />
03-738 Warszawa<br />
tel. (22) 210 80 00<br />

<?require_once "files/php/bottom.php"?>
 </body>
</html>

