<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Słownik : Zasady dopuszczalności słów</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("scrabble","osps");</script>
  <style type="text/css">
  	div.podziek{
		text-align:center;
		font-weight: bold;
		padding: 10px;
	}
	q{
		display:block;
		font-style:italic;
	}
	div.slownik{
	width: 400px;
	height: 90px;
	float:left;
	margin-top: 10px;
	}

  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Zasady dopuszczalności słów")</script></h1>
<!--(ze zmianami z 30.03.2005)-->
<div class="podziek ramka">Pragniemy gorąco podziękować naszym konsultantom:<br />prof. dr. hab. Zygmuntowi Saloniemu i dr. Włodzimierzowi
Gruszczyńskiemu.</div>
 
<h2>Zasady ogólne</h2>
Zgodnie z <a href="reguly.php">regułami gry w Scrabble</a>:
<q>dopuszczalne są wszystkie słowa występujące w słownikach języka polskiego oraz wszystkie ich prawidłowe formy gramatyczne, z wyjątkiem takich słów, które rozpoczynają się od wielkiej litery, są skrótami, bądź słowami wymagającymi cudzysłowu lub łącznika.</q> 
Ponieważ powyższa formuła jest zbyt ogólnikowa, należy doprecyzować jej kluczowe elementy.<br /><br /> 

Słowo jest pisane zgodnie z zasadami języka polskiego jako zwarty ciąg liter oddzielony od innych słów przerwami międzywyrazowymi. Niedozwolone są więc słowa zawierające wewnątrz znak graficzny (także apostrof, np. w słowie <i>scrabble'owy</i>) oraz słowa pisane z niepolskimi znakami diakrytycznymi (np.: <i>öre</i>, <i>écru</i>), natomiast obecność znaku przestankowego (np. wykrzyknika po słowie <i>sic!</i>) nie ma wpływu na dopuszczalność.<br /> 
<br /> 

<h2>Słowniki języka polskiego</h2>
Zestaw dozwolonych źródeł obejmuje następujące słowniki <a href="http://www.pwn.com.pl">Wydawnictwa Naukowego PWN</a>:
<div class="slownik">
    <img src="files/img/sjp.gif" class="onleft" alt="" /> „Słownik języka polskiego”</br/>
    pod redakcją Mieczysława Szymczaka<br />
    (trzytomowy z suplementem)
</div>
<div class="slownik">
	<img src="files/img/maly.gif" class="onleft" alt="" /> „Mały słownik języka polskiego”</br/>
    pod redakcją Stanisława Skorupki, Haliny Auderskiej, Zofii Łempickiej<br />
    lub - w nowszych wydaniach - pod redakcją Elżbiety Sobol
</div>
<div class="slownik">
	„Słownik poprawnej polszczyzny”</br/> 
	pod redakcją Witolda Doroszewskiego<br /><br />
	„Słownik wyrazów obcych”</br/>
	pod redakcją Jana Tokarskiego
</div>
 <div class="slownik">
	<img src="files/img/nspp.jpg" class="onleft" alt="" /> „Nowy słownik poprawnej polszczyzny PWN”</br/>
    pod redakcją Andrzeja Markowskiego
</div>
 <div class="slownik">
 	<img src="files/img/swon.gif" class="onleft" alt="" /> „Słownik wyrazów obcych. Wydanie nowe”</br/> 
 	pod redakcją Elżbiety Sobol
</div>
<div class="slownik">
	<img src="files/img/nso.gif" class="onleft" alt="" /> „Nowy słownik ortograficzny PWN”</br/>
    pod redakcją Edwarda Polańskiego
</div>
 <div class="slownik">
 	<img src="files/img/isjp.jpg" class="onleft" alt="" /> „Inny słownik języka polskiego PWN”</br/>
    pod redakcją Mirosława Bańki<br />
    (dwutomowy)
</div>
<div class="slownik"> 
	<img src="files/img/nsjp.jpg" class="onleft" alt="" /> „Nowy słownik języka polskiego PWN”</br/>
    pod red. Elżbiety Sobol
</div>
<div class="slownik">
	<img src="files/img/pso.jpg" class="onleft" alt="" /> „Popularny słownik ortograficzny PWN”</br/>
    w oprac. Anny Kłosińskiej
</div> 
<div class="slownik">
	<img src="files/img/uniwers.gif" class="onleft" alt="" /> „Uniwersalny słownik języka polskiego PWN”</br/>
    pod red. Stanisława Dubisza
</div>
<div class="slownik">
	<img src="files/img/ortog.gif" class="onleft" alt="" /> „Wielki słownik ortograficzny PWN”</br/>
    pod red. Edwarda Polańskiego<br />
    2003 
</div>
<div class="slownik">
	<img src="files/img/obcy.gif" class="onleft" alt="" /> „Wielki słownik wyrazów obcych PWN”</br/>
    pod red. Mirosława Bańki<br />
    2003
</div>
<div class="slownik">
	<img src="files/img/spp2004.jpg" class="onleft" alt="" /> „Słownik poprawnej polszczyzny PWN”</br/> 
	w oprac. Lidii Drabik i Elżbiety Sobol<br />
    2004
</div>
<div class="slownik">
	<img src="files/img/sort2004.jpg" class="onleft" alt="" /> „Słownik ortograficzny PWN”</br/>
    w oprac. Anny Kłosińskiej<br />
    2004
</div>
<div class="slownik">
	<img src="files/img/swo2004.jpg" class="onleft" alt="" /> „Słownik wyrazów obcych PWN”</br/>
    w oprac. Lidii Wiśniakowskiej<br />
    2004
</div>
<div class="slownik">
	<img src="files/img/wspp.jpg" class="onleft" alt="" /> „Wielki słownik poprawnej polszczyzny PWN”</br/>
    pod red. Andrzeja Markowskiego<br />
    2005
</div>

<p style="clear:both"><br />
Dopuszczalne są wydania z roku 1980 i późniejsze, z wyjątkiem słownika ortograficznego, dopuszczalnego tylko w wydaniach od roku 1996, które ukazały się pod tytułem „Nowy słownik ortograficzny PWN”. (Uwaga! W roku 1996 było również ostatnie wydanie „Słownika ortograficznego języka polskiego” pod redakcją Mieczysława Szymczaka, który <b>nie jest dozwolony</b>.<br /> 
Dopuszczono również wybrane słowa z 11-tomowego „Słownika języka polskiego” pod redakcją Witolda Doroszewskiego (ich pełna lista znajduje się w pliku 
<a href="osps/dorosz.txt" target="_blank">dorosz.txt</a>).
</p>

<h2>Formy wyrazu</h2>
<b>Hasło</b> - wyraz lub wyrażenie stanowiące odrębną pozycję w słowniku.<br /> 
<b>Podhasło</b> - hasło umieszczone w słowniku w obrębie innego hasła, niebędące hasłem samodzielnym (np. „iii”).<br /> 
<b>Definicja</b> - określenie znaczenia hasła lub podhasła wraz z informacją przyhasłową i równoznacznikami (synonimami) jednak bez przykładów użycia.<br /> 
<b>Prawidłowe formy</b> - poprawność formy wyrazu nie może budzić wątpliwości w ujęciu przynajmniej jednego z dozwolonych słowników, to znaczy: 
<ul> 
	<li>forma podstawowa sprawdzanego wyrazu jest zanotowana jako hasło lub podhasło, jako jego część oddzielona od innych części przerwami międzywyrazowymi, bądź też jednoznacznie jako synonim.</li>
    <li>konstrukcja sprawdzanej formy jest zgodna z regułami gramatycznymi (także fonetycznymi i ortograficznymi)</li>
</ul> 
Natomiast poprawność semantyczna (znaczeniowa) sprawdzanej formy nie jest konieczna. Dopuszcza się więc formy poprawne gramatycznie, bez względu na to, czy i jakie mają zastosowanie w żywym języku (np. świtam). Zagadnienia formy podstawowej oraz poprawności są omówione szczegółowo w dalszej części tekstu.

<h2>Skróty</h2>
 <b>Skróty</b> - wyrazy, które w czytaniu wymagają rozbudowania o brakujące głoski, np.: <i>ckm</i> (czytaj: cekaem), <i>dcą</i> (czytaj: dowódcą), <i>fud.</i> (czytaj: fudit) są  niedozwolone. Natomiast dozwolone są np.: <i>cekaem</i>, <i>bus</i>, <i>gees</i>, <i>op</i> (krótsze określenie <i>op-artu</i>).
 
<h2>Rozwiązania szczególne</h2>
Mimo sprecyzowania pojęcia poprawności, w praktyce można się natknąć na wiele szczegółowych wątpliwości. Większość z nich udało się uporządkować i rozstrzygnąć a priori, o czym poniżej.
<ol>
	<li>Formę podstawową wyrazu traktujemy w zgodzie z tradycją słowników języka polskiego, tzn. dochodzimy do niej (w sposób regularny) przez odmianę wyrazu w sposób przyjęty w gramatyce polskiej (według programu szkoły podstawowej). Przy dochodzeniu do formy podstawowej niedopuszczalne jest powoływanie się na wyraz pokrewny słowotwórczo, nawet bardzo blisko morfologicznie i znaczeniowo związany ze słowem testowanym.</li>
    <li>Wyrazy głównych typów mają następujące formy podstawowe:
    	<ol type="a">
        	<li>rzeczowniki - mianownik liczby pojedynczej (a rzeczowniki nie mające liczby pojedynczej - mianownik);</li>
            <li>przymiotniki - mianownik w stopniu równym;</li>
            <li>czasowniki - bezokolicznik;</li>
            <li>przysłówki - stopień równy.</li>
        </ol>         
Formy przymiotników w stopniu wyższym i najwyższym powinny być zestawiane ze wskazaną wyżej formą stopnia równego; formy imiesłowów (np. <i>dopiwszy</i>, <i>dopitych</i>) i regularnych rzeczownikowych nazw czynności, tzw. odsłowników (np.: <i>dopiciem</i>) - z odpowiednimi bezokolicznikami (dla podanych przykładów: <i>dopić</i>) z uwzględnieniem tego, czy dany czasownik jest dokonany, czy niedokonany. Jednak dla imiesłowów  biernych i przeszłych niezbędne jest, aby mianownik liczby pojedynczej rodzaju męskiego danego imiesłowu był wymieniony wśród form czasownikowych, w informacji przyhasłowej słownika, wynikał z niej, z definicji lub z przykładów użycia, a nie tylko dał się utworzyć na podstawie tabel odmiany (niedozwolone są więc np.: <i>popadnięty</i>, <i>banych</i>).<br /> 
Także dla alternatywnych form odsłowników, utworzonych od czasowników grup koniugacyjnych Va, Vb, Vc, niezbędne jest, aby forma mianownika zanotowana
była jako hasło, podhasło lub podana w definicji (dozwolone są więc: <i>ciągnień</i>, <i>kwitnienia</i>, a niedozwolone: <i>plunieniu</i>, <i>fiknieniom</i>).</li>
	<li>Niedopuszczalne jest wiązanie wyrazu testowanego z formą podstawową wyrazu blisko z nią związanego, ale różniącego się słowotwórczo i uznawanego w gramatykach i słownikach polskich za inny wyraz. W szczególności formą podstawową nie jest: 
		<ol type="a">
        	<li>dla przymiotników (np.: <i>grogowej</i>) - odpowiednia forma rzeczownika podstawowego (<i>grog</i>, gdyż forma podstawowa musi brzmieć np.: <i>grogowy</i>);</li>
            <li>dla rzeczowników zdrobniałych i innych pochodnych (np.: <i>kredensiku</i>) - odpowiednia forma rzeczownika podstawowego (<i>kredens</i>, gdyż forma podstawowa musi brzmieć: <i>kredensik</i>);</li>
            <li>dla form czasowników przedrostkowych (np.: <i>zatańczę</i>, <i>stańczyła</i>) - bezokolicznik bez przedrostka (<i>tańczyć</i>, gdyż formy podstawowe muszą zawierać takie same przedrostki, jak słowa testowane; stąd <i>zatańczyć</i> jest dopuszczalne, ale <i>stańczyć</i> - nie);</li>
            <li>dla przysłówków (np. <i>grudniowo</i>, <i>lżej</i>) - odpowiednia forma przymiotnika podstawowego (<i>grudniowy</i>, <i>lekki</i>, gdyż forma podstawowa musi brzmieć: <i>grudniowo</i>, <i>lekko</i>);</li>
            <li>dla wszelkich innych form wyrazów pochodnych - forma podstawowa wyrazu podstawowego ani innego wyrazu pokrewnego; dlatego dla słowa <i>wielgachną</i> formą podstawową nie jest <i>wielki</i> (musi być np.: <i>wielgachny</i>), a dla <i>dymkowej</i> - <i>dymowy</i> (musi być np.: <i>dymkowy</i>).</li>
		</ol> 
	</li>
 	<li>Przymiotniki i przysłówki w stopniu wyższym i najwyższym są dozwolone wtedy, gdy po odpowiednim haśle jest podane przynajmniej zakończenie głównej formy stopnia wyższego (np.: niedozwolony jest wyraz <i>błoższy</i>, bo po nazwie hasła <i>błogi</i> nie podano zakończenia stopnia wyższego, który ma formę złożoną <i>bardziej błogi</i>).</li>
    <li>Dozwolone są słowa występujące tylko w kontekście innych słów, a w szczególności:
       <ol type="a">
            <li>rzeczowniki w miejscowniku (np.: <i>oknie</i>, <i>polach</i>);</li>
            <li>formy czasownikowe występujące tylko z <i>się</i> (np. <i>boisz</i>, <i>bojący</i>, <i>baniem</i>);</li>
            <li>formy używane tylko w połączeniach z innymi słowami, jeśli swą budową (w sposób oczywisty) wiążą się słowotwórczo z innymi słowami pełnoznacznymi języka polskiego; np.: słowa zakończone na <i>-sku</i>, <i>-cku</i>, <i>-dzku</i> (występujące po przyimku <i>po</i>), nawet nie zanotowane przez słownik (np.: <i>korektorsku</i>), utworzone od przymiotników zakończonych na <i>-ski</i>, <i>-cki</i>, <i>-dzki</i>; zasada ta jednak dotyczy tylko przymiotników utworzonych od rzeczowników, niedozwolone są więc np.: <i>blisku</i>, <i>wąsku</i>.</li>
		</ol> 
 	</li>
    <li>Słowa, które powstają przez dołączenie ortograficzne do słowa dozwolonego cząstek, mających zgodnie z polską ortografią pisownię łączną, są dozwolone w bardzo ograniczonym zakresie: 
        <ol type="a">
            <li>cząstki czasownikowe: <i>-(e)m</i>, <i>-(e)ś</i>, <i>-(e)śmy</i>, <i>-(e)ście</i> można dodawać tylko do odpowiednich form czasownikowych, do spójników i partykuł zakończonych na <i>-by</i>, do zanotowanych przez słownik połączeń spójników i partykuł z cząstką <i>-by</i> oraz do wyrazu <i>byle i bodaj</i> (dozwolone są więc np.: <i>pisałem</i>, <i>niechbyś</i>, <i>bodajem, jakbyśmy</i>, <i>bylebyście</i>; a niedozwolone np.: <i>świniam</i>, <i>głupiś</i>, <i>jemuśmy</i>, <i>czemuście</i>);</li>
            <li>cząstki czasownikowe: <i>-by</i>, <i>-bym</i>, <i>-byś</i>, <i>-byśmy</i>, <i>-byście</i> można dodawać tylko do odpowiednich form czasownikowych, np.: <i>pisałbym</i>, <i>zlazłobyś</i>, <i>mielibyśmy</i>, <i>grałybyście</i>;</li>
            <li>partykułę: <i>-że</i>, <i>-ż</i> można dodawać tylko jednokrotnie  do odpowiednich form czasownikowych w trybie rozkazującym, np.: <i>jedzże</i>, <i>idźcież</i> (niedozwolone jest więc np. <i>kupiłże</i>);</li>
            <li>nie dopuszcza się słów powstałych przez dołączenie do słowa dozwolonego cząstek: <i>-li</i>, <i>-ć</i> (niedozwolone są więc np.: <i>maszli</i>, <i>krowęć</i>);</li>
            <li>ze słów powstałych przez dołączenie formy zaimkowej <i>-ń</i> do przyimków dopuszcza się tylko te, które zanotowane są  jako hasło lub podhasło, np.: <i>weń</i>, <i>podeń</i> (niedozwolone są np.: <i>pozań</i>, <i>spodeń</i>);</li>
            <li>partykuła przecząca <i>nie-</i> może by dodawana tylko do:
                <ul style="list-style-type:none">
                    <li>- wszystkich form niezaprzeczonych przymiotników w stopniu równym z wyłączeniem form zakończonych na <i>-sku</i>, <i>-cku</i>, <i>-dzku</i>;</li>
                    <li>- niezaprzeczonych przysłówków w stopniu równym, jeśli przysłówek jest utworzony od przymiotnika;</li>
                    <li>- wszystkich niezaprzeczonych form odsłowników;</li>
                    <li>- niezaprzeczonych imiesłowów przymiotnikowych czynnych, biernych i przeszłych. </li>
				</ul>
			</li>
		</ol>
Powyższe ograniczenia nie dotyczą wyrazów, które z zastrzeżonymi końcówkami zanotowane są w słowniku jako  hasło lub podhasło (np. <i>znaszli</i>).</li>
	<li>Nie dopuszcza się słów występujących tylko w przejętych przez język polski kilkuwyrazowych zwrotach obcojęzycznych, np.: <i>de facto</i>, <i>in flagranti</i>, chyba że testowane słowo jest także  zawarte w słowniku jako jednowyrazowe hasło, np. molto, lub jego pisownia została spolszczona, np. ganc egal.</li>
    <li>Wśród form poprawnych gramatycznie, a zatem dopuszczalnych, które nie znajdują lub znajdują w niewielkim zakresie zastosowanie w żywym języku (tzw. form potencjalnych), należy wymienić przede wszystkim:
    	<ol type="a">
        	<li>liczbę mnogą wszystkich rzeczowników odmiennych, nawet jeśli są opatrzone informacją „blm”, np.: <i>dekadentyzmy</i>, <i>tlenom</i>, <i>trwaniach</i>;</li>
            <li>„nieosobowy” mianownik liczby mnogiej rzeczowników męskoosobowych, np.: <i>prałaty</i>, <i>rajdowce</i>, <i>sędzie</i>, <i>pogany</i>, <i>ziemiany</i> (formy te są w pełni poprawne regularne i nie tyle archaiczne, co familiarne - nasze dzielne komandosy lub deprecjonujące - te okrutne belfry, a niektóre spotyka się względnie często we współczesnej polszczyźnie - te doktory);</li>
            <li>wołacz wszystkich rzeczowników, np. <i>abstynencjo</i>;</li>
            <li>rodzaj męskoosobowy wszystkich przymiotników i imiesłowów, np.: <i>napotni</i>, <i>murowani</i>;</li>
            <li>przymiotniki i imiesłowy występujące w rodzaju niezgodnym z naturalnym odniesieniem „płciowym”, np.: <i>żonata</i> (<i>żonata osoba</i>);</li>
            <li>pierwszą i drugą osobę liczby pojedynczej rodzaju nijakiego wszystkich czasowników, np. poszłom, wzięłobyś, oprócz kilkunastu czasowników defektywnych, których odmiana zamyka się w 3. osobie liczby pojedynczej rodzaju nijakiego, np. dnieć, cnić (niedopuszczone więc dniałom, cniłoś), jak i innych o ograniczonej odmianie, np. słychać; (pełna lista czasowników defektywnych znajduje się w pliku <a href="osps/defekt.txt" target="_blank">defekt.txt</a>)</li>
            <li>pierwszą i drugą osobę czasowników nieużywanych na ogół w odniesieniu do osób, np. mżę, za wyjątkiem kilku czasowników defektywnych;</li>
            <li>tryb rozkazujący i formy nieosobowe czasu przeszłego wszystkich czasowników niedefektywnych, np.: <i>muś</i>, <i>wólmy</i>, <i>liniano</i>, <i>byto</i>;</li>
            <li>wariantywny (zakończony na <i>-ij</i>, <i>-yj</i>) dopełniacz liczby mnogiej rzeczowników rodzaju żeńskiego, które w dopełniaczu liczby pojedynczej mają końcówkę <i>-ii</i>, <i>-ji</i>, np.: <i>bazylij</i>, <i>feeryj</i>, <i>gracyj</i>;</li>
            <li>tradycyjny (zakończony na <i>-cze</i>) wołacz liczby pojedynczej rzeczowników męskoosobowych, które w mianowniku liczby pojedynczej mają końcówkę <i>-ec</i>, np.: <i>akowcze</i>, <i>japończe</i>;</li>
		</ol>
	</li>
    <li>Dozwolone są formy liczby pojedynczej niektórych rzeczowników, zanotowanych w słownikach w liczbie mnogiej, mimo że w użyciu są także formy liczby pojedynczej. Dotyczy to przede wszystkim roślin, zwierząt, minerałów, związków chemicznych, grup osób (np. azyna - azyny, plazmid - plazmidy, trylobit - trylobity), (pełna ich lista znajduje się w pliku <a href="osps/lplm.html" target="_blank">lplm.html</a>).</li>
    <li>Aby sprawdzić poprawność formy pochodnej od formy podstawowej danego wyrazu należy w pierwszym rzędzie posłużyć się dostępnymi w słownikach informacjami fleksyjnymi. Większość słowników zawiera oznaczenia po hasłach głównych oraz tabele odmiany na początku tomu. „Inny słownik języka polskiego” podaje klasę i podklasę gramatyczną oraz brzmienie form niezbędnych do syntezy fleksyjnej według reguł podanych we wstępie.<br /> 
Słowniki ortograficzne nie podają żadnych oznaczeń, ale przytoczone formy pochodne pozwalają często na jednoznaczne przypisanie właściwego wzorca odmiany. Natomiast jeśli chcemy sprawdzić poprawność form pochodnych od wyrazu występującego tylko w starszych wydaniach słowników wyrazów obcych, które nie podają żadnych informacji gramatycznych, możemy to zrobić (w ograniczonym rozsądkiem zakresie) przez analogię do podobnego wyrazu występującego w jednym z wcześniej wymienionych słowników (np. odmianę wyrazu gimpa ze „Słownika wyrazów obcych” wzorujemy na odmianie wyrazów lampa, pompa z innych słowników; ale już odmiana nazwy polinezyjskiego tańca hula analogicznie do słowa kula jest co najmniej wątpliwa, bo może hula jest nieodmienne, co potwierdza najnowszy „Wielki słownik wyrazów obcych PWN”).</li>
	<li>Jeśli słowniki różnie oceniają poprawność danego wyrazu, to należy przyjąć wersję tego słownika, który wyraźnie uznaje dany wyraz za dopuszczalny. Jeśli po skorzystaniu z oznaczeń po hasłach i z tabel odmian pozostają wątpliwości co do poprawności gramatycznej sprawdzanego wyrazu, należy ów wyraz uznać za niedozwolony. Na turnieju decyzję podejmuje sędzia.</li>
    <li>Wszystkie powyższe zasady zostały uwzględnione (a pozostające wątpliwości rozstrzygnięte przez językoznawców) przy tworzeniu Oficjalnego Słownika Polskiego Scrabblisty (OSPS), który służy do sprawdzania dopuszczalności słów podczas turniejów. Jeśli jednak istnieją poważne wątpliwości co do werdyktu OSPS, ostateczną decyzję podejmuje sędzia turnieju.</li>
</ol>

<?require_once "files/php/bottom.php"?>
</body>
</html>

