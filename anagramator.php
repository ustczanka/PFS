<? include_once "files/php/funkcje.php"; ?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Anagramator OSPS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("glowna","anagramator");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Anagramator OSPS")</script></h1>

<a href="http://www.scrabble.mirelka.pl/" title="Nudzisz się zagraj w scrabble..." target=_blank>
<img alt="Wszystko dla scrabblisty" src="http://www.mirelka.pl/sklep/images/banners/scrabble.jpg" class="onright"></a>

Anagramator OSPS służy do sprawdzania słów (znajdujących się w Oficjalnym Słowniku Polskiego Scrabblisty), które można ułożyć z podanych liter, a zarazem do sprawdzenia poprawności wpisanego słowa.<br>
Oprócz liter można użyć (co najwyżej dwukrotnie) znaku '?' oznaczającego dowolną literę (blank).<br>
W przypadku sprawdzania słowa, słowo to zostanie wyróżnione na liście kolorem czerwonym.<br>
Jeżeli słowo nie znajduje się w OSPS, nie będzie go również na liście.<br>
Wyniki są wyświetlane w porcjach po 10 słów. <br><br>

<?
if (isset($_GET[slowo]))    { $slowo=$_GET[slowo];  $szuk=$_GET[slowo]; }
if (isset($_POST[slowo]))   { $slowo=$_POST[slowo]; $szuk=$_POST[slowo]; }

$no_ogonki = array ('Ą' => 'a', 'Ć' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ś' => 's', 'Ź' => 'x', 'Ż' => 'z', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'x', 'ż' => 'z', '?' => '*');
$to_ogonki = array ('a' => 'Ą', 'c' => 'Ć', 'e' => 'Ę', 'l' => 'Ł', 'n' => 'Ń', 'o' => 'Ó', 's' => 'Ś', 'x' => 'Ź', 'z' => 'Ż', '*' => '?', 'ą' => 'Ą', 'ć' => 'Ć', 'ę' => 'Ę', 'ł' => 'Ł', 'ń' => 'Ń', 'ó' => 'Ó', 'ś' => 'Ś', 'ź' => 'Ź', 'ż' => 'Ż');
$slowo = strtoupper($slowo);
$slowo = strtr ($slowo, $no_ogonki);
?>

<div class="anag">
    <form enctype="application/x-www-form-urlencoded" action="anagramator.php?s=ana" target="_self" method="post" charset="utf-8">
        <input name="slowo" style="width:170px" value="<? echo strtr($slowo, $to_ogonki); ?>">
        <input type='image' alt="Sprawdz" style='border:none;' name="submit" src="files/img/submit.png">
    </form>
</div>

<?
if(strlen($slowo) > 0) {
    $conn = "";
    $litery_wyst = "";
    $litery = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','W','Y','Z','a','c','e','l','n','o','s','x','z');
    $licznik = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    $len = strlen($slowo);
    $blank_counter = 0;

    for($i=0; $i<$len; $i++) {
        if($slowo[$i] != '*'){
            if(in_array($slowo[$i], $litery)) {
                for($j=0; $j<32; $j++){
                    if($slowo[$i] == $litery[$j]) {
                        $licznik[$j]++;
                        break;
                    }
                }
            }else
                $blank_counter = 3;
        }else
            $blank_counter++;
    }

    if(strlen($slowo) != $blank_counter)
        $litery_wyst = " WHERE ";
    for($i=0; $i<32; $i++) {
        if($licznik[$i] !=0) {
            $litery_wyst .= $spojnik."substring(letters,".($i+1).",1) >= ".$licznik[$i];
            if($spojnik == "")
                $spojnik = " AND ";
        }
    }

    if($blank_counter < 3) {
        $query = "SELECT WORD FROM ana".$len.$litery_wyst;

        $conn = pfs_connect ('osps');
        $res = @mysql_query($query);

        if(@mysql_num_rows($res) > 0) {
            $i = 0;
            while($row = @mysql_fetch_array($res)) {
                $wynik[$i] = strtr($row[0], $to_ogonki);
                $i++;
            }
        }
    }
}


if ($_GET[s]==ana) {
    $ile = count($wynik);

    (!isset($_GET[i]))  ?   $od=0       :   $od=$_GET[i];
    ($ile - $od <= 50)  ?   $do = $ile  :   $do = $od + 50;

    if ($ile == 0)      echo "<div class='info'>Nie ma słów z podanych liter lub podano za dużo blanków w zapytaniu.</div>";
    else {
        echo "<div class='info'>";
        if (($od - 49) > 0) {
            $i = $od - 50;
            echo("<a href=\"anagramator.php?s=ana&i=$i&slowo=".strtr($szuk, "?", "*")."\" target=\"_self\"><img src=\"files/img/prev.png\" alt='prev'></a>&nbsp;&nbsp;");
        }
        echo(($od + 1)."-".$do." z ".$ile);

        if (($ile - $do) > 0) {
            $i = $od + 50;
            echo("&nbsp;&nbsp;<a href=\"anagramator.php?s=ana&i=$i&slowo=".strtr($szuk, "?", "*")."\" target=\"_self\"><img src=\"files/img/next.png\" alt='next'></a>");
        }
        echo "</div><div class='wyniki'>";

        for ($j = $od, $i = 0; $j <= ($od + 49); $j++, $i++) {
            if($i%10 == 0)
                echo "<div class='kol'>";
            if($wynik[$j] == strtr(strtoupper($slowo), $to_ogonki))
                print "<span class='red'>";
            else
                print "<span class='normal'>";

            echo $wynik[$j]."</span><br />";
            if($i%10 == 9)  echo "</div>";
        }
        echo "</div>";
    }
}
@mysql_close($conn);

require_once "files/php/bottom.php"?>
</body>
</html>
