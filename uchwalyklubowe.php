<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Uchwały : Uchwały dotyczące klubów</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("kluby","uchwalyklubowe");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Uchwały dotyczące klubów")</script></h1>

<h2>Uchwała nr 34/2004 <span>z dnia 3.11.2004 r. w sprawie rejestracji klubów scrabblowych</span></h2>
Realizując cele Stowarzyszenia na podstawie § 7.p.7 Statutu Zarząd Polskiej Federacji Scrabble postanawia co następuje:
<ol>
	<li>Zostaje utworzona ewidencja klubów scrabblowych.</li>
	<li>Ewidencję klubów prowadzi Zarząd.</li>
	<li>Warunkiem rejestracji klubu jest liczba co najmniej sześciu członków klubu.</li>
	<li>Dla celów ewidencji kluby, które pragną się w niej znaleźć podają następujące dane:
		<ul>
			<li>nazwa klubu,</li>
			<li>miejsce i terminy spotkań,</li>
			<li>liczba członków klubu,</li>
			<li>telefon i mail kontaktowy, ew. adres strony internetowej klubu,</li>
			<li>imię nazwisko i kontakt do osoby odpowiedzialnej za funkcjonowanie klubu,</li>
			<li>dane ewentualnych władz klubowych</li>
		</ul>
	</li>
	<li>Zarząd zobowiązuje się do umieszczenia otrzymywanych z klubu informacji o jego działalności (w szczególności: o turniejach, wydarzeniach, rekordach, ciekawostkach i zdjęcia) na Oficjalnej Stronie Internetowej PFS.</li>
	<li>Wszystkie wpisane do ewidencji kluby mają prawo organizacji turniejów rankingowych zgodnie z Uchwałą Zarządu PFS nr 9/2004 p.14.</li>
</ol>

<h2>Uchwała nr 9/2004 <span>z 22.03.2004 r. w sprawie ustalenia obowiązków, które musi wypełnić organizator turnieju Scrabble, aby turniej organizowany przez niego mógł być uznany jako turniej rankingowy.</span></h2>
Niniejsza uchwała reguluje kryteria, które powinny zostać spełnione, aby turniej Scrabble mógł być uznany za turniej rankingowy przez Zarząd Polskiej Federacji Scrabble.<br />
Poniższe kryteria nie stosują się do turniejów szczebla centralnego (w szczególności Mistrzostw Polski i Pucharu Polski).<br />
Ostateczną decyzję w sprawie zakwalifikowania turnieju jako rankingowy podejmuje Zarząd PFS.<br /><br />

Aby turniej mógł być uznany za rankingowy przez Zarząd PFS muszą być spełnione przy jego organizacji następujące warunki:
<ol>
	<li>zgłoszenie na formularzu (stanowiącym załącznik nr 1 do niniejszej uchwały) planowanego turnieju z wyprzedzeniem co najmniej 30 dniowym. Po otrzymaniu Zarząd PFS powiadamia organizatora turnieju o tym, czy dany turniej został uznany za rankingowy,</li>
	<li>planowany termin turnieju nie może pokrywać się z terminami turniejów szczebla centralnego i turniejów z cyklu Grand Prix,</li>
	<li>turniej powinien się odbywać w dni ustawowo wolne od pracy (soboty, niedziele lub święta). Do wyjątków należy zaliczyć taką sekwencję dni, podczas których pomiędzy dniami wolnymi od pracy znajduje się jeden dzień roboczy,</li>
	<li>liczba rund turnieju nie może być mniejsza niż 6,</li>
	<li>turniej powinien mieć formułę otwartą to znaczy, że każdy kto zechce, może wziąć w nim udział. Jedynym ograniczeniem może być pojemność sali,</li>
	<li>turniej powinien być prowadzony:
		<ol type="a">
			<li>przez sędziego z uprawnieniami PFS,</li>
			<li>przy pomocy programu sędziowskiego PFS,</li>
			<li>wg regulaminu turniejowego PFS,</li>
		</ol>
	</li>
	<li>wpisowe do turnieju nie może być wyższe niż ustalone przez PFS. Organizatorzy nie mają prawa pobierania od uczestników żadnych innych, obowiązkowych opłat,</li>
	<li>zagwarantowanie właściwych warunków lokalowe do gry, w tym:
		<ol type="a">
			<li>przestrzeń ogólna dla co najmniej 50 osób,</li>
			<li>minimalna szerokość stanowiska do gry - 80 cm,</li>
			<li>wydzielenie odrębnego stanowiska dla sędziego,</li>
			<li>dobre oświetlenie sali,</li>
			<li>odpowiednia liczba stolików i krzeseł,</li>
			<li>toalety.</li>
		</ol>
	</li>
	<li>zapewnienie turniejowych kompletów do gry przy wykorzystaniu możliwości i zasobów PFS oraz zegarów (w przypadku pokrywania się terminów turniejów, przy wypożyczeniu kompletów do gry decyduje pierwszeństwo zgłoszenia turnieju),</li>
	<li>poinformowanie graczy o dostępnej bazie noclegowej oraz możliwościach wyżywienia,</li>
	<li>zapewnienie dodatkowego komputera z aktualną wersją OSPS przeznaczonego w trakcie turnieju wyłącznie do sprawdzania wyrazów,</li>
	<li>poinformowanie mediów lokalnych o turnieju, oraz dołożenie wszelkich starań w celu zapewnienia turniejowi właściwej oprawy medialnej,</li>
	<li>zapewnienie nagród dla zwycięzców,</li>
	<li>turnieje organizowane przez kluby scrabblowe mogą uzyskać status turnieju rankingowego nie częściej niż raz na kwartał.</li>
</ol>


<h2>Załącznik nr 1 do uchwały </h2>
<a href="rozne/zgloszenie_turnieju.doc">ZGŁOSZENIE TURNIEJU</a><br /><br />
Informacje do zamieszczenia na stronie PFS:
<ol>
	<li>Termin</li>
	<li>Nazwa turnieju</li>
	<li>Rodzaj turnieju (otwarty - czy są jakieś ograniczenia, zamknięty - kto może brać udział)</li>
	<li>Sposób rozgrywania (liczba rund, system: połówkowy, szwajcar, duńczyk, pucharowy, inne)</li>
	<li>Informacja o obrońcy tytułu</li>
	<li>Miejsce rozgrywek</li>
	<li>Harmonogram</li>
	<li>Informacja o wysokości wpisowego</li>
	<li>Dojazd</li>
	<li>Wyżywienie</li>
	<li>Noclegi</li>
	<li>Dodatkowe atrakcje</li>
	<li>Nagrody</li>
	<li>Informacja o zgłoszeniach uczestników</li>
	<li>Organizatorzy (kontakt - telefony, maile)</li>
	<li>Adres strony turnieju</li>
</ol><br />
Informacje dla Zarządu PFS:
<ol>
	<li>Opis miejsca rozgrywek (sala - wielkość, oświetlenie, wyposażenie)</li>
	<li>Informacja o sprzęcie będącym w dyspozycji organizatorów (zestawy, zegary, komputery)</li>
	<li>Sędzia (wymagane potwierdzenie)</li>
	<li>Informacja w środkach masowego przekazu</li>
	<li>Sponsorzy</li>
</ol>

<?require_once "files/php/bottom.php"?>
</body>
</html>

