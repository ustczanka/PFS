<?include_once "files/php/funkcje.php";?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Sędziowie PFS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("pfs","sedziowie");</script>
  <style type="text/css">
    ol li{
    line-height:24px;
    }
    td{
    padding-right:8px;
    }
  </style>
</head>


<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Sędziowie PFS")</script></h1>

<h2>Sędziowie posiadający licencję I stopnia <span>(mogą prowadzić wszystkie turnieje):</span></h2>
<table>
        <tr>
        <td>1.</td>
        <td>Sylwia Buks (Warszawa)</td>
        <td><a onClick="sendMail('bezpieka2','wp.pl')" class="email"></a></td>
        <td>telefon: 663 984 463</td>
        <td><!--<img src="http://status.gadu-gadu.pl/users/status.asp?id=" alt="" />--></td>
        <td><!--<a href="gg:"></a>--></td>
    </tr>
    <tr>
        <td>2. </td>
        <td>Andrzej Gostomski (Łódź)</td>
        <td><a onClick="sendMail('coalaand','wp.pl')" class="email"></a></td>
        <td>telefon: 605 447 933</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=11857770" alt="" /></td>
        <td><a href="gg:11857770">11857770</a></td>
    </tr>
    
     <tr>
        <td>3.</td>
        <td>Adam Janicki (Łódź)</td>
        <td><a onClick="sendMail('janicki.adam','interia.pl')" class="email"></a></td>
        <td>telefon: 502 949 180</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=2807324" alt="" /></td>
        <td><a href="gg:2807324">2807324</a></td>
    </tr>
    
    <tr>
        <td>4.</td>
        <td>Andrzej Lożyński (Piastów)</td>
        <td><a onClick="sendMail('prezes','upcpoczta.pl')" class="email"></a></td>
        <td>telefon: 602 278 365</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
       </tr>
    <tr>
        <td>5.</td>
        <td>Jarosław Puchalski (Łódź)</td>
        <td><a onClick="sendMail('jarek.puchalski','gmail.com')" class="email"></a></td>
        <td>telefon: 605 546 654</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=18249" alt="" /></td>
        <td><a href="gg:18249">18249</a></td>
    </tr>
    <tr>
        <td>6.</td>
        <td>Weronika Rudnicka (Szczecin)</td>
        <td><a onClick="sendMail('ustczanka','gmail.com')" class="email"></a></td>
        <td>telefon: 660 429 150</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=433100" alt="" /></td>
        <td><a href="gg:433100">433100</a></td>
    </tr>
    <tr>
        <td>7.</td>
        <td>Mariusz Sylwestrzuk (Warszawa)</td>
        <td><a onClick="sendMail('msylwest','gmail.com')" class="email"></a></td>
        <td>telefon: 603 692 502</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=2456681" alt="" /></td>
        <td><a href="gg:2456681">2456681</a></td>
    </tr>
    <tr>
        <td>8.</td>
        <td>Maciej Śliwa (Zabrze)</td>
        <td><a onClick="sendMail('maciej','sliwa.zabrze.pl')" class="email"></a></td>
        <td>telefon: 601 916 600</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=4674486" alt="" /></td>
        <td><a href="gg:4674486">4674486</a></td>
    </tr>
    <tr>
        <td>9.</td>
        <td>Grzegorz Wiączkowski (Wrocław)</td>
        <td><a onClick="sendMail('broda','imaster.pl')" class="email"></a></td>
        <td>telefon: 697 033 468</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=5200934" alt="" /></td>
        <td><a href="gg:5200934">5200934</a></td>
    </tr>

    <tr>
        <td>10.</td>
        <td>Miłosz Wrzałek (Reda) </td>
        <td><a onClick="sendMail('milosz.wrzalek','gmail.com')" class="email"></a></td>
        <td>telefon: 606 763 452</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=1952931" alt="" /></td>
        <td><a href="gg:1952931">1952931</a></td>
    </tr>
    
    <tr>
        <td>11.</td>
        <td>Karol Wyrębkiewicz (Łódź)</td>
        <td><a onClick="sendMail('k.wyrebkiewicz','pfs.org.pl')" class="email"></a></td>
        <td>telefon: 691 664 325</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=3211214" alt="" /></td>
        <td><a href="gg:3211214">3211214</a></td>
    </tr>
   <tr>
        <td>12.</td>
        <td>Sławomir Zabawa (Wałcz)</td>
        <td><a onClick="sendMail('grafin','post.pl')" class="email"></a></td>
        <td>telefon: 602 299 683</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=40020" alt="" /></td>
        <td><a href="gg:1231530">40020</a></td>
    </tr>
    <tr>
        <td>13.</td>
        <td>Mateusz Żbikowski (Warszawa)</td>
        <td><a onClick="sendMail('rzbik','wp.pl')" class="email"></a></td>
        <td>telefon: 663 052 271</td>
        <td><img src="http://status.gadu-gadu.pl/users/status.asp?id=152268" alt="" /></td>
        <td><a href="gg:152268">152268</a></td>
    </tr>




</table>

<h2>Sędziowie posiadający licencję II stopnia <span>(mogą prowadzić wszystkie turnieje za wyjątkiem MP, PP i cyklu Grand Prix):</span></h2>
<table>




</table>
<br /><i>Uwaga: Po otrzymaniu licencji II stopnia sędzia nabywa automatycznie licencję I stopnia po przeprowadzeniu dwóch turniejów.</i>

<h2>Jak zostać sędzią?</h2>
Regulacje dotyczące sędziów w Polskiej Federacji Scrabble określa <a href="regulaminsed.php">regulamin sędziowski</a> z dnia 8 września 2005, wraz&nbsp;z&nbsp;załącznikiem - <a href="pytania_sedz.php">zestawem pytań</a> na egzamin teoretyczny.
<h2>Diety dla sędziów</h2>
Zarząd informuje, że zasady wypłacania diet dla sędziów PFS nie są uregulowane. Kwestie związane w honorarium za&nbsp;sędziowanie turnieju należy ustalać indywidualnie z wybranym sędzią.
<br><br>
<a href="http://www.pfs.org.pl/instalacja.php">Instrukcja instalacji programu sędziowskiego.</a>

<?require_once "files/php/bottom.php"?>
</body>
</html>
