
function odswiezRundy (ilosc_rund, id_turnieju){
    if(ilosc_rund <= 12)            $("#buttonContainer").height(60);
    else if(ilosc_rund <= 24)       $("#buttonContainer").height(120);
    else if(ilosc_rund <= 36)       $("#buttonContainer").height(190);

    var db = new Array ();

    $.ajax ({
        type: "POST",
        url:  "http://www.pfs.org.pl/live/rounds.php",
        data: "id=" + id_turnieju,
        async: false,
        success: function (data) {
            db = eval('(' + data + ')');
        }
    });

    var pliki = new Array ();
    for (i = 1; i <= db.length; i++) {
        if (db[i-1] && (db[i-1]['roz'] || db[i-1]['wyn'] || db[i-1]['kla'] || db[i-1]['kom'])) {
            runda           = db[i-1]['runda'];
            pliki[runda-1]  = db[i-1];
            if (db[i-1]['roz']) $("#tab_" + runda + " div.roz").html (db[i-1]['roz']);
            if (db[i-1]['wyn']) $("#tab_" + runda + " div.wyn").html (db[i-1]['wyn']);
            if (db[i-1]['kla']) $("#tab_" + runda + " div.kla").html (db[i-1]['kla']);
            if (db[i-1]['kom']) $('#tab_' + runda + " div.kom").html (db[i-1]['kom']);
        }
    }

    $("ul.tabNavigation a").click (function () {
        $(".tabContainer .tabcontent").hide ().filter (this.hash).show ();
        $(".tabNavigation a").removeClass ("tabSelected");
        $(this).addClass ("tabSelected");
        return false;
    });
    var $button;
    $("#buttonContainer").empty ();
    for (i = 1; i <= ilosc_rund; i++) {
        $button = $('<div/>', { 'class': "button", id: "button_" + i }).text (i);
        if (pliki[i-1] && (pliki[i-1]['roz'] != null || pliki[i-1]['wyn'] != null || pliki[i-1]['kla'] != null || pliki[i-1]['kom'] != null)) {
            $button.addClass ("enabled");
        }
        $("#buttonContainer").append ($button);
    }
    $last = $(".enabled").last ();
    if ($last.attr ("id") == "button_" + ilosc_rund && pliki[ilosc_rund-1]['kla']) {
        $last.addClass ("pressed");
        $("div#tab_" + ilosc_rund + " ul.tabNavigation a#tab_klasyfikacja").click ();
    }
    else {
        $last.addClass ("actual");
    }

    $("#buttonContainer .enabled, #buttonContainer .pressed").click (function() {
        $("#buttonContainer .button").removeClass ("pressed");
        $("div.tabContainer").hide ();
        tabid = "#" + $(this).attr ("id").replace ("button", "tab");
        $(tabid).show();
        $(this).addClass ("pressed");
        sel_tab_id = $("a.tabSelected").attr ("id");
        if (!sel_tab_id) {
            sel_tab_id = $("ul.tabNavigation a:first").attr ("id");
        }
        $(".tabNavigation a#" + sel_tab_id, $(tabid)).click();
    });

    if ($(".pressed").length) {;
        $visible = $(".pressed");
    }
    else {
        $visible = $(".actual");
    }

    $visible.click ();
}
