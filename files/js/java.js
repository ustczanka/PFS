function nowosci(){
    window.open('nowosci.php', 'nowosci', 'menubar=0, toolbar=0, location=0, status=0, scrollbars=0, resizable=0, width=600, height=400');
}

function naglowek(tytul){
    for(i=0; i<tytul.length; i++)
    {
        var a = tytul[i].toLowerCase();
        switch(a) {
            case 'ą': a='a5'; break;
            case 'ć': a='c6'; break;
            case 'ę': a='e5'; break;
            case 'ł': a='l3'; break;
            case 'ń': a='n7'; break;
            case 'ó': a='o5'; break;
            case 'ś': a='s5'; break;
            case 'ż': a='z5'; break;
            case 'ź': a='z9'; break;
            case ' ': a='spacja'; break;
            default:  break;
        }

        document.write("<img src='http://www.pfs.org.pl/files/tiles/" + a + ".png' alt='" + tytul[i] + "'>");
    }
}

function jKalendarz(){
    $(document).ready(function(){
        $("#nextTours div.nextTour").hover(function() {
            $(this).find("div.kartka").animate({opacity: "show"}, "slow");
            $(this).find("div.karteczka").css("opacity", "0");
        }, function() {
                $(this).find("div.kartka").animate({opacity: "hide"}, "fast");
                $(this).find("div.karteczka").css("opacity", "100");
        });
    });
}

function jSubmenu(zakladka, zakladka2){
    $(document).ready(function(){
        $("#menu div.tab").parent().removeClass("active");
        $("#m_"+zakladka).addClass("active");
        $("#menu .sublink").removeClass("activesub");
        $("#"+zakladka2).addClass("activesub");
    });
}

function sendMail(b, domena) {
    if (b != '') {
        var a = 'mailto:';
        var c = '%40';
        window.open([a]+[b]+[c]+[domena], '_blank');
    }
}

function wyczyscInfoTurnieju(){
    $("#tourInfo").hide ();
}

function infoTurnieju(){
    id = $(this).attr ('name');
    coords = $(this).attr ('coords').split(',');

    $('#tourInfo').css ({ top: parseInt ($.trim (coords[1]))+40 , left: parseInt ($.trim (coords[0])) +25 });
    $('#tourInfo').html (turnieje[id]);
    $('#tourInfo').slideDown (160);
}

function getElementsByClassName(cn){
  var arr = new Array();
  var els = document.getElementsByTagName("*");
  var exp= new RegExp("^(.* )?"+cn+"( .*)?$", "g");
  for (var i = 0; i < els.length; i++ ){
    if (exp.test(els[i].className)){
      arr.push(els[i]);
    }
  }
  return arr;
}

function setMenu(){
    var menuel  = [ '#m_glowna', '#m_turnieje', '#m_pfs', '#m_scrabble', '#m_ranking', '#m_scrabblisci', '#m_kluby', '#m_roznosci', '#m_english' ];

    function getXPos(obj){
        id = obj.parent().attr('id');
        for ( i=0; i<menuel.length; i++ ){
            if ('#'+id == menuel[i] )
                return i*(-114) - 4;
        }
        return 0;
    }

    $(document).ready(function(){
        $('#menu .tab').bind('mouseover', fmouseover);
        $('#menu .tab').bind('mouseout', fmouseout);
        $('.active').children('div').css({'marginTop' : '41px'});
        $('.activesub').children('a').prepend("<img style='margin:-1px 2px;' src='http://www.pfs.org.pl/files/img/kulka.png'>");
    });

    function fmouseover(){
        if (this.parentNode.className.indexOf('active') >= 0) return;
        $(this).stop().animate({backgroundPosition: '(' + getXPos($(this)) + 'px 8px)'},350, function()
                                                                                                { $(this).animate({backgroundPosition: '(' + getXPos($(this)) + 'px 4px)'},200);});
    }

    function fmouseout(e){
        if (this.parentNode.className.indexOf('active') >= 0)   return;
        $(this).stop().animate({backgroundPosition: '(' + getXPos($(this)) + 'px -4px)'}, 200, function()
                                                        {$(this).animate({backgroundPosition: '(' + getXPos($(this)) + 'px 0)'},200);});
    }
}
