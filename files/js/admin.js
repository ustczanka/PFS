$(function() {
    $( ".button, ul.menu li a" ).button();
    dodajKalendarzyk ($('input.datepicker'));
});

function wstaw(elem){
	ta = document.getElementsByName(elem)[0];
	switch(elem){
		case 'wpisowe':
		ta.value = "20 zł (19 zł + 1 zł na cele statutowe PFS) za dzień<br>\nczłonkowie PFS z aktualnie opłaconymi składkami - 12,5 zł za dzień<br>\nmłodzież szkolna - 7,5 zł za dzień (do liceum włącznie)<br>\ndebiutanci - bez opłat";
		break;

		case 'sposob':
		ta.value = "liczba rund: <b>12</b><br>\nsystem: połówkowy (rundy 1-11), duńczyk (runda 12)";
		break;

		case 'harmonogram':
		ta.value = "<b>Sobota, 00 stycznia (7 rund)</b><br>\n12:00 - rozpoczęcie turnieju<br>\n15:00-16:00 - przerwa obiadowa<br>\n20:00 - zakończenie gier<br><br>\n\n<b>Niedziela, 00 stycznia (5 rund)</b><br>\n10:00 - rozpoczęcie drugiego dnia rozgrywek<br>\n15:00 - rozdanie nagród i zakończenie turnieju";
		break;

		case 'rodzaj':
		if(ta.value != '')	ta.value = ta.value + " ";
		ta.value = ta.value + "<br /><a href='#lista'>Lista uczestników</a>";
		break;

		case 'uczestnicy':
		if(ta.value != '')	ta.value = ta.value + "\n";
		ta.value = ta.value + "<tr><td>NR</td><td>IMIE_I_NAZWISKO</td><td>MIASTO</td></tr>";
		break;

		case 'data':
		var today = new Date();
		ta.value = today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate();
		break;
	}
}

function odznaczInne(){
	if ($("#wczasy").is (":checked")) {
		$("#rank, #gp").attr ('checked', false).attr ('disabled', 'disabled');
        $("#gp").prev ("label:first").addClass ("ui-button-disabled ui-state-disabled");
        $("#rank").prev ("label:first").addClass ("ui-button-disabled ui-state-disabled");
	}
	else{
        $("#rank, #gp").attr ('disabled', false);
        $("#gp").prev ("label:first").removeClass ("ui-button-disabled ui-state-disabled");
        $("#rank").prev ("label:first").removeClass ("ui-button-disabled ui-state-disabled");
	}
}

function aktualizujNapis(frm, miasto, zprzycisku){
	if (miasto in miasteczka){
		if(zprzycisku){
			frm['napisx'].value = miasteczka[miasto]['napisx'];
			frm['napisy'].value = miasteczka[miasto]['napisy'];
		}
		(frm['napisx'].value) ? nx = frm['napisx'].value : nx = 0;
		(frm['napisy'].value) ? ny = frm['napisy'].value : ny = 0;
		document.getElementById('miasta').innerHTML = "<img src='http://www.pfs.org.pl/pliki_strony/img/miasta/"+miasteczka[miasto]['skrot']+".png' class='punkt' style='top:"+(ny-1)+"px;left:"+(nx-1)+"px;border:1px solid white;'/>";
	}
}

function aktualizujPunkt(frm, miasto, zprzycisku){
	if ((miasto in miasteczka) && zprzycisku)
	{
		frm['wspx'].value = miasteczka[miasto]['wspx'];
		frm['wspy'].value = miasteczka[miasto]['wspy'];
	}
	(frm['wspx'].value) ? x = frm['wspx'].value : x = 0;
	(frm['wspy'].value) ? y = frm['wspy'].value : y = 0;
	diam = 5;
	mist = document.getElementsByName('typ')[0].selectedIndex;
	rank = document.getElementsByName('rank')[0].checked;
	gp = document.getElementsByName('gp')[0].checked;
	wczasy = document.getElementsByName('wczasy')[0].checked;
	typ = sprawdzTypTurnieju(gp, mist, rank, wczasy);
	document.getElementById('punkty').innerHTML = "<img src='http://www.pfs.org.pl/pliki_strony/img/"+typ+".png' class='punkt' style='top:"+(y-1)+"px;left:"+(x-1)+"px;border:1px solid white;'/>";
}

function sprawdzTypTurnieju(gp, mist, rank, wczasy){
	if(gp) typ = "gp";
	else if (mist) typ = "mist";
	else if (rank) typ="rank";
	else if (wczasy) typ = "wczasy";
	else typ = "nierank";
	return typ;
}

function dodajKalendarzyk(selektor){
	selektor.datepicker({
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        gotoCurrent: true,
        nextText: 'Dalej',
        prevText: 'Wcześniej',
        showAnim: 'fadeIn',
        dayNames: ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'],
        dayNamesMin: ['Nd', 'Po', 'Wt', 'Śr', 'Cz', 'Pt', 'So'],
        buttonImage: 'http://pfs.org.pl/files/img/datepicker.gif',
        showOn: 'button',
        buttonImageOnly: true,
        buttonText: 'Kalendarz',
        monthNames: ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień']}
    );
	selektor.datepicker();
}

Array.prototype.inArray = function(v){
  for(var i in this){
    if(this[i] == v){
      return true;}
    }
  return false;
}

function confirmDelete (txt) {
    if (confirm ('Czy na pewno usunąć ' + txt + '?')) {
        return true;
    }
    return false;
}

var Loader = {
    loader_obj: null,
    init: function () {
        this.loader_obj = $("<div/>", { id: "loader" }).append ($("<div/>"));
        $("body").prepend (this.loader_obj);
    },
    show: function () {
        this.loader_obj.show ();
    },
    hide: function () {
        this.loader_obj.hide ();
    },
}

var Dialog = {
    dialog_obj: null,
    init: function () {
        this.dialog_obj = $("<div/>", { id: "msg-dialog" });
        this.dialog_obj.dialog ({
            open: function () {
                $(this).html ("<p>" + $(this).data('msg') + "</p>");
            },
            autoOpen: false,
            resizable: false
        });
    },
    show: function (msg) {
        this.dialog_obj.data ('msg', msg).dialog ("open");
    }
}
