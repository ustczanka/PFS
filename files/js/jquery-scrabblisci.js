
$(document).ready(function(){
  var slideWidth = 720;
  var slides = $('.slide');
  var thumbs = $('.thumb');
  var numberOfSlides = slides.length;
  var currentPosition = slides.length;

	$('#slidesContainer').css('overflow', 'hidden');
	slides
		.wrapAll('<div id="slideInner"></div>')
		.css({
			'float' : 'left',
			'width' : slideWidth
		});
	$('#slideInner').css('width', slideWidth * numberOfSlides);
	$('#slideshow')
		.prepend('<span class="control" id="leftControl">Poprzedni</span>')
		.append('<span class="control" id="rightControl">Następny</span>');

	manageControls(currentPosition);
	$('.control').bind('click', function(){
		currentPosition = ($(this).attr('id')=='rightControl') ? currentPosition-1 : currentPosition+1;
		manageControls(currentPosition);
		$('#slideInner').animate({'marginLeft' : slideWidth*(currentPosition-slides.length)});
	});

	thumbs.bind('click', function(){
		currentPosition = thumbs.index(this) + 1;
		manageControls(currentPosition);
		$('#slideInner').animate({'marginLeft' : slideWidth*(currentPosition-slides.length)});
	});
	
	function manageControls(position){
		if(position==slides.length){ $('#leftControl').hide() } else{ $('#leftControl').show() }
		if(position==1){ $('#rightControl').hide() } else{ $('#rightControl').show() }
	}	
});
