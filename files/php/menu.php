<script type="text/javascript">setMenu();</script>

<div id="top" onclick="location.href='http://www.pfs.org.pl'">
    <a href="http://www.pfs.org.pl/rss.php" id="rss"></a>
</div>
<div id="menu">

    <div id="m_glowna">
        <div class="tab"><a href="http://www.pfs.org.pl/index.php"></a></div>

        <ul>
            <li class="sublink" id="aktualnosci"><a href="http://www.pfs.org.pl">Aktualności</a></li>
            <li class="sublink" id="anagramator"><a href="http://www.pfs.org.pl/anagramator.php">Anagramator</a></li>
            <li class="sublink" id="siodemka"><a href="http://www.pfs.org.pl/siodemka.php">Siódemka tygodnia</a></li>
            <li class="sublink"><a href="http://www.scrabblewszkole.pl/">„Scrabble w szkole”</a></li>
            <li class="sublink"><a href="http://forum.pfs.org.pl">Forum PFS</a></li>
            <li class="sublink" id="kontakt"><a href="http://www.pfs.org.pl/kontakt.php">Kontakt</a></li>
        </ul>
    </div>
    <div id="m_turnieje">
        <div  class="tab"><a href="http://www.pfs.org.pl/kalendarz.php"></a></div>
        <ul>
            <li class="sublink" id="kalendarz"><a href="http://www.pfs.org.pl/kalendarz.php">Kalendarz</a></li>
            <li class="sublink" id="gp2014"><a href="http://www.pfs.org.pl/gp2014.php">Grand Prix</a></li>
            <li class="sublink" id="mr2014"><a href="http://www.pfs.org.pl/mr2014.php">Mistrzostwa Regionów</a></li>
            <li class="sublink" id="archiwum"><a href="http://www.pfs.org.pl/archiwum.php">Historia</a></li>
            <li class="sublink" id="galeria"><a href="http://www.pfs.org.pl/galeria.php">Zdjęcia</a></li>
            <li class="sublink" id="rekordy"><a href="http://www.pfs.org.pl/rekordy.php">Rekordy</a></li>
            <li class="sublink" id="regulaminturniejowy"><a href="http://www.pfs.org.pl/regulamintur.php">Regulamin turniejowy</a></li>
            <li class="sublink" id="regulaminsedziowski"><a href="http://www.pfs.org.pl/regulaminsed.php">Regulamin sędziowski</a></li>
        </ul>
    </div>
    <div id="m_pfs">
        <div  class="tab"><a href="http://www.pfs.org.pl/opfs.php"></a></div>
        <ul>
            <li class="sublink" id="opfs"><a href="http://www.pfs.org.pl/opfs.php">O PFS</a></li>
            <li class="sublink" id="jakprzystapic"><a href="http://www.pfs.org.pl/jakprzystapic.php">Jak przystąpić</a></li>
            <li class="sublink" id="dlaczlonkow"><a href="http://www.pfs.org.pl/dlaczlonkow.php">Dla członków</a></li>
            <li class="sublink" id="wladze"><a href="http://www.pfs.org.pl/wladze.php">Władze</a></li>
            <li class="sublink" id="sedziowie"><a href="http://www.pfs.org.pl/sedziowie.php">Sędziowie</a></li>
            <li class="sublink" id="statut"><a href="http://www.pfs.org.pl/statut.php">Statut</a></li>
            <li class="sublink" id="uchwaly"><a href="http://www.pfs.org.pl/uchwaly.php">Uchwały</a></li>
            <li class="sublink" id="finanse"><a href="http://www.pfs.org.pl/finanse.php">Finanse</a></li>
            <li class="sublink"><a href="http://forum.pfs.org.pl">Forum PFS</a></li>
            <li class="sublink" id="dlasponsorow"><a href="http://www.pfs.org.pl/dlasponsorow.php">Dla sponsorów</a></li>
        </ul>
    </div>
    <div id="m_scrabble">
        <div  class="tab"><a href="http://www.pfs.org.pl/mattel.php"></a></div>
        <ul>
            <li class="sublink" id="mattel"><a href="http://www.pfs.org.pl/mattel.php">Mattel</a></li>
            <li class="sublink" id="reguly"><a href="http://www.pfs.org.pl/reguly.php">Reguły</a></li>
            <li class="sublink" id="historia"><a href="http://www.pfs.org.pl/historia.php">Historia</a></li>
            <li class="sublink" id="odmiany"><a href="http://www.pfs.org.pl/odmiany.php">Odmiany</a></li>
            <li class="sublink" id="osps"><a href="http://www.pfs.org.pl/osps.php">Słownik (OSPS)</a></li>
            <li class="sublink" id="dwojki"><a href="http://www.pfs.org.pl/dwojki.php">2-literówki</a></li>
            <li class="sublink" id="trojki"><a href="http://www.pfs.org.pl/trojki.php">3-literówki</a></li>
            <li class="sublink" id="slowneroznosci"><a href="http://www.pfs.org.pl/slowneroznosci.php">Słowne różności</a></li>
            <li class="sublink" id="slowniczek"><a href="http://www.pfs.org.pl/slowniczek.php">Słowniczek</a></li>
        </ul>
    </div>
    <div id="m_ranking">
        <div  class="tab"><a href="http://www.pfs.org.pl/ranking.php"></a></div>
        <ul>
            <li class="sublink" id="aktranking"><a href="http://www.pfs.org.pl/ranking.php">Aktualny ranking</a></li>
            <li class="sublink" id="medale"><a href="http://www.pfs.org.pl/medale.php">Klasyfikacja medalowa</a></li>
            <li class="sublink"><a href="http://scrabble.stats.org.pl/">Statystyki</a></li>
        </ul>
    </div>
    <div id="m_scrabblisci">
        <div  class="tab"><a href="http://www.pfs.org.pl/mistrzowiepolski.php"></a></div>
        <ul>
            <li class="sublink" id="mistrzowiepolski"><a href="http://www.pfs.org.pl/mistrzowiepolski.php">Mistrzowie Polski</a></li>
            <li class="sublink" id="zdobywcypp"><a href="http://www.pfs.org.pl/zdobywcypp.php">Zdobywcy Pucharu Polski</a></li>
            <li class="sublink" id="zwyciezcygp"><a href="http://www.pfs.org.pl/zwyciezcygp.php">Zwycięzcy Grand Prix</a></li>
            <li class="sublink" id="liderzyrankingu"><a href="http://www.pfs.org.pl/liderzyrankingu.php">Liderzy rankingu</a></li>
        </ul>
    </div>
    <div id="m_kluby">
        <div  class="tab"><a href="http://www.pfs.org.pl/kluby.php"></a></div>
        <ul>
            <li class="sublink" id="rejestrklubow"><a href="http://www.pfs.org.pl/kluby.php">Rejestr klubów</a></li>
            <li class="sublink" id="kmp"><a href="http://www.pfs.org.pl/kmp.php">KMP</a></li>
            <li class="sublink" id="wiescizklubow"><a href="http://www.pfs.org.pl/wiescizklubow.php">Wieści z klubów</a></li>
            <li class="sublink"><a href="http://scrabble.stats.org.pl/about.php">Statystyki klubowe</a></li>
        </ul>
    </div>
    <div id="m_roznosci">
        <div  class="tab"><a href="http://www.pfs.org.pl/scrabblealia.php"></a></div>
        <ul>
            <li class="sublink" id="scrabblealia"><a href="http://www.pfs.org.pl/scrabblealia.php">Scrabblealia</a></li>
            <li class="sublink" id="anegdoty"><a href="http://www.pfs.org.pl/anegdoty.php">Anegdoty</a></li>
            <li class="sublink" id="programy"><a href="http://www.pfs.org.pl/programy.php">Programy</a></li>
            <li class="sublink" id="download"><a href="http://www.pfs.org.pl/download.php">Do pobrania</a></li>
            <li class="sublink" id="ksiazki"><a href="http://www.pfs.org.pl/ksiazki.php">Książki</a></li>
            <li class="sublink" id="scrabblenet"><a href="http://www.pfs.org.pl/scrabblenet.php">Scrabble w internecie</a></li>
            <li class="sublink" id="franko"><a href="http://www.pfs.org.pl/franko.php">Scrabble frankofońskie</a></li>
        </ul>
    </div>
    <div id="m_english">
        <div  class="tab"><a href="http://www.pfs.org.pl/english.php"></a></div>
        <ul>
            <li class="sublink" id="about"><a href="http://www.pfs.org.pl/english.php">About Polish Scrabble</a></li>
            <li class="sublink" id="news"><a href="http://www.pfs.org.pl/en_news.php">News</a></li>
            <li class="sublink" id="bartek"><a href="http://www.pfs.org.pl/bartek.php">Bartek w Johor Bahru</a></li>
        </ul>
    </div>
</div>

<div id="submenu"></div>
<div id="content">
