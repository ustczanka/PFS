﻿<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Wydarzenia towarzyszące WSC 2011 :: Turnieje w empik school</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>

<?require_once "../files/php/menu.php"?>
<h1>Wydarzenia towarzyszące WSC 2011 :: Turnieje w empik school</h1>

<center>
<img src="http://pfs.org.pl/rozne/wsc_turnieje_empik.jpg">
</center>

<div>
<p><b>Jeden dzień, 16 szkół językowych empik school, wspólny cel - zabawa słowami. <br>
2 października (niedziela) 2011 roku, największe miasta Polski zmienią się w arenę zmagań entuzjastów Scrabble. Wszystkie zainteresowane osoby staną do walki, której główną i jedyną bronią będą słowa. Na najlepszych czekają nagrody!</b><br><br>
Rozgrywki Scrabble w szkołach języków obcych empik school, pozwolą przeżyć emocje towarzyszące turniejowej rywalizacji wszystkim amatorom słownych potyczek. W każdym mieście uczestnicy będą mogli sami zdecydować czy chcą rywalizować w angielskiej czy polskiej wersji językowej Scrabble. <br>
</p>

<p>W zawodach może wziąć udział każdy, kto chce sprawdzić swoje umiejętności strategiczne i zasób słów, a przy tym powalczyć o atrakcyjne nagrody. Są to m.in.</p>
<ul>
<li>Kursy semestralne w empik school</li>
<li>Zestawy Scrabble</li>
<li>Multimedialne kursy biznesowe</li>
<li>Plecaki Campus</li>
<li>Vouchery rabatowe na kursy w empik school</li>
<li>Oraz mnóstwo gadżetów i upominków</li>
</ul>

<p>Na 8 najlepszych uczestników 16 października czeka Wielki Finał w Warszawie, w którym będą mogli wygrać profesjonalne plecaki turystyczne Campus.</p>
<p>Aby wziąć udział w turnieju należy do 1 października zgłosić się do jednej ze szkół sieci empik school lub zarejestrować się poprzez stronę internetową: <a href="http://ww.empikschool.com/lp/ES_JESIEN11_TURNIEJ" target="_blank">www.empikschool.com</a> <br>

<p>Lista szkół empik school organizujących turnieje:</p>
<table>
<tr><td>Białystok</td><td>&nbsp;&nbsp;</td><td>ul. Białówny 9/1</td></tr>
<tr><td>Bydgoszcz</td><td>&nbsp;&nbsp;</td><td>ul. Focha 4</td></tr>
<tr><td>Gdańsk Wrzeszcz</td><td>&nbsp;&nbsp;</td><td>ul. Grunwaldzka 82 (CH Manhattan - III piętro)</td></tr>
<tr><td>Katowice</td><td>&nbsp;&nbsp;</td><td>ul. 3-go Maja 11</td></tr>
<tr><td>Kielce</td><td>&nbsp;&nbsp;</td><td>ul. Warszawska 5</td></tr>
<tr><td>Kraków</td><td>&nbsp;&nbsp;</td><td>ul. Rynek Główny 5</td></tr>
<tr><td>Lublin</td><td>&nbsp;&nbsp;</td><td>ul. Krakowskie Przedmieście 40</td></tr>
<tr><td>Łódź</td><td>&nbsp;&nbsp;</td><td>ul. Narutowicza 8/10</td></tr>
<tr><td>Olsztyn</td><td>&nbsp;&nbsp;</td><td>ul. 1-go Maja 18/19 (I piętro)</td></tr>
<tr><td>Opole</td><td>&nbsp;&nbsp;</td><td>ul. Krakowska 45/47</td></tr>
<tr><td>Poznań</td><td>&nbsp;&nbsp;</td><td>ul. 27-go Grudnia 17/19 (Domar nad McDonalds)</td></tr>
<tr><td>Rzeszów</td><td>&nbsp;&nbsp;</td><td>ul. Rynek 15</td></tr>
<tr><td>Szczecin</td><td>&nbsp;&nbsp;</td><td>Pl. Brama Portowa 4</td></tr>
<tr><td>Warszawa</td><td>&nbsp;&nbsp;</td><td>ul. Marszałkowska 104/122 (DH Junior)</td></tr>
<tr><td>Wrocław</td><td>&nbsp;&nbsp;</td><td>Pl. Teatralny 8</td></tr>
<tr><td>Zielona Góra</td><td>&nbsp;&nbsp;</td><td>ul. Bohaterów Westerplatte 19</td></tr>
</table>


</div>








<?require_once "../files/php/bottom.php"?>
</body>
</html>

