﻿<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Wydarzenia towarzyszące WSC 2011 :: Dzień Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>

<?require_once "../files/php/menu.php"?>
<h1>Wydarzenia towarzyszące WSC 2011 :: Dzień Scrabble</h1>

<center>
<img src="http://pfs.org.pl/rozne/wsc_dzien_scrabble.jpg">
</center>

<div>
<p><b>Turniej Scrabble, Slam Poetycki, konkursy z nagrodami, warsztaty z Polską Federacją Scrabble, a na koniec Mecz Gwiazd – te wszystkie atrakcje czekają na Was w niedzielę 16 października podczas Dnia Scrabble.</b>

<p>Na Placu Defilad w Warszawie, vis-a-vis Kinoteki, stanie ogrzewany namiot Scrabble, który będzie można odwiedzać już od godziny 10 rano.

<p>Dzień Scrabble® zaczynamy od konkursów i rodzinnych rozgrywek scrabblowych.
<p>W samo południe zapraszamy na <b>Wielki Finał Turniejów Scrabble</b>organizowanych przez sieć szkół językowych <b>empik school.</b> Ośmiu zawodników, którzy w przeprowadzonych 2 października w największych miastach w Polsce turniejach empik school uzyskali najwyższą liczbę punktów, zmierzy się na scenie i powalczy o wspaniałe nagrody.
<p>Kolejnym punktem programu jest <b>Slam Poetycki </b>z udziałem najlepszych „slamerów” w Polsce. Slam, który łączy w sobie poezję i performance, jest stosunkowo młodym zjawiskiem w naszym kraju, ale już zyskał sobie grono fanów. To bardzo żywiołowa konkurencja, która ma więcej wspólnego z raperskimi pojedynkami na słowa, niż  z recytowaniem wierszy.

<p>Ukoronowaniem Dnia Scrabble będzie <b>Mecz Gwiazd</b> – charytatywny pojedynek dwóch pięcioosobowych drużyn. Na czele każdej z nich staną znane osoby, a pomagać im będą zwycięzcy konkursów Scrabble przeprowadzonych wcześniej w portalu Onet.pl oraz w Radiu WAWA. Dochód z rozgrywki zasili konto Fundacji Spełnionych Marzeń.
<p>Całodzienną imprezę poprowadzą prezenterzy Radia WAWA – Monika Tarka i Robert Kilen. Radio WAWA zadba również o oprawę muzyczną wydarzenia. Przez cały czas w namiocie działać będzie strefa zabaw dla najmłodszych, w której profesjonalni animatorzy organizować będą zabawy i gry. Można będzie też pograć ze scrabblistami z Polskiej Federacji Scrabble, poznać tajniki gry turniejowej i dowiedzieć się, jak poprawić swoje umiejętności w Scrabble.

<b><p>Dzień Scrabble<br>
Niedziela, 16 października 2011, 10.00 - 17.00<br>
Plac Defilad, Warszawa (vis-a-vis Kinoteki)</b>


</div>








<?require_once "../files/php/bottom.php"?>
</body>
</html>

