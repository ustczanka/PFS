<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Eliminacje do francuskich Mistrzostw Świata</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
	p{
		margin: 15px 0;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Eliminacje do Mistrzostw Świata Scrabbli Frankofońskich</h1>

W dniu <b>27 marca 2010r.</b> - podczas <a href="http://www.pfs.org.pl/turniej.php?id=531">V Mistrzostw Doliny Karpia</a> w Graboszycach - odbędą się eliminacje do Mistrzostw Świata Scrabbli Frankofońskich. Zwycięzca będzie miał możliwość reprezentować Polskę podczas Mistrzostw Świata w Montpellier w dniach 14-22 sierpnia 2010r.<br><br>
Zainteresowanych prosimy o przysyłanie zgłoszeń do <b>20 marca 2010r.</b> na adres <a href="mailto:pfs@pfs.org.pl">pfs@pfs.org.pl</a>.


<?require_once "../files/php/bottom.php"?>
</body>
</html>

