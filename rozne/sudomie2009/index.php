<?
$turniej = "Wczasy scrabblowe - Sudomie 2009 - Wie?ci z wczas?w";
$data00  = "Ko?cowa klasyfikacja wczasowa";
$data01  = "Turniej rozpoznawczy - 27.06.2009 - sobota wiecz?r";
$data02  = "Wczasowicze na Turnieju o Puchar W?jta Gminy Ko?cierzyna  - 28.06.2009 - niedziela";
$data03  = "Si?demki - 28.06.2009 - niedziela wiecz?r";
$data04  = "Szarlotka - 29.06.2009 - poniedzia?ek wiecz?r";
$data05  = "Superscrabble - 30.06.2009 - wtorek wiecz?r";
$data06  = "Scrabblomarsz - 01.07.2009 - ?roda przedpo?udnie";
$data07  = "Prezes?wka - 01.07.2009 - ?roda wiecz?r";
$data08  = "Podw?jna belgijka - 02.07.2009 - czwartek popo?udnie";
$data09  = "Blitz - 02.07.2009 - czwartek popo?udnie";
$data10  = "Wariatka - 03.07.2009 - pi?tek przed- i popo?udniu";
$data11  = "Szwajcar - 04.07.2009 - sobota popo?udniu";
$data13  = "Liga wczasowa - 29.06-01.07.2009";
$name    = substr(__FILE__, 66);
?>

<html>
<head>
	<title><?print $turniej;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
	<meta name="Author" content="Grzegorz Wi?czkowski">
	<meta name="Author" content="Andrzej Lo?y?ski">
	<link rel="stylesheet" href="../../../files/css/style.css" type="text/css"  title="default"/>
	<link rel="alternate stylesheet" href="../../../files/css/stylelight.css" type="text/css" title="light"/>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/styleie.css" /><![endif]-->
	<script type="text/javascript" src="../../../files/js/styleswitcher.js"></script>
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../../../files/js/pngfix.js"></script><![endif]-->
	<style>
		.rundy{
			margin: 10px 0 20px 0;
		}
		.rundy td{
			padding: 3px 20px 3px 10px;
		}
		.rundy td:first-child{
			padding: 3px 0px;
			font-weight: bold;
			text-align:right;
		}
	</style>
</head>

<body>
<?include "../../../files/php/menu.php"?>
<?
print "<h1>".$turniej."</h1>";
?>

<?include "picture.php"?>

<div style="float:left;width: 100%;">

<?print "<h2>".$data00."</h2>";?>
<img align="right" src="sudomie10.gif" alt="kr?lowie wczas?w: Marek Reda i Beata Wi?czkowska">
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking    KW<b><sup>*</sup></b>
--- ---------- ----------- -------- --------  ---
<font color="yellow">  1 Marek      Reda        Wo?omin    133.07  162</font>
  2 Stanis?aw  Rydzik      Warszawa   145.74  160
<font color="yellow">  3 Beata      Wi?czkowska Wroc?aw    121.16  137</font>
  4 Tomasz     Zwoli?ski   Skierdy    142.39  134
  5 Grzegorz   Kurowski    Warszawa   128.36  112
  6 Zdzis?aw   Szota       Warszawa   114.07   77
  7 Adam       Janicki     ??d?       105.62   72
  8 Grzegorz   Wi?czkowski Wroc?aw    117.37   65
  9 Kuba       Koisar      Warszawa   125.55   64
 10 Piotr      Madej       Wroc?aw    113.41   56
 11 Katarzyna  Bogus?awska Piast?w    100.00   51
 12 Alicja     Bielecka    Pozna?     100.00   19
 13 Sylwia     Buks        Warszawa   100.00   17
 14 Andrzej    Lo?y?ski    Piast?w    110.44    7
 15 Aleksandra Brzeska     Warszawa   100.00    7
</pre>

<br/><p>Marek T. Reda zwyci?zc? klasyfikacji wczasowej!</p>
<b><sup>*</sup></b> KW = Klasyfikacja wczasowa.</p>

<?print "<h2>".$data01."</h2>";?>
<img align="right" src="sudomie10.jpg" alt="wczasowicze">
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Kuba       Koisar      Warszawa   125.55      4.0     1679  0
  2 Tomasz     Zwoli?ski   Skierdy    142.39      3.0     1818  0
  3 Grzegorz   Kurowski    Warszawa   128.36      3.0     1672  0
  4 Zdzis?aw   Szota       Warszawa   114.07      2.0     1545  0
  5 Marek      Reda        Wo?omin    133.07      2.0     1512  0
  6 Beata      Wi?czkowska Wroc?aw    121.16      2.0     1510  0
  7 Piotr      Madej       Wroc?aw    113.41      2.0     1445  0
  8 Alicja     Bielecka    Pozna?     100.00      1.0     1363  0
  9 Stanis?aw  Rydzik      Warszawa   145.74      1.0     1313  0
 10 Adam       Janicki     ??d?       105.62      0.0     1396  0
 11 Grzegorz   Wi?czkowski Wroc?aw    117.37      0.0      314  0
</pre>

<?print "<h2>".$data02."</h2>";?>
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  3 Beata      Wi?czkowska Wroc?aw    121.16      5.0     2807 43
  4 Tomasz     Zwoli?ski   Skierdy    142.39      5.0     2716 40
  8 Marek      Reda        Wo?omin    133.07      4.0     2760 32
  9 Kuba       Koisar      Warszawa   125.55      4.0     2649 30
 10 Grzegorz   Wi?czkowski Wroc?aw    117.37      4.0     2562 28
 11 Zdzis?aw   Szota       Warszawa   114.07      3.5     2645 26
 12 Adam       Janicki     ??d?       105.62      3.5     2400 24
 14 Stanis?aw  Rydzik      Warszawa   145.74      3.0     2606 20
 15 Piotr      Madej       Wroc?aw    113.41      3.0     2518 18
 16 Katarzyna  Bogus?awska Piast?w    100.00      3.0     2459 16
 19 Grzegorz   Kurowski    Warszawa   128.36      2.0     2625 10
 21 Sylwia     Buks        Warszawa   100.00      2.0     2337  6
</pre>

<?print "<h2>".$data03."</h2>";?>
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Marek      Reda        Wo?omin    133.07      4.0       22 13
  2 Beata      Wi?czkowska Wroc?aw    121.16      4.0       18 11
  3 Tomasz     Zwoli?ski   Skierdy    142.39      3.5       21  9
  4 Grzegorz   Kurowski    Warszawa   128.36      2.5       19  7
  5 Andrzej    Lo?y?ski    Piast?w    110.44      2.5       18  6
  6 Stanis?aw  Rydzik      Warszawa   145.74      2.0       19  5
  7 Katarzyna  Bogus?awska Piast?w    100.00      2.0       15  4
  8 Zdzis?aw   Szota       Warszawa   114.07      1.5       16  3
  9 Adam       Janicki     ??d?       105.62      1.5       15  2
 10 Sylwia     Buks        Warszawa   100.00      1.5       15  1
</pre>

<?print "<h2>".$data04."</h2>";?>
<img align="right" src="sudomie6.jpg" alt="gwiazda Marek Reda przy superscrabble">
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Tomasz     Zwoli?ski   Skierdy    142.39      4.0     1989 17
  2 Stanis?aw  Rydzik      Warszawa   145.74      3.0     1833 15
  3 Marek      Reda        Wo?omin    133.07      3.0     1827 13
  4 Grzegorz   Wi?czkowski Wroc?aw    117.37      3.0     1806 11
  5 Beata      Wi?czkowska Wroc?aw    121.16      3.0     1765 10
  6 Grzegorz   Kurowski    Warszawa   128.36      2.0     1657  9
  7 Piotr      Madej       Wroc?aw    113.41      2.0     1542  8
  8 Adam       Janicki     ??d?       105.62      2.0     1418  7
  9 Zdzis?aw   Szota       Warszawa   114.07      1.0     1463  6
 10 Kuba       Koisar      Warszawa   125.55      1.0     1283  5
 11 Sylwia     Buks        Warszawa   100.00      0.5     1239  4
 12 Alicja     Bielecka    Pozna?     100.00      0.0      940  3
 13 Aleksandra Brzeska     Warszawa   100.00      0.0      485  2
 14 Katarzyna  Bogus?awska Piast?w    100.00      0.0      322  1
</pre>

<br/><p>Gwiazd? poniedzia?kowego wieczoru zosta? niecwa?szy Marek T. Reda<b><sup>*</sup></b></p>
<p><b><sup>*</sup></b> T. czytaj jak ti, a b??d ortograficzny powy?ej jest jak najbardziej zamierzony.</p>

<?print "<h2>".$data05."</h2>";?>
<img align="right" src="sudomie7.jpg" alt="superscrabble w akcji">
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Beata      Wi?czkowska Wroc?aw    121.16      2.0     2183 13
  2 Grzegorz   Kurowski    Warszawa   128.36      2.0     1906 11
  3 Tomasz     Zwoli?ski   Skierdy    142.39      2.0     1901  9
  4 Stanis?aw  Rydzik      Warszawa   145.74      1.0     1791  7
  5 Marek      Reda        Wo?omin    133.07      1.0     1754  6
  6 Piotr      Madej       Wroc?aw    113.41      1.0     1656  5
  7 Grzegorz   Wi?czkowski Wroc?aw    117.37      1.0     1544  4
  8 Zdzis?aw   Szota       Warszawa   114.07      0.0     1709  3
  9 Adam       Janicki     ??d?       105.62      0.0     1507  2
 10 Alicja     Bielecka    Pozna?     100.00      0.0     1355  1
</pre>

<?print "<h2>".$data06."</h2>";?>
<img align="right" src="scrabblobieg6.jpg" alt="Beata i Tomek z Koszmarem">
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Punkty KW
--- ---------- ----------- -------- -------- ------ --
  1 Stanis?aw  Rydzik      Warszawa   145.74    368 11
  2 Marek      Reda        Wo?omin    133.07    314  9
  3 Grzegorz   Kurowski    Warszawa   128.36    222  7
  4 Beata      Wi?czkowska Wroc?aw    121.16    163  5
  5 Tomasz     Zwoli?ski   Skierdy    142.39    110  4
  6 Grzegorz   Wi?czkowski Wroc?aw    117.37     65  3
  7 Katarzyna  Bogus?awska Piast?w    100.00   -247  2
  8 Adam       Janicki     ??d?       105.62   -351  1
</pre>

<br/><p><a href="scrab.php">wi?cej o samym scrabblobiegu</a></p>

<?print "<h2>".$data07."</h2>";?>
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Stanis?aw  Rydzik      Warszawa   145.74      3.0     2889 17
  2 Grzegorz   Wi?czkowski Wroc?aw    117.37      3.0     2627 15
  3 Kuba       Koisar      Warszawa   125.55      2.0     2660 13
  4 Grzegorz   Kurowski    Warszawa   128.36      2.0     2636 11
  5 Marek      Reda        Wo?omin    133.07      2.0     2625 10
  6 Zdzis?aw   Szota       Warszawa   114.07      2.0     2485  9
  7 Tomasz     Zwoli?ski   Skierdy    142.39      2.0     2455  8
  8 Beata      Wi?czkowska Wroc?aw    121.16      1.0     2484  7
  9 Alicja     Bielecka    Pozna?     100.00      1.0     2030  6
 10 Adam       Janicki     ??d?       105.62      1.0     1993  5
 11 Sylwia     Buks        Warszawa   100.00      1.0     1722  4
 12 Katarzyna  Bogus?awska Piast?w    100.00      0.0     2022  3
 13 Piotr      Madej       Wroc?aw    113.41      0.0     1437  2
 14 Aleksandra Brzeska     Warszawa   100.00      0.0      972  1
</pre>

<?print "<h2>".$data08."</h2>";?>
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  I cz??? II cz??? razem KW
--- ---------- ----------- -------- -------- ------- -------- ----- --
  1 Marek      Reda        Wo?omin    133.07     451      341   792 11
  2 Stanis?aw  Rydzik      Warszawa   145.74     425      316   741  9
  3 Tomasz     Zwoli?ski   Skierdy    142.39     424      306   730  7
  4 Grzegorz   Kurowski    Warszawa   128.36     399      301   700  5
  5 Kuba       Koisar      Warszawa   125.55     402      297   699  4
  6 Grzegorz   Wi?czkowski Wroc?aw    117.37     362      301   663  3
  7 Zdzis?aw   Szota       Warszawa   114.07     381      251   632  2
  8 Aleksandra Brzeska     Warszawa   100.00     231      238   469  1
</pre>

<?print "<h2>".$data09."</h2>";?>
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Stanis?aw  Rydzik      Warszawa   145.74      3.0     1229 11
  2 Grzegorz   Kurowski    Warszawa   128.36      2.0     1105  9
  3 Marek      Reda        Wo?omin    133.07      2.0     1098  7
  4 Piotr      Madej       Wroc?aw    113.41      2.0      913  5
  5 Tomasz     Zwoli?ski   Skierdy    142.39      1.0     1156  4
  6 Zdzis?aw   Szota       Warszawa   114.07      1.0     1039  3
  7 Beata      Wi?czkowska Wroc?aw    121.16      1.0      979  2
  8 Andrzej    Lo?y?ski    Piast?w    110.44      0.0      735  1
</pre>

<?print "<h2>".$data10."</h2>";?>
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Marek      Reda        Wo?omin    133.07      6.0     3446 16
  2 Stanis?aw  Rydzik      Warszawa   145.74      5.0     4045 14
  3 Tomasz     Zwoli?ski   Skierdy    142.39      5.0     3929 12
  4 Grzegorz   Kurowski    Warszawa   128.36      4.0     3834 10
  5 Katarzyna  Bogus?awska Piast?w    100.00      3.0     2618  9
  6 Adam       Janicki     ??d?       105.62      3.0     2467  8
  7 Beata      Wi?czkowska Wroc?aw    121.16      2.0     2146  7
  8 Piotr      Madej       Wroc?aw    113.41      2.0     1769  6
  9 Alicja     Bielecka    Pozna?     100.00      1.0     2728  5
 10 Kuba       Koisar      Warszawa   125.55      1.0      551  4
 11 Aleksandra Brzeska     Warszawa   100.00      0.5      506  3
 12 Sylwia     Buks        Warszawa   100.00      0.0     1357  2
 13 Grzegorz   Wi?czkowski Wroc?aw    117.37      0.0      351  1
</pre>

<!--<?print "<h2>".$data11."</h2>";?>
<pre>
</pre>-->

<?print "<h2>".$data13."</h2>";?>
<pre>
Lp. Imi?       Nazwisko    Miasto   Ranking  Du?e pkt Ma?e pkt KW
--- ---------- ----------- -------- -------- -------- -------- --
  1 Stanis?aw  Rydzik      Warszawa   145.74      9.5     5018 51
  2 Marek      Reda        Wo?omin    133.07      7.5     4233 45
  3 Beata      Wi?czkowska Wroc?aw    121.16      7.5     4120 39
  4 Grzegorz   Kurowski    Warszawa   128.36      6.0     4332 33
  5 Adam       Janicki     ??d?       105.62      6.0     3817 28
  6 Tomasz     Zwoli?ski   Skierdy    142.39      5.5     4447 24
  7 Zdzis?aw   Szota       Warszawa   114.07      5.0     3747 20
  8 Katarzyna  Bogus?awska Piast?w    100.00      4.5     3658 16
  9 Piotr      Madej       Wroc?aw    113.41      3.5     3669 12
 10 Kuba       Koisar      Warszawa   125.55      3.5     3528  8
 11 Alicja     Bielecka    Pozna?     100.00      2.0     3286  4
</pre>

</div>

<?include "../../../files/php/bottom.php"?>
</body>
</html>