<?
$turniej = "Wczasy scrabblowe - Sudomie 2009 - Scrabblomarsz";
$name    = substr(__FILE__,50);
?>

<html>
<head>
	<title><?print $turniej;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
	<meta name="Author" content="Grzegorz Wi?czkowski">
	<meta name="Author" content="Andrzej Lo?y?ski">
	<link rel="stylesheet" href="../../../files/css/style.css" type="text/css"  title="default"/>
	<link rel="alternate stylesheet" href="../../../files/css/stylelight.css" type="text/css" title="light"/>
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/styleie.css" /><![endif]-->
	<script type="text/javascript" src="../../../files/js/styleswitcher.js"></script>
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../../../files/js/pngfix.js"></script><![endif]-->
</head>

<body>
<?include "../../../files/php/menu.php"?>
<?
print "<h1>".$turniej."</h1>";
?>

<?include "picture.php"?>

<div style="float:left;">

<h2>Regulamin Scrabblomarszu (Sudomie, 1 lipca 2009r)</h2>
<img align="right" src="scrabblobieg1.jpg" alt="Grzegorz z Koszmarem" style="padding-left: 10px;">Scrabblomarsz to konkurencja obejmuj?ca rozwi?zanie zada? scrabblowych oraz przebycie wytyczonej trasy w okre?lonym czasie. Ka?dy z uczestnik?w musi
odnale?? ukryte na drzewach zestawy siedmioliterowe (jest ich siedem), z ka?dego zestawu u?o?y? wyraz siedmioliterowy (z ka?dego zestawu jest tylko jeden
mo?liwy), a nast?pnie z ich <b>pierwszych liter wyraz siedmioliterowy</b> oraz z <b>ostatnich liter</b> kolejny <b>wyraz siedmioliterowy</b>.<br/>Nast?pnie
z tych <b>14 liter</b> trzeba znale?? jak najwi?cej <b>wyraz?w siedmioliterowych</b>.<br/>Wszystkie uk?adane wyrazy musz? znajdowa? si? w
OSPS-ie.<br/>Miejsca gdzie nale?y szuka? zestaw?w s? oznaczone pomara?czow? ta?m? i znajduj? si? w promieniu 10 metr?w od miejsca
oznaczenia.<br/>&nbsp;<br/>Pocz?tek - godz. 10.00<br/>Czas przej?cia - 120 min<br/>Trasa - oznaczona  bia?? ta?m?.<br/>&nbsp;<br/>Zawodnicy zostaj?
wypuszczani w odst?pach 3 minutowych wg. kolejno?ci ustalonej drog? wcze?niejszego losowania (najpierw startuje ostatni zawodnik na
li?cie).<br/>&nbsp;<br/><b>Punktacja</b><br/>Czas - 120 min<br/>Ka?da minuta poni?ej 120 - +1 pkt<br/>Ka?da minuta
powy?ej 120 - -1 pkt<br/>&nbsp;<br/><img align="right" src="scrabblobieg2.jpg" alt="Marek my?li nad rozwi?zaniami" style="padding-left: 10px;">Wyrazy:<br/>Ka?dy wyraz po?redni (z 7) -  10 pkt.<br/>Ka?dy wyraz docelowy (z 2) - 30 pkt.<br/>Ka?dy wyraz u?o?ony
z zestawu 14 liter - 2 pkt<br/>Ka?dy b??dny wyraz - -10 pkt<br/>&nbsp;<br/>Zwyci?zc? zostaje zdobywca najwi?kszej ilo?ci punkt?w.<br/>W przypadku r?wnej ilo?ci punkt?w decyduje kryterium wyrazowe (wi?ksza ilo?? u?o?onych wyraz?w)<br/>w przypadku nieroztrzygni?cia zwyci?zcy przy pomocy kryterium wyrazowego zawodnicy rozgrywaj? 5-minutow? parti? scrabble.


<h2>Relacja ze scrabblomarszu</h2>

<img align="right" src="scrabblobieg7.jpg" alt="Adam i Koszmar" style="padding-left: 10px;">Po przedzieraniu si? przez le?ne chaszcze i ost?py... myleniu drogi... nara?aniu si? na dotkliwe poparzenia... zawodnicy dotarli do mety przed czasem. Najlepszym "anagramatorem" okaza? si? by? Stanis?aw Rydzik, ktory z zestawu 14 liter u?o?y? az 114 "si?demek". Tym samym zbli?y? si? do prowadz?cych w klasyfikacji wczasowej o kolejne kilka punkt?w. Zawodnikom podczas pokonywania trasy towarzyszy? wierny i czujny pies Koszmar. Zapraszamy wszystkich do wsp?lnej zabawy w rozwi?zywanie tego zadania i podajemy zestawy siedmioliterowe jakie mieli uczestnicy scrabblobiegu:<br/>
<p style="color: yellow; font-weight:bold;">&nbsp;C&nbsp;H&nbsp;Y&nbsp;L&nbsp;?&nbsp;C&nbsp;C<br/>
&nbsp;S&nbsp;K&nbsp;O&nbsp;?&nbsp;O&nbsp;N&nbsp;E<br/>
&nbsp;?&nbsp;O&nbsp;N&nbsp;N&nbsp;A&nbsp;M&nbsp;I<br/>
&nbsp;A&nbsp;B&nbsp;A&nbsp;L&nbsp;E&nbsp;T&nbsp;U<br/>
&nbsp;O&nbsp;B&nbsp;E&nbsp;R&nbsp;A&nbsp;N&nbsp;A<br/>
&nbsp;S&nbsp;A&nbsp;?&nbsp;U&nbsp;T&nbsp;O&nbsp;M<br/>
&nbsp;S&nbsp;M&nbsp;Y&nbsp;R&nbsp;C&nbsp;Z&nbsp;Y<br/></p>
</div>

<?include "../../../files/php/bottom.php"?>
</body>
</html>