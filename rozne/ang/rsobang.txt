Wyniki poszczeg�lnych rund turnieju III Mistrzostwa Polski w Scrabble po angielsku



Runda nr  1
-----------
St� Gospodarz                             Go��                                Wynik
---- ----------------------------------- - ----------------------------------- ---------
   1 Mariusz        Skrobosz             - Leszek         Matuszak              513: 334
   2 Marek          Matuszak             - Wojciech       Usakiewicz            186: 496
   3 Kuba           Ku�aczkowski         - Micha�         Mruszczyk             367: 313
   4 Bartosz        Pi�ta                - Miko�aj        Szyma�ski             352: 344
   5 Rafa�          Dominiczak           - Micha�         Trumpus               415: 415
   6 Micha�         �ab�dzki             - BYE            BYE                   300: 250


Runda nr  2
-----------
St� Gospodarz                             Go��                                Wynik
---- ----------------------------------- - ----------------------------------- ---------
   1 Wojciech       Usakiewicz           - Mariusz        Skrobosz              454: 336
   2 Micha�         �ab�dzki             - Kuba           Ku�aczkowski          383: 413
   3 Bartosz        Pi�ta                - Rafa�          Dominiczak            417: 253
   4 Micha�         Trumpus              - Miko�aj        Szyma�ski             422: 392
   5 Micha�         Mruszczyk            - Leszek         Matuszak              313: 328
   6 Marek          Matuszak             - BYE            BYE                   300: 250


Runda nr  3
-----------
St� Gospodarz                             Go��                                Wynik
---- ----------------------------------- - ----------------------------------- ---------
   1 Wojciech       Usakiewicz           - Bartosz        Pi�ta                 371: 321
   2 Micha�         Trumpus              - Kuba           Ku�aczkowski          426: 419
   3 Mariusz        Skrobosz             - Micha�         �ab�dzki              304: 284
   4 Leszek         Matuszak             - Marek          Matuszak              372: 299
   5 Miko�aj        Szyma�ski            - Rafa�          Dominiczak            322: 532
   6 Micha�         Mruszczyk            - BYE            BYE                   300: 250


Runda nr  4
-----------
St� Gospodarz                             Go��                                Wynik
---- ----------------------------------- - ----------------------------------- ---------
   1 Micha�         Trumpus              - Wojciech       Usakiewicz            365: 395
   2 Bartosz        Pi�ta                - Mariusz        Skrobosz              338: 300
   3 Kuba           Ku�aczkowski         - Leszek         Matuszak              373: 376
   4 Rafa�          Dominiczak           - Micha�         �ab�dzki              390: 290
   5 Micha�         Mruszczyk            - Marek          Matuszak              434: 192
   6 Miko�aj        Szyma�ski            - BYE            BYE                   300: 250


Runda nr  5
-----------
St� Gospodarz                             Go��                                Wynik
---- ----------------------------------- - ----------------------------------- ---------
   1 Leszek         Matuszak             - Wojciech       Usakiewicz            308: 386
   2 Micha�         Trumpus              - Bartosz        Pi�ta                 458: 333
   3 Rafa�          Dominiczak           - Micha�         Mruszczyk             356: 338
   4 Miko�aj        Szyma�ski            - Kuba           Ku�aczkowski          362: 404
   5 Marek          Matuszak             - Micha�         �ab�dzki              235: 369
   6 Mariusz        Skrobosz             - BYE            BYE                   300: 250


Runda nr  6
-----------
St� Gospodarz                             Go��                                Wynik
---- ----------------------------------- - ----------------------------------- ---------
   1 Wojciech       Usakiewicz           - Rafa�          Dominiczak            431: 361
   2 Mariusz        Skrobosz             - Micha�         Trumpus               467: 285
   3 Kuba           Ku�aczkowski         - Bartosz        Pi�ta                 276: 466
   4 Micha�         �ab�dzki             - Micha�         Mruszczyk             428: 404
   5 Miko�aj        Szyma�ski            - Marek          Matuszak              362: 307
   6 Leszek         Matuszak             - BYE            BYE                   300: 250


Runda nr  7
-----------
St� Gospodarz                             Go��                                Wynik
---- ----------------------------------- - ----------------------------------- ---------
   1 Wojciech       Usakiewicz           - Micha�         �ab�dzki              373: 334
   2 Rafa�          Dominiczak           - Mariusz        Skrobosz              419: 368
   3 Leszek         Matuszak             - Bartosz        Pi�ta                 336: 415
   4 Marek          Matuszak             - Micha�         Trumpus               298: 406
   5 Micha�         Mruszczyk            - Miko�aj        Szyma�ski             288: 353
   6 Kuba           Ku�aczkowski         - BYE            BYE                   300: 250
