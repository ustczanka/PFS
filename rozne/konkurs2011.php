<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Słownik : Konkurs na organizację imprez PFS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Konkurs ofert na organizację imprez centralnych w 2011 roku</h1>

Zarząd Polskiej Federacji Scrabble ogłasza konkurs na organizację następujących imprez w 2011 roku:
<ul>
<li>XIX Mistrzostw Polski (faza eliminacyjna: 11(12)-13.XI., lub faza finałowa wraz z turniejem towarzyszącym: 26-27.XI.)</li>
<li>XVII Pucharu Polski</li>
<li>VI Klubowych Mistrzostw Polski (preferowany termin: 23-26.VI.)</li>
<li>XI Turnieju LeMans </li>
<li>Turnieju „Masters” (turniej dla zwycięzców/medalistów wszystkich turniejów rozegranych w 2011 roku + turniej towarzyszący) (preferowany termin grudzień)</li>
</li>
</ul>

Oferta powinna zawierać:
<ul>
<li>dane organizatora</li>
<li>nazwę turnieju</li>
<li>proponowany termin</li>
<li>opis miejsca rozgrywek</li>
<li>informacje o planowanej wysokości wpisowego i nagrodach</li>
<li>wstępne informacje dotyczące liczby i ceny noclegów i wyżywienia</li>
<li>wstępne informacje dotyczące oprawy medialnej i sponsorskiej turnieju</li>
<li>proponowaną wysokość dofinansowania ze strony PFS</li>
</ul>

Oferty można składać na adres poczty internetowej PFS <a href="mailto:pfs@pfs.org.pl">pfs@pfs.org.pl</a> <b>do 10 stycznia 2011 roku</b>.<br>
W wypadku zgłoszenia dwóch lub więcej ofert na ten sam turniej, ofertę wybierze w drodze konkursu Zarząd PFS.<br><br>

Regulaminy powyższych turniejów zostaną uchwalone przez Zarząd PFS po konsultacjach z wybranymi organizatorami.<br>
Sprzęt niezbędny do przeprowadzenia turniejów dostarczy Zarząd PFS.<br>
Sędziego na poszczególne imprezy wybierze i opłaci Zarząd PFS.<br><br>
Wybrany organizator zobowiązany będzie do przedstawienia Zarządowi PFS rozliczenia wykorzystania środków pozyskanych z wpisowego oraz z otrzymanego dofinansowania w terminie 30 dni od dnia zakończeniu turnieju.<br><br>
Zarząd PFS rozstrzygnie konkurs i ogłosi wybór organizatorów powyższych imprez do 25.01.2011.

<?require_once "../files/php/bottom.php"?>
</body>
</html>

