<? require "files/php/funkcje.php";

if ($_GET['id']) {
    $tour = pfs_select_one (array (
        table   => $DB_TABLES[tours],
        where   => array ( id => $_GET['id'], ),
        fields  => array ( 'id', 'nazwa', 'ilosc_rund', 'miasto', 'data_od', 'data_do' )
    ));
}
?>

<html>
<head>
    <title><?php print $tour ? $tour->nazwa . ' - na żywo' : 'Relacje LIVE';?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <link rel="shortcut icon" href="http://www.pfs.org.pl/files/img/favicon.ico" />
    <link rel="stylesheet" href="http://www.pfs.org.pl/files/css/style.css" type="text/css" />
    <link rel="stylesheet" href="http://www.pfs.org.pl/files/css/style-live.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="http://www.pfs.org.pl/files/css/styleie.css" /><![endif]-->
    <!--[if IE]><link rel="stylesheet" type="text/css" href="http://www.pfs.org.pl/files/css/style-liveie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="http://www.pfs.org.pl/files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="http://www.pfs.org.pl/files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="http://www.pfs.org.pl/files/js/jquery.js"></script>
    <!--<script type="text/javascript" src="http://www.pfs.org.pl/files/js/jquery-bp.js"></script>-->
    <script type="text/javascript" src="http://www.pfs.org.pl/files/js/java.js"></script>
    <script type="text/javascript" src="http://www.pfs.org.pl/files/js/live.js"></script>
    <script type="text/javascript">
    jSubmenu('glowna', 'live');
    </script>
	<script>
	$(document).ready(function(){
		$('a#aklaind').bind('click', function(event){
			$('div#divklaind').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('schowaj klasyfikację indywidualną'); }, function() { $(this).text("pokaż klasyfikację indywidualną"); });
		$('div#divklaind table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#akladru').bind('click', function(event){
			$('div#divkladru').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('pokaż klasyfikację drużynową'); }, function() { $(this).text("schowaj klasyfikację drużynową"); });
		$('div#divkladru table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#arunda1').bind('click', function(event){
			$('div#divrunda1').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('Runda I'); }, function() { $(this).text("Runda I"); });
		$('div#divrunda1 table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#arunda2').bind('click', function(event){
			$('div#divrunda2').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('Runda II'); }, function() { $(this).text("Runda II"); });
		$('div#divrunda2 table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#arunda3').bind('click', function(event){
			$('div#divrunda3').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('Runda III'); }, function() { $(this).text("Runda III"); });
		$('div#divrunda3 table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#arunda4').bind('click', function(event){
			$('div#divrunda4').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('Runda IV'); }, function() { $(this).text("Runda IV"); });
		$('div#divrunda4 table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#arunda5').bind('click', function(event){
			$('div#divrunda5').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('Runda V'); }, function() { $(this).text("Runda V"); });
		$('div#divrunda5 table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#arunda6').bind('click', function(event){
			$('div#divrunda6').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('Runda VI'); }, function() { $(this).text("Runda VI"); });
		$('div#divrunda6 table tr:last td').css({"border-bottom" : "none"});
	});
	$(document).ready(function(){
		$('a#arunda7').bind('click', function(event){
			$('div#divrunda7').toggle();
			event.preventDefault();
		}).toggle(function() { $(this).text('Runda VII'); }, function() { $(this).text("Runda VII"); });
		$('div#divrunda7 table tr:last td').css({"border-bottom" : "none"});
	});
	</script>
</head>

<body>

<?php
  include_once "files/php/menu.php";
?>
<!--<div class="box" style="margin-right:20px;"><a href="http://www.pfs.org.pl/czat.php" target="_blank">Czat</a></div>-->
<!--<div style="padding:5px;"><a href="#" class="przycisk" onClick="javascript:refreshLive();"><b>Odśwież</b></a></div>-->
<!--<div style="clear:both;"><a href="http://www.scrabble.info.pl/index.php?t=id_show&idgal=47&g1=1" target="_blank">GALERIA</a><br><br></div>-->
<!--<div id='buttonContainer'></div>-->

<div style="position:absolute;bottom:0px;padding: 15px 0;"></div>

<h1>Klubowe Mistrzostwa Polski w Scrabble - Ostróda 2011</h1>

<a href="http://www.pfs.org.pl/rozne/KMP_2011_reg.pdf" target=_blank>System rozgrywek (PDF)</a>
<br/><a href="kluby1.pdf" target=_blank>Lista uczestniczących drużyn (PDF)</a>
<br/><a href="rozstawienia.pdf" target=_blank>Harmonogram gier (PDF)</a>




<!--                    runda I   ---  poczatek                                -->
<br/><a href='' id='arunda1'>Runda I</a> - zakończona
<div id='divrunda1' style="display: none;">
<table>
<tr><td align="left">&nbsp;KBKS Paranoja&nbsp;</td><td align="left">&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;11</td><td align="center">&nbsp:&nbsp;</td><td align="left">5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda15A.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="left">&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;11</td><td align="center">&nbsp:&nbsp;</td><td align="left">5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda15B.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="left">&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;14</td><td align="center">&nbsp:&nbsp;</td><td align="left">2&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda15C.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="left">&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;5</td><td align="center">&nbsp:&nbsp;</td><td align="left">11&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda15D.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
</table>
</div>
<!--                    runda I   ---  koniec                                  -->






<!--                    runda II  ---  poczatek                                -->
<br/><a href='' id='arunda2'>Runda II</a> - zakończona
<div id='divrunda2' style="display: none;">
<table>
<tr><td align="left">&nbsp;KBKS Paranoja&nbsp;</td><td align="left">&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;11</td><td align="center">&nbsp:&nbsp;</td><td align="left">5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda25A.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="left">&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;12</td><td align="center">&nbsp:&nbsp;</td><td align="left">4&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda25B.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="left">&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;9</td><td align="center">&nbsp:&nbsp;</td><td align="left">7&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda25C.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="left">&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;10</td><td align="center">&nbsp:&nbsp;</td><td align="left">6&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda25D.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
</table>
</div>
<!--                    runda II  ---  koniec                                  -->






<!--                    runda III ---  poczatek                                -->
<br/><a href='' id='arunda3'>Runda III</a> - zakończona
<div id='divrunda3' style="display: none;">
<table>
<tr><td align="left">&nbsp;KBKS Paranoja&nbsp;</td><td align="left">&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;8</td><td align="center">&nbsp:&nbsp;</td><td align="left">8&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda35A.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="left">&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;8,5</td><td align="center">&nbsp:&nbsp;</td><td align="left">7,5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda35B.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="left">&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;10</td><td align="center">&nbsp:&nbsp;</td><td align="left">6&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda35C.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="left">&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;8</td><td align="center">&nbsp:&nbsp;</td><td align="left">8&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda35D.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
</table>
</div>
<!--                    runda III ---  koniec                                  -->






<!--                    runda IV  ---  poczatek                                -->
<br/><a href='' id='arunda4'>Runda IV</a> - zakończona
<div id='divrunda4' style="display: none;">
<table>
<tr><td align="left">&nbsp;KBKS Paranoja&nbsp;</td><td align="left">&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;11</td><td align="center">&nbsp:&nbsp;</td><td align="left">5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda45A.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="left">&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;7</td><td align="center">&nbsp:&nbsp;</td><td align="left">9&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda45B.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="left">&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;14</td><td align="center">&nbsp:&nbsp;</td><td align="left">2&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda45C.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="left">&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;8,5</td><td align="center">&nbsp:&nbsp;</td><td align="left">7,5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda45D.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
</table>
</div>
<!--                    runda IV  ---  koniec                                  -->






<!--                    runda V   ---  poczatek                                -->
<br/><a href='' id='arunda5'>Runda V</a> - zakończona
<div id='divrunda5' style="display: none;">
<table>
<tr><td align="left">&nbsp;KBKS Paranoja&nbsp;</td><td align="left">&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;9</td><td align="center">&nbsp:&nbsp;</td><td align="left">7&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda55A.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="left">&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;6</td><td align="center">&nbsp:&nbsp;</td><td align="left">10&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda55B.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="left">&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;9</td><td align="center">&nbsp:&nbsp;</td><td align="left">7&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda55C.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="left">&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;9,5</td><td align="center">&nbsp:&nbsp;</td><td align="left">6,5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda55D.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
</table>
</div>
<!--                    runda V   ---  koniec                                  -->






<!--                    runda VI  ---  poczatek                                -->
<br/><a href='' id='arunda6'>Runda VI</a> - zakończona
<div id='divrunda6' style="display: none;">
<table>
<tr><td align="left">&nbsp;KBKS Paranoja&nbsp;</td><td align="left">&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;7</td><td align="center">&nbsp:&nbsp;</td><td align="left">9&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda65A.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="left">&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;11,5</td><td align="center">&nbsp:&nbsp;</td><td align="left">4,5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda65B.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="left">&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;9</td><td align="center">&nbsp:&nbsp;</td><td align="left">7&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda65C.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="left">&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;6</td><td align="center">&nbsp:&nbsp;</td><td align="left">10&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda65D.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
</table>
</div>
<!--                    runda VI  ---  koniec                                  -->






<!--                    runda VII ---  poczatek                                -->
<br/><a href='' id='arunda7'>Runda VII</a> - zakończona
<div id='divrunda7' style="display: none;">
<table>
<tr><td align="left">&nbsp;KBKS Paranoja&nbsp;</td><td align="left">&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="right">&nbsp;12</td><td align="center">&nbsp:&nbsp;</td><td align="left">4&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda75A.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="left">&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;8,5</td><td align="center">&nbsp:&nbsp;</td><td align="left">7,5&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda75B.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="left">&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;12</td><td align="center">&nbsp:&nbsp;</td><td align="left">4&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda75C.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
<tr><td align="left">&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="left">&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;9</td><td align="center">&nbsp:&nbsp;</td><td align="left">7&nbsp;</td><td align="center">&nbsp;&nbsp;&nbsp;&nbsp;<a href="runda75D.pdf" target="_blank">protokół (PDF)</a>&nbsp;</td><td>&nbsp;zakończona&nbsp;</td></tr>
</table>
</div>
<!--                    runda VII ---  koniec                                  -->






<!--                    klasyfikacja drużynowa --- poczatek                    -->
<br/><br/><a href='' id='akladru'>schowaj klasyfikację drużynową</a>
<div id='divkladru' style="display: block;">
<table><tr><td colspan="7">Końcowa klasyfikacja drużynowa</td></tr><tr><td align="center">&nbsp;m.&nbsp;</td><td align="center">&nbsp;klub&nbsp;</td><td align="center" colspan="3">&nbsp;pkt. mecz.&nbsp;</td><td align="center" colspan="3">&nbsp;pkt. duże&nbsp;</td><td align="center" colspan="3">&nbsp;pkt. małe&nbsp;</td></tr>
<tr><td align="right">&nbsp;1.&nbsp;</td><td>&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;14</td><td align="center">&nbsp;-&nbsp;</td><td align="left">0&nbsp;</td><td align="right">&nbsp;74,5</td><td align="center">&nbsp;-&nbsp;</td><td align="left">37,5&nbsp;</td><td align="right">&nbsp;45213</td><td align="center">&nbsp;-&nbsp;</td><td align="left">41829&nbsp;</td></tr>
<tr><td align="right">&nbsp;2.&nbsp;</td><td>&nbsp;KBKS Paranoja&nbsp;</td><td align="right">&nbsp;11</td><td align="center">&nbsp;-&nbsp;</td><td align="left">3&nbsp;</td><td align="right">&nbsp;69</td><td align="center">&nbsp;-&nbsp;</td><td align="left">43&nbsp;</td><td align="right">&nbsp;44761</td><td align="center">&nbsp;-&nbsp;</td><td align="left">41630&nbsp;</td></tr>
<tr><td align="right">&nbsp;3.&nbsp;</td><td>&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;8</td><td align="center">&nbsp;-&nbsp;</td><td align="left">6&nbsp;</td><td align="right">&nbsp;60,5</td><td align="center">&nbsp;-&nbsp;</td><td align="left">51,5&nbsp;</td><td align="right">&nbsp;43571</td><td align="center">&nbsp;-&nbsp;</td><td align="left">43402&nbsp;</td></tr>
<tr><td align="right">&nbsp;4.&nbsp;</td><td>&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="right">&nbsp;8</td><td align="center">&nbsp;-&nbsp;</td><td align="left">6&nbsp;</td><td align="right">&nbsp;60</td><td align="center">&nbsp;-&nbsp;</td><td align="left">52&nbsp;</td><td align="right">&nbsp;43851</td><td align="center">&nbsp;-&nbsp;</td><td align="left">42591&nbsp;</td></tr>
<tr><td align="right">&nbsp;5.&nbsp;</td><td>&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;5</td><td align="center">&nbsp;-&nbsp;</td><td align="left">9&nbsp;</td><td align="right">&nbsp;50,5</td><td align="center">&nbsp;-&nbsp;</td><td align="left">61,5&nbsp;</td><td align="right">&nbsp;42767</td><td align="center">&nbsp;-&nbsp;</td><td align="left">42708&nbsp;</td></tr>
<tr><td align="right">&nbsp;6.&nbsp;</td><td>&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;5</td><td align="center">&nbsp;-&nbsp;</td><td align="left">9&nbsp;</td><td align="right">&nbsp;46</td><td align="center">&nbsp;-&nbsp;</td><td align="left">66&nbsp;</td><td align="right">&nbsp;41097</td><td align="center">&nbsp;-&nbsp;</td><td align="left">43403&nbsp;</td></tr>
<tr><td align="right">&nbsp;7.&nbsp;</td><td>&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;5</td><td align="center">&nbsp;-&nbsp;</td><td align="left">9&nbsp;</td><td align="right">&nbsp;44,5</td><td align="center">&nbsp;-&nbsp;</td><td align="left">67,5&nbsp;</td><td align="right">&nbsp;41414</td><td align="center">&nbsp;-&nbsp;</td><td align="left">43653&nbsp;</td></tr>
<tr><td align="right">&nbsp;8.&nbsp;</td><td>&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;0</td><td align="center">&nbsp;-&nbsp;</td><td align="left">14&nbsp;</td><td align="right">&nbsp;43</td><td align="center">&nbsp;-&nbsp;</td><td align="left">69&nbsp;</td><td align="right">&nbsp;40959</td><td align="center">&nbsp;-&nbsp;</td><td align="left">44417&nbsp;</td></tr>
</table>
</div>
<!--                    klasyfikacja drużynowa --- koniec                      -->




<!--                 klasyfikacja indywidualna --- poczatek                    -->
<br/><a href='' id='aklaind'>pokaż klasyfikację indywidualną</a>
<div id='divklaind' style="display: block;">
<table><tr><td colspan="7">Końcowa klasyfikacja indywidualna</td></tr><tr><td align="center">&nbsp;m.&nbsp;</td><td align="center">&nbsp;gracz&nbsp;</td><td align="center">&nbsp;klub&nbsp;</td><td align="center" colspan="2">&nbsp;pkt.&nbsp;</td><td align="center">&nbsp;proc.&nbsp;</td><td align="center" colspan="3">&nbsp;pkt. małe&nbsp;</td><td align="center" colspan="3">&nbsp;pkt. małe śr.&nbsp;</td></tr>
<tr><td align="right">&nbsp;1.&nbsp;</td><td>&nbsp;Kazimierz Merklejn&nbsp;</td><td>&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;21&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;75 %&nbsp;</td><td align="right">&nbsp;11647</td><td>&nbsp;-&nbsp;</td><td align="left">10674&nbsp;</td><td align="right">&nbsp;416,0</td><td>&nbsp;-&nbsp;</td><td align="left">381,2&nbsp;</td></tr>
<tr><td align="right">&nbsp;2.&nbsp;</td><td>&nbsp;Miłosz Wrzałek&nbsp;</td><td>&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;17&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;61 %&nbsp;</td><td align="right">&nbsp;10894</td><td>&nbsp;-&nbsp;</td><td align="left">10831&nbsp;</td><td align="right">&nbsp;389,1</td><td>&nbsp;-&nbsp;</td><td align="left">386,8&nbsp;</td></tr>
<tr><td align="right">&nbsp;3.&nbsp;</td><td>&nbsp;Mariusz Skrobosz&nbsp;</td><td>&nbsp;KBKS Paranoja&nbsp;</td><td align="right">&nbsp;17&nbsp;</td><td align="left">(z 26)</td><td align="right">&nbsp;65 %&nbsp;</td><td align="right">&nbsp;10333</td><td>&nbsp;-&nbsp;</td><td align="left">9391&nbsp;</td><td align="right">&nbsp;397,4</td><td>&nbsp;-&nbsp;</td><td align="left">361,2&nbsp;</td></tr>
<tr><td align="right">&nbsp;4.&nbsp;</td><td>&nbsp;Krzysztof Obremski&nbsp;</td><td>&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;17&nbsp;</td><td align="left">(z 23)</td><td align="right">&nbsp;74 %&nbsp;</td><td align="right">&nbsp;9453</td><td>&nbsp;-&nbsp;</td><td align="left">8432&nbsp;</td><td align="right">&nbsp;411,0</td><td>&nbsp;-&nbsp;</td><td align="left">366,6&nbsp;</td></tr>
<tr><td align="right">&nbsp;5.&nbsp;</td><td>&nbsp;Stanisław Rydzik&nbsp;</td><td>&nbsp;KBKS Paranoja&nbsp;</td><td align="right">&nbsp;16&nbsp;</td><td align="left">(z 23)</td><td align="right">&nbsp;70 %&nbsp;</td><td align="right">&nbsp;9194</td><td>&nbsp;-&nbsp;</td><td align="left">8445&nbsp;</td><td align="right">&nbsp;399,7</td><td>&nbsp;-&nbsp;</td><td align="left">367,2&nbsp;</td></tr>
<tr><td align="right">&nbsp;6.&nbsp;</td><td>&nbsp;Bogusław Szyszka&nbsp;</td><td>&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;16&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;73 %&nbsp;</td><td align="right">&nbsp;8868</td><td>&nbsp;-&nbsp;</td><td align="left">8376&nbsp;</td><td align="right">&nbsp;403,1</td><td>&nbsp;-&nbsp;</td><td align="left">380,7&nbsp;</td></tr>
<tr><td align="right">&nbsp;7.&nbsp;</td><td>&nbsp;Irena Sołdan&nbsp;</td><td>&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;15,5&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;55 %&nbsp;</td><td align="right">&nbsp;11211</td><td>&nbsp;-&nbsp;</td><td align="left">10840&nbsp;</td><td align="right">&nbsp;400,4</td><td>&nbsp;-&nbsp;</td><td align="left">387,1&nbsp;</td></tr>
<tr><td align="right">&nbsp;8.&nbsp;</td><td>&nbsp;Adam Szukalski&nbsp;</td><td>&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;15,5&nbsp;</td><td align="left">(z 24)</td><td align="right">&nbsp;65 %&nbsp;</td><td align="right">&nbsp;9734</td><td>&nbsp;-&nbsp;</td><td align="left">9060&nbsp;</td><td align="right">&nbsp;405,6</td><td>&nbsp;-&nbsp;</td><td align="left">377,5&nbsp;</td></tr>
<tr><td align="right">&nbsp;9.&nbsp;</td><td>&nbsp;Grzegorz Koczkodon&nbsp;</td><td>&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;15&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;54 %&nbsp;</td><td align="right">&nbsp;10937</td><td>&nbsp;-&nbsp;</td><td align="left">10596&nbsp;</td><td align="right">&nbsp;390,6</td><td>&nbsp;-&nbsp;</td><td align="left">378,4&nbsp;</td></tr>
<tr><td align="right">&nbsp;10.&nbsp;</td><td>&nbsp;Bartosz Morawski&nbsp;</td><td>&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;15&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;68 %&nbsp;</td><td align="right">&nbsp;9046</td><td>&nbsp;-&nbsp;</td><td align="left">8104&nbsp;</td><td align="right">&nbsp;411,2</td><td>&nbsp;-&nbsp;</td><td align="left">368,4&nbsp;</td></tr>
<tr><td align="right">&nbsp;11.&nbsp;</td><td>&nbsp;Kamil Górka&nbsp;</td><td>&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="right">&nbsp;15&nbsp;</td><td align="left">(z 23)</td><td align="right">&nbsp;65 %&nbsp;</td><td align="right">&nbsp;9032</td><td>&nbsp;-&nbsp;</td><td align="left">8671&nbsp;</td><td align="right">&nbsp;392,7</td><td>&nbsp;-&nbsp;</td><td align="left">377,0&nbsp;</td></tr>
<tr><td align="right">&nbsp;12.&nbsp;</td><td>&nbsp;Grzegorz Kurowski&nbsp;</td><td>&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;14&nbsp;</td><td align="left">(z 25)</td><td align="right">&nbsp;56 %&nbsp;</td><td align="right">&nbsp;9685</td><td>&nbsp;-&nbsp;</td><td align="left">9216&nbsp;</td><td align="right">&nbsp;387,4</td><td>&nbsp;-&nbsp;</td><td align="left">368,6&nbsp;</td></tr>
<tr><td align="right">&nbsp;13.&nbsp;</td><td>&nbsp;Mirosław Uglik&nbsp;</td><td>&nbsp;KBKS Paranoja&nbsp;</td><td align="right">&nbsp;14&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;64 %&nbsp;</td><td align="right">&nbsp;8692</td><td>&nbsp;-&nbsp;</td><td align="left">8068&nbsp;</td><td align="right">&nbsp;395,1</td><td>&nbsp;-&nbsp;</td><td align="left">366,7&nbsp;</td></tr>
<tr><td align="right">&nbsp;14.&nbsp;</td><td>&nbsp;Andrzej Rechowicz&nbsp;</td><td>&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;13,5&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;48 %&nbsp;</td><td align="right">&nbsp;10455</td><td>&nbsp;-&nbsp;</td><td align="left">11224&nbsp;</td><td align="right">&nbsp;373,4</td><td>&nbsp;-&nbsp;</td><td align="left">400,9&nbsp;</td></tr>
<tr><td align="right">&nbsp;15.&nbsp;</td><td>&nbsp;Jan Mrozowski&nbsp;</td><td>&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="right">&nbsp;13&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;59 %&nbsp;</td><td align="right">&nbsp;8547</td><td>&nbsp;-&nbsp;</td><td align="left">8048&nbsp;</td><td align="right">&nbsp;388,5</td><td>&nbsp;-&nbsp;</td><td align="left">365,8&nbsp;</td></tr>
<tr><td align="right">&nbsp;16.&nbsp;</td><td>&nbsp;Weronika Rudnicka&nbsp;</td><td>&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;12,5&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;45 %&nbsp;</td><td align="right">&nbsp;10376</td><td>&nbsp;-&nbsp;</td><td align="left">10795&nbsp;</td><td align="right">&nbsp;370,6</td><td>&nbsp;-&nbsp;</td><td align="left">385,5&nbsp;</td></tr>
<tr><td align="right">&nbsp;17.&nbsp;</td><td>&nbsp;Andrzej Kroc&nbsp;</td><td>&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;12,5&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;45 %&nbsp;</td><td align="right">&nbsp;10147</td><td>&nbsp;-&nbsp;</td><td align="left">11082&nbsp;</td><td align="right">&nbsp;362,4</td><td>&nbsp;-&nbsp;</td><td align="left">395,8&nbsp;</td></tr>
<tr><td align="right">&nbsp;18.&nbsp;</td><td>&nbsp;Robert Głowacki&nbsp;</td><td>&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;12&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;43 %&nbsp;</td><td align="right">&nbsp;10650</td><td>&nbsp;-&nbsp;</td><td align="left">10747&nbsp;</td><td align="right">&nbsp;380,4</td><td>&nbsp;-&nbsp;</td><td align="left">383,8&nbsp;</td></tr>
<tr><td align="right">&nbsp;19.&nbsp;</td><td>&nbsp;Mariusz Wrześniewski&nbsp;</td><td>&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="right">&nbsp;12&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;55 %&nbsp;</td><td align="right">&nbsp;8859</td><td>&nbsp;-&nbsp;</td><td align="left">8396&nbsp;</td><td align="right">&nbsp;402,7</td><td>&nbsp;-&nbsp;</td><td align="left">381,6&nbsp;</td></tr>
<tr><td align="right">&nbsp;20.&nbsp;</td><td>&nbsp;Michał Alabrudziński&nbsp;</td><td>&nbsp;KBKS Paranoja&nbsp;</td><td align="right">&nbsp;12&nbsp;</td><td align="left">(z 21)</td><td align="right">&nbsp;57 %&nbsp;</td><td align="right">&nbsp;8637</td><td>&nbsp;-&nbsp;</td><td align="left">7826&nbsp;</td><td align="right">&nbsp;411,3</td><td>&nbsp;-&nbsp;</td><td align="left">372,7&nbsp;</td></tr>
<tr><td align="right">&nbsp;21.&nbsp;</td><td>&nbsp;Szymon Płachta&nbsp;</td><td>&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;11,5&nbsp;</td><td align="left">(z 23)</td><td align="right">&nbsp;50 %&nbsp;</td><td align="right">&nbsp;9022</td><td>&nbsp;-&nbsp;</td><td align="left">8695&nbsp;</td><td align="right">&nbsp;392,3</td><td>&nbsp;-&nbsp;</td><td align="left">378,0&nbsp;</td></tr>
<tr><td align="right">&nbsp;22.&nbsp;</td><td>&nbsp;Justyna Górka&nbsp;</td><td>&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="right">&nbsp;11,5&nbsp;</td><td align="left">(z 23)</td><td align="right">&nbsp;50 %&nbsp;</td><td align="right">&nbsp;8981</td><td>&nbsp;-&nbsp;</td><td align="left">8946&nbsp;</td><td align="right">&nbsp;390,5</td><td>&nbsp;-&nbsp;</td><td align="left">389,0&nbsp;</td></tr>
<tr><td align="right">&nbsp;23.&nbsp;</td><td>&nbsp;Krzysztof Langiewicz&nbsp;</td><td>&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;11&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;39 %&nbsp;</td><td align="right">&nbsp;10576</td><td>&nbsp;-&nbsp;</td><td align="left">10748&nbsp;</td><td align="right">&nbsp;377,7</td><td>&nbsp;-&nbsp;</td><td align="left">383,9&nbsp;</td></tr>
<tr><td align="right">&nbsp;24.&nbsp;</td><td>&nbsp;Magdalena Kupczyńska&nbsp;</td><td>&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;11&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;39 %&nbsp;</td><td align="right">&nbsp;10268</td><td>&nbsp;-&nbsp;</td><td align="left">11055&nbsp;</td><td align="right">&nbsp;366,7</td><td>&nbsp;-&nbsp;</td><td align="left">394,8&nbsp;</td></tr>
<tr><td align="right">&nbsp;25.&nbsp;</td><td>&nbsp;Dorota Kuć&nbsp;</td><td>&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;11&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;39 %&nbsp;</td><td align="right">&nbsp;10257</td><td>&nbsp;-&nbsp;</td><td align="left">11164&nbsp;</td><td align="right">&nbsp;366,3</td><td>&nbsp;-&nbsp;</td><td align="left">398,7&nbsp;</td></tr>
<tr><td align="right">&nbsp;26.&nbsp;</td><td>&nbsp;Michał Kolaj&nbsp;</td><td>&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;11&nbsp;</td><td align="left">(z 24)</td><td align="right">&nbsp;46 %&nbsp;</td><td align="right">&nbsp;9194</td><td>&nbsp;-&nbsp;</td><td align="left">9253&nbsp;</td><td align="right">&nbsp;383,1</td><td>&nbsp;-&nbsp;</td><td align="left">385,5&nbsp;</td></tr>
<tr><td align="right">&nbsp;27.&nbsp;</td><td>&nbsp;Karol Waniek&nbsp;</td><td>&nbsp;Wrocławski Klub Scrabble "Siódemka"&nbsp;</td><td align="right">&nbsp;11&nbsp;</td><td align="left">(z 21)</td><td align="right">&nbsp;52 %&nbsp;</td><td align="right">&nbsp;8112</td><td>&nbsp;-&nbsp;</td><td align="left">7857&nbsp;</td><td align="right">&nbsp;386,3</td><td>&nbsp;-&nbsp;</td><td align="left">374,1&nbsp;</td></tr>
<tr><td align="right">&nbsp;28.&nbsp;</td><td>&nbsp;Dariusz Kuć&nbsp;</td><td>&nbsp;Szczeciński Klub Scrabblowy "BLANK"&nbsp;</td><td align="right">&nbsp;10&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;36 %&nbsp;</td><td align="right">&nbsp;10513</td><td>&nbsp;-&nbsp;</td><td align="left">10639&nbsp;</td><td align="right">&nbsp;375,5</td><td>&nbsp;-&nbsp;</td><td align="left">380,0&nbsp;</td></tr>
<tr><td align="right">&nbsp;29.&nbsp;</td><td>&nbsp;Kamil Kister&nbsp;</td><td>&nbsp;KBKS Paranoja&nbsp;</td><td align="right">&nbsp;10&nbsp;</td><td align="left">(z 20)</td><td align="right">&nbsp;50 %&nbsp;</td><td align="right">&nbsp;7905</td><td>&nbsp;-&nbsp;</td><td align="left">7900&nbsp;</td><td align="right">&nbsp;395,3</td><td>&nbsp;-&nbsp;</td><td align="left">395,0&nbsp;</td></tr>
<tr><td align="right">&nbsp;30.&nbsp;</td><td>&nbsp;Robert Kamiński&nbsp;</td><td>&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;9,5&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;43 %&nbsp;</td><td align="right">&nbsp;8598</td><td>&nbsp;-&nbsp;</td><td align="left">8872&nbsp;</td><td align="right">&nbsp;390,8</td><td>&nbsp;-&nbsp;</td><td align="left">403,3&nbsp;</td></tr>
<tr><td align="right">&nbsp;31.&nbsp;</td><td>&nbsp;Paweł Jackowski&nbsp;</td><td>&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;9,5&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;43 %&nbsp;</td><td align="right">&nbsp;8277</td><td>&nbsp;-&nbsp;</td><td align="left">8062&nbsp;</td><td align="right">&nbsp;376,2</td><td>&nbsp;-&nbsp;</td><td align="left">366,5&nbsp;</td></tr>
<tr><td align="right">&nbsp;32.&nbsp;</td><td>&nbsp;Małgorzata Sulejewska&nbsp;</td><td>&nbsp;Krakowski Klub Scrabble "Ghost"&nbsp;</td><td align="right">&nbsp;8,5&nbsp;</td><td align="left">(z 22)</td><td align="right">&nbsp;39 %&nbsp;</td><td align="right">&nbsp;8432</td><td>&nbsp;-&nbsp;</td><td align="left">8530&nbsp;</td><td align="right">&nbsp;383,3</td><td>&nbsp;-&nbsp;</td><td align="left">387,7&nbsp;</td></tr>
<tr><td align="right">&nbsp;33.&nbsp;</td><td>&nbsp;Sławomir Długosz&nbsp;</td><td>&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;8&nbsp;</td><td align="left">(z 18)</td><td align="right">&nbsp;44 %&nbsp;</td><td align="right">&nbsp;6112</td><td>&nbsp;-&nbsp;</td><td align="left">6884&nbsp;</td><td align="right">&nbsp;339,6</td><td>&nbsp;-&nbsp;</td><td align="left">382,4&nbsp;</td></tr>
<tr><td align="right">&nbsp;34.&nbsp;</td><td>&nbsp;Grzegorz Łukasik&nbsp;</td><td>&nbsp;Rumski Klub Scrabble "Mikrus"&nbsp;</td><td align="right">&nbsp;7&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;25 %&nbsp;</td><td align="right">&nbsp;9819</td><td>&nbsp;-&nbsp;</td><td align="left">11057&nbsp;</td><td align="right">&nbsp;350,7</td><td>&nbsp;-&nbsp;</td><td align="left">394,9&nbsp;</td></tr>
<tr><td align="right">&nbsp;35.&nbsp;</td><td>&nbsp;Dariusz Maciejuk&nbsp;</td><td>&nbsp;Warszawski Klub Scrabblowy "Macaki"&nbsp;</td><td align="right">&nbsp;6&nbsp;</td><td align="left">(z 20)</td><td align="right">&nbsp;30 %&nbsp;</td><td align="right">&nbsp;7185</td><td>&nbsp;-&nbsp;</td><td align="left">7863&nbsp;</td><td align="right">&nbsp;359,3</td><td>&nbsp;-&nbsp;</td><td align="left">393,2&nbsp;</td></tr>
<tr><td align="right">&nbsp;36.&nbsp;</td><td>&nbsp;Urszula Solarska&nbsp;</td><td>&nbsp;Łódzki Klub Miłośników Scrabble&nbsp;</td><td align="right">&nbsp;5&nbsp;</td><td align="left">(z 28)</td><td align="right">&nbsp;18 %&nbsp;</td><td align="right">&nbsp;9707</td><td>&nbsp;-&nbsp;</td><td align="left">11364&nbsp;</td><td align="right">&nbsp;346,7</td><td>&nbsp;-&nbsp;</td><td align="left">405,9&nbsp;</td></tr>
<tr><td align="right">&nbsp;37.&nbsp;</td><td>&nbsp;Danuta Langiewicz&nbsp;</td><td>&nbsp;OSPS "Anagram" Ostróda&nbsp;</td><td align="right">&nbsp;1&nbsp;</td><td align="left">(z 14)</td><td align="right">&nbsp;7 %&nbsp;</td><td align="right">&nbsp;4278</td><td>&nbsp;-&nbsp;</td><td align="left">5922&nbsp;</td><td align="right">&nbsp;305,6</td><td>&nbsp;-&nbsp;</td><td align="left">423,0&nbsp;</td></tr>
</table>
</div>
<!--                 klasyfikacja indywidualna --- koniec                      -->




<?php
  include_once "files/php/bottom.php";
?>

</body>
</html>