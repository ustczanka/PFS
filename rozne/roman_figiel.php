<?include_once "../files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Roman Figiel</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="../files/img/favicon.ico" />
	<link rel="stylesheet" href="../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../files/js/jquery.js"></script>
	<script type="text/javascript" src="../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../files/js/java.js"></script>
	<script>jSubmenu("glowna","aktualnosci");</script>
  <style type="text/css">
	ul{
		margin: 10px 15px 20px 15px;
	}
  </style>
</head>

<body>
<?require_once "../files/php/menu.php"?>
<h1>Zmarł Roman Figiel</h1>

<center><img src="../rozne/roman_figiel.jpg"></center><br> <br>
Z głębokim żalem zawiadamiamy, że odszedł nasz długoletni kolega, weteran turniejów w Scrabble - Roman Figiel.<br> <br>
Zawsze będziemy pamiętać jego pogodny nastrój i uśmiech na twarzy. <br><br>

Nabożeństwo żałobne zostanie odprawione w kościele pod wezwaniem NMP w Zawichoście w dniu 3 grudnia, o godzinie 13:00.<br>
Pochowanie urny z prochami na Cmentarzu Bożego Miłosierdzia w Zawichoście nastąpi w dniu 4 grudnia o godzinie 12:15.

<?require_once "../files/php/bottom.php"?>
</body>
</html>

