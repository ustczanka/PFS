<?
$turniej = "XVII Mistrzostwa Polski - harmonogram gier";

$format = "php";
?>

<html>
<head>
	<title><?print $turniej;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
	<meta http-equiv="Pragma" content="no-cache">
	<link rel="shortcut icon" href="../../../files/img/favicon.ico" />
	<link rel="stylesheet" href="../../../files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../../../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../../../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../../../files/js/jquery.js"></script>
	<script type="text/javascript" src="../../../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../../../files/js/java.js"></script>
	<script>jSubmenu("turnieje","archiwum");</script>
	<style>
	table{
		line-height: 140%;
	}
	table td{
		padding: 10px 20px;
	}
	</style>
</head>

<body>
<?include "../../../files/php/menu.php";?>
<h1><script>naglowek("rozk�ad rund")</script></h1>

<table>
<tr><td>
<B>Runda 1</B><br><br>

Pawe� Jana� - �ukasz Tuszy�ski<br>
Bartosz Morawski - Janusz Kaczor<br>
�ukasz Bobowski - Micha� Makowski<br>
Marcin Mroziuk - Kamil G�rka<br>
Rafa� Lenartowski - Marek Reda<br>
Dawid Pikul - Andrzej Kwiatkowski<br>
Dariusz Puton - Dariusz Kosz<br>
Mateusz �bikowski - Rafa� D�browski<br><br>
</td>
<td>
<B>Runda 2</B><br><br>

�ukasz Tuszy�ski - Janusz Kaczor<br>
Bartosz Morawski - Pawe� Jana�<br>
Micha� Makowski - Kamil G�rka<br>
Marcin Mroziuk - �ukasz Bobowski<br>
Marek Reda - Andrzej Kwiatkowski<br>
Dawid Pikul - Rafa� Lenartowski<br>
Dariusz Kosz - Rafa� D�browski<br>
Mateusz �bikowski - Dariusz Puton<br><br>
</td>
<td>
<B>Runda 3</B><br><br>

�ukasz Bobowski - �ukasz Tuszy�ski<br>
Kamil G�rka - Bartosz Morawski<br>
Pawe� Jana� - Micha� Makowski<br>
Janusz Kaczor - Marcin Mroziuk<br>
Dariusz Puton - Marek Reda<br>
Rafa� D�browski - Dawid Pikul<br>
Rafa� Lenartowski - Dariusz Kosz<br>
Andrzej Kwiatkowski - Mateusz �bikowski<br><br>
</td></tr>
<tr><td>
<B>Runda 4</B><br><br>

�ukasz Tuszy�ski - Kamil G�rka<br>
Bartosz Morawski - �ukasz Bobowski<br>
Micha� Makowski - Janusz Kaczor<br>
Marcin Mroziuk - Pawe� Jana�<br>
Marek Reda - Rafa� D�browski<br>
Dawid Pikul - Dariusz Puton<br>
Dariusz Kosz - Andrzej Kwiatkowski<br>
Mateusz �bikowski - Rafa� Lenartowski<br><br>
</td>
<td>
<B>Runda 5</B><br><br>

Rafa� Lenartowski - �ukasz Tuszy�ski<br>
Andrzej Kwiatkowski - Bartosz Morawski<br>
Dariusz Puton - Micha� Makowski<br>
Rafa� D�browski - Marcin Mroziuk<br>
Pawe� Jana� - Marek Reda<br>
Janusz Kaczor - Dawid Pikul<br>
�ukasz Bobowski - Dariusz Kosz<br>
Kamil G�rka - Mateusz �bikowski<br><br>
</td>
<td>
<B>Runda 6</B><br><br>

�ukasz Tuszy�ski - Andrzej Kwiatkowski<br>
Bartosz Morawski - Rafa� Lenartowski<br>
Micha� Makowski - Rafa� D�browski<br>
Marcin Mroziuk - Dariusz Puton<br>
Marek Reda - Janusz Kaczor<br>
Dawid Pikul - Pawe� Jana�<br>
Dariusz Kosz - Kamil G�rka<br>
Mateusz �bikowski - �ukasz Bobowski<br><br>
</td></tr>
<tr><td>
<B>Runda 7</B><br><br>

Dariusz Puton - �ukasz Tuszy�ski<br>
Rafa� D�browski - Bartosz Morawski<br>
Rafa� Lenartowski - Micha� Makowski<br>
Andrzej Kwiatkowski - Marcin Mroziuk<br>
�ukasz Bobowski - Marek Reda<br>
Kamil G�rka - Dawid Pikul<br>
Pawe� Jana� - Dariusz Kosz<br>
Janusz Kaczor - Mateusz �bikowski<br><br>
</td>
<td>
<B>Runda 8</B><br><br>

�ukasz Tuszy�ski - Rafa� D�browski<br>
Bartosz Morawski - Dariusz Puton<br>
Micha� Makowski - Andrzej Kwiatkowski<br>
Marcin Mroziuk - Rafa� Lenartowski<br>
Marek Reda - Kamil G�rka<br>
Dawid Pikul - �ukasz Bobowski<br>
Dariusz Kosz - Janusz Kaczor<br>
Mateusz �bikowski - Pawe� Jana�<br><br>
</td>
<td>
<B>Runda 9</B><br><br>

Mateusz �bikowski - �ukasz Tuszy�ski<br>
Dariusz Kosz - Bartosz Morawski<br>
Dawid Pikul - Micha� Makowski<br>
Marek Reda - Marcin Mroziuk<br>
Pawe� Jana� - Rafa� D�browski<br>
Janusz Kaczor - Dariusz Puton<br>
�ukasz Bobowski - Andrzej Kwiatkowski<br>
Kamil G�rka - Rafa� Lenartowski<br><br>
</td></tr>
<tr><td>
<B>Runda 10</B><br><br>

�ukasz Tuszy�ski - Dariusz Kosz<br>
Bartosz Morawski - Mateusz �bikowski<br>
Micha� Makowski - Marek Reda<br>
Marcin Mroziuk - Dawid Pikul<br>
Rafa� D�browski - Janusz Kaczor<br>
Dariusz Puton - Pawe� Jana�<br>
Andrzej Kwiatkowski - Kamil G�rka<br>
Rafa� Lenartowski - �ukasz Bobowski<br><br>
</td>
<td>
<B>Runda 11</B><br><br>

Dawid Pikul - �ukasz Tuszy�ski<br>
Marek Reda - Bartosz Morawski<br>
Mateusz �bikowski - Micha� Makowski<br>
Dariusz Kosz - Marcin Mroziuk<br>
�ukasz Bobowski - Rafa� D�browski<br>
Kamil G�rka - Dariusz Puton<br>
Pawe� Jana� - Andrzej Kwiatkowski<br>
Janusz Kaczor - Rafa� Lenartowski<br><br>
</td>
<td>
<B>Runda 12</B><br><br>

�ukasz Tuszy�ski - Marek Reda<br>
Bartosz Morawski - Dawid Pikul<br>
Micha� Makowski - Dariusz Kosz<br>
Marcin Mroziuk - Mateusz �bikowski<br>
Rafa� D�browski - Kamil G�rka<br>
Dariusz Puton - �ukasz Bobowski<br>
Andrzej Kwiatkowski - Janusz Kaczor<br>
Rafa� Lenartowski - Pawe� Jana�<br><br>
</td></tr>
<tr><td>
<B>Runda 13</B><br><br>

Marcin Mroziuk - �ukasz Tuszy�ski<br>
Micha� Makowski - Bartosz Morawski<br>
Mateusz �bikowski - Marek Reda<br>
Dariusz Kosz - Dawid Pikul<br>
Rafa� Lenartowski - Rafa� D�browski<br>
Andrzej Kwiatkowski - Dariusz Puton<br>
Pawe� Jana� - Kamil G�rka<br>
Janusz Kaczor - �ukasz Bobowski<br><br>
</td>
<td>
<B>Runda 14</B><br><br>

�ukasz Tuszy�ski - Micha� Makowski<br>
Bartosz Morawski - Marcin Mroziuk<br>
Marek Reda - Dariusz Kosz<br>
Dawid Pikul - Mateusz �bikowski<br>
Rafa� D�browski - Andrzej Kwiatkowski<br>
Dariusz Puton - Rafa� Lenartowski<br>
Kamil G�rka - Janusz Kaczor<br>
�ukasz Bobowski - Pawe� Jana�<br><br>
</td>
<td>
<B>Runda 15</B><br><br>

�ukasz Tuszy�ski - Bartosz Morawski<br>
Micha� Makowski - Marcin Mroziuk<br>
Marek Reda - Dawid Pikul<br>
Dariusz Kosz - Mateusz �bikowski<br>
Rafa� D�browski - Dariusz Puton<br>
Andrzej Kwiatkowski - Rafa� Lenartowski<br>
Kamil G�rka - �ukasz Bobowski<br>
Janusz Kaczor - Pawe� Jana�<br><br>
</td></tr>
</table>

<?include "../../../files/php/bottom.php"?>

