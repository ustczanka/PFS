<?
//
//
//
//UWAGA! Wystarczy podaæ tylko nazwê turnieju, datê oraz ilo¶æ rund. 
//Linki bêd± siê aktualizowaæ automatycznie po wrzuceniu plików na serwer.
//
//
//

$turniej = "XVII Mistrzostwa Polski (faza fina³owa)";
$data = "Warszawa, 05-06 grudnia 2009";
$ilosc_rund = 15;


$format = "php";
$czas_odswiezania = 300;		//w sekundach
?>


<html>
<head>
	<title><?print $turniej;?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
	<meta http-equiv="Pragma" content="no-cache"> 
	<link rel="shortcut icon" href="../../../files/img/favicon.ico" />
	<link rel="stylesheet" href="../../../files/css/style.css" type="text/css" />
	<link rel="stylesheet" href="../../../files/css/style-live.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/styleie.css" /><![endif]-->
	<!--[if IE]><link rel="stylesheet" type="text/css" href="../../../files/css/style-liveie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="../../../files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="../../../files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="../../../files/js/jquery.js"></script>
	<script type="text/javascript" src="../../../files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="../../../files/js/java.js"></script>
	<script type="text/javascript" src="../../../files/js/live.js"></script>
	<script>jSubmenu("turnieje","archiwum");</script>
	<script type="text/javascript"> 
	var ilosc_rund = <?echo $ilosc_rund;?>;
	var format = <?echo "'".$format."'";?>;
	var czas_odswiezania = <?echo "'".$czas_odswiezania."'";?>;
	
	function odswiezRundy(){
		wyswietlPrzyciskiRund(ilosc_rund, format, $(".aktywna").text());
		wyswietlZakladki(ilosc_rund, format);
		przypiszClickaPrzyciskom();
	}
	
	$(document).ready(function(){
		ustawWysokoscPrzyciskowRund();
		wyswietlPrzyciskiRund(ilosc_rund, format);
		wyswietlZakladki(ilosc_rund, format);
		setInterval("odswiezRundy()", czas_odswiezania*1000);
		przypiszClickaZakladkom();
		przypiszClickaPrzyciskom();
	});
	</script>
</head>

<body>


<?include "../../../files/php/menu.php";
print "<h1>".$turniej." - na ¿ywo<span>".$data."</span></h1>";?>


<div class="box"><a href="rozklad.php" target="_blank">Rozk³ad rund</a></div>

<div id='rundy'></div>
<?
for($i=1; $i<=$ilosc_rund; $i++){
	($i<10) ? $a = "0".$i : $a = $i;
	print	"<div class='tabs' id='tab".$i."'>
			<ul class='tabNavigation'>
				<li><a href='#rozstawienie' id='arozstawienie'>Rozstawienie</a></li>
				<li><a href='#wyniki' id='awyniki'>Wyniki</a></li>
				<li><a href='#klasyfikacja' id='aklasyfikacja'>Klasyfikacja</a></li>
			</ul>";

	print "<div id='rozstawienie' class='roz tabcontent'></div>";
	print "<div id='wyniki' class='wyn tabcontent'></div>";
	print "<div id='klasyfikacja' class='kla tabcontent'></div>";
	print "</div>";
}
?>

<!--<div style="position:absolute;bottom:0px;padding: 15px 0;">Strona od¶wie¿a siê automatycznie co <b><?echo $czas_odswiezania;?></b> sekund.</div>
<div style="position:absolute;bottom:0px;padding: 15px 0;"><a href="#" class="przycisk" onClick="javascript:odswiezRundy();"><b>Od¶wie¿</b></a></div>-->
<div style="clear:both;"></div>
<?include "../../../files/php/bottom.php"?>
</body>
</html>