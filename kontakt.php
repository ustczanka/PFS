<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Kontakt</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("glowna","kontakt");</script>
  <style type="text/css">
	#info{
		padding-bottom: 16px;
		margin-bottom: 20px;
	}
	#info img{
		float: left;
		margin-right: 40px;
	}
	img.em{
		margin: 6px;
		vertical-align: middle;
	}
  </style>
</head>


<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Kontakt")</script></h1>

<div id="info">
	<img src="files/img/pfs_logo2d.png" alt="pgs logo" />
	<b>Polska Federacja Scrabble</b><br /><br />
	
	tel. 691 664 325 (Prezes)<br />
	lub 606 288 378 (Wiceprezes)
</div>

<h2>Adresy e-mail</h2>
W sprawach związanych z funkcjonowaniem federacji, turniejami, regulaminami:<br />
<a onClick="sendMail('zarzad','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />zarzad@pfs.org.pl</a><br /><br />

W sprawie OSPS (Oficjalnego Słownika Polskiego Scrabblisty), zasad dopuszczalności słów:<br />
<a onClick="sendMail('osps','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />osps@pfs.org.pl</a><br /><br />

W sprawie witryny WWW:<br />
<a onClick="sendMail('admin','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />admin@pfs.org.pl</a><br /><br />

W innych sprawach niemieszczących się w powyższych kategoriach:<br />
<a onClick="sendMail('pfs','pfs.org.pl')"><img src="files/img/email.png" class="em" alt="" />pfs@pfs.org.pl</a><br /><br />

<a href="wladze.php">Kontakt z członkami Zarządu PFS</a><br />
<a href="sedziowie.php">Kontakt z sędziami PFS</a><br />
<a href="kluby.php">Kontakt z klubami scrabblowymi</a><br />
<?require_once "files/php/bottom.php"?>
</body>
</html>
