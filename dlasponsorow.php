<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: PFS : Oferta dla sponsorów</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","dlasponsorow");</script>
 <style type="text/css">
 	p.wstep{
		font-style:italic;
	}
 </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Oferta dla sponsorów")</script></h1>

<p class="wstep">
Szanowni Państwo,<br />
Polska Federacja Scrabble pragnie zwrócić się do Państwa z propozycją sponsoringu turniejów scrabble w Polsce. Scrabble to najpopularniejsza gra słowna na świecie; zabawa jaką dają gry planszowe połączona jest tutaj z doskonałą rozrywką intelektualną, poszerzającą słownictwo i zakres wiedzy, oraz rywalizacją sportową, podobną do szachów czy brydża. W Polsce scrabble jest obecne od ponad 20 lat i stale zyskuje coraz większe grono zwolenników.<br />
Będziemy wdzięczni za zapoznanie się z poniższą ofertą<br /><br />
Z wyrazami szacunku<br />
Karol Wyrębkiewicz<br />
Prezes Polskiej Federacji Scrabble
</p>

<h2>Historia Scrabble</h2>
Gra Scrabble została wymyślona w czasach Wielkiego Kryzysu, w roku 1933. Autorem był bezrobotny amerykański architekt z miasteczka Poughkeepsie w stanie Nowy Jork, Alfred Mosher Butts. Aż do roku 1948 gra pozostawała znana tylko przyjaciołom i znajomym twórcy. Jej pierwowzór nazywał się Lexico i początkowo grało się bez planszy — ilość punktów zależała od długości słowa. Tylko niektóre litery (rzadziej występujące), dawały dodatkowe punkty.<br />
Początki <b>światowego sukcesu Scrabble</b> należy datować na rok 1952, kiedy to gra trafiła do sieci supermarketów, a sprzedaż skoczyła z kilkunastu do kilkuset kompletów dziennie. W ciągu roku sprzedano milion zestawów. Od tego momentu gra rozpoczęła systematyczny podbój świata. W Stanach Zjednoczonych scrabble są obecne niemal w każdym domu, na świecie sprzedano do tej pory ponad <b>100 milionów</b> zestawów w 29 różnych wersjach językowych, aż w 121 krajach. Od 1991 r. rozgrywane są <b>Mistrzostwa Świata</b> w wersji angielskojęzycznej.

<h2>Scrabble w Polsce</h2>
W Polsce gry oparte na zasadach Scrabble pojawiły się na przełomie lat 70-tych i 80-tych. Pierwsze lata to również dopracowywanie zasad gry i zestawu liter (np. początkowo zestawy zawierały X, V i Q). <b>I Ogólnopolski Turniej Scrabble</b> pod patronatem tygodnika „Razem” odbył się 11 kwietnia 1987 roku w Warszawie. W kolejnych latach grę popularyzowały m.in. Radio Zet, Gazeta Wyborcza, Super Express. W listopadzie 1989 roku powstał w Warszawie pierwszy klub scrabble. Od 1993 roku odbywają się <b>Mistrzostwa Polski</b>, a w 1997 roku powstała Polska Federacja Scrabble.

<h2>Polska Federacja Scrabble</h2>
Polska Federacja Scrabble ma status Stowarzyszenia i jej celem statutowym jest propagowanie znajomości języka poprzez grę Scrabble. Federacja nie prowadzi działalności gospodarczej i utrzymuje się wyłącznie ze składek członków i darowizn. PFS realizuje cele poprzez m.in. organizowanie i prowadzenie zawodów regionalnych i ogólnopolskich, prowadzenie centralnej krajowej klasyfikacji sportowej, rozwijanie umiejętności gry wśród młodzieży, prowadzenie ewidencji graczy oraz sekcji i klubów.<br />
Najważniejsze turnieje PFS to Mistrzostwa Polski, Puchar Polski, Mistrzostwa Polski Szkół, Mistrzostwa Polski Nauczycieli oraz cykl Grand Prix.<br />
Nasz reprezentant bierze udział w Mistrzostwach Świata w Scrabble w wersji anglojęzycznej.<br />
Wspólnie z Wydawnictwem Naukowym PWN wydaliśmy <a href="osps.php">Oficjalny Słownik Polskiego Scrabblisty</a> — największy w Polsce słownik zawierający ok. 2,5 mln słów. Jest on wykorzystywany w grach online serwisu <a href="http://www.zagraj.pl" target="_blank">zagraj.pl</a>, gdzie codziennie grywa w scrabble kilkaset osób.<br />
Prowadzimy własną witrynę internetową <a href="http://www.pfs.org.pl" target="_blank">www.pfs.org.pl</a>. Nasze strony są odwiedzane średnio <b>1700</b> razy dziennie. Staramy się przyciągać nie tylko wynikami turniejów, klasyfikacjami, lecz również ciekawostkami („<a href="siodemka.php">Siódemka tygodnia</a>”), poradami. Umieściliśmy również wersję online naszego słownika OSPS — służy ona jako pomoc w internetowych grach słownych, a także jako arbiter w rozgrywkach domowych. Dzięki temu jest używana ponad <b>500 000</b> razy miesięcznie.<br />
<a href="kluby.php">Kluby scrabble</a> istnieją w ponad 20 miastach w Polsce — regularnie spotykają się tam entuzjaści gry, rozgrywają turnieje, organizują także zawody ogólnopolskie.
<h2>Kto gra w Scrabble</h2>
Scrabble to gra dla wszystkich, bez względu na wiek. Większość scrabblistów to jednak <b>ludzie młodzi</b>, w wieku od 20 do 30 lat, posiadający średnie lub wyższe wykształcenie, chętnie poszerzający swoją wiedzę i zdolności językowe, lubiący również rywalizację sportową. Około 70% graczy to mężczyźni. W turniejach uczestniczą również dzieci i osoby starsze, gdyż formuła turniejów jest otwarta i nie zawiera żadnych ograniczeń.<br />
Lista rankingowa PFS liczy prawie 700 osób, w turniejach bierze udział przeważnie ok. 50-100 graczy. <br />
Zainteresowanie grą w naszym kraju stale rośnie, z szacunków wynika, iż od 1993 r. zagrało w scrabble co najmniej raz ok. 2 mln osób.<br />

<h2>Sponsoring</h2>
Mamy nadzieję, że scrabble zainteresuje również Państwa i wspólnie będziemy mogli rozpowszechniać tę pasjonującą i niezwykle pożyteczną grę.<br />
Obecnie podstawową działalnością PFS jest organizowanie <b>turniejów mistrzowskich</b> na szczeblu centralnym - Mistrzostwa Polski, Puchar Polski, Mistrzostwa Polski Szkół oraz współorganizowanie ogólnopolskich turniejów zaliczanych do rankingu PFS. Kalendarz na 2012 r. zawierał <b>36</b> takich <b>turniejów</b> w 28 miastach Polski - najwięcej w Warszawie i okolicach oraz w Wałczu. Co roku najlepsze i najsilniej obsadzone turnieje tworzą cykl <a href="archiwum_gp.php" target="_blank">Grand Prix</a>, w którym uczestnicy zdobywają punkty za zajęcie czołowych miejsc. <br />
<a href="podsumowania.php" target="_blank">W 2012 r.</a> w turniejach PFS wzięło udział w sumie ponad <b>1800</b> osób<b>.</b> Obecnie
<a href="ranking.php">lista rankingowa</a> PFS wraz z poczekalnią liczy prawie 700 osób.<br /><br />

W zamian za sponsorowanie działalności PFS oferujemy szereg działań o charakterze promocyjnym:
<ol>
	<li>Umieszczenie nazwy sponsora przed nazwą cyklu Grand Prix np. SPONSOR Grand Prix 2013.</li>
	<li>Umieszczenie logo firmy i linku do strony na witrynie internetowej Polskiej Federacji Scrabble, na oficjalnej stronie Mistrzostw Polski itp.</li>
	<li>Umieszczenie dużych ściennych banerów reklamowych na turniejach.</li>
	<li>Umieszczenie logo firmy na materiałach informacyjnych, plakatach, kartach zapisu wyników.</li>
	<li>Rozprowadzanie materiałów informacyjno-reklamowych wśród uczestników i widzów.</li>
	<li>Zaznaczenie wkładu sponsora poprzez wymienianie nazwy w trakcie trwania imprezy.</li>
	<li>Konkursy utrwalające wśród graczy nazwę sponsora — np. konkurs z nagrodami za ułożenie na planszy nazwy firmy, o ile słowo to znajduje się w Oficjalnym Słowniku Polskiego Scrabblisty.</li>
</ol>
Zobowiązujemy się do podkreślania Państwa roli w propagowaniu scrabble podczas cyklu ogólnopolskich turniejów. Przewidujemy nagłośnienie kolejnych imprez w mediach, co może stać się znakomitą okazją do promocji reprezentowanej przez Państwa firmy. Logo Państwa firmy może jako znak partnera w wydarzeniu pojawiać się w telewizji, na plakatach oraz poprzez wymienienie nazwy w radiu. Udział Państwa zostanie podkreślony przez organizatorów w wywiadach telewizyjnych i radiowych. <br /><br />

Sponsorowanie PFS może przybrać następujące formy:
<ul>
	<li>darowizny finansowe,</li>
	<li>nagrody rzeczowe,</li>
	<li>pokrycie kosztów związanych z wynajęciem sali turniejowej,</li>
	<li>pokrycie kosztów związanych z noclegiem i wyżywieniem uczestników,</li>
	<li>darowizna celowa na zakup zegarów turniejowych (w zamian dodatkowo oferujemy możliwość umieszczenia logo sponsora na zegarach),</li>
	<li>darowizna celowa na zakup zestawów turniejowych (w zamian dodatkowo oferujemy możliwość umieszczenia logo sponsora na workach z literkami oraz stojakach).</li>
</ul><br />
Serdecznie dziękujemy za zapoznanie się z naszą ofertą i liczymy na rozpoczęcie owocnej współpracy.<br /><br />
<b>Polska Federacja Scrabble</b><br />

tel. 691 664 325 (Prezes) lub 606 288 378 (Wiceprezes)<br />
<a href="wladze.php">Zarząd</a><br /><br />
REGON 012 745 363<br />
NIP 534 22 60 652<br /><br />
<a href="http://www.pfs.org.pl/" target="_blank">http://www.pfs.org.pl</a><br />
<a onClick="sendMail('pfs','pfs.org.pl')">pfs@pfs.org.pl</a>
<?require_once "files/php/bottom.php"?>
</body>
</html>
