<? require "files/php/funkcje.php";

if ($_GET['id']) {
    $tour = pfs_select_one (array (
        table   => $DB_TABLES[tours],
        where   => array ( id => $_GET['id'], ),
        fields  => array ( 'id', 'nazwa', 'ilosc_rund', 'miasto', 'data_od', 'data_do' )
    ));
}
?>

<html>
<head>
    <title><?php print $tour ? $tour->nazwa . ' - na żywo' : 'Relacje LIVE';?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <link rel="shortcut icon" href="http://www.pfs.org.pl/files/img/favicon.ico" />
    <link rel="stylesheet" href="http://www.pfs.org.pl/files/css/style.css" type="text/css" />
    <link rel="stylesheet" href="http://www.pfs.org.pl/files/css/style-live.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="http://www.pfs.org.pl/files/css/styleie.css" /><![endif]-->
    <!--[if IE]><link rel="stylesheet" type="text/css" href="http://www.pfs.org.pl/files/css/style-liveie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="http://www.pfs.org.pl/files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="http://www.pfs.org.pl/files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="http://www.pfs.org.pl/files/js/jquery.js"></script>
    <!--<script type="text/javascript" src="http://www.pfs.org.pl/files/js/jquery-bp.js"></script>-->
    <script type="text/javascript" src="http://www.pfs.org.pl/files/js/java.js"></script>
    <script type="text/javascript" src="http://www.pfs.org.pl/files/js/live.js"></script>
    <script type="text/javascript">

    jSubmenu('glowna', 'live');

    var ilosc_rund   = <?php echo $tour->ilosc_rund;?>;
    var id_turnieju  = <?php echo $tour->id;?>;
    var refresh_time = 30;

    function refreshLive () {
        if ($('#refresh_on').is (':checked')) {
            odswiezRundy (ilosc_rund, id_turnieju);
        }
    }

    $(document).ready(function(){
        odswiezRundy (ilosc_rund, id_turnieju);
        setInterval ("refreshLive()", refresh_time * 1000);
    });
    </script>
</head>

<body>


<?php
include_once "files/php/menu.php";

if ($tour) {
    print "<h1>$tour->nazwa - na żywo<span>$tour->miasto, ". wyswietlDate ($tour->data_od, $tour->data_do, true) . "</span></h1>";

    $zapisy = pfs_select (array (
        table   => $DB_TABLES[games],
        where   => array ( id_turnieju => $tour->id ),
        order   => array ( '!id' )
    ));

    foreach ($zapisy as $zapis) {
        print "<a href='http://www.pfs.org.pl/zapis.php?id=$zapis->id'>$zapis->nazwa - <b>$zapis->gospodarz : $zapis->gosc</b></a><br>";
    }
?>
<!--<div class="box" style="margin-right:20px;"><a href="http://www.pfs.org.pl/czat.php" target="_blank">Czat</a></div>-->
<!--<div style="padding:5px;"><a href="#" class="przycisk" onClick="javascript:refreshLive();"><b>Odśwież</b></a></div>-->

<input type="checkbox" value="1" id="refresh_on" name="refresh_on">
    <label for="refresh_on">Włącz automatyczne odświeżanie co 30 sekund</label><br><br>
<!--   
<center>
<a href=" http://www.lotto.pl/" target="_blank"><img src="../rozne/logo/lotto.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="http://www.pfs.org.pl/czat.php" target="_blank"><script>naglowek("CZAT")</script></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href=" http://www.lotto.pl/" target="_blank"><img src="../rozne/logo/lotto.png"></a></center>
<br><br>  
-->
<div id='buttonContainer'></div>

<?php
for ($i = 1; $i <= $tour->ilosc_rund; $i++){
    print   "
        <div class = 'tabContainer' id = 'tab_" . $i . "'>
            <ul class='tabNavigation'>
                <li><a href='#rozstawienie' id='tab_rozstawienie'>Rozstawienie</a></li>
                <li><a href='#wyniki'       id='tab_wyniki'>Wyniki</a></li>
                <li><a href='#klasyfikacja' id='tab_klasyfikacja'>Klasyfikacja</a></li>
                <li><a href='#komentarz'    id='tab_komentarz'>Komentarze sędziego</a></li>
            </ul>
            <div id='rozstawienie'   class='roz tabContent'></div>
            <div id='wyniki'         class='wyn tabContent'></div>
            <div id='klasyfikacja'   class='kla tabContent'></div>
            <div id='komentarz'      class='kom tabContent'></div>
        </div>";
}
?>

<div style="position:absolute;bottom:0px;padding: 15px 0;"></div>



<?php
}
else {
    print "
        <h1>Relacje LIVE</h1>
        <a href='../kalendarz.php'>Sprawdź, kiedy odbędzie się najbliższy turniej.</a>";
}

include_once "files/php/bottom.php"; ?>
</body>
</html>
