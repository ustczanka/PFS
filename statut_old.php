<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Statut PFS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","statut");</script>
  <style type="text/css">
  	p.rozdzial{
		text-align:center;
		font-size:12px;
		font-weight:bold;
		margin-top:30px;
	}
	p.paragraf{
		text-align:center;
		font-size:12px;
		margin-top: 16px;
		margin-bottom: 6px;
	}
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Statut Polskiej Federacji Scrabble")</script></h1>
Poniższy tekst Statutu zawiera zmiany przyjęte przez Walne Zgromadzenie 24.06.2006 r.
<div class="alignright"><a href="sad_kolezenski.php">Regulamin Sądu Koleżeńskiego</a></div>

<p class="rozdzial">Rozdział I<br />POSTANOWIENIA OGÓLNE</p>
<p class="paragraf">§ 1.</p>
Stowarzyszenie o nazwie Polska Federacja Scrabble zwane dalej „Stowarzyszeniem”, jest dobrowolnym, samorządnym, trwałym zrzeszeniem mającym na celu rozwijanie i propagowanie inicjatyw, postaw i działań sprzyjających znajomości języka polskiego i innych języków, upowszechnianie tej znajomości poprzez grę Scrabble, a także wspieranie organizacyjne i rzeczowe osób fizycznych i jednostek organizacyjnych, które podejmują takie działania.
<p class="paragraf">§ 2.</p>
Siedzibą Stowarzyszenia jest Warszawa.
<p class="paragraf">§ 3.</p>
Stowarzyszenie działa na podstawie przepisów ustawy Prawo o stowarzyszeniach (Dz. U. z 1989 r. Nr 20, poz. 104 z późn. zm.) oraz niniejszego statutu i z tego tytułu posiada osobowość prawną.
<p class="paragraf">§ 4.</p>
Stowarzyszenie może być członkiem krajowych i międzynarodowych organizacji o podobnym celu działania.
<p class="paragraf">§ 5.</p>Stowarzyszenie swoim działaniem obejmuje obszar Rzeczpospolitej Polskiej. Dla właściwego realizowania swych celów
Stowarzyszenie może prowadzić działalność poza granicami Rzeczpospolitej Polskiej.
<p class="paragraf">§ 6.</p>Czas trwania Stowarzyszenia nie jest ograniczony.
<p class="paragraf">§ 7.</p>
Stowarzyszenie realizuje cele przez:
<ol>
	<li>wszechstronne propagowanie umiejętności gry Scrabble,</li>
    <li>współpracę i wzajemną pomoc członków Stowarzyszenia,</li>
    <li>współpracę z osobami i instytucjami w zakresie zbierania informacji i wymiany doświadczeń związanych z grą Scrabble,</li>
    <li>organizowanie i prowadzenie:
    	<ol style="list-style-type:lower-latin;">
        	<li>w sposób bezpośredni lub pośredni zawodów regionalnych, krajowych, pucharowych, treningowych, propagandowych i innych zgodnie z ustalonym systemem współzawodnictwa sportowego,</li>
            <li>centralnej krajowej klasyfikacji sportowej,</li>
            <li>obozów i wczasów rekreacyjno-scrabblowych,</li>
        </ol></li>
    <li>ustalanie kalendarza sportowego i regulaminu rozgrywek,</li>
    <li>rozwijanie umiejętności gry wśród młodzieży,</li>
    <li>prowadzenie ewidencji graczy oraz sekcji i klubów,</li>
    <li>prowadzenie rejestracji zawodów, wyników oraz ewidencji , dokumentacji i sprawozdawczości sportowej i organizacyjnej,</li>
    <li>prowadzenie działalności integrującej członków Stowarzyszenia poprzez aktywność kulturalną, rekreacyjną i towarzyską,</li>
    <li>inne działania sprzyjające rozwojowi statutowych celów organizacji.</li>
</ol>
<p class="paragraf">§ 8.</p>
Stowarzyszenie dla realizacji swoich statutowych celów może powołać inne organizacje w granicach dopuszczonych prawem.
<p class="paragraf">§ 9.</p>
Realizując powyższe cele Stowarzyszenie opiera się na społecznej pracy członków. Może jednak zatrudniać pracowników do prowadzenia swych spraw.
<p class="paragraf">§ 9'.</p>
Członkowie Stowarzyszenia dzielą się na:
<ol>
	<li>Członków Zwyczajnych</li>
    <li>Członków Honorowych</li>
    <li>Członków Wspierających</li>
</ol>
<p class="paragraf">§ 10.</p>
Członkiem zwyczajnym Stowarzyszenia może być pełnoletni obywatel Rzeczpospolitej Polskiej oraz cudzoziemiec, także nie mający miejsca zamieszkania na terytorium Rzeczpospolitej Polskiej, który:
<ol>
	<li>ma pełną zdolność do czynności prawnych</li>
    <li>nie jest pozbawiony praw publicznych</li>
    <li>złoży pisemną deklarację członkostwa</li>
    <li>zostanie przyjęty przez Zarząd Stowarzyszenia</li>
</ol>
Osoby prawne mogą być członkami wspierającymi.
<p class="paragraf">§ 11.</p>
Członkostwo Stowarzyszenia nabywa się przez przyjęcie kandydatury przez Zarząd Stowarzyszenia zwykła większością głosów w drodze uchwały.
<p class="paragraf">§ 12.</p>
<ol>
	<li>Członkowie Stowarzyszenia zobowiązani są:
    	<ol style="list-style-type:lower-latin">
        	<li>swoją postawą i działaniami przyczyniać się do wzrostu roli i znaczenia Stowarzyszenia,</li>
            <li>dbać o jego dobre imię,</li>
            <li>zabiegać o poprawę opinii publicznej i klimatu wokół środowiska ludzi związanych z Scrabble,</li>
            <li>popierać i czynnie realizować cele Stowarzyszenia,</li>
            <li>przestrzegać postanowień statutu,</li>
            <li>regularnie opłacać składki.</li>
        </ol>
    </li>
    <li>Członek zwyczajny oraz honorowy Stowarzyszenia ma prawo brać udział w życiu Stowarzyszenia, w szczególności:
    	<ol style="list-style-type:lower-latin">
        	<li><i>[usunięto]</i></li>
            <li>wnioskować we wszystkich sprawach dot. celów i funkcjonowania Stowarzyszenia,</li>
            <li>posiadać legitymację Stowarzyszenia i nosić odznaki Stowarzyszenia,</li>
            <li>korzystać z rekomendacji, gwarancji i opieki Stowarzyszenia w swojej działalności.</li>
        </ol>
    </li>
    <li>Czynne i bierne prawo wyborcze przysługuje wyłącznie członkom zwyczajnym Stowarzyszenia.</li>
    <li>Na wniosek Zarządu Stowarzyszenia osobom fizycznym szczególnie zasłużonym dla rozwoju ruchu scrabblowego może być nadana godność członka honorowego.</li>
    <li>Zarząd Stowarzyszenia określa kryteria nadawania członkostwa honorowego oraz prawa przysługujące członkom honorowym.</li>
</ol>
<p class="paragraf">§ 13.</p>
<ol>
	<li>Osoby prawne mogą zostać członkami wspierającymi poprzez złożenie oświadczenia woli Zarządowi Stowarzyszenia, który podejmuje w tej kwestii stosowną uchwałę.</li>
    <li>W takim samym trybie następuje ustanie członkostwa wspierającego Stowarzyszenia.</li>
    <li>Formę i rodzaj wspierania Stowarzyszenia członkowie wspierający ustalą z Zarządem Stowarzyszenia.</li>
    <li>Członkowie wspierający korzystają z praw przysługujących członkom zwyczajnym wymienionych w § 12 ust. 2 pkt. 2 - 4.</li>
</ol>
<p class="paragraf">§ 14.</p>
Skreślenie z listy członków Stowarzyszenia następuje przez:
<ol>
	<li>rezygnację pisemną złożona na ręce Zarządu,</li>
    <li>wykluczenie przez Zarząd za:
    	<ol style="list-style-type:lower-latin">
        	<li>za działalność sprzeczną ze statutem oraz uchwałami Stowarzyszenia,</li>
            <li>za zaleganie z opłatą składki członkowskiej przez trzy miesiące od daty ostatecznego terminu wpłaty,</li>
            <li>na pisemny umotywowany wniosek co najmniej 10% ogólnej liczby zwyczajnych członków Stowarzyszenia z przyczyn określonych lit. a,</li>
            <li>utratę praw obywatelskich w wyniku prawomocnego wyroku sądu,</li>
        </ol>
    </li>
    <li>śmierć członka.</li>
</ol>    
<p class="paragraf">§ 15.</p>
Od uchwały Zarządu w przedmiocie wykluczenia członkowi przysługuje odwołanie do Walnego Zgromadzenia Członków na co najmniej 7 dni przed terminem Walnego Zgromadzenia. Uchwała Walnego Zgromadzenia jest ostateczna.

<p class="rozdzial">Rozdział II<br />WŁADZE STOWARZYSZENIA</p>
<p class="paragraf">§ 15.</p>
Władzami Stowarzyszenia są:
<ol>
	<li>Walne Zgromadzenie Członków,</li>
    <li>Zarząd,</li>
    <li>Komisja Rewizyjna,</li>
    <li>Sąd Koleżeński.</li>
</ol>
<p class="paragraf">§ 17.</p>
Kadencja wszystkich władz wybieralnych Stowarzyszenia trwa 2 lata.
<p class="paragraf">§ 18.</p>
<ol>
	<li>Uchwały wszystkich władz Stowarzyszenia zapadają zwykłą większością głosów przy obecności co najmniej połowy członków uprawnionych do głosowania, jeśli dalsze postanowienia statutu nie stanowią inaczej.</li>
    <li>W przypadku ustąpienia członków władz wybieralnych w czasie trwania kadencji władzom tym przysługuje prawo kooptacji do wysokości jednej drugiej składu pochodzącego z wyboru.</li>
</ol>
<p class="paragraf">§ 19.</p>
Najwyższą władza Stowarzyszenia jest Walne Zgromadzenie Członków.
<ol>
	<li>Walne Zgromadzenie zwołuje Zarząd co najmniej jeden raz na dwanaście miesięcy. Nadzwyczajne Walne Zgromadzenie jest zwoływane przez Zarząd z własnej inicjatywy, na wniosek Komisji Rewizyjnej lub co najmniej 20% ogólnej liczby zwyczajnych członków Stowarzyszenia. Zarząd powiadamia o terminie, miejscu i propozycjach porządku obrad Walnego Zgromadzenia wszystkich członków Stowarzyszenia listami poleconymi lub w każdy inny skuteczny sposób co najmniej 14 dni przed terminem rozpoczęcia obrad. Nadzwyczajne Walne Zgromadzenie zwołuje Zarząd w terminie 3 miesięcy od daty zgłoszenia wniosku. Nadzwyczajne Walne Zgromadzenie obraduje nad sprawami, dla których zostało zwołane.</li>
    <li>W Walnym Zgromadzeniu winna uczestniczyć co najmniej połowa członków uprawnionych do głosowania w pierwszym terminie, a w drugim terminie, który może być wyznaczony 15 minut po terminie pierwszym - może ono skutecznie obradować bez względu na liczbę uczestników.</li>
    <li>W Walnym Zgromadzeniu mogą uczestniczyć wyłącznie członkowie zwyczajni, honorowi oraz, z głosem doradczym, przedstawiciele członków wspierających Stowarzyszenia i zaproszeni goście.</li>
    <li>Do kompetencji Walnego Zgromadzenia należy:
        <ol style="list-style-type:lower-latin">
            <li>uchwalanie programu działania Stowarzyszenia,</li>
            <li>rozpatrywanie i zatwierdzanie sprawozdań Zarządu i Komisji Rewizyjnej,</li>
            <li>uchwalanie regulaminu obrad Walnego Zgromadzenia,</li>
            <li>udzielanie absolutorium ustępującemu Zarządowi,</li>
            <li>wybór członków Zarządu i Komisji Rewizyjnej,</li>
            <li>uchwalanie zmian statutu,</li>
            <li>podjęcie uchwały w sprawie rozwiązania Stowarzyszenia,</li>
            <li>rozpatrywanie odwołań od uchwał Zarządu,</li>
            <li>powoływanie i odwoływanie składu Sądu Koleżeńskiego oraz rozpatrywanie odwołań od jego orzeczeń,</li>
            <li>rozpatrywanie skarg członków na działalność Zarządu.</li>
        </ol>
    </li>
    <li>Uchwały Walnego Zgromadzenia zapadają zwykła większością głosów członków.</li>
    <li>Zmiana statutu, odwołanie Prezesa, członków Zarządu, Komisji Rewizyjnej, Sądu Koleżeńskiego oraz rozwiązanie Stowarzyszenia wymaga bezwzględnej większości przy obecności połowy członków Stowarzyszenia w pierwszym terminie; w drugim terminie wymóg obecności ponad połowy członków nie obowiązuje.</li>
    <li>Każdemu członkowi przysługuje jeden głos.</li>
</ol>
<p class="paragraf">§ 20.</p>
<ol>
	<li>Zarząd liczy od 5 do 7 członków wybranych przez Walne Zgromadzenie.</li>
    <li>Zarząd składa się z Prezesa, wiceprezesa, skarbnika i od dwóch do czterech innych członków Zarządu.</li>
    <li>Prezesa powołuje i odwołuje Walne zgromadzenie na 2-letnią kadencję.</li>
    <li>Zarząd konstytuuje się na pierwszym zebraniu po wyborach.</li>
    <li>Do kompetencji Zarządu należy:
    	<ol style="list-style-type:lower-latin">
        	<li>przyjmowanie nowych członków Stowarzyszenia,</li>
            <li>reprezentowanie Stowarzyszenia na zewnątrz i działanie w jego imieniu,</li>
            <li>kierowanie bieżącą pracą Stowarzyszenia,</li>
            <li>zwoływanie Walnego Zgromadzenia,</li>
            <li>ustalanie wysokości składek członkowskich.</li>
        </ol>
  	</li>
</ol>
<p class="paragraf">§ 21.</p> 
<ol>
	<li>Komisja Rewizyjna składa się z 3 członków wybranych przez Walne Zgromadzenie.</li>
    <li>Komisja Rewizyjna składa się z przewodniczącego, wiceprzewodniczącego i 1 członka.</li>
    <li>Do kompetencji Komisji Rewizyjnej należy:
    	<ol style="list-style-type:lower-latin">
        	<li>kontrola bieżącej pracy Stowarzyszenia,</li>
            <li>składanie wniosków w przedmiocie absolutorium na Walnym Zgromadzeniu,</li>
            <li>występowanie z wnioskiem o zwołanie Walnego Zgromadzenia.</li>
        </ol>
    </li>
</ol>
<p class="paragraf">§ 22.</p>
<ol>
	<li>Sąd Koleżeński składa się z 3 członków Stowarzyszenia nie będących członkami Zarządu ani Komisji Rewizyjnej.</li>
    <li>Do kompetencji Sądu należy rozpatrywanie każdego pisemnego wniosku członka Stowarzyszenia dotyczącego spraw Stowarzyszenia i jego członków poza
wnioskami i skargami wniesionymi na władze Stowarzyszenia.</li>
	<li>Stronom sporu przysługuje odwołanie od orzeczenia Sądu Koleżeńskiego do najbliższego Walnego Zgromadzenia.</li>
    <li>Orzeczenia Sądu zapadają w pełnym składzie.</li>
</ol>
<p class="paragraf">§ 23.</p>
<ol>
	<li>Majątek Stowarzyszenia powstaje ze składek członkowskich, darowizn, spadków, zapisów, dochodów z własnej działalności oraz ofiarności publicznej.</li>
    <li>Funduszami i majątkiem Stowarzyszenia zarządza Zarząd.</li>
    <li>Do reprezentowania Stowarzyszenia oraz zaciągania zobowiązań majątkowych potrzebne są podpisy dwóch Członków Zarządu łącznie.</li>
</ol>    
<p class="paragraf">§ 24.</p>
<ol>
	<li>Stowarzyszenie rozwiązuje się na podstawie uchwały Walnego Zgromadzenia lub w innych przypadkach przewidzianych w przepisach prawa.</li>
    <li>Podejmując uchwałę o rozwiązaniu Stowarzyszenia Walne Zgromadzenie określa sposób jego likwidacji oraz przeznaczenia majątku Stowarzyszenia.</li>
    <li>W sprawach nie uregulowanych niniejszym statutem mają zastosowanie przepisy Prawa o stowarzyszeniach.</li>
</ol>

<?require_once "files/php/bottom.php"?>
</body>
</html>

