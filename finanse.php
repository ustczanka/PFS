<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: PFS : Finanse PFS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","finanse");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Finanse PFS")</script></h1>

<h2>2013</h2>
<a href="rozne/finanse/2013/Bilans.xls">Bilans i rachunek zyskow 2013</a><br />
<a href="rozne/finanse/2013/Informacja.doc">Informacja dodatkowa do sprawozdania finansowego 2013</a><br />


<h2>2012</h2>
<a href="rozne/finanse/2012/Bilans i rachunek zyskow 2012.pdf">Bilans i rachunek zyskow 2012</a><br />
<a href="rozne/finanse/2012/Informacja dodatkowa do sprawozdania finansowego 2012.pdf">Informacja dodatkowa do sprawozdania finansowego 2012</a><br />


<h2>2011</h2>
<a href="rozne/finanse/2011/rach_wyn_2011.pdf">Rachunek wyników za 2011 rok</a><br />
<a href="rozne/finanse/2011/bilans_2011.pdf">Bilans na 31.12.2011</a><br />
<a href="rozne/finanse/2011/inf_dod_2011.pdf">Informacja dodatkowa do sprawozdania za 2011 rok</a><br />
<a href="rozne/finanse/2011/spr_mer_2011.pdf">Sprawozdanie merytoryczne z działalności PFS w 2011 roku</a><br />



<h2>2010</h2>
<a href="rozne/finanse/2010/rach_wyn_2010.pdf">Rachunek wyników za 2010 rok</a><br />
<a href="rozne/finanse/2010/bilans_2010.pdf">Bilans na 31.12.2010</a><br />
<a href="rozne/finanse/2010/inf_dodatkowa_2010.pdf">Informacja dodatkowa do sprawozdania za 2010 rok</a><br />
<a href="rozne/finanse/2010/spr_merytoryczne_2010.pdf">Sprawozdanie merytoryczne z działalności PFS w 2010 roku</a><br />



<h2>2009</h2>
<a href="rozne/finanse/2009/rachunek_wynikow_2009.pdf">Rachunek wyników za 2009 rok</a><br />
<a href="rozne/finanse/2009/bilans2009.pdf">Bilans na 31.12.2009</a><br />
<a href="rozne/finanse/2009/informacja_dodatkowa_2009.pdf">Informacja dodatkowa do sprawozdania za 2009 rok</a><br />
<a href="rozne/finanse/2009/sprawozdanie_merytoryczne_2009.pdf">Sprawozdanie merytoryczne z działalności PFS w 2009 roku</a>

<h2>2008</h2>
<a href="rozne/finanse/2008/rachunek_wynikow_2008.pdf">Rachunek wyników za 2008 rok</a><br />
<a href="rozne/finanse/2008/bilans2008.pdf">Bilans na 31.12.2008</a><br />
<a href="rozne/finanse/2008/informacja_dodatkowa_2008.pdf">Informacja dodatkowa do sprawozdania za 2008 rok</a><br />
<a href="rozne/finanse/2008/sprawozdanie_merytoryczne_2008.pdf">Sprawozdanie merytoryczne z działalności PFS w 2008 roku</a>

<h2>2007</h2>
<a href="rozne/finanse/2007/rach2007.jpg">Rachunek wyników za 2007 rok</a><br />
<a href="rozne/finanse/2007/bilans2007.jpg">Bilans na 31.12.2007</a><br />
<a href="rozne/finanse/2007/inf_dod2007.htm" target="_blank">Informacja dodatkowa do sprawozdania za 2007 rok</a>

<h2>2006</h2>
<a href="rozne/finanse/2006/bilans2006.htm" target="_blank">Bilans na 31.12.2006</a><br />
<a href="rozne/finanse/2006/InfDod_2006.htm" target="_blank">Informacja dodatkowa do sprawozdania za 2006 rok</a>

<h2>2005</h2>
<a href="rozne/finanse/2005/Spr_finans_2005.htm" target="_blank">Sprawozdanie finansowe za 2005 rok</a><br />
<a href="rozne/finanse/2005/Info_dod_2005.htm" target="_blank">Informacja dodatkowa do sprawozdania za 2005 rok</a>

<h2>2004</h2>
<a href="rozne/finanse/2004/bilans2004_06.htm" target="_blank">Bilans na 30.06.2005</a><br />
<a href="rozne/finanse/2004/bilans2004_12.htm" target="_blank">Bilans na 31.12.2005</a><br />
<a href="rozne/finanse/2004/InfDod2004.htm" target="_blank">Informacja dodatkowa do sprawozdania za 2004 rok</a>
<?require_once "files/php/bottom.php"?>
</body>
</html>
