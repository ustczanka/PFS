<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Nowości na stronie</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("glowna","anagramator");</script>
	<script>
		function go(url){
			/*url = url.replace("'", "&#39;");
			url = url.replace("&apos;", "&#39;");*/
			window.opener.location.href = url;
		}
	</script>
	<style>
		h2{
			text-align: center;
			margin-top: 10px;
		}
		#nowosci{
			background: url("files/img/bkg-nowosci.png") repeat-y scroll;
			padding: 10px;
			height: 100%;
			width: 580px;
			margin-top: -30px;
		}

		h1{
			background: url("files/img/top-nowosci.png") no-repeat scroll;
			margin-top: 0px;
			padding: 30px 10px;
			height: 30px;
			width: 580px;
		}
	</style>
</head>

<body onLoad="javascript: window.focus()" onUnload="javascript: window.opener.focus()">


<h1><script>naglowek("Nowości na stronie")</script></h1>

<div id="nowosci">


<?
pfs_connect ();
$result = mysql_query("SELECT * FROM $DB_TABLES[news] WHERE pokaz='1' ORDER BY data DESC");
$poprzednia = '';
while($row = mysql_fetch_array($result)){
	if ($row['data'] != $poprzednia){
		if($poprzednia != '')
			print "</ul>";
		if($row['data'] != '0000-00-00')
			print "<h2>".wyswietlDate($row['data'], $row['data'], false)."</h2><ul>";
		else
			print "<h2 style='margin-top:24px;'>Aktualnie</h2><ul>";
	}
	print "<li>".$row['tresc']."</li>";
	$poprzednia = $row['data'];
}
print "</ul>";
?>

</div>
</body>
</html>
