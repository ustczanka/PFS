<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Scrabble : Słowniczek gwary skrablowej</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("scrabble","slowniczek");</script>
	<style type="text/css">
	ul.hasla{
		list-style: none;
	}
	ul.hasla li{
		margin: 12px 0 12px -30px;
	}
	ul.hasla li span{
		font-size: 12px;
		font-weight: bold;
	}
	</style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Słowniczek gwary skrablowej")</script></h1>

<ul class='hasla'>
<li><span>BELGIJKA</span> – turniej polegający na tym, że wszyscy uczestnicy, mając identyczny zestaw liter na stojaku, starają się jednocześnie znaleźć najwyżej punktowany ruch; zwykle BELGIJKA trwa piętnaście–dwadzieścia dwuminutowych rund.</li>
<li><span>BEZRANKINGOWIEC</span> – gracz, który nie posiada jeszcze rankingu, zatem ma bardzo skromne doświadczenie turniejowe – porażka z BEZRANKINGOWCEM jest zwykle wstydliwie przemilczana przez doświadczonych graczy.</li>
<li><span>BIAŁA</span> – Biała Podlaska i środowisko pochodzących z tego miasta graczy (m.in. mistrz Polski z 1995 r. Paweł Stefaniak oraz zdobywca I Pucharu Polski z 1995 r. Ryszard Dębski).</li>
<li><span>BLOK</span> – wykonanie ruchu, po którym na planszy jest mniej możliwości wykonywania wysoko punktowanych ruchów; przeciwieństwo ROZWINIĘCIA.</li>
<li><span>BLOKER</span> – gracz reprezentujący defensywny styl gry, blokujący planszę od początku partii.</li>
<li><span>BLOKOWANIE</span> – prowadzenie partii w ten sposób, żeby uniemożliwić wykonywanie wysoko punktowanych ruchów, przeciwieństwo OTWARTEJ GRY; w zależności od sytuacji na planszy BLOKOWANIE może polegać na PRZYKŁADCE wykonanej krótkim słowem lub na położeniu jak najdłuższego słowa, by zająć jak najwięcej miejsca na planszy i uniemożliwić położenie SIÓDEMKI.</li>
<li><span>CIĘŻKI (DROGI) RUCH</span> – ruch za przynajmniej 50 punktów, ale nie SIÓDEMKA; na ostatnich mistrzostwach Polski Paweł Stefaniak w partii z Marcinem Skrzyniarzem przy pomocy czterech płytek wykonał ruch ORAŃ/OKOSTNYCH (1A–1D) za 90 punktów.</li>
<li><span>CIĘŻKIE (DROGIE) LITERY</span> – Ą, Ć, Ę, Ń, Ó, Ś, Ź, Ż; niektórzy uważają za ciężką literę F, inni wykluczają z tej grupy Ą i Ę.</li>
<li><span>CYBORG, IMPLANT</span> – dawniej: lekceważące, czasem z nutą zazdrości, określenie gracza, dysponującego szerokim słownictwem, ale nie potrafiącego podać choćby przybliżonego znaczenia większości używanych słów.</li>
<li><span>CZYSZCZENIE STOJAKA</span> – wybranie z kilku możliwych ruchów tego, który pozwala pozbyć się największej liczby CIĘŻKICH LITER, nadmiaru samogłosek albo spółgłosek, a nie tego, który jest najlepszy punktowo.</li>
<li><span>CZERWONA</span> – pole potrójnej premii słownej.</li>
<li><span>CZWÓRECZKI</span> – granie w cztery osoby, z pewnymi szczególnymi regułami, dotyczącymi dobierania liter i odmierzania czasu, spopularyzowane przez graczy z BIAŁEJ.</li>
<li><span>CZYŚCIOCHA</span> – siódemka bez blanka.</li>
<li><span>DOŁEM (IŚĆ)</span> – określenie taktyki w krótkim, pięcio–, sześciorundowym SZWAJCARZE, polegającej na tym, że dobry gracz, choć wygrywa pierwszych kilka partii, układa mniej punktów niż mógłby, by w następnych rundach spotykać się z potencjalnie słabszymi zawodnikami i jak najpóźniej dojść do pierwszego stołu.</li>
<li><span>DUŃCZYK</span> – turniej rozgrywany systemem duńskim, polegającym na tym, że w kolejnych rundach spotykają się ze sobą gracze, mający zbliżony dorobek z poprzednich rund, ale w przeciwieństwie do SZWAJCARA, mogą grać ze sobą gracze, którzy już wcześniej ze sobą grali.</li>
<li><span>DUŻE PUNKTY</span> – liczba zwycięstw w turnieju.</li>
<li><span>DZIEWIĄTKA, DZIEWIĘCIOKROTNOŚĆ</span> – zakrycie w jednym ruchu dwóch pól potrójnej premii słownej.</li>
<li><span>GÓRĄ (IŚĆ)</span> – określenie taktyki w krótkim, pięcio–, sześciorundowym SZWAJCARZE, polegającej na robieniu od pierwszej partii jak najlepszego SKORU.</li>
<li><span>GRANATOWA</span> – pole potrójnej premii literowej.</li>
<li><span>HEADSHOT, RZUT NA TAŚMĘ</span> – siódemka ułożona w ostatnim ruchu partii, dzięki której kładący wygrywa partię.</li>
<li><span>HIGH SCORE (SYSTEM)</span> (ang. „high score” – wysoki wynik) – zarzucony już sposób rozgrywania turniejów Scrabble, w którym ważniejsze od wygrania partii było uzyskanie dużej liczby punktów, gdyż końcowa kolejność ustalana była według sumy punktów zdobytych we wszystkich partiach..</li>
<li><span>KARPATKA</span> – sposób gry w cztery osoby parami wymyślony w marcu 1998 w Karpaczu (stąd nazwa), polegający w skrócie na tym, że gracze tworzący parę siedzą obok siebie, widzą swoje płytki i wykonują ruchy jeden po drugim (w dowolnej kolejności), nie mogą się jednak porozumiewać słowami, a jedynie za pomocą odpowiedniego ustawiania płytek na swoim stojaku.</li>
<li><span>KIT</span> – słowo niedopuszczalne w Scrabble, a jednak ułożone na planszy; typowe KITY to np.: BOŚ, MAŁŻĘ, PRZYBYJ.</li>
<li><span>KLAPKA</span> – dawniej: stojak, na którym trzyma się płytki z literami.</li>
<li><span>KUNSZT</span> – o grze, w której o wygranej zadecydowały wysokie umiejętności gracza a nie korzystne losowanie.</li>
<li><span>MACAK</span> – osoba oskarżana o MACANIE.</li>
<li><span>MACANIE</span> – szukanie blanka podczas ciągnięcia liter z worka poprzez próbę wyczucia go palcami (w niektórych zestawach blank jest gładką płytką w odróżnieniu od innych, których „druk” odznacza się na powierzchni).</li>
<li><span>MAŁE PUNKTY</span> – zsumowana liczba punktów ugranych przez gracza w całym turnieju.</li>
<li><span>MYDŁO</span> – inne określenie blanka, czyli płytki, na której nie ma żadnej litery.</li>
<li><span>NAPISAĆ</span> – dawniej: położyć słowo na planszy.</li>
<li><span>NIEBIESKA</span> – pole podwójnej premii literowej.</li>
<li><span>NIEDOCZAS</span> – sytuacja, w której graczowi zostało niewiele czasu do końca partii.</li>
<li><span>OSPS</span> – Oficjalny Słownik Polskiego Scrabblisty.</li>
<li><span>OTWARCIE</span> – położenie pierwszego słowa na planszy lub ROZWINIĘCIE.</li>
<li><span>OTWARTA GRA</span> – prowadzenie partii w ten sposób, żeby była możliwość wykonywania w przyszłości wysoko punktowanych ruchów.</li>
<li><span>ÓSEMKA</span> – ośmioliterowe słowo, powstałe zazwyczaj przez wyłożenie wszystkich płytek tworząc słowo z literą leżącą już na planszy.</li>
<li><span>POCZWÓRNOŚĆ</span> – zakrycie w jednym ruchu dwóch pól podwójnej premii słownej.</li>
<li><span>PO KOSZTACH (GRA)</span> – sytuacja, gdy spotykają się gracze, których dzieli więcej niż 50 pkt. w rankingu i wygrywa gracz wyżej sklasyfikowany; po takim pojedynku żaden z graczy nie zyskuje ani nie traci w rankingu.</li>
<li><span>POŁÓWKOWY</span> – obecnie najczęściej stosowany system rozgrywania turniejów, polegający na tym, że grupa graczy mających tyle samo zwycięstw zostaje „złamana na pół” i pierwszy gracz z górnej połówki gra z pierwszym zawodnikiem z dolnej połówki.</li>
<li><span>POPRAWKA KRYSI</span> – reguła, stosowana w partiach towarzyskich, popularna w Łodzi, polegająca na tym, że jeśli gracz nie zna choćby przybliżonego znaczenia układanego słowa, szuka go na swoim czasie w słownikach i podaje definicję przeciwnikowi; propozycja łódzkiej scrabblistki Krystyny Augustyniak.</li>
<li><span>POSTAWIĆ</span> – położyć słowo na planszy.</li>
<li><span>PREMIA, SIÓDEMKA, SKRABEL</span>, rzadko: STOJAK, WYKŁADKA, CAŁOSTKA, ZE WSZYSTKICH (WYJŚĆ), PIĘĆDZIESIĄTKA – wyłożenie wszystkich siedmiu liter ze stojaka premiowane dodatkowymi 50 punktami; także: posiadanie na stojaku siedmioliterowego słowa, nawet jeśli nie ma możliwości wyłożenia go na planszę („miałem SIÓDEMKĘ, ale przeciwnik zablokował wszystkie miejsca”).</li>
<li><span>PSEUDOBLOK</span> – zagranie słowa, które przedłuża się tylko TAJNĄ PRZEDŁUŻKĄ.</li>
<li><span>PRZEDŁUŻKA</span> – słowo, które skrócone na początku lub na końcu o jedną literę daje inne słowo dopuszczalne w Scrabble, np. DŹGAŃ jest przedłużką do ŹGAŃ i do DŹGA; także: położenie słowa na planszy w ten sposób, że leżące już na niej słowo zostaje przedłużone o jedną literę.</li>
<li><span>PRZYKŁADKA, SKLEJKA</span> – położenie słowa równolegle do słowa już leżącego na planszy tak, że prostopadle do nich powstają przynajmniej dwa nowe słowa.</li>
<li><span>ROZWINIĘCIE, OTWARCIE</span> – wykonanie ruchu, po którym na planszy jest więcej możliwości wykonywania wysoko punktowanych ruchów; przeciwieństwo BLOKU.</li>
<li><span>RÓŻOWA</span> – pole podwójnej premii słownej.</li>
<li><span>SAMOGRAJ, ŻARCIE</span> – określenie sytuacji, gdy gracz dolosowuje takie litery, że bez specjalnego wysiłku, układając niezbyt skomplikowane słowa, robi w partii duży wynik („strasznie mi ŻARŁO i bez problemów nagrałem 500 punktów”), podczas gdy przeciwnik toczy WALKĘ Z LITERAMI.</li>
<li><span>SCHODKI</span> – sytuacja powstająca na planszy, gdy obaj gracze grają krótkie słowa po przekątnej – klasyczny wygląd planszy przy której gra dwóch BLOKERÓW.</li>
<li><span>SIÓDEMKI, SKRABLE</span> – odmiana Scrabble, zaproponowana przez Michała Derlackiego, polegająca na tym, że każdy z graczy może w ciągu 10 minut wykonać maksymalnie 20 ruchów dwojakiego rodzaju: położenie SKRABLA lub wymiana.</li>
<li><span>SKALP, SKALPY</span> – punkty pomocnicze do obliczania rankingu uzyskane w partii.</li>
<li><span>SKOK TYGRYSA</span> – wynik gracza, który zajął dużo wyższe miejsce w turnieju niż wskazywał jego numer startowy</li>
<li><span>SKOR, SCORE</span> – (z ang. „score” – wynik, rachunek) suma punktów ułożonych przez gracza.</li>
<li><span>SPAŚĆ</span> – o słowie, które po sprawdzeniu w słowniku OSPS okazuje się być niedopuszczalne w grze i w rezultacie zostaje zdjęte z planszy.</li>
<li><span>SZARLOTKA</span> – sposób gry w dwie osoby wymyślony w lecie 2000 w Szarlocie (stąd nazwa), podobny do KARPATKI. Każdy z graczy gra ma do dyspozycji płytki na dwóch stojakach i w swojej kolejce wykonuje z każdego z nich po jednym ruchu w dowolnej kolejności.</li>
<li><span>SZWAJCAR</span> – turniej rozgrywany systemem szwajcarskim, polegającym na tym, że w kolejnych rundach spotykają się ze sobą gracze, mający zbliżony dorobek z poprzednich rund, przy czym ta sama para graczy nie może się ze sobą spotkać więcej niż raz.</li>
<li><span>SZYMCZAK</span> – trzytomowy Słownik Języka Polskiego PWN pod redakcją Mieczysława Szymczaka.</li>
<li><span>SYF, ŚMIEĆ, ŚMIECIE</span> – zestaw liter, z którego nie sposób wykonać dobrego ruchu; w zależności od sytuacji na planszy te same litery mogą być SYFEM albo nie.</li>
<li><span>TAJNA BROŃ, TAJNA PRZEDŁUŻKA</span> – mało znane słowo, będące przedłużeniem (zazwyczaj jednoliterowym) słowa znanego, czasem nawet uważanego za nieprzedłużalne (za odtajnioną już TAJNĄ PRZEDŁUŻKĘ można uważać opisywane w gazetach OPÓŁ Pawła Dawidsona).</li>
<li><span>TURNIEJ FRUSTRATÓW</span> – określenie turnieju towarzyszącego, rozgrywanego przy okazji V Mistrzostw Polski w Scrabble, w którym brali udział gracze wyeliminowani już z mistrzostw (niewątpliwie sfrustrowani); od tego czasu określenie turniejów towarzyszących mistrzostw i pucharów Polski.</li>
<li><span>WALKA Z LITERAMI</span> – określenie sytuacji, gdy gracz dobiera litery, które nie pozwalają mu robić wysoko punktowanych ruchów, a gdy już zdecyduje się na wymianę, to wymieni złe litery na jeszcze gorsze.</li>
<li><span>WCISKANIE KITU</span> – ułożenie słowa niedopuszczalnego w Scrabble i poprzez szybkie przystąpienie do zapisywania ruchu, odwrócenie uwagi bądź odpowiednią argumentację, a czasem i bez tych „subtelnych” zabiegów, skłonienie przeciwnika do zaakceptowania go.</li>
<li><span>WYSTAWKA</span> – ułożenie słowa, którego wykorzystanie w przyszłości pozwoli wykonać wysoko punktowany ruch.</li>
<li><span>ZAPIS PEŁNY</span> – zapis partii, w którym gracze notują posiadane przed każdym ruchem litery, układane wyrazy, punkty uzyskane za każdy ruch, sumę punktów po każdym ruchu i końcowy stan planszy.</li>
<li><span>ZAPIS PÓŁPEŁNY</span> – zapis partii, w którym gracze notują układane wyrazy, punkty uzyskane za każdy ruch, sumę punktów po każdym ruchu, ale nie notują posiadanych przed każdym ruchem liter; w 99 proc. przypadków ZAPIS PÓŁPEŁNY można odtworzyć na podstawie ZAPISU SKRÓCONEGO i końcowego stanu planszy.</li>
<li><span>ZAPIS SKRÓCONY</span> – zapis partii obejmujący jedynie punktację za poszczególne ruchy i sumę punktów po każdym ruchu.</li>
<li><span>ZBIERACTWO</span> – styl gry, polegający na robieniu nisko punktowanych ruchów w nadziei szybkiego uzbierania SKRABLA.</li>
</ul>


<?require_once "files/php/bottom.php"?>
</body>
</html>

