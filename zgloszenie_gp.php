<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: Turnieje : Zgłoszenie turnieju do cyklu Grand Prix 2013</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("turnieje","gp2010");</script>
  <style type="text/css">
	input, textarea, select{
		margin: 3px 0 15px 0;
		padding: 2px;
	}
  </style>
</head>
<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Zgłoszenie turnieju do GP 2013");</script></h1>

<a href="http://www.pfs.org.pl/uchwala.php?rok=2013#uchwala2" target="_blank">Uchwała nr 2/2013 z dnia 15.01.2013 w sprawie zasad wyznaczenia turniejów Grand Prix Polski w Scrabble w 2013 roku</a><br><br>



<? if(empty($_POST['submit'])){
	print "<form method=\"post\">

	<table><tr><td>
	<div>Imię i nazwisko organizatora:<br />
	<input name=\"organizator\" type=\"text\" maxlength=\"50\" size=\"30\"></div>

	<div>Kontaktowy adres e-mail:<br />
	<input name=\"email\" type=\"text\" maxlength=\"50\" size=\"30\"></div>

	<div>Nazwa turnieju:<br />
	<input name=\"turniej_n\" type=\"text\" maxlength=\"50\" size=\"50\"></div>

	<div>Termin turnieju:<br />
	<input name=\"turniej_d\" type=\"date\" maxlength=\"20\" size=\"20\"></div>

	<div>Transmisja:<br />
		<select name='transmisja'>
			<option>TAK, zapewnimy</option>
			<option>Nie wiemy czy będzie</option>
			<option>NIE, nie będzie</option>
	</select></div>

    </td></tr>
	<tr><td colspan=\"2\">
	<div>Krótki opis turnieju (przewidywane nagrody, warunki do gry, atrakcje dodatkowe i inne):<br />
	<textarea name=\"opis\" rows=\"3\" cols=\"60\"></textarea></div>

	<input name=\"reset\" type=\"reset\" value=\"Wyczyść\" class=\"przycisk\" >
	<input type=\"submit\" name=\"submit\" value=\"Wyślij\" class=\"przycisk\" >
	</td></tr></table>
	</form>";
 }
else{

	$message = "Organizator: $_POST[organizator]\nTurniej: $_POST[turniej_n] - $_POST[turniej_d]\nTransmisja: $_POST[transmisja]\n\nOpis turnieju: $_POST[opis]";
	$header = "From: $_POST[organizator] <$_POST[email]>\nContent-Type: text/plain; charset=\"utf-8\"\nContent-Transfer-Encoding: 8bit";
	@mail("pfs@pfs.org.pl", "Zgloszenie turnieju do cyklu Grand Prix 2013", "$message", "$header") or die('Nie udało się wysłać wiadomości');
	echo "Dziękujemy za zgłoszenie turnieju do cyklu <b>Grand Prix 2013</b>.";
}


require_once "files/php/bottom.php"?>
</body>
</html>
