﻿<? include_once "files/php/funkcje.php"; ?>

<html>
<head>
    <title>Polska Federacja Scrabble :: Różności : Scrabblealia</title>
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="shortcut icon" href="files/img/favicon.ico" />
    <link rel="stylesheet" href="files/css/style.css" type="text/css" />
    <!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
    <!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
    <!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
    <script type="text/javascript" src="files/js/jquery.js"></script>
    <script type="text/javascript" src="files/js/jquery-bp.js"></script>
    <script type="text/javascript" src="files/js/java.js"></script>
    <script>jSubmenu("roznosci","scrabblealia");</script>
</head>

<body>
<? require_once "files/php/menu.php"; ?>
<h1><script>naglowek("Scrabblealia")</script></h1>

W tym miejscu będziemy prezentować artykuły o scrabble, turniejach, audycje radiowe, programy telewizyjne, a także reklamy,
filmy i książki, w których występują scrabble. Mile widziana będzie pomoc w tworzeniu tego działu — czekamy na informacje od
Państwa o artykułach, materiały itp. <a onClick="sendMail('pfs','pfs.org.pl')">Napisz do nas!</a>

<h2>Artykuły o scrabble</h2>
<ul>

    <li><a href="rozne/wel_to_war.jpg">Welcome to Warsaw</a> (Wojciech Usakiewicz - biuletyn ABSP "OnBoard")</li>
    <li><a href="rozne/Asseco.pdf">Magia literek</a> (Asseco News) [pdf]</li>
    <li><a href="rozne/gazeta_olsztynska.jpg">Pierwszy klub Scrabble w powiecie szczycieńskim</a> (Nasz Mazur / Gazeta Olsztyńska)</li>
    <li><a href="rozne/tyg_podlaski.jpg">Miasto scrabblistami słynące</a> (Tygodnik Podlaski - 2.07.2010)</li>
    <li><a href="http://www.mmwroclaw.pl/10021/2010/7/3/scrabble-z-siodemka-we-wroclawiu-wideo?category=magazyn">Scrabble z Siódemką we Wrocławiu [wideo]</a></li>
    <li><a href="http://gu.us.edu.pl/node/198161">Poezja Scrabble</a> — Miesięcznik Uniwersytetu Śląskiego w Katowicach (nr 7 (52) Kwiecień 1998)</li>
    <li><a href="http://www.warsawvoice.pl/view/2451/">Triple Word Score</a> — The Warsaw Voice (29.05.2003)</li>
    <li><a href="http://www.rzeczpospolita.pl/dodatki/warszawa_070324/warszawa_a_3.html">Mistrzowie nietypowych sportów</a> — Rzeczpospolita (22.03.2007)</li>
    <li><a href="rozne/wyborcza-970209.jpg">Niełatwo pokonać mistrza</a> — Gazeta Wyborcza - 8-9.02.1997</li>
    <li><a href="rozne/stoleczna-pp1996.jpg">Znów dobre rozstawienie</a> — Gazeta Stołeczna, Puchar Polski 1996</li>
    <li>Artykuł o turnieju w Książu - <a href="rozne/ksiaz01.jpg">część 1</a>, <a href="rozne/ksiaz02.jpg">część 2</a></li>
</ul>


<h2>Relacje z turniejów</h2>
<ul>
	<li><span class="red"><b>NOWOŚĆ</b></span><a href="http://www.scrabblewszkole.pl/aktualnosci,19,wspomnienia_z_iv_turnieju_mikolajkowego" target="_blank">Relacja wideo z IV Turnieju Mikołajkowego</a> (scrabblewszkole.pl - 13.01.2012) — IV Turniej Mikołajkowy</li>
	<li><a href="rozne/milan_express.pdf">Mistrzostwa w Scrabble </a>(Express Wieczorny nr 11 - 11.06.2010) — V Mistrzostwa Milanówka w Scrabble</li>
    <li><a href="http://www.pojezierze.com.pl/gazeta/content.php?sid=6751c076dd709189f1d8da54d9da9ca4&amp;cms_id=2365">Scrabbrywalizacja</a> — I Mistrzostwa Polski Nauczycieli i I Mistrzostwa Wałcza (1-2.10.2005)</li>
    <li><a href="http://www.watra.pl/rozne/scrabble/">Podhalańskie scrabble...</a> — Podhalański Serwis Informacyjny WATRA (V Mistrzostwa Podhala, 20.02.2005)</li>
    <li><a href="rozne/maraton.jpg">Strategia i znajomość słów </a>(Gazeta Wyborcza) — Maraton Scrabblowy Katowice 2005</li>
    <li><a href="http://www.legnica.wfp.pl/news.php?id=2611&amp;rodzaj=1">V-lecie Wrocławskiego Klubu Scrabble Siódemka</a>
    — styczeń 2007</li>
</ul>

<h2>Scrabble w radiu i telewizji</h2>
<ul>
    <li><span class="red"><b>NOWOŚĆ</b></span><a href="http://tvswinoujscie.pl/201201201498/scrabblisci-czekaja-na-was.html" target="_blank">Relacja z pierwszego spotkania świnoujskiego klubu</a> (Telewizja Świnoujście - 20.01.2012)</li>
    <li><a href="http://www.youtube.com/watch?v=2APJ2LBYE5Q" target="_blank">Relacja z VII Wałeckiego Scrabblobrania”</a> (Kronika - TVP Szczecin - 13.08.2011)</li>
    <li></span><a href="http://www.youtube.com/watch?v=nnIf29eQdlA" target="_blank">Relacja z VIII Mistrzostw Bydgoszczy w Scrabble”</a> (Zbliżenia - TVP Bydgoszcz - 16.07.2011)</li>
    <li><a href="http://www.youtube.com/watch?v=vFKx2ZnCg_4" target="_blank">Relacja z turnieju XI Otwarte Mistrzostwa Ziemi Ostródzkiej „Blanki w Szranki”</a> (TV Ostróda  - 11.07.2011)</li>
    <li><a href="http://www.youtube.com/watch?v=M9UAMamSaYI" target="_blank">Relacja z turnieju "IV Walentynki w Sulęcinie"</a> (Informacje - TVP Gorzów - 13.02.2011)</li>
    <li><a href="http://youtu.be/VC248QMY9Xo" target="_blank">Film z III turnieju mikołajkowego</a> (aut. Magda Doraczyńska)</li>
    <li><a href="http://www.filmpolski.pl/fp/film1.dll/opisfilmu?baza=4&amp;start=1&amp;nrf=19543 target="_blank"">Dookoła scrabble</a> — reportaż w TVP2, 2004</li>
    <li><a href="http://www.medianews.com.pl/info_media2817.php3" target="_blank">Weekend ze Scrabble w MTV Classic</a> — 27-28.11.2004</li>
    <li><a href="http://w129.wrzuta.pl/audio/5LvAAUZMO7Z/" target="_blank">Wywiad dla Radia Eska</a> przy okazji turnieju gwiazdkowego w Bielsku Podlaskim — grudzień 2006</li>
    <li><a href="rozne/podbeskidzie2006.mp3" target="_blank">IX Mistrzostwa Podbeskidzia (Bielsko-Biała)</a> (materiał przekazało Radio Katowice) — 2006</li>
    <li><a href="http://www.tvn24.pl/2119746,28378,0,0,1,wideo.html" target="_blank">Wręczenie OSPS-a żonie premiera Tuska w „Szkle kontaktowym”</a> — 16.04.2008</li>
</ul>

<h2>Prace magisterskie o scrabble</h2>
<ul>
    <li>Justyna Nurkiewicz — <i>„Wpływ reaktywności emocjonalnej i aktywności na orientację motywacyjną u polskich Scrabblistów.”</i> <a href="rozne/mgr-nurkiewicz.html">Fragment</a><br />Praca magisterska przygotowana pod kierunkiem prof. dra hab. Jana F. Terelaka. Instytut Psychologii Uniwersytetu Kardynała Stefana Wyszyńskiego. Praca obroniona 27.02.2002 na ocenę bardzo dobrą.</li>
    <li>Justyna Fugiel — <i>„Dlaczego mistrzowie są lepsi od innych? Analiza właściwości poznawczych najlepszych polskich scrabblistów.”</i> <a href="rozne/mgr-fugiel.html">Fragment</a><br />Praca magisterska przygotowana pod kierunkiem prof. dra hab. Edwarda Nęcki. Instytut Psychologii Uniwersytetu Jagiellońskiego. Praca obroniona 25.06.2003 na ocenę bardzo dobrą.</li>
    <li>Anna Andrzejczuk — <i>„Słowniki do gier słownych jako nowy typ wydawnictw leksykograficznych.”</i> <a href="rozne/mgr_andrzejczuk.pdf">Czytaj</a><br />Praca magisterska przygotowana pod kierunkiem dra hab. Radosława Pawelca. Instytut Języka Polskiego Uniwersytetu Warszawskiego. Praca obroniona 12.05.2006 na ocenę bardzo dobrą.</li>
</ul>

<h2 id="inne">Inne:</h2>
<ul>
    <li><a href="rozne/jakscrabblistazostalem.pdf">„Jak scrabblistą zostałem”</a> — opowieść posła Marka Borowskiego (honorowego patrona programu „Scrabble w szkole”)</li>
    <li><a href="rozne/tort.jpg">Tort weselny Justyny Fugiel i Kamila Górki</a></li>
    <li><a href="http://www.vidlit.com/craziest/craziest.html">Craziest — A short story by Liz Dubelman</a> (po angielsku)</li>
    <li><a href="http://www.sculpture.org.uk/image/000000100430/">Do czego służą płytki scrabble — David Mach „Myslexia”</a></li>
    <li>Ciekawostki z archiwum Mariusza Skrobosza - ręcznie robione plansze (
        <a href="rozne/plansza_arch_1.jpg">1</a>,
        <a href="rozne/plansza_arch_2.jpg">2</a>,
        <a href="rozne/plansza_arch_3.jpg">3</a>,
        <a href="rozne/plansza_arch_3a.jpg">3a</a>,
        <a href="rozne/plansza_arch_4.jpg">4</a>,
        <a href="rozne/plansza_arch_4a.jpg">4a</a>,
        <a href="rozne/plansza_arch_5.jpg">5</a>,
        <a href="rozne/plansza_arch_5a.jpg">5a</a>,
        <a href="rozne/plansza_arch_6.jpg">6</a>,
        <a href="rozne/plansza_arch_6a.jpg">6a</a>
        ), <a href="rozne/legitymacje.jpg">legitymacje PFS</a></li>
</ul>

<? require_once "files/php/bottom.php"; ?>
</body>
</html>
