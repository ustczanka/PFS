<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: O PFS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","opfs");</script>
  <style type="text/css">
  	#info{
		padding-bottom: 16px;
		margin-bottom: 20px;
	}
  </style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("O Polskiej Federacji Scrabble")</script></h1>

<div id="info" class="ramkadolna">
	<img src="files/img/pfs_logo2d.png" class="onleft" alt="pfs logo" width="140"/>

	tel. 691 664 325 (Prezes)<br /><br />
	tel. 606 288 378 (Wiceprezes)<br /><br />
	REGON: 012 745 363<br />
	NIP: 534-226-06-52<br />
	KRS: 00 00 12 52 04
</div>
<b>PFS jest stowarzyszeniem, którego podstawowym celem jest propagowanie znajomości języka polskiego (i nie tylko polskiego) przez grę w Scrabble. </b>
<br /><br />
<ul>
<li>Walne Zgromadzenie Członków PFS, obradujące <b>18 stycznia 2014</b> wybrało nowe władze stowarzyszenia. Prezesem został Karol Wyrębkiewicz. W skład Zarządu weszli: Justyna Górka (wiceprezes), Krzysztof Bukowski (skarbnik), Kamil Górka, Joanna Jewtuch, Krzysztof Mówka, Krzysztof Obremski.
	
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>9 czerwca 2012</b> (kontynuacja 23.06.2012) wybrało nowe władze stowarzyszenia. Prezesem została Sylwia Buks. W skład Zarządu weszli: Mariola Osajda-Matczak (wiceprezes), Krzysztof Bukowski (skarbnik), Justyna Górka, Irena Sołdan.
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>14 kwietnia 2011</b> (kontynuacja 4.06.2011) dokonało zmian w składzie Zarządu. Odwołano Roberta Wagnera, a w jego miejsce powołano Grzegorza Koczkodona.<br>
		<a href="http://forum.pfs.org.pl/index.php?topic=570.msg6681#msg6681">Protokół z Walnego Zgromadzenia (dostępny na forum - tylko dla członków PFS)</a><br>

	<li>Walne Zgromadzenie Członków PFS, obradujące <b>24 kwietnia 2010</b> wybrało nowe władze stowarzyszenia. Prezesem został Krzysztof Sporczyk. W
skład Zarządu weszli: Mariola Osajda-Matczak (wiceprezes - rezygnacja 14.09.2010), Rafał Wesołowski (skarbnik), Mariusz Makuch (rezygnacja 15.09.2010), Urszula Solarska (rezygnacja 16.09.2010), Robert Wagner i Kamil Kister.<br>
W dn. 7.10.2010 Zarząd dokooptował do swojego składu Justynę Górkę.<br>
		<a href="rozne/protokol_100424.pdf">Protokół z Walnego Zgromadzenia</a><br>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>15 listopada 2008</b> postanowiło przyjąć zmiany w statucie PFS. <br>
		<a href="rozne/protokol_20081115.pdf">Protokół z Nadzwyczajnego Walnego Zgromadzenia</a><br>
		<a href="rozne/statut_20081115.pdf">Treść nowego statutu</a></li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>12 kwietnia 2008</b> wybrało nowe władze stowarzyszenia. Prezesem został Karol Wyrębkiewicz. W skład Zarządu weszli: Krzysztof Sporczyk (wiceprezes), Rafał Wesołowski (skarbnik), Mariusz Skrobosz, Jarosław Puchalski, Weronika Rudnicka, Kuba Koisar.</li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>8 kwietnia 2006</b> wybrało nowe władze stowarzyszenia. Prezesem został Karol Wyrębkiewicz. W skład Zarządu weszli: Mariusz Skrobosz (wiceprezes), Krzysztof Bukowski (skarbnik), Mariola Osajda-Matczak, Mateusz Żbikowski, Maciej Perzyński, Jarosław Puchalski.</li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>18 czerwca 2005</b> wybrało nowe władze stowarzyszenia. Prezesem został Mariusz Skrobosz. W skład Zarządu weszli: Andrzej Lożyński (wiceprezes), Krzysztof Bukowski (skarbnik), Andrzej Gostomski, Grzegorz Koczkodon, Maciej Perzyński, Karol Wyrębkiewicz.</li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>14 lutego 2004</b> w Warszawie, wysłuchało sprawozdania Zarządu i Komisji Rewizyjnej, przeprowadziło dyskusję programową i wybrało nowe władze stowarzyszenia. Prezesem PFS ponownie został Andrzej Lożyński.</li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>18 stycznia 2003</b> w Warszawie, przyjęło sprawozdania Zarządu i Komisji Rewizyjnej, przeprowadziło dyskusję programową i zatwierdziło regulaminy Mistrzostw Polski, Pucharu Polski oraz regulamin turniejowy. Zatwierdzono również system zniżek dla członków PFS.</li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>2 marca 2002</b> w Warszawie, wysłuchało sprawozdania Zarządu i Komisji Rewizyjnej, przeprowadziło dyskusję programową i wybrało wybrało nowe władze stowarzyszenia. Prezesem PFS ponownie został Andrzej Lożyński.</li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>16 grudnia 2000</b> w Warszawie, wysłuchało sprawozdań Zarządu, Komisji Rewizyjnej,  zobowiązało władze Stowarzyszenia do opracowania szczegółowych regulaminów działalności., wprowadziło zmiany w statucie.</li>
	<li>Walne Zgromadzenie Członków PFS, obradujące <b>15 listopada 1999</b> w Warszawie, wysłuchało sprawozdania Zarządu i Komisji Rewizyjnej, przeprowadziło dyskusję programową i wybrało nowe władze stowarzyszenia. Prezesem PFS ponownie został Andrzej Lożyński.</li><li>Walne Zgromadzenie Członków PFS, obradujące <b>15 listopada 1998</b> w Warszawie, wysłuchało sprawozdań Zarządu, Komisji Językowej i Komisji Rewizyjnej, dokonało zmiany w statucie umożliwiającej działanie sekcyj obcojęzycznych, a na miejsce ustępującego z Zarządu Wiktora Stępnia wybrało Grzegorza Wiączkowskiego.</li>
	<li><b>20 maja 1997</b> roku Sąd Wojewódzki w Warszawie dokonał rejestracji PFS, a <b>16 sierpnia 1997</b> w Warszawie odbyło się I Walne Zgromadzenie Członków PFS, na którym przeprowadzono dyskusję programową oraz wybrano władze stowarzyszenia.</li>
	<li><b>16 lutego 1997</b> roku w Białymstoku miało miejsce zebranie założycielskie Polskiej Federacji Scrabble, na którym postanowiono wystąpić o rejestrację stowarzyszenia.</li>
</ul>

<?require_once "files/php/bottom.php"?>
</body>
</html>
