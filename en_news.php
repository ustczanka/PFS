﻿<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polish Scrabble Federation :: About Polish Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("english","news");</script>
	<style>
		img{
			vertical-align: middle;
	}
	table.klasyfikacja td:first-child{
		font-weight: normal;
		text-align: center;
		vertical-align: middle;
		padding: 10px;
	}
	table.klasyfikacja tr:first-child td:first-child{
		font-weight: bold;
		text-align: center;
	}
	table.klasyfikacja td{
		padding: 10px;
	}
	</style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("News")</script></h1>

<h2>First German Open</h2><br>
W dniach 11-13.03. w Mannheim odbył się 22-rundowy międzynarodowy turniej First German Open.<br>
Polskę reprezentował Bartek Pięta, który zajął siódme miejsce wygrywając 13 partii. Pokonał między innymi czołowego gracza brytyjskiego Terry'ego Kirka.<br><br>


Wyniki dostępne są na stronie <a href="http://www.centrestar.co.uk/11german/html/" target="_blank">www.centrestar.co.uk</a>.<br><br>

Relacja i wrażenia Bartka na jego blogu <a href="http://www.literaxx.blogspot.com/" target="_blank">Scrabble Bubble</a>.


<br><br>




<h2>UK Open 2011 - Dzień piąty</h2><br>

Jest drugie miejsce! Brawo Rafał! Już samo przetrwanie w dobrej kondycji umysłowej pięciu dni, podczas których trzeba zagrać czterdzieści osiem partii turniejowych, stanowi nie lada wyczyn. Jeśli zaś ponadto wygrywa się trzydzieści trzy z tych czterdziestu ośmiu partii, naprawdę robi to wrażenie. Ostatni dzień niczego już w klasyfikacji nie zmienił, ostatecznie więc czołówka grupy B wygląda następująco: Steve Balment (36 pkt.), Rafał Dominiczak (33), Paul Thompson (33), Ian Coventry (30). Aby docenić wynik Rafała, warto również zwrócić uwagę na to, że Andy Gray, który w zeszłym roku rywalizował z nim o zwycięstwo piętro niżej, w grupie B zajął drugie miejsce od końca z piętnastoma zwycięstwami. <br>
Również w grupie A medaliści od wczoraj się nie zmienili. Nigel Richards (36 pkt.), reprezentujący Malezję, dwukrotnie odparł atak Amerykanina Nathana Benedicta. W pierwszej dzisiejszej partii pokonał go 566 : 348, a w szóstej 509 : 335. Ta ostatnia wygrana zapewniła mu zwycięstwo na dwie rundy przed końcem. Także symboliczny pojedynek kończący turniej Richards rozstrzygnął na swoją korzyść. Benedict (33) zajął drugie miejsce, a na trzecim stopniu podium stanął Alastair Richards (32) z Australii, być może onieśmielający przeciwników nazwiskiem. Za nimi znaleźli się amerykanin Steve Polatnick i Nigeryjczyk Wale Fashina (obaj 29). Gościnni Anglicy z Edem Martinem na czele zajęli miejsca od szóstego do dziesiątego.<br>
Zapowiadając duńczyka w trzech ostatnich rundach, nie miałem pojęcia, jak będzie on wyglądał w praktyce. Okazało się, że sędzia modyfikuje zestawienia tak, by w bezpośrednich pojedynkach spotykali się gracze o coś rywalizujący. Dlatego w czterdziestej szóstej rundzie Rafał, zamiast grać ze Steve’em, mającym wówczas już zapewnione zwycięstwo, zmierzył się z Paulem, którego wyprzedzał tylko o jeden punkt. Rozstawienie wyglądało więc następująco: pierwszy z dziesiątym, drugi z trzecim, czwarty z piątym itd. Również Nigel Richards w grupie A po zapewnieniu sobie zwycięstwa w bezpośrednim pojedynku zagrał w czterdziestej siódmej rundzie z Pakistańczykiem, zajmującym miejsce poza czołową dwudziestką. Pozwoliło to rozstrzygnąć twarzą w twarz rywalizację Nathana Benedicta z Alastairem Richardsem. Wydaje mi się to przykładem twórczej ingerencji sędziego w bezduszny system (na co zresztą pozwala regulamin światowej organizacji scrabblistów, WESPA).<br>
W 2011 roku odbędą się kolejne mistrzostwa świata. Sukces Rafała w Coventry pozwala mieć nadzieję, że polskiemu reprezentantowi uda się tam nieco awansować w światowej hierarchii. Korzystając z okazji, zapraszam więc do rywalizacji po angielsku wszystkich miłośników scrabble w Polsce, którzy chcieliby włączyć się do zabawy. Rafała, Bartka Piętę, Michała Josko i mnie można często spotkać wieczorami w ICS, internetowym klubie scrabble’owym <a href="http://www.isc.ro" target="_blank">www.isc.ro</a>. Oby latem podczas bezpośrednich eliminacji spotkało się nas jak najwięcej.<br><br>

<i>Wojciech Usakiewicz</i><br><br>




<h2>UK Open 2011 - Dzień czwarty</h2><br>

Niestety, daleko do lidera! Rafał zachowuje drugie miejsce, ale traci pięć
punktów do Steve’a Balmenta (33 pkt.). Dzień zaczął się obiecująco, bo w
pierwszej partii Rafał nadrobił punkt. W trzydziestej piątej rundzie doszło
do kolejnego bezpośredniego spotkania na szczycie, oddaję więc głos
jego uczestnikowi: „Początek był dla mnie obiecujący – pisze Rafał – bo
wyciągnąłem AEIRTU* i zagrałem URINATE [oddawać mocz]. To najlepszy
skład jaki miałem w tym turnieju, choć i tu U nie chciało mnie opuścić. Steve
zrzucił V za 14 punktów, a do mnie wróciła typowa dieta Coventry, czyli
WU [Hm… mam nadzieję, że to nie moja wina, bo zbitka W z U po angielsku
rzeczywiście jest męcząca – przyp. WU]. Dostałem IILUWWY. Nawet gdybym
zagrał WILY [przebiegły] za 23, to i tak zostałoby mi nieapetyczne IWU.
Zagrałem to jednak, na co Steve odpowiedział JOE [dla Szkota „miła sercu
dziewczyna”, a dla Amerykanina „facet”]. Ucieszyłem się, że mogę drogo
sprzedać kolejne W za 29 na nieciekawej planszy, ale nieciekawa była tylko
dla mnie, bo Steve zagrał PARENTS za 92 i dwa kolejne skrable, które weszły
jak marzenie. Po pięciu ruchach prowadził 289 : 170, a ja miałem na stojaku
DMTTVXZ i przed sobą ciasną planszę. Miodzio”. Ostatecznie Rafał przegrał tę
partię 398 : 469, z czego należy wnioskować, że parę razy jednak się odgryzł.<br>
Dalej aż do ostatniej rundy było remisowo, wtedy jednak Rafał przegrał
z Paulem 404 : 448, co oddaliło go od prowadzącego, a zbliżyło do grupy
pościgowej. Wyniki po czterdziestu rundach są następujące: Steve Balment
(33 pkt.), Rafał Dominiczak (28), Paul Thompson (26), Ian Coventry
(25), Tom Wilson (21). Chcąc myśleć o wygraniu turnieju, Rafał może
mieć po czterdziestu pięciu rundach najwyżej 2 punkty straty do lidera,
różnicą bowiem wyraźnie mu ustępuje. Oznacza to, że zostało mu pięć rund
na korespondencyjne nadrobienie przynajmniej trzech punktów. Jest to
zdecydowanie trudne zadanie. Potem trzeba byłoby jeszcze wygrać trzy ostatnie
partie…<br>
Opowiadając o turnieju, warto zajrzeć do obfitego materiału statystycznego
dołączanego przez organizatorów do wyników. Po 37 rundach Rafał zajmował
w swojej grupie trzecie miejsce pod względem liczby małych punktów, a
zdobywał ich przeciętnie 410,51 w partii. Ustępował pod tym względem
Steve’owi (437,08) i Paulowi (416,54). Dla porównania, przeciętne dwóch
najlepszych graczy grupy A, Nigela Richardsa i Nathana Benedicta, wynosiły
odpowiednio 457,57 oraz 443,50. Steve byłby tam ze swoją przeciętną czwarty,
Paul piętnasty, a Rafał szesnasty (w grupie A grało 30 osób). Różnice w średnim
wyniku partii są wyraźne, ponieważ w grupie C najlepszy wynik to 397,22, a
w grupie D drugi – po wybijającym się 401,86 liderki – to 379,16. Przeciętna
liczba układanych punktów jest więc niewątpliwie miernikiem siły gry.<br>
Ciekawa jest również statystyka zwycięstw graczy rozpoczynających partię.
We wszystkich grupach wygrywali oni większość partii, a wskaźniki dla grup
A, B, C, D w procentach wynosiły odpowiednio 56,8, 52,3, 51,0, 54,6. Grupa D
zakłóca więc tendencję spadkową. Co ciekawe, podobnie wyglądają wskaźniki
informujące o liczbie partii wygranych przez zawodnika z wyższym rankingiem.
Dla grup A, B, C, D wynosiły one w procentach odpowiednio 62,2, 57,9, 52,0,
62,1. Można by więc postawić hipotezę, że obliczanie wskaźnika na próbie
graczy najsłabszych pokazuje pewną tendencję (przypadek odgrywa w tej grupie
największą rolę), natomiast dana tendencja występuje coraz wyraźniej wraz ze
wzrostem poziomu gry. Ponieważ jednak próbie daleko do reprezentatywności,
potraktujmy tę hipotezę z przymrużeniem oka.<br>
Czas na najlepszych. Tu sytuacja się zmieniła. Czołówka liczy dwie osoby,
a grupa pościgowa ma duży dystans do odrobienia. Nigel Richards (29 pkt),
mimo porażki w ostatniej wieczornej rundzie z Amerykaninem Bobem Linnem,
wciąż prowadzi, ale już tylko małymi punktami wyprzedza Amerykanina
Nathana Benedicta (29), który trzema punktami wycisnął zwycięstwo z Theresą
Brousson z Malty. Następni w klasyfikacji są Australijczyk Alastair Richards
(26), Brytyjczycy Ed Martin i Craig Beevers oraz Nigeryjczyk Wale Fashina
(25) wreszcie Brytyjczycy David Webb i Steve Perry (24). Dwaj prowadzący
walczą o zwycięstwo, reszta – raczej o brązowy medal. Już niedługo wszystko
będzie jasne.<br><br>

<i>Wojciech Usakiewicz</i><br><br>

<h2>UK Open 2011 - Dzień trzeci</h2><br>

Jest awans! Po trzecim dniu Rafał zajmuje drugie miejsce. Od prowadzącego
Steve’a Balmenta (26 pkt.) dzielą go cztery punkty, ma jednak trzy punkty
przewagi nad trzecim graczem. Wygląda na to, że czołówka się ustabilizowała,
bo wczorajsza pierwsza piątka nadal przewodzi, choć w nieco innej kolejności
– za Rafałem są: Ian Coventry (19), Paul Thomas (19) i Jim Wilkie (18). Dzień
upłynął pod znakiem pościgu za Steve’em. Dwóch pierwszych porażek rywala
Rafał niestety nie wykorzystał, po trzeciej nieco się do niego zbliżył, ale w
dwudziestej dziewiątej i trzydziestej pierwszej rundzie sam z kolei stracił
punkty, i w ten sposób mimo awansu w klasyfikacji dzień kończy z większą
stratą do lidera.<br>
Ostatnia wieczorna runda, trzydziesta pierwsza, stanowiła zarazem początek
trzeciego koła. Jutro więc Rafał zagra jeszcze raz ze Steve’em, a w sobotę
można się spodziewać kolejnych pojedynków między nimi, bo trzy końcowe
rundy turnieju będą grane systemem duńskim (pierwszy gracz z drugim, trzeci z
czwartym itd.). Dziś najtrudniejsze teoretycznie pojedynki Rafał toczył z Ianem
Coventrym, turniejową jedynką. Oba wygrał, uległ za to innym przeciwnikom.
Zdjęcia plansz ze zwycięskich partii Rafała ze Steve’em Balmentem (458 :
398, runda 8.; partia opisana wczoraj), Andym Grayem (422 : 391, runda 18.)
i Grahamem Bonhamem (456 : 356, runda 19.) można obejrzeć, korzystając
z linku „Click here for photos” na <a href="http://
www.centrestar.co.uk/11ukopen.html">stronie firmy obsługującej turniej</a>. Andy Gray, któremu w tym turnieju
na razie wybitnie się nie wiedzie, w zeszłym roku stoczył zaciekłą i dość
zabawną walkę z Rafałem o zwycięstwo w grupie C. Zabawną, ponieważ mimo
trzydziestu ośmiu rund spotkali się ze sobą tylko w dwóch pierwszych partiach
(osiągnęli remis), a dalej rywalizowali korespondencyjnie. Na czternaście
rund przed końcem Andy miał trzy punkty przewagi, ale dwie gry później już
tylko jeden. Rafał wyszedł na prowadzenie w trzydziestej pierwszej rundzie
dzięki zdecydowanie lepszej różnicy małych punktów. W trzydziestej czwartej
zanotował stratę, remisując, ale Andy… również zremisował. Co więcej, w
przedostatniej partii obaj zgodnie zaliczyli porażki i w końcu wpadli na metę z
takim samym wynikiem. Małe punkty zdecydowały.<br>
Warto przyjrzeć się tegorocznej planszy Rafała i Andy’ego. Na środku
leży otwierające zagranie Andy’ego: VEG (warzywo lub warzywa, wyraz
nie przyjmuje więc końcowego S liczby mnogiej). Pierwszy stojak Rafała to:
DEHIQS*. Rafał postanawia zaryzykować i gra QIS/VEGS za 40 punktów. Jeśli
blef się powiedzie, da mu przewagę popartą prawdopodobieństwem rychłego
ułożenia skrabla z blankiem, jeśli nie, pozostaje szansa, że przeciwnik wystawi
potrzebną literę. Andy zdejmuje z planszy błędny ruch i układa WRaNGLES
(spiera się), a ostatnia litera trafia między różowe pola w najlepszym możliwym
miejscu. I wtedy Rafał inkasuje 130 punktów za jedynego skrabla ze swojego
składu, „czwórkę” SQuISHED (zachlupotał). Zyskuje w ten sposób na starcie
53 punkty przewagi i nie oddaje już prowadzenia do końca partii. Jak widać,
czasem odrobina szaleństwa bardzo się opłaca.<br>
Tymczasem w grupie A przez chwilę zrobiło się ciekawie. Od dwudziestej
piątej rundy Nigel Richards miał czarną serię i przegrał cztery partie z pięciu,
więc jego przewaga stopniała do dwóch punktów. W tym momencie jednak
pokonał wicelidera, Amerykanina Nathana Benedicta 431 : 411 i znów zgubił
pościg. Na razie zgromadził 24 punkty. Amerykanie są tymczasem górą w
rywalizacji z Anglikami, Nathan Benedict (21) i Steve Polatnick (20) zajmują
bowiem drugie i trzecie miejsce, ale 20 punktów ma aż siedmiu graczy – oprócz
Polatnicka jeszcze jeden Amerykanin, trzech Anglików, Australijczyk Alastair
Richards i Nigeryjczyk Wale Fashina. Pół punktu traci do nich pierwsza dama
turnieju, Maltanka Theresa Brousson.<br>
Od jutra gracze mają z górki. W sobotę grają tylko dziewięć partii, a w
niedzielę osiem, i w dodatku odbierają nagrody.<br><br>

<i>Wojciech Usakiewicz</i><br><br>


<h2>UK Open 2011 - Dzień drugi</h2><br>
Jest dobrze! Na cztery partie przed połową turnieju Rafał zajmuje trzecie miejsce, a do prowadzącego Steve’a Balmenta brakuje mu trzech punktów. Wprawdzie lider przegrał ostatnią partię wieczoru (jednym punktem!), ale niestety przegrał również Rafał, więc dystans się nie zmienił. Powoli zaczyna wykształcać się czołówka. Lider ma punktów siedemnaście, za nim są Paul Thompson i Rafał (14 pkt.) oraz Ian Coventry i Jim Wilkie (13). Z trzema członkami tej grupy Rafał zremisował w pierwszym kole 1 : 1, z Ianem jeszcze nie grał.<br>
Steve Balment to jego stary rywal. Przez trzy ostatnie lata spotkali się na UK Open sześć razy, z czego Rafał zwyciężył dwukrotnie. Jednak mimo niekorzystnego bilansu bezpośredniego, to właśnie Rafał rok temu wygrał grupę C, a Steve zajął trzecie miejsce. Ciekawa dla nas może być ich druga partia z tego roku, uwidoczniła się w niej bowiem różnica między scrabble’ami angielskimi i polskimi. Wprawdzie blanki i ciężkie litery stanowią ostrą amunicję w obu językach, ale po angielsku dodatkową grupę w tej kategorii tworzą cztery litery S, które można dołożyć do końca większości wyrazów. I właśnie we wspomnianej partii dwóm blankom Steve’a Rafał przeciwstawił cztery es. Litera ta znalazła się i w zapewniającym mu zwycięstwo 458 : 398 skrablu ONSTAGE (na scenie), i w dwóch wcześniejszych: CELOSIA i SQUEAKED (pisnął). Kolejny pojedynek Rafała ze Steve’em obejrzymy w sobotę, w trzydziestej piątej rundzie.<br>
Chcąc napisać, czym jest „celosia”, sięgnąłem do źródeł. Na swoje nieszczęście wybrałem bardzo lakoniczny słownik scrabble’owy, tam zaś znalazłem synonim „cockscomb”. Szybko napisałem więc „koguci grzebień” i w tym momencie poczułem, że coś nie gra. Jakoś nie mogłem sobie wyobrazić króla drobiu z celozją na głowie. Okazało się, że słusznie, chociaż bowiem każdy koguci grzebień można nazwać „cockscomb”, to nie zawsze „cockscomb” jest kogucim grzebieniem. Niech więc „celosia” pozostanie celozją lub, jeśli ktoś woli, grzebionatką, egzotyczną, ładnie kwitnącą rośliną. (A „cockscomb” może też być błazeńską czapką).<br>
Dość dygresji, wróćmy do Coventry. W grupie A Nigel Richards powiększa przewagę. Ma po dwudziestu rundach osiemnaście punktów i trzy wygrane więcej niż ścigający go rywale. W obu niefortunnych dla siebie partiach uległ zawodnikom z dalszych miejsc, Brytyjczykowi Bobowi Lynnowi i Nigeryjczykowi Wale’owi Fashinie. Tymczasem za jego plecami toczy się rywalizacja Brytyjczyków z resztą zagranicznych gości. Na razie górą są Brytyjczycy Drugie miejsce zajmuje w tej chwili Wayne Kelly (15 pkt.), trzeci jest – po porażce 398 : 484 z Richardsem w kończącym wieczór meczu na szczycie – David Webb (15), a piąty Ed Martin (14). Rywalizuje z nimi Amerykanin Nathan Benedict z miejsca czwartego (14), który wypchnął poza szóstkę swojego rodaka, Steve’a Polatnicka (13). Korzystając z tego bratobójczego pojedynku, na szóste miejsce awansował Pakistańczyk Muhammad Sulaiman (14). Ciekawe, ilu z tych graczy utrzyma miejsca w czołówce przez następne jedenaście rund.<br>


<br>

<i>Wojciech Usakiewicz</i><br><br>


<h2>UK Open 2011 - Dzień pierwszy</h2><br>
Zaczęło się! Dla Rafała pomyślnie, choć na samym starcie zanotował pięciopunktową porażkę. Cóż jednak znaczy pięć punkcików wobec czterdziestu ośmiu rund. Potem było znacznie lepiej. W chwili gdy piszę relację, gracze mają do rozegrania jeszcze jedną wieczorną rundę. Rafał z sześcioma punktami – czyli takim samym wynikiem, jak lider, Tom Wilson ze Szkocji – zajmuje czwarte miejsce. Zgodnie z przewidywaniami trafił do grupy B i wśród szesnastu graczy jest rozstawiony z siódemką. Wprawdzie gdyby wierzyć w magię nazwiska, faworytem grupy powinien być Ian Coventry, rankingowo numer jeden, obecnie na drugim miejscu, ale wyniki dotychczasowych partii wskazują na to, że będzie miał kilkoro groźnych konkurentów, wśród nich właśnie Rafała. A Rafał najbliższe dwie partie rozegra ze swoim sąsiadem w klasyfikacji Paulem Thompsonem, mającym pięć punktów.<br>
Tymczasem rywalizacja grupy A prawdopodobnie będzie się odbywać pod hasłem „Huzia na mistrza!”. Nigela Richardsa czasem udaje się pokonać, ale częściej nie. W każdym razie po ośmiu rundach Richards prowadzi bez porażki, a kroku dotrzymuje mu jedynie Amerykanin Steve Polatnick. Następni gracze mają dwa punkty mniej.<br>
Z kronikarskiego obowiązku wrócę jeszcze do turnieju rozgrzewkowego. Wygrał go Nigel Richards. I jak tu być przesądnym, skoro przełomowa okazała się dla niego trzynasta runda? To właśnie w niej pokonał Brytyjczyka Phila Robertshaw i odebrał mu prowadzenie, którego nie oddał już do końca. Naturalnie zwolennicy przesądów mogą twierdzić, że to Brytyjczyk w trzynastej rundzie miał pecha. Niemniej jednak w czternastej przegrał z Richardsem ponownie. Warto też zwrócić uwagę na fantastyczny finisz Nowozelandczyka. Po dwunastu grach Robertshaw miał 9 punktów i różnicę +667, a jego przeciwnik punkt mniej i różnicę +622. Sześć rund później zwycięski Richards miał 14 punktów i różnicę +1399, a drugi w stawce Robertshaw 12 punktów i różnicę +706. Ech…<br>
Skoro zaś o Richardsie mowa, trzeba jeszcze wspomnieć o niezwykłym wyczynie, jakiego dokonał niedawno, bo na początku grudnia, podczas turnieju mistrzów w malezyjskim mieście Johor Bahru. W dwudziestosiedmiorundowej rywalizacji (trzy razy każdy z każdym) startowało dziewięciu z dziesięciu dotychczasowych mistrzów świata oraz były mistrz świata juniorów. Richards wygrał ten turniej, ale nie w tym rzecz. Zadziwiająca była partia, którą rozegrał w piętnastej rundzie przeciwko Kanadyjczykowi Adamowi Loganowi. Zakończyła się ona wynikiem 715 : 325 (bez dziewiątki!), a trwała dziewięć i pół ruchu. Z dziewięciu pełnych stojaków Richards ułożył siedem skrabli i zakończył partię… słowem VIVAT. Każdy by tak chciał, jest więc o czym pomarzyć przed snem. Tylko zawodnicy muszą poczekać ze spaniem, bo mają jeszcze przed sobą jeszcze dziewiątą partię. To zresztą i tak ulgowa dawka, bo jutro muszą ich zagrać jedenaście.<br>

<br>

<i>Wojciech Usakiewicz</i><br><br>



<h2>UK Open 2011 - Rozgrzewka</h2><br>

Scrabble’owy nowy rok w Anglii po raz czwarty zaczyna się mocnym akordem. Styczniowy turniej UK Open, inicjatywa pełnego energii organizatora i dobrego scrabblisty Lena Moira, ściąga do Coventry nie tylko czołówkę brytyjską, lecz również znanych gości zagranicznych, a wśród nich najlepszego scrabblistę świata, mieszkającego w Malezji Nowozelandczyka Nigela Richardsa. Literowe harce będą trwały od środy 5 stycznia przez 5 dni, a dystans jest niemały – 48 rund.<br>
Richards wygrał już UK Open dwukrotnie, raz ustąpił pola Brytyjczykom, Helen Gipson i Philowi Robertshaw. W tym roku stawka będzie jeszcze bardziej urozmaicona niż zwykle, pierwszy raz przyjechali bowiem gracze ze Stanów Zjednoczonych i Kanady. Pojawią się zwycięzca zeszłorocznego turnieju na Malcie, Muhammad Sulaiman z Pakistanu, oraz zawsze mocni Nigeryjczycy.<br>
Członkiem tego doborowego towarzystwa będzie też – już po raz trzeci – gracz z Polski, Rafał Dominiczak. W zeszłym roku udało mu się po dramatycznym i udanym finiszu wygrać w Coventry grupę C. Wszystko wskazuje na to, że w tym roku poprzeczka zostanie podniesiona i Rafał trafi do grupy B. Jego ranking 148, który w Polsce daje miejsce wśród najlepszych, w Wielkiej Brytanii pozwala jeszcze na znaczny awans (prowadzący Nigel Richards ma ranking 207). W każdym razie aż do niedzieli będziemy Rafałowi gorąco kibicować, a przy okazji przyglądać się tym, których wszyscy polscy gracze, próbujący sił po angielsku, chcieliby kiedyś ograć.<br>
Tymczasem w Coventry scrabble’owe boje już trwają (choć na razie bez Rafała). Sześćdziesięciu graczy, podzielonych na sześć grup, bierze udział w osiemnastorundowym turnieju rozgrzewkowym, dwa razy każdy z każdym. Zaczęło się ciekawie, jako że w pierwszej grupie Nigel Richards przegrał dwie początkowe partie z Amerykaninem Steve’em Polatnickiem, a mimo to po piątej rundzie znajdował się na drugim miejscu z imponującą różnicą małych punktów +557. Ponieważ jednak scrabble to gra kapryśna, w siódmej rundzie Nigel Richards przegrał 206 punktami z… Richardsem, grupowym rywalem obecnego wicemistrza świata jest bowiem młody gracz australijski o tym samym nazwisku (i imieniu Alastair). Jak skończy się rozgrzewka, doprawdy trudno przewidzieć, ponieważ w chwili gdy piszę te słowa, trzech graczy ma 5 punktów, a trzech następnych – 4 punkty, walka jest więc bardzo wyrównana. Prowadzi Irlandczyk Feargal Weatherhead. Obiecuję dokończyć ten akapit w środę, kiedy będziemy już po pierwszych rundach turnieju głównego.


<br><br>

<i>Wojciech Usakiewicz</i><br><br>


<h2>UK Open 2011</h2><br>

W środę (5.01.) rozpocznie się w Coventry turniej UK Open. Polskę reprezentował będzie Rafał Dominiczak, który w ubiegłym roku zwyciężył w "Division C".<br>
Już wkrótce zapowiedź turnieju i codzienne relacje autorstwa Wojciecha Usakiewicza.<br><br>



<h2>Zacięty bój w Wielkiej Brytanii – Mikki Nicholson mistrzem</h2><br>

<a href="http://www.centrestar.co.uk/uknsc/2010/">2010 National SCRABBLE(R) Championship - oficjalna strona</a><br><br>

Wkrótce poznamy kolejnego mistrza Polski w scrabble. Tymczasem w ostatni weekend swojego czempiona wyłonili Brytyjczycy,
a zmagania trzymały w napięciu do ostatniej chwili.<br><br>
Walka o tytuł mistrza Wielkiej Brytanii była trzystopniowa. Z regionalnych eliminacji w 7 okręgach (szwajcar, 7 rund) do
półfinału przeszło 56 osób. Warto dodać, że liczba graczy awansujących z poszczególnych turniejów nie była równa, lecz
zależała od liczby i rankingów uczestników. Półfinałowy szwajcar (14 rund) wyłonił dwóch finalistów, którzy w niedzielę
31 października rozegrali w Londynie mistrzowski pojedynek do trzech wygranych partii.<br><br>
Już podczas półfinału w Leeds emocji nie brakowało, a rozstrzygnięcie zapadło dopiero w ostatniej rundzie. Niemal pewny
kandydat do awansu po dwunastu rundach, walijski gracz Gareth Williams, który samodzielnie prowadził z przewagą dwóch
punktów, przegrał dwie ostatnie partie i ostatecznie musiał ustąpić miejsca dwóm bardziej fortunnym rywalom.
Niespodzianki jednak nie było. Do finału awansowali Mark Nyman i Mikki Nicholson.<br><br>
Nyman jest najwyżej sklasyfikowanym graczem brytyjskim w rankingu Association of British Scrabble Players
(203, 3. miejsce). W 1993 roku zdobył mistrzostwo świata, sześć lat później był wicemistrzem. Ma też w swojej kolekcji
cztery tytuły mistrza Wielkiej Brytanii. Mikki Nicholson, należący do młodszego pokolenia, tak utytułowany nie jest,
ale ranking 191 i dwunaste miejsce na liście wystarczają jako rekomendacja.<br><br>
DEGIIJO… i znany również z polskich plansz, choć tu nieco droższy JIG (22 punkty) Nymana rozpoczyna finałową rywalizację.
Nicholson odpowiada skrablem i potem niemal przez całą partię stopniowo powiększa przewagę, między innymi słowem FRAY,
oznaczającym emocjonującą walkę (wbrew pozorom nie jest to „frajer”, co daję pod rozwagę tym, którzy chcą tłumaczyć
scrabble’owe division słowem „dywizja”). Dopiero przy stanie 395:314 Nyman wyprowadza kontrę. Z beznadziejnie
wyglądającego układu AACENOU układa do T na planszy skrabla OCEANAUT (badacz oceanów), dzięki czemu zmniejsza przewagę
przeciwnika do 9 punktów. Końcówka należy jednak do Nicholsona, który wygrywa 423:410.<br><br>
Druga partia to bezlitosna wymiana ciosów. Wystarczy powiedzieć, że połowa płytek z woreczka (właściwie prawie połowa,
bo 49) zostaje zużyta na skrable. I przewaga Nicholsona w meczu znika, nie pomaga mu nawet ostatni w tej partii skrabel
VANISHES. Nyman wygrywa 550:391. Układa między innymi bardzo ładne i znane nawet mało zaawansowanym w angielszczyźnie
uczniom słowo, mając na stojaku EEGHORT i wykorzystując T z planszy (chętni mogą poanagramować, rozwiązanie na końcu tekstu).<br><br>
Po lanczu Nyman zaczyna zmagania od skrabla ONSTEAD (farma po szkocku), ale kolejkę później Nicholson wyrównuje,
układając z dwoma blankami BECALMS (uspokaja). Decydującym momentem w bardzo zaciętej partii staje się ułożone przez
Nicholsona na czerwonym polu i dobrze znane polskim scrabblistom, a tak naprawdę łacińskie słowo SOREDIA (cząstki
plechy porostów służące do rozmnażania). Nymanowi pozostaje pochwała od prowadzącego transmisję za słowo związane
tematycznie z Halloween – GRAVE (grób). 444:435 dla Nicholsona. <br><br>
Czwarta partia rozkręca się bardzo powoli, bo po drugiej kolejce jest zaledwie 12:5. Nyman przejmuje jednak inicjatywę
dzięki INJURES (rani) z blankiem i już jej nie oddaje. Ostateczny cios zadaje drugim blankiem i – znów łacińskim –
słowem LOCUSTAE (jakieś kwiatostany, ale nie mam pojęcia jakie). Choć partia trwa aż 13,5 kolejki, wynik pozostaje niski,
383:327. Ale w meczu znowu jest remis.<br><br>
Dynamika powraca w decydującej piątej partii. Na samym początku wydaje się, że górą jest Nyman, który dzięki przedłużeniu
QUID do EQUID inkasuje 105 punktów za ułożone na czterokrotności UPDATES (uakatualnienia). W odpowiedzi jednak Nicholson
częstuje go nóżkami w galarecie, COWHEEL, i znów jest równo. Drugi skrabel, SOARERS (szybujący ludzie), zapewnia Nymanowi
jedynie minimalne prowadzenie wskutek banalnej, ale bardzo skutecznej odpowiedzi przeciwnika: EX za 45 punktów. Wkrótce
więc staje się jasne, że mistrzem zostanie ten z graczy, który dostanie drugiego blanka. Szczęście dopisuje Nicholsonowi,
a słowo OBEISANT (uniżony) zapewnia mu niezbędną przewagę. Jeszcze Nyman przedłuża PINY (sosnowy) do pięknego i dającego
mu zwycięstwo PINYVOLTHIN, ale w żadnej aptece nie ma takiego środka i w słowniku, niestety, również nie. Tak więc piąta
partia kończy się 483: 337 i przez najbliższy rok tytuł mistrza Wielkiej Brytanii dzierżyć będzie Mikki Nicholson z
Carlisle, który zwycięża w finałowym pojedynku 3:2.<br><br>
Gratulując niesamowitej wyobraźni językowej obu finalistom, zachęcam czytelników do próbowania swoich sił również w grze
po angielsku. Może komuś uda się kiedyś osiągnąć choćby zbliżony poziom. Za rok kolejne mistrzostwa Wielkiej Brytanii,
a także mistrzostwa świata.<br><br>

<i>Wojciech Usakiewicz</i><br><br>

PS: Skrabel-zagadka z drugiej partii to TOGETHER (razem).



<h2>Bartek Pięta will go to Johor Bahru</h2><br>

<img class=onleft src=foto/mpa.jpg />
Here we go! Poland has its representative for WSC in Johor Bahru.
<b>Bartek Pięta</b> from Swarzędz near Poznań is a real fighter and a Scrabble addict,
so hopefully he will better the former results of Polish players.
To meet the current Polish champion, go to his Scrabble Bubble blog at:<br>
<a href="http://www.literaxx.blogspot.com">http://www.literaxx.blogspot.com</a><br>
Bartek closed the 24-round qualifying marathon with 20 wins and knocked out his rivals.
In November we will keep our fingers crossed.
<br><br>

<h2>Polish Qualifying Tournament for WSC 2009 in Johor Bahru - Warsaw, June 27-28 and July 11-12</h2><br>

<b>Format<br></b>
June 27-28. One open division, 12 rounds with Swiss pairing for rounds 1-11 and a king of the hill for the last one. Non-Polish players are welcome.<br>
July 11-12. The four best Polish players of the first stage will keep half of their match points scored in June and play 4 round robins more. <br>
The tournament is unrated.<br><br>

<b>Location<br></b>
Warsaw Study Centre, Widok 19.<br>
The venue is situated in the centre of Warsaw, 10 minute walk from the main city crossroads of Marszałkowska and Aleje Jerozolimskie streets.<br><br>

<b>Accomodation<br></b>
No accomodation offered by the organiser. Each player must choose a hotel himself/herself. There are plenty of possibilities at: <a href="http://www.warsawtour.pl" target="_blank">www.warsawtour.pl</a><br>
There are also plenty of bars and restaurants around the venue where you can have your lunch. <br>
There will be free coffee/tea/mineral water and some cakes at the venue.<br><br>

<b>Starting fee<br></b>
25 zlotys for the tournament (ca. 6 Euro). Foreign players pay it to the organiser before the first round.<br><br>

<b>Registration<br></b>
If you are sure to come, please register <a href="formularze/formmpa09.php" target="_blank">here</a>.<br><br>

<b>Schedule<br></b>
Saturday, June 27<br>
11.00 Rounds 1-4<br>
14.00 Lunch break<br>
15.00 Rounds 5-7<br>
19.00 End of games<br>
Sunday, June 28<br>
10.00 Rounds 8-12<br>
15.00 Presentation, goodbye<br><br>

<b>Prices<br></b>
A cup for the winner, small gifts.<br><br>

<b>Rules of play<br></b>
Principally, CSW as a dictionary and ABSP rules of play are adopted (and -5 points challenge). There are some modifications however you should be aware of.<br>

The Polish custom is to use standard boards positioned sideways to both players, with no turning during the game. If you want to use a turning deluxe board, you have to bring it to Warsaw yourself.<br>

Similarly, the timers used will probably be traditional "chess clocks". If you do not like them, you can bring your own digital timer, of course.<br>

No runners will appear during the tournament. Both players check the challenged word(s) on the PC screen (to write it/them down is a duty of the challenging player and the challenged player must confirm the right spelling). If the result of the checking is doubtful for any of the players he/she can ask the tournament director to recheck it in the paper version of CSW. The director's decision is final.<br><br>

<b>Contact<br></b>
<a href="mailto:zarzad@pfs.org.pl">zarzad@pfs.org.pl</a>








<h2>Tournament in Kutno</h2>
<img src="foto/kutno1.jpg" class="onleft">
<img src="foto/kutno2.jpg" class="onright">
Warsaw and Poznań, two main centres of English scrabble in Poland, are situated 300 kilometres from one another, so mutual visits are not easy to arrange. Knowing this, players from both cities have decided to meet halfway in Kutno. The 7th of February thanks to the hospitality of the local Lutheran community they could stage a match. <br>
Two four-person teams played round robin and supplemented it by two rounds with pairings chosen by each team. Warsaw won the first round 3:1 but lost the next 1:3, so it was then as if the games should start again. But later the tile fairy seemingly had its own way and a massive win 4:0 in the fourth round led Warsaw to the final victory 16:8. Individually the only one scorer of six points was Rafał Dominiczak from Warsaw. The match was certainly a good training before the Polish qualifier to the Worlds planned for June, and the second win by Warsaw in the history of mutual contacts.<br><br>

<a href="rozne/en_kutno.doc" target="_blank">Results</a> <br><br>

Read more:<br>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.ekutno.pl/index.php?kutno=0&y=1&zoom=3346" target="_blank">www.ekutno.pl</a><br>
&nbsp;&nbsp;&nbsp;&nbsp;<a href="rozne/kutno_parafia.jpg" target="_blank">www.luteranie.pl/plock</a>



<?require_once "files/php/bottom.php"?>
</body>
</html>
