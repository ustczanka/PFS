<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polska Federacja Scrabble :: PFS : Jak przystąpić do PFS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("pfs","jakprzystapic");</script>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("Jak przystąpić do PFS")</script></h1>

<h2>Warunki, które należy spełniać, by zostać członkiem PFS</h2>
Nie jest konieczne spełnienie specjalnych warunków. Wystarczy, że masz ukończone 18 lat i pełną zdolność do czynności prawnych. Jeżeli jesteś niepełnoletni, potrzebna będzie zgoda Twoich rodziców lub opiekunów prawnych. Wypełniając deklarację zobowiązujesz się do przestrzegania Statutu PFS.

<h2>Jak przystąpić?</h2>
Aby wstąpić do Polskiej Federacji Scrabble wystarczy dokonać dwu czynności:
<ul>
	<li>wypełnić <a href="rozne/deklaracja_czlonkowska.doc">deklarację członkowską</a> i przesłać na adres PFS (Al. Armii Ludowej 17/57
 00-632 Warszawa),</li>
    <li>opłacić pierwszą składkę członkowską.</li>
</ul>
     O przyjęciu do Stowarzyszenia zadecyduje Zarząd PFS najpóźniej w ciągu 14 dni od dnia spełnienia powyższych warunków.<br />
     Jeżeli masz ochotę na przystąpienie w trakcie turnieju - zapytaj o wzór deklaracji sędziego prowadzącego.

<h2>Składka członkowska</h2>
      Składka za cały rok 2013 wynosi 84 zł. Przystępując do PFS płaci się pierwszą składkę za okres nie krótszy niż rok (kolejne składki można regulować w krótszych okresach).<br />
      Składkę członkowską można opłacić na dwa sposoby:
	  <ul>
      	<li>na konto Polskiej Federacji Scrabble w Volkswagen Bank direct
          60 2130 0004 2001 0386 3883 0001,</li>
      <li>w czasie turnieju organizowanego przez PFS - do skarbnika Federacji.</li>
	  </ul>

<h2>Korzyści z członkostwa w Polskiej Federacji Scrabble</h2>
	<ol>
      <li>Zniżki w opłacie za wpisowe w turniejach organizowanych przez PFS</li>
      <li>Zniżki na wczasach scrabblowych organizowanych przez Federację</li>
      <li>Dostępu do forum dyskusyjnego dla członków PFS</li>
	</ol>
<?require_once "files/php/bottom.php"?>
</body>
</html>
