<?include_once "files/php/funkcje.php";?>

<html>
<head>
	<title>Polish Scrabble Federation :: About Polish Scrabble</title>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="shortcut icon" href="files/img/favicon.ico" />
	<link rel="stylesheet" href="files/css/style.css" type="text/css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="files/css/styleie.css" /><![endif]-->
	<!--[if lt IE 7.]><script defer type="text/javascript" src="files/js/pngfix.js"></script><![endif]-->
	<!--[if IE]><script type="text/javascript" src="files/js/minmax.js"></script><![endif]-->
	<script type="text/javascript" src="files/js/jquery.js"></script>
	<script type="text/javascript" src="files/js/jquery-bp.js"></script>
	<script type="text/javascript" src="files/js/java.js"></script>
	<script>jSubmenu("english","about");</script>
	<style>
		img{
			vertical-align: middle;
	}
	table.klasyfikacja td:first-child{
		font-weight: normal;
		text-align: center;
		vertical-align: middle;
		padding: 10px;
	}
	table.klasyfikacja tr:first-child td:first-child{
		font-weight: bold;
		text-align: center;
	}
	table.klasyfikacja td{
		padding: 10px;
	}
	</style>
</head>

<body>
<?require_once "files/php/menu.php"?>
<h1><script>naglowek("About Polish Scrabble")</script></h1>

<h2>Some history</h2>
 Scrabble in Poland started in 1986, after a youth magazine „Razem” had 
 published the first set of rules adapted for the Polish language and a paper 
 board to cut out. Some tournaments were held then, the first on 11 April 1987 
 with Jacek Ciesielski as a judge. Players used home-made sets or revamped 
 English sets. Alas, the Scrabble movement collapsed after the closing of 
 „Razem” in 1991. <br>
 In 1993 the Polish version of the game was launched by the Polish company 
 „Joker” acting as an agent for „J.W. Spear &amp; Sons”. That year, the first 
 official Polish Championship was held (32 players; high-score system).<br>
&nbsp;In 1995 Mattel took over the game and in the 3rd Polish Championship the 
 system was changed to matchplay. The Scrabble movement revived and in 1996 a 
 rating system for players was established. In 1997 a big promotional campaign 
 supported by the largest Polish daily „Gazeta Wyborcza” and involving a series 
 of more than 30 tournaments in various Polish cities and towns took place. In 
 May 1997 the Polish Scrabble Federation (PSF) was founded.<br>
 Since then the PSF has co-ordinated the organisation of 20-30 tournaments 
 yearly. Apart from the Polish Championship and the Cup of Poland (played in a 
 knockout format), the most interesting event is a 24-hour tournament. It always gathers about 100 players, with a record number of 118 in 2004. In addition, a 
 lot of players meet virtually, using an on-line site for playing Scrabble.


<h2>Polish tile set (100 tiles)</h2>
<table class="klasyfikacja">
	<tr><td>Tiles / Points</td><td>Tiles / Points</td><td>Tiles / Points</td><td>Tiles / Points</td><td>Tiles / Points</td><td>Tiles / Points</td><td>Tiles / Points</td><td>Tiles / Points</td></tr>
	<tr><td><img src="files/tiles/a.png"> 9 / 1</td>
		<td><img src="files/tiles/a5.png"> 1 / 5</td>
		<td><img src="files/tiles/b.png"> 2 / 3</td>
		<td><img src="files/tiles/c.png"> 3 / 2</td>
		<td><img src="files/tiles/c6.png"> 1 / 6</td>
		<td><img src="files/tiles/d.png"> 3 / 2</td>
		<td><img src="files/tiles/e.png"> 7 / 1</td>
		<td><img src="files/tiles/e5.png"> 1 / 5</td>
	</tr><tr>
		
		<td><img src="files/tiles/f.png"> 1 / 5</td>
		<td><img src="files/tiles/g.png"> 2 / 3</td>
		<td><img src="files/tiles/h.png"> 2 / 3</td>
		<td><img src="files/tiles/i.png"> 8 / 1</td>
		<td><img src="files/tiles/j.png"> 2 / 3</td>
		<td><img src="files/tiles/k.png"> 3 / 3</td>
		<td><img src="files/tiles/l.png"> 3 / 2</td>
		<td><img src="files/tiles/l3.png"> 2 / 3</td>
	</tr><tr>
		<td><img src="files/tiles/m.png"> 3 / 2</td>
		<td><img src="files/tiles/n.png"> 5 / 1</td>
		<td><img src="files/tiles/n7.png"> 1 / 7</td>
		<td><img src="files/tiles/o.png"> 6 / 1</td>		
		<td><img src="files/tiles/o5.png"> 1 / 5</td>
		<td><img src="files/tiles/p.png"> 3 / 2</td>
		<td><img src="files/tiles/r.png"> 4 / 1</td>
		<td><img src="files/tiles/s.png"> 4 / 1</td>
	</tr><tr>
		<td><img src="files/tiles/s5.png"> 1 / 5</td>
		<td><img src="files/tiles/t.png"> 3 / 2</td>
		<td><img src="files/tiles/u.png"> 2 / 3</td>
		<td><img src="files/tiles/w.png"> 4 / 1</td>
		<td><img src="files/tiles/y.png"> 4 / 2</td>
		<td><img src="files/tiles/z.png"> 5 / 1</td>
		<td><img src="files/tiles/z9.png"> 1 / 9</td>
		<td><img src="files/tiles/z5.png"> 1 / 5</td>
	</tr><tr>
		<td><img src="files/tiles/blank.png"> 2 / 0</td>
		<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
	</tr>
</table>


<h2>Polish Championships</h2>
 The Polish Championship has been held since 1993 (high-score tournaments for 
 two years, then matchplay). According to the current system there is a 
 preliminary 15-round Swiss tournament for all announced players. Sixteen of 
 them then play round robins in two groups, created according to the results of 
 the preliminary tournament. The two winners of these groups play best of five 
 games for the title.<br><br>

<b>Champions of Poland</b>
<ul>
	<li>2009 <b>Dariusz Kosz (Nysa)</b></li>
	<li>2008 <b>Paweł Jackowski (Warsaw)</b> — Dariusz Dzierza (London) 3:2</li>
	<li>2007 <b>Stanisław Rydzik (Warsaw)</b> — Kazimierz M. Merklejn 3:2</li>
	<li>2006 <b>Krzysztof Mówka (Warsaw)</b> — Mariusz Skrobosz 4:1</li>
	<li>2005 <b>Mariusz Skrobosz</b> — Marek Syczuk (Warsaw) 5:0</li>
	<li>2004 <b>Mariusz Wrześniewski (Warsaw)</b> — Krzysztof Mówka (Toruń) 4:1</li>
	<li>2003 <b>Kazimierz M. Merklejn (Szczecin)</b> — Kamil Górka 4:1</li>
	<li>2002 <b>Kamil Górka (Krakow)</b> — Szymon Fidziński (Dębica) 3:2</li>
	<li>2001 <b>Marcin Mroziuk</b> — Tomasz Zwoliński 4:1</li>
	<li>2000 <b>Mariusz Skrobosz (Warsaw)</b> — Jacek Pietruszka (Cracow) 4:1</li>
	<li>1999 <b>Marcin Mroziuk (Warsaw)</b> — Tomasz Zwoliński 4:1</li>
	<li>1998 <b>Jarosław Borowski (Bielsk Podlaski)</b> — Paweł Stanosz 3:2</li>
	<li>1997 <b>Tomasz Zwoliński </b> — Dariusz Banaszek (Warsaw) 3:2</li>
	<li>1996 <b>Tomasz Zwoliński</b> — Dariusz Puton (Katowice) 378:376</li>
	<li>1995 <b>Paweł Stefaniak (Biała Podlaska)</b> — Marcin Skrzyniarz (Warsaw) 387:387</li>
	<li>1994 <b>Tomasz Zwoliński</b> — Paweł Stanosz (Warsaw) 378:357</li>
	<li>1993 <b>Tomasz Zwoliński (Warsaw)</b> — Michał Derlacki (Warsaw) 288:263</li>
</ul>

<h2>Polish Scrabble Federation</h2>
Founded in May 1997.<br>
166 members.<br>
President: Krzysztof Sporczyk.<br>
16 officially registered clubs (Gdańsk-Gdynia-Sopot, Gorzów Wlkp., Katowice, Krakow, Lamki, Lublin, Łódź, Mielec, Ostróda, Ostrów Wlkp., Piła, Racibórz, Rumia, Szczecin, Warsaw, Wrocław and Zator).<br>
It co-ordinates organisation of Scrabble tournaments, announces the rating of players, helps in promoting the game in Poland.<br>
Mail address: <a onClick="sendMail('pfs','pfs.org.pl')">pfs@pfs.org.pl</a>


<h2>PSF Tournaments</h2>
The PSF holds mostly one-day (6-round) or two-day (12-round) tournaments. The Swiss pairing system is used, with a king of the hill in the last round. The Cup of Poland (June) and Polish Championship (November) are 
held every year. The Grand Prix consists of 7-10 important tournaments through 
the year and has its own classification system. The final event of the season is a 24-hour tournament in Warsaw (December). The results of all PSF-tournaments affect the compilation of the players’ ratings.


<h2>Players' rating</h2>
The player’s ratings have been compiled by the PSF since 1996. In December 2008 the number of rated players was 337 with c. 350 more players with a preliminary 
rating. You have to play at least 30 games in PSF tournaments to be rated and at 
least 1 game to have a preliminary rating. The rating is calculated as an 
average of your last 30-200 games in PSF tournaments. If you have played less 
than 200 games, your score remains valid for two years. For each game played you 
score the rating of your opponent plus/minus 50 points, depending on a win/defeat. 
A new player starts with a rating of 100. If your opponent has a rating under 
100, then during a tournament he/she is treated as a 100-point player anyway. 
The leader of the ratings in December 2008, Mariusz Wrześniewski, has an average of 
149. The lowest average is 59.



<h2>Dictionaries</h2>
„Oficjalny Słownik Polskiego Scrabblisty” (OSPS) exists only in a CD-ROM version. It is a compilation of word-lists extracted from over a dozen dictionaries published by 
Wydawnictwo Naukowe PWN (the Scientific Publishing House PWN). It is the main 
tool for verifying words during tournaments. Apart from checking words its 
functions are: anagramming and showing both the basic form of the word (important 
in Polish, which is very rich in word inflections) and hooks. A new version of 
OSPS with more functions is being prepared.

<?require_once "files/php/bottom.php"?>
</body>
</html>
